<?php

	include("../includes/incsitecommon.php");
	header ("Content-Type: text/css");

	$conn = connect();

	$strdbsql = "SELECT * FROM site_colours WHERE recordID = 1";
	$strType = "single";
	$colours = query($conn, $strdbsql, $strType);

	$primary = $colours['colourPrimary'];
	$secondary = $colours['colourSecondary'];
	$tertiary = $colours['colourTertiary'];

	$base = $colours['colourBase'];

	$backgroundImage = $colours['imageBackground'];
	$backgroundColour = $colours['colourBackground'];

	$font = $colours['font'];

	ob_start();	

?>

/* GENERAL
-------------------------------------------------- */
/*body{background-color:<?php print($backgroundColour); ?>;<?php if(!empty($backgroundImage)){print("background-image:url(/images/themes/".$backgroundImage.");");}?>;font-family:<?php print($font); ?>;padding:158.48px 0 0;}*/
body{background-color:<?php print($base); ?>;font-family:<?php print($font); ?>;font-size:15px;}
body.fixed-head{padding-top:141px;}


/* FONTS
-------------------------------------------------- */
@font-face {
	font-family: 'open sans';
	src: url('../fonts/OpenSans/open_sans.woff2') format('woff2'),
		 url('../fonts/OpenSans/open_sans.woff') format('woff');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'montserrat';
    src: url('../fonts/MontserratLight/montserrat-light.woff2') format('woff2'),
         url('../fonts/MontserratLight/montserrat-light.woff') format('woff');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'montserrat';
    src: url('../fonts/MontserratBold/montserrat-bold.woff2') format('woff2'),
         url('../fonts/MontserratBold/montserrat-bold.woff') format('woff');
    font-weight: bold;
    font-style: normal;
}


/* HEADERS
-------------------------------------------------- */
h1{font-family:Montserrat;font-size:54px;font-weight:bold;text-transform:uppercase;line-height:61px;text-shadow:none;}
h2{font-size:22px;font-weight:normal;text-transform:none;line-height:1.2;text-shadow:none;}
h3{font-size:28px;font-weight:normal;text-transform:none;line-height:normal;text-shadow:none;}
h4{font-size:17px;font-weight:bold;text-transform:uppercase;line-height:20px;text-shadow:none;}
h5{font-size:20px;font-weight:normal;text-transform:none;line-height:20px;text-shadow:none;}
h6{font-size:18px;font-weight:normal;text-transform:none;line-height:20px;text-shadow:none;}


/* TEXT
-------------------------------------------------- */
p{font-size:15px;font-weight:normal;text-align:left;text-transform:none;line-height:20px;}
p a:active,p a:focus,p a:hover{color:<?php print($tertiary); ?>;}
address{font-style:italic;font-weight:bold;}

ul:not(.browser-default) {
    padding-left: 0;
    list-style-type: none;
}

.sidebar ul {
    margin: 0;
}

/* BUTTONS
-------------------------------------------------- */
a:hover, a:focus, a:active{color:white;text-decoration:none;}

.btn-primary{border:solid 3px <?php print($primary); ?>;background:<?php print($primary); ?>;border-radius:10px;color:<?php print($base); ?>;display:inline-block;font-size:16px;font-weight:bold;line-height:66px;padding:0 24px;text-align:center;-webkit-transition:all .3s ease;transition:all .3s ease;width:100%;}
.btn-primary:hover{border:solid 3px <?php print($secondary); ?>;background:<?php print($secondary); ?>;}

.btn-arrow{border-radius:100%;padding:6px 11px;line-height:15px;}

.btn-more-blue{font-size:18px;background:<?php print($primary); ?>;padding:11px 20px;color:#FFFFFF;display:inline-block;line-height:22px;text-shadow:none;outline:none;border:none;}
.btn-more-blue:hover{background:<?php print($secondary); ?>;}
.btn-more-orange{font-size:16px;background:<?php print($secondary); ?>;padding:10px 16px;color:#FFFFFF;display:inline-block;line-height:22px;text-shadow:none;outline:none;border:none;}
.btn-more-orange:hover{background:<?php print($primary); ?>;}

.btn-link{font-size:13px;color:#FFFFFF;background:<?php print($primary); ?>;padding:3px 8px;display:inline-block;margin:0px 3px 8px 0px;}
.btn-link:hover, .btn-link:focus, .btn-link:active{background:<?php print($secondary); ?>;color:#FFFFFF;text-decoration:none;}

.btn-back{font-size:20px;background:<?php print($tertiary); ?>;color:#FFFFFF;padding:11px 10px;display:inline-block;margin-top:10px;vertical-align:top;}



/* MAIN CONTAINERS
-------------------------------------------------- */
body > .row{margin:0;}

.row{margin-bottom:30px;}
.row:last-child{margin-bottom:0;}

.main-content{padding-right:30px;}
.sidebar-content{float:right;}


/* TO TOP
-------------------------------------------------- */
.to-top{background-color:<?php print($primary); ?>;border-radius:10px;box-shadow:0rem 0.15rem 0rem 0rem rgba(0,0,0,0.1);bottom:20px;color:<?php print($base); ?>;cursor:pointer;font-size:22px;height:40px;line-height:32px;opacity:0;position:fixed;right:20px;text-align:center;transform:translateY(60px);-webkit-transform:translateY(60px);transition:all .6s;-webkit-transition:all .6s;width:40px;z-index:600;}
.to-top:hover{background-color:<?php print($secondary); ?>;}
.fixed-head .to-top{opacity:1;transform:translateY(0);-webkit-transform:translateY(0);}
.to-top i{font-size:14px;vertical-align:middle;}


/* TOP
-------------------------------------------------- */
.block-top{background-color:<?php print($secondary); ?>;background-image:url(/images/elements/top.jpg);background-position:center center;background-size:cover;position:fixed;left:0;right:0;top:0;z-index:1001;}
.block-top:before{content:'';position:absolute;left:0;right:0;top:0;bottom:0;opacity:.9;background:<?php print($secondary); ?>;}

.block-top .container{position:relative;}

.block-top button{background-color:<?php print($primary); ?> !important;border:none;border-radius:0;box-shadow:none !important;font-family:open sans;font-size:13px;min-width:170px;outline:none !important;padding:14px 21px 16px 21px;}
.block-top button:hover{background-color:<?php print($primary); ?> !important;}
.block-top .open button,.block-top button:focus,.block-top button:active{background-color:<?php print($primary); ?> !important;}
.block-top button .caret{margin-left:18px;}

.block-top .dropdown-menu{background-color:<?php print($secondary); ?>;border:none;border-radius:0;box-shadow:none;display:block;height:0;margin:0;overflow:hidden;padding:0;-webkit-transition:height .1s linear;transition:height .1s linear;}
.block-top .dropdown-menu li:first-child{margin-top:12px;}
.block-top .dropdown-menu li:last-child{margin-bottom:12px;}
.block-top .dropdown-menu li a{color:<?php print($base); ?>;cursor:pointer;font-size:13px;min-width:170px;padding:10px 25px;}
.block-top .dropdown-menu li a:hover{background:rgba(255, 255, 255, 0.2);}

.block-top .open .dropdown-menu{height:138px;}

.block-top .label{color:<?php print($base); ?>;font-size:13px;font-weight:normal;margin:14px 0;}

.block-top .email{background-color:transparent;border:solid 3px <?php print($primary); ?>;border-radius:40px;color:<?php print($base); ?>;font-size:13px;line-height:26px;margin:8px 0 8px 20px;padding:0 9px;transition:all .3s;-webkit-transition:all .3s;}
.block-top .email:hover{background-color:<?php print($primary); ?>;}

.block-contact{display:inline-block;list-style:none;margin:9px -23px;padding:0 6px 0 0;}
.block-contact li{display:inline-block;vertical-align:middle;padding:5px 5px 5px 23px;color:<?php print($base); ?>;font-size:13px;}
.block-contact li a{color:inherit;outline:none;}
.block-contact li i{display:inline-block;vertical-align:middle;margin:0 12px 0 0;font-size:16px;color:<?php print($primary); ?>;}
.block-contact li span{display:inline-block;vertical-align:middle;}

/* HEADER
-------------------------------------------------- */
header{background-color:<?php print($base); ?>;background-position:center right;background-repeat:no-repeat;background-size:auto 100%;box-shadow:0 5px 12px rgba(0,0,0,.25);padding:30px 0 0;position:relative;transition:transform .3s,box-shadow .3s;-webkit-transition:transform .3s,box-shadow .3s;width:100%;will-change:transform,top,box-shadow;z-index:50 !important;}
/*header .logo{width:260px;}*/
/*.fixed-head header{position:fixed;top:-170px;transform:translateY(62px);-webkit-transform:translateY(62px);transition:transform .6s,box-shadow .6s;-webkit-transition:transform .6s,box-shadow .6s;}*/
header .row{margin:0;}
header .row div{padding:0;}

header .disguise{background-color:<?php print($base); ?>;display:none;left:0;padding:20px 15px 0;position:absolute;top:73px;}
/*.fixed-head header .logo.disguise{display:block;}*/

header input{border:solid 1px #cacaca;border-top-left-radius:10px;border-bottom-left-radius:10px;height:42px;min-width:220px;outline:none;padding:0 15px;transition:border-color .3s;-webkit-transition:border-color .3s;}
header button[type=submit]{background-color:<?php print($primary); ?>;border:solid 1px <?php print($primary); ?>;;border-top-right-radius:10px;border-bottom-right-radius:10px;color:<?php print($base); ?>;height:42px;outline:none;text-align:center;vertical-align:top;width:42px;}
header button[type=submit]:hover{background-color:<?php print($secondary); ?>;border-color:<?php print($secondary); ?>;}

@media only screen and (max-width : 1570px) {
	/*.fixed-head header .logo.disguise{display:none;}*/
}

/* NAVBAR
-------------------------------------------------- */
.navbar{background-color:transparent;border:none;border-radius:0;margin-bottom:0;min-height:33px;padding:15px 0 0;width:100%;}
/*.navbar.static{position:fixed;top:0;}*/
.navbar-brand{display:inline-block;height:auto;padding:40px 50px 8px 15px;position:relative;}
.navbar-brand img{max-width:220px;opacity:.9;}
.navbar-brand img:hover{opacity:1;}

.navbar-collapse{padding:0;}

.navbar-nav{padding:0;width:100%;}
.navbar-nav li{margin:0 15px;padding-bottom:15px;}
.navbar-nav li a{background-color:transparent;border-bottom:solid 3px transparent;color:<?php print($secondary); ?> !important;border-radius:0;display:inline-block;font-family:montserrat;font-size:14px;font-weight:bold;padding:5px 0;text-transform:uppercase;-webkit-transition:all .2s linear;transition:all .2s linear;vertical-align:middle;}
.navbar-nav li:first-child{margin-left:0 !important;}
.navbar-nav li:last-child{margin-right:0 !important;}
.navbar-nav li a.active:hover{background-color:<?php print($base); ?>;color:<?php print($primary); ?> !important;}
.navbar-nav li a:hover,.navbar-nav li a.active{background-color:<?php print($base); ?>;color:<?php print($primary); ?> !important;}
.navbar-nav .open a, .navbar-nav .open a:focus, .navbar-nav .open a:hover{background-color:transparent !important;border-color:transparent !important;}
.navbar-nav li a.highlight{background-color:<?php print($primary); ?>;border-radius:50%;border:none;color:<?php print($base); ?> !important;height:33px;line-height:33px;padding:0;text-align:center;text-shadow:none;width:33px;}
.navbar-nav li a.facebook{background-color:#3B5998;border:none;border-radius:50%;color:#FFFFFF !important;height:33px;line-height:33px;padding:0;text-align:center;text-shadow:none;width:33px;}
.navbar-nav li a:hover{color:<?php print($primary); ?> !important;}
.navbar-nav li a.highlight:hover{background-color:<?php print($secondary); ?>;border-color:<?php print($secondary); ?>;color:<?php print($base); ?> !important;}
.navbar-nav li a.facebook:hover{background-color:#5371b1;color:#FFFFFF !important;}
.navbar-nav li a.glow{background-color:<?php print($primary); ?>;border-radius:10px;color:#FFFFFF !important;margin:-3px 0;padding:8px 18px;}
.navbar-nav li a.glow:hover{background-color:<?php print($tertiary); ?>;color:#FFFFFF !important;}
.navbar-nav li a i{font-weight:bold;margin-left:5px;}

.navbar-nav li .dropdown-menu{background-color:transparent;display:block;border:none;border:none;box-shadow:none;position:absolute;z-index:600;margin:0;top:100%;left:50%;width:auto;height:auto;overflow:hidden;opacity:0;padding:20px 0 0;pointer-events:none;-webkit-transition:all .25s ease-out;-moz-transition:all .25s ease-out;-o-transition:all .25s ease-out;transition:all .25s ease-out;-webkit-transform:translate(-50%,30px);-moz-transform:translate(-50%,30px);-ms-transform:translate(-50%,30px);-o-transform:translate(-50%,30px);transform:translate(-50%,30px)}
.navbar-nav li .dropdown-menu ul{background-color:<?php print($base); ?>;border-radius:10px;box-shadow:0 5px 15px rgba(0,0,0,.16);list-style:none;margin:0;padding:10px;min-width:220px;}
.navbar-nav li .dropdown-menu:before{content:"";position:absolute;width:0;height:0;border-width:5px;border-style:solid;border-color:transparent transparent white;top:10px;left:50%;-webkit-transition:.20s ease;transition:.20s ease;transform:translateX(-50%);-webkit-transform:translateX(-50%);pointer-events:none;}
.navbar-nav li.dropdown:hover .dropdown-menu{opacity:1;overflow:visible;-webkit-transform:translate(-50%,0);-moz-transform:translate(-50%,0);-ms-transform:translate(-50%,0);-o-transform:translate(-50%,0);transform:translate(-50%,0);pointer-events:auto}
.navbar-nav li .dropdown-menu li{margin:0;padding-bottom:0;}
.navbar-nav li .dropdown-menu li:hover{background-color:transparent;}
.navbar-nav li .dropdown-menu li a{padding:10px 15px;width:100%;}
.navbar-nav li .dropdown-menu li a:hover{background-color:<?php print($base); ?>;color:<?php print($primary); ?> !important;}

#contenedor{
	width:100%;
	height: 198px;
	overflow:hidden;
	position: relative;
}
#image-slider{
	width: 100%;
	height: 168px;
	list-style: none;
	margin: 0;
	position: relative;
	transition: all 500ms ease-in-out;
}

#image-slider li:nth-child(1){
	position: absolute;
	bottom:0;
	top: 0px;
	left: 0px;
	width: 100%;
}
#image-slider li:nth-child(2){
	position: absolute;
	bottom:0;
	top: 0px;
	left: 100%;
	width: 100%;
}
#image-slider li:nth-child(3){
	position: absolute;
	bottom:0;
	top: 0px;
	left: 200%;
	width: 100%;
}
#image-slider li:nth-child(4){
	position: absolute;
	bottom:0;
	top: 0px;
	left: 300%;
	width: 100%;
}

.mini{
	background-color: <?php print($primary); ?>;
	border-right: solid 1px <?php print($base); ?>;
	cursor: ew-resize;
	float: left;
	height: 30px;
	margin-top: 168px;
	width: 25%;
	transition: all .5s linear;
}

#contenedor div:nth-child(1):active ~ #image-slider{
  transform: translateX(0px) translateZ(0px);
}
#contenedor div:nth-child(2):active ~ #image-slider{
  transform: translateX(-100%) translateZ(0px);
}
#contenedor div:nth-child(3):active ~ #image-slider{
  transform: translateX(-200%) translateZ(0px);
}
#contenedor div:nth-child(4):active ~ #image-slider{
  transform: translateX(-300%) translateZ(0px);
}


/* CAROUSEL
-------------------------------------------------- */
/*.carousel{max-height:1000px; max-width:1170px; margin: auto; position:relative;}*/
.carousel{max-height:1000px; margin: auto; position:relative;}
.carousel .carousel-inner{height:100%;}
.carousel .item{background-position:center center;background-repeat:no-repeat;background-size:cover;height:1000px;overflow:hidden;}

.carousel-caption {bottom:auto;left:auto;margin:0;padding:0;position:absolute;right:auto;top:10%;transform:translateY(-50%);}

.carousel-caption-top {bottom:auto;left:auto;margin:0;padding:0;position:absolute;right:auto;top:10%;transform:translateY(-50%);}

.carousel-caption-bottom {bottom:1%;left:auto;margin:0;padding:0;position:absolute;right:auto;top:auto;transform:translateY(-50%);}

.carousel-caption .container{padding:0;}

/*.carousel-caption h1{color:<?php print($base); ?>;display:inline-block;font-size:60px;line-height:60px;margin:28px 0 0;text-align:left;text-transform:none;width:100%;}*/

.carousel-caption h1 {
    color: #ffffff;
    display: inline-block;
    font-size: 40px;
    line-height: 40px;
    margin: 28px auto;
    text-align: center;
    text-transform: none;
    max-width: 1000px;
	padding:20px;
	background-color: rgba(0, 65, 137, 0.5);
}

.carousel-caption .bottom h1 {
    color: #ffffff;
    display: inline-block;
    font-size: 40px;
    line-height: 40px;
    margin: 28px auto;
    text-align: right;
    text-transform: none;
    width: 880px;
	padding:20px;
	background-color: rgba(0, 65, 137, 0.5);
}

.carousel-caption h1 strong{color:<?php print($base); ?>;}
.devSlider .carousel-caption h1{color:<?php print($base); ?>;}
.carousel-caption h2{color:<?php print($base); ?>;font-size:24px;line-height:32px;margin:20px 0 40px;text-shadow:none;text-align:left;}
.devSlider .carousel-caption h2{color:<?php print($base); ?>;}
.carousel-caption a{color:<?php print($base); ?>;border:solid 3px <?php print($primary); ?>;background:<?php print($primary); ?>;border-radius:10px;float:left;font-family:Montserrat;font-size:14px;margin-right:15px;font-weight:bold;padding:8px 37px 8px 21px;position:relative;text-shadow:none;text-transform:none;-webkit-transition:all .3s ease;transition:all .3s ease;}
.carousel-caption a:hover{background:<?php print($secondary); ?>;border-color:<?php print($secondary); ?>;color:<?php print($base); ?>;}
.carousel-caption a.special{background:<?php print($primary); ?>;border-color:<?php print($primary); ?>;color:#FFFFFF;}
.carousel-caption a i{right:15px;width:auto;position:absolute;top:12px;font-size:14px;height:14px;line-height:14px;color:<?php print($base); ?>;-moz-box-sizing:content-box;-webkit-transition:all .3s ease;transition:all .3s ease;}
.carousel-caption a:hover i{color:<?php print($base); ?>;}
.carousel-caption .house-image{background-color:transparent;border:none;border-radius:50%;display:inline-block;float:none;padding:0;position:relative;}
.carousel-caption .house-image:hover{background-color:transparent;opacity:.9;}
.carousel-caption .house-image img{width:220px;margin-top:40px;}
.carousel-caption .house-image span{background:<?php print($primary); ?>;bottom:10px;color:<?php print($base); ?>;border:solid 3px <?php print($primary); ?>;border-radius:10px;padding:8px 37px 8px 21px;position:relative;-webkit-transition:all .3s ease;transition:all .3s ease;position:absolute;right:50%;transform:translateX(50%);-webkit-transform:translateX(50%);width:180px;}
.carousel-caption .house-image:hover span{background-color:<?php print($secondary); ?>;border-color:<?php print($secondary); ?>;}
.carousel-caption .house-image span i{right:15px;width:auto;position:absolute;top:12px;font-size:14px;height:14px;line-height:14px;color:<?php print($base); ?>;-moz-box-sizing:content-box;-webkit-transition:all .3s ease;transition:all .3s ease;}
.carousel-caption .house-image:hover span i{color:<?php print($primary); ?>;}

.carousel-indicators{bottom:120px;left:50%;margin:0;padding:0 15px;right:auto;text-align:left;top:auto;transform:translateX(-50%);width:1170px;}
.carousel-indicators li{background:<?php print($base); ?>;border:none;display:inline-block;width:12px;height:12px;opacity:.9;margin:0 12px 0 0;transition:width .3s;-webkit-transition:width .3s;}
.carousel-indicators li.active{background:<?php print($tertiary); ?>;border:none;height:12px;width:26px;margin:0 12px 0 0;}

.carousel-control{width:45px;height:45px;background-color:transparent;background-image:none !important;border:3px solid <?php print($secondary); ?>;color:<?php print($secondary); ?>;line-height:40px;text-shadow:none;top:50%;transform:translateY(-50%);-webkit-transform:translateY(-50%);-webkit-transition:all .3s ease;transition:all .3s ease;}
.carousel-control.left{left:20px;}
.carousel-control.right{right:20px;}
.carousel-control:hover{background-color:<?php print($primary); ?>;border-color:<?php print($primary); ?>;color:<?php print($base); ?>;}


/* QUOTE BAR
-------------------------------------------------- */
.block-quote{background-color:<?php print($primary); ?>;bottom:0;padding:17px 0;position:relative;width:100%;}
.block-quote.static{background-color:<?php print($primary); ?>;position:relative;}
.block-quote img{margin:-4px 15px 0 0;width:30px;}
.block-quote span{display:inline-block;font-family:Montserrat;font-size:20px;font-weight:bold;color:white;letter-spacing:-.02em;line-height:24px;padding:10px 0;text-transform:none;}
.block-quote a{color:<?php print($primary); ?>;border:solid 3px <?php print($base); ?>;background:<?php print($base); ?>;border-radius:10px;float:right;font-family:Montserrat;font-size:14px;font-weight:bold;padding:8px 37px 8px 21px;position:relative;text-transform:none;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-quote.static a{background-color:<?php print($base); ?>;border-color:<?php print($base); ?>;color:<?php print($primary); ?>;}
.block-quote a:hover{background:<?php print($secondary); ?>;border-color:<?php print($secondary); ?>;color:<?php print($base); ?>;}
.block-quote.static a:hover{background:<?php print($secondary); ?>;border-color:<?php print($secondary); ?>;color:<?php print($base); ?>;}
.block-quote a i{right:15px;width:auto;position:absolute;top:12px;font-size:14px;height:14px;line-height:14px;color:<?php print($primary); ?>;-moz-box-sizing:content-box;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-quote.static a i{color:<?php print($primary); ?>;}
.block-quote a:hover i{color:<?php print($base); ?>;}
.block-quote.static a:hover i{color:<?php print($base); ?>;}


/* TYPES
-------------------------------------------------- */
.block-type{background-position:center center;background-size:cover;box-shadow:0 5px 15px rgba(0,0,0,.16);border-radius:10px;display:table;height:auto;margin:-58px 0;min-height:116px;overflow:hidden;position:relative;width:100%;z-index:10;}
.block-type a{background:rgba(57, 89, 121, .85);display:table-cell;padding:30px;-webkit-transition:all .3s ease;transition:all .3s ease;vertical-align:middle;width:100%;}
.block-type:hover a{background:rgba(108, 108, 108, 0.85);}
.block-type a i{color:<?php print($primary); ?>;display:inline-block;font-size:40px;margin-bottom:8px;padding:8px 16px 9px 0;}
.block-type a svg,.block-type a img{float:left;;margin-bottom:8px;padding:8px 16px 9px 0;width:56px;}
.block-type a h4{color:<?php print($base); ?>;display:inline-block;font-size:22px;line-height:28px;padding:0;margin:0;vertical-align:top;text-transform:none;width:100%;text-align:center;}
.block-type a p{margin:0 0 15px;color:<?php print($base); ?>;padding-left:55px;font-size:14px;line-height:24px;}
.block-type a a{display:inline-block;vertical-align:top;color:<?php print($primary); ?>;border: solid 3px <?php print($base); ?>;background:<?php print($base); ?>;border-radius:10px;font-size:14px;font-weight:bold;margin-left:55px;padding:8px 37px 8px 21px;position:relative;text-transform:none;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-type a a:hover{color:<?php print($secondary); ?>;text-decoration:none;}
.block-type a a i{right:15px;width:auto;position:absolute;top:12px;font-size:14px;height:14px;line-height:14px;right:0;top:3px;color:<?php print($primary); ?>;-moz-box-sizing:content-box;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-type a a:hover i{color:<?php print($secondary); ?>;}


/* BREADCRUMB	
-------------------------------------------------- */
.block-bread{margin-bottom:10px;padding:35px 0 15px;}
.block-bread ul{list-style:none;margin:0;padding:0;}
.block-bread ul li{display:inline-block;font-size:12px;margin:0 12px 10px 0;}


/* BOOK	
-------------------------------------------------- */
.block-book{background-color:<?php print($primary); ?>;border-radius:10px;margin:0 0 30px;padding:37px 30px;}
.block-book h5{color:<?php print($base); ?>;font-family:Montserrat;font-size:16px;font-weight:bold;margin:0 0 13px;}
.block-book p{color:<?php print($base); ?> !important;font-size:13px;line-height:22px;margin:0 0 40px;opacity:1 !important;}
.block-book a{border:solid 3px <?php print($base); ?>;background:<?php print($base); ?>;border-radius:10px;color:<?php print($secondary); ?>;display:inline-block;font-family:Montserrat;font-weight:bold;line-height:33px;padding:0 12px;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-book a:hover{border-color:<?php print($secondary); ?>;background-color:<?php print($secondary); ?>;color:<?php print($base); ?>;}
.block-book a i{font-weight:bold;margin-right:8px;}


/* PRICE	
-------------------------------------------------- */
.block-price{background-color:<?php print($primary); ?>;border-radius:10px;color:<?php print($base); ?>;margin:0 0 30px;overflow:hidden;padding:10px 25px 10px 125px;position:relative;text-align:center;}
.block-price span{background-color:<?php print($tertiary); ?>;display:inline-block;left:0;line-height:29px;padding:10px 25px;position:absolute;top:0;}
.block-price i{display:inline-block;font-size:18px;line-height:29px;margin-right:8px;}
.block-price h5{display:inline-block;font-family:Montserrat;font-size:16px;font-weight:bold;line-height:29px;margin:0;}


/* TABS
-------------------------------------------------- */
.block-tabs{display:inline-block;list-style:none;margin:0 0 -15px;padding:0;vertical-align:top;width:100%;}
.block-tabs li{font-family:Montserrat;float:left;font-size:16px;margin:0 0 15px;min-height:44px;padding:0 25px;position:relative;width:50%;}
.block-tabs li:hover{border-color:<?php print($primary); ?>;}
.block-tabs li:before{border-top:8px solid #65af22;border-top-width:.8rem;content:"";position:absolute;top:5px;top:.5rem;left:0;width:8px;width:.8rem;}


/* HIGHLIGHTS	
-------------------------------------------------- */
.block-high{padding:0;}
.block-high ul{display:flex;list-style:none;margin:0 0 5px;padding:0;}
.block-high li{margin:0 0 10px 20px;padding:0 0 0 21px;position:relative;}
.block-high li:first-child{margin:0;padding:0;}
.block-high li:before{content:'';width:1px;height:12px;background:#DDD;position:absolute;left:0;top:4px;}
.block-high li:first-child:before{display:none;}
.block-high span{font-size:13px;}
.block-high i{color:<?php print($primary); ?>;font-size:15px;margin:0 5px 0 0;}


/* OFFICES
-------------------------------------------------- */
.block-office{margin:0;}
.block-office h3{font-size:20px;color:<?php print($tertiary); ?> !important;line-height:45px;text-align:center;font-weight:BOLD;margin:0 0 15px !important;padding:0;}
.block-office ul{list-style:none;margin:0 0 25px;padding:0;}
.block-office li{line-height:22px;margin:0 0 13px;text-align:left;}
.block-office a{color:inherit;}
.block-office i{background-color:<?php print($primary); ?>;color:<?php print($base); ?>;border-radius:10px;font-size:18px;margin:0px 15px 0 0;vertical-align:top;line-height:30px;text-align:center;width:30px;}
.block-office span{display:inline-block;font-family:Montserrat;width:calc(70% - 30px);}
.block-office a span{text-decoration:underline;}


/* LOGO CONTAINER	
-------------------------------------------------- */
.block-logo{background-position:center center;background-repeat:no-repeat;background-size:contain;height:370px;width:100%;}


/* WIDGET
-------------------------------------------------- */
.block-widget{background:<?php print($base); ?>;border-radius:2px;float:right;margin-top:0;max-width:292px;padding:45px 30px 30px;position:relative;width:100%;}

.block-widget h4{color:<?php print($tertiary); ?>;margin:0 0 30px;}
.block-widget a{background:<?php print($primary); ?>;border:none;border-radius:40px;color:<?php print($base); ?>;display:inline-block;opacity:.9;outline:none;padding:14px 20px;text-shadow:none;text-transform:uppercase;width:100%;-webkit-transition:background 0.2s;-moz-transition:background 0.2s;-o-transition:background 0.2s;transition:background 0.2s;}
.block-widget button{background:<?php print($primary); ?>;border:none;border-radius:40px;color:<?php print($base); ?>;display:inline-block;margin-top:15px;opacity:.9;outline:none;padding:14px 20px;text-shadow:none;text-transform:uppercase;width:100%;-webkit-transition:background 0.2s;-moz-transition:background 0.2s;-o-transition:background 0.2s;transition:background 0.2s;}

.block-widget input,.block-widget select{background-color:transparent;border:none;border-bottom:solid 2px <?php print($tertiary); ?>;color:<?php print($tertiary); ?>;margin-bottom:10px;outline:none;width:calc(100% - 24px);}
.block-widget input{padding:10px 12px;}
.block-widget select{padding:10px 10px;}

.block-slider{margin:auto;padding:10px 12px;text-align:left;width:calc(100% - 24px);}
.block-slider label{color:<?php print($tertiary); ?>;display:inline-block;font-weight:normal;text-shadow:none;}
.block-slider input{border:none;float:right;font-weight:normal !important;margin-bottom:0;padding:0;text-align:right;width:120px;}


/* TEXT
-------------------------------------------------- */
/*.block-text{background-image:url(/images/elements/tree.png);background-position:top 60px right;background-repeat:no-repeat;padding:60px 0 30px;position:relative;}*/
.block-text{padding:60px 0 30px;position:relative;}

.block-text h2{color:<?php print($primary); ?>;font-weight:bold;margin: 0 0 15px;}
.block-text h3{color:<?php print($tertiary); ?>;margin:0 0 17px;}
.block-text h4{font-family:Montserrat;font-size:20px;font-weight:700;margin:0 0 23px;letter-spacing:-.04em;line-height:28px;padding:23px 0 0;text-transform:none;}
.block-text h4:after{content:'';display:block;width:35px;height:6px;background:<?php print($primary); ?>;border-radius:10px;margin:4px 0 0;}
.block-text p{color:<?php print($tertiary); ?>;font-size:15px;line-height:22px;margin:0 auto 15px;text-shadow:none;}
.block-text p strong{font-size:16px;font-weight:bold;letter-spacing:-0.4px;line-height:26px;}
.block-text blockquote{border-left:none;margin:30px 0;padding:15px 30px;background:#F2F2F2;font-size:16px;line-height:26px;color:<?php print($tertiary); ?>;font-style:italic;}
.block-text .block-image img{margin:0 0 30px;width:100%;}
.block-text .btn-standard{border:solid 3px <?php print($secondary); ?>;background:<?php print($secondary); ?>;border-radius:10px;color:<?php print($base); ?>;display:inline-block;font-family:Montserrat;font-size:14px;font-weight:bold;padding:11.5px 21px 11.5px 37px;position:relative;text-transform:none;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-text .btn-standard:hover{background:<?php print($primary); ?>;border-color:<?php print($primary); ?>;}
.block-text .btn-standard i{left:15px;width:auto;position:absolute;top:15px;font-size:14px;height:14px;line-height:14px;color:<?php print($primary); ?>;-moz-box-sizing:content-box;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-text .btn-standard:hover i{color:<?php print($base); ?>;}


/* PROPERTY
-------------------------------------------------- */
.block-detail{background-image:url(/images/elements/tree.png);background-position:top 60px right;background-repeat:no-repeat;margin-top:40px;padding:40px 0;position:relative;text-align:left;}

.block-detail.add-decor:after{bottom:75px;content:url(/images/decor.png);left:50%;position:absolute;top:auto;transform:translateX(-50%);}

.block-detail .carousel{height:auto;margin:0 0 -42px;}

.block-detail h1{color:<?php print($base); ?>;margin:0 0 5px;font-size:26px;font-weight:700;line-height:30px;text-transform:none;}
.block-detail p{color:<?php print($base); ?>;margin:0 auto 15px;}
.block-para p{color:#333333;}

.block-detail .price{color:<?php print($primary); ?>;display:inline-block;font-size:28px;font-weight:bold;margin:0 0 15px;vertical-align:top;width:100%;}

.block-detail .block-options{display:flex;flex-wrap:wrap;list-style:none;margin:15px 0 0;padding:0;vertical-align:top;width:100%;}
.block-detail .block-options li{float:left;flex-grow:1;margin-right:15px;}
.block-detail .block-options li:last-child{margin-right:0;}
.block-detail .block-options li a{border:solid 3px <?php print($primary); ?>;background:<?php print($primary); ?>;border-radius:10px;color:<?php print($base); ?>;display:inline-block;font-size:16px;font-weight:bold;line-height:66px;padding:0 24px;text-align:center;-webkit-transition:all .3s ease;transition:all .3s ease;width:100%;}
.block-detail .block-options li a:hover{border-color:<?php print($secondary); ?>;background:<?php print($secondary); ?>;}
.block-detail .carousel .item{height:auto;}

.block-contain{background-color:<?php print($secondary); ?>;border-radius:10px;padding:15px;}

.block-detail .nav-tabs{background-color:transparent;position:relative;z-index:10;}
.block-detail .nav-tabs > li > a{font-weight:normal;padding:10px 20px;}
.block-detail .nav-tabs > li > a, .block-detail .nav-tabs > li > a:focus, .block-detail .nav-tabs > li > a:hover{background-color:<?php print($secondary); ?>;border-top:solid 2px transparent !important;border-top-left-radius:10px;border-top-right-radius:10px;margin-right:5px;}
.block-detail .nav-tabs > li.active > a, .block-detail .nav-tabs > li.active > a:focus, .block-detail .nav-tabs > li.active > a:hover{background-color:<?php print($primary); ?>;border-color:<?php print($primary); ?> !important;color:<?php print($base); ?>;}
.block-detail .tab-content{background-color:<?php print($base); ?>;padding:30px;}


/* SOCIAL
-------------------------------------------------- */
.block-share{border-top:solid 3px <?php print($primary); ?>;color:<?php print($primary); ?>;padding:0;position:relative;text-align:left;}
.block-share h3{float:left;font-size:24px;font-weight:bold;margin:47px 0;line-height:32px;margin-right:30px;padding-right:50px;position:relative;width:30%;}
.block-share h3:after{background-color:<?php print($primary); ?>;bottom:0;content:"";position:absolute;right:0;top:0;width:3px;}
.block-share {width:100%;}


/* SLICK
-------------------------------------------------- */
.slick {
 background-color:#f3f3f4;
 border-radius:2px;
 height:600px;
 overflow:hidden;
 padding:10px;
 width:100%;
}
.slick.main {
 border-bottom-left-radius:0;
 border-bottom-right-radius:0;
}
.slick.carousel {
 border-bottom:solid 1px #3138491a;
 border-top-left-radius:0;
 border-top-right-radius:0;
 height:110px;
 margin:-10px 0 24px;
}
.slick-arrow {
 background-color:#d32f2fe6;
 color:#ffffff;
 border:none;
 border-radius:50%;
 height:32px;
 left:30px;
 padding:4px 0;
 position:absolute;
 top:50%;
 transform:translateY(-50%);
 -webkit-transform:translateY(-50%);
 width:32px;
 z-index:10;
}
.slick-arrow:hover {
 background-color:#313849e6;
}
.slick-next {
 left:auto;
 right:30px;
}
.slick-track {
 height:100%;
}
.slick-list {
 border-radius:2px;
 height:100% !important;
}
.carousel .slick-list {
 margin:0 -5px;
}
.slick-slide img {
 position:absolute;
 top:50%;
 transform:translateY(-50%);
 -webkit-transform:translateY(-50%);
 width:100%;
}
.slick-prev:before,
.slick-next:before {
 color:black;
}
.slick-slide {
 opacity:.2;
 outline:none;
 position:relative;
 transition:all ease-in-out .3s;
}
.carousel .slick-slide {
 border-radius:2px;
 cursor:pointer;
 margin:0 5px;
 overflow:hidden;
}
.slick-active {
 opacity:.2;
}
.slick-current {
 opacity:1;
}

/* FEATURES
-------------------------------------------------- */
.block-feat{background-color:<?php print($base); ?>;padding:73px 0;text-align:center;}

.block-feat h3{font-family:Montserrat;font-size:20px;font-weight:700;letter-spacing:-.04em;line-height:28px;margin:0 0 23px;padding:23px 0 0;}
.block-feat h3:after{content:'';display:block;width:35px;height:6px;background:<?php print($primary); ?>;border-radius:10px;margin:13px 0 0;}

.block-feat h4{color:<?php print($tertiary); ?>;margin:0 0 60px;text-align:center;}
.block-feat h4 span{display:inline-block;}

.block-feat p{color:<?php print($tertiary); ?>;margin:0 auto 80px;opacity:.6;text-align:center;text-shadow:none;width:85%;}

.block-feat .dropdown{display:inline-block;}
.block-feat .dropdown button{background:none;border-radius:0;color:<?php print($tertiary); ?>;font-size:inherit;font-weight:inherit;line-height:inherit;margin:-10px 10px 0;opacity:1;outline:none;padding:10px;vertical-align:top;}
.block-feat .dropdown button:hover{background-color:rgba(0, 0, 0, 0.05) !important;}
.block-feat .dropdown button:focus,.block-feat h4 .dropdown.open button{background-color:rgba(0, 0, 0, 0.08) !important;border:none;box-shadow:none;color:<?php print($tertiary); ?> !important;outline:none;}
.block-feat .dropdown button i{font-size:12px;height:20px;line-height:20px;margin-left:8px;vertical-align:top;-webkit-transition:all 0.2s;-moz-transition:all 0.2s;-o-transition:all 0.2s;transition:all 0.2s;}

.block-feat .dropdown.default{width:100%;}
.block-feat .dropdown.default button{background-color:#cacaca;border:0;font-size:13px;margin:0;padding:15px 30px 14px;text-align:left;text-transform:none;width:100%;}
.block-feat .dropdown.default button[disabled]{background:inherit;opacity:.4;}
.block-feat .dropdown.default button i{float:right;}
.block-feat .dropdown.default button.error{background:inherit;border-color:#F44336;color:#F44336;font-size:inherit;line-height:inherit;opacity:1;}
.block-feat .dropdown.default button.error:hover{background:rgba(244, 67, 54, 0.05) !important;}
.block-feat .dropdown.default .dropdown-menu{margin:0;width:100%;}
.block-feat .dropdown.default .dropdown-menu li{padding:15px 30px 14px;text-align:left;}

.block-feat .dropdown .dropdown-menu{background:<?php print($base); ?>;border:none;border-radius:4px;margin:0 8px;padding:0;width:calc(100% - 16px);}
.block-feat .dropdown .dropdown-menu li{background:rgba(0, 0, 0, 0.02);cursor:pointer;font-size:14px;padding:8px;text-align:center;}
.block-feat .dropdown .dropdown-menu li:hover{background:rgba(0, 0, 0, 0.04);}
.block-feat .dropdown .dropdown-menu li.selected{display:none;}

.block-feat form .callback-con{position:relative;text-align:center;}

.block-feat form{border:0;padding:45px 50px;margin:2em 0 2em 0;text-align:left;border-radius:0;background:#f2f2f2;position:relative;}
.block-feat form.add-decor:before{background-color:<?php print($secondary); ?>;background-image:url(/images/company/ross-icon-outline.png);background-position:center center;background-repeat:no-repeat;background-size:65px;border:solid 5px <?php print($base); ?>;border-radius:50%;content:"";height:120px;position:absolute;right:-45px;top:-45px;width:120px;}
.block-feat form input[type="text"],.block-feat form input[type="number"],.block-feat form input[type="email"],.block-feat form textarea{font-size:13px;line-height:18px;color:#222;background:#cacaca;border:0;box-shadow:none;margin-bottom:0;outline:0;padding:16px 30px 15px;border-radius:0;-webkit-transition:all .3s ease;transition:all .3s ease;box-sizing:border-box !important;width:100%;display:block;height:49px;}
.block-feat form input:focus{background-color:<?php print($base); ?>;border-radius:2px;box-shadow:0 0 0 2px <?php print($primary); ?>;}
.block-feat form input[readonly="readonly"],.block-feat form textarea[readonly="readonly"]{opacity:.4;}
.block-feat form input[type=number]::-webkit-outer-spin-button,.block-feat form input[type=number]::-webkit-inner-spin-button{-webkit-appearance:none;margin:0;}
.block-feat form input[type=number]{-moz-appearance:textfield;}
.block-feat form input[type="text"].error,.block-feat form input[type="text"].error:hover,.block-feat form input[type="text"].error:focus,.block-feat form input[type="number"].error,.block-feat form input[type="number"].error:hover,.block-feat form input[type="number"].error:focus,.block-feat form input[type="email"].error,.block-feat form input[type="email"].error:hover,.block-feat form input[type="email"].error:focus{border-color:#F44336;color:#F44336;opacity:1;}
.block-feat form button{border:solid 3px <?php print($secondary); ?>;background:<?php print($secondary); ?>;border-radius:0;color:<?php print($base); ?>;float:right;font-family:Montserrat;font-size:14px;font-weight:bold;margin-top:0;outline:none;padding:11.5px 37px 11.5px 21px;position:relative;text-transform:none;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-feat form button:hover,.block-feat form button:focus{background-color:<?php print($primary); ?>;border-color:<?php print($primary); ?>;}
.block-feat form button[disabled]{background:rgba(0,0,0,0.2);}
.block-feat form button.success[disabled]{background:#4AAF50;}
.block-feat form button.error[disabled]{background:#F44336;}
.block-feat form button i{right:15px;width:auto;position:absolute;top:15px;font-size:14px;height:14px;line-height:14px;color:<?php print($primary); ?>;-moz-box-sizing:content-box;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-feat form button:hover i{color:<?php print($base); ?>;}

.block-feat form .error-icon{color:#F44336;display:none;position:absolute;right:28px;text-shadow:none;top:15px;}

.block-feat form .callback-con button{border:solid 3px <?php print($primary); ?>;background:<?php print($primary); ?>;border-radius:0;color:<?php print($base); ?>;float:right;font-family:Montserrat;font-size:14px;font-weight:bold;margin-top:0;padding:8px 37px 8px 21px;position:relative;text-transform:none;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-feat form .callback-con button:hover{background:transparent;border-color:<?php print($primary); ?>;color:<?php print($primary); ?>;}
.block-feat form .callback-con button i{right:15px;width:auto;position:absolute;top:12px;font-size:14px;height:14px;line-height:14px;color:<?php print($base); ?>;-moz-box-sizing:content-box;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-feat form .callback-con button:hover i{color:<?php print($primary); ?>;}

.sstcRibbon{
	position: absolute;
	right: 74%;
	width: 28% !important;
	object-fit: contain !important;
	margin-top: -20%;
}


/* FORM
-------------------------------------------------- */
.block-form{background-color:<?php print($primary); ?>;background-image:url(/images/elements/form-background.jpg);background-position:center center;background-repeat:no-repeat;background-size:cover;padding:60px 0;position:relative;text-align:left;}
.block-form.empty{background-color:<?php print($base); ?>;background-image:none;}
.block-form:not(.empty):after{background-color:<?php print($primary); ?>;bottom:0;content:"";left:0;opacity:.85;position:absolute;right:0;top:0;z-index:10;}
.block-form .container{position:relative;z-index:20;}

.block-form h3{color:<?php print($base); ?>;font-size:32px;font-weight:bold;margin:0 0 30px;position:relative;}
.block-form h3:after{content:"";display:block;width:100%;height:6px;background:<?php print($secondary); ?>;border-radius:10px;margin:15px 0 0;}

.block-form p{color:<?php print($base); ?>;margin:0 0 15px;text-align:justify;}

.block-form .dropdown{display:inline-block;}
.block-form .dropdown button{background:none;border:none;border-radius:0;box-shadow:none;color:<?php print($tertiary); ?>;float:none;font-size:inherit;font-weight:inherit;line-height:inherit;margin:0;opacity:1;outline:none !important;padding:0;vertical-align:top;}
.block-form .dropdown button:hover{background-color:transparent;border-color:transparent;}
.block-form .dropdown button:focus,.block-form .dropdown.open button{background-color:transparent !important;}
.block-form .dropdown button i{color:<?php print($tertiary); ?>;font-size:12px;height:18px;line-height:18px;margin-left:8px;position:relative;right:auto;top:0;vertical-align:top;-webkit-transition:all 0.2s;-moz-transition:all 0.2s;-o-transition:all 0.2s;transition:all 0.2s;}
.block-form .dropdown button:hover i{color:inherit;}
.block-form .dropdown.open button i{transform:rotate(180deg);-webkit-filter:rotate(180deg);}

.block-form .dropdown.default{width:100%;}
.block-form .dropdown.default button{background-color:rgba(255, 255, 255, .9) !important;font-size:14px;line-height:18px;font-weight:normal;border:solid 1px rgba(0, 0, 0, 0.3);color:<?php print($secondary); ?>;background:transparent;border-radius:3px;margin-bottom:30px;outline:0;padding:15px;-webkit-transition:all .3s ease;transition:all .3s ease;box-sizing:border-box !important;width:100%;display:block;text-align:left;}
.block-form .dropdown.default button[disabled]{background:inherit;opacity:.4;}
.block-form .dropdown.default button i{color:<?php print($tertiary); ?> !important;float:right;}
.block-form .dropdown.default button.error{background:inherit;border-color:#F44336;color:#F44336;font-size:inherit;line-height:inherit;opacity:1;}
.block-form .dropdown.default button.error:hover{background:rgba(244, 67, 54, 0.05) !important;}
.block-form .dropdown.default .dropdown-menu{margin:-30px 0 0;width:100%;}
.block-form .dropdown.default .dropdown-menu li{font-size:13px;padding:15px 20px;text-align:left;}

.block-form .dropdown .dropdown-menu{background:<?php print($base); ?>;border:none;border-radius:4px;margin:8px 0;padding:0;width:100%;}
.block-form .dropdown .dropdown-menu li{background:rgba(0, 0, 0, 0.02);cursor:pointer;font-size:16px;padding:8px 14px;text-align:left;text-transform:none;}
.block-form .dropdown .dropdown-menu li:hover{background:rgba(0, 0, 0, 0.04);}
.block-form .dropdown .dropdown-menu li.selected{display:none;}

.block-form form{padding:15px 0 0 15px;}
.block-form form .callback-con{position:relative;text-align:center;}

.block-form form input[type="text"],.block-form form input[type="number"],.block-form form input[type="email"],.block-form form textarea{background-color:rgba(255, 255, 255, .9) !important;border:solid 1px rgba(0, 0, 0, 0.3);font-size:14px;line-height:18px;color:#222;background:transparent;border-radius:3px;margin-bottom:30px;outline:0;padding:15px;-webkit-transition:border-color .3s ease;transition:border-color .3s ease;box-sizing:border-box !important;width:100%;display:block;}
.block-form form textarea{height:134px;max-height:234px;min-height:134px;min-width:100%;}
.block-form form input:focus,.block-form form textarea:focus{border-color:<?php print($tertiary); ?>;}
.block-form form input[readonly="readonly"],.block-form form textarea[readonly="readonly"]{opacity:.4;}
.block-form form input[type=number]::-webkit-outer-spin-button,.block-form form input[type=number]::-webkit-inner-spin-button{-webkit-appearance:none;margin:0;}
.block-form form input[type=number]{-moz-appearance:textfield;}
.block-form form input[type="text"].error,.block-form form input[type="text"].error:hover,.block-form form input[type="text"].error:focus,.block-form form input[type="number"].error,.block-form form input[type="number"].error:hover,.block-form form input[type="number"].error:focus,.block-form form input[type="email"].error,.block-form form input[type="email"].error:hover,.block-form form input[type="email"].error:focus{border-color:#F44336;color:#F44336;opacity:1;}
.block-form form button{border:solid 3px <?php print($secondary); ?>;background:<?php print($secondary); ?>;border-radius:0;color:<?php print($base); ?>;font-family:Montserrat;float:right;font-size:14px;font-weight:bold;margin-top:0;padding:11.5px 37px 11.5px 21px;position:relative;text-transform:none;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-form form button:hover,.block-form form button:focus{background-color:<?php print($primary); ?>;border-color:<?php print($primary); ?>;}
.block-form form button[disabled]{background:rgba(0,0,0,0.2);}
.block-form form button.success[disabled]{background-color:#86C241 !important;border-color:#86C241 !important;}
.block-form form button.error[disabled]{background:#F44336;}
.block-form form button i{right:15px;width:auto;position:absolute;top:15px;font-size:14px;height:14px;line-height:14px;color:<?php print($primary); ?>;-moz-box-sizing:content-box;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-form form button:hover i,.block-form form button:focus i{color:<?php print($base); ?>;}

.block-form form .error-icon{color:#F44336;display:none;position:absolute;right:15px;text-shadow:none;top:15px;}

.block-form form .callback-con button{border:solid 3px <?php print($secondary); ?>;background:<?php print($secondary); ?>;border-radius:10px;color:<?php print($base); ?>;float:right;font-family:Montserrat;font-size:14px;font-weight:bold;margin-top:0;padding:11.5px 37px 11.5px 21px;position:relative;text-transform:none;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-form form .callback-con button:hover{background:<?php print($tertiary); ?>;border-color:<?php print($tertiary); ?>;}
.block-form form .callback-con button[disabled]{background-color:#F44336;border-color:#F44336;}
.block-form form .callback-con button i{right:15px;width:auto;position:absolute;top:16px;font-size:12px;height:14px;line-height:14px;color:<?php print($base); ?>;-moz-box-sizing:content-box;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-form form .callback-con button:hover i{color:<?php print($base); ?>;}
.block-form form .callback-con button[disabled] i{color:#FFFFFF;}

.block-form form div.find-button{background-color:<?php print($secondary); ?>;color:<?php print($base); ?>;cursor:pointer;font-family:Montserrat;font-weight:bold;height:49px;line-height:49px;padding:0 37.5px 0 21px;position:absolute;right:15px;top:0;}
.block-form form div.find-button.disabled{background-color:rgba(0,0,0,0.15);}
.block-form form div.find-button i{right:15px;width:auto;position:absolute;top:18px;font-size:14px;height:14px;line-height:14px;color:#FFFFFF;-moz-box-sizing:content-box;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-form form input.find{padding-right:172px;text-transform:uppercase;}
.block-form form input.find::-webkit-input-placeholder{text-transform:none;}
.block-form form input.find:-moz-placeholder{text-transform:none;}
.block-form form input.find::-moz-placeholder{text-transform:none;}
.block-form form input.find:-ms-input-placeholder{text-transform:none;}
.block-form form .error-icon.find{right:188px;}


/* TABLES
-------------------------------------------------- */
table.grid{border-top:solid 1px <?php print($secondary); ?>;border-left:solid 1px <?php print($secondary); ?>;}
table.grid td,table.grid th{border-bottom:solid 1px <?php print($secondary); ?>;border-right:solid 1px <?php print($secondary); ?>;padding:5px;vertical-align:top;}
table.grid th{background-color:<?php print($secondary); ?>;color:<?php print($base); ?>;}


/* DATEPICKER
-------------------------------------------------- */
.datepicker{border:none;}

.datepicker table tr td.today,.datepicker table tr td.today.disabled,.datepicker table tr td.today.disabled:hover,.datepicker table tr td.today:hover{background:<?php print($secondary); ?>;color:<?php print($base); ?>;opacity:.9;}
.datepicker table tr td.today.active,.datepicker table tr td.today.disabled,.datepicker table tr td.today.disabled.active,.datepicker table tr td.today.disabled.disabled,.datepicker table tr td.today.disabled:active,.datepicker table tr td.today.disabled:hover,.datepicker table tr td.today.disabled:hover.active,.datepicker table tr td.today.disabled:hover.disabled,.datepicker table tr td.today.disabled:hover:active,.datepicker table tr td.today.disabled:hover:hover,.datepicker table tr td.today.disabled:hover[disabled],.datepicker table tr td.today.disabled[disabled],.datepicker table tr td.today:active,.datepicker table tr td.today:hover,.datepicker table tr td.today:hover.active,.datepicker table tr td.today:hover.disabled,.datepicker table tr td.today:hover:active,.datepicker table tr td.today:hover:hover,.datepicker table tr td.today:hover[disabled],.datepicker table tr td.today[disabled]{background:<?php print($secondary); ?>;color:<?php print($base); ?>;opacity:1;}

.datepicker table tr td.active,.datepicker table tr td.active.disabled,.datepicker table tr td.active.disabled:hover,.datepicker table tr td.active:hover{background:<?php print($primary); ?>;color:<?php print($base); ?>;opacity:.9;}
.datepicker table tr td.active.active,.datepicker table tr td.active.disabled,.datepicker table tr td.active.disabled.active,.datepicker table tr td.active.disabled.disabled,.datepicker table tr td.active.disabled:active,.datepicker table tr td.active.disabled:hover,.datepicker table tr td.active.disabled:hover.active,.datepicker table tr td.active.disabled:hover.disabled,.datepicker table tr td.active.disabled:hover:active,.datepicker table tr td.active.disabled:hover:hover,.datepicker table tr td.active.disabled:hover[disabled],.datepicker table tr td.active.disabled[disabled],.datepicker table tr td.active:active,.datepicker table tr td.active:hover,.datepicker table tr td.active:hover.active,.datepicker table tr td.active:hover.disabled,.datepicker table tr td.active:hover:active,.datepicker table tr td.active:hover:hover,.datepicker table tr td.active:hover[disabled],.datepicker table tr td.active[disabled]{background:<?php print($primary); ?>;opacity:1;}


/* SERVICES
-------------------------------------------------- */
.block-services{background-color:#F2F2F2;padding:60px 0;position:relative;text-align:left;}
.block-services h3{color:<?php print($tertiary); ?>;font-size:32px;font-weight:bold;margin:0 0 30px;position:relative;}
.block-services h3:after{content:"";display:block;width:35px;height:6px;background:<?php print($primary); ?>;border-radius:10px;margin:15px 0 0;}


/* SPECIFICATION BOXES
-------------------------------------------------- */
.block-spec{}

.block-spec i{background-color:<?php print($primary); ?>;border-radius:50%;color:<?php print($base); ?>;float:left;font-size:20px;height:50px;line-height:50px;margin-right:20px;text-align:center;width:50px;}
.block-spec img{float:left;height:50px;margin-right:20px;width:50px;}

.block-spec .block-info{display:inline-block;width:calc(100% - 70px);}
.block-spec .block-info h5{display:inline-block;font-size:18px;font-weight:bold;margin:0 0 15px;line-height:22px;padding:0;width:100%;}
.block-spec .block-info h5:after{content: '';display:block;width:35px;height:5px;background:<?php print($primary); ?>;border-radius:10px;margin:15px 0 0;}
.block-spec .block-info p{font-size:13px;line-height:24px;text-align:justify;text-shadow:none;}


/* CRITERIA
-------------------------------------------------- */
.block-crit{background:transparent;display:inline-block;height:60px;margin:0 0 15px;padding:0;vertical-align:top;width:100%;}

.block-crit span{display:inline-block;line-height:26px;padding:7px 0 0;}
.block-crit span strong{font-family:Montserrat;}

.block-crit .dropdown{float:right;}
.block-crit .dropdown button{background:<?php print($base); ?> !important;border:1px solid #cacaca !important;border-radius:10px;box-shadow:none;color:#222222 !important;font-family:Montserrat;font-size:14px;height:40px;margin:0;outline:none !important;padding:0 0 0 20px;text-shadow:none;}
.block-crit .dropdown.open button{background:#FFFFFF !important;color:#222222 !important;}
.block-crit .dropdown button span{line-height:normal;padding:0;}
.block-crit .dropdown button i{width:38px;}
.block-crit .dropdown.open button i{transform:rotate(180deg);-webkit-transform:rotate(180deg);}

.block-crit .dropdown .dropdown-menu{border:1px solid #cacaca;border-top:none;border-radius:0;margin:0;overflow-y:overlay;padding:0;right:0;}
.block-crit .dropdown .dropdown-menu li{background:rgba(0, 0, 0, 0.02);cursor:pointer;font-size:13px;text-align:left;}
.block-crit .dropdown .dropdown-menu li:hover{background-color:#6C98E1;color:#FFFFFF;}
.block-crit .dropdown .dropdown-menu li.selected{background-color:#DDDDDD;}
.block-crit .dropdown .dropdown-menu li.selected:hover{color:inherit;}
.block-crit .dropdown .dropdown-menu li a{background-color:transparent !important;color:inherit !important;display:inline-block;padding:8px 45px 8px 29px;white-space:normal;width:100%;}


/* LIST
-------------------------------------------------- */
.block-list{display:inline-block;margin:0 0 45px;text-align:center;width:100%;}
.block-list.coloured{background-color:<?php print($secondary); ?>;border-radius:10px;padding:15px;}

.block-list h5{color:<?php print($tertiary); ?>;display:inline-block;margin:0 0 23px;font-size:20px;font-weight:600;line-height:22px;padding:0;text-align:center;}
.block-list.coloured h5{color:<?php print($base); ?>;padding-top:15px;}
.block-list h5:after{content:"";display:block;width:30px;height:5px;background:<?php print($primary); ?>;border-radius:10px;margin:15px auto 0;width:100%;}

.block-list label{margin:0 5px 0 0;}
.block-list input[type=radio]{background-color:transparent;margin-top:0;}
.block-list select{background-color:<?php print($base); ?>;border:none;border-radius:2px !important;box-shadow:0 0 0 2px rgba(0,0,0,0.15);color:<?php print($primary); ?>;cursor:pointer;font-family:Montserrat;font-size:13px;font-weight:bold;height:auto;outline:none;padding:10px;margin:12px 0 23px;width:100%;transition:all .3s linear;-webkit-transition:all .3s linear;}
.block-list select:focus{box-shadow:0 0 0 2px <?php print($tertiary); ?>;}
.block-list select option{color:<?php print($tertiary); ?>;padding:10px;}
.block-list input[type="text"]{font-size: 13px;line-height:18px;color:#222;background:transparent;border:0;border-top-left-radius:2px !important;border-bottom-left-radius:2px !important;box-shadow:0 0 0 2px rgba(0,0,0,0.15);float:left;margin-bottom:30px;outline:0;padding:16px 20px 15px;border-radius:0;-webkit-transition:all .3s ease;transition:all .3s ease;box-sizing:border-box !important;width:calc(100% - 48px);display:block;height:49px;}
.block-list input.slider{background-color:transparent;border:none;}
.block-list .value.slider{display:inline-block;line-height:30px;}
.block-list .ui-slider{margin:12px 0 23px;}

.block-list .dropdown{display:inline-block;vertical-align:top;width:100%;}
.block-list button{border:solid 3px <?php print($base); ?>;background:<?php print($base); ?>;border:none;border-radius:2px;box-shadow:0 0 0 2px rgba(0,0,0,0.15);color:<?php print($primary); ?>;display:inline-block;font-family:Montserrat;font-size:14px;font-weight:bold;outline:none !important;height:38px;margin:12px 0 23px;padding:10px;position:relative;text-shadow:none;text-transform:none;-webkit-transition:all .3s ease;transition:all .3s ease;width:100%;text-align:left;}
.block-list button:hover{background:<?php print($base); ?>;color:<?php print($primary); ?>;}
.block-list button i{right:15px;width:auto;position:absolute;top:17px;font-size:14px;height:14px;line-height:14px;color:<?php print($primary); ?>;-moz-box-sizing:content-box;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-list button:hover i{color:<?php print($base); ?>;}
.block-list .dropdown-menu{border-radius:0;top:50px;}

.block-list .block-prop{background-color:<?php print($base); ?>;border-bottom:1px solid #ddd;margin:0 0 30px;padding:0;vertical-align:top;width:100%;}
.block-list.coloured .block-prop{margin:0 0 15px;}
.block-list .block-prop:last-child{margin-bottom:0;}
.block-list .row:last-child .block-prop{border-bottom:none;}
.block-list .block-prop:before{display:none;}
.block-list .block-prop .carousel{display:inline-block;height:180px;margin:0;vertical-align:top;width:100%;}
.block-list .block-prop .carousel .item{height:180px;}
.block-list .block-prop .block-info{display:inline-block;padding:20px;vertical-align:top;width:100%;}
.block-list .block-prop .block-info .head{color:<?php print($secondary); ?>;font-size:14px;height:auto;-webkit-line-clamp:2;}
.block-list .block-prop .block-info .head:before{display:none;}
.block-list .block-prop .block-info span.price{font-size:14px;}

.filter-side{background-color:<?php print($secondary); ?>;border-radius:10px;box-shadow:0 5px 15px rgba(0,0,0,.16);padding:30px;}
.filter-side h5{color:<?php print($base); ?>;margin:0 0 15px;}
.filter-side h5:after{background:<?php print($primary); ?>;}
.filter-side label{color:<?php print($base); ?>;}
.filter-side .value.slider{color:<?php print($base); ?>;}
.filter-side .jplist-panel{color:<?php print($base); ?>;text-align:left;}

.squaredThree{width:20px;position:relative;margin:12px 0 23px;}
.squaredThree label{background-color:<?php print($base); ?>;border-radius:2px;width:20px;height:20px;cursor:pointer;position:absolute;top:0;left:0;}
.squaredThree label:after{content:'';width:9px;height:5px;position:absolute;top:6px;left:5px;border:3px solid <?php print($secondary); ?>;border-top:none;border-right:none;background:transparent;opacity:0;-webkit-transform:rotate(-45deg);transform:rotate(-45deg);}
.squaredThree label:hover::after{opacity:0.3;}
.squaredThree input[type=checkbox]{height:auto;visibility:hidden;}
.squaredThree input[type=checkbox]:checked + label:after{opacity:1;}


/* STAFF MEMBERS
-------------------------------------------------- */
.staff-all{margin:0 0 60px 0;display:inline-block;width:100%;}
.staff-all .staff-con{display:inline-block;vertical-align:top;width:16.66666667%;}
.staff-all .staff-con .staff-img{background-position:center center;background-size:cover;border-radius:50%;width:125px;height:125px;}
.staff-all .staff-con .staff-img a{display:block;width:100%;height:100%;border-radius:50%;-webkit-transition:opacity .4s;-moz-transition:opacity .4s;-o-transition:opacity .4s;transition:opacity .4s;background-color:rgba(255, 255, 255, 0.55);opacity:0;padding:15px;text-align:center;position:relative;}
.staff-all .staff-con .staff-img a:hover{opacity:1;}
.staff-all .staff-con .staff-img a i{position:absolute;top:50%;left:50%;width:40px;height:40px;background:rgba(255,255,255,0.25);border:solid 2px <?php print($secondary); ?>;border-radius:100%;color:<?php print($secondary); ?>;line-height:36px;-webkit-transform:translate(-50%, -50%);-moz-transform:translate(-50%, -50%);-ms-transform:translate(-50%, -50%);-o-transform:translate(-50%, -50%);transform:translate(-50%, -50%);}
.staff-all .staff-con .staff-txt h2{background-color:transparent;color: <?php print($secondary); ?>;display:block;font-size: 14px;font-weight:bold;line-height:1.2;margin:15px 0 5px 0;padding:0;text-align:center;width:125px;}
.staff-all .staff-con .staff-txt h3{background-color:transparent;color:<?php print($primary); ?>;display:block;font-size:14px;font-weight:bold;line-height:1.2;margin:0;text-align:center;width:125px;}
.staff-all .staff-con .staff-txt a{background-color:<?php print($tertiary); ?>;border-radius:10px;color:<?php print($base); ?>;display:inline-block;margin:15px 20px 0;padding:5px 10px;vertical-align:middle;}


/* MODAL
-------------------------------------------------- */
.modal-backdrop{background-color:#2C2C2C}
.modal-backdrop.in{opacity:.9}
.modal .modal-dialog{width:100%;height:100vh;margin:0}
.modal .modal-dialog .container{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);-moz-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);-o-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}
.modal .modal-dialog .container h1{color:#FFF;font-size:58px;font-weight:700}
.modal .modal-dialog .container form{margin-top:60px}
.modal .modal-dialog .container input{background-color:transparent;border:1px solid rgba(255,255,255,.25);border-radius:2px;box-shadow:inset 0 2px 1px rgba(0,0,0,.025);color:#FFF;font-size:26px;padding:1em 3em 1em 1.5em;width:100%;-webkit-transition:all .3s;-moz-transition:all .3s;-o-transition:all .3s;transition:all .3s}
.modal .modal-dialog .container input.transparent,.modal .modal-dialog .container input.transparent:active,.modal .modal-dialog .container input.transparent:focus,.modal .modal-dialog .container input.transparent:hover{color:transparent}
.modal .modal-dialog .container input.error-color,.modal .modal-dialog .container input.error-color:active,.modal .modal-dialog .container input.error-color:focus,.modal .modal-dialog .container input.error-color:hover{border-color:#F44336;color:#F44336}
.modal .modal-dialog .container input.success-color,.modal .modal-dialog .container input.success-color:active,.modal .modal-dialog .container input.success-color:focus,.modal .modal-dialog .container input.success-color:hover{border-color:#6EAA1A;color:#6EAA1A}
.modal .modal-dialog .container input:active,.modal .modal-dialog .container input:focus,.modal .modal-dialog .container input:hover{border-color:rgba(255,255,255,.5)}
.modal .modal-dialog .container input::-webkit-input-placeholder{color:rgba(255,255,255,.75);-webkit-transition:color .3s;-moz-transition:color .3s;-o-transition:color .3s;transition:color .3s}
.modal .modal-dialog .container input:-moz-placeholder{color:rgba(255,255,255,.75);transition:color .3s}
.modal .modal-dialog .container input::-moz-placeholder{color:rgba(255,255,255,.75);transition:color .3s}
.modal .modal-dialog .container input:-ms-input-placeholder{color:rgba(255,255,255,.75);transition:color .3s}
.modal .modal-dialog .container input.transparent::-webkit-input-placeholder{color:transparent!important}
.modal .modal-dialog .container input.transparent:-moz-placeholder{color:transparent!important}
.modal .modal-dialog .container input.transparent::-moz-placeholder{color:transparent!important}
.modal .modal-dialog .container input.transparent:-ms-input-placeholder{color:transparent!important}
.modal .modal-dialog .container input.error-color::-webkit-input-placeholder{color:#F44336!important}
.modal .modal-dialog .container input.error-color:-moz-placeholder{color:#F44336!important}
.modal .modal-dialog .container input.error-color::-moz-placeholder{color:#F44336!important}
.modal .modal-dialog .container input.error-color:-ms-input-placeholder{color:#F44336!important}
.modal .modal-dialog .container input.success-color-color::-webkit-input-placeholder{color:#6EAA1A!important}
.modal .modal-dialog .container input.success-color:-moz-placeholder{color:#6EAA1A!important}
.modal .modal-dialog .container input.success-color::-moz-placeholder{color:#6EAA1A!important}
.modal .modal-dialog .container input.success-color:-ms-input-placeholder{color:#6EAA1A!important}
.modal .modal-dialog .container #signup-error,.modal .modal-dialog .container #signup-success,.modal .modal-dialog .container button{position:absolute;top:15px;right:30px;bottom:15px;background-color:transparent;border:none;color:#FFF;font-size:20px;line-height:58px;width:60px;height:60px;border-radius:0;box-shadow:none;padding:0;text-align:center;transition:none}
.modal .modal-dialog .container #signup-error{display:none;color:#F44336}
.modal .modal-dialog .container #signup-success{display:none;color:#6EAA1A}

.modal .modal-dialog .container #staff-name{color:#FFFFFF;font-size:58px;font-weight:normal;margin:0 0 15px;text-transform:none;}
.modal .modal-dialog .container #staff-title{color:<?php print($primary);?>;font-size:26px;font-weight:700;margin:0 0 30px}
.modal .modal-dialog .container #staff-qual{background-color:rgba(0,0,0,.25);border-radius:40px;color:#E5E5E5;float:right;font-size:16px;font-weight:700;line-height:30px;margin:0 0 30px;overflow:hidden;padding:0 15px 0 35px;position:relative}
.modal .modal-dialog .container #staff-qual:before{content:"\f0a3";font-family:FontAwesome;position:absolute;left:5px;width:30px;height:30px;text-align:center}
.modal .modal-dialog .container #staff-img{background-position:center center;background-size:cover;border-radius:50%;width:200px;height:200px}
.modal .modal-dialog .container #staff-bio{color:#FFF;overflow:auto;width:100%;max-height:350px}
.modal .modal-dialog .container #staff-bio::-webkit-scrollbar{display:none}
.modal .modal-dialog .container #staff-bio p{color:#FFF;line-height:1.6}
.modal .modal-dialog .container #staff-bio p:last-child{margin:0}

button.close{position:absolute;top:20px;right:20px;color:#FFF;font-size:46px;font-weight:400;opacity:.75;text-shadow:none;z-index:10}
button.close:active,button.close:focus,button.close:hover{color:#FFF;opacity:1}.scroll{position:absolute;right:-5%;bottom:30%}

.doi,.trei,.unu{-webkit-animation-direction:alternate}
.doi,.trei,.unu{display:block;width:5px;height:5px;-ms-transform:rotate(45deg);-webkit-transform:rotate(45deg);transform:rotate(45deg);border-right:2px solid #fff;border-bottom:2px solid #fff;margin:0 auto 3px;-webkit-animation:mouse-scroll 1s infinite;-moz-animation:mouse-scroll 1s infinite}
.unu{margin-top:6px;-webkit-animation-delay:.1s;-moz-animation-delay:.1s}
.doi{-webkit-animation-delay:.2s;-moz-animation-delay:.2s}
.trei{-webkit-animation-delay:.3s;-moz-animation-delay:.3s}

.mouse{height:21px;width:14px;border-radius:10px;transform:none;border:2px solid #fff;top:170px}
.wheel{height:5px;width:2px;display:block;margin:5px auto;background:#fff;position:relative;-webkit-animation:mouse-wheel 1.2s ease infinite;-moz-animation:mouse-wheel 1.2s ease infinite}

#myModal{overflow:hidden;}

@-webkit-keyframes mouse-wheel{
	0%{opacity:1;-webkit-transform:translateY(0);-ms-transform:translateY(0);transform:translateY(0)}
	100%{opacity:0;-webkit-transform:translateY(6px);-ms-transform:translateY(6px);transform:translateY(6px)}
}

@-moz-keyframes mouse-wheel{
	0%{top:1px}
	50%{top:2px}100%{top:3px}
}

@-webkit-keyframes mouse-scroll{
	0%{opacity:0}
	50%{opacity:.5}
	100%{opacity:1}
}

@-moz-keyframes mouse-scroll{
	0%{opacity:0}50%{opacity:.5}
	100%{opacity:1}
}

@-o-keyframes mouse-scroll{
	0%{opacity:0}50%{opacity:.5}
	100%{opacity:1}
}

@keyframes mouse-scroll{
	0%{opacity:0}
	50%{opacity:.5}
	100%{opacity:1}
}

@media only screen and (max-width : 992px) {
	.staff-all .staff-con{width:20%;}
}

@media only screen and (max-width : 767px) {
	.staff-all .staff-con{width:25%;}
}

@media only screen and (max-width : 640px) {
	.staff-all .staff-con{width:33%;}
        #myModal{overflow-y:auto}
}

@media only screen and (max-width : 480px) {
	.staff-all .staff-con{width:50%;}
        #myModal{overflow-y:auto;}
}

@media only screen and (max-width : 380px) {
  .sstcMobile{
	left: 61% !important;
	}
}



/* ADVERTS
-------------------------------------------------- */
/*.block-adverts{background-color:#DBDBDB;background-image:url(/images/elements/tree.png);background-position:top 60px right;background-repeat:no-repeat;padding:60px 0;position:relative;text-align:center;}*/
.block-adverts{background-color:#DBDBDB;padding:60px 0;position:relative;text-align:center;}
/*.block-types ~ .block-adverts{padding-top:116px;}*/
.block-adverts h3{display:inline-block;font-size:32px;font-weight:bold;margin:0 0 30px;position:relative;vertical-align:top;}
.block-adverts h3:after{content:"";display:block;width:35px;height:6px;background:<?php print($primary); ?>;border-radius:10px;margin:15px auto 0;width:100%;}

.block-advert{border-radius:10px;display:inline-block;padding:25px;position:relative;text-align:left;-webkit-transition:all .3s ease;transition:all .3s ease;vertical-align:top;width:100%;}
.block-advert:hover{background-color:<?php print($base); ?>;}

.block-advert .artwork{float:left;position:relative;-webkit-transition:all .3s ease;transition:all .3s ease;width:56px;}
.block-advert:hover .artwork{padding:20px 0;}
.block-advert .artwork:before{content:"";display:block;position:absolute;left:25px;top:-3px;width:6px;background:<?php print($primary); ?>;height:0;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-advert:hover .artwork:before{height:15px;}
.block-advert .artwork:after{content:"";display:block;position:absolute;left:25px;bottom:-3px;width:6px;background:<?php print($primary); ?>;height:0;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-advert:hover .artwork:after{height:15px;}

.block-advert .text{float:right;padding-left:15px;width:calc(100% - 56px);}
.block-advert .text h5{font-weight:bold;line-height:24px;margin:0 0 15px;}
.block-advert .text p{margin:0;}

.block-adverts a{display:inline-block;vertical-align:top;color:<?php print($base); ?>;border:solid 3px <?php print($primary); ?>;background:<?php print($primary); ?>;border-radius:10px;font-size:14px;font-weight:bold;margin-left:55px;padding:12px 48px 12px 24px;position:relative;text-transform:none;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-adverts a:hover{background-color:<?php print($secondary); ?>;border-color:<?php print($secondary); ?>;}
.block-adverts a i{display:inline-block;margin-bottom:8px;padding:12px 16px 12px 0;right: 15px;width:auto;position:absolute;top:12px;font-size:14px;height:14px;line-height:14px;right:0;top:3px;color:<?php print($base); ?>;-moz-box-sizing:content-box;-webkit-transition:all .3s ease;transition:all .3s ease;}


/* MAP
-------------------------------------------------- */
#map p{color:#FFFFFF;text-align:center;}

#office-map{border:0;margin-top:-100px;width:100%;}

.block-map{}
.block-map #map{height:450px;}

.block-advice{background-color:#EBEBEB;background-image:url(http://consulting.stylemixthemes.com/wp-content/themes/consulting/assets/images/bg/img_4.png);padding:90px 0;position:relative;}
.block-advice h3{font-size:36px;font-weight:bold;margin:0 0 30px;}
.block-advice p{line-height:22px;margin:0 0 15px;}
.block-advice .options{display:inline-block;margin-top:15px;vertical-align:top;}
.block-advice .options a{color:<?php print($base); ?>;border:solid 3px <?php print($primary); ?>;background:<?php print($primary); ?>;border-radius:10px;float:left;margin-right:15px;font-weight:bold;padding:8px 37px 8px 21px;position:relative;text-shadow:none;text-transform:none;-webkit-transition:all .3s ease;transition:all .3s ease;}
.block-advice .options a:hover{background-color:<?php print($secondary); ?>;border-color:<?php print($secondary); ?>;}
.block-advice .options a i{display:inline-block;margin-bottom:8px;padding:8px 16px 9px 0;right:15px;width:auto;position:absolute;top:12px;font-size:14px;height:14px;line-height:14px;right:0;top:3px;color:<?php print($base); ?>;-moz-box-sizing:content-box;-webkit-transition:all .3s ease;transition:all .3s ease;}

.block-advice #map{border-radius:10px;box-shadow:0 5px 15px rgba(0,0,0,.16);height:256px;overflow:hidden;position:relative;}


/* TABS
-------------------------------------------------- */
.nav-tabs{background-color:<?php print($secondary); ?>;border:none;}
.nav-tabs>li{margin-bottom:0;}
.nav-tabs>li>a{border:none;border-radius:0;color:<?php print($base); ?>;font-family:Montserrat;font-weight:700;letter-spacing:-.04px;margin-right:0;padding:15px 30px;position:relative;text-align:center;}
.nav-tabs>li>a,.nav-tabs>li>a:focus,.nav-tabs>li>a:hover{background-color:transparent;}
.nav-tabs>li.active>a,.nav-tabs>li.active>a:focus,.nav-tabs>li.active>a:hover{background-color:<?php print($primary); ?>;border:none;color:<?php print($base); ?>;}

.tab-content{padding-top:30px;}


/* PAGINATION 
-------------------------------------------------- */
.pagination{border-radius:0;border-top:solid 1px rgba(0, 0, 0, 0.1);margin:0;padding:30px 0 40px;width:100%}
.pagination:first-child{border-bottom:solid 1px rgba(0, 0, 0, 0.1);border-top:none;margin:0 0 60px;padding:0 0 30px;}
.pagination>li{display:inline-block;min-width:40px;height:40px;text-align:center;border:2px solid transparent;padding:0;font-size:16px;line-height:40px;margin:0 11px 0 0;font-weight:600;font-family:Montserrat;color:<?php print($secondary); ?>;}
/*.pagination>li:hover{border:solid 2px <?php print($secondary); ?>;} */
/*.pagination>li.active{border:solid 2px <?php print($secondary); ?>;} */
.pagination>li>a,.pagination>li>span{background-color:transparent !important;border:none;color:inherit !important;display:inline-block;float:none;height:100%;line-height:inherit;margin:0;padding:0;}
.pagination>li>a:hover,.pagination>li>span:hover{background-color:transparent;color:inherit;}


/* PROPERTIES
-------------------------------------------------- */
/*.block-props{background-color:<?php print($base); ?>;background-image:url(/images/elements/tree.png);background-position:top 60px right;background-repeat:no-repeat;padding:45px 0;position:relative;}*/
.block-props{background-color:<?php print($base); ?>;background-repeat:no-repeat;padding:45px 0;position:relative;}

.block-prop{border-radius:10px;box-shadow:0 5px 15px rgba(0,0,0,.16);display:inline-block;margin-bottom:30px;margin-right:30px;overflow:hidden;position:relative;vertical-align:top;width:calc(33.33333333% - 20px);}
.block-prop:nth-child(3n){margin-right:0;}

.block-prop .status{background-color:rgba(0, 0, 0, 0.5);bottom:0;color:<?php print($base); ?>;display:inline-block;font-size:13px;font-weight:bold;left:0;padding:5px 20px;position:absolute;width:100%;z-index:20;}

.block-prop .block-cover{background:rgba(0, 0, 0, 0.6);display:table;height:198px;opacity:0;position:absolute;text-align:center;transition:all .3s;vertical-align:middle;width:100%;z-index:10;}
.block-prop .block-cover:hover{opacity:1;}
.block-prop .block-cover span{color:<?php print($base); ?>;display:inline-block;font-size:72px;margin:48px 0;opacity:0;transform:translateY(20px);transition:all .3s;}
.block-prop .block-cover:hover span{opacity:1;transform:translateY(0);}

.block-prop .carousel{height:198px;}
.block-prop .carousel .item{height:198px;}
.block-prop .carousel .item a{display:inline-block;height:100%;width:100%;}
.block-prop .carousel .carousel-control{background-color:<?php print($tertiary); ?>;border-color:transparent;border-radius:10px;bottom:-60px;height:30px;opacity:.6;top:50%;transform:translateY(-50%);-webkit-transform:translateY(-50%);width:30px;z-index:20;}
.block-prop .carousel .carousel-control:hover{bopacity:.8;}
.block-prop .carousel-control.left{border-top-left-radius:0;border-bottom-left-radius:0;left:0;}
.block-prop .carousel-control.right{border-top-right-radius:0;border-bottom-right-radius:0;right:0;}
.block-prop .carousel .carousel-control span{color:<?php print($base); ?>;font-size:12px;line-height:30px;}
.block-prop .carousel .carousel-control:hover span{color:<?php print($base); ?>;}

.block-prop .block-over{background-color:rgba(34,34,34,0.85); ?>;height:198px;opacity:0;left:0;padding-top:75px;position:absolute;text-align:center;top:0;transition:opacity .3s;-webkit-transition:opacity .3s;width:100%;}
.block-prop .block-over:hover{opacity:1;}
.block-prop .block-over a{box-shadow:0 0 0 2px rgba(255,255,255,0.85);color:#FFFFFF;display:inline-block;height:36px;line-height:36px;padding:0 8px;vertical-align:middle;transition:all .3s;-webkit-transition:all .3s;}
.block-prop .block-over a:hover{background-color:#FFFFFF;box-shadow:0 0 0 2px #FFFFFF;color:<?php print($tertiary); ?>;}
.block-prop .block-over a:last-child{box-shadow:none;color:<?php print($primary); ?>;font-size:13px;height:auto;line-height:normal;margin-top:8px;text-decoration:underline;text-underline-position:under;}
.block-prop .block-over a:last-child:hover{background-color:transparent;text-decoration:none;}

.block-prop .price{background-color:<?php print($primary); ?>;background-image:url(https://www.indlu.co.uk/images/property_arrow.png);background-size:20px;background-position:center right 20px;font-size:0;margin:0;text-align:left;background-repeat:no-repeat;display:inline-block;padding:10px 20px;vertical-align:top;width:100%;transition:all .3s;-webkit-transition:all .3s;}
.block-prop .price:hover{background-color:#7bb23c;}
.block-prop.featured .price{background-color:<?php print($primary); ?>;}
.block-prop .price span{color:<?php print($base); ?>;display:inline-block;width:100%;}
.block-prop .price span:first-child{font-size:18px;font-weight:bold;}

.block-prop .block-info{padding:20px;text-align:left;}
.block-prop .block-info .top{color:<?php print($primary); ?>;display:inline-block;font-size:16px;font-weight:bold;height:44px;margin-bottom:15px;width:100%;}
.block-prop.featured .block-info .top{color:<?php print($primary); ?>;}
.block-prop .block-info .head{color:<?php print($secondary); ?>;display:inline-block;font-weight:bold;height:90px;margin-bottom:0;width:100%;}
.block-prop .block-info .head:hover{text-decoration:underline;}
.block-prop .block-info .head span{width:100%;display:inline-block;line-height:18px;}
.block-prop .block-info .head span.postCode{display:inline-block;width:100%;}
.block-prop .block-info span.feat{display:inline-block;font-size:13px;margin-top:-2px;opacity:.7;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:100%;}
.block-prop .block-info span.price{color:<?php print($primary); ?>;float:right;font-size:15px;margin-top:-23px;opacity:.7;width:auto;}
.block-prop .block-info span.price span{display:inline-block;vertical-align:top;width:auto;}

.block-prop .block-button{padding:0 20px 20px;}
.block-prop .block-button a{background-color:<?php print($primary); ?>;border-radius:10px;color:<?php print($base); ?> !important;display:inline-block;font-weight:bold;margin:-3px 0;padding:8px 18px;text-align:center;transition:background-color .3s;-webkit-transition:background-color .3s;vertical-align:top;width:100%;}
.block-prop.featured .block-button a{background-color:<?php print($primary); ?>;}
.block-prop .block-button a:hover{background-color:<?php print($secondary); ?>;}


.container {
 margin:0 auto;
 max-width:1280px;
 width:90%
}
@media only screen and (min-width: 601px) {
 .container {
  width:85%
 }
}
@media only screen and (min-width: 993px) {
 .container {
  width:70%
 }
}
.container .row {
 margin-left:-.75rem;
 margin-right:-.75rem
}
.section {
 padding-top:1rem;
 padding-bottom:1rem
}
.section.no-pad {
 padding:0
}
.section.no-pad-bot {
 padding-bottom:0
}
.section.no-pad-top {
 padding-top:0
}
.row {
 margin-left:auto;
 margin-right:auto;
 margin-bottom:20px
}
.row:after {
 content:"";
 display:table;
 clear:both
}
.row .col {
 float:left;
 box-sizing:border-box;
 padding:0 .75rem;
 min-height:1px
}
.row .col[class*="push-"],
.row .col[class*="pull-"] {
 position:relative
}
.row .col.s1 {
 width:8.3333333333%;
 margin-left:auto;
 left:auto;
 right:auto
}
.row .col.s2 {
 width:16.6666666667%;
 margin-left:auto;
 left:auto;
 right:auto
}
.row .col.s3 {
 width:25%;
 margin-left:auto;
 left:auto;
 right:auto
}
.row .col.s4 {
 width:33.3333333333%;
 margin-left:auto;
 left:auto;
 right:auto
}
.row .col.s5 {
 width:41.6666666667%;
 margin-left:auto;
 left:auto;
 right:auto
}
.row .col.s6 {
 width:50%;
 margin-left:auto;
 left:auto;
 right:auto
}
.row .col.s7 {
 width:58.3333333333%;
 margin-left:auto;
 left:auto;
 right:auto
}
.row .col.s8 {
 width:66.6666666667%;
 margin-left:auto;
 left:auto;
 right:auto
}
.row .col.s9 {
 width:75%;
 margin-left:auto;
 left:auto;
 right:auto
}
.row .col.s10 {
 width:83.3333333333%;
 margin-left:auto;
 left:auto;
 right:auto
}
.row .col.s11 {
 width:91.6666666667%;
 margin-left:auto;
 left:auto;
 right:auto
}
.row .col.s12 {
 width:100%;
 margin-left:auto;
 left:auto;
 right:auto
}
.row .col.offset-s1 {
 margin-left:8.3333333333%
}
.row .col.pull-s1 {
 right:8.3333333333%
}
.row .col.push-s1 {
 left:8.3333333333%
}
.row .col.offset-s2 {
 margin-left:16.6666666667%
}
.row .col.pull-s2 {
 right:16.6666666667%
}
.row .col.push-s2 {
 left:16.6666666667%
}
.row .col.offset-s3 {
 margin-left:25%
}
.row .col.pull-s3 {
 right:25%
}
.row .col.push-s3 {
 left:25%
}
.row .col.offset-s4 {
 margin-left:33.3333333333%
}
.row .col.pull-s4 {
 right:33.3333333333%
}
.row .col.push-s4 {
 left:33.3333333333%
}
.row .col.offset-s5 {
 margin-left:41.6666666667%
}
.row .col.pull-s5 {
 right:41.6666666667%
}
.row .col.push-s5 {
 left:41.6666666667%
}
.row .col.offset-s6 {
 margin-left:50%
}
.row .col.pull-s6 {
 right:50%
}
.row .col.push-s6 {
 left:50%
}
.row .col.offset-s7 {
 margin-left:58.3333333333%
}
.row .col.pull-s7 {
 right:58.3333333333%
}
.row .col.push-s7 {
 left:58.3333333333%
}
.row .col.offset-s8 {
 margin-left:66.6666666667%
}
.row .col.pull-s8 {
 right:66.6666666667%
}
.row .col.push-s8 {
 left:66.6666666667%
}
.row .col.offset-s9 {
 margin-left:75%
}
.row .col.pull-s9 {
 right:75%
}
.row .col.push-s9 {
 left:75%
}
.row .col.offset-s10 {
 margin-left:83.3333333333%
}
.row .col.pull-s10 {
 right:83.3333333333%
}
.row .col.push-s10 {
 left:83.3333333333%
}
.row .col.offset-s11 {
 margin-left:91.6666666667%
}
.row .col.pull-s11 {
 right:91.6666666667%
}
.row .col.push-s11 {
 left:91.6666666667%
}
.row .col.offset-s12 {
 margin-left:100%
}
.row .col.pull-s12 {
 right:100%
}
.row .col.push-s12 {
 left:100%
}
@media only screen and (min-width: 601px) {
 .row .col.m1 {
  width:8.3333333333%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.m2 {
  width:16.6666666667%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.m3 {
  width:25%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.m4 {
  width:33.3333333333%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.m5 {
  width:41.6666666667%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.m6 {
  width:50%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.m7 {
  width:58.3333333333%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.m8 {
  width:66.6666666667%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.m9 {
  width:75%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.m10 {
  width:83.3333333333%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.m11 {
  width:91.6666666667%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.m12 {
  width:100%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.offset-m1 {
  margin-left:8.3333333333%
 }
 .row .col.pull-m1 {
  right:8.3333333333%
 }
 .row .col.push-m1 {
  left:8.3333333333%
 }
 .row .col.offset-m2 {
  margin-left:16.6666666667%
 }
 .row .col.pull-m2 {
  right:16.6666666667%
 }
 .row .col.push-m2 {
  left:16.6666666667%
 }
 .row .col.offset-m3 {
  margin-left:25%
 }
 .row .col.pull-m3 {
  right:25%
 }
 .row .col.push-m3 {
  left:25%
 }
 .row .col.offset-m4 {
  margin-left:33.3333333333%
 }
 .row .col.pull-m4 {
  right:33.3333333333%
 }
 .row .col.push-m4 {
  left:33.3333333333%
 }
 .row .col.offset-m5 {
  margin-left:41.6666666667%
 }
 .row .col.pull-m5 {
  right:41.6666666667%
 }
 .row .col.push-m5 {
  left:41.6666666667%
 }
 .row .col.offset-m6 {
  margin-left:50%
 }
 .row .col.pull-m6 {
  right:50%
 }
 .row .col.push-m6 {
  left:50%
 }
 .row .col.offset-m7 {
  margin-left:58.3333333333%
 }
 .row .col.pull-m7 {
  right:58.3333333333%
 }
 .row .col.push-m7 {
  left:58.3333333333%
 }
 .row .col.offset-m8 {
  margin-left:66.6666666667%
 }
 .row .col.pull-m8 {
  right:66.6666666667%
 }
 .row .col.push-m8 {
  left:66.6666666667%
 }
 .row .col.offset-m9 {
  margin-left:75%
 }
 .row .col.pull-m9 {
  right:75%
 }
 .row .col.push-m9 {
  left:75%
 }
 .row .col.offset-m10 {
  margin-left:83.3333333333%
 }
 .row .col.pull-m10 {
  right:83.3333333333%
 }
 .row .col.push-m10 {
  left:83.3333333333%
 }
 .row .col.offset-m11 {
  margin-left:91.6666666667%
 }
 .row .col.pull-m11 {
  right:91.6666666667%
 }
 .row .col.push-m11 {
  left:91.6666666667%
 }
 .row .col.offset-m12 {
  margin-left:100%
 }
 .row .col.pull-m12 {
  right:100%
 }
 .row .col.push-m12 {
  left:100%
 }
}
@media only screen and (min-width: 993px) {
 .row .col.l1 {
  width:8.3333333333%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.l2 {
  width:16.6666666667%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.l3 {
  width:25%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.l4 {
  width:33.3333333333%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.l5 {
  width:41.6666666667%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.l6 {
  width:50%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.l7 {
  width:58.3333333333%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.l8 {
  width:66.6666666667%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.l9 {
  width:75%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.l10 {
  width:83.3333333333%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.l11 {
  width:91.6666666667%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.l12 {
  width:100%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.offset-l1 {
  margin-left:8.3333333333%
 }
 .row .col.pull-l1 {
  right:8.3333333333%
 }
 .row .col.push-l1 {
  left:8.3333333333%
 }
 .row .col.offset-l2 {
  margin-left:16.6666666667%
 }
 .row .col.pull-l2 {
  right:16.6666666667%
 }
 .row .col.push-l2 {
  left:16.6666666667%
 }
 .row .col.offset-l3 {
  margin-left:25%
 }
 .row .col.pull-l3 {
  right:25%
 }
 .row .col.push-l3 {
  left:25%
 }
 .row .col.offset-l4 {
  margin-left:33.3333333333%
 }
 .row .col.pull-l4 {
  right:33.3333333333%
 }
 .row .col.push-l4 {
  left:33.3333333333%
 }
 .row .col.offset-l5 {
  margin-left:41.6666666667%
 }
 .row .col.pull-l5 {
  right:41.6666666667%
 }
 .row .col.push-l5 {
  left:41.6666666667%
 }
 .row .col.offset-l6 {
  margin-left:50%
 }
 .row .col.pull-l6 {
  right:50%
 }
 .row .col.push-l6 {
  left:50%
 }
 .row .col.offset-l7 {
  margin-left:58.3333333333%
 }
 .row .col.pull-l7 {
  right:58.3333333333%
 }
 .row .col.push-l7 {
  left:58.3333333333%
 }
 .row .col.offset-l8 {
  margin-left:66.6666666667%
 }
 .row .col.pull-l8 {
  right:66.6666666667%
 }
 .row .col.push-l8 {
  left:66.6666666667%
 }
 .row .col.offset-l9 {
  margin-left:75%
 }
 .row .col.pull-l9 {
  right:75%
 }
 .row .col.push-l9 {
  left:75%
 }
 .row .col.offset-l10 {
  margin-left:83.3333333333%
 }
 .row .col.pull-l10 {
  right:83.3333333333%
 }
 .row .col.push-l10 {
  left:83.3333333333%
 }
 .row .col.offset-l11 {
  margin-left:91.6666666667%
 }
 .row .col.pull-l11 {
  right:91.6666666667%
 }
 .row .col.push-l11 {
  left:91.6666666667%
 }
 .row .col.offset-l12 {
  margin-left:100%
 }
 .row .col.pull-l12 {
  right:100%
 }
 .row .col.push-l12 {
  left:100%
 }
}
@media only screen and (min-width: 1201px) {
 .row .col.xl1 {
  width:8.3333333333%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.xl2 {
  width:16.6666666667%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.xl3 {
  width:25%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.xl4 {
  width:33.3333333333%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.xl5 {
  width:41.6666666667%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.xl6 {
  width:50%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.xl7 {
  width:58.3333333333%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.xl8 {
  width:66.6666666667%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.xl9 {
  width:75%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.xl10 {
  width:83.3333333333%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.xl11 {
  width:91.6666666667%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.xl12 {
  width:100%;
  margin-left:auto;
  left:auto;
  right:auto
 }
 .row .col.offset-xl1 {
  margin-left:8.3333333333%
 }
 .row .col.pull-xl1 {
  right:8.3333333333%
 }
 .row .col.push-xl1 {
  left:8.3333333333%
 }
 .row .col.offset-xl2 {
  margin-left:16.6666666667%
 }
 .row .col.pull-xl2 {
  right:16.6666666667%
 }
 .row .col.push-xl2 {
  left:16.6666666667%
 }
 .row .col.offset-xl3 {
  margin-left:25%
 }
 .row .col.pull-xl3 {
  right:25%
 }
 .row .col.push-xl3 {
  left:25%
 }
 .row .col.offset-xl4 {
  margin-left:33.3333333333%
 }
 .row .col.pull-xl4 {
  right:33.3333333333%
 }
 .row .col.push-xl4 {
  left:33.3333333333%
 }
 .row .col.offset-xl5 {
  margin-left:41.6666666667%
 }
 .row .col.pull-xl5 {
  right:41.6666666667%
 }
 .row .col.push-xl5 {
  left:41.6666666667%
 }
 .row .col.offset-xl6 {
  margin-left:50%
 }
 .row .col.pull-xl6 {
  right:50%
 }
 .row .col.push-xl6 {
  left:50%
 }
 .row .col.offset-xl7 {
  margin-left:58.3333333333%
 }
 .row .col.pull-xl7 {
  right:58.3333333333%
 }
 .row .col.push-xl7 {
  left:58.3333333333%
 }
 .row .col.offset-xl8 {
  margin-left:66.6666666667%
 }
 .row .col.pull-xl8 {
  right:66.6666666667%
 }
 .row .col.push-xl8 {
  left:66.6666666667%
 }
 .row .col.offset-xl9 {
  margin-left:75%
 }
 .row .col.pull-xl9 {
  right:75%
 }
 .row .col.push-xl9 {
  left:75%
 }
 .row .col.offset-xl10 {
  margin-left:83.3333333333%
 }
 .row .col.pull-xl10 {
  right:83.3333333333%
 }
 .row .col.push-xl10 {
  left:83.3333333333%
 }
 .row .col.offset-xl11 {
  margin-left:91.6666666667%
 }
 .row .col.pull-xl11 {
  right:91.6666666667%
 }
 .row .col.push-xl11 {
  left:91.6666666667%
 }
 .row .col.offset-xl12 {
  margin-left:100%
 }
 .row .col.pull-xl12 {
  right:100%
 }
 .row .col.push-xl12 {
  left:100%
 }
}

.collapsible {
 border:none;
 box-shadow:none;
 margin:0;
}
.collapsible li {
 background-color:#f3f3f4;
 border-bottom:solid 1px #3138491a;
 border-top:solid 1px #ffffff;
}
.collapsible-header {
 background-color:transparent;
 border:none;
 color:#313849;
 font-size:17px;
 font-weight:bold;
 height:52px;
 line-height:52px;
 min-height:auto;
 padding:0 27px;
 position:relative;
}
.collapsible-header i {
 color:#d32f2f;
 font-size:24px;
 line-height:30px;
 margin:0;
 position:absolute;
 right:17px;
 top:11px;
 transform:rotate(90deg);
 -webkit-transform:rotate(90deg);
 transition:transform .3s;
 -webkit-transition:transform .3s;
}
.collapsible-header.active i {
 transform:rotate(0deg);
 -webkit-transform:rotate(0deg);
}
.collapsible-body {
 border:none;
 padding:0 27px 15px !important;
}
.collapsible-body .map {
 border-radius:4px;
 height:300px;
 margin-bottom:30px;
 overflow:hidden;
 width:100%;
}
.collapsible-body .map iframe {
 border:none;
 height:calc(100% * 2);
 margin-top:-20%;
 width:100%;
}
.collapsible ul {
 margin:0 0 15px;
}
.collapsible ul li {
 border:none;
}

.quicksearch {
 background-color:#313849e6;
 display:inline-block;
 max-width:360px;
 padding:35px 30px;
 position:relative;
 text-align:center;
 width:90%;
}
.quicksearch:before {
 background-color:#d32f2fe6;
 content:"";
 height:5px;
 left:0;
 position:absolute;
 right:0;
 top:0;
}
.quicksearch .btn {
 bottom:-20px;
 left:50%;
 position:absolute;
 transform:translateX(-50%);
 -webkit-transform:translateX(-50%);
 white-space:nowrap;
}
.business,
.solicitor {
 background-color:#f3f3f4;
 border-left:solid 5px #039BE5;
 border-bottom:solid 1px #3138491a;
 border-radius:2px;
 margin:15px 0;
 padding:15px 15px 15px 30px;
}
.business span.title,
.solicitor span.title {
 display:inline-block;
 font-weight:bold;
 width:100%;
}
.business ul,
.solicitor ul {
 display:inline-block;
}
.business li,
.solicitor li {
 float:left;
 margin-right:15px;
}
.business addres,
.solicitor address {
 display:inline-block;
 vertical-align:top;
 width:100%;
}
.business a,
.solicitor a {
 display:inline-block;
 overflow:hidden;
 text-overflow:ellipsis;
 vertical-align:top;
 white-space:nowrap;
}
.business i,
.solicitor i {
 margin:4px 8px 0 0;
}
.specification {
 border-radius:2px;
 margin:0 0 24px;
 overflow:hidden;
}
.specification li {
 background-color:#f3f3f4;
 height:50px;
 line-height:50px;
 margin:0 0 5px;
 padding:0 15px;
}
.specification li:first-child {
 background-color:#FFD54F;
 color:#000000;
}
.specification li:last-child {
 margin-bottom:0;
}
.specification i {
 height:50px;
 line-height:50px;
 transform:rotate(-90deg);
 -webkit-transform:rotate(-90deg);
}
.specification span {
 font-size:16px;
}
.contact {
 background-color:transparent;
 border-top:solid 1px #f3f3f4;
 padding:30px 0;
}
.input-field.col input {
 background-color:#ffffff;
 border-bottom:none;
 border-radius:2px;
 box-shadow:0 1px 2px -1px rgba(0,0,0,0.1),0 1px 0px 0 rgba(0,0,0,0.1),0 1px 0px -2px rgba(0,0,0,0.1);
 box-sizing:border-box;
 padding:0 15px;
}
.input-field.col label {
 color:#313849;
 font-weight:bold;
 left:0;
 position:relative;
 top:0;
}
form {
 padding:15px 0;
}
.input-field {
 margin:0;
 text-align:left;
}
.input-field label,
.input-field label:not(.label-icon).active {
 color:#313849;
 display:inline-block;
 font-size:14px;
 font-weight:bold;
 height:30px;
 line-height:30px;
 margin-left:0 !important;
 padding-left:0;
 position:relative;
 top:0;
 transform:none;
 -webkit-transform:none;
 width:100%;
}
.input-field label i {
 font-size:20px;
 font-weight:bold;
 height:30px;
 line-height:30px;
}
.input-field.labelled .prefix {
 top:30px;
}
.input-field input,
.input-field input:hover,
.input-field input:active,
.input-field textarea,
.input-field textarea:hover,
.input-field textarea:active {
 background-color:#ffffff !important;
 border:none !important;
 border-radius:2px;
 box-shadow:0 1px 2px -1px rgba(0,0,0,0.1),0 1px 0px 0 rgba(0,0,0,0.1),0 1px 0px -2px rgba(0,0,0,0.1);
 box-sizing:border-box;
 height:50px !important;
 line-height:50px;
 padding:0 15px;
}
.input-field textarea,
.input-field textarea:hover,
.input-field textarea:active {
 height:100px;
 line-height:20px;
 margin-bottom:0;
 max-height:100px;
 min-height:100px;
 padding:15px 0;
 overflow-y:auto;
}
.input-field .controls {
 position:absolute;
 right:15px;
 top:15px;
}
.input-field .controls span {
 background-color:#3138494d;
 border-radius:50%;
 color:#ffffff;
 cursor:pointer;
 display:inline-block;
 font-size:16px;
 font-weight:bold;
 height:20px;
 line-height:20px;
 margin-left:5px;
 text-align:center;
 width:20px;
}
.input-field .controls span:hover {
 background-color:#313849;
}
.input-field .controls span:first-child {
 margin-left:0;
}
.select-wrapper span.caret {
 pointer-events:none;
 text-align:center;
 width:45px;
 z-index:10;
}
.input-field .prefix {
 color:#d32f2f;
 font-size:18px;
 height:50px;
 line-height:50px;
 pointer-events:none;
 text-align:center;
 z-index:10;
}
.input-field .prefix ~ input,
.input-field .prefix ~ textarea,
.input-field .prefix ~ .autocomplete-content {
 margin-left:0;
 padding-left:3rem;
 width:100%;
}
.prefix ~ .select-wrapper {
 margin-left:0;
 width:100%;
}
.prefix ~ .select-wrapper input {
 padding-left:3rem;
}


.left {
    float: left !important;
}

.right {
    float: right !important;
}

.pagination {
 margin:0;
 text-align:center;
}
.pagination li.active {
 background-color:transparent;
}
.pagination li a,
.pagination li span {
 color:#444;
 cursor:pointer;
 display:inline-block;
 font-size:14px;
 height:30px;
 line-height:30px;
 padding:0 10px;
}
.pagination li.disabled a,
.pagination li.disabled span {
 cursor:default;
 color:#999;
}
.pagination li.active a,
.pagination li.active span {
 color:#313849;
 font-weight:bold;
 text-decoration:underline;
}
.pagination li.left a,
.pagination li.left span,
.pagination li.right a,
.pagination li.right span {
 padding:0;
}
.pagination li:first-child i {
 font-size:18px;
 padding-left:8px;
 transform:rotate(90deg);
 -webkit-transform:rotate(90deg);
}
.pagination li:last-child i {
 font-size:18px;
 padding-right:8px;
 transform:rotate(-90deg);
 -webkit-transform:rotate(-90deg);
}

.material-icons {
 text-rendering:optimizeLegibility;
 -webkit-font-feature-settings:'liga';
 -moz-font-feature-settings:'liga';
 font-feature-settings:'liga'
}


/* fallback */
@font-face {
  font-family: 'Material Icons';
  font-style: normal;
  font-weight: 400;
  src: url(https://fonts.gstatic.com/s/materialicons/v77/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2) format('woff2');
}

.material-icons {
  font-family: 'Material Icons';
  font-weight: normal;
  font-style: normal;
  font-size: 24px;
  line-height: 1;
  letter-spacing: normal;
  text-transform: none;
  display: inline-block;
  white-space: nowrap;
  word-wrap: normal;
  direction: ltr;
  -moz-font-feature-settings: 'liga';
  -moz-osx-font-smoothing: grayscale;
}

.card {
 background-color:#f3f3f4;
 border:solid 1px transparent;
 border-bottom:solid 1px #3138491a;
 border-radius:2px;
 border-right:solid 5px #313849;
 box-shadow:none;
 display:inline-block;
 margin:0 0 10px;
 overflow:hidden;
 padding:10px 0 10px 10px;
 width:100%;
}
.card:hover {
 border-color:#d32f2f;
}
.card .card-counter {
 background-color:#313849;
 border-radius:2px;
 color:#ffffff;
 font-size:12px;
 font-weight:bold;
 height:20px;
 line-height:20px;
 left:-26px;
 padding:0 10px;
 position:absolute;
 text-align:center;
 top:24px;
 transform:rotate(-45deg);
 -webkit-transform:rotate(-45deg);
 width:120px;
 z-index:10;
}
.card:hover .card-counter {
 background-color:#d32f2f;
 color:#ffffff;
}
.card .card-counter.yellow {
 background-color:#FFD54F !important;
 color:#000000 !important;
}
.card .card-counter.blue {
 background-color:#00BCD4 !important;
 color:#FFFFFF !important;
}
.card .card-counter.red {
 background-color:#D32F2F !important;
 color:#FFFFFF !important;
}
.card .card-image {
 background-color:#0000000D;
 border-radius:4px;
 display:table;
 float:left;
 height:172px;
 overflow:hidden;
 width:258px;
 position: relative;
}
.card .card-image img {
 /*position:absolute;
 top:50%;
 transform:translateY(-50%);
 -webkit-transform:translateY(-50%);*/
 height:172px;
 overflow:hidden;
 width:258px;
 object-fit: cover;
}
.card .card-image span {
 color:#313849;
 font-weight:bold;
 display:table-cell;
 text-align:center;
 vertical-align:middle;
}
.card .carousel {
 border-radius:4px;
 float:left;
 height:172px;
 width:258px;
}
.card .card-content {
 float:left;
 padding:5px 20px;
 width:calc(82% - 258px);
}

.card .card-content .card-title {
 color:#313849;
 display:inline-block;
 font-size:17px;
 font-weight:bold;
 line-height:22px;
 margin:0;
 width:100%;
}
.card:hover .card-content .card-title {
 color:#d32f2f;
}
.card .card-content .card-subtitle {
 color:#313849;
 display:inline-block;
 font-size:14px;
 font-style:italic;
 font-weight:bold;
 margin:0 0 15px;
 opacity:.6;
 width:100%;
}
.card .card-content p {
 color:#313849;
 font-size:14px;
 height:102px;
 opacity:.8;
 overflow:hidden;
}
.card .card-content p a {
 color:#d32f2f;
 font-weight:bold;
}
.card .card-action {
 background-color:transparent;
 border:none;
 float:right;
 padding:8px 0;
 width:18%;
}
.card .card-action .card-price {
 background-color:#313849;
 color:#ffffff;
 display:inline-block;
 font-weight:bold;
 height:38px;
 line-height:38px;
 margin-left:12px;
 padding:0 10px;
 position:relative;
 text-align:center;
 width:calc(100% - 12px);
}
.card:hover .card-action .card-price {
 background-color:#d32f2f;
}
.card .card-action .card-price:before,
.card .card-action .card-price:after {
 border-style:solid;
 border-color:transparent #313849 transparent transparent;
 content:"";
 height:0;
 left:-12px;
 position:absolute;
 width:0;
}
.card .card-action .card-price:before {
 border-width:0 12px 19px 0;
 top:0;
}
.card .card-action .card-price:after {
 border-width:19px 12px 0 0;
 bottom:0;
}
.card:hover .card-action .card-price:before,
.card:hover .card-action .card-price:after {
 border-color:transparent #d32f2f transparent transparent;
}
.card .card-action .card-details {
 margin-left:- 12px;
}
.card .card-action .card-details span {
 color:#313849;
 display:inline-block;
 font-size:12px;
 height:30px;
 line-height:30px;
}
.card .card-action .card-details strong {
 font-size:16px;
 font-weight:bold;
 margin-right:5px;
}
.card .card-action .card-details i {
 color:#d32f2f;
 font-size:16px;
 height:30px;
 line-height:30px;
 width:20px;
}



/* SWITCH
-------------------------------------------------- */
.switch{position:relative;display:inline-block;width:100%;height:11px;margin:12px 0 23px !important;}
.switch input{display:none;}
.switch-slide{position:absolute;cursor:pointer;top:0;left:0;right:0;bottom:0;background-color:rgba(0, 0, 0, 0.08);-webkit-transition:.4s;transition:.4s;}
.switch-slide:before{position:absolute;content:"";box-shadow:0 2px 2px 0 rgba(0,0,0,0.14),0 1px 5px 0 rgba(0,0,0,0.12),0 3px 1px -2px rgba(0,0,0,0.2);height:16.8px;width:16.8px;left:-8px;bottom:-2px;background-color:white;-webkit-transition:.6s;transition:.6s;}
input:checked + .switch-slide{background-color:<?php print($primary); ?>;}
input:focus + .switch-slide{box-shadow:0 0 1px <?php print($primary); ?>;}
input:checked + .switch-slide:before{left:calc(100% - 10px);}
.switch-slide.round{border-radius:34px;}
.switch-slide.round:before{border-radius:50%;}


/* CONTACT-US
-------------------------------------------------- */
.block-text .block-img .block-details{padding:15px 15px;background:#EBEFF2;box-shadow:inset 0px 0px 10px -2px #B7C4CE;}
.block-text .block-img .block-details h2{color:#FFFFFF;font-size:20px;text-transform:none;margin:0px 0px 15px 0px;font-weight:normal;}

.block-text .block-img .block-details ul{list-style:none;padding-left:50px;margin-bottom:0px;}
.block-text .block-img .block-details ul li{position:relative;color:#909598;font-size:14px;margin-bottom:10px;}
.block-text .block-img .block-details ul li:last-child{margin-bottom:0px;}
.block-text .block-img .block-details ul li a{color:#909598;}

.block-text .block-img .block-details ul li:before{position:absolute;top:0px;left:-50px;color:#9BADB8;width:22px;height:22px;line-height:22px;background:#CAD4DA;border-radius:100%;font-size:14px;font-family:FontAwesome;text-align:center;}
.block-text .block-img .block-details ul li.address:before{content:"\f041";}
.block-text .block-img .block-details ul li.phone:before{content:"\f095";}
.block-text .block-img .block-details ul li.email:before{content:"\f0e0";}

#contact-form label:not(.error){color:<?php print($tertiary); ?>;font-size:12px;width:100%;}
#contact-form label.error{display:none;position:absolute;top:0px;right:0px;color:#F82424;font-size:12px;}
#contact-form input, textarea {width:100%;outline:none;padding:2px 5px;margin-bottom:15px;max-height:150px;max-width:100%;border:solid 1px #CDD1D4;}
#contact-form input:hover, #contact-form textarea:hover{border:solid 1px #ABADAF;}
#contact-form input:focus, #contact-form textarea:focus{border:solid 1px #909394;}


/* BANNER	
-------------------------------------------------- */
.block-banner{background-color:<?php print($base); ?>;background-position:center center;background-size:cover;padding:60px 0;position:relative;}
.block-banner.empty{border-bottom:solid 2px <?php print($secondary); ?>;}
.block-banner .container{position:relative;}

.block-banner .block-bread{margin-bottom:0;padding:0;}
.block-banner .block-bread ul{margin:0 0 10px 0;}
.block-banner .block-bread li{color:<?php print($base); ?>;}
.block-banner.empty .block-bread li{color:<?php print($secondary); ?>;}
.block-banner .block-bread a{color:inherit;}
.block-banner .block-bread a:hover{color:<?php print($primary); ?>;}

.block-banner h1{color:<?php print($base); ?>;font-size:36px;line-height:45px;margin:0;padding:0;position:relative;text-transform:none;}
.block-banner.empty h1{color:<?php print($secondary); ?>;}
.block-banner h1:after{content:"";display:none;width:35px;height:6px;background:<?php print($primary); ?>;border-radius:10px;margin:15px 0 0;}
.block-banner h1 span{color:#C3EE92;}

.block-banner .logo{position:absolute;right:0;top:40%;transform:translateY(-50%);-webkit-transform:translateY(-50%);width:140px;}
.block-banner .decor{position:absolute;right:0;bottom:-60px;height:170px;}

/* IFRAMES	
-------------------------------------------------- */
/*.val-iframe{border:none;margin:0 -260px;min-height:600px;width:calc(100% + 260px);}*/
.val-iframe{border:none;margin:0;min-height:600px;width:100%;}


/* OFFICES	
-------------------------------------------------- */
.office-image{background-position:center center;background-size:cover;height:340px;}

.office-info{height:340px;padding:46px 50px;position:relative;}
.office-info:before{content:'';position:absolute;left:0;right:0;top:0;bottom:0;opacity:.9;background:<?php print($secondary); ?>;}
.office-info h5{color:<?php print($base); ?>;font-family:Montserrat;font-size:20px;font-weight:700;line-height:28px;margin:0 0 23px;position:relative;}
.office-info ul{list-style:none;margin:0 0 30px;padding:0;position:relative;}
.office-info ul li{line-height:22px;margin:0 0 13px;}
.office-info ul li i{color:<?php print($primary); ?>;font-size:20px;height:20px;line-height:20px;margin-right:15px;vertical-align:top;width:20px;}
.office-info ul li span{color:<?php print($base); ?>;display:inline-block;width:calc(100% - 35px);}
.office-info ul li a{color:<?php print($base); ?>;display:inline-block;}

.office-info ul:last-child{margin:0 0 13px;}
.office-info ul:last-child li{display:inline-block;margin:0 8px 8px 0;}
.office-info ul li img{width:32px;}


/* SITEMAP
-------------------------------------------------- */
.site-map{padding:0px;}
.site-map li{list-style:none;margin-bottom:15px;margin-top:0px;}
.site-map li a{padding:5px 10px;background:<?php print($primary); ?>;color:#FFFFFF;}

.site-map li ul{padding:0px;}
.site-map li ul li{list-style:none;margin-bottom:0px;margin-top:15px;display:inline-block;}
.site-map li ul li a{padding:5px 10px;background:#E7E7E7;color:#A6A6A6;}


/* NEWS/REVIEWS
-------------------------------------------------- */
.art-con{background-color:#FFFFFF;box-shadow:0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);position:relative;}
.rev-con{background-color:#FFFFFF;box-shadow:0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);position:relative;}
.art-con .art-ico{position:absolute;top:30px;right:30px;cursor:pointer;}
.art-con .art-ico i{color:rgba(255,255,255,0.7);border:solid 2px rgba(255,255,255,0.7);border-radius:100%;font-size:12px;height:28px;line-height:24px;text-align:center;transition:all .3s;width:28px;}
.art-con .art-img{height:212px;background-size:cover;background-position:center center;}
.rev-con .rev-img{height:212px;background-size:cover;background-position:center center;}
.art-con .art-txt{padding:35px 30px;}
.rev-con .rev-txt{padding:35px 30px;}
.art-con .art-txt h3{font-size:12px;font-weight:500;line-height:1.1;margin:0 0 15px;text-transform:uppercase;}
.rev-con .rev-txt h3{font-size:12px;font-weight:500;line-height:1.1;margin:0 0 15px;text-transform:uppercase;}
.art-con .art-txt h2{color:#86c241;font-size:14px;font-weight:bold;height:18px;line-height:1.2;margin:0 0 15px;display:-webkit-box;-webkit-line-clamp:2;-webkit-box-orient:vertical;overflow:hidden;text-overflow:ellipsis;}
.rev-con .rev-txt h2{color:#86c241;font-size:14px;font-weight:bold;height:18px;line-height:1.2;margin:0 0 15px;display:-webkit-box;-webkit-line-clamp:2;-webkit-box-orient:vertical;overflow:hidden;text-overflow:ellipsis;}
.art-con .art-txt h2:hover{color:#222222;}
.art-con .art-txt h2 a{color:inherit;}
.rev-con .rev-txt h2 a{color:inherit;}
.art-con .art-txt p{color:#5a6470;font-size:14px;max-height:33px;margin:0 0 15px 0;line-height:1.2;display:-webkit-box;-webkit-line-clamp:2;-webkit-box-orient:vertical;overflow:hidden;text-overflow:ellipsis;}
.rev-con .rev-txt p{color:#5a6470;font-size:14px;margin:0 0 15px 0;line-height:1.2;overflow:hidden;}
.art-con .art-txt span{color:#5a6470;font-size:13px;}
.rev-con .rev-txt span{color:#5a6470;font-size:13px;}
.pst-con h2{color:#222222;font-family:Montserrat;font-size:20px;font-weight:bold;margin:0 0 30px;letter-spacing:-.04em;line-height:28px;}
.pst-con h2:after{content:'';display:block;width:35px;height:3px;background:#86c241;border-radius:10px;margin:13px 0 0;width:100%;}
.pst-con .pst-img{width:450px;height:250px;background-size:cover;background-position:center center;float:left;margin:0 30px 30px 0;}
.pst-con .pst-aut{font-style:italic;}

.stats{margin:0;padding:0;}
.stats li{background-color:#CACACA;display:inline-block;margin-bottom:10px;padding:0 10px;position:relative;text-align:left;width:100%;}
.stats li span:first-child{color:<?php print($tertiary); ?>;display:inline-block;font-size:12px;font-weight:bold;height:25px;line-height:25px;position:relative;width:100%;z-index:1;}
.stats li span:last-child{background-color:<?php print($primary); ?>;bottom:0;left:0;position:absolute;right:0;top:0;}

/* POSTCODE LOOKUP
-------------------------------------------------- */
.ajax_address{background-color:<?php print($primary); ?>;border-radius:2px !important;box-shadow:0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);left:15px;max-height:199px;overflow-y:auto;position:absolute;right:15px;top:calc(100% - 20px);z-index:10;}
.ajax_address ul{list-style:none;margin:0;padding:10px 0;width:calc(100% - 50px);}
.ajax_address ul li{color:<?php print($base); ?>;cursor:pointer;padding:5px 20px;}
.ajax_address ul li:hover{background-color:rgba(0, 0, 0, 0.1);}
.ajax_address .close{font-size:30px;line-height:18px;padding:15px !important;position:absolute;right:0px;top:0;width:auto;}
.ajax_address .close:hover{background-color:transparent;opacity:.8;}
.ajax_address span{color:<?php print($base); ?>;display:inline-block;padding:15px 20px;width:calc(100% - 50px);}

/* FOOTER
-------------------------------------------------- */
footer{background-color:#EBEBEB;padding:60px 0;}
footer ul{display:inline-block;list-style:none;margin:0 0 30px;padding:0 0 30px;text-align:center;vertical-align:middle;width:100%;}
footer ul.links{border-bottom:solid 1px <?php print($secondary); ?>;display:flex;justify-content:space-between;}
footer ul li{display:inline-block;vertical-align:middle;}
footer ul.links li{font-size:14px;font-weight:bold;text-transform:uppercase;}
footer ul li a{color:<?php print($secondary); ?>;}

.block-foot{background:transparent;padding:57px 0px 40px 0px;}

.block-foot .container{padding:0px 30px;}

.block-foot .block-block:nth-child(1){padding-right:30px;}
.block-foot .block-block:nth-child(2){padding:0px 30px;}
.block-foot .block-block:nth-child(3){padding-left:30px;}

.block-foot h3{color:<?php print($tertiary); ?>;margin:0px 0px 27px 0px;}
.block-foot p{color:#ABB2BA;margin:0px 0px 23px 0px;font-size:13px;}

.block-foot .block-block ul{list-style:none;padding-left:12px;}
.block-foot .block-block ul.no-bullet{padding-left:0;}
.block-foot .block-block ul li{position:relative;font-size:13px;margin-bottom:10px;}
.block-foot .block-block ul li.marginex{margin-bottom:22px;}
.block-foot .block-block ul li a{color:#ABB2BA;line-height:20px;text-decoration:none;}

.block-foot .block-block ul li:before{color:<?php print($tertiary); ?>;content:"\f0da";font-family:FontAwesome;font-size:12px;left:-10px;position:absolute;}
.block-foot .block-block ul.no-bullet li:before{display:none;}
.block-foot .block-block ul li:last-child{margin-bottom:0px;}

.block-foot .block-block ul li.address:before{content:"\f041";}
.block-foot .block-block ul li.phone:before{content:"\f095";}
.block-foot .block-block ul li.email:before{content:"\f0e0";}


/* BOTTOM
-------------------------------------------------- */
.block-bottom{bottom:0;border-top:solid 3px <?php print($primary); ?>;box-shadow:inset 0 2px 5px 0 rgba(0,0,0,.39);padding:28px 0px;background-image:url(/images/elements/footer-background.jpg);background-position:center center;background-size:cover;position:relative;display:flow-root;}
.block-bottom:before{content:'';position:absolute;left:0;right:0;top:0;bottom:0;opacity:.9;background:<?php print($secondary); ?>;}
.block-bottom p{font-size:15px;color:rgba(255,255,255,.6);margin:0;}
.block-bottom a{color:inherit;text-decoration:underline;}
.block-bottom a:hover{text-decoration:underline;}
.block-bottom a.link{display:inline-block;width:100%;}
.block-bottom span{display:inline-block;margin-left:10px;vertical-align:top;width:200px;}


/* RESPONSIVE
-------------------------------------------------- */
@media only screen and (max-width : 1199px) {	
	.navbar-brand{padding-top:30px;}
	.navbar-brand img{max-width:50px;}
	
	.navbar-nav>li:last-child{position:absolute;right:0;}
	
	.carousel-caption h1{font-size:44px;line-height:51px;}
	
	.navbar-default .navbar-collapse .navbar-nav>li>a{font-size:14px;}
	
	.carousel-indicators{display:none;}
	
	.block-prop{width:calc(50% - 20px);}
	.block-prop:nth-child(2n){margin-right:0;}
	.block-prop:nth-child(3n){margin-right:30px;}
	
	.block-text p{width:100%;}
	
	.val-iframe{margin:0 -60px;width:calc(100% + 60px);}
}
@media only screen and (max-width : 991px) {
	.main-content{padding-right:15px;}
	.sidebar-content{float:none;}
	
	.navbar-nav li{margin:0 10px;}
	.navbar-default .navbar-collapse .navbar-nav>li>a{font-size:12px;}
	.navbar-nav li a{font-size:12px;}
	
	.block-top .label{display: none;}
	.block-top .email{display:none;}
	
	.block-banner{padding:45px;}
	.block-banner h1{font-size:30px;}
	
	.carousel-caption h1{font-size:28px;line-height:35px;}
	
	.block-props{padding-bottom:15px;}
	.block-props > .container > .row > .col-md-9{padding-right:15px !important;}
	.block-props > .container > .row > .col-md-3{padding-left:15px !important;}
	
	.block-prop{width:calc(33.33333333% - 20px);}
	.block-prop:nth-child(2n){margin-right:30px;}
	.block-prop:nth-child(3n){margin-right:0;}
	
	.block-nav a{font-size:28px;}
	
	.block-share ul{overflow:auto;white-space:nowrap;}
	.block-share li{height:50px;line-height:50px;}
	.block-share img{height:35px;}
	
	.filter-side label{display:inline-block;vertical-align:top;width:100%;}
	
	.to-top{display:none;}
	
	.val-iframe{margin:0;width:100%;}
}
@media only screen and (max-width : 850px) {
	.desktopView {
		display: none;
		visibility: hidden;
	}
}

@media only screen and (min-width : 850px) {
	.mobileView {
		visibility: hidden;
		display: none;
	}
}
@media only screen and (max-width : 767px) {
	body.fixed-head{padding-top:68px;}
	
	p{font-size:14px;}
	
	header{background-position:top right;background-size:auto 131.1px;padding-top:20px;padding-bottom:20px; position: relative; z-index:50 !important;}
	.fixed-head header{top:-96px;transform:translateY(96px);-webkit-transform:translateY(96px);}
	header .logo{width:200px;}
	header input{width:calc(100% - 42px);}
	
	.block-top{display:none;}
	
	.block-logo img{width:95px;}
	
	.block-nav a{font-size:24px;}
	
	.navbar-default .navbar-toggle{background-color:<?php print($tertiary); ?>;border:none;box-shadow:0 5px 15px rgba(0,0,0,.16);height:40px;margin:0;right:15px;position:absolute;top:-150px;width:40px;}
	.navbar-default .navbar-toggle:focus,.navbar-default .navbar-toggle:hover{background-color:#000000;}
	.navbar-default .navbar-toggle .icon-bar{background-color:<?php print($base); ?>;}
	
	header h2{display:none;}
	
	.navbar{height:0;left:0;margin:0;min-height:0;padding:0;position:absolute;right:0;top:100%;}
	.navbar .navbar-header{height:0;}
	.navbar-nav{float:none;margin:0;padding:15px;}
	.navbar-nav li{float:none !important;margin:0;}
	.navbar-nav>li:last-child{position:relative;}
	.navbar-default .navbar-collapse .navbar-nav>li>a{font-size:14px;}
	.navbar-nav li a{font-size:14px;}
	
	.navbar-nav li .dropdown-menu{transform:none;-webkit-transform:none;box-shadow:none;background-color:transparent;opacity:0;}
	.navbar-nav li.dropdown:hover ul.dropdown-menu{opacity:0;
    overflow: hidden;
        -webkit-transform: translate(0,30px);
    -moz-transform: translate(0,30px);
    -ms-transform: translate(0,30px);
    -o-transform: translate(0,30px);
    transform: translate(0,30px);
    pointer-events: auto;}
	.navbar-nav .open .dropdown-menu{opacity:1 !important;tranform:none !important;-webkit-transform:none !important;transition:none !important;-webkit-transition:none !important;}
	.navbar-nav li .dropdown-menu li:not(:last-child){padding-bottom:15px;}
	.navbar-nav .open .dropdown-menu .dropdown-header, .navbar-nav .open .dropdown-menu>li>a{padding:5px 0;transition:none;-webkit-transition:none;}
	
	.navbar .navbar-header{height:0;}
	.navbar-default .navbar-collapse, .navbar-default .navbar-form{background-color:<?php print($base); ?>;box-shadow:inset 0 5px 12px -5px rgba(0,0,0,.25),0 5px 12px rgba(0,0,0,.25);width:100%;}
	
	.navbar-brand{padding-top:0;}
	.navbar-brand img{max-width:140px;}
	
	.carousel{height:360px;}
	.carousel-caption{ right: 5%; left: 5%;}
	.carousel-caption h1{font-size:32px;text-align:center;}
	.carousel-caption h2{font-size:18px;line-height:26px;margin-top:0;text-align:center;}
	.carousel .item{height:100%;}
	
	.block-prop{margin-right:15px;width:calc(50% - 7.5px);}
	.block-prop:nth-child(2n){margin-right:0;}
	.block-prop:nth-child(3n){margin-right:15px;}
	.block-prop .carousel .carousel-control span{line-height:20px;}
	
	.block-types .row > div{padding:0;}
	.block-type{border-radius:0;display:flex;margin:0;}
	.block-type a{line-height:86px;padding:15px;}
	.block-type a h4{font-size:16px;line-height:22px;vertical-align:middle;}
	
	.block-adverts{padding:15px 0;}
	.block-types ~ .block-adverts{padding-top:45px;}
	.block-adverts h3{font-size:24px;}
	.block-adverts h3:after{height:4px;margin-top:10px;}
	.block-adverts .row{margin-bottom:0;}
	.block-advert{background-color:<?php print($base); ?>;margin:0 0 15px;width:calc(100% - 15px);}
	.block-adverts .row:nth-child(1){float:right;}
	.block-advert .text h5{font-size:16px;margin:0 0 5px;}
	.block-advert .artwork{display:none;}
	.block-advert .text{float:none;padding-left:0;width:100%;}
	
	.block-banner{padding:30px 0;}
	.block-banner h1{font-size:24px;text-align:center;}
	
	.jplist-panel{display:block;}
	
	.filter-side{background-color:<?php print($base); ?>;left:100%;padding:0 20px;position:fixed;top:120px;z-index:1000;}
	.filter-side:before{background-color:<?php print($secondary); ?>;content:"f1de":font-family:FontAwesome;}
	
	.modal-dialog{width:100%;}
	
	.block-text{padding:60px 0 80px;}
	.block-text:after{bottom:40px;}
	.block-text.add-decor:after{bottom:40px;}
	
	.block-widget{border-bottom-left-radius:0;border-bottom-right-radius:0;float:none;margin:60px auto 0 !important;}
	
	.block-block:not(:first-child){margin-top:15px;}
	.block-block .block-content{padding-left:30px;padding-right:30px;}
	
	.block-text .block-img{padding-right:0px;margin-bottom:30px;}
	.block-text .block-par{padding-left:0px;}
	.block-text .block-side{padding-right:0px;}
	
	.block-feat{padding:60px 0;}
	
	.block-feat h4{margin-bottom:40px;}
	
	.block-feat .row:last-child{margin-top:-30px;}
	
	.block-feat .row div{margin-bottom:20px;padding:0 15px !important;}
	.block-feat .row:last-child div{margin-bottom:0;}
	
	.block-feat form button{margin-top:0;}
	
	.block-share h3{display:none;}
	.block-share li{height:20px;line-height:20px;}
	.block-share img{height:20px;}

	.block-quote{position:relative;}
	.block-quote img{display:none;}
	
	.tabs-normal{display:none;}
	.tabs-collapse{display:block;}
	
	.tabs-left>.nav-tabs>li, .tabs-right>.nav-tabs>li{float:left;text-align:center;}
	.tabs-left>.nav-tabs>li>a, .tabs-right>.nav-tabs>li>a{padding:10px 0px;min-width:inherit;}
	
	#contact-form .col-sm-6{padding:0px;}
	
	.block-foot{padding-bottom:38px;}
	.block-foot .block-block:nth-child(2){margin-top:30px;padding:0px 15px;}
	.block-foot .block-block:nth-child(3){margin-top:30px;padding:0px 15px;}
	
	.block-bottom .container{padding:0px 45px;}
	.block-bottom .container .col-sm-12{padding:0px;}
	.block-bottom .container .col-sm-12 p{margin:0px;}
}

@media only screen and (max-width : 670px) {
	.card .card-content{
		width: 80%;
	}
	img{width: -webkit-fill-available;}
}

@media only screen and (max-width : 512px) {
	.mTitle{font-size:12px;}
	
	.mPara{font-size: 10px;}
	img{width: -webkit-fill-available;}
	
	.card .card-action .card-price{
		width: 160%;
	}
	
	.card .card-action{
		width: 25%;
		float: none;
	}

	.card .card-image{
		width: 350px;
		height: 300px;
	}
	

	
	.card .card-image img{
		width: 350px;
		height: 300px;
	}
}

@media only screen and (max-width : 479px) {
	h3{font-size:22px;}
	
	.block-nav a{font-size:18px;}
	img{width: -webkit-fill-available;}
	
	.carousel-caption h1{font-size:23px;line-height:30px;}
	
	.block-prop{margin-right:0;width:100%;}
	
	.block-quote span{display:none;}
	.block-quote a{background-color:transparent;border-color:transparent;color:<?php print($base); ?>;width:100%;}
	.block-quote a i{color:<?php print($base); ?>;}
	
	.block-share li{margin:30px 10px 10px;}
	.block-share img{height:20px;}
}

@media only screen and (max-width : 375px) {
	.mTitle{font-size:11px;}
	img{width: -webkit-fill-available;}
	
}

@media only screen and (max-width : 319px) {
	h4{font-size:14px;}
	
	.carousel .item{height:720px;}
	img{width: -webkit-fill-available;}
	.block-widget{margin-top:40px;padding:40px 25px 25px;}
}




/*
  Bootstrap Carousel Fade Transition (for Bootstrap 3.3.x)
  CSS from:       http://codepen.io/transportedman/pen/NPWRGq
  and:            http://stackoverflow.com/questions/18548731/bootstrap-3-carousel-fading-to-new-slide-instead-of-sliding-to-new-slide
  Inspired from:  http://codepen.io/Rowno/pen/Afykb 
*/
.carousel-fade .carousel-inner .item {
  opacity: 0;
  transition-property: opacity;
}

.carousel-fade .carousel-inner .active {
  opacity: 1;
}

.carousel-fade .carousel-inner .active.left,
.carousel-fade .carousel-inner .active.right {
  left: 0;
  opacity: 0;
  z-index: 1;
}

.carousel-fade .carousel-inner .next.left,
.carousel-fade .carousel-inner .prev.right {
  opacity: 1;
}

.carousel-fade .carousel-control {
  z-index: 2;
}

/*
  WHAT IS NEW IN 3.3: "Added transforms to improve carousel performance in modern browsers."
  Need to override the 3.3 new styles for modern browsers & apply opacity
*/
@media all and (transform-3d), (-webkit-transform-3d) {
    .carousel-fade .carousel-inner > .item.next,
    .carousel-fade .carousel-inner > .item.active.right {
      opacity: 0;
      -webkit-transform: translate3d(0, 0, 0);
              transform: translate3d(0, 0, 0);
    }
    .carousel-fade .carousel-inner > .item.prev,
    .carousel-fade .carousel-inner > .item.active.left {
      opacity: 0;
      -webkit-transform: translate3d(0, 0, 0);
              transform: translate3d(0, 0, 0);
    }
    .carousel-fade .carousel-inner > .item.next.left,
    .carousel-fade .carousel-inner > .item.prev.right,
    .carousel-fade .carousel-inner > .item.active {
      opacity: 1;
      -webkit-transform: translate3d(0, 0, 0);
              transform: translate3d(0, 0, 0);
    }
}

.zigzag{height:10px;position:relative;width:70px;}

.zigzag:before {
  content: '';
  position: absolute;
  background-repeat: repeat;
  z-index: -1;
}

.zigzag:before {
  background: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuMCIgeTE9IjAuMCIgeDI9IjEuMCIgeTI9IjEuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9Ijc1JSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9Ijc1JSIgc3RvcC1jb2xvcj0iI2I2YjVlYiIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2I2YjVlYiIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA=='), url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjEuMCIgeTE9IjAuMCIgeDI9IjAuMCIgeTI9IjEuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9Ijc1JSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9Ijc1JSIgc3RvcC1jb2xvcj0iI2I2YjVlYiIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2I2YjVlYiIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA=='), url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuMCIgeTE9IjEuMCIgeDI9IjEuMCIgeTI9IjAuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9Ijc1JSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9Ijc1JSIgc3RvcC1jb2xvcj0iI2I2YjVlYiIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2I2YjVlYiIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA=='), url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjEuMCIgeTE9IjEuMCIgeDI9IjAuMCIgeTI9IjAuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9Ijc1JSIgc3RvcC1jb2xvcj0iIzAwMDAwMCIgc3RvcC1vcGFjaXR5PSIwLjAiLz48c3RvcCBvZmZzZXQ9Ijc1JSIgc3RvcC1jb2xvcj0iI2I2YjVlYiIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2I2YjVlYiIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
  background: -moz-linear-gradient(315deg, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 75%, #b6b5eb 75%, #b6b5eb), -moz-linear-gradient(225deg, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 75%, #b6b5eb 75%, #b6b5eb), -moz-linear-gradient(45deg, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 75%, #b6b5eb 75%, #b6b5eb), -moz-linear-gradient(135deg, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 75%, #b6b5eb 75%, #b6b5eb);
  background: -webkit-linear-gradient(315deg, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 75%, #b6b5eb 75%, #b6b5eb), -webkit-linear-gradient(225deg, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 75%, #b6b5eb 75%, #b6b5eb), -webkit-linear-gradient(45deg, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 75%, #b6b5eb 75%, #b6b5eb), -webkit-linear-gradient(135deg, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 75%, #b6b5eb 75%, #b6b5eb);
  background: linear-gradient(135deg, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 75%, #b6b5eb 75%, #b6b5eb), linear-gradient(225deg, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 75%, #b6b5eb 75%, #b6b5eb), linear-gradient(45deg, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 75%, #b6b5eb 75%, #b6b5eb), linear-gradient(315deg, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 75%, #b6b5eb 75%, #b6b5eb);
  background-position: 0.5em 0.5em, 0.5em 0.5em, 0 0.5em, 0 0.5em;
  background-size: 1em 1em;
  left: 0;
  top: -1em;
  height: 100%;
  width: 100%;
}

@media print {
	body.fixed-head {
		padding-top: 0;
	}
	.hide-on-print {
		display: none;
	}
	header {
		display: none;
	}
	.to-top {
		display: none;
	}
	.block-contain {
		background-color: orange;
	}
	.block-detail .carousel {
		border-top: solid 1px #cacaca;
		border-left: solid 1px #cacaca;
		margin: 0;
	}
	.block-detail .carousel-control {
		display: none;
	}
	.block-detail .carousel-fade .carousel-inner .item {
		border-bottom: solid 1px #cacaca;
		border-right: solid 1px #cacaca;
		display: block;
		float: left;
		height: 140px;
		opacity: 1;
		overflow: hidden;
		padding: 5px;
		width: 33.33%;
	}
	.block-detail .nav-tabs {
		display: none;
	}
	.block-detail .tab-content {
		padding: 30px 0;
	}
	.block-detail .tab-content .tab-pane {
		display: block;
		opacity: 1;
		margin: 0 0 30px;
	}
	.block-tabs {
		margin: 0;
	}
	.block-tabs li {
		margin: 0;
		min-height: 0;
		width: 100%;
	}
	.block-detail .block-options {
		display: none;
	}
	#map { 
		display: none;
	}
	.sharethis-inline-share-buttons {
		display: none !important;
	}
	.block-bottom {
		display: none;
	}
}

.sidebar .area {
 background-color:#f3f3f4;
 border-bottom:solid 1px #3138491a;
 border-top:solid 1px #ffffff;
 padding:20px 27px;
 position:relative;
}
.sidebar h4 {
 font-size:18px;
 margin:0 0 20px;
}
.sidebar p {
 margin:0 0 15px;
}
.sidebar ul {
 margin:0;
}
.sidebar .enquire {
 padding:35px 30px;
}
.sidebar .enquire:before {
 background-color:#d32f2fe6;
 content:"";
 height:5px;
 left:0;
 position:absolute;
 right:0;
 top:0;
}
.sidebar .enquire p {
 margin:0 0 20px;
}
.sidebar .enquire .btn {
 float:left;
 font-size:14px;
 height:40px;
 line-height:40px;
 margin:15px 5px 0 0;
 padding:0 15px;
}
  
@media only screen and (max-width : 513px) {
	.sstcRibbon{
		display: none;
	}
	
	.sstcMobile{
	position: absolute;
	left: 75%;
	width: 28% !important;
	object-fit: contain !important;
	margin-top: -31%;
	}
}  
@media only screen and (min-width : 513px) {
	
	.sstcMobile{
		display: none;
	}
}  

@media only screen and (min-width : 992px) {
	.col-md-6{
		
		width: 30% !important;
	}
}

@media only screen and (max-width : 992px) {
	.col-md-6{
		width: 100% !important;
	}
}
<?php
	
	$styleCss = ob_get_clean();
	print(compressCss($styleCss));

?>
