// Get the modal
var modal = document.getElementById("gallery-modal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

//var featured = document.getElementsByClassName("feature")[0];

var gallery = document.querySelector('.gallery');
var galleryItems = document.querySelectorAll('.gallery-item');
var numOfItems = gallery.children.length;
var itemWidth = 12.5; // percent: as set in css

var featured = document.querySelector('.featured-item');

var imageSets = [
	//MK added
	[
	'images/100_0129.png',
	'images/100_0135.JPG',
	'images/100_0154.JPG',
	'images/IMG-20201204-WA0017.jpg',
	'images/IMG-20201204-WA0020.jpg',
	'images/IMG-20201204-WA0030.jpg',
	'images/IMG-20201204-WA0034.jpg',
	'images/IMG-20210226-WA0001.jpg',
	'images/IMG-20210226-WA0005.jpg',
	'images/IMG-20210226-WA0010.jpg',
	'images/IMG-20210226-WA0011.jpg',
	'images/IMG-20210306-WA0007.jpg',
	'images/IMG-20210306-WA0011.jpg',
	'images/IMG-20210306-WA0012.jpg',
	'images/IMG-20210306-WA0015.jpg',
	'images/IMG-20210306-WA0016.jpg',
	'images/IMG-20210306-WA0018.jpg',
	'images/IMG-20210306-WA0021.jpg',
	'images/IMG-20210306-WA0023.jpg',
	'images/IMG-20210306-WA0024.jpg',
	'images/IMG-20210306-WA0028.jpg',
	'images/IMG-20210312-WA0017.jpg',
	'images/IMG-20210312-WA0035.jpg',
	'images/IMG-20210312-WA0041.jpg',
	'images/IMG-20210312-WA0046.jpg',
	'images/IMG-20210312-WA0047.jpg',
	'images/IMG-20210312-WA0051.jpg',
	'images/IMG-20210318-WA0009.jpg',
	'images/IMG-20210318-WA0020.jpg',
	'images/IMG-20210318-WA0022.jpg',
	'images/IMG-20210318-WA0023.jpg',
	'images/IMG-20210318-WA0032.jpg',
	'images/IMG-20210318-WA0035.jpg',
	'images/IMG-20210318-WA0083.jpg',
	'images/IMG-20210318-WA0097.jpg',
	'images/IMG-20210322-WA0037.jpg',
	'images/IMG-20210322-WA0038.jpg',
	'images/IMG-20210322-WA0040.jpg',
	'images/IMG-20210322-WA0050.jpg',
	'images/IMG-20210322-WA0060.jpg',
	'images/IMG-20210322-WA0061.jpg',
	'images/IMG-20210322-WA0063.jpg',
	'images/IMG-20210322-WA0065.jpg',
	'images/IMG-20210322-WA0068.jpg',
	'images/IMG-20210322-WA0069.jpg',
	'images/IMG-20210322-WA0081.jpg',
	'images/IMG-20210322-WA0086.jpg',
	'images/IMG-20210322-WA0087.jpg',
	'images/IMG-20210322-WA0100.jpg',
	'images/IMG-20210322-WA0101.jpg',
	],
	
	
];

// Set buttons such that, when the user clicks on a button, the modal is opened and populated with relevant images

function fnLoadImageSet(imageSetNum) {
	console.log(imageSetNum);
	let images = [...imageSets[imageSetNum]];
	
	//Set Initial Featured Image
	featured.style.backgroundImage = 'url(' + images[0] + ')';
	
	//Set Images for Gallery and Add Event Listeners
	for (var i = 0; i < galleryItems.length; i++) {
		galleryItems[i].style.backgroundImage = 'url(' + images[i] + ')';
		galleryItems[i].addEventListener('click', selectItem);
	}
	
	modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

/*gallery scroll*/

var leftBtn = document.querySelector('.move-btn.left');
var rightBtn = document.querySelector('.move-btn.right');
var leftInterval;
var rightInterval;

var scrollRate = 0.8;
var left;

function selectItem(e) {
	if (e.target.classList.contains('active')) return;
	
	featured.style.backgroundImage = e.target.style.backgroundImage;
	
	for (var i = 0; i < galleryItems.length; i++) {
		if (galleryItems[i].classList.contains('active'))
			galleryItems[i].classList.remove('active');
	}
	
	e.target.classList.add('active');
}

function galleryWrapLeft() {
	var first = gallery.children[0];
	gallery.removeChild(first);
	gallery.style.left = -itemWidth + '%';
	gallery.appendChild(first);
	gallery.style.left = '0%';
}

function galleryWrapRight() {
	var last = gallery.children[gallery.children.length - 1];
	gallery.removeChild(last);
	gallery.insertBefore(last, gallery.children[0]);
	gallery.style.left = '-23%';
}

function moveRight() {
	left = left || 0;

	leftInterval = setInterval(function() {
		gallery.style.left = left + '%';

		if (left > -itemWidth) {
			left -= scrollRate;
		} else {
			left = 0;
			galleryWrapLeft();
		}
	}, 1);
}

function moveLeft() {
	//Make sure there is element to the left
	if (left > -itemWidth && left < 0) {
		left = left  - itemWidth;
		
		var last = gallery.children[gallery.children.length - 1];
		gallery.removeChild(last);
		gallery.style.left = left + '%';
		gallery.insertBefore(last, gallery.children[0]);	
	}
	
	left = left || 0;

	leftInterval = setInterval(function() {
		gallery.style.left = left + '%';

		if (left < 0) {
			left += scrollRate;
		} else {
			left = -itemWidth;
			galleryWrapRight();
		}
	}, 1);
}

function stopMovement() {
	clearInterval(leftInterval);
	clearInterval(rightInterval);
}

leftBtn.addEventListener('mouseenter', moveLeft);
leftBtn.addEventListener('mouseleave', stopMovement);
rightBtn.addEventListener('mouseenter', moveRight);
rightBtn.addEventListener('mouseleave', stopMovement);