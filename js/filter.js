$(document).ready(function() {
	$("#properties").load("/includes/properties.php");
	var data = {};
	data["location"] = $("select[name='location']").val();
		data["type"] = $("select[name='type']").val();
		data["bedrooms"] = parseInt($("select[name='bedrooms']").val());
		data["bathrooms"] = parseInt($("select[name='bathrooms']").val());
		$.ajax({
			url: '/includes/properties.php',
			type: 'POST',
			data: data,
			success: function(response) {
				$("#properties").html(response);
			},
			error: function() {
				alert("Error loading properties!");
			}
		});
	
	
	$("select.filter").change(function() {
		data["location"] = $("select[name='location']").val();
		data["type"] = $("select[name='type']").val();
		data["bedrooms"] = parseInt($("select[name='bedrooms']").val());
		data["bathrooms"] = parseInt($("select[name='bathrooms']").val());
		$.ajax({
			url: '/includes/properties.php',
			type: 'POST',
			data: data,
			success: function(response) {
				$("#properties").html(response);
			},
			error: function() {
				alert("Error loading properties!");
			}
		});
	});
/*	$(".numToggle").click(function(){
		var location = $("select[name='location']").val();
		var type = $("select[name='type']").val();
		// Check which filter has been toggled
		if($(this).hasClass("bed")) {
			var bedrooms = parseInt($("input[name='bedrooms']").val()) + $(this).data("value");
			var bathrooms = parseInt($("input[name='bathrooms']").val());
		} else {
			var bedrooms = parseInt($("input[name='bedrooms']").val());
			var bathrooms = parseInt($("input[name='bathrooms']").val()) + $(this).data("value");
		}
		// Check if value is more/less than max/min
		if(bedrooms < 0 || bedrooms > 4 || bathrooms < 0 || bathrooms > 4) {
			return false;
		} else {
			data['location'] = location;
			data['type'] = type;
			data['bedrooms'] = bedrooms;
			data['bathrooms'] = bathrooms;
			$.ajax({
				url: '/includes/properties.php',
				type: 'POST',
				data: data,
				success: function(response) {
					$('#properties').html(response);
				},
				error: function() {
					alert('Hello World!');
				}
			});
			// Change input value after toggle
			if($(this).hasClass("bed")) {
				if(bedrooms < 1) $("input[name='bedrooms']").val("Any Bedrooms");
				else $("input[name='bedrooms']").val(bedrooms + " Bedrooms");
			} else {
				if(bathrooms < 1) $("input[name='bathrooms']").val("Any Bathrooms");
				else $("input[name='bathrooms']").val(bathrooms + " Bathrooms");
			}
			alert("Bedrooms: "+bedrooms+"\nBathrooms: "+bathrooms);
		}
	});*/
});