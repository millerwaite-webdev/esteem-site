<?php

print("<div class='col-sm-4 block-side'>");
	print("<div class='tabbable tabs-left'>");

		switch($strPage){

			case "who-we-are":
				
				print("<ul class='nav nav-tabs tabs-normal'>");
					print("<li class='active'><a href='#a' data-toggle='tab'>Who We are</a></li>");
					print("<li><a href='#b' data-toggle='tab'>Our People</a></li>");
					print("<li><a href='#c' data-toggle='tab'>Treating Customers Fairly</a></li>");
					print("<li><a href='#d' data-toggle='tab'>Privacy</a></li>");
				print ("</ul>");
				
				print("<ul class='nav nav-tabs tabs-collapse'>");
					print("<li class='active' style='width:25%;'><a href='#a' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
					print("<li style='width:25%;'><a href='#b' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
					print("<li style='width:25%;'><a href='#c' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
					print("<li style='width:25%;'><a href='#d' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
				print ("</ul>");

			break;	
			
			case "what-we-do":

				print("<ul class='nav nav-tabs tabs-normal'>");
					print("<li class='active'><a href='#a' data-toggle='tab'>Wealth Management</a></li>");
					print("<li><a href='#b' data-toggle='tab'>Savings & Investment</a></li>");
					print("<li><a href='#c' data-toggle='tab'>Saving For Retirement</a></li>");
					print("<li><a href='#d' data-toggle='tab'>Income In Retirement</a></li>");
					print("<li><a href='#e' data-toggle='tab'>Family Protection</a></li>");
					print("<li><a href='#f' data-toggle='tab'>Mortgages</a></li>");
					print("<li><a href='#g' data-toggle='tab'>Business Advice</a></li>");
					print("<li><a href='#h' data-toggle='tab'>Tax Planning</a></li>");
				print ("</ul>");

				print("<ul class='nav nav-tabs tabs-collapse'>");
					print("<li class='active' style='width:12.5%;'><a href='#a' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
					print("<li style='width:12.5%;'><a href='#b' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
					print("<li style='width:12.5%;'><a href='#c' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
					print("<li style='width:12.5%;'><a href='#d' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
					print("<li style='width:12.5%;'><a href='#e' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
					print("<li style='width:12.5%;'><a href='#f' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
					print("<li style='width:12.5%;'><a href='#g' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
					print("<li style='width:12.5%;'><a href='#h' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
				print ("</ul>");
				
			break;	
			
			case "how-we-do-it":

				print("<ul class='nav nav-tabs tabs-normal'>");
					print("<li class='active'><a href='#a' data-toggle='tab'>A Journey Built On Trust</a></li>");
					print("<li><a href='#b' data-toggle='tab'>Client Proposition</a></li>");
					print("<li><a href='#c' data-toggle='tab'>Our Fees</a></li>");
				//	print("<li><a href='#d' data-toggle='tab'>Testimonials</a></li>");
				print ("</ul>");
				
				print("<ul class='nav nav-tabs tabs-collapse'>");
					print("<li class='active' style='width:25%;'><a href='#a' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
					print("<li style='width:25%;'><a href='#b' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
					print("<li style='width:25%;'><a href='#c' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
					print("<li style='width:25%;'><a href='#d' data-toggle='tab'><i class='fa fa-circle-o'></i></a></li>");
				print ("</ul>");
			
			break;
		
		}
		
	print("</div>");
print("</div>");
			
?>