<?php
 
	include("incsitecommon.php");
 
	//sanitize data
	$type = $_REQUEST['type'];
	$name = $_REQUEST['name'];
	$email = $_REQUEST['email'];
	$phone = $_REQUEST['phone'];
	$address = $_REQUEST['address'];
	$postcode = strtoupper($_REQUEST['postcode']);
	$property = $_REQUEST['property'];
	$date = $_REQUEST['date'];
	$time = $_REQUEST['time'];
	$message = $_REQUEST['message'];

	// insert form data into database	
/*	$strdbsql = "SELECT * FROM customer_callback WHERE fld_email = :email OR fld_phone = :phone";
	$resultdata = query($conn,$strdbsql,"multi",Array("phone" => $phone, "email" => $email));
	
	if(count($resultdata) > 0){
		
		print("error");
		
	} else {
		
		$strdbsql = "INSERT INTO customer_callback(fld_name, fld_email, fld_phone, fld_date) VALUES (:name, :email, :phone, UNIX_TIMESTAMP())";
		$resultdata = query($conn,$strdbsql,"insert",Array("name" => $name,"email" => $email,"phone" => $phone));
		
		print("success");
		
	}*/
	
	$to = "sales@rossestateagencies.co.uk";
	
	$subject = "Website - ".$type;
	
	$headers = "From: " . strip_tags("website@rossestateagencies.co.uk") . "\r\n";
	$headers .= "Reply-To: ". strip_tags($email) . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	
	$html = "<html>";
		$html .= "<head>";			
			$html .= "<style>";
				$html .= "body { background-color: #F1F1F1; margin: 0; font-family: arial; }";
				$html .= "a { color: #887777; }";
				$html .= "p { font-size: 14px; }";
				$html .= "strong.primary { color: #887777; }";
			$html .= "</style>";
		$html .= "</head>";
		$html .= "<body>";
			$html .= "<table width='100%' cellpadding='0' cellspacing='0'>";
				$html .= "<tr>";
					$html .= "<td>";
						$html .= "<table width='600px' align='center' cellpadding='0' cellspacing='0'>";
							$html .= "<thead>";
								$html .= "<tr>";
									$html .= "<td align='right'>";
										$html .= "<img width='100%' src='http://rossestateagencies.co.uk/images/emails/header.jpg'/>";
									$html .= "</td>";
								$html .= "</tr>";
							$html .= "</thead>";
							$html .= "<tbody>";
								$html .= "<tr>";
									$html .= "<td>";
										$html .= "<table width='100%' align='left' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF'>";
											$html .= "<tr>";
												$html .= "<td>";
													$html .= "<table width='510' align='left' cellpadding='45' cellspacing='0'>";
														$html .= "<tr>";
															$html .= "<td>";
																$html .= "<p>Ross Estate Agencies,</p>";
																$html .= "<p>You have <strong>1</strong> new &quot;".$type."&quot; from a visitor on your website. The details are shown below:</p>";
																$html .= "<p>";
																	if(!empty($name)) $html .= "<strong>Name</strong></br>".$name."<br/><br/>";
																	if(!empty($email)) $html .= "<strong>Email</strong><br/>".$email."<br/><br/>";
																	if(!empty($phone)) $html .= "<strong>Phone</strong><br/>".$phone."<br/><br/>";
																	if(!empty($address)) $html .= "<strong>Address</strong><br/>".$address."<br/><br/>";
																	if(!empty($postcode)) $html .= "<strong>Postcode</strong><br/>".$postcode."<br/><br/>";
																	if(!empty($property)) $html .= "<strong>Type of property</strong><br/>".$property."<br/><br/>";
																	if(!empty($date)) $html .= "<strong>Response Date</strong><br/>".$date."<br/><br/>";
																	if(!empty($time)) $html .= "<strong>What time of day is best for you</strong><br/>".$time."<br/><br/>";
																	if(!empty($message)) $html .= "<strong>Addition comments</strong><br/>".$message;
																$html .= "</p>";
															$html .= "</td>";
														$html .= "</tr>";
													$html .= "</table>";
												$html .= "</td>";
											$html .= "</tr>";
										$html .= "</table>";
									$html .= "</td>";
								$html .= "</tr>";
							$html .= "</tbody>";
							$html .= "<tfoot>";
								$html .= "<tr>";
									$html .= "<td align='right'>";
										$html .= "<img width='100%' src='http://rossestateagencies.co.uk/images/emails/footer.jpg'/>";
									$html .= "</td>";
								$html .= "</tr>";
							$html .= "</tfoot>";
						$html .= "</table>";
					$html .= "</td>";
				$html .= "</tr>";
			$html .= "</table>";
		$html .= "</body>";
	$html .= "</html>";
	
	mail($to, $subject, $html, $headers);
	
?>