<?php

	print("<div class='block-map'>");

		print("<div id='map'></div>");
		
		$googleKey = "AIzaSyAHmdh7fOKpDtp1zv5Rrc_T2YDfJ7TgT9k";
		
		$officelocations = array(
			array("name"=>"Barrow Office","postcode"=>"LA141RW","lat"=>"","lng"=>"")
		);
		
		if(count($officelocations) > 0) {
			$i = 0;
			foreach($officelocations AS $key => $officelocation) {
			//	print("https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($officelocation['postcode'])."&key=".$googleKey);
				$geolocation = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($officelocation['postcode'])."&key=".$googleKey);
				$mapaddress = json_decode($geolocation, true);
				if($mapaddress['status'] == 'OK') {
					if(isset($mapaddress['results'][0]['geometry']['location']['lat'])) {
						$officelocations[$key]['lat'] = $mapaddress['results'][0]['geometry']['location']['lat'];
					}
					if(isset($mapaddress['results'][0]['geometry']['location']['lng'])) {
						$officelocations[$key]['lng'] = $mapaddress['results'][0]['geometry']['location']['lng'];
					}
				}
			}
		}
		
		?>
		
		<script>
		function initMap() {
			var locations = [
				<?php
				if(count($officelocations) > 0) {
					foreach($officelocations AS $officelocation) {
						echo "['".$officelocation['name']."', ".$officelocation['lat'].", ".$officelocation['lng']."],";
					}
				}
				?>
			];
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 11,
				center: new google.maps.LatLng(<?php echo $officelocations[1]['lat']; ?>, <?php echo $officelocations[1]['lng']; ?>)
			});
			var infowindow = new google.maps.InfoWindow();
			var marker, i;
			for(i = 0; i < locations.length; i++) {
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(locations[i][1], locations[i][2]),
					map: map,
					icon: "/images/elements/marker-house.png"
				});
				google.maps.event.addListener(marker, 'click', (function(marker, i) {
					return function() {
						infowindow.setContent(locations[i][0]);
						infowindow.open(map, marker);
					}
				})(marker, i));
			}
		}
		</script>
		<script async src="https://maps.googleapis.com/maps/api/js?key=<?php echo $googleKey; ?>&callback=initMap"></script>
		
		<?php

	print("</div>");

?>