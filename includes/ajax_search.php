<?php

	include("incsitecommon.php");
	
	$bookConn = bookConnect();
	
	if(isset($_POST["strPage"])) {
		$strPage = $_POST['strPage'];
	} else {
		$strPage = "sales";
	}
	
	switch($strPage) {
		case "sales":
			$title = "properties for sale";
			$sql = "SELECT tbl_property.*, tbl_residentialSales.* FROM tbl_property
			INNER JOIN tbl_residentialSales ON tbl_property.fld_propertyID = tbl_residentialSales.fld_propertyID 
			WHERE tbl_property.fld_propertyID = :propID";
		break;
		case "rental":
			$title = "properties for rent";
			$sql = "SELECT tbl_property.*, tbl_residentialLet.*, tbl_residentialLetRentFrequency.fld_value AS frequency FROM tbl_property 
			INNER JOIN tbl_residentialLet ON tbl_property.fld_propertyID = tbl_residentialLet.fld_propertyID
			INNER JOIN tbl_residentialLetRentFrequency ON tbl_residentialLet.fld_rentFrequency = tbl_residentialLetRentFrequency.fld_counter
			WHERE tbl_property.fld_propertyID = :propID";
		break;
		case "commercial":
			$title = "commercial properties";
		break;
	}
	
	$strdbsql = $sql;
	$dataParams = array("propID"=>$_POST["id"]);
	$arrType = "multi";
	$resultdata = query($bookConn, $strdbsql, $arrType, $dataParams);
	
	if(!empty($resultdata)) {

		$i = 0;
	
		print("<div class='block-crit'>");
			print("<span>Showing <strong>1</strong> ".$title."</span>");
		print("</div>");
		
		foreach($resultdata AS $row) {
			
			$strdbsql = "SELECT * FROM tbl_propertyImages
			WHERE fld_propertyID = :propID ORDER BY fld_counter";
			$arrType = "single";
			$dataParams = array('propID' => $row['fld_propertyID']);
			$resultdata2 = query($bookConn, $strdbsql, $arrType, $dataParams);
			
			$propLink = str_replace(" ", "-", str_replace(",", "-", strtolower($row['fld_displayAddress'])));
			$propImage = str_replace("IMG_", "", substr($resultdata2['fld_image'], 0, strpos($resultdata2['fld_image'], '_', strpos($resultdata2['fld_image'], '_')+1)));
		
			if($i % 3 == 0) {
				print("<div class='row' style='margin-bottom:0;'>");
			}
				
				print("<div class='col-xs-12 col-sm-6 col-md-4'>");
	
					print("<div class='block-prop".($row['fld_featuredProperty'] == 1 ? " featured" : "")."'>");
						
						$strdbsql = "SELECT * FROM tbl_propertyImages
						WHERE fld_propertyID = :propID ORDER BY fld_counter
						LIMIT 4";
						$arrType = "multi";
						$dataParams = array('propID' => $row['fld_propertyID']);
						$resultdata2 = query($bookConn, $strdbsql, $arrType, $dataParams);
							
						print("<div id='carousel-".$i."' class='carousel slide' data-ride='carousel' data-interval='false'>");
							print("<div class='carousel-inner' role='listbox'>");

								if(count($resultdata2) > 0) {
							
									$x = 0;
							
									foreach($resultdata2 AS $row2) {
									
										$propImage = str_replace("IMG_", "", substr($row2['fld_image'], 0, strpos($row2['fld_image'], '_', strpos($row2['fld_image'], '_')+1)));
									
										print("<div class='item".($x == 0 ? " active" : "")."' style='background-image:url(http://media2.jupix.co.uk/v3/clients/1677/properties/".$propImage."/".$row2['fld_image'].");'>");
											print("<a href='/".$strPage."/".$row['fld_propertyID']."'></a>");
										print("</div>");
										
										$x++;
										
									}
									
								} else {
									
									print("<div class='item active' style='background-image:url(/datafeed/images/full/placeholder.jpg'>");
										print("<a href='/".$strPage."/".$row['fld_propertyID']."'></a>");
									print("</div>");
									
								}
							
								if(count($resultdata2)> 1) {
								
									print("<a class='left carousel-control' href='#carousel-".$i."' role='button' data-slide='prev'>");
										print("<span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>");
										print("<span class='sr-only'>Previous</span>");
									print("</a>");
									print("<a class='right carousel-control' href='#carousel-".$i."' role='button' data-slide='next'>");
										print("<span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>");
										print("<span class='sr-only'>Next</span>");
									print("</a>");
									
								}	
									
							print("</div>");
						print("</div>");
						
						print("<div class='block-info'>");
							print("<a class='head' href='/".$strPage."/".$row['fld_propertyID']."'>".str_replace(", ", " | ", $row['fld_displayAddress'])."</a>");
							
							switch($strPage) {
								case "sales":
									print("<span class='price'>&pound;".number_format($row['fld_price'])."</span>");
								break;
								case "rental":
									if($row['frequency'] == "pw") $frequency = "weekly";
									if($row['frequency'] == "pcm") $frequency = "monthly";
									if($row['frequency'] == "pa") $frequency = "yearly";
									print("<span class='price'>&pound;".number_format($row['fld_rent'])."/<span style='font-size:14px;'>".$frequency."</span></span>");
								break;
								case "commercial":
								//	Show property type here (office, tower etc...)
								//	print("<span class='price'>/<span style='font-size:14px;'>".$frequency."</span></span>");
								break;
							}
							
						print("</div>");
					print("</div>");
					
				print("</div>");
			
			if($i % 3 == 2) {
				print("</div>");
			}
			
			$i++;
			
		}
		
	} else {
		
		print("No properties matching your search criteria!");
	
	}

?>