<!--
<script type="text/javascript">	
function jsfindaddresswithfields (postcode,add1,add2,add3,town,county,addpaneID)
{
	//var addpaneID = "ajax_address";
	var addpane = document.getElementById(addpaneID );
	document.getElementById(postcode).value = document.getElementById(postcode).value.toUpperCase();
	var postcodeValue = document.getElementById(postcode).value;
	var request = "/includes/ajaxPostCodeSearch.php";
	var vars = "postcode="+postcodeValue;
	
	if(addpane == null) {
		var addpane = document.createElement('span');
		addpane.id = addpaneID ;
		var ownerNode = document.getElementById(map.b.id);
		ownerNode.parentNode.insertBefore(addpane, ownerNode.nextSibling);
	} else {
		var addpane = document.getElementById(addpaneID); 
		$("#"+addpane.id).show();
	}
	var params = "postcode="+postcodeValue;
	var xmlhttp;
	
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("POST",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var xml = xmlhttp.responseXML;
			if(xml.getElementsByTagName("ErrorNumber")[0].childNodes[0].nodeValue == 0) {
				if(xml.getElementsByTagName("Address2").length != 0)
				{
					document.getElementById(add2).value= xml.getElementsByTagName("Address2")[0].childNodes[0].nodeValue;
				}
				if(xml.getElementsByTagName("Address3").length != 0)
				{
					document.getElementById(add3).value= xml.getElementsByTagName("Address3")[0].childNodes[0].nodeValue;
				}
				
				var townName = xml.getElementsByTagName("Town")[0].childNodes[0].nodeValue;
				
				var res = townName.split("-");
				if(res.length == 3) {
					res[2] = res[2].charAt(0).toUpperCase() + res[2].slice(1);
					townName = res.join("-");
				}
				
				var countyName = xml.getElementsByTagName("County")[0].childNodes[0].nodeValue;

				var resultlist =  xml.getElementsByTagName("PremiseData")[0].childNodes[0].nodeValue;
				var arrlist = resultlist.split(";");
				var addlist = "<ul id='list'>";
				
				for (var i = 0; i < arrlist.length; i++) {
					if(arrlist[i] != "") {
						arrlist[i] = arrlist[i].replace("||","|");
						arrlist[i] = arrlist[i].replace("|",", ");
						arrlist[i] = arrlist[i].replace("|",", ");
						arrlist[i] = arrlist[i].replace(/  +/g, ' ');
						if(xml.getElementsByTagName("Address1").length > 0) var straddress =arrlist[i]+" "+xml.getElementsByTagName("Address1")[0].childNodes[0].nodeValue;
						else var straddress =arrlist[i]
						straddress = straddress.replace(/(^,)|(,$)/g, "");
						straddress = straddress.replace(/(^ )|(,$)/g, "");
						straddress = straddress.replace("<br>", " ");
						var csaddress = straddress.split(", ");
						
						if(typeof csaddress[1] == 'undefined')
						{
							csaddress[1] = "";
						}
						if(typeof csaddress[2] == 'undefined')
						{
							csaddress[2] = "";
						}
						csaddress[0] = csaddress[0].trim();
						csaddress[1] = csaddress[1].trim();
						csaddress[2] = csaddress[2].trim();
						
						/*csaddress[0].substring(0, 35);
						csaddress[1].substring(0, 35);
						csaddress[2].substring(0, 35);*/

						if(csaddress[0].length > 35) {
							var bits = csaddress[0].split(" ");
							csaddress[0] = "";
							var test = bits[0];
							
							while (test.length < 35) {
								csaddress[0] = test;
								bits.shift();
								test = csaddress[0]+ " "+bits[0];
								test = test.trim();
							}
							if (csaddress[2] == "" && csaddress[1].length < 35 && bits.join(" ").length < 35) {
								csaddress[2] = csaddress[1];
								csaddress[1] = bits.join(" ");
							} else {
								csaddress[1] = bits.join(" ") + ", " + csaddress[1];
								if(csaddress[1].length > 35) {
									var bits2 = csaddress[1].split(" ");
									csaddress[1] = "";
									var test2 = bits2[0];

									while (test2.length < 35) {
										csaddress[1] = test2;
										bits2.shift();

										test2 = csaddress[1]+ " "+bits2[0];
										test2 = test2.trim();
									}
									csaddress[2] = bits2.join(" ") + ", " + csaddress[2];
								}

							}
						}
						
						addlist += "<li class='listitem' onclick=\"javascript:jsaddselwithfields('"+csaddress[0].replace("'","\\'")+"','"+csaddress[1].replace("'","\\'")+"','"+csaddress[2].replace("'","\\'")+"','"+townName+"','"+countyName+"','"+addpaneID+"')\">"+straddress+"</li>";
					}
				}
				addlist += "<li class='listitem close' onclick='dismissAjaxAddress(\""+addpaneID+"\")'>×</li>";
				addlist += "</ul>";
				addpane.innerHTML = addlist;
				
				var childNodes = document.getElementById(postcode).parentNode.childNodes;
				for(var i = 0, j = childNodes.length; i < j; i++)
				{
					if((' ' + childNodes[i].className + ' ').indexOf(' manualToggle ') > -1) {
						childNodes[i].style.display = 'none';
					}
				}
			} else {
				document.getElementById(addpaneID).innerHTML = "<span class='notification-warning'>"+xml.getElementsByTagName("ErrorMessage")[0].childNodes[0].nodeValue+"<br/><span class='close' onclick='dismissAjaxAddress(\""+addpaneID+"\")'>×</span></span>";
			}

		} else if (xmlhttp.readyState==4) {
			document.getElementById(addpaneID).innerHTML = " <span class='notification-warning'>There has been an error finding your address please try again.</span> ";

		}
	}
	
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(vars);
	document.getElementById(addpaneID).innerHTML = "<span>Search...</span>";
	
}

function jsaddselwithfields(address1,address2,address3,town,county,addpaneID) {
	if(address1 != "") {
		address = address1;
	}
	else {
		address = "";
	}
	if(address2 != "")
	{
		address += ", "+address2;
	}
	else {
		address += "";
	}
	if(address3 != "")
	{
		address += ", "+address3;
	}
	else {
		address += "";
	}
	if(town != "")
	{
		address += ", "+town;
	}
	else {
		address += "";
	}
	if(county != "")
	{
		address += ", "+county;
	}
	else {
		address += "";
	}
	
	document.getElementById("val_address").value = address;
	document.getElementById(addpaneID).style.display = "none";
}

function dismissAjaxAddress(addpaneID)
{
	document.getElementById(addpaneID).style.display = "none";
}
</script>-->

<?php

	print("<div class='block-form empty' style='padding-top:0;'>");
		print("<div class='container'>");
		
		/*	print("<form id='valuation-form' class='add-decor' action='?action=valuation' method='post'>");
				print("<h3>Book your free valuation</h3>");				
				print("<div class='row' style='margin-bottom:0;'>");
					print("<div class='valuation-con val_name col-sm-4'>");
						print("<input type='text' name='val_name' id='val_name' placeholder='Name*' />");
						print("<div class='error-icon'><i class='fa fa-times' aria-hidden='true'></i></div>");
					print("</div>");
					print("<div class='valuation-con val_email col-sm-4'>");	
						print("<input type='email' name='val_email' id='val_email' placeholder='Email*' />");
						print("<div class='error-icon'><i class='fa fa-times' aria-hidden='true'></i></div>");
					print("</div>");
					print("<div class='valuation-con val_phone col-sm-4'>");
						print("<input type='number' name='val_phone' id='val_phone' placeholder='Phone*' />");
						print("<div class='error-icon'><i class='fa fa-times' aria-hidden='true'></i></div>");
					print("</div>");
				print("</div>");
				print("<div class='row' style='margin-bottom:0;'>");
					print("<div class='valuation-con val_postcode col-sm-4'>");
						print("<input class='find' type='text' name='PostZipCode' id='PostZipCode' maxlength='7' placeholder='Postcode*' />");
						print("<div class='find error-icon'><i class='fa fa-times' aria-hidden='true'></i></div>");
						print("<div class='find-button' onclick='jsfindaddresswithfields(\"PostZipCode\",\"address1\",\"address2\",\"address3\",\"TownCity\",\"CountyState\", \"ajax_address\")' id='lookupbutton'>Find Property <i class='fa fa-chevron-right' aria-hidden='true'></i></div>");
						print("<div id='ajax_address' class='ajax_address'></div>");
					print("</div>");
					print("<div class='valuation-con val_address col-sm-8'>");
						print("<input type='text' name='val_address1' id='val_address' placeholder='Address*' />");
						print("<div class='error-icon'><i class='fa fa-times' aria-hidden='true'></i></div>");
					print("</div>");
				print("</div>");
				print("<div class='row' style='margin-bottom:0;'>");
					print("<div class='valuation-con val_property col-sm-6'>");	
						print("<div class='dropdown default'>");
							print("<button class='btn btn-default dropdown-toggle select-default' type='button' id='val_property' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>");
								print("<span>Type of Property*</span>");
								print("<i class='fa fa-chevron-down' aria-hidden='true'></i>");
							print("</button>");
							print("<ul class='dropdown-menu' aria-labelledby='val_property'>");
								print("<li>Sales</li>");
								print("<li>Rental</li>");
								print("<li>Commercial</li>");
							print("</ul>");
						print("</div>");
						print("<div class='error-icon'><i class='fa fa-times' aria-hidden='true'></i></div>");
					print("</div>");
					print("<div class='valuation-con val_time col-sm-6'>");	
						print("<div class='dropdown default'>");
							print("<button class='btn btn-default dropdown-toggle select-default' type='button' id='val_time' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>");
								print("<span>What time of day is best for you?*</span>");
								print("<i class='fa fa-chevron-down' aria-hidden='true'></i>");
							print("</button>");
							print("<ul class='dropdown-menu' aria-labelledby='val_time'>");
								print("<li>Morning</li>");
								print("<li>Afternoon</li>");
								print("<li>Evening</li>");
							print("</ul>");
						print("</div>");
						print("<div class='error-icon'><i class='fa fa-times' aria-hidden='true'></i></div>");
					print("</div>");					
				print("</div>");
				print("<div class='row' style='margin-bottom:0;'>");
					print("<div class='valuation-con val_msg col-xs-12'>");	
						print("<textarea name='val_msg' id='val_msg' placeholder='Type in here'></textarea>");
					print("</div>");
				print("</div>");
				print("<div class='row' style='margin-bottom:0;'>");
					print("<div class='valuation-con col-xs-12'>");	
						print("<button type='submit' id='val_submit'>Submit <i class='fa fa-chevron-right' aria-hidden='true'></i></button>");
					print("</div>");
				print("</div>");
			print("</form>");*/
		
		print("</div>");
		
		print("<iframe class='val-iframe' scrolling='yes' src='https://nethouseprices.com/widget/sold-prices/?id=A598DCE2DB382F'></iframe>");
		
	print("</div>");
	
	
?>