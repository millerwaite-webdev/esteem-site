<?php

	include("incsitecommon.php");
	
	$bookConn = bookConnect();
	
	$item_per_page = 21; //item to display per page
	
	if(isset($_POST["page"])){
		$page_number = $_POST{"page"};
	}else{
		$page_number = 1;
	}
	
	if(isset($_POST["sort"])){
		$result_sort = $_POST{"sort"};
	}else{
		$result_sort = "high-low";
	}
	
	if(isset($_POST["strPage"])) {
		$strPage = $_POST['strPage'];
	} else {
		$strPage = "sales";
	}
	
	switch($strPage) {
		case "sales":
			$title = "properties for sale";
			$department = "Sales";
			$sql = "SELECT tbl_property.*, tbl_residentialSales.* FROM tbl_property
			INNER JOIN tbl_residentialSales ON tbl_property.fld_propertyID = tbl_residentialSales.fld_propertyID
			WHERE tbl_property.fld_department = :department AND tbl_residentialSales.fld_availability <> 4 AND tbl_residentialSales.fld_availability <> 5";
		break;
		case "rental":
			$title = "properties for rent";
			$department = "Lettings";
			$sql = "SELECT tbl_property.*, tbl_residentialLet.* FROM tbl_property 
			INNER JOIN tbl_residentialLet ON tbl_property.fld_propertyID = tbl_residentialLet.fld_propertyID
			WHERE tbl_property.fld_department = :department AND tbl_residentialLet.fld_availability <> 4";
		break;
		case "commercial":
			$title = "commercial properties";
			$department = "Commercial";
			$sql = "SELECT tbl_property.*, tbl_commercial.* FROM tbl_property 
			INNER JOIN tbl_commercial ON tbl_property.fld_propertyID = tbl_commercial.fld_propertyID
			WHERE tbl_property.fld_department = :department AND tbl_commercial.fld_availability = 2";
		break;
	}
	
	//get total number of records from database
	$strdbsql = $sql;
	$arrType = "multi";
	$dataParams = array("department" => $department);
	$resultdata = query($bookConn, $strdbsql, $arrType, $dataParams);
	
	$get_total_rows = count($resultdata); //hold total records in variable
	//break records into pages
	$total_pages = ceil($get_total_rows/$item_per_page);

	//position of records
	$page_position = (($page_number-1) * $item_per_page);
	
	//Limit our results within a specified range
	switch($strPage) {
		case "sales":
			if($result_sort == "low-high") { $result_query = "ORDER BY tbl_residentialSales.fld_price ASC";	}
			if($result_sort == "high-low") { $result_query = "ORDER BY tbl_residentialSales.fld_price DESC"; }
			if($result_sort == "new-old") { $result_query = "ORDER BY tbl_property.fld_dateLastModified DESC"; }
			if($result_sort == "old-new") { $result_query = "ORDER BY tbl_property.fld_dateLastModified ASC"; }
			$strdbsql = "SELECT tbl_property.*, tbl_residentialSales.*, tbl_residentialSaleAvailability.fld_value AS availability FROM tbl_property
			INNER JOIN tbl_residentialSales ON tbl_property.fld_propertyID = tbl_residentialSales.fld_propertyID 
			INNER JOIN tbl_residentialSaleAvailability ON tbl_residentialSales.fld_availability = tbl_residentialSaleAvailability.fld_counter
			WHERE tbl_property.fld_department = :department AND tbl_residentialSales.fld_availability <> 4 AND tbl_residentialSales.fld_availability <> 5 
			$result_query
			LIMIT $page_position, $item_per_page";
		break;
		case "rental":
			if($result_sort == "low-high") { $result_query = "ORDER BY tbl_residentialLet.fld_rent ASC";	}
			if($result_sort == "high-low") { $result_query = "ORDER BY tbl_residentialLet.fld_rent DESC"; }
			if($result_sort == "new-old") { $result_query = "ORDER BY tbl_property.fld_dateLastModified DESC"; }
			if($result_sort == "old-new") { $result_query = "ORDER BY tbl_property.fld_dateLastModified ASC"; }
			$strdbsql = "SELECT tbl_property.*, tbl_residentialLet.*, tbl_residentialLetRentFrequency.fld_value AS frequency, tbl_residentialLetAvailability.fld_value AS availability FROM tbl_property 
			INNER JOIN tbl_residentialLet ON tbl_property.fld_propertyID = tbl_residentialLet.fld_propertyID
			INNER JOIN tbl_residentialLetRentFrequency ON tbl_residentialLet.fld_rentFrequency = tbl_residentialLetRentFrequency.fld_counter
			INNER JOIN tbl_residentialLetAvailability ON tbl_residentialLet.fld_availability = tbl_residentialLetAvailability.fld_counter
			WHERE tbl_property.fld_department = :department AND tbl_residentialLet.fld_availability <> 4 
			$result_query
			LIMIT $page_position, $item_per_page";
		break;
		case "commercial":
			// Add new filter to find different between for sale and to let
			// This filter will always have a value as for sale or to let
			// Then an if statment can be please in the if statement below to filter for price or price per month
			if($result_sort == "low-high") { $result_query = "ORDER BY tbl_commercial.fld_priceTo ASC";	}
			if($result_sort == "high-low") { $result_query = "ORDER BY tbl_commercial.fld_priceTo DESC"; }
			if($result_sort == "new-old") { $result_query = "ORDER BY tbl_property.fld_dateLastModified DESC"; }
			if($result_sort == "old-new") { $result_query = "ORDER BY tbl_property.fld_dateLastModified ASC"; }
			$strdbsql = "SELECT tbl_property.*, tbl_commercial.*, tbl_commAvailability.fld_value AS availability FROM tbl_property	
			INNER JOIN tbl_commercial ON tbl_property.fld_propertyID = tbl_commercial.fld_propertyID 
			INNER JOIN tbl_commAvailability ON tbl_commercial.fld_availability = tbl_commAvailability.fld_counter 
			WHERE tbl_property.fld_department = :department AND tbl_commercial.fld_availability = 2
			$result_query
			LIMIT $page_position, $item_per_page";
		break;
	}
	
	$arrType = "multi";
	$dataParams = array('department' => $department);
	$resultdata = query($bookConn, $strdbsql, $arrType, $dataParams);
					
	if(!empty($resultdata)) {

		$i = 0;
	
		print("<div class='block-crit'>");
			print("<span>Showing <strong>".$get_total_rows."</strong> ".$title.". Page <strong>".$page_number."</strong> of <strong>".$total_pages."</strong></span>");
			print("<div class='dropdown' id='sort-drop'>");
				print("<button class='btn btn-primary dropdown-toggle' type='button' data-toggle='dropdown'>");
					print("<span>");
					
						switch($result_sort) {
							case "low-high":
								print("Sort by price: low to high");
							break;
							case "high-low":
								print("Sort by price: high to low");
							break;
							case "new-old":
								print("Sort by date: newest to oldest");
							break;
							case "old-new":
								print("Sort by date: oldest to newest");
							break;
						}
						
					print("</span>");
					print("<i class='fa fa-caret-down' aria-hidden='true'></i>");
				print("</button>");
				print("<ul class='dropdown-menu'>");
					print("<li data-sort='low-high'".($result_sort == "low-high" ? " class='selected'" : "").">Sort by price: low to high</li>");
					print("<li data-sort='high-low'".($result_sort == "high-low" ? " class='selected'" : "").">Sort by price: high to low</li>");
					print("<li data-sort='new-old'".($result_sort == "new-old" ? " class='selected'" : "").">Sort by date: newest to oldest</li>");
					print("<li data-sort='old-new'".($result_sort == "old-new" ? " class='selected'" : "").">Sort by date: oldest to newest</li>");
				print("</ul>");
			print("</div>");
		print("</div>");
		
		foreach($resultdata AS $row) {
		
			if($i % 3 == 0) {
				print("<div class='row' style='margin-bottom:0;'>");
			}
				
				print("<div class='col-xs-12 col-sm-6 col-md-4'>");
	
					print("<div class='block-prop".($row['fld_featuredProperty'] == 1 ? " featured" : "")."'>");
						
						switch($row['availability']) {
							case "On Hold":
								$colour = "#9e9e9e";
								break;
							case "For Sale":
							case "To Let":
								$colour = "";
								break;
							case "Under Offer":
							case "References Pending":
								$colour = "#f4b136";
								break;
							case "Sold STC":
							case "Sold":
							case "Let Agreed":
								$colour = "#f44336";
								break;
							case "Withdrawn":
								$colour = "#9e9e9e";
								break;
						}
						
						print("<span class='status' style='background-color:".$colour.";'>".($row['availability'] == "To Let" || $row['availability'] == "Let" ? "For Let" : $row['availability'])."</span>");
						
						$strdbsql = "SELECT * FROM tbl_propertyImages
						WHERE fld_propertyID = :propID ORDER BY fld_counter
						LIMIT 4";
						$arrType = "multi";
						$dataParams = array('propID' => $row['fld_propertyID']);
						$resultdata2 = query($bookConn, $strdbsql, $arrType, $dataParams);
							
						print("<div id='carousel-".$i."' class='carousel slide' data-ride='carousel' data-interval='false'>");
							print("<div class='carousel-inner' role='listbox'>");

								if(count($resultdata2) > 0) {
							
									$x = 0;
							
									foreach($resultdata2 AS $row2) {
									
										$propImage = str_replace("IMG_", "", substr($row2['fld_image'], 0, strpos($row2['fld_image'], '_', strpos($row2['fld_image'], '_')+1)));
									
										print("<div class='item".($x == 0 ? " active" : "")."' style='background-image:url(http://media2.jupix.co.uk/v3/clients/1677/properties/".$propImage."/".$row2['fld_image'].");'>");
											print("<a href='/".$strPage."/".$row['fld_propertyID']."'></a>");
										print("</div>");
										
										$x++;
										
									}
									
								} else {
									
									print("<div class='item active' style='background-color:white;background-image:url(/datafeed/images/full/placeholder.jpg);background-size:contain;'>");
										print("<a href='/".$strPage."/".$row['fld_propertyID']."'></a>");
									print("</div>");
									
								}
									
							print("</div>");
							
							if(count($resultdata2)> 1) {
								
								print("<a class='left carousel-control' href='#carousel-".$i."' role='button' data-slide='prev'>");
									print("<span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>");
									print("<span class='sr-only'>Previous</span>");
								print("</a>");
								print("<a class='right carousel-control' href='#carousel-".$i."' role='button' data-slide='next'>");
									print("<span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>");
									print("<span class='sr-only'>Next</span>");
								print("</a>");
								
							}
							
						print("</div>");
						
						print("<div class='block-info'>");
							print("<a class='head' href='/".$strPage."/".$row['fld_propertyID']."'>");
							
								print("<span>");
									if(!empty($row['fld_addressName'])) print($row['fld_addressName'].", ");
									if(!empty($row['fld_addressNumber'])) print($row['fld_addressNumber']." ");
									if(!empty($row['fld_addressStreet'])) print($row['fld_addressStreet'].", ");
									if(!empty($row['fld_address2'])) print($row['fld_address2'].", ");
								print("</span>");
								if(!empty($row['fld_address3'])) print("<span>".$row['fld_address3']."</span>");
								if(!empty($row['fld_addressPostcode'])) print("<span class='postCode'>".$row['fld_addressPostcode']."</span>");
							
								switch($strPage) {
									case "sales":
										print("<span class='price'>&pound;".number_format($row['fld_price'])."</span>");
									break;
									case "rental":
										if($row['frequency'] == "pw") $frequency = "weekly";
										if($row['frequency'] == "pcm") $frequency = "monthly";
										if($row['frequency'] == "pa") $frequency = "yearly";
										print("<span class='price'>&pound;".number_format($row['fld_rent'])."/<span style='font-size:14px;'>".$frequency."</span></span>");
									break;
									case "commercial":
										print("<span class='price'>");
											if($row['fld_forSale'] == 1) { $price = $row['fld_priceTo']; }
											else {
												$price = $row['fld_rentTo'];
												if($row['fld_rentFrequency'] == "pw") $frequency = "/weekly";
												if($row['fld_rentFrequency'] == "pcm") $frequency = "/monthly";
												if($row['fld_rentFrequency'] == "pa") $frequency = "/yearly";
											}
											print("&pound;".number_format($price));
											print("<span style='font-size:14px;'>".$frequency."</span>");
										print("</span>");
									break;
								}
								
							print("</a>");
							
						print("</div>");
					print("</div>");
					
				print("</div>");
			
			if($i % 3 == 2) {
				print("</div>");
			}
			
			$i++;
			
		}
		
		print(paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages));
		
	} else {
		
		print("No properties matching your search criteria!");
	
	}
	
	?>
	
	<script type="text/javascript">
	$("#sort-drop .dropdown-menu li").click(function(){
		$('#sort-drop span').text($(this).text());
		$('.selected').removeClass('selected');
		$(this).addClass('selected');
	});
	</script>
	
	<?php
	
?>