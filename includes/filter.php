<?php

	//	FILTER
	print("<div class='block-list filter-side'>");
		print("<h5>Filter Properties</h5>");
		
		print("<div class='jplist-panel jplist-panel-".$strPage."'>");
		
			print("<div class='row' style='margin-bottom:0;'>");
				print("<div class='col-xs-12 col-sm-6 col-md-12'>");
					print("<label>Sort:</label>");
					
				/*	print("<select class='jplist-select' data-control-type='sort-select' data-control-name='sort' data-control-action='sort' data-datetime-format='{year}-{month}-{day}'>");
						print("<option data-path='default'>Any</option>");
						print("<option data-path='.price-".$strPage."' data-order='asc' data-type='number'>Price: low to high</option>");
						print("<option data-path='.price-".$strPage."' data-order='desc' data-type='number' data-default='true'>Price: high to low</option>");
						print("<option data-path='.date-time' data-order='desc' data-type='datetime'>Date: newest to oldest</option>");
						print("<option data-path='.date-time' data-order='asc' data-type='datetime'>Date: oldest to newest</option>");
					print("</select>");*/
					
					print("<div class='dropdown sort-dd' data-control-type='boot-sort-drop-down' data-control-name='bootstrap-sort-dropdown-demo' data-control-action='sort' data-datetime-format='{year}-{month}-{day}'>");
						print("<button class='btn btn-default dropdown-toggle' type='button' id='sort-dropdown-menu' data-toggle='dropdown' aria-expanded='true'>");
							print("<span data-type='selected-text'>Sort by</span>");
							print("<span class='caret' style='margin:0 15px;'></span>");
						print("</button>");
						print("<ul class='dropdown-menu' role='menu' aria-labelledby='sort-dropdown-menu'>");
							print("<li role='presentation'>");
								print("<a role='menuitem' tabindex='-1' href='#' data-path='.price-".$strPage."' data-order='asc' data-type='number'>Price: low to high</a>");
							print("</li>");
							print("<li role='presentation'>");
								print("<a role='menuitem' tabindex='-1' href='#' data-path='.price-".$strPage."' data-order='desc' data-type='number' data-default='true'>Price: high to low</a>");
							print("</li>");
							print("<li role='presentation'>");
								print("<a role='menuitem' tabindex='-1' href='#' data-path='.date-time' data-order='desc' data-type='datetime'>Date: newest to oldest</a>");
							print("</li>");
							print("<li role='presentation'>");
								print("<a role='menuitem' tabindex='-1' href='#' data-path='.date-time' data-order='asc' data-type='datetime'>Date: oldest to newest</a>");
							print("</li>");
						print("</ul>");
					print("</div>");
				print("</div>");
			/*	print("<div class='col-xs-12 col-sm-6 col-md-12 hide'>");
		
					//	GET ADDRESSES OF ALL PROPERTIES AND POPULATE DROPDOWN
					$strdbsql = "SELECT fld_value AS typeName, fld_counter AS typeID FROM tbl_residentialPropertyType";
					$result = query($bookConn,$strdbsql,"multi");
					
					if(count($result) > 0) {
						print("<label for='type'>Type:</label>");
						print("<select class='jplist-select' name='type' data-control-type='filter-select' data-control-name='type' data-control-action='filter'>");
							print("<option data-path='default'>Any</option>");
							foreach($result AS $row) {
								print("<option data-path='.".$row['typeID']."' data-default='true'>".$row['typeName']."</option>");
							}
						print("</select>");
					}
						
				print("</div>");*/
				print("<div class='col-xs-12 col-sm-6 col-md-12'>");
		
					//	GET ADDRESSES OF ALL PROPERTIES AND POPULATE DROPDOWN
					if($strPage == "rental") $department = "Lettings";
					else if($strPage == "commercial") $department = "Commercial";
					else  $department = "Sales";
				
					$strdbsql = "SELECT DISTINCT(fld_address3) FROM tbl_property
					WHERE fld_department = :department ORDER BY fld_address3 ASC";
					$arrType = "multi";
					$dataParams = array("department" => $department);
					$resultdata = query($bookConn, $strdbsql, $arrType, $dataParams);
					
					if(count($resultdata) > 0) {

						print("<label ".($strsearch ? "class='hide'" : "")." for='town'>Location (Town):</label>");
						print("<select class='jplist-select ".($strsearch ? "hide" : "")."' name='address' data-control-type='filter-select' data-control-name='address' data-control-action='filter'>");
							
							print("<option data-path='default'>Any</option>");
							
							$search = $strsearch;
							
							// Get correct searched town
							switch($search) {
								case "askam": $search = "askam-in-furness"; break;
								case "barrow": $search = "barrow-in-furness"; break;
								case "broughton": $search = "broughton-in-furness"; break;
								case "dalton": $search = "dalton-in-furness"; break;
								case "urswick": $search = "great urswick"; break;
								case "kirkby": $search = "kirkby-in-furness"; break;
								case "lowick": $search = "lowick green"; break;
								case "newton": $search = "newton-in-furness"; break;
								case "stainton": $search = "stainton with adgarley"; break;
							}

							$addresses = array();
							
							foreach($resultdata AS $row) {
								$address = strtolower($row['fld_address3']);
								$addCut = explode(" ", str_replace("-", " ", $address));
								$address = $addCut[0];
								if(!in_array($address, $addresses) && $row['fld_address3'] != "") $addresses[] = $address;
							}
							
							$strdbsql = "SELECT fld_label, fld_urlkey FROM tbl_location WHERE UPPER(fld_postcode) LIKE :search OR UPPER(fld_urlkey) LIKE :search";
							$result = query($bookConn,$strdbsql,"single",array("search"=>strtoupper($search)));
							
							// Get correct searched town
							switch($result['fld_urlkey']) {
								case "askam-in-furness": $test = "askam"; break;
								case "barrow-in-furness": $test = "barrow"; break;
								case "broughton-in-furness": $test = "broughton"; break;
								case "dalton-in-furness": $test = "dalton"; break;
								case "great urswick": $test = "urswick"; break;
								case "kirkby-in-furness": $test = "kirkby"; break;
								case "lowick green": $test = "lowick"; break;
								case "newton-in-furness": $test = "newton"; break;
								case "stainton with adgarley": $test = "stainton"; break;
								default: $test = $result['fld_urlkey']; break;
							}
							
							if(in_array($test, $addresses) || $search == "") {
								foreach($addresses AS $address) {
									switch($address){
										case "askam": $address = "askam-in-Furness"; break;
										case "barrow": $address = "barrow-in-Furness"; break;
										case "broughton": $address = "broughton-in-Furness"; break;
										case "dalton": $address = "dalton-in-Furness"; break;
										case "great": $address = "great urswick"; break;
										case "kirkby": $address = "kirkby-in-Furness"; break;
										case "lowick": $address = "lowick green"; break;
										case "newton": $address = "newton-in-Furness"; break;
										case "stainton": $address = "stainton with adgarley"; break;
									}
									if($address) print("<option data-path='.".strtolower($address)."' ".(strtolower($address) == strtolower($result['fld_urlkey']) ? "data-default='true'" : "").">".ucwords($address)."</option>");
									else print("<option data-path='.".strtolower($address)."'>".ucwords($address)."</option>");
								}
							} else {
								if(isPostcodeValid($search)) {
									$account = "3215";
									$password = "exrf95ev";
									$url = "http://ws1.postcodesoftware.co.uk/lookup.asmx/getAddress?account=".$account."&password=".$password."&postcode=".$search;
									$xml = simplexml_load_file(str_replace(' ','', $url));

									if($xml->ErrorNumber == 0) $search = $xml->Town;
									else $search = "";
								}
								print("<option data-path='.".$search."' data-default='true'>".ucwords($search)."</option>");
							}
							
						print("</select>");
					
					}
						
				print("</div>");
			print("</div>");
			
			//	NUMBER OF BEDROOMS FILTER
			if($strPage != "commercial") {
				print("<label>No. of Bedrooms</label>");
				print("<div class='row' style='margin-bottom:0;'>");
					print("<div class='col-xs-6'>");
						print("<select class='jplist-select ' data-control-type='filter-select' data-control-name='bedroomsMin' data-control-action='filter'>");
							print("<option data-path='default'>Min</option>");
							print("<option data-path='.1'>1</option>");
							print("<option data-path='.2'>2</option>");
							print("<option data-path='.3'>3</option>");
							print("<option data-path='.4'>4</option>");
							print("<option data-path='.5'>5</option>");
						print("</select>");
					print("</div>");
					print("<div class='col-xs-6'>");
						print("<select class='jplist-select ' data-control-type='filter-select' data-control-name='bedroomsMax' data-control-action='filter'>");
							print("<option data-path='default'>Max</option>");
							print("<option data-path='.1'>1</option>");
							print("<option data-path='.2'>2</option>");
							print("<option data-path='.3'>3</option>");
							print("<option data-path='.4'>4</option>");
							print("<option data-path='.5'>5</option>");
						print("</select>");
					print("</div>");
				print("</div>");
			}
			
			//	STATUS FILTER (SALE/RENT)
			if($strPage == "commercial") {
				print("<div style='display:inline-block;margin:0 0 15px;width:100%;'>");
					print("<div>");
						print("<input class='value-type' data-control-type='radio-buttons-filters' data-control-action='filter' data-control-name='forSale' data-path='.for-sale' id='for-sale' type='radio' name='jplist1' checked='checked' /> ");
						print("<label for='for-sale'>For Sale</label>");
					print("</div>");
					print("<div class='pull-right'>");
						print("<input class='value-type' data-control-type='radio-buttons-filters' data-control-action='filter' data-control-name='forRent' data-path='.for-rent' id='for-rent' type='radio' name='jplist1' /> ");
						print("<label for='for-rent'>For Rent</label>");
					print("</div>");
				print("</div>");
			}
			
			//	PRICE FILTER
			if($strPage == "sales" || $strPage == "search") {
				$min = 50000;
				$max = 1000000;
				$increment = 25000;
				
				$prices = array();
				
				for($i = $min; $i <= $max; $i += $increment) {
					array_push($prices, $i);
				}
			
				print("<label>Price Range (&pound;)</label>");
				print("<div class='row' style='margin-bottom:0;'>");
					print("<div class='col-xs-6'>");
						print("<select class='jplist-select ' data-control-type='filter-select' data-control-name='salesPriceMin' data-control-action='filter'>");
							print("<option data-path='default'>Min</option>");
							foreach($prices AS $price) {
								print("<option data-path='.".$price."'>".number_format($price)."</option>");
							}
						print("</select>");
					print("</div>");
					print("<div class='col-xs-6'>");
						print("<select class='jplist-select ' data-control-type='filter-select' data-control-name='salesPriceMax' data-control-action='filter'>");
							print("<option data-path='default'>Max</option>");
							foreach($prices AS $price) {
								print("<option data-path='.".$price."'>".number_format($price)."</option>");
							}
						print("</select>");
					print("</div>");
				print("</div>");
			}
			
			if($strPage == "rental") {
				$min = 100;
				$max = 5000;
				$increment = 100;
				
				$prices = array();
				
				for($i = $min; $i <= $max; $i += $increment) {
					array_push($prices, $i);
				}
				
				print("<label>Price Range (&pound;)</label>");
				print("<div class='row' style='margin-bottom:0;'>");
					print("<div class='col-xs-6'>");
						print("<select class='jplist-select ' data-control-type='filter-select' data-control-name='rentalPriceMin' data-control-action='filter'>");
							print("<option data-path='default'>Min</option>");
							foreach($prices AS $price) {
								print("<option data-path='.".$price."'>".number_format($price)."</option>");
							}
						print("</select>");
					print("</div>");
					print("<div class='col-xs-6'>");
						print("<select class='jplist-select ' data-control-type='filter-select' data-control-name='rentalPriceMax' data-control-action='filter'>");
							print("<option data-path='default'>Max</option>");
							foreach($prices AS $price) {
								print("<option data-path='.".$price."'>".number_format($price)."</option>");
							}
						print("</select>");
					print("</div>");
				print("</div>");
			}
			
			if($strPage == "commercial") {
				$min = 50000;
				$max = 1000000;
				$increment = 25000;
				
				$prices = array();
				
				for($i = $min; $i <= $max; $i += $increment) {
					array_push($prices, $i);
				}
			
				print("<label>Price Range (&pound;)</label>");
				print("<div class='row' style='margin-bottom:0;'>");
					print("<div class='col-xs-6'>");
						print("<select class='jplist-select ' data-control-type='filter-select' data-control-name='commercialPriceMin' data-control-action='filter'>");
							print("<option data-path='default'>Min</option>");
							foreach($prices AS $price) {
								print("<option data-path='.".$price."'>".number_format($price)."</option>");
							}
						print("</select>");
					print("</div>");
					print("<div class='col-xs-6'>");
						print("<select class='jplist-select ' data-control-type='filter-select' data-control-name='commercialPriceMax' data-control-action='filter'>");
							print("<option data-path='default'>Max</option>");
							foreach($prices AS $price) {
								print("<option data-path='.".$price."'>".number_format($price)."</option>");
							}
						print("</select>");
					print("</div>");
				print("</div>");
			}
			
			//	SSTC FILTER
			if($strPage == "sales" || $strPage == "search") print("<span><strong>Include SSTC?</strong></span>");
			if($strPage == "rental") print("<span><strong>Let Agreed</strong></span>");
			if($strPage == "commercial") print("<span><strong>Include SSTC/Let Agreed</strong></span>");
			print("<div class='squaredThree'>");
				print("<input type='checkbox' value='None' id='sstc' name='check'>");
				print("<label for='sstc'></label>");
			print("</div>");

			//	HIDDEN SSTC RADIO (NEEDED FOR SSTC CHECKBOX)
			print("<div style='display:none;'>");
				print("<div>");
					print("<input style='display:none;' data-control-type='radio-buttons-filters' data-control-action='filter' data-control-name='sstcoff-".$strPage."' data-path='.normal-".$strPage."' id='sstc-off' type='radio' name='jplist2' checked='checked' /> ");
					print("<input style='display:none;' data-control-type='radio-buttons-filters' data-control-action='filter' data-control-name='sstcon-".$strPage."' data-path='.all-".$strPage."' id='sstc-on' type='radio' name='jplist2' /> ");
				print("</div>");
			print("</div>");
			
		//	print("<button id='filterButton' class='pull-right' style='margin-right:0;'>Filter <i class='fa fa-chevron-right' aria-hidden='true'></i></button>");
			
		print("</div>");
	
	print("</div>");

?>