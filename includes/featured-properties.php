<?php
	
	$bookConn = bookConnect();
	
	if(isset($_GET['page'])) $strPage = $_GET['page'];
	else $strPage = 'index';
	
	switch($strPage) {
		case "sales":
			$department = "Sales";
		break;
		case "rental":
			$department = "Lettings";
		break;
		case "commercial":
			$department = "Commercial";
		break;
	}
	
	$strdbsql = "SELECT * FROM tbl_property	
	INNER JOIN tbl_propertyImages ON tbl_property.fld_propertyID = tbl_propertyImages.fld_propertyID
	WHERE fld_department = :department AND fld_featuredProperty = 1";
	$arrType = "multi";
	$dataParams = array('department' => $department);
	$resultdata = query($bookConn, $strdbsql, $arrType, $dataParams);
	
	print("<div class='block-text add-decor'>");
		print("<div class='container'>");
			print("<h3>Featured Properties</h3>");
		print("</div>");
	print("</div>");
			
	$i = 0;
	
	print("<div class='block-props'>");
		print("<div class='container'>");
			
			foreach($resultdata AS $row) {
			
				if($i % 4 == 0) {
					print("<div class='row'>");
				}
					
					print("<div class='col-xs-12 col-sm-6 col-md-3'>");
		
						print("<div class='block-prop'>");
							print("<a href='/property/' class='block-cover'><i class='fa fa-search' aria-hidden='true'></i></a>");
							print("<div class='block-img' style='background-image:url(".$row['fld_image'].");'></div>");
							print("<div class='block-info'>");
								print("<span class='head'>".$row['fld_displayAddress']."</span>");
							print("</div>");
						print("</div>");
						
					print("</div>");
				
				if($i % 4 == 3) {
					print("</div>");
				}
		
				if(++$i == 28) break;
				
			}
			
		print("</div>");
	print("</div>");

?>