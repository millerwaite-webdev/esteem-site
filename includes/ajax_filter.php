<?php

	include("incsitecommon.php");
	
	$bookConn = bookConnect();
	
	if(isset($_POST['strPage'])) $strPage = $_POST['strPage']; else $strPage = "sales";
	if(isset($_POST["sort"])) $result_sort = $_POST{"sort"}; else $result_sort = "high-low";
	
	$strtown = $_POST['town'];
	if($strtown == "Any") { $strtown = '%'; }
	
	if($_POST["status"] == 1)$status = "1"; else $status = "0";
	
	// Need to apply filter for commercial (if for sale or for lease)
	
	switch($strPage) {
		case "sales":
			if($_POST["stc"] == "on") $stc = "IS NOT NULL"; else $stc = "<> 4 AND tbl_residentialSales.fld_availability <> 5";
			if($result_sort == "low-high") { $result_query = "ORDER BY tbl_residentialSales.fld_price ASC";	}
			if($result_sort == "high-low") { $result_query = "ORDER BY tbl_residentialSales.fld_price DESC"; }
			if($result_sort == "new-old") { $result_query = "ORDER BY tbl_property.fld_dateLastModified DESC"; }
			if($result_sort == "old-new") { $result_query = "ORDER BY tbl_property.fld_dateLastModified ASC"; }
			$title = "properties for sale";
			$query = "SELECT tbl_property.*, tbl_residentialSales.*, tbl_residential.*, tbl_residentialSaleAvailability.fld_value AS availability FROM tbl_property
			INNER JOIN tbl_residentialSales ON tbl_property.fld_propertyID = tbl_residentialSales.fld_propertyID 
			INNER JOIN tbl_residential ON tbl_property.fld_propertyID = tbl_residential.fld_propertyID
			INNER JOIN tbl_residentialSaleAvailability ON tbl_residentialSales.fld_availability = tbl_residentialSaleAvailability.fld_counter
			WHERE fld_department = :department AND tbl_residentialSales.fld_availability $stc AND tbl_residentialSales.fld_price >= :priceMin AND tbl_residentialSales.fld_price <= :priceMax AND tbl_residential.fld_propertyBedrooms >= :bedrooms AND tbl_property.fld_address3 like :town 
			$result_query";
			$params = array("department" => "Sales", "priceMin" => $_POST["min"], "priceMax" => $_POST["max"], "bedrooms" => str_replace("+", "", $_POST["bedrooms"]), "town" => $strtown);
		break;
		case "rental":
			if($_POST["stc"] == "on") $stc = "IS NOT NULL"; else $stc = "<> 4";
			if($result_sort == "low-high") { $result_query = "ORDER BY tbl_residentialLet.fld_rent ASC";	}
			if($result_sort == "high-low") { $result_query = "ORDER BY tbl_residentialLet.fld_rent DESC"; }
			if($result_sort == "new-old") { $result_query = "ORDER BY tbl_property.fld_dateLastModified DESC"; }
			if($result_sort == "old-new") { $result_query = "ORDER BY tbl_property.fld_dateLastModified ASC"; }
			$title = "properties for rent";
			$query = "SELECT tbl_property.*, tbl_residentialLet.*, tbl_residential.*, tbl_residentialLetRentFrequency.fld_value AS frequency, tbl_residentialLetAvailability.fld_value AS availability FROM tbl_property 
			INNER JOIN tbl_residentialLet ON tbl_property.fld_propertyID = tbl_residentialLet.fld_propertyID
			INNER JOIN tbl_residential ON tbl_property.fld_propertyID = tbl_residential.fld_propertyID
			INNER JOIN tbl_residentialLetRentFrequency ON tbl_residentialLet.fld_rentFrequency = tbl_residentialLetRentFrequency.fld_counter
			INNER JOIN tbl_residentialLetAvailability ON tbl_residentialLet.fld_availability = tbl_residentialLetAvailability.fld_counter
			WHERE fld_department = :department AND tbl_residentialLet.fld_availability $stc AND tbl_residentialLet.fld_rent >= :priceMin AND tbl_residentialLet.fld_rent <= :priceMax AND tbl_residential.fld_propertyBedrooms >= :bedrooms AND tbl_property.fld_address3 like :town
			$result_query";
			$params = array("department" => "Lettings", "priceMin" => $_POST["rentalMin"], "priceMax" => $_POST["rentalMax"], "bedrooms" => str_replace("+", "", $_POST["bedrooms"]), "town" => $strtown);
		break;
		case "commercial":
			if($_POST["stc"] == "on") $stc = "IS NOT NULL"; else $stc = "<> 6 AND tbl_commercial.fld_availability <> 9";
			if($status == 1) {
				$price_query = "tbl_commercial.fld_priceTo";
				$priceMin = $_POST["min"];
				$priceMax = $_POST["max"];
			} else {
				$price_query = "tbl_commercial.fld_rentTo";
				$priceMin = $_POST["rentalMin"];
				$priceMax = $_POST["rentalMax"];
			}
		//	if($result_sort == "low-high") { $result_query = "ORDER BY tbl_commercial.fld_priceTo ASC";	}
		//	if($result_sort == "high-low") { $result_query = "ORDER BY tbl_commercial.fld_priceTo DESC"; }
		//	if($result_sort == "new-old") { $result_query = "ORDER BY tbl_property.fld_dateLastModified DESC"; }
		//	if($result_sort == "old-new") { $result_query = "ORDER BY tbl_property.fld_dateLastModified ASC"; }
			$title = "commercial properties";
			$department = "Commercial";
			$query = "SELECT tbl_property.*, tbl_commercial.*, tbl_commAvailability.fld_value AS availability FROM tbl_property 
			INNER JOIN tbl_commercial ON tbl_property.fld_propertyID = tbl_commercial.fld_propertyID 
			INNER JOIN tbl_commAvailability ON tbl_commercial.fld_availability = tbl_commAvailability.fld_counter 
			WHERE fld_department = :department AND tbl_commercial.fld_availability $stc AND $price_query >= :priceMin AND $price_query <= :priceMax AND tbl_property.fld_address3 like :town AND tbl_commercial.fld_forSale = :status";
			$params = array("department" => "Commercial", "priceMin" => $priceMin, "priceMax" => $priceMax, "town" => $strtown, "status" => $status);
		break;
	}
	
	$strdbsql = $query;
	$dataParams = $params;
	$arrType = "multi";
	$resultdata = query($bookConn, $strdbsql, $arrType, $dataParams);
	
	if(!empty($resultdata)) {

		$i = 0;
	
		print("<div class='block-crit'>");
			print("<span>Showing <strong>".count($resultdata)."</strong> ".$title."</span>");
		print("</div>");
		
		foreach($resultdata AS $row) {
			
			$strdbsql = "SELECT * FROM tbl_propertyImages
			WHERE fld_propertyID = :propID ORDER BY fld_counter";
			$arrType = "single";
			$dataParams = array('propID' => $row['fld_propertyID']);
			$resultdata2 = query($bookConn, $strdbsql, $arrType, $dataParams);
			
			$propLink = str_replace(" ", "-", str_replace(",", "-", strtolower($row['fld_displayAddress'])));
			$propImage = str_replace("IMG_", "", substr($resultdata2['fld_image'], 0, strpos($resultdata2['fld_image'], '_', strpos($resultdata2['fld_image'], '_')+1)));
		
			if($i % 3 == 0) {
				print("<div class='row' style='margin-bottom:0;'>");
			}
				
				print("<div class='col-xs-12 col-sm-6 col-md-4'>");
	
					print("<div class='block-prop".($row['fld_featuredProperty'] == 1 ? " featured" : "")."'>");
						
						switch($row['availability']) {
							case "On Hold":
								$colour = "#9e9e9e";
								break;
							case "For Sale":
							case "To Let":
							case "Let":
								$colour = "";
								break;
							case "Under Offer":
							case "References Pending":
								$colour = "#f4b136";
								break;
							case "Sold STC":
							case "Sold":
							case "Let Agreed":
								$colour = "#f44336";
								break;
							case "Withdrawn":
								$colour = "#9e9e9e";
								break;
						}
						
						print("<span class='status' style='background-color:".$colour.";'>");
							if($row['availability'] == "To Let") {
								print("For Let");
							} else {
								if($row['availability'] == "Let") {
									print("For Sale");	
								} else {
									print($row['availability']);
								}
							}
						print("</span>");
						
						$strdbsql = "SELECT * FROM tbl_propertyImages
						WHERE fld_propertyID = :propID ORDER BY fld_counter
						LIMIT 4";
						$arrType = "multi";
						$dataParams = array('propID' => $row['fld_propertyID']);
						$resultdata2 = query($bookConn, $strdbsql, $arrType, $dataParams);
							
						print("<div id='carousel-".$i."' class='carousel slide' data-ride='carousel' data-interval='false'>");
							print("<div class='carousel-inner' role='listbox'>");

								if(count($resultdata2) > 0) {
							
									$x = 0;
							
									foreach($resultdata2 AS $row2) {
									
										$propImage = str_replace("IMG_", "", substr($row2['fld_image'], 0, strpos($row2['fld_image'], '_', strpos($row2['fld_image'], '_')+1)));
									
										print("<div class='item".($x == 0 ? " active" : "")."' style='background-image:url(http://media2.jupix.co.uk/v3/clients/1677/properties/".$propImage."/".$row2['fld_image'].");'>");
											print("<a href='/".$strPage."/".$row['fld_propertyID']."'></a>");
										print("</div>");
										
										$x++;
										
									}
									
								} else {
									
									print("<div class='item active' style='background-color:white;background-image:url(/datafeed/images/full/placeholder.jpg);background-size:contain;'>");
										print("<a href='/".$strPage."/".$row['fld_propertyID']."'></a>");
									print("</div>");
									
								}
									
							print("</div>");
							
							if(count($resultdata2)> 1) {
								
								print("<a class='left carousel-control' href='#carousel-".$i."' role='button' data-slide='prev'>");
									print("<span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>");
									print("<span class='sr-only'>Previous</span>");
								print("</a>");
								print("<a class='right carousel-control' href='#carousel-".$i."' role='button' data-slide='next'>");
									print("<span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>");
									print("<span class='sr-only'>Next</span>");
								print("</a>");
								
							}
							
						print("</div>");
						
						print("<div class='block-info'>");
							print("<a class='head' href='/".$strPage."/".$row['fld_propertyID']."'>");
							
								print("<span>");
									if(!empty($row['fld_addressName'])) print($row['fld_addressName'].", ");
									if(!empty($row['fld_addressNumber'])) print($row['fld_addressNumber']." ");
									if(!empty($row['fld_addressStreet'])) print($row['fld_addressStreet'].", ");
									if(!empty($row['fld_address2'])) print($row['fld_address2'].", ");
								print("</span>");
								if(!empty($row['fld_address3'])) print("<span>".$row['fld_address3']."</span>");
								if(!empty($row['fld_addressPostcode'])) print("<span class='postCode'>".$row['fld_addressPostcode']."</span>");
							
								switch($strPage) {
									case "sales":
										print("<span class='price'>&pound;".number_format($row['fld_price'])."</span>");
									break;
									case "rental":
										if($row['frequency'] == "pw") $frequency = "weekly";
										if($row['frequency'] == "pcm") $frequency = "monthly";
										if($row['frequency'] == "pa") $frequency = "yearly";
										print("<span class='price'>&pound;".number_format($row['fld_rent'])."/<span style='font-size:14px;'>".$frequency."</span></span>");
									break;
									case "commercial":
										if($status == 1){
											print("<span class='price'>&pound;".number_format($row['fld_priceTo'])."</span>");
										} else {
											if($row['fld_rentFrequency'] == "pw") $frequency = "weekly";
											if($row['fld_rentFrequency'] == "pcm") $frequency = "monthly";
											if($row['fld_rentFrequency'] == "pa") $frequency = "yearly";
											print("<span class='price'>&pound;".number_format($row['fld_rentTo'])."<span>/".$frequency."</span></span>");
										}
									break;
								}
								
							print("</a>");							
						print("</div>");
					print("</div>");
					
				print("</div>");
			
			if($i % 3 == 2) {
				print("</div>");
			}
			
			$i++;
			
		}
		
	} else {
		
		print("No properties matching your search criteria!");
	
	}

?>