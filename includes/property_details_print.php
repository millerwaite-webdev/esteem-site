<?php 

	print("<!DOCTYPE html>");
	print("<html lang='en'>");
	
	if(isset($_GET['more'])) $propertyID = $_GET['more'];
	else $propertyID = '0';
	
	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");
	
	$bookConn = bookConnect();
	
	$strdbsql = "SELECT * FROM properties 
	INNER JOIN property_images ON properties.rs_id = property_images.rs_id 
	WHERE properties.egnID = :propertyID ORDER BY property_images.image_order LIMIT 1";
	$strType = "single";
	$dataParams = array("propertyID"=>$propertyID);
	$eg_Result1 = query($bookConn, $strdbsql, $strType, $dataParams);
	
	?>
	
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title><?php echo @$eg_Result1['metatitle'];?></title>    
		<meta content="<?php echo  @$eg_Result1['metadescription'] ?>" name="description">
		<meta name="robots" content="noindex, nofollow">
		<link href="css/printlayout.css" type="text/css" rel="stylesheet">
		<LINK REL="SHORTCUT ICON" HREF="http://www.ralphspours.com/favicon.ico">
	</head>
	
	<body>

		<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					
					<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td><img src="/images/elements/headerprint.jpg" width="1000" height="163" /></td>
						</tr>
					</table>

					<table width="970" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td width="475" height="19" valign="top"><p align="left">
								<span class="style36">      T</span><span class="style37">he <span class="style38">E</span>state</span> <span class="style36">A</span><span class="style37">gent</span><br />
								<br />
								<span class="style39"><span class="style52"><span class="style38">Upper Brook Street ,</span> <span class="style40">Ulverston,</span></span> <span class="style52"><br />
								<span class="style38">Cumbria, LA12 7BH</span></span></span><br />
								<br />
							</p>      </td>
							<td width="495" valign="top"><div align="center" class="style39">
								<p align="right" class="style67"><span class="style68">Web - www.ralphspours.com<br />
									Email - ralphspours@btconnect.com</span><br />
									<br />
									<span class="style52"><span class="style38">Tel </span>- <span class="style40">01229 587866 </span></span><br />
									<span class="style44"><span class="style38">Mobile</span> - <span class="style40">07736 255367</span></span><span class="style42"><br />
								</span></p>
							</div></td>
						</tr>
						<tr>
							<td height="60" colspan="2" valign="middle"><div align="center">
								<table width="100%" border="0">
									<tr>
										<td height="62"><div align="center"><span class="style36"><?php echo  @$eg_Result1['p_name'] ?>, <?php echo  @$eg_Result1['p_area'] ?> , <?php echo  @$eg_Result1['postcode'] ?> </span></div>  <div align="right">
										</div></td>
									</tr>
								</table>
							</div></td>
						</tr>
						<tr>
							<td height="23" colspan="2">            <div align="right">
								<SCRIPT LANGUAGE="JavaScript">
								if (window.print) {
									document.write('<form> '
									+ '<input type=button name=print value="Print" '
									+ 'onClick="javascript:window.print()"></form>');
								}
								// End -->
								</script>
							</div></td>
						</tr>
						<tr>
							<td height="365" colspan="2">
								<div align="center">
								
									<?php
									
									if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/properties/".@$eg_Result1['image_ref'].".png")) {
										echo "<img src='/images/properties/".@$eg_Result1['image_ref'].".png' border='4' width='962' />";
									} else {
										echo "<img src='/images/properties/".@$eg_Result1['image_ref'].".jpg' border='4' width='962' />";
									}
									
									?>

								</div>
							<div align="center"></div></td>
						</tr>
					</table>
					<br />
					<br />
					<table width="970" border="2" align="center" cellpadding="2" cellspacing="2" bordercolor="#CC3300">
						<tr>
							<td width="74" bgcolor="#CCCC99"><div align="center"><span class="style40">Price :</span></div></td>
							<td width="120" bgcolor="#CCCC99"><div align="center"><span class="style45">&pound;<?php echo  @$eg_Result1['prop_price'] ?></span></div></td>
							<td width="91" bgcolor="#CCCC99"><div align="center"><span class="style40">Tenure :</span></div></td>
							<td width="214" bgcolor="#CCCC99"><div align="center"><span class="style45"><?php echo  @$eg_Result1['prop_tenure'] ?></span></div></td>
							<td width="62" bgcolor="#CCCC99"><div align="center"><span class="style40">Type :</span></div></td>
							<td width="221" bgcolor="#CCCC99"><div align="center"><span class="style45"><?php echo  @$eg_Result1['prop_type'] ?></span></div></td>
							<td width="68" bgcolor="#CCCC99"><div align="center"><span class="style40">Beds :</span></div></td>
							<td width="52" bgcolor="#CCCC99"><div align="center"><span class="style45"><?php echo  @$eg_Result1['No_beds'] ?></span></div></td>
						</tr>
					</table>
					<div class="pagebreak"></div>
					<table width="970" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td><span class="style65">Property details</span><span class="style44"> :</span><br />
							<br /><?php echo stripslashes( @$eg_Result1['long_description']) ?></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<?php

								$strdbsql = "SELECT * FROM property_images WHERE rs_id = :propID AND make_live = 'yes' AND ((image_title <> 'yes' AND image_title <> 'EPC') OR image_title IS NULL) ORDER BY image_order";
								$strType = "multi";
								$dataParams = array("propID"=>$eg_Result1['rs_id']);
								$eg_recResult2 = query($bookConn, $strdbsql, $strType, $dataParams);
								
								// Check that there are records in the recordset
								if(count($eg_recResult2) > 0)
								{
								
									?>
		  
									<br />
									<span class="style65">Gallery of pictures :</span><br />
									<table width="970" align="center" >
										<tr>
											
											<?php

											$eg_i = 0;
										
											// Set record set pointer to the start of the record set and begin looping
											mysql_data_seek($eg_recResult2, 0);
											
											foreach($eg_recResult2 AS $eg_Result2)
											{
												if (!($eg_i % 3)) echo "</tr><tr>";
											
												?>
			
													<td height="108" valign="top"><div align="center">
														<table width="300" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td valign="top">
																
																	<?php
									
																	if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/properties/".@$eg_Result2['image_ref'].".png")) {
																		echo "<img src='/images/properties/".@$eg_Result2['image_ref'].".png' border='4' width='295' />";
																	} else {
																		echo "<img src='/images/properties/".@$eg_Result2['image_ref'].".jpg' border='4' width='295' />";
																	}
																	
																	?>
																
																</td>
															</tr>
															<tr>
																<td valign="top"><br />
																	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
																		<tr>
																			<td valign="top" class="style1 style67"><?php echo stripslashes (  @$eg_Result2['image_description'] )?></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</div></td>
												
												<?php
		
												$eg_i++;

											}
											
											?>
	  
										</tr>
									</table>

									<?php

									mysql_data_seek($eg_recResult2, 0);
									$eg_Result2 = mysql_fetch_array($eg_recResult2, MYSQL_ASSOC);
								
								}
								
								// Tidy up used objects
								// Close recordset
								if(isset($eg_recResult1)) @mysql_free_result($eg_recResult1);

								// Close database connection
								if(isset($eg_objConn1)) @mysql_close($eg_objConn1);

								// Close recordset
								if(isset($eg_recResult2)) @mysql_free_result($eg_recResult2);

								?>
							
							</td>
						</tr>
					</table>
					
					<?php
					
					$strdbsql = "SELECT * FROM property_images WHERE rs_id = :propID AND image_title = 'EPC' AND make_live = 'yes'";
					$strType = "single";
					$dataParams = array("propID"=>$eg_Result1['rs_id']);
					$eg_recResult3 = query($bookConn, $strdbsql, $strType, $dataParams);
					
					?>
					
					<div> 
						<p><br />
						
							<?php
							
							if(count($eg_recResult3) > 0 && (file_exists("http://www.ralphspours.com/images/properties/".$eg_recResult3['image_ref'].".jpg") || file_exists("http://www.ralphspours.com/images/properties/".$eg_recResult3['image_ref'].".png"))) {
						
								echo "<span class='style65'>Energy Efficiency Rating Details:</span><br />";
								echo "<br />";
								
								if(file_exists("http://www.ralphspours.com/images/properties/".$eg_recResult3['image_ref'].".jpg")) {
									echo "<img src='http://www.ralphspours.com/images/properties/".$eg_recResult3['image_ref'].".jpg' border='0' width='309' alt='".$eg_Result2['image_title']."' />";
								} else {
									echo "<img src='http://www.ralphspours.com/images/properties/".$eg_recResult3['image_ref'].".png' border='0' width='309' alt='".$eg_Result2['image_title']."' />";
								}
							
								?>
							
								<br /><br /><br />
								
								<?php
								
							}
							
							if($eg_Result1['floorplan'] != "" AND $eg_Result1['floorplan'] != "blank") {
								if(file_exists("http://www.ralphspours.com/images/properties/".$eg_Result1['floorplan'].".jpg")) {
							
									echo "<span class='style65'>Floor Plan</span><br />";
									echo "<br />";
									echo "<div align='center'>";
										
										if($eg_Result1['floorplan'] != "" AND $eg_Result1['floorplan'] != "blank") {
											if(file_exists("http://www.ralphspours.com/images/properties/".$eg_Result1['floorplan'].".jpg")) {
												echo "<img src='http://www.ralphspours.com/images/properties/".$eg_Result1['floorplan'].".jpg' border='1' width='800' alt='".$eg_Result1['p_name']."' />";
											} else {
												echo "<img src='http://www.ralphspours.com/images/properties/".$eg_Result1['floorplan'].".png' border='1' width='800' alt='".$eg_Result1['p_name']."' />";
											}
										}

									echo "</div>";
									
								}
							}
							
							?>
							
						</p>
					</div>
						
					<div align="center"><br />***********************************<br />
						<br />
						Viewing arrangements : Only by appointment and accompanied by the sole agent<br />
						<br />
						We offer free valuations without any obligation
						<br><br>
						<br />
					</div>
					
					<div align="center">
						<img src="/images/elements/tsi-logos.jpg" alt="The Property Ombudsman" />
						<br><br>
						<br />
					</div>

					<!-- Start of StatCounter Code for Default Guide -->
					<script type="text/javascript">
					var sc_project=9474619; 
					var sc_invisible=1; 
					var sc_security="9045e7cd"; 
					var scJsHost = (("https:" == document.location.protocol) ?
					"https://secure." : "http://www.");
					document.write("<sc"+"ript type='text/javascript' src='" +
					scJsHost+
					"statcounter.com/counter/counter.js'></"+"script>");
					</script>
					
					<noscript><div class="statcounter"><a title="web analytics"
					href="http://statcounter.com/" target="_blank"><img
					class="statcounter"
					src="http://c.statcounter.com/9474619/0/9045e7cd/1/"
					alt="web analytics"></a></div></noscript>
					<!-- End of StatCounter Code for Default Guide -->
				
				</td>
			</tr>
		</table>
				
    </body>
</html>

<?php

// Tidy up used objects
// Close recordset
if(isset($eg_recResult1)) @mysql_free_result($eg_recResult1);

// Close database connection
if(isset($eg_objConn1)) @mysql_close($eg_objConn1);

?>