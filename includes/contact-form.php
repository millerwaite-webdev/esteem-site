<?php

	$bookConn = bookConnect();

	print("<div class='block-form'>");
		print("<div class='container' style='width:100%'>");
			print("<div class='row'>");
				print("<div class='col-sm-12 col-md-4'>");
				
					switch($strPage) {
						default:
							print("<h3>Contact Us</h3>");
							print("<p>Please let us know if you have a question, want to leave a comment, or would like further information about Esteem Homes.</p>");
							break;
					}

				print("</div>");
				print("<div class='col-sm-12 col-md-8'>");
					print("<form id='callback-form' action='?action=callback' method='post'>");
						print("<input type='hidden' name='call_type' id='call_type' value='Contact Enquiry' />");
						print("<div class='row'>");
							print("<div class='col-md-4'>");
								print("<div class='callback-con call_name'>");
									print("<input type='text' name='call_name' id='call_name' placeholder='Name*' />");
									print("<div class='error-icon'><i class='fa fa-times' aria-hidden='true'></i></div>");
								print("</div>");
								print("<div class='callback-con call_phone'>");
									print("<input type='number' name='call_phone' id='call_phone' placeholder='Phone*' />");
									print("<div class='error-icon'><i class='fa fa-times' aria-hidden='true'></i></div>");
								print("</div>");
								print("<div class='callback-con call_email'>");	
									print("<input type='email' name='call_email' id='call_email' placeholder='Email' />");
									print("<div class='error-icon'><i class='fa fa-times' aria-hidden='true'></i></div>");
								print("</div>");
								print("<div class='callback-con call_reason'>");	
									print("<select name='call_reason' id='call_reason' style='background-color:rgba(255,255,255,.9) !important;
 border:solid 1px rgba(0,0,0,0.3);
 font-size:14px;
 line-height:18px;
 color:#222;
 background:transparent;
 border-radius:3px;
 margin-bottom:30px;
 outline:0;
 padding:15px;
 -webkit-transition:border-color .3s ease;
 transition:border-color .3s ease;
 box-sizing:border-box !important;
 width:100%;
 display:block;'>");
										print("<option value=''>Source of Enquiry</option>");
										print("<option value='Letter in the post'>Letter in the post</option>");
										print("<option value='Newspaper'>Newspaper</option>");
										print("<option value='Facebook'>Facebook</option>");
										print("<option value='Word of Mouth'>Word of Mouth</option>");
										print("<option value='Rightmove'>Rightmove</option>");
										print("<option value='For sale sign'>For sale sign</option>");
										print("<option value='Website'>Website</option>");
										print("<option value='Show Home sign'>Show Home sign</option>");
									print("</select>");
									print("<div class='error-icon'><i class='fa fa-times' aria-hidden='true'></i></div>");
								print("</div>");
							print("</div>");
							print("<div class='col-md-8'>");
							
								print("<div class='row' style='margin-bottom:0;'>");

									if(isset($_GET['id'])) {
										
										$getArticleQuery = "SELECT * FROM properties WHERE egnID = :listID";
										$arrParam = array("listID" => $_GET['id']);
										$articleData = query($conn, $getArticleQuery, "single", $arrParam);
										
										//$property = $articleData['fld_displayAddress'];
										$property = $articleData['p_name']." ".$articleData['p_area']." ".$articleData['postcode'];
										
									} else {
										
										$property = "";
									
									}
								
									print("<div class='col-md-12'>");
										print("<div class='callback-con call_add'>");	
											print("<input type='text' name='call_add' id='call_add' placeholder='Property Address' value='".$property."' />");
											print("<div class='error-icon'><i class='fa fa-times' aria-hidden='true'></i></div>");
										print("</div>");
									print("</div>");
								print("</div>");
							
								print("<div class='callback-con call_msg'>");	
									print("<textarea name='call_msg' id='call_msg' placeholder='Type in here'></textarea>");
								print("</div>");
								print("<div class='callback-con'>");
								print("<div class='g-recaptcha' data-sitekey='6Leuz7AUAAAAAHts449EtPITGF0RdZM1ngCAUbYX'></div>");	
									print("<button type='submit' value='submit' id='call_submit'>Submit <i class='fa fa-chevron-right' aria-hidden='true'></i></button>");
								print("</div>");
							print("</div>");
						print("</div>");

					print("</form>");
				print("</div>");
			print("</div>");
		print("</div>");
	print("</div>");
	
?>