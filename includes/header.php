<?php


	if(isset($_GET['page'])) $strPage = $_GET['page']; else $strPage = 'index';

	if(isset($_REQUEST['view'])) $strview = $_REQUEST['view']; else $strview = "";
	if(isset($_REQUEST['q'])) $strsearch = $_REQUEST['q']; else $strsearch = "";
	
	$pageDataQuery = 'SELECT * FROM site_pages WHERE pageName = :page';
	$pageData = query($conn, $pageDataQuery, "single", array("page"=>$strPage));
	
	if(isset($_REQUEST['id']) && $strPage != "contact") {



		$strCanonical = $strsiteurl.$type."/".$result['fld_propertyID'];
		$strTitle = ($type != "commercial" ? $result2['fld_propertyBedrooms']." Bedroomed " : "").$result2['fld_displayPropertyType']." | ".(!empty($result['fld_addressStreet']) ? $result['fld_addressStreet'] : $result['fld_address2'])." | ".$strTitle;
		$strDescription = substr($result['fld_mainSummary'], 0, 200);
	} else if($pageData) {
		if(!empty($pageData['pageTitle'])) {
			if($strPage == "index") {
				$strCanonical = $strsiteurl;
				$strTitle = $pageData['pageTitle']." | ".$strTitle;
			} else {
				$strCanonical = $strsiteurl.$strPage;
				$strTitle = $pageData['pageTitle']." | ".$strTitle;
			}
		}
		$strDescription = $pageData['metaDescription'];
		$strKeywords = $pageData['metaKeywords'];
		$strPageData = $pageData['pageContent'];
	} else if($strPage == "search") {
		$strCanonical = $strsiteurl.$strPage;
		switch($strsearch) {
			case "askam": $search = "Askam-in-Furness"; break;
			case "barrow": $search = "Barrow-in-Furness"; break;
			case "broughton": $search = "Broughton-in-Furness"; break;
			case "dalton": $search = "Dalton-in-Furness"; break;
			case "urswick": $search = "great urswick"; break;
			case "kirkby": $search = "Kirkby-in-Furness"; break;
			case "lowick": $search = "lowick green"; break;
			case "newton": $search = "Newton-in-Furness"; break;
			case "stainton": $search = "stainton with adgarley"; break;
			default: $search = $strsearch; break;
		}
		$strTitle = "Properties For Sale in ".ucwords($search)." | " .$strTitle;
		$strDescription = "Search";
		$strKeywords = "search";
	
	} else if($strPage == "property_details_print") {
		
		$strTitle = "Properties For Sale in ".ucwords($search)." | " .$strTitle;
		$strDescription = "Search";
		$strKeywords = "search";
	
	} else {
		header("HTTP/1.0 404 Not Found");
		$strTitle = "Page Not Found";
		$strPageData = "<div class='block-banner' style='background-image:url(/images/pages/error.jpg);'><div class='container'><div class='block-bread'><ul><li><a href='/index'>Home</a></li><li><i class='fa fa-angle-right' aria-hidden='true'></i></li><li><span>Page not found</span></li></ul></div><h1>Page not found</h1></div></div><div class='block-text' style='flex:1 0 auto;'><div class='container'><p><strong>The page you are looking for could not be found.</strong></p><p>Please use the following links to return to the home page:</p><a href='index' class='btn-standard'><i class='fa fa-home' aria-hidden='true'></i> Home</a></div></div>";
	}

	print("<!DOCTYPE html>");
	print("<html lang='en'>");
	
	print("<head>");
		print("<meta charset='utf-8' />");
		print("<meta http-equiv='X-UA-Compatible' content='IE=edge' />");
		print("<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0' />");
		print("<meta name='description' content='".$strDescription."' />");
		print("<meta name='keywords' content='".$strKeywords."' />");
		
		print("<meta name='theme-color' content='#3C474D'>");
		print("<meta name='msapplication-navbutton-color' content='#3C474D'>");
		print("<meta name='apple-mobile-web-app-status-bar-style' content='#3C474D'>");
		print("<meta name='google-site-verification' content='t9B8MtkgZWxaHPskZNHH0d2rEMQvsJzkr8y41mBSIDU' />");
		
		print("<title>".$strTitle."</title>");

		print("<link rel='canonical' href='".$strCanonical."' />");
	
		print("<link rel='shortcut icon' href='/favicon.ico'>");
	
	//	print("<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Montserrat%3A400%2C500%2C600%2C700%2C800%2C900&amp;subset=latin%2Clatin-ext' />");
	
		print("<link rel='stylesheet' type='text/css' href='/css/jquery-ui.css' />");
		print("<link rel='stylesheet' type='text/css' href='/css/bootstrap.min.css' />");
		print("<link rel='stylesheet' type='text/css' href='/css/font-awesome.min.css' />");
		print("<link rel='stylesheet' type='text/css' href='/css/jplist.core.min.css' />");
		print("<link rel='stylesheet' type='text/css' href='/css/jplist.filter-toggle-bundle.min.css' />");
		
		//print("<link rel='stylesheet' type='text/css' href='/css/materialize.min.css' />");
		print("<link rel='stylesheet' type='text/css' href='/css/main.css?v=1.1.4' />");
		print("<link rel='stylesheet' type='text/css' href='/css/slick.min.css' />");
		
		print("<script src='/js/jquery.min.js'></script>");
		print("<script src='/js/jquery-ui.min.js'></script>");
		print("<script src='/js/bootstrap.min.js'></script>");
		//print("<script src='/js/materialize.min.js'></script>");
		print("<script src='/js/filter.js'></script>");
		if($strPage == "blog" || $strPage == "reviews") print("<script type='text/javascript' src='/js/isotope.pkgd.min.js'></script>");
		
		?>
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script>
			
		</script>
		

		
		<?php
		
	print("</head>");


	print("<body".($strTitle == "Page Not Found" ? " class='sticky'" : "").">");

	print("<a class='to-top' href='#'><i class='fa fa-chevron-up' aria-hidden='true'></i></a>");
	
	print("<header>");
		print("<div class='container'>");
			print("<div class='row'>");
				print("<div class='col-sm-8'>");
					print("<a href='/index'>");
						print("<img class='logo' src='/images/company/".$compLogo."' alt='".$compName."' />");
					print("</a>");
				print("</div>");
				
				/*print("<div class='col-sm-4' >");
					print("<div itemscope itemtype='https://schema.org/WebSite' style='float:right;'>");
						print("<meta itemprop='url' content='".$strsiteurl."' />");
						print("<form id='search' name='search' action='/search' method='get' itemprop='potentialAction' itemscope itemtype='https://schema.org/SearchAction'>");
							print("<meta itemprop='target' content='".$strsiteurl."search?q={q}' />");
							print("<input type='text' id='q' name='q' placeholder='Enter town or postcode' value='".(substr(strtoupper($strsearch), 0 , 2) == "LA" ? strtoupper($strsearch) : ucwords($strsearch))."' itemprop='query-input' />");
							print("<button type='submit'><i class='fa fa-search' aria-hidden='true'></i></button>");
						print("</form>");
					print("</div>");
				print("</div>");*/
				
			print("</div>");
			print("<a href='/index'>");
				print("<img class='logo disguise' src='/images/company/".$compLogo."' alt='".$compName."' />");
			print("</a>");
			
			print("<h2>Call our sales office today between 9am - 5pm on 01229 813036 or 07950 650033 (Anytime)</h2>");
			
			print("<nav class='navbar navbar-default'>");
				print("<div class='navbar-header'>");
					print("<button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1' aria-expanded='false'>");
						print("<span class='sr-only'>Toggle navigation</span>");
						print("<span class='icon-bar'></span>");
						print("<span class='icon-bar'></span>");
						print("<span class='icon-bar'></span>");
					print("</button>");
				print("</div>");
				print("<div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>");
					print("<ul class='nav navbar-nav'>");
						
						$strdbsql = "SELECT * FROM site_pages WHERE displayInNav = 1 AND parentPageID IS NULL AND visible = 1 ORDER BY pageOrder";
						$parents = query($conn, $strdbsql, "multi");
						
						foreach($parents AS $parent) {
								
							$strdbsql = "SELECT * FROM site_pages WHERE parentPageID = :parentPageID AND visible = 1 ORDER BY pageOrder";
							$children = query($conn, $strdbsql, "multi", array("parentPageID" => $parent['recordID']));
							
							$countChildren = count($children);
					
							if($countChildren > 0) {
								print("<li class='dropdown'>");
									print("<a href='/".$parent['pageName']."' class='dropdown-toggle disable' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>".$parent['metaPageLink']." <i class='fa fa-angle-down' aria-hidden='true'></i></a>");
									print("<div class='dropdown-menu'>");
										print("<ul>");
									
											foreach($children AS $child){
												switch($child['pageName']) {
													case "sales":
													case "rental":
													case "commercial":
														print("<li><a href='/".$child['pageName']."'>".$child['metaPageLink']."</a></li>");
														break;
													default:
														print("<li><a href='/".$child['pageName']."'>".$child['metaPageLink']."</a></li>");
														break;
												}
											}
									
										print("</ul>");
									print("</div>");
								print("</li>");
							} else {
								print("<li>");
									print("<a href='/".$parent['pageName']."' ".($strPage == $parent['pageName'] ? "class='active'" : "").">");
										print($parent['metaPageLink']);
									print("</a>");
								print("</li>");
							}
						}
						
					print("</ul>");
				print("</div>");
			print("</nav>");
			
		print("</div>");
	print("</header>");
	
?>