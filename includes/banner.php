<?php

	$strdbsql = 'SELECT * FROM site_pages WHERE pageName = :page';
	$result = query($conn,$strdbsql,"single",array("page"=>$strPage));

	if($result) {

		$title = $result['metaPageLink'];
		$image = $result['pageImage'];
	} else if($strPage == "search") {
		if($strsearch) {
			switch($strsearch){
				case "askam": $search = "Askam-in-Furness"; break;
				case "barrow": $search = "Barrow-in-Furness"; break;
				case "broughton": $search = "Broughton-in-Furness"; break;
				case "dalton": $search = "Dalton-in-Furness"; break;
				case "urswick": $search = "great urswick"; break;
				case "kirkby": $search = "Kirkby-in-Furness"; break;
				case "lowick": $search = "lowick green"; break;
				case "newton": $search = "Newton-in-Furness"; break;
				case "stainton": $search = "stainton with adgarley"; break;
				default: $search = $strsearch; break;
			}
			$title = "Properties Near <span>".(substr(strtoupper($search), 0 , 2) == "LA" ? strtoupper($search) : ucwords($search))."</span>";
		} else {
			$title = "Property Search";
		}
		$image = "search.jpg";
	}
	
	print("<div class='block-banner".(!empty($image) ? "' style='background-image:url(/images/pages/".$image.");'" : " empty'").">");
		print("<div class='container'>");
			print("<h1>".$title."</h1>");
		print("</div>");
	print("</div>");

?>