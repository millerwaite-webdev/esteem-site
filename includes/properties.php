<?php

	include($_SERVER['DOCUMENT_ROOT']."/includes/incsitecommon.php");
	
	$conn = connect();
	
	try {
		
		$params = array();
		
		if($_REQUEST['location'] != "") {
			$location = "= :location";
			$params['location'] = "".$_REQUEST['location']."";
		} else {
			exit;
			$location = "=0";
		}
		
		if($_REQUEST['type'] != "") {
			$type = "LIKE :type"; 
			$params['type'] = "%".$_REQUEST['type']."%";
		} else {
			$type = "IS NOT NULL";
		}
		if($_REQUEST['bedrooms'] > 0) {
			$bedrooms = "= :bedrooms";
			$params['bedrooms'] = $_REQUEST['bedrooms'];
		} else {
			$bedrooms = "IS NOT NULL";
		}
		if($_REQUEST['bathrooms'] > 0) {
			$bathrooms = "= :bathrooms";
			$params['bathrooms'] = $_REQUEST['bathrooms'];
		} else {
			$bathrooms = "IS NOT NULL";
		}
		if(isset($_REQUEST['pagination'])) {
			$pagination = $_REQUEST['pagination'];
		} else {
			$pagination = 1;
		}
		
		$strdbsql = "SELECT COUNT(*) FROM properties WHERE make_live = 'yes' AND property_location_id $location AND prop_type $type AND No_beds $bedrooms AND No_baths $bathrooms ORDER BY displayOrder";
		$total = query($conn,$strdbsql,"column",$params);
		
		error_log($strdbsql);
		
		// How many items to list per page
		$limit = 100;

		// How many pages will there be
		$pages = ceil($total / $limit);

		// What page are we currently on?
		$page = $pagination;
		
		// Calculate the offset for the query
		$offset = ($page - 1)  * $limit;

		// Some information to display to the user
		$start = $offset + 1;
		$end = min(($offset + $limit), $total);

		// Prepare the paged query
		if($total > 0) {
			$stmt = $conn->prepare("
			SELECT properties.*, property_locations.sitemap
			FROM properties
			INNER JOIN property_locations ON properties.property_location_id = property_locations.recordID 				
			WHERE make_live = 'yes' AND property_location_id $location AND prop_type $type AND No_beds $bedrooms AND No_baths $bathrooms
			ORDER BY displayOrder
			LIMIT :limit 
			OFFSET :offset");
		} else {
			$stmt = $conn->prepare("
			SELECT properties.*, property_locations.sitemap
			FROM properties 
			INNER JOIN property_locations ON properties.property_location_id = property_locations.recordID 
			WHERE make_live = 'yes' AND property_location_id $location AND prop_type $type AND No_beds $bedrooms AND No_baths $bathrooms
			ORDER BY displayOrder
			LIMIT :limit");
		}
		
		//print("<span class='hide'>".$strdbsql."</span>");
		//print("<span class='hide'>SELECT * FROM properties WHERE make_live = 'yes' AND property_location_id :location AND prop_type $type AND No_beds $bedrooms AND No_baths $bathrooms ORDER BY prop_price ASC LIMIT :limit OFFSET :offset</span>");

		// Bind the query params
		$stmt->bindParam(":limit", $limit, PDO::PARAM_INT);
		
		if($_REQUEST['location'] != "") $stmt->bindParam(":location", $params['location'], PDO::PARAM_STR);
		if($_REQUEST['type'] != "") $stmt->bindParam(":type", $params['type'], PDO::PARAM_STR);
		if($_REQUEST['bedrooms'] > 0) $stmt->bindParam(":bedrooms", $_REQUEST['bedrooms'], PDO::PARAM_STR);
		if($_REQUEST['bathrooms'] > 0) $stmt->bindParam(":bathrooms", $_REQUEST['bathrooms'], PDO::PARAM_STR);
		if($total > 0) $stmt->bindParam(":offset", $offset, PDO::PARAM_INT);
		$stmt->execute();
		
		error_log("cw");
		error_log(serialize($stmt->errorInfo()));

		$html .= ("<h4><span class='highlight'>".$total."</span> Results</h4>");
		
		$html .= ("<input type='hidden' name='location' value='".$_REQUEST['location']."' />");
		$html .= ("<input type='hidden' name='type' value='".$_REQUEST['type']."' />");
		$html .= ("<input type='hidden' name='bedrooms' value='".$_REQUEST['bedrooms']."' />");
		$html .= ("<input type='hidden' name='bathrooms' value='".$_REQUEST['bathrooms']."' />");
		
		// Do we have any results?
		if ($stmt->rowCount() > 0) {
			// Define how we want to fetch the results
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$iterator = new IteratorIterator($stmt);
			
			// Display the results
			$sitemap = "";
			$html .= ("<div class='products'>");
				foreach($iterator as $row) {
					
					$sitemap = $row['sitemap'];
					
					$html .= ("<div class='card'>");
						$html .= ("<a href='/properties/".$row['egnID']."'>");
							switch(strtolower($row['sold'])) {
								case "under offer":
									$colour = "yellow";
								break;
								case "to let":
								case "now let":
									$colour = "blue";
								break;
								case "sstc":
								case "sold":
									$colour = "red";
								break;
								default:
									$colour = "";
								break;
							}
								$html .= ("<span class='card-counter ".$colour."'>".ucwords(strtolower((!empty($row['sold']) ? $row['sold'] : "Available")))."</span>");
							
							$strdbsql = "SELECT * FROM property_images WHERE rs_id = :propID ORDER BY image_order LIMIT 1";
							$strType = "single";
							$dataParams = array("propID"=>$row['rs_id']);
							$images = query($conn, $strdbsql, $strType, $dataParams);
							
							
								if($images['url'] != ""){
									$html .= ("<div class='card-image' id='".$row['rs_id']."'>");
											$html .= ("<img src='".$images['url']."' alt='".$images['image_title']."' />");
										$html .= ("</div>");
								} else {
									if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/properties/".$images['image_ref'].".png")) {
										$html .= ("<div class='card-image' id='".$row['rs_id']."'>");
											$html .= ("<img src='/images/properties/".$images['image_ref'].".png' alt='".ucwords(strtolower($row['p_name']))."' />");
										$html .= ("</div>");
									} else if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/properties/".$images['image_ref'].".jpg")) {
										$html .= ("<div class='card-image' id='".$row['rs_id']."'>");
											$html .= ("<img src='/images/properties/".$images['image_ref'].".jpg' alt='".ucwords(strtolower($row['p_name']))."' />");
										$html .= ("</div>");
									} else {
										$html .= ("<div class='card-image none' id='".$row['rs_id']."'>");
											$html .= ("<span>No Images Available</span>");
										$html .= ("</div>");
									}
								}
							
							$html .= ("<div class='card-content'>");
								$html .= ("<span class='card-title'>".ucwords(strtolower($row['p_name']))."</span>");
								$html .= ("<span class='card-subtitle'>");
									$html .= ($row['p_area']);
									if(!empty($row['postcode']))$html .= (", ".$row['postcode']);
								$html .= ("</span>");
								$html .= ("<p>".$row['short_description']."</p>");
							$html .= ("</div>");
							$html .= ("<div class='card-action'>");
								$html .= ("<div class='card-price'>");
									$price = strtoupper(str_replace("from ", "", str_replace("£", "", str_replace(".", "", $row['prop_price']))));
									if($price != "TO LET" && $price != "TBA" && $price != "OFFERS") $html .= ("&pound;".number_format($price,2));
									else $html .= ($price);
								$html .= ("</div>");
								$html .= ("<ul class='card-details'>");
									$html .= ("<li><i class='fa fa-house-user left' aria-hidden='true'></i><span><strong>".(($row['sold'] == "") ? "Available" : $row['sold'])."</strong></span></li>");
									$html .= ("<li><i class='fa fa-bed left' aria-hidden='true'></i><span><strong>".$row['No_beds']."</strong> Bedrooms</span></li>");
									$html .= ("<li><i class='fa fa-bath left' aria-hidden='true'></i><span><strong>".$row['No_baths']."</strong> Bathrooms</span></li>");
									
								//	$html .= ("<li><i class='material-icons left'>directions_car</i><span><strong>#</strong> ".$row['prop_type']."</span></li>");
								$html .= ("</ul>");
							$html .= ("</div>");
						$html .= ("</a>");
					$html .= ("</div>");
				}
			$html .= ("</div>");

		} else {
			$html .= ("<p>No results could be displayed.</p>");
		}

		if($total > 0) {
			$html .= ("<ul class='pagination'>");
				//if($page > 1) $html .= ("<li class='left'><span data-target='".($page - 1)."'><i class='material-icons'>arrow_drop_down</i></span><span data-target='".($page - 1)."'>Previous</span></li>");
				//else $html .= ("<li class='left disabled'><span><i class='material-icons'>arrow_drop_down</i></span><span>Previous</span></li>");
				
				/*for($i = 1; $i <= $pages; $i++) {
					$html .= ("<li".($i == $page ? " class='active'" : "")."><span data-target='".$i."'>".$i."</span></li>");
				}*/
				
				//if($page < $pages) $html .= ("<li class='right'><span data-target='".($page + 1)."'>Next</span><span data-target='".($page + 1)."'><i class='material-icons'>arrow_drop_down</i></span></li>");
				//else $html .= ("<li class='right disabled'><span>Next</span><span><i class='material-icons'>arrow_drop_down</i></span></li>");
			$html .= ("</ul>");
		}

		
		
		if($_REQUEST['location'] != "") {
			$strdbsql = "SELECT sitemap FROM property_locations WHERE recordID = :location";
			$params = array();
			$params['location'] = "".$_REQUEST['location']."";
			$propertyLocation = query($conn,$strdbsql,"single",$params);
			if($propertyLocation['sitemap'] != ""){
				
				$html .= ("<h4>Site Plan</h4>");
				
				$sitemaps = explode(",",$propertyLocation['sitemap']);
				foreach($sitemaps AS $sitemap){
					$html .= ("<br/><img src='".$sitemap."' />");
				}				
			}
		}
		
		
	} catch(Exception $e) {
		$html = ("<p>".$e->getMessage()."</p>");
	}
	
	$html .= ("<script>");
		$html .= ("$(document).ready(function() {");
			$html .= ("var data = {};");
			$html .= ("$('.pagination li span').click(function(){");
				$html .= ("data['pagination'] = $(this).data('target');");
				$html .= ("$.ajax({");
					$html .= ("url: '/includes/properties.php',");
					$html .= ("type: 'POST',");
					$html .= ("async: false,");
					$html .= ("cache: false,");
					$html .= ("timeout: 30000,");
					$html .= ("data: data,");
					$html .= ("success: function(response) {");
						$html .= ("$('#properties').html(response);");
					$html .= ("},");
					$html .= ("error: function() {");
						$html .= ("alert('Error finding properties!');");
					$html .= ("}");
				$html .= ("});");
			$html .= ("});");
		$html .= ("});");
				
	$html .= ("</script>");
	
	
	
	print($html);

?>