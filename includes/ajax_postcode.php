<?php
 
	include("incsitecommon.php");
 
	$bookConn = bookConnect();
 
	//sanitize data
	$postcode = $_REQUEST['postcode'];
	
	$part1 = substr($postcode, -7, 4);
	$part2 = substr($postcode, -3, 3);
	
	$likesearch = $part1."%".$part2;
	
	$strdbsql = "SELECT * FROM tbl_property WHERE fld_addressPostcode LIKE :likesearch LIMIT 1";
	$arrType = "multi";
	$dataParams = array("likesearch" => $likesearch);
	$resultdata = query($bookConn, $strdbsql, $arrType, $dataParams);
	
	if(count($resultdata) > 0){
		
		if(!empty($resultdata[0]['fld_addressNumber'])) print($resultdata[0]['fld_addressNumber']." ");
		if(!empty($resultdata[0]['fld_addressStreet'])) print($resultdata[0]['fld_addressStreet'].", ");
		if(!empty($resultdata[0]['fld_address2'])) print($resultdata[0]['fld_address2'].", ");
		if(!empty($resultdata[0]['fld_address3'])) print($resultdata[0]['fld_address3'].", ");
		if(!empty($resultdata[0]['fld_address4'])) print($resultdata[0]['fld_address4']);
		
	} else {
		
		print("Error");
		
	}
	
?>