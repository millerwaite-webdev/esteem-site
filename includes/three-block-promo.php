<?php

	print("<div class='desktopView' style='clear:both; text-align: center; '>");
		print("<div class=''>");
			print("<div class='row'>");
				print("<div class='col-xs-4'>");
					print("<div class='' style='background:white; padding: 20px; min-height: 485px;'>");
						print ("<img src='/images/showhome.png' width='100%' alt='View our stunning showhomes!' />");
						print("<br/><h4>View our stunning showhomes!</h4><br/>");
						print("<p style='text-align: center;'>Arrange a viewing for one of our stunning Esteem Showhomes.</p>");
						print("<p style='text-align: center;'>Call 01229 813036 to book your viewing today!</p>");
					print("</div>");
				print("</div>");
				print("<div class='col-xs-4'>");
					print("<div class='' style='background:white; padding: 20px; min-height: 485px;'>");
						print ("<img src='/images/kitchen.png' width='100%' alt='Award Winning Kitchens and appliances' />");
						print("<br/><h4>Award Winning Kitchens and appliances</h4><br/>");
						print("<p style='text-align: center;'>We fit all of our new build properties with award winning kitchen designs.</p>");
						print("<p style='text-align: center;'>Every kitchen is fitted with top of the range Zanussi appliances.</p>");
					print("</div>");
				print("</div>");
				print("<div class='col-xs-4'>");
					print("<div class='' style='background:white; padding: 20px; min-height: 485px;'>");
						print ("<img src='/images/home-lakedistrict.jpg' width='100%' alt='The Lake District National Park on your doorstep' />");
						print("<br/><h4>The Lake District National Park on your doorstep</h4><br/>");
						print("<p style='text-align: center;'>Our Barrow-in-Furness developments are minutes away from the stunning views and scenery of the Lake District National Park</p>");
					print("</div>");
				print("</div>");
				
				


			print("</div>");
		print("</div>");
	print("</div>");

print("<div class='mobileView'>");

print("<div style='height: 100%; width: 100%; display: flex '>");
	print("<div class='w-75' style='margin-bottom: 3%; padding-top: 3%;'>");
		print("<div class='card-body'>");
		print("<div style='float: right; width: 50%;'>");
			print("<h4 class='card-title mTitle' style='text-align: center;'>View our stunning showhomes!</h4>");
			print("<p class='mPara' style='text-align: center;'>Arrange a viewing for one of our stunning Esteem Showhomes.</p>");
			print("<p class='mPara' style='text-align: center;'>Call 01229 813036 to book your viewing today!</p>");
		print("</div>");
		print("<div style='float: left; width: 50%;'>");
			print("<img class='card-img-left' src='/images/showhome.png' width='100%'  alt='View our stunning showhomes!' />");
		print("</div>");
		print("</div>");
		print("</div>");
		print("</div>");
print("<div style='height: 100%; width: 100%; display: flex '>");		
		print("<div class='w-75' style='margin-bottom: 3%'>");
		print("<div class='card-body'>");
		print("<div style='float: right; width: 50%;'>");
			print("<h4 class='card-title mTitle' style='text-align: center;'>Award Winning Kitchens and appliances</h4>");
			print("<p  class='mPara' style='text-align: center;'>We fit all of our new build properties with award winning kitchen designs.</p>");
			print("<p class='mPara' style='text-align: center;'>Every kitchen is fitted with top of the range Zanussi appliances.</p>");
		print("</div>");
		print("<div style='float: left; width: 50%;'>");
			print("<img class='card-img-left' src='/images/kitchen.png' width='100%'  alt='Award Winning Kitchens and appliances' />");
		print("</div>");
		print("</div>");
		print("</div>");
		print("</div>");

print("<div style='height: 100%; width: 100%; display: flex '>");		
		print("<div class='w-75' style='margin-bottom: 3%;'>");
		print("<div class='card-body'>");
		print("<div style='float: right; width: 50%;'>");
			print("<h4 class='card-title mTitle' style='text-align: center;'>The Lake District National Park on your doorstep</h4>");
			print("<p class='mPara' style='text-align: center;'>Our Barrow-in-Furness developments are minutes away from the stunning views and scenery of the Lake District National Park</p>");
		print("</div>");
		print("<div style='float: left; width: 50%;'>");
			print("<img class='card-img-left' src='/images/home-lakedistrict.jpg' width='100%'  alt='The Lake District National Park on your doorstep' />");
		print("</div>");
	print("</div>");
print("</div>");
print("</div>");
		
		print("</div>");
		print("</div>");
	
?>