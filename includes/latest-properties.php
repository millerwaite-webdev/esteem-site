<?php

	//	LATEST PROPERTIES
	print("<div class='block-list coloured hidden-sm hidden-xs'>");
		
		switch($strPage) {
			default:
				$strdbsql = "SELECT DISTINCT tbl_property.fld_propertyID, tbl_property.*, tbl_residential.fld_propertyBedrooms AS bedrooms, tbl_residential.fld_displayPropertyType AS type, tbl_residentialSales.* FROM tbl_property
				INNER JOIN tbl_residential ON tbl_property.fld_propertyID = tbl_residential.fld_propertyID AND tbl_property.fld_department = 'Sales'
				INNER JOIN tbl_residentialSales ON tbl_property.fld_propertyID = tbl_residentialSales.fld_propertyID
				WHERE tbl_property.fld_department = 'Sales' AND tbl_residentialSales.fld_availability <> 4 AND tbl_residentialSales.fld_availability <> 5
				ORDER BY tbl_property.fld_dateLastModified DESC
				LIMIT 5";
			break;
			case "rental":
				$strdbsql = "SELECT DISTINCT tbl_property.fld_propertyID, tbl_property.*, tbl_residential.fld_propertyBedrooms AS bedrooms, tbl_residential.fld_displayPropertyType AS type, tbl_residentialLet.*, tbl_residentialLetRentFrequency.fld_value AS frequency FROM tbl_property 
				INNER JOIN tbl_residential ON tbl_property.fld_propertyID = tbl_residential.fld_propertyID
				INNER JOIN tbl_residentialLet ON tbl_property.fld_propertyID = tbl_residentialLet.fld_propertyID
				INNER JOIN tbl_residentialLetRentFrequency ON tbl_residentialLet.fld_rentFrequency = tbl_residentialLetRentFrequency.fld_counter
				WHERE tbl_property.fld_department = 'Lettings' AND tbl_residentialLet.fld_availability <> 4
				ORDER BY tbl_property.fld_dateLastModified DESC
				LIMIT 5";
			break;
			case "commercial":
				$strdbsql = "SELECT DISTINCT tbl_property.fld_propertyID, tbl_property.*, tbl_commercial.* FROM tbl_property 
				INNER JOIN tbl_commercial ON tbl_property.fld_propertyID = tbl_commercial.fld_propertyID
				WHERE tbl_property.fld_department = 'Commercial' AND tbl_commercial.fld_availability = 2
				ORDER BY tbl_property.fld_dateLastModified DESC
				LIMIT 5";
			break;
		}		
		
		$arrType = "multi";
		$resultdata = query($bookConn, $strdbsql, $arrType);
		
		if(count($resultdata) > 0) {
		
			print("<h5 class='featured'>Latest Properties</h5>");
			
			$i = 0;
			
			foreach($resultdata AS $row) {
				
				print("<div class='row'>");
					print("<div class='col-xs-12'>");
						print("<div class='block-prop featured'>");

							$strdbsql = "SELECT * FROM tbl_propertyImages
							WHERE fld_propertyID = :propID ORDER BY fld_counter";
							$arrType = "multi";
							$dataParams = array('propID' => $row['fld_propertyID']);
							$resultdata2 = query($bookConn, $strdbsql, $arrType, $dataParams);
								
							print("<div id='side-carousel-".$i."' class='carousel slide' data-ride='carousel' data-interval='false'>");
								print("<div class='carousel-inner' role='listbox'>");

									if(count($resultdata2) > 0) {
								
										$x = 0;
								
										foreach($resultdata2 AS $row2) {
										
											$propImage = str_replace("IMG_", "", substr($row2['fld_image'], 0, strpos($row2['fld_image'], '_', strpos($row2['fld_image'], '_')+1)));
										if(!empty($row2['fld_image'])) {
											print("<div class='item".($x == 0 ? " active" : "")."' style='background-image:url(https://rossestateagencies.com/datafeed/images/thumb/".$row2['fld_image'].");'>");
												print("<a href='/".($strPage == "search" ? "sales" : $strPage)."/".$row['fld_propertyID']."'></a>");
											print("</div>");
											
											$x++;
										}
										}
										
									} else {
										
										print("<div class='item active' style='background-color:white;background-image:url(https://rossestateagencies.com/datafeed/images/thumb/placeholder.jpg);background-size:contain;'>");
											print("<a href='/".($strPage == "search" ? "sales" : $strPage)."/".$row['fld_propertyID']."'></a>");
										print("</div>");
										
									}
								
									if(count($resultdata2)> 1) {
									
										print("<a class='left carousel-control' href='#side-carousel-".$i."' role='button' data-slide='prev'>");
											print("<span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>");
											print("<span class='sr-only'>Previous</span>");
										print("</a>");
										print("<a class='right carousel-control' href='#side-carousel-".$i."' role='button' data-slide='next'>");
											print("<span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>");
											print("<span class='sr-only'>Next</span>");
										print("</a>");
										
									}	
										
								print("</div>");								
							print("</div>");
							
							print("<a href='/".($strPage == "search" ? "sales" : $strPage)."/".$row['fld_propertyID']."' class='price'>");
							
								switch($strPage) {
									default:
										print("<span>&pound;".number_format($row['fld_price'])."</span>");
									break;
									case "rental":
										if($strPage == "rental") {
											if($row['frequency'] == "pw") $frequency = "Weekly";
											if($row['frequency'] == "pcm") $frequency = "Monthly";
											if($row['frequency'] == "pa") $frequency = "Yearly";
											print("<span>&pound;".number_format($row['fld_rent'])."</span>");
											print("<span style='font-size:14px;'>".$frequency."</span>");
										}
									break;
									case "commercial":
										if($row['fld_forSale'] == 1) {
											print("<span>&pound;".number_format($row['fld_priceTo'])."</span>");
										} else {
											if($row['fld_rentFrequency'] == "pw") $frequency = "Weekly";
											if($row['fld_rentFrequency'] == "pcm") $frequency = "Monthly";
											if($row['fld_rentFrequency'] == "pa") $frequency = "Yearly";
											print("<span>&pound;".number_format($row['fld_rentTo'])."</span>");
											print("<span>".$frequency."</span>");
										}
									break;
								}
								
							print("</a>");
							
							print("<div class='block-info'>");
								if($row['bedrooms'] && count($row['bedrooms'] > 0)) {
									print("<a class='top' href='".($strPage == "search" ? "sales" : $strPage)."/".$row['fld_propertyID']."'>");
										print("<span>".$row['bedrooms']." Bedroomed ".$row['type']."</span>");
									print("</a>");
								}
								print("<a class='head' href='/".($strPage == "search" ? "sales" : $strPage)."/".$row['fld_propertyID']."'>");
									print("<span>");
										if(!empty($row['fld_addressStreet'])) print($row['fld_addressStreet']."<br/>");
										if(!empty($row['fld_address2'])) print($row['fld_address2']."<br/>");
										if($row['fld_address3'] == "Walney") print($row['fld_address3'].", Barrow-in-Furness<br/>");
										else if(!empty($row['fld_address3'])) print($row['fld_address3']."<br/>");
										if(!empty($row['fld_addressPostcode'])) print($row['fld_addressPostcode']);
									print("</span>");
								print("</a>");
							print("</div>");
							print("<div class='block-button'>");
								print("<a href='".($strPage == "search" ? "sales" : $strPage)."/".$row['fld_propertyID']."'>View Property</a>");
							print("</div>");
						print("</div>");
					print("</div>");
				print("</div>");
			
				$i++;
			
			}
			
		}
		
	print("</div>");

?>