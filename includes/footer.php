<?php
	
/*	print("<footer class='page-footer'>");
		print("<div class='container'>");
		
			$strdbsql = "SELECT * FROM site_pages WHERE displayInFoot = 1 AND visible = 1";
			$result = query($conn,$strdbsql,"multi");
		
			print("<ul class='links'>");
				foreach($result AS $row) {
					print("<li><a href='/".$row['pageName']."'>".$row['metaPageLink']."</a></li>");
				}
			print("</ul>");
			
			print("<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.</p>");
			print("<ul>");
				print("<li><a href='#'>Ross Privacy Notice</a></li>");
				print("<li><a href='#'>Mortgage Privacy Notice</a></li>");
			print("</ul>");
		print("</div>");
		print("<div class='footer-copyright'>");
			print("<div class='container'>");
				print("© 2018 Copyright: <a href='".$strsiteurl."'>".$compName."</a>");
			print("</div>");
		print("</div>");
	print("</footer>");*/

	print("<div class='block-bottom'>");
		print("<div class='container'>");
			print("<div class='row'>");
				print("<div class='col-sm-8'>");
					print("<p>&copy; ".date("Y")." All Rights Reserved by</p>");
					print("<div class='box'>");
						//print("<span>".$compName." <a class='link' href='/files/gdpr-notification-2.pdf' target='_blank'>Privacy Notice</a></span>");
					print("</div>");
				print("</div>");
				print("<div class='col-sm-4'>");
					print("<p class='pull-right'><a href='https://millerwaite.co.uk/' target='_blank'>Powered by Miller Waite</a></p>");
				print("</div>");
			print("</div>");
		print("</div>");
	print("</div>");
	
	switch($strPage) {
		
		case "properties":
		case "sales":
		case "rental":
		case "commercial":
		case "search":
		
			?>
			
			<!-- jPList lib -->
			<script src="/js/jplist.core.min.js"></script>
			<script src="/js/jplist.core-ajax.min.js"></script>

			<script src="/js/jplist.bootstrap-filter-dropdown.min.js"></script>
			<script src="/js/jplist.filter-dropdown-bundle.min.js"></script>
			<script src="/js/jplist.filter-toggle-bundle.min.js"></script>
			<script src="/js/jplist.bootstrap-pagination-bundle.min.js"></script>
		<!--<script src="/js/jplist.sort-bundle.min.js"></script>-->
			<script src="/js/jplist.bootstrap-sort-dropdown.min.js"></script>
			<script src="/js/jplist.jquery-ui-bundle.min.js"></script>
			<script src="/js/filter.js"></script>
			
			<script>
			$(document).ready(function() {

				$('#sstc').change(function() {
					if($(this).is(':checked')) $('#sstc-on').click();
					else $('#sstc-off').click();
				});
				function kFormatter(num) {
					if(num > 999999) {
						var number = (num / 1000000).toFixed(1) + 'm';
					} else if(num > 999) {
						var number = (num / 1000).toFixed(1) + 'k';
					} else {
						var number = num;
					}
					return number;
				}
				jQuery.fn.jplist.settings = {
					bedroomSliderSales: function ($slider, $prev, $next){
						var myStorage = $.parseJSON(window.localStorage.getItem('storage-sales'));
						if(myStorage != null) {
							var bedroomSliderSales = myStorage[4];
						} else {
							var bedroomSliderSales = {"data":{"prev":0,"next":10}};
						}
						$slider.slider({
							min: 0
							,max: 10
							,range: true
							,values: [bedroomSliderSales.data.prev, bedroomSliderSales.data.next]
							,step: 1
							,slide: function (event, ui){
								$prev.text(ui.values[0] + " - ");
								$next.text(ui.values[1]);
							}
						});
					}
					,bedroomSliderRental: function ($slider, $prev, $next){
						var myStorage = $.parseJSON(window.localStorage.getItem('storage-rental'));
						if(myStorage != null) {
							var bedroomSliderRental = myStorage[4];
						} else {
							var bedroomSliderRental = {"data":{"prev":0,"next":10}};
						}
						$slider.slider({
							min: 0
							,max: 10
							,range: true
							,values: [bedroomSliderRental.data.prev, bedroomSliderRental.data.next]
							,step: 1
							,slide: function (event, ui){
								$prev.text(ui.values[0] + " - ");
								$next.text(ui.values[1]);
							}
						});
					}
					,bedroomSliderSearch: function ($slider, $prev, $next){
						var myStorage = $.parseJSON(window.localStorage.getItem('storage-search'));
						if(myStorage != null) {
							var bedroomSliderSearch = myStorage[4];
						} else {
							var bedroomSliderSearch = {"data":{"prev":0,"next":10}};
						}
						$slider.slider({
							min: 0
							,max: 10
							,range: true
							,values: [bedroomSliderSearch.data.prev, bedroomSliderSearch.data.next]
							,step: 1
							,slide: function (event, ui){
								$prev.text(ui.values[0] + " - ");
								$next.text(ui.values[1]);
							}
						});
					}
					,bedroomValues: function ($slider, $prev, $next){
						$prev.text($slider.slider('values', 0) + " - ");
						$next.text($slider.slider('values', 1));
					}
					,priceSliderSales: function ($slider, $prev, $next){
						var myStorage = $.parseJSON(window.localStorage.getItem('storage-sales'));
						if(myStorage != null) {
							var priceSlider = myStorage[5];
						} else {
							var priceSlider = {"data":{"prev":0,"next":1000000}};
						}
						$slider.slider({
							min: 0
							,max: 1000000
							,range: true
							,values: [priceSlider.data.prev, priceSlider.data.next]
							,step: 25000
							,slide: function (event, ui){
								$prev.text("£" + kFormatter(ui.values[0]) + " - ");
								$next.text("£" + kFormatter(ui.values[1]));
							}
						});
					}
					,priceSliderCommercial: function ($slider, $prev, $next){
						var myStorage = $.parseJSON(window.localStorage.getItem('storage-commercial'));
						if(myStorage != null) {
							var priceSlider = myStorage[5];
						} else {
							var priceSlider = {"data":{"prev":0,"next":1000000}};
						}
						$slider.slider({
							min: 0
							,max: 1000000
							,range: true
							,values: [priceSlider.data.prev, priceSlider.data.next]
							,step: 25000
							,slide: function (event, ui){
								$prev.text("£" + kFormatter(ui.values[0]) + " - ");
								$next.text("£" + kFormatter(ui.values[1]));
							}
						});
					}
					,priceSliderSearch: function ($slider, $prev, $next){
						var myStorage = $.parseJSON(window.localStorage.getItem('storage-search'));
						if(myStorage != null) {
							var priceSlider = myStorage[5];
						} else {
							var priceSlider = {"data":{"prev":0,"next":1000000}};
						}
						$slider.slider({
							min: 0
							,max: 1000000
							,range: true
							,values: [priceSlider.data.prev, priceSlider.data.next]
							,step: 25000
							,slide: function (event, ui){
								$prev.text("£" + kFormatter(ui.values[0]) + " - ");
								$next.text("£" + kFormatter(ui.values[1]));
							}
						});
					}
					,priceValues: function ($slider, $prev, $next){
						$prev.text("£" + kFormatter($slider.slider('values', 0)) + " - ");
						$next.text("£" + kFormatter($slider.slider('values', 1)));
					}
					,rentalSlider: function ($slider, $prev, $next){
						var myStorage = $.parseJSON(window.localStorage.getItem('storage-rental'));
						if(myStorage != null) {
							var rentalSlider = myStorage[5];
						} else {
							var rentalSlider = {"data":{"prev":0,"next":5000}};
						}
						$slider.slider({
							min: 0
							,max: 5000
							,range: true
							,values: [rentalSlider.data.prev, rentalSlider.data.next]
							,step: 100
							,slide: function (event, ui){
								$prev.text("£" + kFormatter(ui.values[0]) + " - ");
								$next.text("£" + kFormatter(ui.values[1]));
							}
						});
					}
					,rentalValues: function ($slider, $prev, $next){
						$prev.text("£" + kFormatter($slider.slider('values', 0)) + " - ");
						$next.text("£" + kFormatter($slider.slider('values', 1)));
					}
				};
				$('#prop-sales').jplist({
					itemsBox: '.list-sales'
					,itemPath: '.block-prop-sales'
					,panelPath: '.jplist-panel-sales'
					,animateToTop: '.block-banner'
					,storage: 'localstorage'
					,storageName: 'storage-sales'
					,dataSource: {
						type: 'server'
						,server: {
							ajax:{
								url: '/server/server-html-sales.php'
								,dataType: 'html'
								,type: 'POST'
							}
							,serverOkCallback: function(serverData, statuses, ajax, response){
								//server callback in case of success
								//alert("Success");
							}
							,serverErrorCallback: function(statuses){
								//server callback in case of fail
								//alert("Failed");
							}
						}
						//render function for json + templates like handlebars, xml + xslt etc.
						,render: null
					}
				});
				$('#prop-rental').jplist({
					itemsBox: '.list-rental'
					,itemPath: '.block-prop-rental'
					,panelPath: '.jplist-panel-rental'
					,animateToTop: '.block-banner'
					,storage: 'localstorage'
					,storageName: 'storage-rental'
					,dataSource: {
						type: 'server'
						,server: {
							ajax:{
								url: '/server/server-html-rental.php'
								,dataType: 'html'
								,type: 'POST'
							}
							,serverOkCallback: function(serverData, statuses, ajax, response){
								//server callback in case of success
								//alert("Success");
							}
							,serverErrorCallback: function(statuses){
								//server callback in case of fail
								//alert("Failed");
							}
						}
						//render function for json + templates like handlebars, xml + xslt etc.
						,render: null
					}
				});
				$('#prop-commercial').jplist({
					itemsBox: '.list-commercial'
					,itemPath: '.block-prop-commercial'
					,panelPath: '.jplist-panel-commercial'
					,animateToTop: '.block-banner'
					,storage: 'localstorage'
					,storageName: 'storage-commercial'
					,dataSource: {
						type: 'server'
						,server: {
							ajax:{
								url: '/server/server-html-commercial.php'
								,dataType: 'html'
								,type: 'POST'
							}
							,serverOkCallback: function(serverData, statuses, ajax, response){
								//server callback in case of success
							}
							,serverErrorCallback: function(statuses){
								//server callback in case of fail
								//alert("Failed");
							}
						}
						//render function for json + templates like handlebars, xml + xslt etc.
						,render: null
					}
				});
				$('#prop-search').jplist({
					itemsBox: '.list-search'
					,itemPath: '.block-prop-search'
					,panelPath: '.jplist-panel-search'
					,animateToTop: '.block-banner'
					,dataSource: {
						type: 'server'
						,server: {
							ajax:{
								url: '/server/server-html-search.php'
								,dataType: 'html'
								,type: 'POST'
							}
							,serverOkCallback: function(serverData, statuses, ajax, response){
								//server callback in case of success
								//alert("Success");
							}
							,serverErrorCallback: function(statuses){
								//server callback in case of fail
								//alert("Failed");
							}
						}
						//render function for json + templates like handlebars, xml + xslt etc.
						,render: null
					}
				});
				if($('#sstc-on').is(':checked')) $('#sstc').prop('checked', true);
				else $('#sstc').prop('checked', false);
				
				
				// Slick slider
				$(".slick.main").slick({
					adaptiveHeight: true,
					arrows: true,
					asNavFor: '.slick.carousel',
					centerMode: false,
					dots: false,
					draggable: false,
					prevArrow: "<button class='slick-arrow slick-prev'><i class='material-icons'>keyboard_arrow_left</i></button>",
					nextArrow: "<button class='slick-arrow slick-next'><i class='material-icons'>keyboard_arrow_right</i></button>",
					slidesToShow: 1,
					slidesToScroll: 1,
					vertical: false
				});
				
				// Slick carousel
				$('.slick.carousel').slick({
					adaptiveHeight: true,
					arrows: false,
					asNavFor: '.slick.main',
					centerMode: false,
					dots: false,
					draggable: false,
					focusOnSelect: true,
					prevArrow: "<button class='slick-arrow slick-prev'><i class='material-icons'>keyboard_arrow_left</i></button>",
					nextArrow: "<button class='slick-arrow slick-next'><i class='material-icons'>keyboard_arrow_right</i></button>",
					slidesToShow: 4,
					slidesToScroll: 1
				});
				
				
			});
			</script>

			

			
			
			
			<?php
		
		break;
		
		case "value-my-property":
		
			?>
			
			<script>
			$('.dropdown-menu li').click(function(){
				var button = $(this).closest(".dropdown").find("button").attr("id");
				$("#" + button).removeClass("select-default");
				$("#" + button + " span").text($(this).text());
			});
			$(function(){
				var txt = $("#PostZipCode");
				var func = function(e) {
					if(e.keyCode === 32){
						txt.val(txt.val().replace(/\s/g, ''));
					}
				}
				txt.keyup(func).blur(func);
			});
			$('#valuation-form').submit(function (e) {
				e.preventDefault();
				var nameval = $('#valuation-form #val_name').val();
				var emailval = $('#valuation-form #val_email').val();
				var phoneval = $('#valuation-form #val_phone').val();
				var addressval = $('#valuation-form #val_address').val();
				var postcodeval = $('#valuation-form #PostZipCode').val();
				var propertystat = $('#valuation-form #val_property').hasClass("select-default");
				var propertyval = $('#valuation-form #val_property span').text();
				var timestat = $('#valuation-form #val_time').hasClass("select-default");
				var timeval = $('#valuation-form #val_time span').text();
				var messageval = $('#valuation-form #val_msg').val();
				if (nameval !== "" && emailval !== "" && phoneval !== "" && addressval !== "" && postcodeval !== "" && propertystat == false && timestat == false) {
					if( /(.+)@(.+){2,}\.(.+){2,}/.test(emailval) ) {
						$.ajax({
							cache: false,
							url: '/includes/ajax_valuation.php',
							type: 'POST',
							dataType: 'text',
							data: {
								type: "Valuation",
								name: nameval,
								email: emailval,
								phone: phoneval,
								address: addressval,
								postcode: postcodeval,
								property: propertyval,
								time: timeval,
								message: messageval
							},
							success: function (data) {
								if(data == "error"){
									$('#valuation-form #val_submit').addClass('error');
									$('#valuation-form #val_submit').attr('disabled', true);
									$('#valuation-form #val_submit').text("Some of these details have already been registered");
									$('#valuation-form #val_name').attr('readonly', true);
									$('#valuation-form #val_phone').attr('readonly', true);
									$('#valuation-form #val_email').attr('readonly', true);
									$('#valuation-form #val_msg').attr('readonly', true);
									setTimeout(function(){
										$('#valuation-form #val_submit').removeClass('error');
										$('#valuation-form #val_submit').attr('disabled', false);
										$('#valuation-form #val_submit').text("Submit");
										$('#valuation-form #val_name').attr('readonly', false);
										$('#valuation-form #val_phone').attr('readonly', false);
										$('#valuation-form #val_email').attr('readonly', false);
										$('#valuation-form #val_msg').attr('readonly', false);
									}, 6000);
								} else {
									$('#valuation-form #val_submit').addClass('success');
									$('#valuation-form #val_submit').attr('disabled', true);
									$('#valuation-form #val_submit').text("We will contact you as soon as possible to handle your request");
									$('#valuation-form #val_name').attr('readonly', true);
									$('#valuation-form #val_email').attr('readonly', true);
									$('#valuation-form #val_phone').attr('readonly', true);
									$('#valuation-form #val_address').attr('readonly', true);
									$('#valuation-form #PostZipCode').attr('readonly', true);
									$('#valuation-form #val_property').attr('disabled', true);
									$('#valuation-form #val_date').attr('readonly', true);
									$('#valuation-form #val_time').attr('disabled', true);
									$('#valuation-form #val_msg').attr('readonly', true);
								}
							},
							error: function (e) {
								alert("Error");
							}
						});
					} else {
						setTimeout(function(){
						$('#valuation-form #val_submit').attr('disabled', true);
						$('#valuation-form #val_email').attr('readonly', true);
						$('#valuation-form .val_email .error-icon').fadeIn(300);
						$('#valuation-form #val_email').addClass('error');
					}, 200);
					setTimeout(function(){
						$('#valuation-form .val_email .error-icon').fadeOut(300);
						$('#valuation-form #val_email').removeClass('error');
						setTimeout(function(){
							$('#valuation-form #val_submit').attr('disabled', false);
							$('#valuation-form #val_email').attr('readonly', false);
						}, 200);
					}, 6000);
					}
				} else {
					if(nameval == "") {
						setTimeout(function(){
							$('#valuation-form #val_submit').attr('disabled', true);
							$('#valuation-form #val_name').attr('readonly', true);
							$('#valuation-form .val_name .error-icon').fadeIn(300);
							$('#valuation-form #val_name').addClass('error');
						}, 200);
						setTimeout(function(){
							$('#valuation-form .val_name .error-icon').fadeOut(300);
							$('#valuation-form #val_name').removeClass('error');
							setTimeout(function(){
								$('#valuation-form #val_submit').attr('disabled', false);
								$('#valuation-form #val_name').attr('readonly', false);
							}, 200);
						}, 6000);
					}
					if(emailval == "") {
						setTimeout(function(){
							$('#valuation-form #val_submit').attr('disabled', true);
							$('#valuation-form #val_email').attr('readonly', true);
							$('#valuation-form .val_email .error-icon').fadeIn(300);
							$('#valuation-form #val_email').addClass('error');
						}, 200);
						setTimeout(function(){
							$('#valuation-form .val_email .error-icon').fadeOut(300);
							$('#valuation-form #val_email').removeClass('error');
							setTimeout(function(){
								$('#valuation-form #val_submit').attr('disabled', false);
								$('#valuation-form #val_email').attr('readonly', false);
							}, 200);
						}, 6000);
					}
					if(phoneval == "") {
						setTimeout(function(){
							$('#valuation-form #val_submit').attr('disabled', true);
							$('#valuation-form #val_phone').attr('readonly', true);
							$('#valuation-form .val_phone .error-icon').fadeIn(300);
							$('#valuation-form #val_phone').addClass('error');
						}, 200);
						setTimeout(function(){
							$('#valuation-form .val_phone .error-icon').fadeOut(300);
							$('#valuation-form #val_phone').removeClass('error');
							setTimeout(function(){
								$('#valuation-form #val_submit').attr('disabled', false);
								$('#valuation-form #val_phone').attr('readonly', false);
							}, 200);
						}, 6000);
					}
					if(addressval == "") {
						setTimeout(function(){
							$('#valuation-form #val_submit').attr('disabled', true);
							$('#valuation-form #val_address').attr('readonly', true);
							$('#valuation-form .val_address .error-icon').fadeIn(300);
							$('#valuation-form #val_address').addClass('error');
						}, 200);
						setTimeout(function(){
							$('#valuation-form .val_address .error-icon').fadeOut(300);
							$('#valuation-form #val_address').removeClass('error');
							setTimeout(function(){
								$('#valuation-form #val_submit').attr('disabled', false);
								$('#valuation-form #val_address').attr('readonly', false);
							}, 200);
						}, 6000);
					}
					if(postcodeval == "") {
						setTimeout(function(){
							$('#valuation-form #val_submit').attr('disabled', true);
							$('#valuation-form #PostZipCode').attr('readonly', true);
							$('#valuation-form .val_postcode .error-icon').fadeIn(300);
							$('#valuation-form #PostZipCode').addClass('error');
						}, 200);
						setTimeout(function(){
							$('#valuation-form .val_postcode .error-icon').fadeOut(300);
							$('#valuation-form #PostZipCode').removeClass('error');
							setTimeout(function(){
								$('#valuation-form #val_submit').attr('disabled', false);
								$('#valuation-form #PostZipCode').attr('readonly', false);
							}, 200);
						}, 6000);
					}
					if(propertystat == true) {
						setTimeout(function(){
							$('#valuation-form #val_submit').attr('disabled', true);
							$('#valuation-form #val_property').attr('disabled', true);
							$('#valuation-form .val_property .error-icon').fadeIn(300);
							$('#valuation-form #val_property').addClass('error');
						}, 200);
						setTimeout(function(){
							$('#valuation-form .val_property .error-icon').fadeOut(300);
							$('#valuation-form #val_property').removeClass('error');
							setTimeout(function(){
								$('#valuation-form #val_submit').attr('disabled', false);
								$('#valuation-form #val_property').attr('disabled', false);
							}, 200);
						}, 6000);
					}
					if(timestat == true) {
						setTimeout(function(){
							$('#valuation-form #val_submit').attr('disabled', true);
							$('#valuation-form #val_time').attr('disabled', true);
							$('#valuation-form .val_time .error-icon').fadeIn(300);
							$('#valuation-form #val_time').addClass('error');
						}, 200);
						setTimeout(function(){
							$('#valuation-form .val_time .error-icon').fadeOut(300);
							$('#valuation-form #val_time').removeClass('error');
							setTimeout(function(){
								$('#valuation-form #val_submit').attr('disabled', false);
								$('#valuation-form #val_time').attr('disabled', false);
							}, 200);
						}, 6000);
					}
				}
			});
			</script>
			
			<?php
		
		break;
		
	}

?>	

	<script src="/js/slick.min.js"></script>
	<script>

	<?php 
		if ($strPage == "index") {
			print ("$(document).ready(function(){");
				print ("$(\"#myModal\").modal('show');");
			print ("});");
		}
	?>
		
	$(document).on('ready', function() {
		$(".slick-carousel").slick({
			arrows: false,
			autoplay: true,
			autoplaySpeed: 4000,
			dots: false,
			draggable: true,
			infinite: true,
			centerMode: false,
			slidesToShow: 5,
			slidesToScroll: 1,
			swipeToSlide: true
		});
    });
	</script>
	
	<script>
	// Fixed elements
	/*var topofHead = $("header").offset().top;
	var heightofHead = $("header").outerHeight();
	
	// Fixed navbar on page scroll
	$(window).scroll(function(){
		if($(window).scrollTop() > (topofHead + heightofHead)){
			$("body").addClass('fixed-head');
		} else {
			$("body").removeClass('fixed-head');
		}
	});
	
	// Fixed navbar on page load
	$(document).ready(function(){
		if($(window).scrollTop() > (topofHead + heightofHead)){
			$("body").addClass('fixed-head');
		} else {
			$("body").removeClass('fixed-head');
		}
	});*/
	
	$(document).ready(function() {
		$('.dropdown-toggle').dropdown();
	});
	
	function ReplaceNumberWithCommas(yourNumber) {
		//Seperates the components of the number
		var n= yourNumber.toString().split(".");
		//Comma-fies the first part
		n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		//Combines the two sections
		return n.join(".");
	}
	function kFormatter(num) {
		if(num > 999999) {
			var number = (num / 1000000).toFixed(1) + 'm';
		} else if(num > 999) {
			var number = (num / 1000).toFixed(1) + 'k';
		} else {
			var number = num;
		}
		return number;
	}
	$(function() {
		$("#slider").slider({
			value : 1,
			min: 1,
			max: 10,
			step: 1,
			slide: function( event, ui ) {
				$("#bedrooms").val(ui.value + "+");
			}
		});
		$("#bedrooms").val($("#slider").slider("value") + "+");
	});
	$(function() {
		$("#slider-range").slider({
			range: true,
			min: 0,
			max: 1000000,
			values: [ 0, 1000000 ],
			step: 25000,
			slide: function( event, ui ) {
				$("#price").val("£" + kFormatter(ui.values[0]) + " - £" + kFormatter(ui.values[1]));
				$("#min").val(ui.values[0]);
				$("#max").val( ui.values[1]);
			}
		});
		$("#price").val("£" + kFormatter($("#slider-range").slider("values", 0)) + " - £" + kFormatter($("#slider-range").slider("values", 1)));
		$("#min").val($("#slider-range").slider("values", 0));
		$("#max").val($("#slider-range").slider("values", 1));
	});
	$(function() {
		$("#rental-range").slider({
			range: true,
			min: 0,
			max: 50000,
			values: [ 0, 50000 ],
			step: 1000,
			slide: function( event, ui ) {
				$("#rental-price").val("£" + kFormatter(ui.values[0]) + " - £" + kFormatter(ui.values[1]));
				$("#rental-min").val(ui.values[0]);
				$("#rental-max").val( ui.values[1]);
			}
		});
		$("#rental-price").val("£" + kFormatter($("#rental-range").slider("values", 0)) + " - £" + kFormatter($("#rental-range").slider("values", 1)));
		$("#rental-min").val($("#rental-range").slider("values", 0));
		$("#rental-max").val($("#rental-range").slider("values", 1));
	});
	$('.dropdown-menu li').click(function(){
		$('#call-drop span').text($(this).text());
		$('.selected').removeClass('selected');
		$(this).addClass('selected');
	});
	$('#callback-form').submit(function (e) {
		e.preventDefault();
		var typeval = $('#callback-form #call_type').val();
		var nameval = $('#callback-form #call_name').val();
		var emailval = $('#callback-form #call_email').val();
		var phoneval = $('#callback-form #call_phone').val();
		var addressval = $('#callback-form #call_add').val();
		var messageval = $('#callback-form #call_msg').val();
		if (nameval !== "" && phoneval !== "" && emailval !== "") {
			if( /(.+)@(.+){2,}\.(.+){2,}/.test(emailval) ) {
				$.ajax({
					cache: false,
					url: '/includes/ajax_callback.php',
					type: 'POST',
					dataType: 'text',
					data: {
						type: typeval,
						name: nameval,
						email: emailval,
						phone: phoneval,
						property: addressval,
						message: messageval,
						captcha: grecaptcha.getResponse()
					},
					success: function (data) {
						if(data == "error"){
							$('#callback-form #call_submit').addClass('error');
							$('#callback-form #call_submit').attr('disabled', true);
							$('#callback-form #call_submit').text("Some of these details have already been registered");
							$('#callback-form #call_name').attr('readonly', true);
							$('#callback-form #call_phone').attr('readonly', true);
							$('#callback-form #call_email').attr('readonly', true);
							$('#callback-form #call_add').attr('readonly', true);
							$('#callback-form #call_msg').attr('readonly', true);
							setTimeout(function(){
								$('#callback-form #call_submit').removeClass('error');
								$('#callback-form #call_submit').attr('disabled', false);
								$('#callback-form #call_submit').text("Submit");
								$('#callback-form #call_name').attr('readonly', false);
								$('#callback-form #call_phone').attr('readonly', false);
								$('#callback-form #call_email').attr('readonly', false);
								$('#callback-form #call_add').attr('readonly', false);
								$('#callback-form #call_msg').attr('readonly', false);
							}, 6000);
						} else if(data == 0) {
							$('#callback-form #call_submit').addClass('error');
							$('#callback-form #call_submit').attr('disabled', true);
							$('#callback-form #call_submit').text("Please prove you are not a robot");
							setTimeout(function(){
								$('#callback-form #call_submit').removeClass('error');
								$('#callback-form #call_submit').attr('disabled', false);
								$('#callback-form #call_submit').text("Submit");
							}, 6000);
						} else {
							$('#callback-form #call_submit').addClass('success');
							$('#callback-form #call_submit').attr('disabled', true);
							$('#callback-form #call_submit').text("We will contact you as soon as possible to handle your request");
							$('#callback-form #call_name').attr('readonly', true);
							$('#callback-form #call_phone').attr('readonly', true);
							$('#callback-form #call_email').attr('readonly', true);
							$('#callback-form #call_add').attr('readonly', true);
							$('#callback-form #call_msg').attr('readonly', true);
						}
					},
					error: function (e) {
						alert("Error");
					}
				});
			} else {
				setTimeout(function(){
					$('#callback-form #call_submit').attr('disabled', true);
					$('#callback-form #call_email').attr('readonly', true);
					$('#callback-form .call_email .error-icon').fadeIn(300);
					$('#callback-form #call_email').addClass('error');
				}, 200);
				setTimeout(function(){
					$('#callback-form .call_email .error-icon').fadeOut(300);
					$('#callback-form #call_email').removeClass('error');
					setTimeout(function(){
						$('#callback-form #call_submit').attr('disabled', false);
						$('#callback-form #call_email').attr('readonly', false);
					}, 200);
				}, 6000);
			}
		}
		if(nameval == "") {
			setTimeout(function(){
				$('#callback-form #call_submit').attr('disabled', true);
				$('#callback-form #call_name').attr('readonly', true);
				$('#callback-form .call_name .error-icon').fadeIn(300);
				$('#callback-form #call_name').addClass('error');
			}, 200);
			setTimeout(function(){
				$('#callback-form .call_name .error-icon').fadeOut(300);
				$('#callback-form #call_name').removeClass('error');
				setTimeout(function(){
					$('#callback-form #call_submit').attr('disabled', false);
					$('#callback-form #call_name').attr('readonly', false);
				}, 200);
			}, 6000);
		}
		if(phoneval == "") {
			setTimeout(function(){
				$('#callback-form #call_submit').attr('disabled', true);
				$('#callback-form #call_phone').attr('readonly', true);
				$('#callback-form .call_phone .error-icon').fadeIn(300);
				$('#callback-form #call_phone').addClass('error');
			}, 200);
			setTimeout(function(){
				$('#callback-form .call_phone .error-icon').fadeOut(300);
				$('#callback-form #call_phone').removeClass('error');
				setTimeout(function(){
					$('#callback-form #call_submit').attr('disabled', false);
					$('#callback-form #call_phone').attr('readonly', false);
				}, 200);
			}, 6000);
		}
		if(emailval == "") {
			setTimeout(function(){
				$('#callback-form #call_submit').attr('disabled', true);
				$('#callback-form #call_email').attr('readonly', true);
				$('#callback-form .call_email .error-icon').fadeIn(300);
				$('#callback-form #call_email').addClass('error');
			}, 200);
			setTimeout(function(){
				$('#callback-form .call_email .error-icon').fadeOut(300);
				$('#callback-form #call_email').removeClass('error');
				setTimeout(function(){
					$('#callback-form #call_submit').attr('disabled', false);
					$('#callback-form #call_email').attr('readonly', false);
				}, 200);
			}, 6000);
		}
	});
	</script>

</body>

</html>