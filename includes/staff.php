<?php
		
	$strdbsql = "SELECT * FROM site_staff_members ORDER BY fld_order";
	$returnType = "multi";
	$staffMembers = query ($conn, $strdbsql, $returnType);

	$staff = array();
	
	$arrNo = 0;
	
	foreach($staffMembers AS $staffMember) {
	
		$staff[$arrNo] = array(
			"staffName"=>$staffMember['fld_name'],
			"staffTitle"=>$staffMember['fld_title'],
			"staffQual"=>$staffMember['fld_qualifications'],
			"staffImage"=>$staffMember['fld_image'],
			"staffBio"=>preg_replace('@\>\s{1,}\<@','><',$staffMember['fld_bio'])
		);
	
		$arrNo++;
	
	}

	$i = 0;
	$total = count($staff);

	print("<div class='staff-all'>");
		print("<div class='container'>");
		
			foreach($staff AS $member)
			{
				print("<div class='staff-con'>");
					print("<div class='staff-img' style='background-image: url(/images/staff/".$member['staffImage'].");'>");
						print("<a href='#' data-key='".$i."' data-toggle='modal' data-target='#staff-modal' style='outline: none;'><i class='fa fa-expand' aria-hidden='true'></i></a>");
					print("</div>");
					print("<div class='staff-txt'>");
						print("<h2>".$member['staffName']."</h2>");
						print("<h3>".$member['staffTitle']."</h3>");
						print("<a href='#'>Email Me</a>");
					print("</div>");
				print("</div>");
				
				$i++;
			}
			
		print("</div>");
	print("</div>");
	
	print("<div class='modal fade' id='staff-modal' tabindex='-1' role='dialog' aria-labelledby='myLargeModalLabel'>");
		print("<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span></button>");
		print("<div class='modal-dialog'>");
			print("<div class='container'>");
				print("<div class='row'>");
					print("<div class='col-sm-3'>");
						print("<div id='staff-img' style='background-image: url(/images/staff/default.png);'></div>");
					print("</div>");
					print("<div class='col-sm-9'>");
						print("<h1 id='staff-name'></h1>");
						print("<h2>");
							print("<span id='staff-title'></span>");
							print("<span id='staff-qual'></span>");
						print("</h2>");
						print("<div id='staff-bio'></div>");
						print("<div class='scroll' style='display:none;'>");
							print("<div class='mouse'>");
								print("<div class='wheel'></div>");
							print("</div>");
							print("<div>");
								print("<span class='unu'></span>");
								print("<span class='doi'></span>");
								print("<span class='trei'></span>");
							print("</div>");
						print("</div>");
					print("</div>");
				print("</div>");
			print("</div>");
		print("</div>");
	print("</div>");

?>

	<script type="text/javascript">
	
	var staff = [];
	
<?php

	foreach($staff AS $key => $member)
	{
		print("staff[".$key."] = [];");
		print("staff[".$key."][\"staffName\"]=\"".$member['staffName']."\";");
		print("staff[".$key."][\"staffTitle\"]=\"".$member['staffTitle']."\";");
		print("staff[".$key."][\"staffImage\"]=\"".$member['staffImage']."\";");
		print("staff[".$key."][\"staffQual\"]=\"".$member['staffQual']."\";");
		print("staff[".$key."][\"staffBio\"]=\"".$member['staffBio']."\";");
	}
	
?>
	
	$(".staff-img a").click(function(){
		var staffKey = $(this).attr("data-key");
		var staffData = staff[staffKey];
		var staffName = staffData["staffName"];
		
		$("#staff-name").text(staffData["staffName"]);
		$("#staff-title").text(staffData["staffTitle"]);
		$("#staff-img").css("background-image", "url(/images/staff/"+staffData["staffImage"]+")");
		$("#staff-qual").text(staffData["staffQual"]);
		$("#staff-bio").html(staffData["staffBio"]);
		
		setTimeout(function(){
			var totalHeight = 0;
			
			$("#staff-bio").children().each(function(){
				totalHeight = totalHeight + $(this).outerHeight(true);
			});	
			
			if(totalHeight > 346){
				$(".scroll").fadeIn(500);
			}
			
		}, 300);
		
		$("#staff-modal").on('hidden.bs.modal', function(){
			$(".scroll").hide();
		});

		$('#staff-bio').on('scroll', function() {
			if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
				$(".scroll").fadeOut(300);
			}
			else{
				$(".scroll").fadeIn(300);
			}
		})
	});
	
	</script>