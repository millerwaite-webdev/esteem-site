<?php

	$offices = array();
	
	$offices[0] = array("officeName"=>"Barrow", "officeAddress"=>"New Homes Centre, 67 Duke Street, Barrow-in-Furness, LA14 1RW", "officeHours"=>"9.00 - 17.00 any day of the week", "officeNumber"=>"01229 813036", "officeMap"=>"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26460.31733344065!2d-3.253117417963338!3d54.11303695214992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487ca8db5d51783b%3A0x949995a1f0463a55!2sRoss+Estate+Agencies+-+Barrow+Branch!5e0!3m2!1sen!2suk!4v1487936094842", "officeDirections"=>"https://www.google.co.uk/maps/place/Ross+Estate+Agencies+-+Barrow+Branch/@54.1131667,-3.230544,17z/data=!3m1!4b1!4m5!3m4!1s0x487ca8db5d51783b:0x949995a1f0463a55!8m2!3d54.1131667!4d-3.2283553");

	if(count($offices) > 0) {
	
		$i = 0;
	
		print("<div class='block-text'>");
			if($strPage == "contact") print("<div class='container'>");
			
				foreach($offices AS $office) {
			
					if($i % 3 == 0) print("<div class='row' style='margin-bottom:0;'>");
						print("<div class='col-md-4'>");
							print("<div class='block-office'>");
								print("<div>");
									print("<h3 style='text-align:left;'>".$office['officeName']." Office</h3>");
									print("<ul>");
										print("<li>");
											print("<i class='fa fa-map-marker' aria-hidden='true'></i>");
											print("<span>".str_replace(", ", "<br/>", $office['officeAddress'])."</span>");
										print("</li>");
										print("<li>");
											print("<i class='fa fa-clock-o' aria-hidden='true'></i>");
											print("<span>".str_replace(", ", "<br/>", $office['officeHours'])."</span>");
										print("</li>");
										print("<li>");
											print("<a href='tel:".str_replace(" ", "", $office['officeNumber'])."'>");
												print("<i class='fa fa-phone' aria-hidden='true'></i>");
												print("<span>".$office['officeNumber']."</span>");
											print("</a>");
										print("</li>");
									print("</ul>");
								print("</div>");
							print("</div>");
						print("</div>");
					if($i % 3 == 2) print("</div>");
					
					$i++;
					
				}
				
			if($strPage == "contact") print("</div>");
		print("</div>");
		
	}

?>
