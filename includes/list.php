<?php

	if(isset($_REQUEST['id'])) $listID = $_REQUEST['id']; else $listID = "";
	
	if(!empty($listID)) {

		$getArticleQuery = "SELECT * FROM properties WHERE egnID = :listID";
		$arrParam = array("listID" => $listID);
		$articleData = query($conn, $getArticleQuery, "single", $arrParam);
		
		if(!empty($articleData))
		{
			$id = $articleData['egnID'];
			
			$name = $articleData['p_name'];
			$area = $articleData['p_area'];
			$postcode = $articleData['postcode'];
			$type = $articleData['prop_type'];
			$price = $articleData['prop_price'];
			$beds = $articleData['No_beds'];
			$baths = $articleData['No_baths'];
			$shortDescription = $articleData['short_description'];
			$longDescription = str_replace("&nbsp;", "", preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', strip_tags($articleData['long_description'], "<p>")));
			
			$imageID = $articleData['rs_id'];
			$mainImage = $articleData['main_image'];
			$mainTitle = $articleData['main_image_title'];
			
			$offerID = $articleData['under_offer'];
			$recent = $articleData['recent'];
			$status = $articleData['sold'];
			$tenure = $articleData['prop_tenure'];
			$enabled = $articleData['make_live'];
			
			$brochure = $articleData['brochure'];
			
			$floorplan = $articleData['floorplan'];
			$print = $articleData['print_image'];
			
			$metaTitle = $articleData['metatitle'];
			$metaDescription = $articleData['metadescription'];
		}
		else
		{
			$name = "Property Not Found";
		}
		
		print("<div class='row'>");
			print("<div class='col s12 m8'>");
				print("<h3>".$name."</h3>");
				print("<h2>".$area.(!empty($postcode) ? ", ".$postcode : "")."</h2>");
				
				$strdbsql = "SELECT * FROM property_images WHERE rs_id = :imageID AND make_live = 'yes' ORDER BY image_order ASC";
				$strType = "multi";
				$dataParams = array("imageID"=>$imageID);
				$images = query($conn, $strdbsql, $strType, $dataParams);
				
				if(count($images) > 0) {
					print("<div class='slick main'>");
						foreach($images AS $image) {
						//	print($_SERVER['DOCUMENT_ROOT']."/images/properties/".$image['image_ref'].".jpg");
							print("<div>");
								
								if($image['url'] != ""){
									print("<img src='".$image['url']."' alt='".$image['image_title']."'>");
								} else {
							
									if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/properties/".$image['image_ref'].".png")) {
										print("<img src='/images/properties/".$image['image_ref'].".png'>");
									} else if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/properties/".$image['image_ref'].".jpg")) {
										print("<img src='/images/properties/".$image['image_ref'].".jpg'>");
									}
								}
							print("</div>");
						}
					print("</div>");
					print("<div class='slick carousel'>");
						foreach($images AS $image) {
							print("<div>");
								
								if($image['url'] != ""){
									print("<img src='".$image['url']."' alt='".$image['image_title']."'>");
								} else {
							
								if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/properties/".$image['image_ref'].".png")) {
									print("<img src='/images/properties/".$image['image_ref'].".png'>");
								} else if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/properties/".$image['image_ref'].".jpg")) {
									print("<img src='/images/properties/".$image['image_ref'].".jpg'>");
								}
								}
							print("</div>");
						}
					print("</div>");
				}
				
				print("<ul class='collapsible' data-collapsible='accordion'>");
					print("<li>");
						print("<div class='collapsible-header active'><i class='material-icons'>arrow_drop_down</i>Details</div>");
						print("<div class='collapsible-body'>");
							print($longDescription);
						print("</div>");
					print("</li>");
					/*print("<li>");
						print("<div class='collapsible-header'><i class='material-icons'>arrow_drop_down</i>Map</div>");
						print("<div class='collapsible-body'>");
						
							print("<input id='address' type='hidden' value='".$postcode."'>");
						//	print("<input id='property' type='hidden' value='".(!empty($resultdata['fld_addressStreet']) ? $resultdata['fld_addressStreet'].", " : "").(!empty($resultdata['fld_address2']) ? $resultdata['fld_address2'].", " : "").(!empty($resultdata['fld_address3']) ? $resultdata['fld_address3'].", " : "").(!empty($resultdata['fld_addressPostcode']) ? $resultdata['fld_addressPostcode'] : "")."' />");

						
						/*	$coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($postcode) . '&sensor=true');
							$coordinates = json_decode($coordinates);
						 
							$latitude = $coordinates->results[0]->geometry->location->lat;
							$longitude = $coordinates->results[0]->geometry->location->lng;
							
							print("<div class='map'>");
								print("<span id='map-lat' class='hide'>".$latitude."</span>");
								print("<span id='map-lng' class='hide'>".$longitude."</span>");
								print("<div id='map'></div>");
							print("</div>");*/
						
						/*print("</div>");
					print("</li>");*/
					print("<li>");
						if($brochure != "") print("<div class='collapsible-header'>Brochure View</div>");
						if($brochure != "") print("<div class='collapsible-body'><p>View <a href='$brochure' target='_blank'>print version</a> for this property (version will open in new window).</p></div>");
					print("</li>");
				print("</ul>");
				
			print("</div>");
			print("<div class='col s12 m4 sidebar'>");
				print("<div class='specification' style='margin-top:92px;'>");
					print("<ul>");
						print("<li><i class='material-icons left'>arrow_drop_down</i><span><strong>Price: </strong>&pound;".number_format($price,2)."</span></li>");
						print("<li><i class='material-icons left'>arrow_drop_down</i><span><strong>Status: </strong>".(($status!="") ? ucfirst($status) : "Available")."</span></li>");
						print("<li><i class='material-icons left'>arrow_drop_down</i><span><strong>Bedrooms: </strong>".$beds."</span></li>");
						print("<li><i class='material-icons left'>arrow_drop_down</i><span><strong>Bathrooms: </strong>".$baths."</span></li>");
						print("<li><i class='material-icons left'>arrow_drop_down</i><span><strong>Type: </strong>".$type."</span></li>");
					print("</ul>");
				print("</div>");
				print("<div class='area enquire'>");
					print("<h4><strong>Interested in this property?</strong></h4>");
					print("<p>Make an enquiry or book a viewing and we will reply as soon as possible.</p>");
					print("<p><a class='btn btn-primary' href='/contact?id=$id'>Enquire now</a></p>");
					if($brochure != "") print("<p><a class='btn btn-primary' href='$brochure' target='_blank'>Download Brochure</a></p>");
				print("</div>");
			print("</div>");
		print("</div>");
		
		print("<div id='enquiry-form' class='modal'>");
			print("<div class='modal-header'>");
				print("<span>Enquiry form</span>");
				print("<span class='modal-close right'>Close</span>");
			print("</div>");
			print("<div class='modal-content'>");
				print("<p>Your enquiry will be sent to us via email. We will do our utmost to ensure we respond to your enquiry as soon as possible using the methods of contact you provide.</p>");
			print("</div>");
			print("<form class='no-padding'>");
				print("<div class='modal-content'>");
					print("<div class='row'>");
						print("<div class='col s12 m6'>");
							print("<div class='input-field labelled'>");
								print("<i class='material-icons prefix'>person</i>");
								print("<label for='name'>Full name:</label>");
								print("<input placeholder='Enter your name...' id='name' type='text' class='validate'>");
							print("</div>");
						print("</div>");
						print("<div class='col s12 m6'>");
							print("<div class='input-field labelled'>");
								print("<i class='material-icons prefix'>email</i>");
								print("<label for='email'>Email address:</label>");
								print("<input placeholder='email@example.com' id='email' type='text' class='validate'>");
							print("</div>");
						print("</div>");
					print("</div>");
					print("<div class='row'>");
						print("<div class='col s12 m6'>");
							print("<div class='input-field labelled'>");
							print("<i class='material-icons prefix'>phone</i>");
								print("<label for='phone'>Phone number:</label>");
								print("<input placeholder='(optional)' id='phone' type='text' class='validate'>");
							print("</div>");
						print("</div>");
						print("<div class='col s12 m6'>");
							print("<div class='input-field labelled'>");
							print("<i class='material-icons prefix'>home</i>");
								print("<label for='property'>Property Referral:</label>");
								print("<input placeholder='Enter property address...' id='property' type='text' class='validate' value='".$name.", ".$area.(!empty($postcode) ? ", ".$postcode : "")."'>");
							print("</div>");
						print("</div>");
					print("</div>");
					print("<div class='row'>");
						print("<div class='col s12'>");
							print("<div class='input-field labelled'>");
								print("<i class='material-icons prefix'>comment</i>");
								print("<label for='message'>Comments:</label>");
								print("<textarea id='message' class='materialize-textarea validate' placeholder='Type in here...'></textarea>");
							print("</div>");
						print("</div>");
					print("</div>");
				print("</div>");
				print("<div class='modal-footer'>");
					print("<button type='submit' class='btn btn-submit'>Submit enquiry</button>");
				print("</div>");
			print("</form>");
		print("</div>");
		
	} else {
	
		print("<div class='row'>");
			
			$selectedIndex = 0;
			switch($strPage){
				case "properties-thorncliffe-south":
					$selectedIndex = 1;
				break;
				case "properties-ratings-village":
					$selectedIndex = 2;
				break;
				case "properties-retirement-apartments":
					$selectedIndex = 3;
				break;
				case "properties-park-view":
					$selectedIndex = 4;
				break;
				case "properties-abbotsmead":
					$selectedIndex = 5;
				break;
				case "properties-bevan-house":
					$selectedIndex = 6;
				break;
				case "properties-boarshaw-clough":
					$selectedIndex = 7;
				break;
				case "properties-scotland":
					$selectedIndex = 8;
				break;
				case "properties-wales":
					$selectedIndex = 9;
				break;
				case "partexchangeprop":
					$selectedIndex = 10;
				break;
				case "renttobuyprop":
					$selectedIndex = 11;
				break;
				case "properties-rock-lea":
					$selectedIndex = 12;
				break;
			}
			
			print("<select style='display:none;' class='filter' name='location'>");
				
				$getArticleQuery = "SELECT * FROM `property_locations` ORDER BY description";
				$locations = query($conn, $getArticleQuery, "multi");
				
				print("<option value='' >Any location</option>");
				foreach($locations AS $location) {
					
					if($selectedIndex == $location['recordID']) $selected = "selected"; else $selected = "";
					print("<option value='".$location['recordID']."' $selected>".$location['description']."</option>");
				}
			
			print("</select>");
		
			/*print("<div class='col s12 m12 l4'>");
				print("<div class='side-filter'>");
					print("<h4>Refine your search</h4>");
					print("<ul class='collapsible' data-collapsible='expandable'>");
						print("<li>");
							print("<div class='collapsible-header active'><i class='material-icons'>arrow_drop_down</i>Location</div>");
							print("<div class='collapsible-body'>");
								print("<div class='input-field'>");
									print("<i class='material-icons prefix'>location_on</i>");
									print("<select class='filter' name='location'>");
										
										$getArticleQuery = "SELECT DISTINCT(p_area) AS location FROM properties WHERE make_live = 'yes' ORDER BY location";
										$locations = query($conn, $getArticleQuery, "multi");
										
										print("<option value='' selected>Any location</option>");
										
										foreach($locations AS $location) {
											$short = explode(" ", $location['location']);
											print("<option value='".$short[0]."'>".$location['location']."</option>");
										}
										
									print("</select>");
									
								print("</div>");
							print("</div>");
						print("</li>");
						print("<li>");
							print("<div class='collapsible-header active'><i class='material-icons'>arrow_drop_down</i>Property Type</div>");
							print("<div class='collapsible-body'>");
								print("<div class='input-field'>");
									print("<i class='material-icons prefix'>home</i>");
									print("<select class='filter' name='type'>");
										
										$getArticleQuery = "SELECT DISTINCT(prop_type) AS type FROM properties WHERE make_live = 'yes' ORDER BY type";
										$types = query($conn, $getArticleQuery, "multi");
										
										print("<option value='' selected>Any property type</option>");
										
										foreach($types AS $type) {
											$short = explode(" ", $type['type']);
											print("<option value='".$short[0]."'>".$type['type']."</option>");
										}
									
									print("</select>");
								print("</div>");
							print("</div>");
						print("</li>");
						print("<li>");
							print("<div class='collapsible-header active'><i class='material-icons'>arrow_drop_down</i>Features</div>");
							print("<div class='collapsible-body'>");
								print("<div class='input-field'>");
									print("<i class='fa fa-bed prefix' aria-hidden='true'></i>");
									print("<select class='filter' name='bedrooms'>");
										print("<option value='0' selected>Any Bedrooms</option>");
										print("<option value='1'>1 Bedroom</option>");
										print("<option value='2'>2 Bedrooms</option>");
										print("<option value='3'>3 Bedrooms</option>");
										print("<option value='4'>4 Bedrooms</option>");
									print("</select>");
								/*	print("<input value='2 Bedrooms' name='bedrooms' type='text' readonly>");
									print("<div class='controls'>");
										print("<span class='numToggle bed' data-value='-1'>-</span>");
										print("<span class='numToggle bed' data-value='1'>+</span>");
									print("</div>");*/
								/*print("</div>");
								print("<div class='input-field'>");
									print("<i class='fa fa-bath prefix' aria-hidden='true'></i>");
								/*	print("<input value='1 Bathrooms' name='bathrooms' type='text' readonly>");
									print("<div class='controls'>");
										print("<span class='numToggle bath' data-value='-1'>-</span>");
										print("<span class='numToggle bath' data-value='1'>+</span>");
									print("</div>");*/
									/*print("<select class='filter' name='bathrooms'>");
										print("<option value='0' selected>Any Bathrooms</option>");
										print("<option value='1'>1 Bathroom</option>");
										print("<option value='2'>2 Bathrooms</option>");
										print("<option value='3'>3 Bathrooms</option>");
										print("<option value='4'>4 Bathrooms</option>");
									print("</select>");
								print("</div>");
							print("</div>");
						print("</li>");
					print("</ul>");
				print("</div>");
			print("</div>");*/
			print("<div class=''>");

				//print("<div id='properties'></div>");
				

			$strdbsql = "SELECT properties.*, property_locations.sitemap
				FROM properties 
				INNER JOIN property_locations ON properties.property_location_id = property_locations.recordID 
				WHERE make_live = 'yes' AND property_location_id = :location
				ORDER BY displayOrder";
			$params = array();
			$params['location'] = $selectedIndex;
			$properties = query($conn,$strdbsql,"multi",$params);
			

			$html .= ("<h4><span class='highlight'>".count($properties)."</span> Results</h4>");
			
			$html .= ("<input type='hidden' name='location' value='".$_REQUEST['location']."' />");

			
			// Do we have any results?
			if (count($properties) > 0) {
				
				// Display the results
				$sitemap = "";
				$html .= ("<div class='products'>");
					foreach($properties as $row) {
						$sstc = "";
						$sitemap = $row['sitemap'];
						
						$html .= ("<div class='card'>");
							$html .= ("<a href='/properties/".$row['egnID']."'>");
								switch(strtolower($row['sold'])) {
									case "under offer":
										$colour = "yellow";
									break;
									case "to let":
									case "now let":
										$colour = "blue";
									break;
									case "sstc":

										$sstc = "<img class='sstcRibbon' src='/images/sstcRibbon.png'><img class='sstcMobile' src='/images/sstcRibbonMobile.png'>";
										break;

									case "sold":
										$colour = "red";
									break;
									default:
										$colour = "";
									break;
								}
									$html .= ("<span class='card-counter ".$colour."'>".ucwords(strtolower((!empty($row['sold']) ? $row['sold'] : "Available")))."</span>");
								
								$strdbsql = "SELECT * FROM property_images WHERE rs_id = :propID ORDER BY image_order LIMIT 1";
								$strType = "single";
								$dataParams = array("propID"=>$row['rs_id']);
								$images = query($conn, $strdbsql, $strType, $dataParams);
								
								
									if($images['url'] != ""){
										$html .= ("<div class='card-image' id='".$row['rs_id']."'>");
												$html .= ("<img src='".$images['url']."' alt='".$images['image_title']."' />$sstc");
											$html .= ("</div>");
									} else {
										if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/properties/".$images['image_ref'].".png")) {
											$html .= ("<div class='card-image' id='".$row['rs_id']."'>");
												$html .= ("<img src='/images/properties/".$images['image_ref'].".png' alt='".ucwords(strtolower($row['p_name']))."' />");
											$html .= ("</div>");
										} else if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/properties/".$images['image_ref'].".jpg")) {
											$html .= ("<div class='card-image' id='".$row['rs_id']."'>");
												$html .= ("<img src='/images/properties/".$images['image_ref'].".jpg' alt='".ucwords(strtolower($row['p_name']))."' />");
											$html .= ("</div>");
										} else {
											$html .= ("<div class='card-image none' id='".$row['rs_id']."'>");
												$html .= ("<span>No Images Available</span>");
											$html .= ("</div>");
										}
									}
								
								$html .= ("<div class='card-content'>");
									$html .= ("<span class='card-title'>".ucwords(strtolower($row['p_name']))."</span>");
									$html .= ("<span class='card-subtitle'>");
										$html .= ($row['p_area']);
										if(!empty($row['postcode']))$html .= (", ".$row['postcode']);
									$html .= ("</span>");
									$html .= ("<p>".$row['short_description']."</p>");
								$html .= ("</div>");
								$html .= ("<div class='card-action'>");
									$html .= ("<div class='card-price'>");
										$price = strtoupper(str_replace("from ", "", str_replace("£", "", str_replace(".", "", $row['prop_price']))));
										if($price != "TO LET" && $price != "TBA" && $price != "OFFERS") $html .= ("&pound;".number_format($price,2));
										else $html .= ($price);
									$html .= ("</div>");
									$html .= ("<ul class='card-details'>");
										$html .= ("<li><i class='fa fa-house-user left' aria-hidden='true'></i><span><strong>".(($row['sold'] == "") ? "Available" : $row['sold'])."</strong></span></li>");
										$html .= ("<li><i class='fa fa-bed left' aria-hidden='true'></i><span><strong>".$row['No_beds']."</strong> Bedrooms</span></li>");
										$html .= ("<li><i class='fa fa-bath left' aria-hidden='true'></i><span><strong>".$row['No_baths']."</strong> Bathrooms</span></li>");
										
									//	$html .= ("<li><i class='material-icons left'>directions_car</i><span><strong>#</strong> ".$row['prop_type']."</span></li>");
									$html .= ("</ul>");
								$html .= ("</div>");
							$html .= ("</a>");
						$html .= ("</div>");
					}
				$html .= ("</div>");

			} else {
				$html .= ("<p>No results could be displayed.</p>");
			}
		
			
			if($_REQUEST['location'] != "") {
				$strdbsql = "SELECT sitemap FROM property_locations WHERE recordID = :location";
				$params = array();
				$params['location'] = "".$_REQUEST['location']."";
				$propertyLocation = query($conn,$strdbsql,"single",$params);
				if($propertyLocation['sitemap'] != ""){
					
					$html .= ("<h4>Site Plan</h4>");
					
					$sitemaps = explode(",",$propertyLocation['sitemap']);
					foreach($sitemaps AS $sitemap){
						$html .= ("<br/><img src='".$sitemap."' />");
					}				
				}
			}
				
			print $html;
				
					
			print("</div>");
		print("</div>");
		
	}

?>