<?php	
	
	// Define DB Functions
	function connect($driver = DRIVER, $username = USER, $password = PASS, $host = HOST, $db = DB) {
		try {
			// Define new PHP Data Object connection
			$conn = new PDO('' . $driver . ':host=' . $host . ';dbname=' . $db, $username, $password);
			// Sets error reporting level
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conn;
		} catch(PDOException $e) {
			// If failed, display error
			echo "ERROR: " . $e->getMessage();
		}
	}
	
	function bookConnect($driver = BOOKDRIVER, $username = BOOKUSER, $password = BOOKPASS, $host = BOOKHOST, $db = BOOKDB) {
		try
		{
			// Define new PHP Data Object connection
			$bookConn = new PDO(''.$driver.':host='.$host.';dbname='.$db, $username, $password);

			// Sets error reporting level
			$bookConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $bookConn;
		}
		catch(PDOException $e)
		{
			// If failed, display error
			echo "ERROR: " . $e->getMessage();
		}
	}
	
	//
	function query($conn, $querystring, $querytype, $values = null) {
		if ($conn) {
			// Prepares query
			$query = $conn->prepare($querystring);
			
			// Binds values and executes query
			$query->execute($values);
			
			switch ($querytype) {
				case "single":
					// Returns associative array of single result
					$result = $query->fetch(PDO::FETCH_ASSOC);
				break;

				case "multi":
					// Returns multidimensional associative array of all results
					$result = $query->fetchAll(PDO::FETCH_ASSOC);
				break;

				case "count":
					// Returns number of selected rows
					$result = $query->rowCount();
				break;

				case "insert":
					// Insert new row and return is fld_counter (auto increment) of the inserted line
					$result = $conn->lastInsertID();
				break;

				case "update":
					// Return number of rows updated by query
					$result = $query->rowCount();
				break;

				case "delete":
					// Return number of rows deleted by query
					$result = $query->rowCount();
				break;
				
				case "column":
					// Return number of rows deleted by query
					$result = $query->fetchColumn();
				break;

				default:
					// Invalid query type
					$result = "Invalid !";
				break;
			}
			return $result;
		} else {
			return "Failure !";
		}
	}
	
	//	Compress given code
	function compressCss($buffer) {
		// Remove comments:
		$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
		// Remove tabs, excessive spaces and newlines
		$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '   '), '', $buffer);
		$buffer = str_replace(array("{ ", " {"), '{', $buffer);
		$buffer = str_replace(array("} ", " }"), '}', $buffer);
		$buffer = str_replace(array("; ", " ;"), ';', $buffer);
		$buffer = str_replace(array(", ", " ,"), ',', $buffer);
		$buffer = str_replace(array(": ", " :"), ':', $buffer);
		return $buffer;
	}
	
	//	Get site blocks from admin
	function fnGetSiteBlocks($lookupID,$lookupType,$position,$conn){
			
		if (isset($_REQUEST['page'])) $strPage = $_REQUEST['page']; else $strPage = "";
		if (isset($_REQUEST['view'])) $strview = $_REQUEST['view']; else $strview = "";
			
		$strdbsql = "SELECT site_blocks.*, site_block_position.description AS position FROM site_block_relations INNER JOIN site_blocks ON site_block_relations.blockID = site_blocks.recordID INNER JOIN site_block_position ON site_block_relations.positionID = site_block_position.recordID WHERE site_block_relations.$lookupType = :lookupID AND site_block_position.description = :position ORDER BY site_block_relations.pageOrder";
		$returnType = "multi";
		$dataParam = array("lookupID"=>$lookupID,"position"=>$position);
		$blockRelations = query ($conn, $strdbsql, $returnType, $dataParam);
		
		if (count($blockRelations) != 0) {

			foreach ($blockRelations AS $relation) {
				
				if ($relation['type'] == 3) { //gallery
					$GLOBALS['boolSlideshow'] = true;
					
					$strdbsql = "SELECT * FROM site_gallery_images WHERE galleryID = :galleryID ORDER BY galleryOrder";
					$returnType = "multi";
					$dataParam = array("galleryID"=>$relation['galleryID']);
					$galleryImages = query ($conn, $strdbsql, $returnType, $dataParam);
					
					print("<div id='myCarousel' class='carousel slide carousel-fade".($strPage == "development" ? " devSlider" : "")."' ".($strPage != "" ? " style='width:1170px; margin-top: 10px;'" : "")." data-ride='carousel'>");
						
						if(count($galleryImages) > 0) {
							print("<ol class='carousel-indicators'>");
								if(count($galleryImages) > 1) {
									$x = 0;
									foreach($galleryImages AS $indicator) {
										print("<li data-target='#myCarousel' data-slide-to='".$x."'".($x == 0 ? " class='active'" : "")."></li>");
										$x++;
									}	
								}
							print("</ol>");
						}
						
						print("<div class='carousel-inner' role='listbox'>");
							print("<div class='container'>");
						
								$i = 0;
							
								foreach($galleryImages AS $caption) {
							
									if($i == 0) {
										
										print("<div class='carousel-caption carousel-caption-top'>");
											print("<div class=''>");
												
												if($strPage != "development") {
													
													print("<div class='row'>");
														print("<div class=''>");
															print("<h1>".$caption['title']."</h1>");
														print("</div>");
													print("</div>");
													
												} else {
													
													print("<h1>".$caption['title']."</h1>");
													print("<a href='".$caption['linkLocation']."' target='_blank'>Read More <i class='fa fa-chevron-right' aria-hidden='true'></i></a>");
													
												}
												
											print("</div>");
										print("</div>");
										
										/*
										print("<div class='carousel-caption carousel-caption-bottom'>");
											print("<div class=''>");
												
												if($strPage != "development") {
													print("<div class='row'>");
														print("<div class=''>");
															print("<h1>".$caption['description']."</h1>");
														print("</div>");
													print("</div>");
												} else {
													print("<h1>".$caption['description']."</h1>");
													print("<a href='".$caption['linkLocation']."' target='_blank'>Read More <i class='fa fa-chevron-right' aria-hidden='true'></i></a>");
												}
												
											print("</div>");
										print("</div>");
										*/
									}
									
									$i++;
									
								}
							
							print("</div>");
						
						//	$y = 0;
						
							foreach($galleryImages AS $image) {
						
								print("<div class='item".($y == 0 ? " active" : "")."' style='background-image:url(/images/gallery/".$image['imageLocation'].");'>");
								/*print("<h1>".$image['title']."</h1>");
								print("<h2>".$image['description']."</h2>");*/
								
								print("</div>");
							
								$y++;
							
							}
							
						print("</div>");
						
					/*	if(count($galleryImages) > 1) {
							print("<a class='left carousel-control' href='#myCarousel' role='button' data-slide='prev'>");
								print("<i class='fa fa-chevron-left' aria-hidden='true'></i>");
								print("<span class='sr-only'>Previous</span>");
							print("</a>");
							print("<a class='right carousel-control' href='#myCarousel' role='button' data-slide='next'>");
								print("<i class='fa fa-chevron-right' aria-hidden='true'></i>");
								print("<span class='sr-only'>Next</span>");
							print("</a>");
						}*/
							
					print("</div>");
					
				/*	print("<div class='block-quote'>");
						print("<div class='container'>");
							print("<img src='/images/company/ross-icon-invert.png' />");
							print("<span>Get your free valuation</span>");
							print("<a href='/value-my-property'>Get a Valuation <i class='fa fa-chevron-right' aria-hidden='true'></i></a>");
						print("</div>");
					print("</div>");*/
					
				} else {
					
					
					if(!empty($relation['includeName']) && file_exists($_SERVER['DOCUMENT_ROOT']."/includes/".$relation['includeName']))
					{
						$includeBlock = true;
					}
					else
					{
						$includeBlock = false;
					}
					if($relation['includePos'] == 0 && $includeBlock)
					{
						include($_SERVER['DOCUMENT_ROOT']."/includes/".$relation['includeName']);
					}
				
					print($relation['content']);
					
					
					if($relation['includePos'] == 1 && $includeBlock)
					{
						include$_SERVER['DOCUMENT_ROOT'].("/includes/".$relation['includeName']);
					}
					
				}			
			}

		}
		
	}
	
	//	Function to redirect browser
	function redirect($url)
	{
	   if (!headers_sent())
			header('Location: '.$url);
	   else
	   {
			echo '<script type="text/javascript">';
			echo 'window.location.href="'.$url.'";';
			echo '</script>';
			echo '<noscript>';
			echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
			echo '</noscript>';
	   }
	}
	
	//
	function abbr($str, $maxLen) {
		if (strlen($str) > $maxLen && $maxLen > 1) {
			preg_match("#^.{1,".$maxLen."}\.#s", $str, $matches);
			return $matches[0];
		} else {
			return $str;
		}
	}
	
	//	Convert unixtimestamp to specified format
	function fnconvertunixtime ($strtime, $strformat = "d/m/Y") {
		return date($strformat, $strtime);
	}
	
	//	Check to see if email is valid
	function is_valid_email($email) {
		$result = TRUE;
		if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email)) {
			$result = FALSE;
		}
		return $result;
	}
	
	if (!function_exists('paginate_function')) {
	function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
	{
		$pagination = '';
		if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
			$pagination .= '<ul class="pagination">';
			
			$right_links    = $current_page + 3; 
			$previous       = $current_page - 3; //previous link 
			$next           = $current_page + 1; //next link
			$first_link     = true; //boolean var to decide our first link
			
			if($current_page > 1){
				$previous_link = ($previous==0)?1:$previous;
			//	$pagination .= '<li class="first"><a href="#" data-page="1" title="First">&laquo;</a></li>'; //first link
				$pagination .= '<li><a href="#" data-page="'.$previous_link.'" title="Previous"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></li>'; //previous link
					for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
						if($i > 0){
							$pagination .= '<li><a href="#" data-page="'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
						}
					}   
				$first_link = false; //set first link to false
			}
			
			if($first_link){ //if current active page is first link
				$pagination .= '<li class="first active">'.$current_page.'</li>';
			}elseif($current_page == $total_pages){ //if it's the last active link
				$pagination .= '<li class="last active">'.$current_page.'</li>';
			}else{ //regular current link
				$pagination .= '<li class="active">'.$current_page.'</li>';
			}
					
			for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
				if($i<=$total_pages){
					$pagination .= '<li><a href="#" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
				}
			}
			if($current_page < $total_pages){ 
					$next_link = ($i > $total_pages)? $total_pages : $i;
					$pagination .= '<li><a href="#" data-page="'.$next_link.'" title="Next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>'; //next link
				//	$pagination .= '<li class="last"><a href="#" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
			}
			
			$pagination .= '</ul>'; 
		}
		return $pagination; //return pagination links
	}
	}
	
	if (!function_exists('isPostcodeValid')) {
		function isPostcodeValid($postcode)
		{
			$postcode = preg_replace('/\s/', '', strtoupper($postcode));
			if(preg_match("/^[A-Z]{1,2}[0-9]{2,3}[A-Z]{2}$/", $postcode) || preg_match("/^[A-Z]{1,2}[0-9]{1}[A-Z]{1}[0-9]{1}[A-Z]{2}$/", $postcode) || preg_match("/^GIR0[A-Z]{2}$/", $postcode)) return true;
			else return false;
		}
	}
?>