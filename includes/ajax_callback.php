<?php
 
	include("incsitecommon.php");
 
	//sanitize data
	$type = $_REQUEST['type'];
	$name = $_REQUEST['name'];
	$email = $_REQUEST['email'];
	$phone = $_REQUEST['phone'];
	$propertytype = $_REQUEST['propertytype'];
	$property = $_REQUEST['property'];
	$call_reason = $_REQUEST['call_reason'];
	$message = $_REQUEST['message'];

	$secret = "6Leuz7AUAAAAAFXBvHh5mg1rTDKnMFnQlPadOfr8";
	$response = $_REQUEST["captcha"];
	$verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$response);
	$captcha_success = json_decode($verify);
	
	if($captcha_success->success == false) {
	
		print("0");
	
	} else if($captcha_success->success == true) {
	
		// insert form data into database	
	/*	$strdbsql = "SELECT * FROM customer_callback WHERE fld_email = :email OR fld_phone = :phone";
		$resultdata = query($conn,$strdbsql,"multi",Array("phone" => $phone, "email" => $email));
		
		if(count($resultdata) > 0){
			
			print("error");
			
		} else {
			
			$strdbsql = "INSERT INTO customer_callback(fld_name, fld_email, fld_phone, fld_date) VALUES (:name, :email, :phone, UNIX_TIMESTAMP())";
			$resultdata = query($conn,$strdbsql,"insert",Array("name" => $name,"email" => $email,"phone" => $phone));
			
			print("success");
			
		}*/
		
		$to = "sales@rossestateagencies.co.uk";
		
		$subject = "Website - ".$type;
		
		$headers = "From: " . strip_tags("sales@rossestateagencies.co.uk") . "\r\n";
		$headers .= "Reply-To: ". strip_tags($email) . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$html = "<html>";
			$html .= "<head>";			
				$html .= "<style>";
					$html .= "body { background-color: #F1F1F1; margin: 0; font-family: arial; }";
					$html .= "a { color: #887777; }";
					$html .= "p { font-size: 14px; }";
					$html .= "strong.primary { color: #887777; }";
				$html .= "</style>";
			$html .= "</head>";
			$html .= "<body>";
				$html .= "<table width='100%' cellpadding='0' cellspacing='0'>".PHP_EOL;
					$html .= "<tr>";
						$html .= "<td>";
							$html .= "<table width='600px' align='center' cellpadding='0' cellspacing='0'>";
								$html .= "<thead>";
									$html .= "<tr>";
										$html .= "<td align='right'>";
											$html .= "<img width='100%' src='http://rossestateagencies.co.uk/images/emails/header.jpg'/>";
										$html .= "</td>";
									$html .= "</tr>";
								$html .= "</thead>";
								$html .= "<tbody>";
									$html .= "<tr>";
										$html .= "<td>".PHP_EOL;
											$html .= "<table width='100%' align='left' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF'>";
												$html .= "<tr>";
													$html .= "<td>";
														$html .= "<table width='510' align='left' cellpadding='45' cellspacing='0'>";
															$html .= "<tr>";
																$html .= "<td>";
																	$html .= "<p>Ross Estate Agencies,</p>".PHP_EOL;
																	$html .= "<br/>";
																	$html .= "<p>You have <strong>1</strong> new &quot;".$type."&quot; from a visitor on your website. The details are shown below:</p>";
																	$html .= "<br/>";
																	if(!empty($property)){
																		$html .= "<p><strong>".$property." ".(!empty($propertytype) ? "(".$propertytype.")" : "")."</strong></p>";
																		$html .= "<br/>".PHP_EOL;
																	}
																	if(!empty($message)){
																		$html .= "<p>".$message."</p>";
																		$html .= "<br/>".PHP_EOL;
																	}
																	if(!empty($call_reason)){
																		$html .= "<p>Source of enquiry: ".$call_reason."</p>";
																		$html .= "<br/>".PHP_EOL;
																	}
																	$html .= "<p>You received this enquiry from <strong class='primary'>".$name." (".$email.")</strong> at ".date('g:ia')." on ".date('l jS F').". To contact the sender, you should reply to this email".($phone != '' ? ' or contact them on <strong>'.$phone.'</strong>.' : '.')."</p>";
																$html .= "</td>";
															$html .= "</tr>";
														$html .= "</table>";
													$html .= "</td>";
												$html .= "</tr>";
											$html .= "</table>".PHP_EOL;
										$html .= "</td>";
									$html .= "</tr>";
								$html .= "</tbody>";
								$html .= "<tfoot>";
									$html .= "<tr>".PHP_EOL;
										$html .= "<td align='right'>";
											$html .= "<img width='100%' src='http://rossestateagencies.co.uk/images/emails/footer.jpg'/>";
										$html .= "</td>";
									$html .= "</tr>";
								$html .= "</tfoot>";
							$html .= "</table>";
						$html .= "</td>";
					$html .= "</tr>";
				$html .= "</table>".PHP_EOL;
			$html .= "</body>";
		$html .= "</html>";
		
		mail($to, $subject, $html, $headers);
	
		print("1");
	
	}
	
?>