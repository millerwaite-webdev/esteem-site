<?php


	
	print("<div class='block-adverts'>");
		print("<div class=''>");
			
			print("<div class='row'>");
				print("<div class='col-sm-12 col-md-6 col-lg-4'>");
					print("<div class='block-advert'>");
						print("<div class='artwork'>");
							print("<img src='/images/elements/promo-icon.png' alt='Esteem Homes' />");
						print("</div>");
						print("<div class='text'>");
							print("<h5>100% Part Exchange</h5>");
							print("<p>Available anywhere in the UK.</p>");
						print("</div>");
					print("</div>");
				print("</div>");
				print("<div class='col-sm-12 col-md-6 col-lg-4'>");
					print("<div class='block-advert'>");
						print("<div class='artwork'>");
							print("<img src='/images/elements/promo-icon.png' alt='Esteem Homes' />");
						print("</div>");
						print("<div class='text'>");
							print("<h5>Multiple sites across the UK</h5>");
							print("<p>Including Cumbria, Scotland, South Wales, Manchester & Liverpool.</p>");
						print("</div>");
					print("</div>");
				print("</div>");
				print("<div class='col-sm-12 col-md-6 col-lg-4'>");
					print("<div class='block-advert'>");
						print("<div class='artwork'>");
							print("<img src='/images/elements/promo-icon.png' alt='Esteem Homes' />");
						print("</div>");
						print("<div class='text'>");
							
							print("<h5>Smooth Move Option</h5>");
							print("<p>If you decided you didn't want to part exchange, we will list your house with 3 Local Estate agents & pay your estate agent fee when sold.</p>");
						print("</div>");
					print("</div>");
				print("</div>");
			print("</div>");
			print("<div class='row'>");
				print("<div class='col-sm-12 col-md-6 col-lg-4'>");
					print("<div class='block-advert'>");
						print("<div class='artwork'>");
							print("<img src='/images/elements/promo-icon.png' alt='Esteem Homes' />");
						print("</div>");
						print("<div class='text'>");
							print("<h5>Guaranteed Buyer</h5>");
							print("<p>You have found a guaranteed cash purchaser with us.</p>");
						print("</div>");
					print("</div>");
				print("</div>");
				print("<div class='col-sm-12 col-md-6 col-lg-4'>");
					print("<div class='block-advert'>");
						print("<div class='artwork'>");
							print("<img src='/images/elements/promo-icon.png' alt='Esteem Homes' />");
						print("</div>");
						print("<div class='text'>");
							print("<h5>Hassle Free</h5>");
							print("<p>No inconvenience of viewings or Chains.</p>");
						print("</div>");
					print("</div>");
				print("</div>");
				print("<div class='col-sm-12 col-md-6 col-lg-4'>");
					print("<div class='block-advert'>");
						print("<div class='artwork'>");
							print("<img src='/images/elements/promo-icon.png' alt='Esteem Homes' />");
						print("</div>");
						print("<div class='text'>");
							print("<h5>We Charge NO FEES</h5>");
							print("<p>Saving customers thousands.</p>");
						print("</div>");
					print("</div>");
				print("</div>");
			print("</div>");
		print("</div>");
	print("</div>");
	
?>