<div class="block-text" style="margin-top:-40px;padding-top:0;">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<p>We always aim to provide a high quality service to our customers. However, if you encounter any problems and we are unable to resolve them you can take your complaint to an independent Ombudsman. Our advice is covered under the Financial Ombudsman Service (<a href="http://www.financial-ombudsman.org.uk/consumer/complaints.htm" target="_blank" rel="noopener">http://www.financial-ombudsman.org.uk/consumer/complaints.htm</a>). You may be able to submit a claim through the EU Online Dispute Resolution Platform (<a href="https://webgate.ec.europa.eu/odr/main/?event=main.home.show" target="_blank">https://webgate.ec.europa.eu/odr/main/?event=main.home.show</a></p>
			</div>
		</div>
	</div>
</div>