<?php
/************************************************************/
/* Created By: Rhian singleton                              */
/* Email: rhian@millerwaite.com                             */
/*                                                          */
/* For: Ross Estate Agencies                                */
/************************************************************/
/* OUTPUTS COMPLETE FEED FROM DB COPY OF JUPIX PROPERTY     */
/* DATA FOR USE BY ROSS ESTATE AGENCIES                     */
/* INCLUDES FULL AND SHORT VIEW, FIND BY ID AND SEARCH      */
/************************************************************/
/* VERSION : 1.0                                            */
/* DATE    : 13/05/2010                                     */
/************************************************************/

include("/var/www/vhosts/esteemhomes.co.uk/httpdocs/datafeed/incsitecommon.php");

$strdb = mysql_connect($strserver,$strdbuser,$strdbpass);
mysql_select_db($strdbname,$strdb);

	//set header for update report
	$strlogheader = " Esteeem Homes Data Update Log \r\n";
	$strlogheader .= " Google Location XML Feed to Miller Waite Database\r\n\r\n";
	$strlogheader .= " -----------------------------\r\n";
	$strlogheader .= " ".$strdate."\r\n";
	$strlogheader .= " -----------------------------\r\n\r\n";


	//used to store addresses for google location feed
	$arraddresses = array ();
	$strggladdress = "";
	$intgglpropertyID = null;

	//used to store data from google location feed
	$arrgglreport = array ();
	$location = "";
	
	//use google geolocate api
	include("/var/www/vhosts/esteemhomes.co.uk/httpdocs/datafeed/geolocate.php");

	$strdbsql = "SELECT * FROM tbl_property WHERE fld_lat = 0 AND fld_lng = 0 ORDEr BY RAND()";
	$result = mysql_query ($strdbsql,$strdb);

	if (mysql_num_rows($result) != 0)
	{
		echo $strlogheader;

		while($resultdata = mysql_fetch_object($result)) {
			$arraddress = array();

			if($resultdata->fld_addressName != "") $arraddress[] = $resultdata->fld_addressName;
			if($resultdata->fld_addressNumber != "") {
				if(strpos($resultdata->fld_addressNumber, "&") === FALSE) $arraddress[] = $resultdata->fld_addressNumber;
				else $arraddress[] = substr($resultdata->fld_addressNumber, 0, strpos($resultdata->fld_addressNumber, "&"));
			}
			if($resultdata->fld_addressStreet != "") $arraddress[] = $resultdata->fld_addressStreet;
			if($resultdata->fld_address2 != "") $arraddress[] = $resultdata->fld_address2;
			if($resultdata->fld_address3 != "") $arraddress[] = $resultdata->fld_address3;
			if($resultdata->fld_address4 != "") $arraddress[] = $resultdata->fld_address4;
			if($resultdata->fld_addressPostcode != "") $arraddress[] = $resultdata->fld_addressPostcode;
			if($resultdata->fld_country != "") $arraddress[] = $resultdata->fld_country;

			$strggladdress = implode(", ",$arraddress);
			$intgglpropertyID = $resultdata->fld_propertyID;

			//get location feed
			$location = geolocate($strggladdress);
			if(is_array($location))
			{
				$strdbsql = "UPDATE tbl_property SET fld_lat = '".$location["lat"]."', fld_lng = '".$location["lng"]."', fld_locationtype = '".$location["type"]."' WHERE fld_propertyID = $intgglpropertyID";
				if(mysql_query ($strdbsql,$strdb)) $arrgglreport[$intgglpropertyID] = " Location Saved Successfully (Type = ".$location["type"].")";
				else  $arrgglreport[$intgglpropertyID] = " Location Save Failed ";
			}
			else
			{
				$arrgglreport[$intgglpropertyID] = " $location";
				sleep(5);
			}
			echo $intgglpropertyID.":\t$location<br/>\r\n";
			$location = NULL;

		}
	}
	else
	{
		$arrgglreport['NA'] = " No Properties require location updates. ";
	} 

//	if(count($arrgglreport) > 0)
//	{
//		foreach($arrgglreport AS $key => $value)
//		{
//			$strmessage .= " $key:\t$value\r\n";
//		}
//		$strmessage.= " -----------------------------\r\n";
//	}

//	echo $strlogheader.$strmessage;

	mysql_free_result ($result);
mysql_close ($strdb);

?>