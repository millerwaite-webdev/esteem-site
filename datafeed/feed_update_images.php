<?php
/************************************************************/
/* Created By: Rhian singleton                              */
/* Email: rhian@millerwaite.com                             */
/*                                                          */
/* For: Ross Estate Agencies                                */
/************************************************************/
/* READS IN JUPIX PROPERTY XML FEED DURING OVERNIGHT WINDOW */
/* AND STORES IN A LOCAL DATABASE FOR LATER RETRIVAL BY     */
/* PHONE USERS DURING THE DAY                               */
/************************************************************/
/* VERSION : 1.0                                            */
/* DATE    : 13/05/2010                                     */
/************************************************************/

include("/var/www/vhosts/esteemhomes.co.uk/httpdocs/datafeed/incsitecommon.php");

error_reporting(E_ALL);
ini_set('display_errors', '1');
set_time_limit(0);


$strdb = mysql_connect($strserver,$strdbuser,$strdbpass);
mysql_select_db($strdbname,$strdb);


	$arrimages = array (); //used to list updates to propertyImage table
	
	
	//use google geolocate api
	include("/var/www/vhosts/esteemhomes.co.uk/httpdocs/datafeed/geolocate.php");


	//------------------------------------------------------------------------------------------------------------//
	//define functions
	//------------------------------------------------------------------------------------------------------------//

	//file transfer and edit functions
	function make_thumb($img_name,$filename,$new_w,$new_h){
		$ext=getExtension($img_name);
		$newextn = $ext;
		$r = 255;
		$g = 255;
		$b = 255;
		if(!strcmp("jpg",$ext) || !strcmp("jpeg",$ext)) $src_img=imagecreatefromjpeg($img_name);
		if(!strcmp("png",$ext)) $src_img=imagecreatefrompng($img_name);
		if(!strcmp("gif",$ext)) $src_img=imagecreatefromgif($img_name);

		$old_x=imageSX($src_img);
		$old_y=imageSY($src_img);

		$ratio1=$old_x/$new_w;
		$ratio2=$old_y/$new_h;

		if($ratio1>$ratio2)
		{
			$thumb_w=$new_w;
			$thumb_h=$old_y/$ratio1;
		}
		else
		{
			$thumb_h=$new_h;
			$thumb_w=$old_x/$ratio2;
		}

		$dst_img=ImageCreateTrueColor($new_w,$new_h);
		$starty = ($new_h - $thumb_h)/2;
		$startx = ($new_w - $thumb_w)/2;
		imagefill ( $dst_img , 0 , 0 , imagecolorallocate ( $dst_img , $r, $g , $b ) );

		imagecopyresampled($dst_img,$src_img,$startx,$starty,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

		if(!strcmp("png",$newextn)) imagepng($dst_img,$filename);
		else if(!strcmp("jpg",$newextn) || !strcmp("jpeg",$newextn)) imagejpeg($dst_img,$filename);
		else if(!strcmp("gif",$newextn)) imagegif($dst_img,$filename);
		if(substr(sprintf('%o', fileperms($filename)), -4) != "0777") chmod($filename, 0777);
  
		imagedestroy($dst_img);
		imagedestroy($src_img);
	}
	function copyFileFromURL($fileUrl, $savepath){
		if(url_exists($fileUrl))
		{
			$in = fopen($fileUrl, 'r');
			if ($in == FALSE) {return $in;}
			else {
				$strupfiledirs = explode("/", $fileUrl);
				$strfilename = $strupfiledirs[count($strupfiledirs)-1];
				$stroutfilepath = $savepath.$strfilename;
				$out = fopen($stroutfilepath, 'w');
				if ($out == FALSE) {return $out;}
				else {

					while (!feof($in)) {
						$buffer = fread($in, 2048);
						fwrite($out, $buffer);
					}
					fclose($out);
					if(substr(sprintf('%o', fileperms($stroutfilepath)), -4) != "0777") chmod($stroutfilepath, 0777);
					return $strfilename;
				}
			}
		}
		else return FALSE;
	}

	//helper functions
	function getExtension($str) {
		$i = strrpos($str,".");
		if (!$i) { return ""; }
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}
	function url_exists($url) {
		if (!$ch = curl_init($url)) {
			return false;
		}
		
		curl_setopt($ch, CURLOPT_HEADER, true);    // we want headers
		curl_setopt($ch, CURLOPT_NOBODY, true);    // we don't need body
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT,10);
		
		$output = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		if($httpcode == 200 || $httpcode == 302 || $httpcode == 301) return true;
		else return false;

	//	$hdrs = @get_headers($url);
	//	return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false;
	}
	function getMimeType($imagepath) {
		$imgdata = getimagesize($imagepath);
		if($imgdata == false) {
			return false;
		} else {
			return $imgdata['mime'];
		}
	}
	



	$strdbsql = "SELECT * FROM tbl_propertyImages ";
	$result = mysql_query ($strdbsql,$strdb);
	while($resultdata = mysql_fetch_object($result))
	{
		$strsrc = $strrootpath.$strimagedir.$resultdata->fld_image;
		$strdest = $strrootpath.$strthumbdir.$resultdata->fld_image;
		if(!file_exists($strdest)) {
			error_log("Counter: ".$resultdata->fld_counter." Image: ".$resultdata->fld_counter." - ".$resultdata->fld_image);
			if(getMimeType($strsrc) != null) {
				make_thumb($strsrc,$strdest,$thumbWidth,$thumbHeight);
			}
		}
		
	}
	mysql_free_result ($result);
	mysql_close ($strdb);
?>