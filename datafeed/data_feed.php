<?php
/************************************************************/
/* Created By: Rhian singleton                              */
/* Email: rhian@millerwaite.com                             */
/*                                                          */
/* For: Ross Estate Agencies                                */
/************************************************************/
/* OUTPUTS COMPLETE FEED FROM DB COPY OF JUPIX PROPERTY     */
/* DATA FOR USE BY ROSS ESTATE AGENCIES                     */
/* INCLUDES FULL AND SHORT VIEW, FIND BY ID AND SEARCH      */
/************************************************************/
/* VERSION : 1.0                                            */
/* DATE    : 13/05/2010                                     */
/************************************************************/

include("incsitecommon.php");

$strdb = mysql_connect($strserver,$strdbuser,$strdbpass);
mysql_select_db($strdbname,$strdb);


if(isset($_REQUEST['list'])) $strview = $_REQUEST['list'];
else $strview = "";

switch ($strview)
{
  case "commtypes": $strdbsql = "SELECT * FROM tbl_commPropertyType"; break;
  case "location": $strdbsql = "SELECT DISTINCT fld_urlkey, fld_label FROM tbl_location"; break;
  case "restypes": $strdbsql = "SELECT * FROM tbl_residentialPropertyType"; break;
  case "branch": $strdbsql = "SELECT * FROM tbl_branch"; break;
  case "region": $strdbsql = "SELECT * FROM tbl_region"; break;
}
    header ("Content-type: text/xml; charset=utf-8");


	$result = mysql_query ($strdbsql,$strdb);
	if (mysql_num_rows($result) != 0)
	{

	print("<?xml version='1.0' encoding='UTF-8' ?>\n");
switch ($strview)
{
  case "commtypes": print("<propertyTypes>\n"); break;
  case "location": print("<locations>\n"); break;
  case "restypes": print("<propertyTypes>\n"); break;
  case "branch": print("<branches>\n"); break;
  case "region": print("<regions>\n"); break;
}
		while($resultdata = mysql_fetch_object($result))
		{
			switch ($strview)
			{
				case "commtypes":
					print("\t<propertyType>\n");
						print("\t\t<typeId>".$resultdata->fld_counter."</typeId>\n");
						print("\t\t<typeName>".$resultdata->fld_value."</typeName>\n");
					print("\t</propertyType>\n");
					break;

				case "restypes":
					print("\t<propertyType>\n");
						print("\t\t<typeId>".$resultdata->fld_counter."</typeId>\n");
						print("\t\t<typeName>".$resultdata->fld_value."</typeName>\n");
					print("\t</propertyType>\n");
					break;
				case "location":
					print("\t<propertyLocation>\n");
						print("\t\t<locationName>".$resultdata->fld_label."</locationName>\n");
						print("\t\t<locationUrlKey>".$resultdata->fld_urlkey."</locationUrlKey>\n");
					print("\t</propertyLocation>\n");
					break;

				case "branch":
					print("\t<branch>\n");
						print("\t\t<branchId>".$resultdata->fld_counter."</branchId>\n");
						print("\t\t<branchName>".$resultdata->fld_branchName."</branchName>\n");
						print("\t\t<branchImage>".$strurlpath.$strimgdir.$resultdata->fld_image."</branchImage>\n");
						print("\t\t<branchDescription><![CDATA[".$resultdata->fld_branchDescription."]]></branchDescription>\n");
						print("\t<branchAddress>".trim($resultdata->fld_addressNumber." ".$resultdata->fld_addressStreet.", ".$resultdata->fld_address3.", ".$resultdata->fld_address4.", ".$resultdata->fld_addressPostcode)."</branchAddress>\n");
		//	print("\t\t<branchAddress>".str_replace(", , ", ", ",trim($resultdata->fld_addressNumber.", ".$resultdata->fld_addressStreet.", ".$resultdata->fld_address2).", ".$resultdata->fld_address3.", ".$resultdata->fld_address4.", ".$resultdata->fld_addressPostcode, ", "))."</branchAddress>\n");
						print("\t\t<branchAddressNumber>".$resultdata->fld_addressNumber."</branchAddressNumber>\n");
						print("\t\t<branchAddressStreet>".$resultdata->fld_addressStreet."</branchAddressStreet>\n");
						print("\t\t<branchAddress2>".$resultdata->fld_address2."</branchAddress2>\n");
						print("\t\t<branchAddress3>".$resultdata->fld_address3."</branchAddress3>\n");
						print ("\t\t<location>\n");
						print ("\t\t\t<lat>".$resultdata->fld_lat."</lat>\n");
						print ("\t\t\t<lng>".$resultdata->fld_lng."</lng>\n");
						print ("\t\t</location>\n");
						print("\t\t<branchAddress4>".$resultdata->fld_address4."</branchAddress4>\n");
						print("\t\t<branchAddressPostcode>".$resultdata->fld_addressPostcode."</branchAddressPostcode>\n");
						print("\t\t<branchPhone>".$resultdata->fld_phone."</branchPhone>\n");
						print("\t\t<branchEmail>".$resultdata->fld_email."</branchEmail>");
					print("\t</branch>\n");
					break;
				case "region":
					print("\t<region>\n");
						print("\t\t<regionId>".$resultdata->fld_counter."</regionId>\n");
						print("\t\t<regionName>".$resultdata->fld_regionname."</regionName>\n");
						print("\t\t<regionDescription><![CDATA[".$resultdata->fld_regionDescription."]]></regionDescription>\n");
					print("\t</region>\n");
					break;
			}
		}

switch ($strview)
{
  case "commtypes": print("</propertyTypes>\n"); break;
  case "location": print("</locations>\n"); break;
  case "restypes": print("</propertyTypes>\n"); break;
  case "branch": print("</branches>\n"); break;
  case "region": print("</regions>\n"); break;
}
	}
	mysql_free_result ($result);
	mysql_close ($strdb);

?>