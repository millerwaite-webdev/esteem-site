<?php
/************************************************************/
/* Created By: Rhian singleton                              */
/* Email: rhian@millerwaite.com                             */
/*                                                          */
/* For: Ross Estate Agencies                                */
/************************************************************/
/* OUTPUTS COMPLETE FEED FROM DB COPY OF JUPIX PROPERTY     */
/* DATA FOR USE BY ROSS ESTATE AGENCIES                     */
/* INCLUDES FULL AND SHORT VIEW, FIND BY ID AND SEARCH      */
/************************************************************/
/* VERSION : 1.0                                            */
/* DATE    : 13/05/2010                                     */
/************************************************************/

include("incsitecommon.php");
include("geolocate.php");

$strdb = mysql_connect($strserver,$strdbuser,$strdbpass);
mysql_select_db($strdbname,$strdb);



$arrwhere = array();
//control visible area by user input
if(isset($_REQUEST['maxlat']) && $_REQUEST['maxlat'] != '') {$strmaxlat = (float)mysql_real_escape_string($_REQUEST['maxlat']); $strboundstype = "Set by User"; $booldefaultdistoverride = TRUE; $setbounds = TRUE;}
if(isset($_REQUEST['minlat']) && $_REQUEST['minlat'] != '') $strminlat = (float)mysql_real_escape_string($_REQUEST['minlat']);
if(isset($_REQUEST['maxlng']) && $_REQUEST['maxlng'] != '') $strmaxlng = (float)mysql_real_escape_string($_REQUEST['maxlng']);
if(isset($_REQUEST['minlng']) && $_REQUEST['minlng'] != '') $strminlng = (float)mysql_real_escape_string($_REQUEST['minlng']);


//set users prefered location
if(isset($_REQUEST['location']) || isset($_REQUEST['mylocation']) || (isset($_REQUEST['mylat']) && isset($_REQUEST['mylng'])))
{
	if(isset($_REQUEST['location']) && $_REQUEST['location'] != "")
	{
		$strlocation = mysql_real_escape_string(strtolower($_REQUEST['location']));
		$arrpostcodes = array();

		$strdbsql = "SELECT * FROM tbl_location WHERE fld_urlkey = '$strlocation'";
		$result = mysql_query ($strdbsql,$strdb);
		while($resultdata = mysql_fetch_object($result))
		{
			$arrpostcodes[] = " fld_addressPostcode LIKE '".$resultdata->fld_postcode."%' ";
			$strlat =  $resultdata->fld_lat;
			$strlng = $resultdata->fld_lng;
			$strlocdisp = $resultdata->fld_label.", UK";
			$booldefaultdistoverride = TRUE;
		}
	mysql_free_result ($result);
		if(!isset($_REQUEST['distance'])) $arrwhere[] = "(".implode("OR", $arrpostcodes).")";
	}
	else if(isset($_REQUEST['mylocation']) && $_REQUEST['mylocation'] != "")
	{
		$strmylocation = $_REQUEST['mylocation'];
		//normalise strings from users
		switch (strtolower($strmylocation))
		{
			case "askham": $strmylocation = "Askham-in-Furness"; break;
			case "barrow": $strmylocation = "Barrow-in-Furness"; break;
			case "barrow+island": case "barrow island": $strmylocation = "Barrow Island"; break;
			case "broughton": $strmylocation = "Broughton-in-Furness"; break;
			case "burnside": case "burnside, kendal": case "burnside,+kendal": case "burneside": case "burneside, kendal": case "burneside,+kendal": $strmylocation = "Burneside, Kendal"; break;
			case "cark": case "cark, lower holker": case "cark,+lower+holker": $strmylocation = "Cark, Lower Holker"; break;
			case "coniston": $strmylocation = "Coniston, Cumbria"; break;
			case "dalton":case "dalton-in-furness":case "dalton+in+furness":case "dalton in furness": $strmylocation = "Dalton-in-Furness"; break;
			case "dromer": case "dromer, windermere": case "dromer,+windermere": case "droomer": case "droomer, windermere": case "droomer,+windermere": $strmylocation = "Droomer, Windere"; break;
			case "foxfield": case "foxfield, broughton-in-furness": case "foxfield, broughton": case "foxfield,+broughton-in-furness": case "foxfield,+broughton": $strmylocation = "Foxfield, Broughton-in-Furness"; break;
			case "ireleth": case "ireleth,+cumbria": case "ireleth, cumbria": $strmylocation = "Ireleth, Cumbria"; break;
			case "grange": $strmylocation = "Grange-over-Sands"; break;
			case "kents bank": $strmylocation = "Kents bank, Cumbria"; break;
			case "kirkby": $strmylocation = "Kirkby-in-Furness, Cumbria"; break;
			case "meathop, grnge-over-sands": case "meathop,+grnge-over-sands": $strmylocation = "Meathop, Grange-over-Sands"; break;
			case "mill brow": case "mill+brow": case "mill brow, barrow-in-furness": case "mill+brow,+barrow-in-furness": $strmylocation = "Mill Brow, Barrow-in-Furness"; break;
			case "mill brow": case "mill+brow": case "mill brow, barrow-in-furness": case "mill+brow,+barrow-in-furness": $strmylocation = "Mill Brow, Barrow-in-Furness"; break;
			case "roose": case "roose, barrow-in-furness": case "roose,+barrow-in-furness": $strmylocation = "Roose, Barrow-in-Furness"; break;
			case "loppergarth": case "pennington": case "pennington,+ulverston": case "pennington, ulverston": case "loppergarth,+ulverston": case "loppergarth, ulverston": case "loppergarth, penngington, ulverston": case "Loppergarth,+penngington,+ulverston": $strmylocation = "Pennington, Ulverston"; break;
			case "silverdale": case "silverdale, carnforth": case "silverdale,+carnforth": $strmylocation = "Silverdale, Carnforth"; break;
			case "south walney": $strmylocation = "South Walney"; break;
			case "urswick": case "urswick, cumbria": case "urswick,+cumbria": $strmylocation = "Urswick, Cumbria"; break;
			case "walney": case "walney island": case "walney+island": $strmylocation = "Walney"; break;
		}


		//location searches and manual overrides
		switch ($strmylocation) {
                  case "Barrow-in-Furness":
                  $strlat = 54.117144;
                  $strlng = -3.225735;
                  $strlocdisp = "Barrow-in-Furness, Cumbria";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {
                  
                  $strmaxlat = 54.144238;
                  $strminlat = 54.082656;
                  $strmaxlng = -3.182116;
                  $strminlng = -3.281164;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;
                  case "Barrow Island":
                  $strlat = 54.104125;
                  $strlng = -3.231525;
                  $strlocdisp = "Barrow Island, Cumbria";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {
                  
                  $strmaxlat = 54.108905;
                  $strminlat = 54.093178;
                  $strmaxlng = -3.22526;
                  $strminlng = -3.239937;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;

                  case "Dalton-in-Furness":
                  $strlat = 54.155524;
                  $strlng = -3.180199;
                  $strlocdisp = "Dalton-In-Furness, Cumbria";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {

                  $strmaxlat = 54.163213;
                  $strminlat = 54.141549;
                  $strmaxlng = -3.168483;
                  $strminlng = -3.198223;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;
                  case "Foxfield, Broughton-in-Furness":
                  $strlat = 54.25808;
                  $strlng = -3.214599;
                  $strlocdisp = "Foxfield, Broughton-in-Furness, Cumbria";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {

                  $strmaxlat = 54.261101;
                  $strminlat = 54.254194;
                  $strmaxlng = -3.209299;
                  $strminlng = -3.218247;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;
                  case "Ireleth, Cumbria":
                  $strlat = 54.1883004;
                  $strlng = -3.1916276;
                  $strlocdisp = "Ireleth, Cumbria";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {

                  $strmaxlat = 54.192725;
                  $strminlat = 54.185594;
                  $strmaxlng = -3.183818;
                  $strminlng = -3.204589;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;
                  case "Meathop, Grange-over-Sands":
                  $strlat = 54.21888;
                  $strlng = -2.864177;
                  $strlocdisp = "Meathop, Grange-over-Sands";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {

                  $strmaxlat = 54.224349;
                  $strminlat = 54.202115;
                  $strmaxlng = -2.848213;
                  $strminlng = -2.879369;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;
                  case "Mill Brow, Barrow-in-Furness":
                  $strlat = 54.144314;
                  $strlng = -3.195655;
                  $strlocdisp = "Mill Brow, Barrow-in-Furness";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {

                  $strmaxlat = 54.147531;
                  $strminlat = 54.140116;
                  $strmaxlng = -3.187459;
                  $strminlng = -3.202136;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;
                  case "Pennington, Ulverston":
                  $strlat = 54.186724;
                  $strlng = -3.133832;
                  $strlocdisp = "Pennington, Ulverston";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {

                  $strmaxlat = 54.1898;
                  $strminlat = 54.182794;
                  $strmaxlng = -3.127524;
                  $strminlng = -3.1419;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;
                  case "Roose, Barrow-in-Furness":
                  $strlat = 54.114691;
                  $strlng = -3.191879;
                  $strlocdisp = "Roose, Barrow-in-Furness";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {

                  $strmaxlat = 54.124476;
                  $strminlat = 54.109006;
                  $strmaxlng = -3.179262;
                  $strminlng = -3.196857;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;
                  case "Silverdale, Carnforth":
                  $strlat = 54.167459;
                  $strlng = -2.826676;
                  $strlocdisp = "Silverdale, Carnforth";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {
                  
                  $strmaxlat = 54.177959;
                  $strminlat = 54.162233;
                  $strmaxlng = -2.813287;
                  $strminlng = -2.836289;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;
                  case "South Walney":
                  $strlat = 54.10264;
                  $strlng = -3.252039;
                  $strlocdisp = "South Walney";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {
                  
                  $strmaxlat = 54.107723;
                  $strminlat = 54.092674;
                  $strmaxlng = -3.245173;
                  $strminlng = -3.255301;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;
                  case "Urswick, Cumbria":
                  $strlat = 54.156705;
                  $strlng = -3.123994;
                  $strlocdisp = "Urswick, Cumbria";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {
                  
                  $strmaxlat = 54.168012;
                  $strminlat = 54.15012;
                  $strmaxlng = -3.110175;
                  $strminlng = -3.13498;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;
                  case "Walney":
                  $strlat = 54.107421;
                  $strlng = -3.251438;
                  $strlocdisp = "Walney Island, Cumbria";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {
                  
                  $strmaxlat = 54.123117;
                  $strminlat = 54.083361;
                  $strmaxlng = -3.235989;
                  $strminlng = -3.272038;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;
                  case "Windermere":
                  $strlat = 54.373434;
                  $strlng = -2.913444;
                  $strlocdisp = "Windermere, Cumbria";
		if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {
                  
                  $strmaxlat = 54.386755;
                  $strminlat = 54.357056;
                  $strmaxlng = -2.889676;
                  $strminlng = -2.930617;
  			$strboundstype = "Manually set for Location";
  			$booldefaultdistoverride = TRUE;
			$setbounds = TRUE;
			}
                  break;
                  default:
                  //use google to find
		$location = geolocate($strmylocation);

		if(is_array($location))
		{
			$strlat =  $location["lat"];
			$strlng = $location["lng"];
			$strlocdisp = $location["disp"];
			if((!isset($_REQUEST['distance']) || $_REQUEST['distance'] == '') && !isset($_REQUEST['property'])) {
			if(!isset($strmaxlat)) $strmaxlat = $location["maxlat"];
			if(!isset($strminlat)) $strminlat = $location["minlat"];
			if(!isset($strmaxlng)) $strmaxlng = $location["maxlng"];
			if(!isset($strminlng)) $strminlng = $location["minlng"];
			$strboundstype = "Set by Location";
			$setbounds = TRUE;
			}
		}
		else
		{
			$strlocdisp = " $location";
		}
		$location = NULL;
                  break;
  }
		if(!isset($booldefaultdistoverride)) $booldefaultdistoverride = FALSE;
	}
	else if($_REQUEST['mylat'] != "" && $_REQUEST['mylng'] != "")
	{
		$strlat = mysql_real_escape_string($_REQUEST['mylat']);
		$strlng = mysql_real_escape_string($_REQUEST['mylng']);
		$strlocdisp = "User defined";
		if(!isset($booldefaultdistoverride)) $booldefaultdistoverride = FALSE;
	}

	if (isset($strlat) && isset($strlng) && ((float)$strlat)+((float)$strlng) != 0)
	{
		$strdistfld = " SQRT( POW((69.1 * ($strlat - tbl_property.fld_lat)), 2) + POW((53 * ($strlng - tbl_property.fld_lng)), 2))";
		if(isset($_REQUEST['distance']) && $_REQUEST['distance'] != '') {
			$strdistance = mysql_real_escape_string($_REQUEST['distance']);
			if($strdistance != 0) $arrwhere[] = $strdistfld." <= '$strdistance' ";
		}
		//
		else if (!$booldefaultdistoverride && ((!isset($strmaxlat) && !isset ($strminlat) && !isset($strmaxlng) && !isset($strminlng)) || ($strmaxlat-$strminlat < 0.007236) || ($strmaxlng-$strminlng < 0.009434))) {
                  //if not been told to skip this and bounds are not set and bounds are less than a quarter mile in either direction
			$strboundstype = "Search Bounds overwritten by default distance";
			$setbounds = FALSE;

			unset($strmaxlat); unset($strmaxlng); unset($strminlat); unset($strminlng);
			$strdistance = 0.5;
			$arrwhere[] = $strdistfld." <= '$strdistance' ";
		}

		$strdistfld = $strdistfld." AS fld_distance, ";
	}
	else $strdistfld = "";
}
else $strdistfld = "";



if($setbounds == TRUE)
{
  $arrwhere[] = " (tbl_property.fld_lat <= '$strmaxlat' AND tbl_property.fld_lat >= '$strminlat' AND tbl_property.fld_lng <= '$strmaxlng' AND tbl_property.fld_lng >= '$strminlng') ";
}
else $setbounds = FALSE;

if(isset($_REQUEST['start']) && $_REQUEST['start'] != '') $intstart = (int)mysql_real_escape_string($_REQUEST['start']);
if(isset($_REQUEST['limit']) && $_REQUEST['limit'] != '') $intlimit = (int)mysql_real_escape_string($_REQUEST['limit']);

if(isset($intlimit))
{
  if(!isset($intstart)) $intstart = 0;
  $strsqllimit = " LIMIT $intstart, $intlimit ";
}
else $strsqllimit = "";

//set
if(isset($_REQUEST['selltype'])) {
	$strselltype = mysql_real_escape_string($_REQUEST['selltype']);
/*	if (strtolower($strselltype) == 'commercial')
	{
		$arrwhere[] = " tbl_property.fld_department = '$strselltype' ";
		$strtable = "tbl_property.*, tbl_property.fld_propertyID AS fld_ID, tbl_commercial.*, tbl_commercial.fld_availability AS fld_commAvailability, tbl_commercial.fld_rentFrequency AS fld_commRentFrequency, tbl_commercial.fld_forSalePOA AS fld_commForSalePOA, tbl_commercial.fld_toLetPOA AS fld_commToLetPOA FROM tbl_property INNER JOIN tbl_commercial ON tbl_property.fld_propertyID = tbl_commercial.fld_propertyID ";
		if(isset($_REQUEST['price'])) {
			$strprice = mysql_real_escape_string($_REQUEST['price']);
			if($strprice != 0) $arrwhere[] = " ((tbl_commercial.fld_priceTo <= $strprice AND tbl_commercial.fld_priceTo != 0) OR (tbl_commercial.fld_rentTo <= $strprice AND tbl_commercial.fld_rentTo != 0) OR tbl_commercial.fld_forSalePOA = 1 OR tbl_commercial.fld_toLetPOA = 1) ";
		}
	}*/
	if (strtolower($strselltype) == 'commlet')
	{
		$arrwhere[] = " tbl_property.fld_department = 'Commercial' ";
		$strtable = "tbl_property.*, tbl_property.fld_propertyID AS fld_ID, tbl_commercial.*, tbl_commercial.fld_availability AS fld_commAvailability, tbl_commercial.fld_rentFrequency AS fld_commRentFrequency, tbl_commercial.fld_forSalePOA AS fld_commForSalePOA, tbl_commercial.fld_toLetPOA AS fld_commToLetPOA FROM tbl_property INNER JOIN tbl_commercial ON tbl_property.fld_propertyID = tbl_commercial.fld_propertyID  LEFT JOIN tbl_commPropertyTypes ON tbl_property.fld_propertyID = tbl_commPropertyTypes.fld_propertyType ";
		$arrwhere[] = " tbl_commercial.fld_toLet = 1 ";
		if(isset($_REQUEST['proptype'])) {
			$strproptype = mysql_real_escape_string($_REQUEST['proptype']);
			if($strproptype != 0) $arrwhere[] = " tbl_commPropertyTypes.fld_propertyType = '$strproptype' ";
		}
		if(isset($_REQUEST['price'])) {
			$strprice = mysql_real_escape_string($_REQUEST['price']);
			if($strprice != 0) $arrwhere[] = " (tbl_commercial.fld_rentTo <= $strprice OR tbl_commercial.fld_toLetPOA = 1) ";
		}
	}
	else if (strtolower($strselltype) == 'commsales')
	{
		$arrwhere[] = " tbl_property.fld_department = 'Commercial' ";
		$strtable = "tbl_property.*, tbl_property.fld_propertyID AS fld_ID, tbl_commercial.*, tbl_commercial.fld_availability AS fld_commAvailability, tbl_commercial.fld_rentFrequency AS fld_commRentFrequency, tbl_commercial.fld_forSalePOA AS fld_commForSalePOA, tbl_commercial.fld_toLetPOA AS fld_commToLetPOA FROM tbl_property INNER JOIN tbl_commercial ON tbl_property.fld_propertyID = tbl_commercial.fld_propertyID  LEFT JOIN tbl_commPropertyTypes ON tbl_property.fld_propertyID = tbl_commPropertyTypes.fld_propertyType ";
		$arrwhere[] = " tbl_commercial.fld_forSale = 1 ";
		if(isset($_REQUEST['proptype'])) {
			$strproptype = mysql_real_escape_string($_REQUEST['proptype']);
			if($strproptype != 0) $arrwhere[] = " tbl_commPropertyTypes.fld_propertyType = '$strproptype' ";
		}
		if(isset($_REQUEST['price'])) {
			$strprice = mysql_real_escape_string($_REQUEST['price']);
			if($strprice != 0) $arrwhere[] = " (tbl_commercial.fld_priceTo <= $strprice OR tbl_commercial.fld_forSalePOA = 1) ";
		}
	}
	else if(strtolower($strselltype) == 'sales')
	{
		$arrwhere[] = " tbl_property.fld_department = 'Sales' ";
		$strtable = "tbl_residentialSales.fld_availability AS fld_saleAvailability, tbl_residentialSales.fld_forSalePOA AS fld_resForSalePOA, tbl_residentialSales.*, tbl_property.*, tbl_property.fld_propertyID AS fld_ID, tbl_residential.* FROM tbl_property INNER JOIN tbl_residential ON tbl_property.fld_propertyID = tbl_residential.fld_propertyID INNER JOIN tbl_residentialSales ON tbl_property.fld_propertyID = tbl_residentialSales.fld_propertyID ";
		if(isset($_REQUEST['bedrooms'])) {
			$strbedrooms = mysql_real_escape_string($_REQUEST['bedrooms']);
			$arrwhere[] = " tbl_residential.fld_propertyBedrooms >= '$strbedrooms' ";
		}

		if(isset($_REQUEST['proptype'])) {
			$strproptype = mysql_real_escape_string($_REQUEST['proptype']);
			if($strproptype != 0) $arrwhere[] = " tbl_residential.fld_propertyType = '$strproptype' ";
		}
		if(isset($_REQUEST['price'])) {
			$strprice = mysql_real_escape_string($_REQUEST['price']);
			if($strprice != 0) $arrwhere[] = " (tbl_residentialSales.fld_price <= $strprice OR tbl_residentialSales.fld_forSalePOA = 1) ";
		}
	}
	else if(strtolower($strselltype) == 'lettings')
	{
		$arrwhere[] = " tbl_property.fld_department = '$strselltype' ";
		$strtable = "tbl_residentialLet.fld_availability AS fld_letAvailability, tbl_residentialLet.fld_toLetPOA AS fld_resToLetPOA, tbl_residentialLet.fld_rentFrequency AS fld_resRentFrequency, tbl_residentialLet.*, tbl_property.*, tbl_property.fld_propertyID AS fld_ID, tbl_residential.* FROM tbl_property INNER JOIN tbl_residential ON tbl_property.fld_propertyID = tbl_residential.fld_propertyID INNER JOIN tbl_residentialLet ON tbl_property.fld_propertyID = tbl_residentialLet.fld_propertyID ";
		if(isset($_REQUEST['bedrooms'])) {
			$strbedrooms = mysql_real_escape_string($_REQUEST['bedrooms']);
			$arrwhere[] = " tbl_residential.fld_propertyBedrooms >= '$strbedrooms' ";
		}

		if(isset($_REQUEST['proptype'])) {
			$strproptype = mysql_real_escape_string($_REQUEST['proptype']);
			if($strproptype != 0) $arrwhere[] = " tbl_residential.fld_propertyType = '$strproptype' ";
		}
		if(isset($_REQUEST['price'])) {
			$strprice = mysql_real_escape_string($_REQUEST['price']);
			if($strprice != 0) $arrwhere[] = " (tbl_residentialLet.fld_rent <= $strprice OR tbl_residentialLet.fld_toLetPOA = 1) ";
		}
	}
}



if(isset($_REQUEST['sort'])) {
	$strsort = mysql_real_escape_string($_REQUEST['sort']);
	if (strtolower($strselltype) == 'commlet')
	{
		$strsortfld = "tbl_commercial.fld_rentTo";
	}
	else if (strtolower($strselltype) == 'commsales')
	{
		$strsortfld = "tbl_commercial.fld_priceTo";
	}
	else if(strtolower($strselltype) == 'sales')
	{
		$strsortfld = "tbl_residentialSales.fld_price";
	}
	else if(strtolower($strselltype) == 'lettings')
	{
		$strsortfld = "tbl_residentialLet.fld_rent";
	}
	if($strsortfld != "" && $strsort == "low") $strsqlsort = "ORDER BY $strsortfld ";
	else if($strsortfld != "" && $strsort == "high") $strsqlsort = "ORDER BY $strsortfld DESC";
	else if($strsort == "distance" && $strdistfld != "") $strsqlsort = "ORDER BY fld_distance ";
	else $strsqlsort = "";
}
else $strsqlsort = "";

if(isset($_REQUEST['favourites'])) {
	$strfavlist = $_REQUEST['favourites'];
	$arrfavorites = explode(",", $strfavlist);
	$arrfavwhere = array();
	foreach($arrfavorites AS $value)
	{
		$arrfavwhere[] = " tbl_property.fld_propertyID = '".mysql_real_escape_string($value)."' ";
	}
	$arrwhere[] = " (".implode("OR", $arrfavwhere).") ";
}

if(isset($_REQUEST['property'])) {
	$strproperty = mysql_real_escape_string($_REQUEST['property']);
	$arrwhere[] = " tbl_property.fld_propertyID = '$strproperty' ";
	$strview= "full";
}
else $strview = "short";


if(!isset($strtable)) $strtable = "tbl_property.*, tbl_property.fld_propertyID AS fld_ID, tbl_commercial.*, tbl_commercial.fld_availability AS fld_commAvailability, tbl_commercial.fld_rentFrequency AS fld_commRentFrequency, tbl_commercial.fld_forSalePOA AS fld_commForSalePOA, tbl_commercial.fld_toLetPOA AS fld_commToLetPOA, tbl_residential.*, tbl_residentialSales.fld_availability AS fld_saleAvailability, tbl_residentialSales.fld_forSalePOA AS fld_resForSalePOA, tbl_residentialSales.*, tbl_residentialLet.fld_availability AS fld_letAvailability, tbl_residentialLet.fld_toLetPOA AS fld_resToLetPOA, tbl_residentialLet.fld_rentFrequency AS fld_resRentFrequency, tbl_residentialLet.* FROM tbl_property LEFT JOIN tbl_commercial ON tbl_property.fld_propertyID = tbl_commercial.fld_propertyID LEFT JOIN tbl_residential ON tbl_property.fld_propertyID = tbl_residential.fld_propertyID LEFT JOIN tbl_residentialSales ON tbl_property.fld_propertyID = tbl_residentialSales.fld_propertyID LEFT JOIN tbl_residentialLet ON tbl_property.fld_propertyID = tbl_residentialLet.fld_propertyID";
if (count($arrwhere) > 0 ) $strwhere = "WHERE ".implode("AND", $arrwhere);
else $strwhere = "";

    header ("Content-type: text/xml; charset=utf-8");
	print(utf8_encode ("<?xml version='1.0' encoding='UTF-8' ?>\n"));
	print(utf8_encode ("<properties>\n"));
			if(isset($strlocdisp)) print(utf8_encode ("\t<mylocation>\n\t\t<lat>$strlat</lat>\n\t\t<lng>$strlng</lng>\n\t\t<display>$strlocdisp</display>\n\t\t\n\t</mylocation>\n"));
	$strdbsql = "SELECT ".$strdistfld.$strtable." ".$strwhere.$strsqlsort;
	$result = mysql_query ($strdbsql,$strdb);
  $strnumresults = mysql_num_rows($result);
	mysql_free_result ($result);
if ($strview == "short")
{

if($setbounds == FALSE)
{
  //work out current max & min lat/lng values...
  $strdbsql = "SELECT ".$strtable." ".$strwhere." AND (tbl_property.fld_lat != 0 OR tbl_property.fld_lng != 0) ORDER BY tbl_property.fld_lat LIMIT 1";
  $result = mysql_query ($strdbsql,$strdb);
  $resultdata = mysql_fetch_object($result);
  $strminlat = $resultdata->fld_lat;
	mysql_free_result ($result);
  $strdbsql = "SELECT ".$strtable." ".$strwhere." AND (tbl_property.fld_lat != 0 OR tbl_property.fld_lng != 0) ORDER BY tbl_property.fld_lat DESC LIMIT 1";
  $result = mysql_query ($strdbsql,$strdb);
  $resultdata = mysql_fetch_object($result);
  $strmaxlat = $resultdata->fld_lat;
	mysql_free_result ($result);
  $strdbsql = "SELECT ".$strtable." ".$strwhere." AND (tbl_property.fld_lat != 0 OR tbl_property.fld_lng != 0) ORDER BY tbl_property.fld_lng LIMIT 1";
  $result = mysql_query ($strdbsql,$strdb);
  $resultdata = mysql_fetch_object($result);
  $strminlng = $resultdata->fld_lng;
	mysql_free_result ($result);
  $strdbsql = "SELECT ".$strtable." ".$strwhere." AND (tbl_property.fld_lat != 0 OR tbl_property.fld_lng != 0) ORDER BY tbl_property.fld_lng DESC LIMIT 1";
  $result = mysql_query ($strdbsql,$strdb);
  $resultdata = mysql_fetch_object($result);
  $strmaxlng = $resultdata->fld_lng;
	mysql_free_result ($result);
  //incorporate user location if necassary
  if((isset($strlat) && $strlat > $strmaxlat) || $strmaxlat == "") $strmaxlat = $strlat;
  if((isset($strlat) && $strlat < $strminlat) || $strminlat == "") $strminlat = $strlat;
  if((isset($strlng) && $strlng > $strmaxlng) || $strmaxlng == "") $strmaxlng = $strlng;
  if((isset($strlng) && $strlng < $strminlng) || $strminlng == "") $strminlng = $strlng;
  $setbounds = TRUE;
  if (isset($strboundstype) && $strboundstype != "") print("\t<boundtype>$strboundstype</boundtype>\n");
  else print("\t<boundtype>Bounds Set by Results</boundtype>\n");
} else print("\t<boundtype>$strboundstype</boundtype>\n");
  print(utf8_encode("\t<currentview>\n\t\t<maxlat>$strmaxlat</maxlat>\n\t\t<minlat>$strminlat</minlat>\n\t\t<maxlng>$strmaxlng</maxlng>\n\t\t<minlng>$strminlng</minlng>\n\t\t<results>$strnumresults</results>\n\t</currentview>\n\n"));
}

$url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	
	$strdbsql = "SELECT ".$strdistfld.$strtable." ".$strwhere.$strsqlsort.$strsqllimit;
	$result = mysql_query ($strdbsql,$strdb);
//print("<mysql><![CDATA[$strdbsql]]></mysql>");
	if (mysql_num_rows($result) != 0)
	{
if ($strview == "full")
{
		while($resultdata = mysql_fetch_object($result)) {
                  if(isset($_REQUEST['view']) && $_REQUEST['view'] == 'more') mysql_query("insert into tbl_stats(fld_propertyID, fld_ip, fld_agent, fld_lang, fld_ref, fld_time, fld_type) values('".$resultdata->fld_ID."','".mysql_real_escape_string($_SERVER['REMOTE_ADDR'])."', '".mysql_real_escape_string($_SERVER['HTTP_USER_AGENT'])."', '".mysql_real_escape_string($_SERVER['HTTP_ACCEPT_LANGUAGE'])."', '".mysql_real_escape_string($url)."', ".mktime().", 'more')");
                  else mysql_query("insert into tbl_stats(fld_propertyID, fld_ip, fld_agent, fld_lang, fld_ref, fld_time, fld_type) values('".$resultdata->fld_ID."','".mysql_real_escape_string($_SERVER['REMOTE_ADDR'])."', '".mysql_real_escape_string($_SERVER['HTTP_USER_AGENT'])."', '".mysql_real_escape_string($_SERVER['HTTP_ACCEPT_LANGUAGE'])."', '".mysql_real_escape_string($url)."', ".mktime().", 'main')");

			print("<property>\n");
			print("\t<propertyID>".$resultdata->fld_ID."</propertyID>\n");
			print("\t<branchID>".$resultdata->fld_branchID."</branchID>\n");
			print("\t<branchName>".$resultdata->fld_branchName."</branchName>\n");
			$strdbsql2 = "SELECT * FROM tbl_branch WHERE fld_counter = ".$resultdata->fld_branchID."";
			$result2 = mysql_query ($strdbsql2,$strdb);
			$resultdata2 = mysql_fetch_object($result2);
			print("\t<branchNumber>".$resultdata2->fld_phone."</branchNumber>\n");
			print("\t<branchAddress>".trim($resultdata2->fld_addressNumber." ".$resultdata2->fld_addressStreet.", ".$resultdata2->fld_address3.", ".$resultdata2->fld_address4.", ".$resultdata2->fld_addressPostcode)."</branchAddress>\n");
			print("\t<branchEmail>".$resultdata2->fld_email."</branchEmail>\n");
			mysql_free_result ($result2);
			print("\t<clientName>".$resultdata->fld_clientName."</clientName>\n");
			print("\t<department>".$resultdata->fld_department."</department>\n");
			print("\t<referenceNumber>".$resultdata->fld_referenceNumber."</referenceNumber>\n");
			print("\t<addressName>".$resultdata->fld_addressName."</addressName>\n");
			print("\t<addressNumber>".$resultdata->fld_addressNumber."</addressNumber>\n");
			print("\t<addressStreet>".$resultdata->fld_addressStreet."</addressStreet>\n");
			print("\t<address2>".$resultdata->fld_address2."</address2>\n");
			print("\t<address3>".$resultdata->fld_address3."</address3>\n");
			print("\t<address4>".$resultdata->fld_address4."</address4>\n");
			print("\t<addressPostcode>".$resultdata->fld_addressPostcode."</addressPostcode>\n");
			print("\t<country>".$resultdata->fld_country."</country>\n");
			if(trim($resultdata->fld_displayAddress) != "") print("\t<displayAddress>".$resultdata->fld_displayAddress."</displayAddress>\n");
			else print("\t<displayAddress>".trim($resultdata->fld_addressName." ".$resultdata->fld_addressNumber." ".$resultdata->fld_addressStreet)."</displayAddress>\n");
			print("\t<propertyFeature1>".$resultdata->fld_propertyFeature1."</propertyFeature1>\n");
			print("\t<propertyFeature2>".$resultdata->fld_propertyFeature2."</propertyFeature2>\n");
			print("\t<propertyFeature3>".$resultdata->fld_propertyFeature3."</propertyFeature3>\n");
			print("\t<propertyFeature4>".$resultdata->fld_propertyFeature4."</propertyFeature4>\n");
			print("\t<propertyFeature5>".$resultdata->fld_propertyFeature5."</propertyFeature5>\n");
			print("\t<propertyFeature6>".$resultdata->fld_propertyFeature6."</propertyFeature6>\n");
			print("\t<propertyFeature7>".$resultdata->fld_propertyFeature7."</propertyFeature7>\n");
			print("\t<propertyFeature8>".$resultdata->fld_propertyFeature8."</propertyFeature8>\n");
			print("\t<propertyFeature9>".$resultdata->fld_propertyFeature9."</propertyFeature9>\n");
			print("\t<propertyFeature10>".$resultdata->fld_propertyFeature10."</propertyFeature10>\n");
			print("\t<dateLastModified>".$resultdata->fld_dateLastModified."</dateLastModified>\n");
			print("\t<regionID>".$resultdata->fld_regionID."</regionID>\n");
			$strdbsql2 = "SELECT * FROM tbl_region WHERE fld_counter = ".$resultdata->fld_regionID."";
			$result2 = mysql_query ($strdbsql2,$strdb);
			$resultdata2 = mysql_fetch_object($result2);
			print(utf8_encode("\t<regionName><![CDATA[".$resultdata2->fld_regionName."]]></regionName>\n"));
			mysql_free_result ($result2);
			print(utf8_encode(str_replace("&", "&amp;", html_entity_decode("\t<mainSummary>".$resultdata->fld_mainSummary."</mainSummary>\n"))));
			print(utf8_encode(str_replace("�", "'", str_replace("�", "\"", str_replace("&", "&amp;", html_entity_decode("\t<fullDescription><![CDATA[".$resultdata->fld_fullDescription."]]></fullDescription>\n"))))));
			if($resultdata->fld_lat != 0 || $resultdata->fld_lng != 0)
			{
                          print("\t<location>\n");
                            print("\t\t<lat>".$resultdata->fld_lat."</lat>\n");
                            print("\t\t<lng>".$resultdata->fld_lng."</lng>\n");
                            if(isset($resultdata->fld_distance) && $resultdata->fld_distance != "") print("\t\t<distance>".number_format($resultdata->fld_distance,2)." miles</distance>\n");
                          print("\t</location>\n");
                        }

			if(strtolower($resultdata->fld_department) == "commercial")
			{
				print("\t<forSale>".$resultdata->fld_forSale."</forSale>\n");
				print("\t<toLet>".$resultdata->fld_toLet."</toLet>\n");
				//print("\t<availability>".$resultdata->fld_availability."</availability>\n");
				$strdbsql = "SELECT * FROM tbl_commAvailability WHERE fld_counter = ".$resultdata->fld_commAvailability;
				$result2 = mysql_query ($strdbsql,$strdb);
				if (mysql_num_rows($result2) != 0)
				{
					while($resultdata2 = mysql_fetch_object($result2)) {
						print("\t<availability>".$resultdata2->fld_value."</availability>\n");
					}
				}
				mysql_free_result ($result2);

				if($resultdata->fld_forSale == 1)
				{
				if($resultdata->fld_commForSalePOA == 1) print(utf8_encode("\t<price>POA</price>\n"));
				else if($resultdata->fld_priceFrom != 0) print(utf8_encode("\t<price><![CDATA[�".number_format($resultdata->fld_priceFrom,0)."-�".number_format($resultdata->fld_priceTo,0)."]]></price>\n"));
				else print(utf8_encode("\t<price><![CDATA[�".number_format($resultdata->fld_priceTo,0)."]]></price>\n"));
				}
				else if ($resultdata->fld_toLet == 1)
				{
				if($resultdata->fld_commToLetPOA == 1) print(utf8_encode("\t<price>POA</price>\n"));
				else if($resultdata->fld_rentFrom != 0) print(utf8_encode("\t<price><![CDATA[�".number_format($resultdata->fld_rentFrom,0)."-�".number_format($resultdata->fld_rentTo,0)." ".$resultdata->fld_commRentFrequency."]]></price>\n"));
				else print(utf8_encode("\t<price><![CDATA[�".number_format($resultdata->fld_rentTo,0)." ".$resultdata->fld_commRentFrequency."]]></price>\n"));
				}

				print("\t<floorAreaTo>".$resultdata->fld_floorAreaTo."</floorAreaTo>\n");
				print("\t<floorAreaFrom>".$resultdata->fld_floorAreaFrom."</floorAreaFrom>\n");
				print("\t<floorAreaUnits>".$resultdata->fld_floorAreaUnits."</floorAreaUnits>\n");
				print("\t<siteArea>".$resultdata->fld_siteArea."</siteArea>\n");
				print("\t<siteAreaUnits>".$resultdata->fld_siteAreaUnits."</siteAreaUnits>\n");
				if($resultdata->fld_strapLine != "") print(utf8_encode(str_replace("&pound;", "�","\t<strapLine><![CDATA[".$resultdata->fld_strapLine."]]></strapLine>\n")));
				else print("<strapline></strapline>");

				$strdbsql = "SELECT * FROM tbl_commPropertyTypes INNER JOIN tbl_commPropertyType ON tbl_commPropertyTypes.fld_propertyType = tbl_commPropertyType.fld_counter WHERE tbl_commPropertyTypes.fld_propertyID = ".$resultdata->fld_ID;
				$result2 = mysql_query ($strdbsql,$strdb);
				if (mysql_num_rows($result2) != 0)
				{
					print("\t<propertyTypes>\n");
					while($resultdata2 = mysql_fetch_object($result2)) {
						print("\t\t<propertyType>".$resultdata2->fld_value."</propertyType>\n");
					}
					print("\t</propertyTypes>\n");
				}
				mysql_free_result ($result2);
			}
			else
			{
				print("\t<propertyBedrooms>".$resultdata->fld_propertyBedrooms."</propertyBedrooms>\n");
				print("\t<propertyBathrooms>".$resultdata->fld_propertyBathrooms."</propertyBathrooms>\n");
				print("\t<propertyEnsuites>".$resultdata->fld_propertyEnsuites."</propertyEnsuites>\n");
				print("\t<propertyReceptionRooms>".$resultdata->fld_propertyReceptionRooms."</propertyReceptionRooms>\n");
				print("\t<propertyKitchens>".$resultdata->fld_propertyKitchens."</propertyKitchens>\n");
				//print("\t<propertyAge>".$resultdata->fld_propertyAge."</propertyAge>\n");
				$strdbsql = "SELECT * FROM tbl_residentialPropertyAge WHERE fld_counter = ".$resultdata->fld_propertyAge;
				$result2 = mysql_query ($strdbsql,$strdb);
				if (mysql_num_rows($result2) != 0)
				{
					while($resultdata2 = mysql_fetch_object($result2)) {
						print("\t<propertyAge>".$resultdata2->fld_value."</propertyAge>\n");
					}
				}
				mysql_free_result ($result2);
				//print("\t<propertyType>".$resultdata->fld_propertyType."</propertyType>\n");
				$strdbsql = "SELECT * FROM tbl_residentialPropertyType WHERE fld_counter = ".$resultdata->fld_propertyType;
				$result2 = mysql_query ($strdbsql,$strdb);
				if (mysql_num_rows($result2) != 0)
				{
					while($resultdata2 = mysql_fetch_object($result2)) {
						print("\t<propertyType>".$resultdata2->fld_value."</propertyType>\n");
						$strproptype = $resultdata2->fld_value;
					}
				}
				mysql_free_result ($result2);
				if(trim($resultdata->fld_displayPropertyType) != "") print("\t<displayPropertyType>".$resultdata->fld_displayPropertyType."</displayPropertyType>\n");
				else  print("\t<displayPropertyType>".$strproptype."</displayPropertyType>\n");
				//print("\t<propertyStyle>".$resultdata->fld_propertyStyle."</propertyStyle>\n");
				$strdbsql = "SELECT * FROM tbl_residentialPropertyStyle WHERE fld_counter = ".$resultdata->fld_propertyStyle;
				$result2 = mysql_query ($strdbsql,$strdb);
				if (mysql_num_rows($result2) != 0)
				{
					while($resultdata2 = mysql_fetch_object($result2)) {
						print("\t<propertyStyle>".$resultdata2->fld_value."</propertyStyle>\n");
					}
				}
				mysql_free_result ($result2);
				if(strtolower($resultdata->fld_department) == "sales")
				{
					//print("\t<availability>".$resultdata->fld_availability."</availability>\n");
					$strdbsql = "SELECT * FROM tbl_residentialSaleAvailability WHERE fld_counter = ".$resultdata->fld_saleAvailability;
					$result2 = mysql_query ($strdbsql,$strdb);
					if (mysql_num_rows($result2) != 0)
					{
						while($resultdata2 = mysql_fetch_object($result2)) {
							print("\t<availability>".$resultdata2->fld_value."</availability>\n");
						}
					}
					mysql_free_result ($result2);
					if($resultdata->fld_resForSalePOA == 1) print("\t<price>POA</price>\n");
					else print(utf8_encode("\t<price><![CDATA[�".number_format($resultdata->fld_price,0)."]]></price>\n"));
					//print("\t<priceQualifier>".$resultdata->fld_priceQualifier."</priceQualifier>\n");
					$strdbsql = "SELECT * FROM tbl_residentialSalesPriceQualifier WHERE fld_counter = ".$resultdata->fld_priceQualifier;
					$result2 = mysql_query ($strdbsql,$strdb);
					if (mysql_num_rows($result2) != 0)
					{
						while($resultdata2 = mysql_fetch_object($result2)) {
							print("\t<priceQualifier>".$resultdata2->fld_value."</priceQualifier>\n");
						}
					}
					mysql_free_result ($result2);
				}
				else if(strtolower($resultdata->fld_department) == "lettings")
				{
					//print("\t<availability>".$resultdata->fld_availability."</availability>\n");
					$strdbsql = "SELECT * FROM tbl_residentialLetAvailability WHERE fld_counter = ".$resultdata->fld_letAvailability;
					$result2 = mysql_query ($strdbsql,$strdb);
					if (mysql_num_rows($result2) != 0)
					{
						while($resultdata2 = mysql_fetch_object($result2)) {
							print("\t<availability>".$resultdata2->fld_value."</availability>\n");
						}
					}
					mysql_free_result ($result2);
					$strdbsql = "SELECT * FROM tbl_residentialLetRentFrequency WHERE fld_counter = ".$resultdata->fld_resRentFrequency;
					$result2 = mysql_query ($strdbsql,$strdb);
					if (mysql_num_rows($result2) != 0)
					{
						while($resultdata2 = mysql_fetch_object($result2)) {
							$strrentfreq = $resultdata2->fld_value;
						}
					}
					mysql_free_result ($result2);
					if($resultdata->fld_resToLetPOA == 1) print("\t<price>POA</price>\n");
					else print(utf8_encode("\t<price><![CDATA[�".number_format($resultdata->fld_rent,0)." $strrentfreq]]></price>\n"));
				}

			}				





                        $strdbsql = "SELECT * FROM tbl_propertyImages WHERE fld_propertyID = ".$resultdata->fld_ID." ORDER BY fld_counter ";
			$result2 = mysql_query ($strdbsql,$strdb);
			if (mysql_num_rows($result2) != 0)
			{
				print("\t<images>\n");
				$intcounter = 1;
				while($resultdata2 = mysql_fetch_object($result2)) {
					print("\t\t<image".$intcounter.">".$strsiteurl.$strimagedir.$resultdata2->fld_image."</image".$intcounter.">\n");
					$intcounter = $intcounter + 1;
				}
				print("\t</images>\n");
			}
			else
			{
				print("\t<images>\n");
					print("\t\t<image1>".$strsiteurl.$strimagedir."placeholder.jpg</image1>\n");
				print("\t</images>\n");
			}
			mysql_free_result ($result2);
			$strdbsql = "SELECT * FROM tbl_propertyFloorplans WHERE fld_propertyID = ".$resultdata->fld_ID;
			$result2 = mysql_query ($strdbsql,$strdb);
			if (mysql_num_rows($result2) != 0)
			{
				print("\t<floorplans>\n");
				while($resultdata2 = mysql_fetch_object($result2)) {
					print("\t\t<floorplan>".$strsiteurl.$strplandir.$resultdata2->fld_image."</floorplan>\n");
				}
				print("\t</floorplans>\n");
			}
			else
			{
				print("\t<floorplans></floorplans>\n");
			}
			mysql_free_result ($result2);
		//	$strdbsql = "SELECT * FROM tbl_propertyBrochures WHERE fld_propertyID = ".$resultdata->fld_ID;
		//	$result2 = mysql_query ($strdbsql,$strdb);
		//	if (mysql_num_rows($result2) != 0)
		//	{
		//		print("\t<brochures>\n");
		//		while($resultdata2 = mysql_fetch_object($result2)) {
		//			print("\t\t<brochure>".$strsiteurl.$strbrochuredir.$resultdata2->fld_link."</brochure>\n");
		//		}
		//		print("\t</brochures>\n");
		//	}
		//	else
		//	{
		//		print("\t<brochures></brochures>\n");
		//	}
		//	mysql_free_result ($result2);
		//	$strdbsql = "SELECT * FROM tbl_propertyVirtualTours WHERE fld_propertyID = ".$resultdata->fld_ID;
		//	$result2 = mysql_query ($strdbsql,$strdb);
		//	if (mysql_num_rows($result2) != 0)
		//	{
		//		print("\t<virtualTours>\n");
		//		while($resultdata2 = mysql_fetch_object($result2)) {
		//			if(strstr($resultdata2->fld_link, "http://")) print("\t\t<virtualTour>".$resultdata2->fld_link."</virtualTour>\n");
		//			else print("\t\t<virtualTour>".$strsiteurl.$strtourdir.$resultdata2->fld_link."</virtualTour>\n");
		//		}
		//		print("\t</virtualTours>\n");
		//	}
		//	else
		//	{
		//		print("\t<virtualTours></virtualTours>\n");
		//	}
		//	mysql_free_result ($result2);
			$strdbsql = "SELECT * FROM tbl_propertyEPC WHERE fld_propertyID = ".$resultdata->fld_ID;
			$result2 = mysql_query ($strdbsql,$strdb);
			if (mysql_num_rows($result2) != 0)
			{
				print("\t<epcGraphs>\n");
				while($resultdata2 = mysql_fetch_object($result2)) {
					print("\t\t<epcGraph>".$strsiteurl.$strepcdir.$resultdata2->fld_image."</epcGraph>\n");
				}
				print("\t</epcGraphs>\n");
			}
			else
			{
				print("\t<epcGraphs></epcGraphs>\n");
			}
			mysql_free_result ($result2);
			print("</property>\n");
			print("\n");			
		}
}
else
{
		while($resultdata = mysql_fetch_object($result)) {

                  mysql_query("insert into tbl_stats(fld_propertyID, fld_ip, fld_agent, fld_lang, fld_ref, fld_time, fld_type) values('".$resultdata->fld_ID."','".mysql_real_escape_string($_SERVER['REMOTE_ADDR'])."', '".mysql_real_escape_string($_SERVER['HTTP_USER_AGENT'])."', '".mysql_real_escape_string($_SERVER['HTTP_ACCEPT_LANGUAGE'])."', '".mysql_real_escape_string($url)."', ".mktime().", 'search')");

			print(utf8_encode ("<property>\n"));
			print(utf8_encode ("\t<propertyID>".$resultdata->fld_ID."</propertyID>\n"));
			if(trim($resultdata->fld_displayAddress) != "") print(utf8_encode ("\t<displayAddress>".$resultdata->fld_displayAddress."</displayAddress>\n"));
			else print(utf8_encode ("\t<displayAddress>".trim($resultdata->fld_addressName." ".$resultdata->fld_addressNumber." ".$resultdata->fld_addressStreet)."</displayAddress>\n"));
			if($resultdata->fld_lat != 0 || $resultdata->fld_lng != 0)
			{
                          print(utf8_encode ("\t<location>\n"));
                            print(utf8_encode ("\t\t<lat>".$resultdata->fld_lat."</lat>\n"));
                            print(utf8_encode ("\t\t<lng>".$resultdata->fld_lng."</lng>\n"));
                            if(isset($resultdata->fld_distance) && $resultdata->fld_distance != "") print(utf8_encode ("\t\t<distance>".number_format($resultdata->fld_distance,2)." miles</distance>\n"));
                          print(utf8_encode ("\t</location>\n"));
                        }

			if(strtolower($resultdata->fld_department) == "commercial")
			{
				if($resultdata->fld_forSale == 1)
				{
				if($resultdata->fld_commForSalePOA == 1) print(utf8_encode ("\t<price>POA</price>\n"));
				else if($resultdata->fld_priceFrom != 0) print(utf8_encode ("\t<price><![CDATA[�".number_format($resultdata->fld_priceFrom,0)."-�".number_format($resultdata->fld_priceTo,0)."]]></price>\n"));
				else print(utf8_encode ("\t<price><![CDATA[�".number_format($resultdata->fld_priceTo,0)."]]></price>\n"));
				}
				else if ($resultdata->fld_toLet == 1)
				{
				if($resultdata->fld_commToLetPOA == 1) print(utf8_encode ("\t<price>POA</price>\n"));
				else if($resultdata->fld_rentFrom != 0) print(utf8_encode ("\t<price><![CDATA[�".number_format($resultdata->fld_rentFrom,0)."-�".number_format($resultdata->fld_rentTo,0)." ".$resultdata->fld_commRentFrequency."]]></price>\n"));
				else print(utf8_encode ("\t<price><![CDATA[�".number_format($resultdata->fld_rentTo,0)." ".$resultdata->fld_commRentFrequency."]]></price>\n"));
				}
				$strdbsql = "SELECT * FROM tbl_commAvailability WHERE fld_counter = ".$resultdata->fld_commAvailability;
				$result2 = mysql_query ($strdbsql,$strdb);
				if (mysql_num_rows($result2) != 0)
				{
					while($resultdata2 = mysql_fetch_object($result2)) {
						print(utf8_encode ("\t<availability>".$resultdata2->fld_value."</availability>\n"));
					}
				}
				mysql_free_result ($result2);
				if($resultdata->fld_strapLine != "") print(utf8_encode (str_replace("&pound;", "�","\t<displayPropertyType><![CDATA[".$resultdata->fld_strapLine."]]></displayPropertyType>\n")));
				else print(utf8_encode ("\t<displayPropertyType></displayPropertyType>\n"));
			}
			else
			{
				$strdbsql = "SELECT * FROM tbl_residentialPropertyStyle WHERE fld_counter = ".$resultdata->fld_propertyStyle;
				$result2 = mysql_query ($strdbsql,$strdb);
				if (mysql_num_rows($result2) != 0)
				{
					while($resultdata2 = mysql_fetch_object($result2)) {
						$strproptype = $resultdata2->fld_value;
					}
				}
				mysql_free_result ($result2);
				if(trim($resultdata->fld_displayPropertyType) != "") print(utf8_encode ("\t<displayPropertyType>".$resultdata->fld_displayPropertyType."</displayPropertyType>\n"));
				else  print(utf8_encode ("\t<displayPropertyType>".$strproptype."</displayPropertyType>\n"));
				if(strtolower($resultdata->fld_department) == "sales")
				{
					$strdbsql = "SELECT * FROM tbl_residentialSaleAvailability WHERE fld_counter = ".$resultdata->fld_saleAvailability;
					$result2 = mysql_query ($strdbsql,$strdb);
					if (mysql_num_rows($result2) != 0)
					{
						while($resultdata2 = mysql_fetch_object($result2)) {
							print(utf8_encode ("\t<availability>".$resultdata2->fld_value."</availability>\n"));
						}
					}
					mysql_free_result ($result2);
					if($resultdata->fld_resForSalePOA == 1) print(utf8_encode ("\t<price>POA</price>\n"));
					else print(utf8_encode ("\t<price><![CDATA[�".number_format($resultdata->fld_price,0)."]]></price>\n"));
					$strdbsql = "SELECT * FROM tbl_residentialSalesPriceQualifier WHERE fld_counter = ".$resultdata->fld_priceQualifier;
					$result2 = mysql_query ($strdbsql,$strdb);
					if (mysql_num_rows($result2) != 0)
					{
						while($resultdata2 = mysql_fetch_object($result2)) {
							print(utf8_encode ("\t<priceQualifier>".$resultdata2->fld_value."</priceQualifier>\n"));
						}
					}
					mysql_free_result ($result2);
				}
				else if(strtolower($resultdata->fld_department) == "lettings")
				{
					$strdbsql = "SELECT * FROM tbl_residentialLetAvailability WHERE fld_counter = ".$resultdata->fld_letAvailability;
					$result2 = mysql_query ($strdbsql,$strdb);
					if (mysql_num_rows($result2) != 0)
					{
						while($resultdata2 = mysql_fetch_object($result2)) {
							print(utf8_encode ("\t<availability>".$resultdata2->fld_value."</availability>\n"));
						}
					}
					mysql_free_result ($result2);
					$strdbsql = "SELECT * FROM tbl_residentialLetRentFrequency WHERE fld_counter = ".$resultdata->fld_resRentFrequency;
					$result2 = mysql_query ($strdbsql,$strdb);
					if (mysql_num_rows($result2) != 0)
					{
						while($resultdata2 = mysql_fetch_object($result2)) {
							$strrentfreq = $resultdata2->fld_value;
						}
					}
					mysql_free_result ($result2);
					if($resultdata->fld_resToLetPOA == 1) print(utf8_encode ("\t<price>POA</price>\n"));
					else print(utf8_encode ("\t<price><![CDATA[�".number_format($resultdata->fld_rent,0)." $strrentfreq]]></price>\n"));
				}

			}				


			$strdbsql = "SELECT * FROM tbl_propertyImages WHERE fld_propertyID = ".$resultdata->fld_ID." ORDER BY fld_counter LIMIT 1";
			$result2 = mysql_query ($strdbsql,$strdb);
			if (mysql_num_rows($result2) != 0)
			{
				print(utf8_encode ("\t<images>\n"));
				while($resultdata2 = mysql_fetch_object($result2)) {
					print(utf8_encode ("\t\t<image>".$strsiteurl.$strthumbdir.$resultdata2->fld_image."</image>\n"));
				}
				print(utf8_encode ("\t</images>\n"));
			}
			else
			{
				print(utf8_encode ("\t<images>\n"));
					print(utf8_encode ("\t\t<image".$intcounter.">".$strsiteurl.$strthumbdir."placeholder.jpg</image".$intcounter.">\n"));
				print(utf8_encode ("\t</images>\n"));
			}
			mysql_free_result ($result2);



			print(utf8_encode ("</property>\n"));
			print(utf8_encode ("\n"));
		}
}
	}

	print(utf8_encode ("</properties>\n"));
	mysql_free_result ($result);
	mysql_close ($strdb);

?>

