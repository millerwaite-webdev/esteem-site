<?php
/************************************************************/
/* Created By: Rhian singleton                              */
/* Email: rhian@millerwaite.com                             */
/*                                                          */
/* For: Esteem Homes                             */
/************************************************************/
/* READS IN JUPIX PROPERTY XML FEED DURING OVERNIGHT WINDOW */
/* AND STORES IN A LOCAL DATABASE FOR LATER RETRIVAL BY     */
/* PHONE USERS DURING THE DAY                               */
/************************************************************/
/* VERSION : 1.0                                            */
/* DATE    : 13/05/2010                                     */
/************************************************************/

include("/var/www/vhosts/esteemhomes.co.uk/httpdocs/includes/incsitecommon.php");

require '/var/www/vhosts/esteemhomes.co.uk/httpdocs/admin/vendor/autoload.php';

use YDD\Vebra\API as VebraAPI;
use YDD\Vebra\TokenStorage\File as TokenStorageFile;
use Buzz\Client\Curl as BuzzClientCurl;
use Buzz\Message\Factory\Factory as BuzzMessageFactory;


$tokenStorage = new \YDD\Vebra\TokenStorage\File('esteemhomesun', __DIR__.'/tokens/');


$api = new VebraAPI(
    'ESTEEMHOMEAPI',
    'esteemhomesun',
    'iGvvF2318#sqW7',
    new TokenStorageFile('esteemhomesun', __DIR__.'/tokens/'),
    new BuzzClientCurl(),
    new BuzzMessageFactory()
);


error_reporting(E_ALL);
ini_set('display_errors', '1');
set_time_limit(0);

//$strdb = mysql_connect($strserver,$strdbuser,$strdbpass);
//mysql_select_db($strdbname,$strdb);
$conn = connect(); // Open Connection to Database


	session_start();
	
	/*DATAFEEDID: ESTEEMHOMEAPI
	USERNAME: esteemhomesun
	PASSWORD: iGvvF2318#sqW7

	Firm ID :   81573  Branch ID : 1*/


	//Define Your Unique Variable Here:
	/*$username = "esteemhomesun";
	$password = "iGvvF2318#sqW7";
	$datafeedID = "ESTEEMHOMEAPI";
	$request = "http://webservices.vebra.com/export/$datafeedID/v10/branch";
	//$request = "http://webservices.vebra.com/export/ESTEEMHOMEAPI/v10/branch/37685";
	$request = "http://webservices.vebra.com/export/ESTEEMHOMEAPI/v10/branch/37685/property";
	$request = "http://webservices.vebra.com/export/ESTEEMHOMEAPI/v10/branch/37685/property/30533661";
	*/
	
	
	
	$branchSummaries = $api->getBranches();


	foreach ($branchSummaries as $branchSummary) {
		$branch = $api->getBranch($branchSummary->getClientId());
		
		//echo $branch->getName();
	}

	$i = 1;
	
	
	switch($_REQUEST['updateType']){
		case "all":
			$propertySummaries = $api->getPropertyList($branch->getClientId());
		break;
		default:
			
			
		
			$propertySummaries = $api->getChangedProperties(new \DateTime(date('Y-m-d',strtotime("-1 days"))));
		break;
	}
		
		
	
	
	//$propertySummaries = $api->getPropertyList($branch->getClientId());
	foreach ($propertySummaries as $propertySummary) {
		$property = $api->getProperty($branch->getClientId(), $propertySummary->getPropId());
		
		
		$address = $property->getAddress();
		$price = $property->getPrice();
		$bullets = $property->getBullets();
		$files = $property->getFiles();
		$attributes = $property->getAttributes();
		
		
		print ("<br/>");
		//print ("<pre>");
		//print_r($attributes);
		print ("<br/>");
		print ("<br/>");
		print ("<br/>");
		print "<br/>PropertyID: ".$attributes["propertyid"];
		print "<br/>Name: ".$address->getName();
		print "<br/>Street: ".$address->getStreet();
		print "<br/>Town: ".$address->getTown();
		print "<br/>County: ".$address->getCounty();
		print "<br/>Postcode: ".$address->getPostcode();
		print "<br/>Price: ".$price->getValue();
		
		print "<br/>Type: ".$property->getType();
		print "<br/>Bedrooms: ".$property->getBedrooms();
		print "<br/>Receptions: ".$property->getReceptions();
		print "<br/>Bathroom: ".$property->getBathrooms();
		print "<br/>Longitude: ".$property->getLongitude();
		print "<br/>Latitude: ".$property->getLatitude();
		print "<br/>Description: ".$property->getDescription();
		print "<br/>userField1: ".$property->getUserField1();
		print "<br/>userField2: ".$property->getUserField2();
		
		switch($property->getUserField1()){
			case "properties-thorncliffe-south":
				$property_location_id = 1;
			break;
			case "properties-ratings-village":
				$property_location_id = 2;
			break;
			case "properties-retirement-apartments":
				$property_location_id = 3;
			break;
			case "properties-park-view":
				$property_location_id = 4;
			break;
			case "properties-abbotsmead":
				$property_location_id = 5;
			break;
			case "properties-bevan-house":
				$property_location_id = 6;
			break;
			case "properties-boarshaw-clough":
				$property_location_id = 7;
			break;
			case "properties-scotland":
				$property_location_id = 8;
			break;
			case "properties-wales":
				$property_location_id = 9;
			break;
			case "partexchangeprop":
				$property_location_id = 10;
			break;
			case "renttobuyprop":
				$property_location_id = 11;
			break;
			case "properties-rock-lea":
				$property_location_id = 12;
			break;
			default: 
				$property_location_id = 1;
			break;
		}
		
		
		
		foreach($bullets AS $bullet){
			
			print "<br/>- ".$bullet->getValue();
		}
		
		foreach($files AS $file){
			
			print "<br/>- ".$file->getName()."(".$file->getUrl().")";
		}
		
		print ("<br/>");
		

		$strdbsql = "DELETE FROM properties WHERE rs_id = :rs_id";
		$deleteProp = query($conn, $strdbsql, "delete", array("rs_id"=>$attributes["propertyid"]));
		
		$strdbsql = "DELETE FROM property_images WHERE rs_id = :rs_id";
		$deletePropImages = query($conn, $strdbsql, "delete", array("rs_id"=>$attributes["propertyid"]));
		
		$status = "";
		
		$webStatus = $property->getWebStatus();
		switch($webStatus){
				case 1: $status = ""; break; //under offer
				case 3: $status = "SSTC"; break; //SSTC
				case 2: $status = "Sold"; break; //Sold
				default: $status = ""; break; //for sale
				
		}
		
		// Get params for page create/update
		$arrdbparams = array(
			"egnID" => $attributes["propertyid"],
			"rs_id" => $attributes["propertyid"],
			"propname" => $address->getName(),
			"propaddress" => $address->getStreet()." ".$address->getTown()." ".$address->getCounty(),
			"proppostcode" => $address->getPostcode(),
			"proptype" => $property->getType(),
			"propprice" => $price->getValue(),			
			"proptenure" => "",
			"propbeds" => $property->getBedrooms(),
			"propbaths" => $property->getBathrooms(),
			"metatitle" => $address->getName()." ".$address->getDisplay(),
			"metadescription" => $property->getDescription(),
			"propshortdesc" => $property->getDescription(),
			"proplongdesc" => $property->getDescription(),


			"property_location_id" => $property_location_id, //need a way to get this!
			"displayOrder" => $i,
			"visible" => "yes",
			"status" => $status
		);
		
		$strdbsql = "INSERT INTO properties (egnID, rs_id, p_name, p_area, postcode, prop_type, prop_price, prop_tenure, No_beds, No_baths, metatitle, metadescription, short_description, long_description, make_live, sold, property_location_id, displayOrder) VALUES (:egnID, :rs_id, :propname, :propaddress, :proppostcode, :proptype, :propprice, :proptenure, :propbeds, :propbaths, :metatitle, :metadescription, :propshortdesc, :proplongdesc, :visible, :status, :property_location_id, :displayOrder)";
		$strType = "insert";			
		$updatePage = query($conn, $strdbsql, $strType, $arrdbparams);
		
		
		
		// Set images order
		$j = 1;
		foreach($files AS $file){
			
			//print "<br/>- ".$file->getName()."(".$file->getUrl().")";
			
			$ext = pathinfo($file->getUrl(), PATHINFO_EXTENSION);
			if($ext == "pdf"){
				if(strpos($file->getUrl(), "Brochure")){
				
					$arrdbparams = array(
						"rs_id" => $attributes["propertyid"],
						"brochure" => $file->getUrl()
					);
				
					$strdbsql = "UPDATE properties SET brochure = :brochure WHERE rs_id = :rs_id";
					$strType = "update";			
					$updatePage = query($conn, $strdbsql, $strType, $arrdbparams);
				
				}
			} else {
			
				$arrdbparams = array(
					"egnID" => $attributes["propertyid"],
					"rs_id" => $attributes["propertyid"],
					"image_order" => $j,
					"make_live" => "Yes",
					"image_title" => $file->getName(),
					"image_description" => $file->getName(),
					"url" => $file->getUrl()
				);
				
				
				$strdbsql = "INSERT INTO property_images (egnID, rs_id, image_order, make_live, image_title, image_description, url) VALUES (:egnID, :rs_id, :image_order, :make_live, :image_title, :image_description, :url)";
				$result = query($conn,$strdbsql,"insert",$arrdbparams);
			}
			
			$j++;
			
		}

		$i++;
		//print ("</pre>");
	}
	
	print ("<br/>");
	print ("<br/>");
	print ("<br/>");
	print ("--------------------------------------------------------------------------------------");
	print ("<br/>");
	print ("<br/>");
	print ("<br/>");

	$changedProperties = $api->getChangedProperties(new \DateTime('2012-01-01'));
	foreach ($changedProperties as $propertySummary) {
		
		$property = $api->getProperty($branch->getClientId(), $propertySummary->getPropId());
		
		print ("<br/>");
		print ("<pre>");
		print_r($property);
		print ("</pre>");
		break;
	}
	

	//$files = $api->getChangedFiles(new \DateTime('2012-01-01'));
	//print_r($properties);
	


//mysql_close ($strdb);


?>