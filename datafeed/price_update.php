<?php
/************************************************************/
/* Created By: Rhian singleton                              */
/* Email: rhian@millerwaite.com                             */
/*                                                          */
/* For: Ross Estate Agencies                                */
/************************************************************/
/* READS IN JUPIX PROPERTY XML FEED DURING OVERNIGHT WINDOW */
/* AND STORES IN A LOCAL DATABASE FOR LATER RETRIVAL BY     */
/* PHONE USERS DURING THE DAY                               */
/************************************************************/
/* VERSION : 1.0                                            */
/* DATE    : 13/05/2010                                     */
/************************************************************/

include("/var/www/vhosts/esteemhomes.co.uk/httpdocs/datafeed/incsitecommon.php");

error_reporting(E_ALL);
ini_set('display_errors', '1');
//set_time_limit(0);


$strdb = mysql_connect($strserver,$strdbuser,$strdbpass);
mysql_select_db($strdbname,$strdb);

	//------------------------------------------------------------------------------------------------------------//
	//set variables
	//------------------------------------------------------------------------------------------------------------//

	//file locations for feeds
	//$strfeedlocation = "http://services.jupix.co.uk/api/get_properties.php?clientID=8be61c32f094edb28b80c02e173e4934&passphrase=GHvYG9f&dateFrom=2009-01-01&limit=1"; //Limited Jupix feed
	//$strfeedlocation = "http://services.jupix.co.uk/api/get_properties.php?clientID=8be61c32f094edb28b80c02e173e4934&passphrase=GHvYG9f&dateFrom=2009-01-01"; //Full Jupix feed
	//$strfeedlocation = "http://services.jupix.co.uk/api/get_properties.php?clientID=8be61c32f094edb28b80c02e173e4934&passphrase=GHvYG9f"; //Nightly Jupix feed


	session_start();

	//Define Your Unique Variable Here:
	$username = "esteemhomesun";
	$password = "iGvvF2318#sqW7";
	$datafeedID = "ESTEEMHOMEAPI";
	$request = "http://webservices.vebra.com/export/$datafeedID/v10/branch";

	//Function to authenticate self to API and return/store the Token
	function getToken($url) {

		//Re-define username and password variable scope so they can be used within the function
		global $username, $password;
		
		//Overwiting the response headers from each attempt in this file (for information only)
		$file = "headers.txt";
		$fh = fopen($file, "w");
		
		//Start curl session
		$ch = curl_init($url);
		//Define Basic HTTP Authentication method
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		//Provide Username and Password Details
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		//Show headers in returned data but not body as we are only using this curl session to aquire and store the token
		curl_setopt($ch, CURLOPT_HEADER, 1); 
		curl_setopt($ch, CURLOPT_NOBODY, 1); 
		//write the output (returned headers) to file
		curl_setopt($ch, CURLOPT_FILE, $fh);
		//execute curl session
		curl_exec($ch);
		// close curl session
		curl_close($ch); 
		//close headers.txt file
		fclose($fh); 

		//read each line of the returned headers back into an array
		$headers = file('headers.txt', FILE_SKIP_EMPTY_LINES);
		
		//for each line of the array explode the line by ':' (Seperating the header name from its value)
		foreach ($headers as $headerLine) {

			$line = explode(':', $headerLine);
			$header = $line[0];
			$value = trim($line[1]);
			
			//If the request is successful and we are returned a token
			if($header == "Token") {
					//save token start and expire time (roughly)
					$tokenStart = time(); 
					$tokenExpire = $tokenStart + 60*60; 
					//save the token in a session variable (base 64 encoded)
					$_SESSION['token'] = base64_encode($value); 
					
					//For now write this new token, its start and expiry datetime into a .txt (appending not overwriting - this is for reference in case you loose your session data)
					$file = "tokens.txt";
					$fh = fopen($file, "a+");
					//write the line in
					$newLine = "'".$_SESSION['token']."','".date('d/m/Y H:i:s', $tokenStart)."','".date('d/m/Y H:i:s', $tokenExpire)."'"."\n";
					fwrite($fh, $newLine);
					//Close file
					fclose($fh);
				}
				
		}
		
		//If we have been given a token request XML from the API authenticating using the token
		if (!empty($_SESSION['token'])) {
			connect($url);
		} else {
			//If we have not been given a new token its because we already have a live token which has not expired yet (check the tokens.txt file)
			echo '<br />There is still an active Token, you must wait for this token to expire before a new one can be requested!<br />';
		}
	}

	//Function to connect to the API authenticating ourself with the token we have been given
	function connect($url) {

		//If token is not set skip to else condition to request a new token 
		if(!empty($_SESSION['token'])) {
			
			//Set a new file name and create a new file handle for our returned XML
			$file = "feed_".substr($strdate,0,10).".xml";
			$fh = fopen($file, "w");
			
			//Initiate a new curl session
			$ch = curl_init($url);
			//Don't require header this time as curl_getinfo will tell us if we get HTTP 200 or 401
			curl_setopt($ch, CURLOPT_HEADER, 0); 
			//Provide Token in header
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$_SESSION['token']));
			//Write returned XML to file
			curl_setopt($ch, CURLOPT_FILE, $fh);
			//Execute the curl session
			curl_exec($ch);
			
			//Store the curl session info/returned headers into the $info array
			$info = curl_getinfo($ch);
			
			//Check if we have been authorised or not
			if($info['http_code'] == '401') {
				getToken($url);
				echo 'Token Failed - getToken() has been run!<br />';
			} elseif ($info['http_code'] == '200') {
				echo 'Token Worked - Success';
			}
			
			//Close the curl session
			curl_close($ch);
			//Close the open file handle
			fclose($fh);
			
		} else {
		
			//Run the getToken function above if we are not authenticated
			getToken($url);
			
		}
		
	}

	//Connect to the API using connect() function above
	connect($request);


	//locate file name for feed - use feed dir defined in incsitecommon
	$strjpxbkpname = "feed_backup_".substr($strdate,0,10).".xml"; //save local copy
	$strjpxbkpnameold = "feed_backup_".substr($strdatelastweek,0,10).".xml"; //save local copy
//	$strfeedlocation = "/var/www/vhosts/esteemhomes.co.uk/httpdocs/datafeed/feeds/feed_backup_2018-11-26.xml"; //retry from backup

	$strfeedlocation = $file;


	//set header for update report
	$strlogheader = " Esteem Homes Data Update Log \r\n";
	$strlogheader .= " Jupix XML Feed to Miller Waite Database\r\n\r\n";
	$strlogheader .= " -----------------------------\r\n";
	$strlogheader .= " ".$strdate."\r\n";
	$strlogheader .= " -----------------------------\r\n\r\n";

	//used to store data from Jupix
	$text = "";
	$arrproperties = array ();  //used to list XML properties
	$arrimages = array (); //used to list updates to propertyImage table
	$arrfloorplans = array (); //used to list updates to propertyFloorplan table
	$arrepcgraphs = array ();
	$arrbrochures = array ();
	$arrvirtualtours = array ();
	$arrcommproptypes = array ();

	//used to store properties from database for comparison
	$arrdbprops = array();

	//arrays for categoriesing properties
	$arrdelete = array();
	$arradd = array();
	$arrupdate = array();
	$arrunchanged = array();

	//used in XML parsering of Jupix property feed
	$intcounter = 0;
	$intpropertyID = null;
	$strtagname = "";
	$strselltype = "";
	$character_data_on = false;
	$tag_complete = true;

	//used to store addresses for google location feed
	$arraddresses = array ();
	$strggladdress = "";
	$intgglpropertyID = null;

	//used to store data from google location feed
	$arrgglreport = array ();
	$location = "";
	
	//use google geolocate api
	include("/var/www/vhosts/esteemhomes.co.uk/httpdocs/datafeed/geolocate.php");


	//------------------------------------------------------------------------------------------------------------//
	//define functions
	//------------------------------------------------------------------------------------------------------------//

	//file transfer and edit functions
	function make_thumb($img_name,$filename,$new_w,$new_h)
	{
		$ext=getExtension($img_name);
		$newextn = $ext;
		$r = 255;
		$g = 255;
		$b = 255;
		if(!strcmp("jpg",$ext) || !strcmp("jpeg",$ext)) $src_img=imagecreatefromjpeg($img_name);
		if(!strcmp("png",$ext)) $src_img=imagecreatefrompng($img_name);
		if(!strcmp("gif",$ext)) $src_img=imagecreatefromgif($img_name);

		$old_x=imageSX($src_img);
		$old_y=imageSY($src_img);

		$ratio1=$old_x/$new_w;
		$ratio2=$old_y/$new_h;

		if($ratio1>$ratio2)
		{
			$thumb_w=$new_w;
			$thumb_h=$old_y/$ratio1;
		}
		else
		{
			$thumb_h=$new_h;
			$thumb_w=$old_x/$ratio2;
		}

		$dst_img=ImageCreateTrueColor($new_w,$new_h);
		$starty = ($new_h - $thumb_h)/2;
		$startx = ($new_w - $thumb_w)/2;
		imagefill ( $dst_img , 0 , 0 , imagecolorallocate ( $dst_img , $r, $g , $b ) );

		imagecopyresampled($dst_img,$src_img,$startx,$starty,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

		if(!strcmp("png",$newextn)) imagepng($dst_img,$filename);
		else if(!strcmp("jpg",$newextn) || !strcmp("jpeg",$newextn)) imagejpeg($dst_img,$filename);
		else if(!strcmp("gif",$newextn)) imagegif($dst_img,$filename);
		if(substr(sprintf('%o', fileperms($filename)), -4) != "0777") chmod($filename, 0777);
  
		imagedestroy($dst_img);
		imagedestroy($src_img);
	}

	function copyFileFromURL($fileUrl, $savepath)
	{
		if(url_exists($fileUrl))
		{
			$in = fopen($fileUrl, 'r');
			if ($in == FALSE) {return $in;}
			else {
				$strupfiledirs = explode("/", $fileUrl);
				$strfilename = $strupfiledirs[count($strupfiledirs)-1];
				$stroutfilepath = $savepath.$strfilename;
				$out = fopen($stroutfilepath, 'w');
				if ($out == FALSE) {return $out;}
				else {

					while (!feof($in)) {
						$buffer = fread($in, 2048);
						fwrite($out, $buffer);
					}
					fclose($out);
					if(substr(sprintf('%o', fileperms($stroutfilepath)), -4) != "0777") chmod($stroutfilepath, 0777);
					return $strfilename;
				}
			}
		}
		else return FALSE;
	}

	//define JUPIX XML Processing functions
	function jpx_start_tag($parser, $name, $attribs) {
		global $arrproperties;
		global $intcounter;
		global $strtagname;
		global $character_data_on;
		global $tag_complete;

		// set prop array counter and store tag names locally a required
		if ($name == "PROPERTY")
		{
			$intcounter = $intcounter + 1;
			$arrproperties[$intcounter] = array();
		}
		else if ($name != "PROPERTIES")
		{
			$strtagname = $name;
		}

		$tag_complete = false;
		$character_data_on = false;
	}

	function jpx_end_tag($parser, $name) {
		global $strtagname;
		global $strselltype;
		global $arrproperties;
		global $intcounter;
		global $intpropertyID;
		global $character_data_on;
		global $tag_complete;

		//deal with self closed tags
		if (!$character_data_on) {
			// Process as self closed tag
			if($name != "PROPERTY" && $name != "PROPERTIES" && $name != "")  $arrproperties[$intcounter][$name] = ""; //store appropriate empty tags to avoid php warnings when trying to access later
			$tag_complete = true;
		}
		$character_data_on = false;

		//clear any tag names and prop ids as required
		$strtagname = "";
		if($name == "PROPERTY") {
			$intpropertyID = null;
			$strselltype = "";
		}
	}

	function jpx_tag_contents($parser, $data) {
		global $arrproperties;
		global $arrimages;
		global $arrfloorplans;
		global $arrepcgraphs;
	//	global $arrbrochures;
	//	global $arrvirtualtours;
		global $arrcommproptypes;
		global $intcounter;
		global $intpropertyID;
		global $strtagname;
		global $strselltype;
		global $character_data_on;
		global $tag_complete;

		//make clear this is not a self-closed tag
		if ((!$character_data_on)&&(!$tag_complete)) {
			$tag_complete = true;
		}
		$character_data_on = true;

		//store property ID locally
		if($strtagname == "PROPERTYID") $intpropertyID = $data;
		if($strtagname == "DEPARTMENT") $strselltype = $data;
		//store tag contains in appropriate array with appropriate keys
		if($strtagname == "IMAGES") $arrproperties[$intcounter][$strtagname] = "UPDATE";
		else if($strtagname == "FLOORPLANS") $arrproperties[$intcounter][$strtagname] = "UPDATE";
		else if($strtagname == "EPCGRAPHS") $arrproperties[$intcounter][$strtagname] = "UPDATE";
		else if($strtagname == "BROCHURES") $arrproperties[$intcounter][$strtagname] = "UPDATE";
		else if($strtagname == "VIRTUALTOURS") $arrproperties[$intcounter][$strtagname] = "UPDATE";
		else if($strtagname == "IMAGE") $arrimages[] = array("propID" => $intpropertyID, "image" => $data);
		else if($strtagname == "FLOORPLAN") $arrfloorplans[] = array("propID" => $intpropertyID, "image" => $data);
		else if($strtagname == "EPCGRAPH") $arrepcgraphs[] = array("propID" => $intpropertyID, "link" => $data);
		else if($strtagname == "BROCHURE") $arrbrochures[] = array("propID" => $intpropertyID, "link" => $data);
		else if($strtagname == "VIRTUALTOUR") $arrvirtualtours[] = array("propID" => $intpropertyID, "link" => $data);
		else if($strselltype == "Commercial" && $strtagname == "PROPERTYTYPE") $arrcommproptypes[] = array("propID" => $intpropertyID, "type" => $data);
		else if($strtagname != "" && trim($data) != "") {
			if(!isset($arrproperties[$intcounter][$strtagname])) $arrproperties[$intcounter][$strtagname] = MYSQL_REAL_ESCAPE_STRING(str_replace("�", "?",str_replace("�", "'",str_replace("�","\"",$data)))); //store database safe contents
			else $arrproperties[$intcounter][$strtagname] .= MYSQL_REAL_ESCAPE_STRING(str_replace("�", "?",str_replace("�", "'",str_replace("�","\"",$data)))); //append additional data in database safe contents
		}

	}

	//helper functions
	function getExtension($str) {
		$i = strrpos($str,".");
		if (!$i) { return ""; }
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}

	function url_exists($url) {
		$hdrs = @get_headers($url);
		return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false;
	}

	//------------------------------------------------------------------------------------------------------------//
	//create XML Parsers
	//------------------------------------------------------------------------------------------------------------//

	//attempt to create XML parsers for jupix
	if (! ($jpxparser = xml_parser_create()) )
	{
		$strmessage = " Cannot create parser.";
	}


	//------------------------------------------------------------------------------------------------------------//
	//retrieve Jupix Feed
	//------------------------------------------------------------------------------------------------------------//

	//attempt to retrieve xml contents and backup a copy locally
	$text = file_get_contents($strfeedlocation);
//	file_put_contents($strfeedbkploc.$strjpxbkpname, $text);
//	if(substr(sprintf('%o', fileperms($strfeedbkploc.$strjpxbkpname)), -4) != "0777") chmod($strfeedbkploc.$strjpxbkpname, 0777);

	//------------------------------------------------------------------------------------------------------------//
	//Process Data
	//------------------------------------------------------------------------------------------------------------//


	if (!isset($strmessage) && $text != ""){

		// set XML Processing functins
		xml_set_element_handler($jpxparser, "jpx_start_tag", "jpx_end_tag");
		xml_set_character_data_handler($jpxparser, "jpx_tag_contents");

		//check if we can process Jupix XML
		if (!xml_parse($jpxparser, $text)) {
			$reason = xml_error_string(xml_get_error_code($jpxparser));
			$reason .= " on line number ".xml_get_current_line_number($jpxparser);
			$strmessage = " Error parsing xml - ".$reason;
		}
		else
		{
			xml_parser_free($jpxparser);
			//for debug only - test jupix imported arrays
			//print_r($arrproperties);
			//print_r($arrimages);
			//print_r($arrfloorplans);
			//print_r($arrbrochures);
			//print_r($arrepcgraphs);
			//print_r($arrvirtualtours);
			//print_r($arrcommproptypes);

			// get array of all db entries and last mod dates.
			$strdbsql = "SELECT fld_propertyID, fld_dateLastModified FROM tbl_property ";
			$result = mysql_query ($strdbsql,$strdb);
			while($resultdata = mysql_fetch_object($result))
			{
				$arrdbprops[$resultdata->fld_propertyID] = $resultdata->fld_dateLastModified;
			}
			mysql_free_result ($result);

			//compare judix imported properties to db stored add id to update categories
			foreach($arrproperties as $property)
			{
				//debug view current property
				//print($property["PROPERTYID"]."<br/>");
				//print_r($arrdbprops);
				//if($property["PROPERTYID"] == 2232) print_r($property);

				//if not in db attempt to add
			/*	if(!isset($arrdbprops[$property["PROPERTYID"]]))
				{
					//add base data to properties table
					$strdbsql = "INSERT INTO tbl_property ";
					$strdbsql .= "(fld_propertyID, fld_branchID, fld_clientName, fld_branchName, fld_department, fld_referenceNumber, fld_addressName, fld_addressNumber, fld_addressStreet, fld_address2, fld_address3, fld_address4, fld_addressPostcode, fld_country, fld_displayAddress, fld_propertyFeature1, fld_propertyFeature2, fld_propertyFeature3, fld_propertyFeature4, fld_propertyFeature5, fld_propertyFeature6, fld_propertyFeature7, fld_propertyFeature8, fld_propertyFeature9, fld_propertyFeature10, fld_dateLastModified, fld_featuredProperty, fld_regionID, fld_mainSummary, fld_fullDescription) VALUES";
					$strdbsql .= "(".$property["PROPERTYID"].", '".$property["BRANCHID"]."', '".$property["CLIENTNAME"]."', '".$property["BRANCHNAME"]."', '".$property["DEPARTMENT"]."', '".$property["REFERENCENUMBER"]."', '".$property["ADDRESSNAME"]."', '".$property["ADDRESSNUMBER"]."', '".$property["ADDRESSSTREET"]."', '".$property["ADDRESS2"]."', '".$property["ADDRESS3"]."', '".$property["ADDRESS4"]."', '".$property["ADDRESSPOSTCODE"]."', '".$property["COUNTRY"]."', '".$property["DISPLAYADDRESS"]."', '".$property["PROPERTYFEATURE1"]."', '".$property["PROPERTYFEATURE2"]."', '".$property["PROPERTYFEATURE3"]."', '".$property["PROPERTYFEATURE4"]."', '".$property["PROPERTYFEATURE5"]."', '".$property["PROPERTYFEATURE6"]."', '".$property["PROPERTYFEATURE7"]."', '".$property["PROPERTYFEATURE8"]."', '".$property["PROPERTYFEATURE9"]."', '".$property["PROPERTYFEATURE10"]."', '".$property["DATELASTMODIFIED"]."', '".$property["FEATUREDPROPERTY"]."', '".$property["REGIONID"]."', '".$property["MAINSUMMARY"]."', '".$property["FULLDESCRIPTION"]."')";
					//print($strdbsql."\r\n");
					$result = mysql_query ($strdbsql,$strdb);


					//add additional data depending on department
					if ($property["DEPARTMENT"] == "Commercial")
					{
						//add comm data to commercial table
						$strdbsql = "INSERT INTO tbl_commercial ";
						$strdbsql .= "(fld_propertyID, fld_forSale, fld_toLet, fld_availability, fld_priceTo, fld_priceFrom, fld_rentTo, fld_rentFrom, fld_rentFrequency, fld_forSalePOA, fld_toLetPOA, fld_floorAreaTo, fld_floorAreaFrom, fld_floorAreaUnits, fld_siteArea, fld_siteAreaUnits, fld_strapLine) VALUES";
						$strdbsql .= "(".$property["PROPERTYID"].", '".$property["FORSALE"]."', '".$property["TOLET"]."', '".$property["AVAILABILITY"]."', '".$property["PRICETO"]."', '".$property["PRICEFROM"]."', '".$property["RENTTO"]."', '".$property["RENTFROM"]."', '".$property["RENTFREQUENCY"]."', '".$property["FORSALEPOA"]."', '".$property["TOLETPOA"]."', '".$property["FLOORAREATO"]."', '".$property["FLOORAREAFROM"]."', '".$property["FLOORAREAUNITS"]."', '".$property["SITEAREA"]."', '".$property["SITEAREAUNITS"]."', '".$property["STRAPLINE"]."')";
						//print($strdbsql."\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);
					}
					else
					{
						// add res data to residential table
						$strdbsql = "INSERT INTO tbl_residential ";
						$strdbsql .= "(fld_propertyID, fld_propertyBedrooms, fld_propertyBathrooms, fld_propertyEnsuites, fld_propertyReceptionRooms, fld_propertyKitchens, fld_propertyAge, fld_displayPropertyType, fld_propertyType, fld_propertyStyle) VALUES";
						$strdbsql .= "(".$property["PROPERTYID"].", '".$property["PROPERTYBEDROOMS"]."', '".$property["PROPERTYBATHROOMS"]."', '".$property["PROPERTYENSUITES"]."', '".$property["PROPERTYRECEPTIONROOMS"]."', '".$property["PROPERTYKITCHENS"]."', '".$property["PROPERTYAGE"]."', '".$property["DISPLAYPROPERTYTYPE"]."', '".$property["PROPERTYTYPE"]."', '".$property["PROPERTYSTYLE"]."')";
						//print($strdbsql."\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);

						if($property["DEPARTMENT"] == "Sales")
						{
							//add sales data to res sales table
							$strdbsql = "INSERT INTO tbl_residentialSales ";
							$strdbsql .= "(fld_propertyID, fld_availability, fld_price, fld_forSalePOA, fld_priceQualifier) VALUES";
							$strdbsql .= "(".$property["PROPERTYID"].", '".$property["AVAILABILITY"]."', '".$property["PRICE"]."', '".$property["FORSALEPOA"]."', '".$property["PRICEQUALIFIER"]."')";
							//print($strdbsql."\r\n");
							$result = (mysql_query ($strdbsql,$strdb) && $result);
						}
						else
						{
							//add letting data to res let table
							$strdbsql = "INSERT INTO tbl_residentialLet ";
							$strdbsql .= "(fld_propertyID, fld_availability, fld_rent, fld_rentFrequency, fld_toLetPOA) VALUES";
							$strdbsql .= "(".$property["PROPERTYID"].", '".$property["AVAILABILITY"]."', '".$property["RENT"]."', '".$property["RENTFREQUENCY"]."', '".$property["TOLETPOA"]."')";
							//print($strdbsql."\r\n");
							$result = (mysql_query ($strdbsql,$strdb) && $result);
						}
					}
					if (isset($property["LATITUDE"]) && isset($property["LONGITUDE"]))
					{
						//insert location from JUPIX
						// add res data to residential table
						$strdbsql = "UPDATE tbl_property ";
						$strdbsql .= "SET fld_lat =  '".$property["LATITUDE"]."', fld_lng = '".$property["LONGITUDE"]."', fld_locationtype = 'JUPIX' ";
						$strdbsql .= "WHERE fld_propertyID = ".$property["PROPERTYID"]." ";
						//print($strdbsql."\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);
						if ($result) $arradd[$property["PROPERTYID"]] = "Property and Location Database Entry Added.";
						else $arradd[$property["PROPERTYID"]] = "Property OR Location Database Entry returned Errors.";
					}
					else {
						//get localtion from google
						$arraddresses[$property["PROPERTYID"]] = array();
						if($property["ADDRESSNAME"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESSNAME"];
						if($property["ADDRESSNUMBER"] != "") {
							if(strpos($property["ADDRESSNUMBER"], "&") === FALSE) $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESSNUMBER"];
							else $arraddresses[$property["PROPERTYID"]][] = substr($property["ADDRESSNUMBER"], 0, strpos($property["ADDRESSNUMBER"], "&"));
						}
						if($property["ADDRESSSTREET"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESSSTREET"];
						if($property["ADDRESS2"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESS2"];
						if($property["ADDRESS3"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESS3"];
						if($property["ADDRESS4"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESS4"];
						if($property["ADDRESSPOSTCODE"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESSPOSTCODE"];
						if($property["COUNTRY"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["COUNTRY"];

						if ($result) $arradd[$property["PROPERTYID"]] = "Property Database Entry Added.";
						else $arradd[$property["PROPERTYID"]] = "Property Database Entry returned Errors.";
					}
				}
				//if  last mod dates do not match attempt update
				else if($arrdbprops[$property["PROPERTYID"]] != $property["DATELASTMODIFIED"])
				{     */
					//update base data in properties table
					$strdbsql = "REPLACE INTO tbl_property ";
					$strdbsql .= "(fld_propertyID, fld_branchID, fld_clientName, fld_branchName, fld_department, fld_referenceNumber, fld_addressName, fld_addressNumber, fld_addressStreet, fld_address2, fld_address3, fld_address4, fld_addressPostcode, fld_country, fld_displayAddress, fld_propertyFeature1, fld_propertyFeature2, fld_propertyFeature3, fld_propertyFeature4, fld_propertyFeature5, fld_propertyFeature6, fld_propertyFeature7, fld_propertyFeature8, fld_propertyFeature9, fld_propertyFeature10, fld_dateLastModified, fld_featuredProperty, fld_regionID, fld_mainSummary, fld_fullDescription) VALUES";
					$strdbsql .= "(".$property["PROPERTYID"].", ".$property["BRANCHID"].", '".$property["CLIENTNAME"]."', '".$property["BRANCHNAME"]."', '".$property["DEPARTMENT"]."', '".$property["REFERENCENUMBER"]."', '".$property["ADDRESSNAME"]."', '".$property["ADDRESSNUMBER"]."', '".$property["ADDRESSSTREET"]."', '".$property["ADDRESS2"]."', '".$property["ADDRESS3"]."', '".$property["ADDRESS4"]."', '".$property["ADDRESSPOSTCODE"]."', '".$property["COUNTRY"]."', '".$property["DISPLAYADDRESS"]."', '".$property["PROPERTYFEATURE1"]."', '".$property["PROPERTYFEATURE2"]."', '".$property["PROPERTYFEATURE3"]."', '".$property["PROPERTYFEATURE4"]."', '".$property["PROPERTYFEATURE5"]."', '".$property["PROPERTYFEATURE6"]."', '".$property["PROPERTYFEATURE7"]."', '".$property["PROPERTYFEATURE8"]."', '".$property["PROPERTYFEATURE9"]."', '".$property["PROPERTYFEATURE10"]."', '".$property["DATELASTMODIFIED"]."', '".$property["FEATUREDPROPERTY"]."', '".$property["REGIONID"]."', '".UTF8_ENCODE($property["MAINSUMMARY"])."', '".$property["FULLDESCRIPTION"]."')";
					//print($strdbsql.";\r\n");
					$result = mysql_query ($strdbsql,$strdb);
				//$result = TRUE;


					if (isset($property["LATITUDE"]) && isset($property["LONGITUDE"]))
					{
						//insert location from JUPIX
						// add res data to residential table
						$strdbsql = "UPDATE tbl_property ";
						$strdbsql .= "SET fld_lat =  '".$property["LATITUDE"]."', fld_lng = '".$property["LONGITUDE"]."', fld_locationtype = 'JUPIX' ";
						$strdbsql .= "WHERE fld_propertyID = ".$property["PROPERTYID"]." ";
						//print($strdbsql."\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);
					}
					else {
						$arraddresses[$property["PROPERTYID"]] = array();
						if($property["ADDRESSNAME"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESSNAME"];
						if($property["ADDRESSNUMBER"] != "") {
							if(strpos($property["ADDRESSNUMBER"], "&") === FALSE) $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESSNUMBER"];
							else $arraddresses[$property["PROPERTYID"]][] = substr($property["ADDRESSNUMBER"], 0, strpos($property["ADDRESSNUMBER"], "&"));
						}
						if($property["ADDRESSSTREET"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESSSTREET"];
						if($property["ADDRESS2"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESS2"];
						if($property["ADDRESS3"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESS3"];
						if($property["ADDRESS4"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESS4"];
						if($property["ADDRESSPOSTCODE"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["ADDRESSPOSTCODE"];
						if($property["COUNTRY"] != "") $arraddresses[$property["PROPERTYID"]][] = $property["COUNTRY"];
					}

					//add additional data depending on department
					if ($property["DEPARTMENT"] == "Commercial")
					{
						//replace comm data in commercial table
						$strdbsql = "REPLACE INTO tbl_commercial ";
						$strdbsql .= "(fld_propertyID, fld_forSale, fld_toLet, fld_availability, fld_priceTo, fld_priceFrom, fld_rentTo, fld_rentFrom, fld_rentFrequency, fld_forSalePOA, fld_toLetPOA, fld_floorAreaTo, fld_floorAreaFrom, fld_floorAreaUnits, fld_siteArea, fld_siteAreaUnits, fld_strapLine) VALUES";
						$strdbsql .= "(".$property["PROPERTYID"].", '".$property["FORSALE"]."', '".$property["TOLET"]."', '".$property["AVAILABILITY"]."', '".$property["PRICETO"]."', '".$property["PRICEFROM"]."', '".$property["RENTTO"]."', '".$property["RENTFROM"]."', '".$property["RENTFREQUENCY"]."', '".$property["FORSALEPOA"]."', '".$property["TOLETPOA"]."', '".$property["FLOORAREATO"]."', '".$property["FLOORAREAFROM"]."', '".$property["FLOORAREAUNITS"]."', '".$property["SITEAREA"]."', '".$property["SITEAREAUNITS"]."', '".$property["STRAPLINE"]."')";
					//	print($strdbsql.";\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);

						//delete any data in res, res sales or res lettings table
						$strdbsql = "DELETE FROM tbl_residential WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql.";\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);

						$strdbsql = "DELETE FROM tbl_residentialSales WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql.";\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);

						$strdbsql = "DELETE FROM tbl_residentialLet WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql.";\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);

				/*		//delete current comm property types
						$strdbsql = "DELETE FROM tbl_commPropertyTypes WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql.";\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);   */

					}
					else
					{
						//replace res data in residential table
						$strdbsql = "REPLACE INTO tbl_residential ";
						$strdbsql .= "(fld_propertyID, fld_propertyBedrooms, fld_propertyBathrooms, fld_propertyEnsuites, fld_propertyReceptionRooms, fld_propertyKitchens, fld_propertyAge, fld_displayPropertyType, fld_propertyType, fld_propertyStyle) VALUES";
						$strdbsql .= "(".$property["PROPERTYID"].", '".$property["PROPERTYBEDROOMS"]."', '".$property["PROPERTYBATHROOMS"]."', '".$property["PROPERTYENSUITES"]."', '".$property["PROPERTYRECEPTIONROOMS"]."', '".$property["PROPERTYKITCHENS"]."', '".$property["PROPERTYAGE"]."', '".$property["DISPLAYPROPERTYTYPE"]."', '".$property["PROPERTYTYPE"]."', '".$property["PROPERTYSTYLE"]."')";
						//print($strdbsql.";\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);

						// delete any data in comm table
						$strdbsql = "DELETE FROM tbl_commercial WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql.";\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);

						//delete any comm property types
						$strdbsql = "DELETE FROM tbl_commPropertyTypes WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql.";\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);

						if($property["DEPARTMENT"] == "Sales")
						{
							//replace sales data in res sales table
							$strdbsql = "REPLACE INTO tbl_residentialSales ";
							$strdbsql .= "(fld_propertyID, fld_availability, fld_price, fld_forSalePOA, fld_priceQualifier) VALUES";
							$strdbsql .= "(".$property["PROPERTYID"].", '".$property["AVAILABILITY"]."', '".$property["PRICE"]."', '".$property["FORSALEPOA"]."', '".$property["PRICEQUALIFIER"]."')";
						//	print($strdbsql.";\r\n");
							$result = (mysql_query ($strdbsql,$strdb) && $result);

							//delete any data in res let table
							$strdbsql = "DELETE FROM tbl_residentialLet WHERE fld_propertyID = ".$property["PROPERTYID"];
							//print($strdbsql.";\r\n");
							$result = (mysql_query ($strdbsql,$strdb) && $result);
						}
						else
						{
							//replace letting data in res let table
							$strdbsql = "REPLACE INTO tbl_residentialLet ";
							$strdbsql .= "(fld_propertyID, fld_availability, fld_rent, fld_rentFrequency, fld_toLetPOA) VALUES";
							$strdbsql .= "(".$property["PROPERTYID"].", '".$property["AVAILABILITY"]."', '".$property["RENT"]."', '".$property["RENTFREQUENCY"]."', '".$property["TOLETPOA"]."')";
						//	print($strdbsql.";\r\n");
							$result = (mysql_query ($strdbsql,$strdb) && $result);

							//delete any data in res sales table
							$strdbsql = "DELETE FROM tbl_residentialSales WHERE fld_propertyID = ".$property["PROPERTYID"];
							//print($strdbsql.";\r\n");
							$result = (mysql_query ($strdbsql,$strdb) && $result);
						}
					}
                     /*
					//delete any images where updates have been recieved
					if (isset($property["IMAGES"]) && $property["IMAGES"] == "UPDATE")
					{
						//get current local images
						$strdbsql = "SELECT * FROM tbl_propertyImages WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql."\r\n");
						$result2 = mysql_query ($strdbsql,$strdb);
						while($resultdata = mysql_fetch_object($result2))
						{

							//delete files
							$strfilepath = $strrootpath.$strimagedir.$resultdata->fld_image;
							//print ("Delete: ".$strfilepath."\r\n");
							$result = (unlink($strfilepath) && $result);

							//delete thumbs
							$strfilepath = $strrootpath.$strthumbdir.$resultdata->fld_image;
							//print ("Delete: ".$strfilepath."\r\n");
							$result = (unlink($strfilepath) && $result);
						}
						mysql_free_result ($result2);

						//delte from database
						$strdbsql = "DELETE FROM tbl_propertyImages WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql.";\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);

					}

					//delete any floorplans where updates have been recieved
					if (isset($property["FLOORPLANS"]) && $property["FLOORPLANS"] == "UPDATE")
					{
						//get current local images
						$strdbsql = "SELECT * FROM tbl_propertyFloorplans WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql."\r\n");
						$result2 = mysql_query ($strdbsql,$strdb);
						while($resultdata = mysql_fetch_object($result2))
						{

							//delete files
							$strfilepath = $strrootpath.$strplandir.$resultdata->fld_image;
							//print ("Delete: ".$strfilepath."\r\n");
							$result = (unlink($strfilepath) && $result);
						}
						mysql_free_result ($result2);

						//delete from database
						$strdbsql = "DELETE FROM tbl_propertyFloorplans WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql.";\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);

					}

					//delete any epcgraphs where updates have been reieved
					if (isset($property["EPCGRAPHS"]) && $property["EPCGRAPHS"] == "UPDATE")
					{
						//get current local images
						$strdbsql = "SELECT * FROM tbl_propertyEPC WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql."\r\n");
						$result2 = mysql_query ($strdbsql,$strdb);
						while($resultdata = mysql_fetch_object($result2))
						{

							//delete files
							$strfilepath = $strrootpath.$strepcdir.$resultdata->fld_image;
							//print ("Delete: ".$strfilepath."\r\n");
							$result = (unlink($strfilepath) && $result);
						}
						mysql_free_result ($result2);

						//delete from database
						$strdbsql = "DELETE FROM tbl_propertyEPC WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql.";\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);

					}

					//delete any brochures where updates have been receieved
					if (isset($property["BROCHURES"]) && $property["BROCHURES"] == "UPDATE")
					{
						//get current local images
					//	$strdbsql = "SELECT * FROM tbl_propertyBrochures WHERE fld_propertyID = ".$property["PROPERTYID"];
					//	//print($strdbsql."\r\n");
					//	$result2 = mysql_query ($strdbsql,$strdb);
					//	while($resultdata = mysql_fetch_object($result2))
					//	{

					//		//delete files
					//		$strfilepath = $strrootpath.$strbrochuredir.$resultdata->fld_link;
					//		//print ("Delete: ".$strfilepath."\r\n");
					//		$result = (unlink($strfilepath) && $result);
					//	}
					//	mysql_free_result ($result2);

						//delte from database
						$strdbsql = "DELETE FROM tbl_propertyBrochures WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql.";\r\n");
						//$result = ($result AND mysql_query ($strdbsql,$strdb));
						$result = (mysql_query ($strdbsql,$strdb) && $result);

					}

					//delete any virtual tours where updates have been recieved
					if (isset($property["VIRTUALTOURS"]) && $property["VIRTUALTOURS"] == "UPDATE")
					{
						//get current local images
					//	$strdbsql = "SELECT * FROM tbl_propertyVirtualTours WHERE fld_propertyID = ".$property["PROPERTYID"];
					//	//print($strdbsql."\r\n");
					//	$result2 = mysql_query ($strdbsql,$strdb);
					//	while($resultdata = mysql_fetch_object($result2))
					//	{

					//		//if local file
					//		if(stristr($resultdata->fld_link, 'http://') === FALSE) {
					//			//delete files
					//			$strfilepath = $strrootpath.$strtourdir.$resultdata->fld_link;
					//			//print ("Delete: ".$strfilepath."\r\n");
					//			$result = (unlink($strfilepath) && $result);
					//		}
					//	}
					//	mysql_free_result ($result2);

						//delete from database
						$strdbsql = "DELETE FROM tbl_propertyVirtualTours WHERE fld_propertyID = ".$property["PROPERTYID"];
						//print($strdbsql."\r\n");
						$result = (mysql_query ($strdbsql,$strdb) && $result);
						//$result = mysql_query ($strdbsql,$strdb);

					}        */

					if($result) $arrupdate[$property["PROPERTYID"]] = "Property Database Entry Updated.";
					else  $arrupdate[$property["PROPERTYID"]] = "Property Database Entry Update Returned Some Errors.";
		/*		}
				//if in db and last mod dates match do nothing
				else
				{
					$arrunchanged[$property["PROPERTYID"]] = "Property unchanged since ".$property["DATELASTMODIFIED"];
				}    */

			}

		/*	//if comm prop types found save to db
			if (count($arrcommproptypes) > 0)
			{
				//insert in db tbl comm prop typeS
				foreach($arrcommproptypes AS $proptype)
				{
					if(!isset($arrunchanged[$proptype["propID"]]))
					{
					$strdbsql = "INSERT INTO tbl_commPropertyTypes (fld_propertyID, fld_propertyType) VALUES (".$proptype["propID"].", ".$proptype["type"].")";
					//print($strdbsql.";\r\n");
					$result = mysql_query ($strdbsql,$strdb);
					if(isset($arradd[$proptype["propID"]]) && stristr($arradd[$proptype["propID"]], " - Property Types Added") === FALSE) $arradd[$proptype["propID"]] .= " - Property Types Added";
					if(isset($arrupdate[$proptype["propID"]]) && stristr($arrupdate[$proptype["propID"]], " - Property Types Added") === FALSE) $arrupdate[$proptype["propID"]] .= " - Property Types Added";
					}
				}
			}    */

	/*		//if images found attempt to copy to servers
			if (count($arrimages) > 0)
			{
				foreach($arrimages AS $propimage)
				{
					if(!isset($arrunchanged[$propimage["propID"]]))
					{
					//if copy successful add to database
					$file = copyFileFromURL($propimage["image"], $strrootpath.$strimagedir);

					$strdbsql = "INSERT INTO tbl_propertyImages (fld_propertyID, fld_image) VALUES (".$propimage["propID"].", '".$file."')";
					//print($strdbsql.";\r\n");
					$result = mysql_query ($strdbsql,$strdb);
					if($file !== FALSE && $result)
					{
						//create thumbs
						make_thumb($strrootpath.$strimagedir.$file,$strrootpath.$strthumbdir.$file,$thumbWidth, $thumbHeight);
						if(isset($arradd[$propimage["propID"]]) && stristr($arradd[$propimage["propID"]], " - Property Images Added") === FALSE) $arradd[$propimage["propID"]] .= " - Property Images Added";
						if(isset($arrupdate[$propimage["propID"]]) && stristr($arrupdate[$propimage["propID"]], " - Property Images Added") === FALSE) $arrupdate[$propimage["propID"]] .= " - Property Images Added";

					}
					else
					{
						if(isset($arradd[$propimage["propID"]]) && stristr($arradd[$propimage["propID"]], " - 1 or more Property Images could not be added") === FALSE) $arradd[$propimage["propID"]] .= " - 1 or more Property Images could not be added";
						if(isset($arrupdate[$propimage["propID"]]) && stristr($arrupdate[$propimage["propID"]], " - 1 or more Property Images could not be added") === FALSE) $arrupdate[$propimage["propID"]] .= " - 1 or more Property Images could not be added";
					}
					}
				}
			}

			//if floorplans found attempt to copy to servers
			if (count($arrfloorplans) > 0)
			{
				foreach($arrfloorplans AS $propfloorplan)
				{
					if(!isset($arrunchanged[$propfloorplan["propID"]]))
					{
					//if copy successful add to database
					$file = copyFileFromURL($propfloorplan["image"], $strrootpath.$strplandir);
					$strdbsql = "INSERT INTO tbl_propertyFloorplans (fld_propertyID, fld_image) VALUES (".$propfloorplan["propID"].", '".$file."')";
					//print($strdbsql.";\r\n");
					$result = mysql_query ($strdbsql,$strdb);
					if($file !== FALSE && $result)
					{
						if(isset($arradd[$propfloorplan["propID"]]) && stristr($arradd[$propfloorplan["propID"]], " - Property Floorplans Added") === FALSE) $arradd[$propfloorplan["propID"]] .= " - Property Floorplans Added";
						if(isset($arrupdate[$propfloorplan["propID"]]) && stristr($arrupdate[$propfloorplan["propID"]], " - Property Floorplans Added") === FALSE) $arrupdate[$propfloorplan["propID"]] .= " - Property Floorplans Added";
					}
					else
					{
						if(isset($arradd[$propfloorplan["propID"]]) && stristr($arradd[$propfloorplan["propID"]], " - 1 or more Property Floorplans could not be added") === FALSE) $arradd[$propfloorplan["propID"]] .= " - 1 or more Property Floorplans could not be added";
						if(isset($arrupdate[$propfloorplan["propID"]]) && stristr($arrupdate[$propfloorplan["propID"]], " - 1 or more Property Floorplans could not be added") === FALSE) $arrupdate[$propfloorplan["propID"]] .= " - 1 or more Property Floorplans could not be added";
					}
					}
				}
			}

			//if epcgraphs found attempt to copy to servers
			if (count($arrepcgraphs) > 0)
			{
				foreach($arrepcgraphs AS $propepc)
				{
					if(!isset($arrunchanged[$propepc["propID"]]))
					{
					//if copy successful add to database
					$file = copyFileFromURL($propepc["link"], $strrootpath.$strepcdir);
					$strdbsql = "INSERT INTO tbl_propertyEPC (fld_propertyID, fld_image) VALUES (".$propepc["propID"].", '".$file."')";
					//print($strdbsql.";\r\n");
					$result = mysql_query ($strdbsql,$strdb);
					if($file !== FALSE && $result)
					{
						if(isset($arradd[$propepc["propID"]]) && stristr($arradd[$propepc["propID"]], " - Property EPC Added") === FALSE) $arradd[$propepc["propID"]] .= " - Property EPC Added";
						if(isset($arrupdate[$propepc["propID"]]) && stristr($arrupdate[$propepc["propID"]], " - Property EPC Added") === FALSE) $arrupdate[$propepc["propID"]] .= " - Property EPC Added";
					}
					else
					{
						if(isset($arradd[$propepc["propID"]]) && stristr($arradd[$propepc["propID"]], " - 1 or more Property EPC could not be added") === FALSE) $arradd[$propepc["propID"]] .= " - 1 or more Property EPC could not be added";
						if(isset($arrupdate[$propepc["propID"]]) && stristr($arrupdate[$propepc["propID"]], " - 1 or more Property EPC could not be added") === FALSE) $arrupdate[$propepc["propID"]] .= " - 1 or more Property EPC could not be added";
					}
					}
				}
			}

			//if brochures found attempt to copy to servers
			if (count($arrbrochures) > 0)
			{
				foreach($arrbrochures AS $propbrochure)
				{
					if(!isset($arrunchanged[$propbrochure["propID"]]))
					{
					//if copy successful add to database
	//				$file = copyFileFromURL($propbrochure["link"], $strrootpath.$strbrochuredir);
					$file = $propbrochure["link"];
					$strdbsql = "INSERT INTO tbl_propertyBrochures (fld_propertyID, fld_link) VALUES (".$propbrochure["propID"].", '".$file."')";
					//print($strdbsql.";\r\n");
					$result = mysql_query ($strdbsql,$strdb);
					if($file !== FALSE && $result)
					{
						if(isset($arradd[$propbrochure["propID"]]) && stristr($arradd[$propbrochure["propID"]], " - Property Brochures Added") === FALSE) $arradd[$propbrochure["propID"]] .= " - Property Brochures Added";
						if(isset($arrupdate[$propbrochure["propID"]]) && stristr($arrupdate[$propbrochure["propID"]], " - Property Brochures Added") === FALSE) $arrupdate[$propbrochure["propID"]] .= " - Property Brochures Added";
					}
					else
					{
						if(isset($arradd[$propbrochure["propID"]]) && stristr($arradd[$propbrochure["propID"]], " - 1 or more Property Brochures could not be added") === FALSE) $arradd[$propbrochure["propID"]] .= " - 1 or more Property Brochures could not be added";
						if(isset($arrupdate[$propbrochure["propID"]]) && stristr($arrupdate[$propbrochure["propID"]], " - 1 or more Property Brochures could not be added") === FALSE) $arrupdate[$propbrochure["propID"]] .= " - 1 or more Property Brochures could not be added";
					}
					}
				}
			}

			//if virtualtours found attempt to copy to servers
			if (count($arrvirtualtours) > 0)
			{
				foreach($arrvirtualtours AS $proptour)
				{
					if(!isset($arrunchanged[$proptour["propID"]]))
					{
					//test if file on jupix or full link
	//				if(stristr($proptour["link"], 'jupix.co.uk') === FALSE) {
						// add linkto database
						$file = TRUE;
						$strdbsql = "INSERT INTO tbl_propertyVirtualTours (fld_propertyID, fld_link) VALUES (".$proptour["propID"].", '".$proptour["link"]."')";

	//				}
	//				else
	//				{
	//					//copy to local sever
	//					// if copy successful add filename to database
	//					$file = copyFileFromURL($proptour["link"], $strrootpath.$strtourdir);
	//					$strdbsql = "INSERT INTO tbl_propertyVirtualTours (fld_propertyID, fld_link) VALUES (".$proptour["propID"].", '".$file."')";
	//				}

					//print($strdbsql.";\r\n");
					$result = mysql_query ($strdbsql,$strdb);
					if($file !== FALSE && $result)
					{
						if(isset($arradd[$proptour["propID"]]) && stristr($arradd[$proptour["propID"]], " - Property Tours Added") === FALSE) $arradd[$proptour["propID"]] .= " - Property Tours Added";
						if(isset($arrupdate[$proptour["propID"]]) && stristr($arrupdate[$proptour["propID"]], " - Property Tours Added") === FALSE) $arrupdate[$proptour["propID"]] .= " - Property Tours Added";
				}
					else
					{
						if(isset($arradd[$proptour["propID"]]) && stristr($arradd[$proptour["propID"]], " - 1 or more Property Tours could not be added") === FALSE) $arradd[$proptour["propID"]] .= " - 1 or more Property Tours could not be added";
						if(isset($arrupdate[$proptour["propID"]]) && stristr($arrupdate[$proptour["propID"]], " - 1 or more Property Tours could not be added") === FALSE) $arrupdate[$proptour["propID"]] .= " - 1 or more Property Tours could not be added";
					}
					}
				}
			}
			
			if(count($arraddresses) > 0)
			{
				foreach ($arraddresses AS $key =>$address)
				{
					if(!isset($arrunchanged[$key]))
					{
					$strggladdress = implode(", ",$address);
					$intgglpropertyID = $key;

					//get location feed
					$location = geolocate($strggladdress);
					if(is_array($location))
					{
						$strdbsql = "UPDATE tbl_property SET fld_lat = '".$location["lat"]."', fld_lng = '".$location["lng"]."', fld_locationtype = '".$location["type"]."' WHERE fld_propertyID = $intgglpropertyID";
						if(mysql_query ($strdbsql,$strdb)) $arrgglreport[$intgglpropertyID] = " Location Saved Successfully (Type = ".$location["type"].")";
						else  $arrgglreport[$intgglpropertyID] = " Location Save Failed ";

					}
					else
					{
						$arrgglreport[$intgglpropertyID] = " $location";
					}
					$location = NULL;
}
				}
			}

			// if db entry not in jupix import array delete from db
			foreach($arrdbprops as $propertyID => $propertydate)
			{
				if(!isset($arradd[$propertyID]) && !isset($arrupdate[$propertyID]) && !isset($arrunchanged[$propertyID]))
				{
					//get image file names from DB and delete files
					$strdbsql = "SELECT * FROM tbl_propertyImages WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql."\r\n");
					$result2 = mysql_query ($strdbsql,$strdb);
					$result = TRUE;
					while($resultdata = mysql_fetch_object($result2))
					{
						//delete files
						$strfilepath = $strrootpath.$strimagedir.$resultdata->fld_image;
						//print ("Delete: ".$strfilepath."\r\n");
						$result = (unlink($strfilepath) && $result);

						//delete thumbs
						$strfilepath = $strrootpath.$strthumbdir.$resultdata->fld_image;
						//print ("Delete: ".$strfilepath."\r\n");
						$result = (unlink($strfilepath) && $result);
					}
					mysql_free_result ($result2);

					//delte from database
					$strdbsql = "DELETE FROM tbl_propertyImages WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql.";\r\n");
					$result = (mysql_query ($strdbsql,$strdb) && $result);

					//get floorplan file names from db and delete
					$strdbsql = "SELECT * FROM tbl_propertyFloorplans WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql."\r\n");
					$result2 = mysql_query ($strdbsql,$strdb);
					while($resultdata = mysql_fetch_object($result2))
					{
						//delete files
						$strfilepath = $strrootpath.$strplandir.$resultdata->fld_image;
						//print ("Delete: ".$strfilepath."\r\n");
						$result = (unlink($strfilepath) && $result);
					}
					mysql_free_result ($result2);

					//delete from database
					$strdbsql = "DELETE FROM tbl_propertyFloorplans WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql.";\r\n");
					$result = (mysql_query ($strdbsql,$strdb) && $result);

					//get brochure filenames from db and delete
					//$strdbsql = "SELECT * FROM tbl_propertyBrochures WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql."\r\n");
					//$result2 = mysql_query ($strdbsql,$strdb);
					//while($resultdata = mysql_fetch_object($result2))
					//{
					//	//delete files
					//	$strfilepath = $strrootpath.$strbrochuredir.$resultdata->fld_link;
					//	//print ("Delete: ".$strfilepath."\r\n");
					//	$result = (unlink($strfilepath)&& $result);
					//}
					//mysql_free_result ($result2);

					//delte from database
					$strdbsql = "DELETE FROM tbl_propertyBrochures WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql.";\r\n");
					$result = (mysql_query ($strdbsql,$strdb) && $result);

					//get epc graphs filesnames from db and delete
					$strdbsql = "SELECT * FROM tbl_propertyEPC WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql."\r\n");
					$result2 = mysql_query ($strdbsql,$strdb);
					while($resultdata = mysql_fetch_object($result2))
					{
						//delete files
						$strfilepath = $strrootpath.$strepcdir.$resultdata->fld_image;
						//print ("Delete: ".$strfilepath."\r\n");
						$result = (unlink($strfilepath) && $result);
					}
					mysql_free_result ($result2);

					//delete from database
					$strdbsql = "DELETE FROM tbl_propertyEPC WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql.";\r\n");
					$result = (mysql_query ($strdbsql,$strdb) && $result);


					//get virtual tour filesnames from db and delete
					//$strdbsql = "SELECT * FROM tbl_propertyVirtualTours WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql."\r\n");
					//$result2 = mysql_query ($strdbsql,$strdb);
					///while($resultdata = mysql_fetch_object($result2))
					//{
					//	//if local file
					//	if(stristr($resultdata->fld_link, 'http://') === FALSE) {
					//		//delete files
					//		$strfilepath = $strrootpath.$strtourdir.$resultdata->fld_link;
					//		//print ("Delete: ".$strfilepath."\r\n");
					//		$result = ($result AND unlink($strfilepath));
					//	}
					//}
					//mysql_free_result ($result2);

					//delete from database
					$strdbsql = "DELETE FROM tbl_propertyVirtualTours WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql."\r\n");
					$result = (mysql_query ($strdbsql,$strdb) && $result);

					//delete entry from properties table
					$strdbsql = "DELETE FROM tbl_property WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql."\r\n");
					$result = (mysql_query ($strdbsql,$strdb) && $result);

					//delete entry from residential table
					$strdbsql = "DELETE FROM tbl_residential WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql."\r\n");
					$result = (mysql_query ($strdbsql,$strdb) && $result);

					//delete entry from commercial table
					$strdbsql = "DELETE FROM tbl_commercial WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql."\r\n");
					$result = (mysql_query ($strdbsql,$strdb) && $result);

					//delete entry from commercial prop types table
					$strdbsql = "DELETE FROM tbl_commPropertyTypes WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql."\r\n");
					$result = (mysql_query ($strdbsql,$strdb) && $result);

					//delete entry from sales table
					$strdbsql = "DELETE FROM tbl_residentialSales WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql."\r\n");
					$result = (mysql_query ($strdbsql,$strdb) && $result);

					//delete entry from lettings table
					$strdbsql = "DELETE FROM tbl_residentialLet WHERE fld_propertyID = ".$propertyID;
					//print($strdbsql."\r\n");
					$result = (mysql_query ($strdbsql,$strdb) && $result);
	
					if($result) $arrdelete[$propertyID] = "Property Deleted";
					else $arrdelete[$propertyID] = "Property Delete Returned some Errors";
				}

			}     */

			//Output update results
			$strmessage = " Downloaded ".count($arrproperties)."\r\n";
			$strmessage.= " In Database ".count($arrdbprops)."\r\n";
			$strmessage.= " Added ".count($arradd)."\r\n";
			$strmessage.= " Updated ".count($arrupdate)."\r\n";
			$strmessage.= " Deleted ".count($arrdelete)."\r\n";
			$strmessage.= " Unchanged ".count($arrunchanged)."\r\n\r\n";
			$strmessage.= " -----------------------------\r\n";

			//loop through update messages
			if(count($arradd) > 0)
			{
				foreach($arradd AS $key => $value)
				{
					$strmessage .= " $key:\t$value\r\n";
				}
				$strmessage.= " -----------------------------\r\n";
			}

			if(count($arrupdate) > 0)
			{
				foreach($arrupdate AS $key => $value)
				{
					$strmessage .= " $key:\t$value\r\n";
				}
				$strmessage.= " -----------------------------\r\n";
			}

			if(count($arrdelete) > 0)
			{
				foreach($arrdelete AS $key => $value)
				{
					$strmessage .= " $key:\t$value\r\n";
				}
				$strmessage.= " -----------------------------\r\n";
			}

			if(count($arrunchanged) > 0)
			{
				foreach($arrunchanged AS $key => $value)
				{
					$strmessage .= " $key:\t$value\r\n";
				}
				$strmessage.= " -----------------------------\r\n";
			}
			if(count($arrgglreport) > 0)
			{
				foreach($arrgglreport AS $key => $value)
				{
					$strmessage .= " $key:\t$value\r\n";
				}
				$strmessage.= " -----------------------------\r\n";
			}
		}
	}
	else if(!isset($strmessage))
	{
		$strmessage = " Error getting feed text.";
	}


	//------------------------------------------------------------------------------------------------------------//
	// geberate Report
	//------------------------------------------------------------------------------------------------------------//


	echo $strlogheader.$strmessage;

mysql_close ($strdb);
?>