<?php
	//	**********************************************************************
	//	* by Lee Watts                                                       *
	//	* Email address: leewatts@millerwaite.co.uk                          *
	//	*                                                                    *
	//	* For: Ross Estate Agencies  		                                 *
	//	*                                                                    *
	//	* incsitecommon.php		                                             *
	//	*--------------------------------------------------------------------*
		$strthisver = "V1.0 - November 2014";
	//	**********************************************************************
	
	// Define database details
	define("DRIVER", "mysql");
	define("HOST", "localhost");
	define("USER", "db_esteem_user");
	define("PASS", "Mfj$1j13");
	define("DB", "db_esteem_site");
	
	// Define database details
	define("BOOKDRIVER", "mysql");
	define("BOOKHOST", "localhost");
	define("BOOKUSER", "db_esteem_user");
	define("BOOKPASS", "Mfj$1j13");
	define("BOOKDB", "db_esteem_properties");
	
	include("functions.php");	
	include("config.php");
	
	// Root site title and name
	$strTitle  = "Esteem Homes";
	
	// Site file path and URL
	$strrootpath = $_SERVER['DOCUMENT_ROOT']."/";
	$strsitehref = (isset($_SERVER['SERVER_NAME']) ? "https://".$_SERVER['SERVER_NAME'] : "");
	$strsiteurl = (isset($_SERVER['SERVER_NAME']) ? "https://".$_SERVER['SERVER_NAME']."/" : "");
	
	date_default_timezone_set('Europe/London');
	
	// Define any other variables
	$booldebug = false;
	
	// Create company variables
	$compName = $companyDetails['companyName'];
	$compEmail = $companyDetails['companyEmail'];
	$compEmail2 = $companyDetails['companyEmail2'];
	$compNumber = $companyDetails['companyNumber'];
	
	$compLogo = $companyDetails['companyLogo'];
	
	$compAddStreet = $companyDetails['addressStreet'];
	$compAddCity = $companyDetails['addressCity'];
	$compAddCounty = $companyDetails['addressCounty'];
	$compAddCountry = $companyDetails['addressCountry'];
	$compAddPostcode = $companyDetails['addressPostCode'];
	
	$compPhone = $companyDetails['companyPhone'];
	$compFax = $companyDetails['companyFax'];
	$compMobile = $companyDetails['companyMob'];
	
	
?>