<?php
/*************************************************/
/* Auto resize all images in main directory that */
/* have not been added to the thumb & iphone     */
/* directories                                   */
/*************************************************/
/* These files should be created automatically   */
/* when a property is created, if this hasn't    */
/* worked you shoul dredownload the main image   */
/* from Jupix before running this script.        */
/*************************************************/


error_reporting(E_ALL);
ini_set('display_errors', '1');
	//file transfer and edit functions
	function make_thumb($img_name,$filename,$new_w,$new_h)
	{
		$ext=getExtension($img_name);
		$newextn = $ext;
		$r = 255;
		$g = 255;
		$b = 255;
		if(!strcmp("jpg",$ext) || !strcmp("jpeg",$ext)) $src_img=imagecreatefromjpeg($img_name);
		if(!strcmp("png",$ext)) $src_img=imagecreatefrompng($img_name);
		if(!strcmp("gif",$ext)) $src_img=imagecreatefromgif($img_name);

		$old_x=imageSX($src_img);
		$old_y=imageSY($src_img);

		$ratio1=$old_x/$new_w;
		$ratio2=$old_y/$new_h;

		if($ratio1>$ratio2)
		{
			$thumb_w=$new_w;
			$thumb_h=$old_y/$ratio1;
		}
		else
		{
			$thumb_h=$new_h;
			$thumb_w=$old_x/$ratio2;
		}

		$dst_img=ImageCreateTrueColor($new_w,$new_h);
		$starty = ($new_h - $thumb_h)/2;
		$startx = ($new_w - $thumb_w)/2;
		imagefill ( $dst_img , 0 , 0 , imagecolorallocate ( $dst_img , $r, $g , $b ) );

		imagecopyresampled($dst_img,$src_img,$startx,$starty,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

		if(!strcmp("png",$newextn)) imagepng($dst_img,$filename);
		else if(!strcmp("jpg",$newextn) || !strcmp("jpeg",$newextn)) imagejpeg($dst_img,$filename);
		else if(!strcmp("gif",$newextn)) imagegif($dst_img,$filename);
		if(substr(sprintf('%o', fileperms($filename)), -4) != "0777") chmod($filename, 0777);
  
		imagedestroy($dst_img);
		imagedestroy($src_img);
	}
	function make_image($img_name,$filename,$new_w,$new_h)
	{
		$ext=getExtension($img_name);
		$newextn = $ext;
		$r = 255;
		$g = 255;
		$b = 255;
		if(!strcmp("jpg",$ext) || !strcmp("jpeg",$ext)) $src_img=imagecreatefromjpeg($img_name);
		if(!strcmp("png",$ext)) $src_img=imagecreatefrompng($img_name);
		if(!strcmp("gif",$ext)) $src_img=imagecreatefromgif($img_name);

		$old_x=imageSX($src_img);
		$old_y=imageSY($src_img);

		$ratio1=$old_x/$new_w;
		$ratio2=$old_y/$new_h;

		if($ratio1>$ratio2)
		{
			$thumb_w=$new_w;
			$thumb_h=$old_y/$ratio1;
		}
		else
		{
			$thumb_h=$new_h;
			$thumb_w=$old_x/$ratio2;
		}

		$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
		$starty = 0;
		$startx = 0;
		imagefill ( $dst_img , 0 , 0 , imagecolorallocate ( $dst_img , $r, $g , $b ) );

		imagecopyresampled($dst_img,$src_img,$startx,$starty,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

		if(!strcmp("png",$newextn)) imagepng($dst_img,$filename);
		else if(!strcmp("jpg",$newextn) || !strcmp("jpeg",$newextn)) imagejpeg($dst_img,$filename);
		else if(!strcmp("gif",$newextn)) imagegif($dst_img,$filename);
		if(substr(sprintf('%o', fileperms($filename)), -4) != "0777") chmod($filename, 0777);
  
		imagedestroy($dst_img);
		imagedestroy($src_img);
	}

	function getExtension($str) {
		$i = strrpos($str,".");
		if (!$i) { return ""; }
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}
$imgroot = '/var/www/vhosts/esteemhomes.co.uk/httpdocs/datafeed/images/';
$maindir =  'full';
$directories = array('iphone','iphone_thumb','thumb');
$sizes = array(480,100,350);
$types = array("image","image","thumb");

$dir = $maindir;

print ("Resize files in directory: '".$dir."'\n\n");
if ($handle = opendir($imgroot.$dir)) {
	print("In directory: '".$dir."'\n\n");
	while (false !== ($file = readdir($handle))) { 
		if ($file != "." && $file != "..") {
			$arrsize = getimagesize($imgroot.$dir."/".$file);
			if(intval($arrsize[0]) > 750 || intval($arrsize[1]) > 750) {
				make_image($dir."/".$file, $dir."/".$file, 750,750);
				Print ("resized main file $file \n");
			}
		}
	}
	print("----------------------------\n\n");
	closedir($handle);
} else {
	print ("Error: can't open directory '".$dir."'\n\n");
	print ("----------------------------\n\n");
}

foreach ($directories as $key => $tempdir) {
	print("In directory: '".$tempdir."'\n\n");
	if ($handle = opendir($imgroot.$dir)) {
		/* This is the correct way to loop over the directory. */
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != "..") {
				if (!file_exists($imgroot.$tempdir."/".$file)) {
					if($types[$key] == "image") make_image($imgroot.$dir."/".$file, $imgroot.$tempdir."/".$file, $sizes[$key],$sizes[$key]);
					else make_thumb($imgroot.$dir."/".$file, $imgroot.$tempdir."/".$file, $sizes[$key],$sizes[$key]);
					Print ("added $tempdir ".$types[$key]." file $file \n");
					if(chmod($imgroot.$tempdir."/".$file, 0777)) print ("Mod Success: '".$tempdir."/".$file."'\n\n");
					else print ("Error: can't mod '".$tempdir."/".$file."'\n\n");
				}
			}
		}
		print("----------------------------\n\n");
		closedir($handle);
	}
	else
	{
		print ("Error: can't open directory '".$dir."'\n\n");
		print ("----------------------------\n\n");
		exit;
	}
}

?>