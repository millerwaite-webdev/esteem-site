<?php
	//partial url for location api
	$strgoogleapiloc = "http://maps.googleapis.com/maps/api/geocode/xml?sensor=false&address=";

	//locate file name for feed - use feed dir defined in incsitecommon
	$strgglbkpname = "location.xml";

	//used to store data from google location feed
	$arrlocation = array ();

	//used in XML parsering of Google location feed
	$strggltagname = "";
	$strggltagparent = "";

	//define Google Processing functions
	function ggl_start_tag($parser, $name, $attribs) {
		global $strggltagname;
		global $strggltagparent;
		
		$strggltagparent = $strggltagname;
		$strggltagname = $name;

	}

	function ggl_end_tag($parser, $name) {
		global $strggltagname;
		global $strggltagparent;

		$strggltagname = $strggltagparent;
		$strggltagparent = "";

	}

	function ggl_tag_contents($parser, $data) {
		global $strggltagname;
		global $strggltagparent;
		global $arrlocation;

		if($strggltagparent == "LOCATION" && $strggltagname == "LAT") $arrlocation["lat"] = $data;
		if($strggltagparent == "LOCATION" && $strggltagname == "LNG") $arrlocation["lng"] = $data;
		if($strggltagparent == "NORTHEAST" && $strggltagname == "LAT") $arrlocation["maxlat"] = $data;
		if($strggltagparent == "NORTHEAST" && $strggltagname == "LNG") $arrlocation["maxlng"] = $data;
		if($strggltagparent == "SOUTHWEST" && $strggltagname == "LAT") $arrlocation["minlat"] = $data;
		if($strggltagparent == "SOUTHWEST" && $strggltagname == "LNG") $arrlocation["minlng"] = $data; 
		if($strggltagname == "LOCATION_TYPE") $arrlocation["type"] = $data;
		if($strggltagname == "STATUS") $arrlocation["status"] = $data;
		if($strggltagname == "FORMATTED_ADDRESS") $arrlocation["disp"] = $data;
	}

	//define main geolocate function
	function geolocate($straddress, $intlocid = '')
	{
		global $arrlocation;
		global $strgglbkpname;
		global $strgoogleapiloc;
		global $strfeedbkploc;
		global $strgglbkpname;

		//get location feed
		$straddress = str_replace(" ", "+", $straddress);
		$strggltext = file_get_contents($strgoogleapiloc.$straddress);

		//back up local copy
		if($intlocid != "") $strfilename = $strfeedbkploc.$intlocid."_".$strgglbkpname;
		else $strfilename = $strfeedbkploc."Temp_".$strgglbkpname;
		file_put_contents($strfilename, $strggltext);
		if(substr(sprintf('%o', fileperms($strfilename)), -4) != "0777") chmod($strfilename, 0777);

		//attempt to create XML parsers for google
		if (! ($gglparser = xml_parser_create()) )
		{
			return " Error creating XML Parser";
		}

		// set XML Processing functins
		xml_set_element_handler($gglparser, "ggl_start_tag", "ggl_end_tag");
		xml_set_character_data_handler($gglparser, "ggl_tag_contents");

		if (!xml_parse($gglparser, $strggltext)) {
			return " Error Processing Geolocate API";
		}
		else
		{
			if(isset($arrlocation["status"]) && strtolower($arrlocation["status"]) == "ok") return $arrlocation;
			else if (isset($arrlocation["status"])) return " Location Not Found - Status returned as: ".$arrlocation["status"];
			else return " Location Not Found - Google API did not return a reason";
		}
		xml_parser_free($gglparser);
	}


?>