<?php
	//added standard php/mysql config file with host, user and password info
	require "../includes/incsitecommon.php";
	
	//models and collections
	require "domain/models/filter-result-model.php";
	require "domain/collection/filter-result-collection.php";
	
	//domain
	require "domain/action.php";
	require "domain/sorting.php";
	require "domain/pagination-commercial.php";
	require "domain/filtering.php";
	require "domain/server-commercial.php";
	
	//controls
	require "controls/sortdropdown.php";	
	require "controls/textbox.php";	
	require "controls/checkboxgroupfilter.php";
	require "controls/filterdropdown.php";
	require "controls/filterselect.php";
    require "controls/range-slider.php";	
    require "controls/range-filter.php";
    require "controls/button-text-filter.php";
		
	class jPListHTML extends jPListServer{
			
		/**
		* get html for one item
		* @param {Object} $item
		* @return {string} html
		*/
		private function getHTML($row){
		
			$html = "";
		
			
			//	PROPERTY CONTAINER
			$html .= ("<div class='block-prop'>");
			
				$bookConn = bookConnect();
				
				//	GET ALL IMAGES FOR CURRENT PROPERTY
				$strdbsql = "SELECT * FROM tbl_propertyImages
				WHERE fld_propertyID = :propID ORDER BY fld_counter";
				$arrType = "multi";
				$dataParams = array('propID' => $row['fld_propertyID']);
				$resultdata2 = query($bookConn, $strdbsql, $arrType, $dataParams);
				
				//	CREATE SLIDER AND ADD IMAGES
				$html .= ("<div id='carousel-".$row['fld_propertyID']."' class='carousel slide' data-ride='carousel' data-interval='false'>");
					$html .= ("<div class='carousel-inner' role='listbox'>");

						if(count($resultdata2) > 0) {
					
							$x = 0;
					
							foreach($resultdata2 AS $row2) {
							
								$propImage = str_replace("IMG_", "", substr($row2['fld_image'], 0, strpos($row2['fld_image'], '_', strpos($row2['fld_image'], '_')+1)));
							
								if(!empty($row2['fld_image'])) {
									$html .= ("<div class='item".($x == 0 ? " active" : "")."' style='background-image:url(https://rossestateagencies.com/datafeed/images/thumb/".$row2['fld_image'].");'>");
										$html .= ("<a href='/commercial/".$row['fld_propertyID']."'></a>");
									$html .= ("</div>");
									$x++;
								}
								
								
								
							}
							
						} else {
							
							$html .= ("<div class='item active' style='background-color:white;background-image:url(https://rossestateagencies.com/datafeed/images/thumb/placeholder.jpg);background-size:contain;'>");
								$html .= ("<a href='/commercial/".$row['fld_propertyID']."'></a>");
							$html .= ("</div>");
							
						}
							
					$html .= ("</div>");
					
					if(count($resultdata2)> 1) {
						
						$html .= ("<a class='left carousel-control' href='#carousel-".$row['fld_propertyID']."' role='button' data-slide='prev'>");
							$html .= ("<span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>");
							$html .= ("<span class='sr-only'>Previous</span>");
						$html .= ("</a>");
						$html .= ("<a class='right carousel-control' href='#carousel-".$row['fld_propertyID']."' role='button' data-slide='next'>");
							$html .= ("<span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>");
							$html .= ("<span class='sr-only'>Next</span>");
						$html .= ("</a>");
						
					}
					
					//	PROPERTY STATUS
					$display = true;
					
					switch($row['availability']) {
						case "On Hold":
							$colour = "#9e9e9e";
							break;
						case "For Sale":
						case "To Let":
						case "Let":
							$colour = "";
							$display = false;
							break;
						case "Under Offer":
						case "References Pending":
							$colour = "#f4b136";
							break;
						case "Sold STC":
						case "Sold":
						case "Let Agreed":
							$colour = "#f44336";
							break;
						case "Withdrawn":
							$colour = "#9e9e9e";
							break;
					}				
					
					if($display == true) {
						$html .= ("<div class='status' style='background-color:".$colour.";'>");
							$html .= ($row['availability']);
						$html .= ("</div>");
					}
					
				$html .= ("</div>");
				
				// PROPERTY PRICE				
				$html .= ("<a href='/sales/".$row['fld_propertyID']."' class='price price-commercial'>");
					if($row['fld_forSale'] == 1 && $row['fld_toLet'] == 1) {
						$html .= ("<span>");
							$html .= ("&pound;".number_format($row['fld_priceTo'])." or ");
							$html .= ("&pound;".number_format($row['fld_rentTo']));
						$html .= ("</span>");
						$html .= ("<span>".strtoupper($row['fld_rentFrequency'])."</span>");
					} else if($row['fld_forSale'] == 1) {
						$html .= ("<span>");
							$html .= ("&pound;".number_format($row['fld_priceTo']));
						$html .= ("</span>");
					} else {
						$html .= ("<span>");
							$html .= ("&pound;".number_format($row['fld_rentTo']));
						//	if(!empty($row['fld_rentFrequency'])) $html .= (" ".strtoupper($row['fld_rentFrequency']));
						$html .= ("</span>");
					}
				$html .= ("</a>");
				
				//	PROPERTY DETAILS
				$html .= ("<div class='block-info'>");
					$html .= ("<a class='head' href='/commercial/".$row['fld_propertyID']."'>");
					
						$html .= ("<span>");
						//	if(!empty($row['fld_addressName'])) $html .= ($row['fld_addressName'].", ");
						//	if(!empty($row['fld_addressNumber'])) $html .= ($row['fld_addressNumber']." ");
							if(!empty($row['fld_addressStreet'])) $html .= ($row['fld_addressStreet']."<br/>");
							if(!empty($row['fld_address2'])) $html .= ($row['fld_address2']."<br/>");
							if($row['fld_address3'] == "Walney") $html .= ($row['fld_address3'].", Barrow-in-Furness<br/>");
							else if(!empty($row['fld_address3'])) $html .= ($row['fld_address3']."<br/>");
							if(!empty($row['fld_addressPostcode'])) $html .= ($row['fld_addressPostcode']);
						$html .= ("</span>");
						
						$html .= ("<div style='display:none;'>");
							$html .= ("<span class='date-time'>".$row['fld_dateLastModified']."</span>");

								if($row['fld_forSale'] == 1) {
									$html .= ("<span class='for-sale'></span>");
								} else {
									$html .= ("<span class='for-rent'></span>");
								}
							$html .= ("<span class='".strtolower(str_replace(" ", "-", $row['fld_address3']))."'>".$row['fld_address3']."</span>");
							//$html .= ("<span class='bedrooms'>".$row['bedrooms']."</span>");
							$html .= ("<span class='all'></span>");

							if($row['fld_availability'] == 2 || $row['fld_availability'] == 3 || $row['fld_availability'] == 4 || $row['fld_availability'] == 10) $html .= ("<span class='normal'></span>");

						$html .= ("</div>");
					$html .= ("</a>");
				$html .= ("</div>");
				
				$html .= ("<div class='block-button'>");
					$html .= ("<a href='/sales/".$row['fld_propertyID']."'>View Property</a>");
				$html .= ("</div>");
				
			$html .= ("</div>");

			return $html;
		}
		
		/**
		* get the whole html
		* @param {string} $itemsHtml - items html
		* @return {string} html
		*/
		private function getHTMLWrapper($itemsHtml){
			
			$html = "";
			
			$html .= "<div data-type='jplist-dataitem' data-count='" . $this->pagination->numberOfPages . "' class='box'>";
			$html .= $itemsHtml;
			$html .= "</div>";		
			
			return $html;
		}
		
		/**
		* constructor
		*/
		public function __construct(){
			
			$html = "";
			
			try{
                parent::__construct();
				
				if(isset($this->statuses)){
					
					$items = $this->getData();
					
					if($items){
						foreach($items as $item){
							$html .= $this->getHTML($item);					
						}
					}
						
                    ob_clean();
                    
					//print html
					echo($this->getHTMLWrapper($html));
				}
				
				//close the database connection
				$this->db = NULL;
			}
			catch(PDOException $ex){
				print "Exception: " . $ex->getMessage();
			}
		}
	}
	
	/**
	* start
	*/
	new jPListHTML();
?>	