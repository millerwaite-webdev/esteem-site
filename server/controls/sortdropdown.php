<?php

	/**
	* get sort dropdown control query
	* @param {Object} $status
	* @return {string} - a part of order by expression
	* sort example
	* {
    *     "action": "sort",
    *     "name": "sort",
    *     "type": "drop-down",
    *     "data": {
    *         "path": ".like",
    *         "type": "number",
    *         "order": "asc",
    *         "dateTimeFormat": "{month}/{day}/{year}"
    *     },
    *     "cookies": true
    * }
	*/
	function bootsortdropdown($status){
		
		$query = "";
		$data = $status->data;
		$order = "desc";
		
		if(isset($data) && isset($data->path)){
		
			switch($data->path){
			
				case ".price-sales":
					$query = "fld_price";
					break;
				
				case ".price-rental":
					$query = "fld_rent";
					break;
				
				case ".price-commercial":
					$query = "(tbl_commercial.fld_priceTo+tbl_commercial.fld_rentTo)";
					break;
				
				case ".price-search":
					$query = "fld_price";
					break;
				
				case ".date-time":
					$query = "fld_dateLastModified";
					break;
				

			}
			
			if(isset($data->order)){
				$order = strtolower($data->order);
			}
			
			$order = ($order == "desc") ? "desc" : "asc";
			
			if($query){
				$query = $query . " " . $order;
			}
		}
		
		return $query;
	}
?>	