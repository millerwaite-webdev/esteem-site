<?php

	/**
	* get filter select control query
	* @param {Object} $status
	* @return {Object}
	* status example
	(
		[action] => filter
		[name] => category-select-filter
		[type] => filter-drop-down
		[data] => stdClass Object
			(
				[path] => .architecture
				[filterType] => path
			)

		[inStorage] => 1
		[inAnimation] => 1
		[isAnimateToTop] => 
		[inDeepLinking] => 1
	)
	*/
	function filterselect($status){
		
		$data = $status->data;
		$result = null;
		
		if(isset($data->path) && ($data->path != 'default')){
		
			$result = new FilterResultCollection('or');
			
			for($i = 0; $i < 2; $i++){
		
				$model = new FilterResultModel();
				
				$path = str_replace("-", " ", str_replace(array("."), "", $data->path));
				
				switch($status->name) {
					case "address":
						$path = str_replace("-", " ", $path);
						$test = explode(" ", $path);
						$tester = $test[0];
												
						if($i == 0) $model->query = " fld_address2 like ? ";
						else $model->query = " fld_address3 like ? ";
						
						$model->param = "%".$tester."%";
						break;
					case "bedroomsMin":
						$model->param = $path;
						$model->query = " tbl_residential.fld_propertyBedrooms >= ? ";
						break;
					case "bedroomsMax":
						$model->param = $path;
						$model->query = " tbl_residential.fld_propertyBedrooms <= ? ";
						break;
					case "salesPriceMin":
						$model->query = " tbl_residentialSales.fld_price >= ? ";
						$model->param = $path;
						break;
					case "salesPriceMax":
						$model->query = " tbl_residentialSales.fld_price <= ? ";
						$model->param = $path;
						break;
					case "rentalPriceMin":
						$model->param = $path;
						$model->query = " tbl_residentialLet.fld_rent >= ? ";
						break;
					case "rentalPriceMax":
						$model->param = $path;
						$model->query = " tbl_residentialLet.fld_rent <= ? ";
						break;
					case "commercialPriceMin":
						$model->param = $path;
						$model->query = " (tbl_commercial.fld_priceTo + tbl_commercial.fld_rentTo) >= ? ";
						break;
					case "commercialPriceMax":
						$model->param = $path;
						$model->query = " (tbl_commercial.fld_priceTo + tbl_commercial.fld_rentTo) <= ? ";
						break;
				}
				
				array_push($result->filterResults, $model);
				
			}
		}
		
		return $result;
	}
	
?>	