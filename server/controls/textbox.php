<?php

	/**
	* get textbox control query
	* @param {Object} $status
	* @return {Object}
	* status example
	* {
    *     "action": "filter",
    *     "name": "title-filter",
    *     "type": "textbox",
    *     "data": {
    *         "path": ".title",
    *         "ignore": "[~!@#$%^&*()+=`'\"/\\_]+",
    *         "value": "",
    *         "filterType": "text"
    *     },
    *     "cookies": true
    * }
	*/
	function radiobuttonsfilters($status){
		
		$data = $status->data;
		$result = null;
		
		if(isset($data->path) && $data->selected){
		
			$result = new FilterResultModel();
			
			switch($data->path){
			
				case ".title":{
					$result->param = "%$data->value%";
			
					$result->query = " title like ? ";
					break;
				}
				
				case ".desc":{
					$result->param = "%$data->value%";
			
					$result->query = " description like ? ";
					break;
				}

				case ".for-sale":
					$result->param = "1";
                    $result->query = " fld_forSale= ? ";
					break;

				case ".for-rent":
					$result->param = "1";
                    $result->query = " fld_toLet = ? ";
					break;
					
				case ".normal-commercial":
					$result->param = "2";
                    $result->query = " tbl_commercial.fld_availability IN (?,3,4,5,10)";
					break;
					
				case ".all-commercial":
					$result->param = "-10 ";
                    $result->query = " tbl_commercial.fld_availability != ?";
					break;
					
				case ".normal-rental":
					$result->param = "4";
                    $result->query = " tbl_residentialLet.fld_availability != ? ";
					break;
					
				case ".all-rental":
					$result->param = "-10";
                    $result->query = " tbl_residentialLet.fld_availability != ?";
					break;
				case ".normal-sales":
					$result->param = "4";
                    $result->query = " tbl_residentialSales.fld_availability NOT IN (?) ";
					break;
					
				case ".all-sales":
					$result->param = "-10";
                    $result->query = " tbl_residentialSales.fld_availability != ? ";
					break;
					
				case ".normal-search":
					$result->param = "4";
                    $result->query = " tbl_residentialSales.fld_availability NOT IN (?) ";
					break;
					
				case ".all-search":
					$result->param = "-10";
                    $result->query = " tbl_residentialSales.fld_availability != ? ";
					break;
			}
		}
		
		return $result;
	}
?>	