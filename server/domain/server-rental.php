<?php
	class jPListServer{
	
		/**
		* database instance
		*/
		protected $db;
		
		/**
		* sorting
		*/
		protected $sorting;
		
		/**
		* filter
		*/
		protected $filter;
		
		/**
		* pagination
		*/
		protected $pagination;
		
		/**
		* jplist statuses
		*/
		protected $statuses;
				
		/**
		* execute query and get data from db
		* @return {Object} data
		*/
		protected function getData(){
			
			$items = null;
			
			//init qury
			$query = "SELECT DISTINCT tbl_property.fld_propertyID, tbl_property.*, tbl_residential.fld_propertyBedrooms AS bedrooms, tbl_residential.fld_displayPropertyType AS type, tbl_residentialLet.*, tbl_residentialLetRentFrequency.fld_value AS frequency, tbl_residentialLetAvailability.fld_value AS availability FROM tbl_property 
			INNER JOIN tbl_residential ON tbl_property.fld_propertyID = tbl_residential.fld_propertyID AND tbl_property.fld_department = 'Lettings'
			INNER JOIN tbl_residentialLet ON tbl_property.fld_propertyID = tbl_residentialLet.fld_propertyID
			INNER JOIN tbl_residentialLetRentFrequency ON tbl_residentialLet.fld_rentFrequency = tbl_residentialLetRentFrequency.fld_counter
			INNER JOIN tbl_residentialLetAvailability ON tbl_residentialLet.fld_availability = tbl_residentialLetAvailability.fld_counter";

			if($this->filter->filterQuery){				
				$query .= " " . $this->filter->filterQuery . " ";
			}
            
			//get queries / fields				
			if($this->sorting->sortQuery){
				$query .= " " . $this->sorting->sortQuery . " ";
			}

		    //error_log(print_r($this->filter->filterQuery));

			if($this->pagination->paginationQuery){
				$query .= " " . $this->pagination->paginationQuery . " ";
			}
			
			if(count($this->filter->preparedParams) > 0){	
                
				$stmt = $this->db->prepare($query);
				$stmt->execute($this->filter->preparedParams);
				$items = $stmt->fetchAll();
			}
			else{
				$items = $this->db->query($query);
			}	

			return $items;
		}
		
		/**
		* constructor
		*/
		public function __construct(){
			
			try{
						
				//connect to database 
				$this->db = new PDO("mysql:host=" . BOOKHOST . ";dbname=" . BOOKDB, BOOKUSER, BOOKPASS);			
				$this->statuses = $_POST["statuses"];
				
				if(isset($this->statuses)){
					
					//statuses => array
					$this->statuses = json_decode(urldecode($this->statuses));	
					
					$this->sorting = new Sorting($this->statuses);
					$this->filter = new Filter($this->statuses);
					$this->pagination = new Pagination($this->statuses, $this->filter, $this->db);															
				}
			}
			catch(PDOException $ex){
				print "Exception: " . $ex->getMessage();
			}
		}
	}
?>	