<?php

//	**********************************************************************
//	* by Lee Watts                                                       *
//	* Email address: leewatts@millerwaite.co.uk                          *
//	*                                                                    *
//	* For: Croft Care Trust                                              *
//	*                                                                    *
//	* sitemap.php                                                        *
//	*--------------------------------------------------------------------*
//		$strthisver = "V1.0 - Jul 2016" 
//	**********************************************************************
//	XML sitemap for search engines auto updating from db.

	$strrootpath = $_SERVER['DOCUMENT_ROOT']."/";
	include($strrootpath."includes/incsitecommon.php");
	header("Content-type: text/xml");

	print("<?xml version='1.0'?>\n");
	print("<urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9' xmlns:xhtml='http://www.w3.org/1999/xhtml'>");
	
	// Start connection
	$conn = connect();
	$bookConn = bookConnect();

	// Site pages
	$strdbsql = "SELECT * FROM site_pages WHERE visible = 1";
	$arrType = "multi";
	$resultdata = query($conn, $strdbsql, $arrType);

	if (count($resultdata) != 0)
	{
		foreach($resultdata AS $sitePage) {
			
			if ($sitePage['pageName'] != "" && $sitePage['pageName'] != "#callback-form") {
				print("<url>\n");
				print("<loc>".$strsiteurl."".($sitePage['pageName'] == "index" ? "" : $sitePage['pageName'])."</loc>\n");
				print("<changefreq>monthly</changefreq>\n");
				print("<priority>0.3</priority>");
				print("</url>\n");
				print("\n");
			}

		}
	}
	
	// Properties for Sale
	$strdbsql = "SELECT * FROM tbl_property
	INNER JOIN tbl_residential ON tbl_property.fld_propertyID = tbl_residential.fld_propertyID AND tbl_property.fld_department = 'Sales'
	INNER JOIN tbl_residentialSales ON tbl_property.fld_propertyID = tbl_residentialSales.fld_propertyID AND tbl_property.fld_department = 'Sales'
	ORDER BY tbl_property.fld_propertyID";
	$arrType = "multi";
	$resultdata = query($bookConn, $strdbsql, $arrType);
	
	if(count($resultdata) != 0)
	{
		foreach($resultdata AS $propPage) {
			
			if ($propPage['fld_propertyID'] != "") {
				print("<url>\n");
				print("<loc>".$strsiteurl."sales/".$propPage['fld_propertyID']."</loc>\n");
				print("<changefreq>monthly</changefreq>\n");
				print("<priority>0.3</priority>");
				print("</url>\n");
				print("\n");
			}

		}
	}
	
	// Properties for Rental
	$strdbsql = "SELECT * FROM tbl_property
	INNER JOIN tbl_residential ON tbl_property.fld_propertyID = tbl_residential.fld_propertyID AND tbl_property.fld_department = 'Lettings'
	ORDER BY tbl_property.fld_propertyID";
	$arrType = "multi";
	$resultdata = query($bookConn, $strdbsql, $arrType);
	
	if(count($resultdata) != 0)
	{
		foreach($resultdata AS $propPage) {
			
			if ($propPage['fld_propertyID'] != "") {
				print("<url>\n");
				print("<loc>".$strsiteurl."rental/".$propPage['fld_propertyID']."</loc>\n");
				print("<changefreq>monthly</changefreq>\n");
				print("<priority>0.3</priority>");
				print("</url>\n");
				print("\n");
			}

		}
	}
	
	// Properties for Commercial Sale
	$strdbsql = "SELECT * FROM tbl_property
	INNER JOIN tbl_commercial ON tbl_property.fld_propertyID = tbl_commercial.fld_propertyID AND tbl_property.fld_department = 'Commercial'
	ORDER BY tbl_property.fld_propertyID";
	$arrType = "multi";
	$resultdata = query($bookConn, $strdbsql, $arrType);
	
	if(count($resultdata) != 0)
	{
		foreach($resultdata AS $propPage) {
			
			if ($propPage['fld_propertyID'] != "") {
				print("<url>\n");
				print("<loc>".$strsiteurl."commercial/".$propPage['fld_propertyID']."</loc>\n");
				print("<changefreq>monthly</changefreq>\n");
				print("<priority>0.3</priority>");
				print("</url>\n");
				print("\n");
			}

		}
	}
	
	$conn = null

?>
</urlset>
