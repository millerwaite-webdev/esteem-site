<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * order-menu.php                                    * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "order-menu"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['categoryID'])) $categoryID = $_REQUEST['categoryID']; else $categoryID = "";
	if (isset($_REQUEST['brandID'])) $brandID = $_REQUEST['brandID']; else $brandID = "";
	if (isset($_REQUEST['rangeID'])) $rangeID = $_REQUEST['rangeID']; else $rangeID = "";
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Order Menu</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function sortableBehaviour(className) {
					//console.log($("."+className).children("li").children("span"));
					$("."+className).children("li").children("span").click(function (e) {
						//console.log("test");
						if (e.ctrlKey || e.metaKey) {
							$(this).parent().toggleClass("selected");
						} else {
							$(this).parentsUntil("#form").find('.selected').removeClass('selected');
							$(this).parent().addClass("selected");
							$("#sortableSubCategories").children().removeClass('selected');
						}
					});
					$("."+className).sortable({
						connectWith: ['#sortableSubCategories'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							 //Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						receive: function(e,ui) {
							//copyHelper= null;
							$(ui.item.data('multidrag')).each(function(index, item) {
								$(item).prepend("<span><span class='expand_collapse'>+</span> ");
								$(item).append("</span><ul class='sortableCategoryList' style='padding-left: 20px; display: none; border-left: 1px dotted; min-height: 17px;'></ul>");
								//console.log("Length is: "+$(ui.item).parent().parent("[data-level]").length);
								if ($(ui.item).parent().parent("[data-level]").length) {
									//console.log(parseInt($(ui.item).parent().parent().attr("data-level")) + 1);
									$(item).attr("data-level", parseInt($(ui.item).parent().parent().attr("data-level")) + 1);
								} else {
									$(item).attr("data-level", 1);
								}
								//console.log(item);
								$(item).parent().children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								$(item).attr("data-parentid", $(item).parent().parent().attr('id'));
								$(item).find(".expand_collapse").click(function() {
									expandCollapse(this);
								});
								sortableBehaviour(className);
							});
						},
						stop: function( event, ui ) {
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul.sortableCategoryList"));
								if ($(item).parent().is("ul.sortableCategoryList")) {
									$(item).find(".expand_collapse").click(function() {
										expandCollapse(this);
									});
									//console.log($(item).find("ul.sortableCategoryList").children());
									$(item).find("ul.sortableCategoryList").find("span").click(function (e) {
										//console.log("test");
										if (e.ctrlKey || e.metaKey) {
											$(this).parent().toggleClass("selected");
										} else {
											$(this).parentsUntil("#form").find('.selected').removeClass('selected');
											$(this).parent().addClass("selected");
											$("#sortableSubCategories").children().removeClass('selected');
										}
									});
									var oldOrder = $(item).attr("data-order");
									$(item).parent().children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									var newOrder = $(item).attr("data-order");
									$(item).attr("data-parentid", $(item).parent().parent().attr('id'));
									
									//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
									if (parseInt(newOrder) > parseInt(oldOrder))
									{
										sortType = "DESC";
									}
									else if (parseInt(newOrder) < parseInt(oldOrder))
									{
										sortType = "ASC";
									}
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["childID"] = $(item).attr('id');
									dataArray[i]["parentID"] = $(item).attr('data-parentid');
									dataArray[i]["level"] = $(item).attr('data-level');
									dataArray[i]["cmd"] = "reorder";
									//console.log(dataArray);
								} else if ($(item).parent().is("#sortableSubCategories")) {
									//console.log(item);
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr('data-order');
									dataArray[i]["childID"] = $(item).attr('id');
									dataArray[i]["parentID"] = $(item).attr('data-parentid');
									dataArray[i]["level"] = $(item).attr('data-level');
									dataArray[i]["cmd"] = "delete";
									sortableBehaviour("sortableCategoryList");
									//console.log(dataArray);
									if ($(item).attr('data-level') == 1) {
										if ($("#"+$(item).attr('data-parentid')+" > ul.sortableCategoryList > li[data-level = 1][id != '"+$(item).attr('id')+"'][data-order != '"+parseInt($(item).attr("data-order"))+"']").parent().length == 1) {
											$("#"+$(item).attr('data-parentid')+" > ul.sortableCategoryList > li[data-level = 1][id != '"+$(item).attr('id')+"'][data-order != '"+parseInt($(item).attr("data-order"))+"']").each(function(index, item) {
												$(item).attr("data-order", $(item).index() + 1);
											});
										}
									} else {
										if ($("li#"+$(item).attr('data-parentid')+" > ul.sortableCategoryList > li[data-level = '"+parseInt($(item).attr('data-level'))+"'][id != '"+$(item).attr('id')+"'][data-order != '"+parseInt($(item).attr("data-order"))+"']").parent().length == 1) {
											$("li#"+$(item).attr('data-parentid')+" > ul.sortableCategoryList > li[data-level = '"+parseInt($(item).attr('data-level'))+"'][id != '"+$(item).attr('id')+"'][data-order != '"+parseInt($(item).attr("data-order"))+"']").each(function(index, item) {
												$(item).attr("data-order", $(item).index() + 1);
											});
										}
									}
									//console.log($("li#"+$(item).attr('data-parentid')+" > ul.sortableCategoryList > li[data-level = '"+parseInt($(item).attr('data-level'))+"'][id != '"+$(item).attr('id')+"'][data-order != '"+parseInt($(item).attr("data-order"))+"']").parent().length);
									//console.log($("#"+$(item).attr('data-parentid')+" > ul.sortableCategoryList > li[data-level = 1][id != '"+$(item).attr('data-parentid')+"'][data-order != '"+parseInt($(item).attr("data-order"))+"']"));
									//console.log($(item).parent().find("li#"+$(item).attr('id')).size());
									if ($(item).parent().find("li#"+$(item).attr('id')).size() > 1) {
										$(item).remove();
									} else {
										$(item).find(".expand_collapse").remove();
										$(item).find(".sortableCategoryList").remove();
										$(item).removeAttr("data-level");
										$(item).removeAttr("data-order");
										$(item).removeAttr("data-parentid");
										//console.log($.trim($(item).children().first().text()));
										$.trim($(item).children().first());
										//console.log(item);
									}
								}
								//console.log($(item).find("ul").attr('class'));
								sortableBehaviour(className);
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									console.log("sort ascending");
									return parseInt(a.order) - parseInt(b.order);
								}
								else if (sortType == "DESC")
								{
									console.log("sort descending");
									return parseInt(b.order) - parseInt(a.order);
								}
							});
							console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_categorystructure.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				}
				function expandCollapse(control) {
					$(control).parent().siblings("ul").toggle("blind", 500, function() {
						if ($(control).parent().siblings("ul").is(':visible'))
						{
							$(control).html("-");
						}
						else
						{
							$(control).html("+");
						}
					});
				}
				$().ready(function() {
					sortableBehaviour('sortableCategoryList');
					
					$("#sortableSubCategories").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$(".sortableCategoryList").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['.sortableSubcategoryList'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						forcePlaceholderSize: false,
						helper: function (e, item) {
							
							 //Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							var selected = $("#sortableSubCategories li.selected");
							
							$(selected).each(function(index, item) {
								$(item).attr("data-orig-index", $(item).parent().children().index(item));
							});
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						remove: function (e, ui) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							
							$(elements).each(function(index, item) {
								$("#sortableSubCategories li:eq("+parseInt($(item).attr("data-orig-index"))+")").before($(item).clone().removeAttr("data-orig-index").removeClass("selected"));
								$(item).removeAttr("data-orig-index");
							});
						},
						stop: function (e, ui) {
							
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							
							//`elements` now contains the originally selected items from the source list (the dragged items)!!
							
							//Finally I insert the selected items after the `item`, then remove the `item`, since 
							//  item is a duplicate of one of the selected items.
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							$(elements).each(function(index, item) {
								//console.log(item);
								if ($(item).parent().is("ul.sortableCategoryList")) {
									$(item).parent().children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									$(item).attr("data-parentid", $(item).parent().parent().attr('id'));
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr('data-order');
									dataArray[i]["childID"] = $(item).attr('id');
									dataArray[i]["parentID"] = $(item).attr('data-parentid');
									dataArray[i]["level"] = $(item).attr('data-level');
									dataArray[i]["cmd"] = "add";
									sortableBehaviour("sortableCategoryList");
									i++;
								}
							});
							//console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_categorystructure.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();

					$("#assignedBrands").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#unassignedBrands").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#unassignedBrands'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								
								if ($(item).parent().is("ul#assignedBrands")) {
									var oldOrder = $(item).attr("data-order");
									$("ul#assignedBrands").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									var newOrder = $(item).attr("data-order");
									//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
									if (parseInt(newOrder) > parseInt(oldOrder))
									{
										sortType = "DESC";
									}
									else if (parseInt(newOrder) < parseInt(oldOrder))
									{
										sortType = "ASC";
									}
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["brandID"] = $(item).attr('id');
									dataArray[i]["categoryID"] = $("#categoryID").attr("value");
									dataArray[i]["cmd"] = "reorder";
									//console.log(dataArray);$(item).find(".expand_collapse").remove();
								} else if ($(item).parent().is("ul#unassignedBrands")) {
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["brandID"] = $(item).attr('id');
									dataArray[i]["categoryID"] = $("#categoryID").attr("value");
									dataArray[i]["cmd"] = "delete";
									//console.log(dataArray);$(item).find(".expand_collapse").remove();
									
									
									$(item).removeAttr("data-order");
									//console.log($.trim($(item).children().first().text()));
									$.trim($(item).children().first());
									//console.log(item);
									$("ul#assignedBrands").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
								}
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									console.log("sort ascending");
									return parseInt(a.order) - parseInt(b.order);
								}
								else if (sortType == "DESC")
								{
									console.log("sort descending");
									return parseInt(b.order) - parseInt(a.order);
								}
							});
							console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_categorybrandmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();

					$("#unassignedBrands").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#assignedBrands").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#assignedBrands'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function(event, ui) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							$(elements).each(function(index, item) {
								if ($(item).parent().is("ul#assignedBrands")) {
									$(item).parent().children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["brandID"] = $(item).attr('id');
									dataArray[i]["categoryID"] = $("#categoryID").attr("value");
									dataArray[i]["cmd"] = "add";
								}
								i++;
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_categorybrandmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();

					$("#assignedProducts").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#unassignedProducts").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#unassignedProducts'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								
								if ($(item).parent().is("ul#assignedProducts")) {
									var oldOrder = $(item).attr("data-order");
									$("ul#assignedProducts").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									var newOrder = $(item).attr("data-order");
									//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
									if (parseInt(newOrder) > parseInt(oldOrder))
									{
										sortType = "DESC";
									}
									else if (parseInt(newOrder) < parseInt(oldOrder))
									{
										sortType = "ASC";
									}
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["stockID"] = $(item).attr('id');
									dataArray[i]["categoryID"] = $("#categoryID").attr("value");
									dataArray[i]["cmd"] = "reorder";
									//console.log(dataArray);$(item).find(".expand_collapse").remove();
								} else if ($(item).parent().is("ul#unassignedProducts")) {
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["stockID"] = $(item).attr('id');
									dataArray[i]["categoryID"] = $("#categoryID").attr("value");
									dataArray[i]["cmd"] = "delete";
									//console.log(dataArray);$(item).find(".expand_collapse").remove();
									
									
									$(item).removeAttr("data-order");
									//console.log($.trim($(item).children().first().text()));
									$.trim($(item).children().first());
									//console.log(item);
									$("ul#assignedProducts").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
								}
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									console.log("sort ascending");
									return parseInt(a.order) - parseInt(b.order);
								}
								else if (sortType == "DESC")
								{
									console.log("sort descending");
									return parseInt(b.order) - parseInt(a.order);
								}
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_categoryproductmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();

					$("#unassignedProducts").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#assignedProducts").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#assignedProducts'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function(event, ui) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							$(elements).each(function(index, item) {
								if ($(item).parent().is("ul#assignedProducts")) {
									$(item).parent().children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["stockID"] = $(item).attr('id');
									dataArray[i]["categoryID"] = $("#categoryID").attr("value");
									dataArray[i]["cmd"] = "add";
								}
								i++;
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_categoryproductmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
					
					$(".expand_collapse").click(function() {
						expandCollapse(this);
					});
					
					$(".blockPosList").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
						$(this).parent().parent().siblings().children("ul").children().removeClass('selected');
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							//console.log(item);
							
							//Clone the selected items into an array
							//console.log(item.parent().children('.selected'));
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								//console.log($(item));
								//console.log($(item).attr('data-order'));
								var oldOrder = $(item).attr("data-order");
								
								$(item).parent().children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if (parseInt(newOrder) > parseInt(oldOrder))
								{
									sortType = "DESC";
								}
								else if (parseInt(newOrder) < parseInt(oldOrder))
								{
									sortType = "ASC";
								}
								
								dataArray[i] = {};
								dataArray[i]["pageOrder"] = $(item).attr("data-order");
								dataArray[i]["positionID"] = $(item).attr("data-pos");
								dataArray[i]["recordID"] = $(item).attr("id");
								dataArray[i]["sitePageID"] = $("#categoryID").attr("value");
								dataArray[i]["type"] = "category";
								dataArray[i]["cmd"] = "reorder";
								//console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									//console.log("sort ascending");
									return parseInt(a.pageOrder) - parseInt(b.pageOrder);
								}
								else if (sortType == "DESC")
								{
									//console.log("sort descending");
									return parseInt(b.pageOrder) - parseInt(a.pageOrder);
								}
							});
							//console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_siteblockrelationmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
			</script>
			<?php
		
			print ("<form action='order-menu.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				print ("<input type='hidden' name='categoryID' id='categoryID' value='$categoryID'/>");
				print ("<input type='hidden' name='blockPositionID' id='blockPositionID' />");
				
				print("<div class='row'>");
					print("<div class='col-lg-3'>");
				
						//allow options to select the category / brand / range products
						print ("<ul class='sortableCategoryList'>");
							
							//This needs to be drag and drop to change the order
							
							$parentsQuery = "SELECT metaPageLink FROM site_pages WHERE displayInNav = 1";
							$strType = "multi";
							$parents = query($conn, $parentsQuery, $strType);
							$level = 1;
							
							$childrenQuery = "SELECT metaPageLink FROM site_pages WHERE displayInNav = 0";
							$strType = "multi";
							$children = query($conn, $childrenQuery, $strType);
							
							if (count($parents) != 0)
							{
								$i = 1;
								foreach ($parents as $parent) {						
									print ("<li id='".$parent['recordID']."' data-level='".$level."' data-order='".$i."' data-parentid='form'>");
									
										print ("<span class='section'><span class='expand_collapse'>+</span>".$parent['metaPageLink']."</span>");
										print ("<ul class='sortableCategoryList sortableSubcategoryList' style='padding-left: 20px; display: none; border-left-width: 1px; border-left-style: dotted; min-height: 17px;'>");
											if ($resultdata['num_nested'] > 0)
											{
												getSubCategories($row['recordID'], $level, $parents, $conn);
											}
										print ("</ul>");
									print ("</li>");
									$i++;
								}
							} else {
								//print ("<tr><td colspan='7'>Currently No Categories</td></tr>");
								print ("<li>Currently No Categories</li>");
							}
							
						print ("</ul>");
						
						$strdbsql = "SELECT category.* FROM category WHERE recordID NOT IN (SELECT DISTINCT parentID FROM category_relations WHERE parentID = childID) ORDER BY metaPageLink ASC";
						$strType = "multi";
						$resultdata = query($conn, $strdbsql, $strType);
						
						if (count($resultdata) != 0)
						{
							print ("<br/>");
							print ("<h3>Subcategories</h3>");
							
							print ("<ul id='sortableSubCategories'>");
							
							foreach ($resultdata as $row) {
								print ("<li id='".$row['recordID']."'><span>".$row['contentName']." (".$row['metaPageLink'].") <a href='#' onclick='jsviewCategory(".$row['recordID']."); return false;'>Edit</a></span></li>");
							}
							
							print ("</ul>");
						}
						
					print("</div>");
				print("</div>");
						
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>