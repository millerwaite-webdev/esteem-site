<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * catalogue-attributes.php	                                       * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "catalogue-attributes"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['attributeTypeID'])) $attributeTypeID = $_REQUEST['attributeTypeID']; else $attributeTypeID = "";
	if (isset($_REQUEST['attributeID'])) $attributeID = $_REQUEST['attributeID']; else $attributeID = "";
	
	switch($strcmd)
	{
		case "insertAttributeType":
		case "updateAttributeType":
		
			$arrdbparams = array(
								"description" => $_POST['frm_typedescription']
							);
			if ($strcmd == "insertAttributeType")
			{
				$getMaxTypeOrderQuery = "SELECT COUNT(*) AS maxTypeOrder FROM stock_attribute_type";
				$strType = "single";
				$maxTypeOrder = query($conn, $getMaxTypeOrderQuery, $strType);
				
				$strdbsql = "INSERT INTO stock_attribute_type (description, attributeTypeOrder) VALUES (:description, :attributeTypeOrder)";
				$strType = "insert";
				$arrdbparams['attributeTypeOrder'] = intval($maxTypeOrder['maxTypeOrder'] + 1);
			}
			elseif ($strcmd == "updateAttributeType")
			{
				$strdbsql = "UPDATE stock_attribute_type SET description = :description WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $attributeTypeID;
				$strType = "update";
			}
			
			$updateAttributeType = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertAttributeType")
			{
				$attributeTypeID = $updateAttributeType;
			}
			
			if ($strcmd == "insertAttributeType")
			{
				if ($updateAttributeType > 0)
				{
					$strsuccess = "Attribute Type successfully added";
				}
				elseif ($updateAttributeType == 0)
				{
					$strerror = "An error occurred while adding the attribute type";
				}
			}
			elseif ($strcmd == "updateAttributeType")
			{
				if ($updateAttributeType <= 1)
				{
					$strsuccess = "Attribute type successfully updated";
				}
				elseif ($updateAttributeType > 1)
				{
					$strwarning = "An error may have occurred while updating this attribute type";
				}
			}
			
			$strcmd = "viewAttributeType";
			
		break;
		
		case "insertAttribute":
		case "updateAttribute":
		
			$arrdbparams = array(
								"description" => $_POST['frm_attributedescription'],
								"swatchImage" => $_POST['frm_swatchimage'],
								"attributeTypeID" => $attributeTypeID
							);
			if ($attributeTypeID == 3)
			{
				$arrdbparams['shortCode'] = $_POST['frm_shortcode'];
				$arrdbparams['hexCode'] = $_POST['frm_hexcode'];
			}
			if ($strcmd == "insertAttribute")
			{	
				$strdbsql = "INSERT INTO stock_attributes (description, swatchImage, attributeTypeID";
				
				if ($attributeTypeID == 3)
				{
					$strdbsql .= ", shortCode, hexCode";
				}
				
				$strdbsql .= ") VALUES (:description, :swatchImage, :attributeTypeID";
				
				if ($attributeTypeID == 3)
				{
					$strdbsql .= ", :shortCode, :hexCode";
				}
				
				$strdbsql .= ")";
				$strType = "insert";
			}
			elseif ($strcmd == "updateAttribute")
			{
				$strdbsql = "UPDATE stock_attributes SET description = :description, swatchImage = :swatchImage";
				
				if ($attributeTypeID == 3)
				{
					$strdbsql .= ", shortCode = :shortCode, hexCode = :hexCode";
				}
				
				$strdbsql .= " WHERE recordID = :recordID AND attributeTypeID = :attributeTypeID";
				
				$arrdbparams['recordID'] = $attributeID;
				$strType = "update";
			}
			
			$updateAttribute = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertAttribute")
			{
				$attributeID = $updateAttribute;
			}
			
			if ($strcmd == "insertAttribute")
			{
				if ($updateAttribute > 0)
				{
					$strsuccess = "Attribute successfully added";
				}
				elseif ($updateAttribute == 0)
				{
					$strerror = "An error occurred while adding the attribute";
				}
			}
			elseif ($strcmd == "updateAttribute")
			{
				if ($updateAttribute <= 1)
				{
					$strsuccess = "Attribute successfully updated";
				}
				elseif ($updateAttribute > 1)
				{
					$strwarning = "An error may have occurred while updating this attribute";
				}
			}
			
			$strcmd = "viewAttribute";
		
		break;
		
		case "insertImage":
			$newfilename = $_FILES['frm_image']['name'];
			$strfileextn = getExtension($newfilename);
			
			$strimguploadpath = "images/";
			$struploaddir = $strrootpath.$strimguploadpath; // where to put full size image
			$upfile = $struploaddir.$newfilename;
			
			//        print ("$upfile - saving image<br>");

			if ($_FILES['frm_image']['error'] > 0)
			{
				switch ($_FILES['frm_image']['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				//   print ("$strfileextn - checking extensions<br>");
				// Does the file have the right extension ?
				//if ($strfileextn != 'jpg' && $strfileextn != 'jpeg' && $strfileextn != 'png' && $strfileextn != 'gif')
				if (strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0)
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_image']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
				}
				else
				{
					//      print ("Uploading to products image directory<br>");
					if (is_uploaded_file($_FILES['frm_image']['tmp_name']))
					{
						if (!move_uploaded_file($_FILES['frm_image']['tmp_name'], $upfile))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_image']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_image']['name'].". Please try again.<br/>";
					}
				}
			}
			
			if ($strerror != "")
			{
				$strcommand = "ERRORMSG";
				//   print ("Got an Error - $strcommand - $strerror<br>");
			}
			else
			{
				$strbrandpath = $strimguploadpath."swatches/";
				
				$strbranddir = $strrootpath.$strbrandpath;
				
				$newfile = $strbranddir.$newfilename;
				$range=copy($upfile,$newfile);
				chmod ($newfile, 0777);
				
				
				unlink($upfile);
				
				if (isset($_REQUEST['frm_replacecurrent'])) $replaceCurrent = $_REQUEST['frm_replacecurrent']; else $replaceCurrent = 0;
				
				if($replaceCurrent)
				{
					$updateAttributeImageQuery = "UPDATE stock_attributes SET swatchImage = :swatchImage WHERE recordID = :recordID";
					$strType = "update";
					$arrdbparams = array (
									"swatchImage" => $newfilename,
									"recordID" => $attributeID
								);
					$updateBrandImage = query($conn, $updateAttributeImageQuery, $strType, $arrdbparams);
				}
			}
			
			$strsuccess = "New swatch image added";
			
			$strcmd = "viewAttribute";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Attributes Control</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddAttributeType() {
					document.form.cmd.value='addAttributeType';
					document.form.submit();
				}
				function jsinsertAttributeType() {
					document.form.cmd.value='insertAttributeType';
					document.form.submit();
				}
				function jsviewAttributeType(attributeTypeID) {
					if ($('#form').valid()) {
						document.form.cmd.value='viewAttributeType';
						document.form.attributeTypeID.value=attributeTypeID;
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsupdateAttributeType() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateAttributeType';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsaddAttribute() {
					document.form.cmd.value='addAttribute';
					document.form.submit();
				}
				function jsinsertAttribute() {
					document.form.cmd.value='insertAttribute';
					document.form.submit();
				}
				function jsviewAttribute(attributeID) {
					if ($('#form').valid()) {
						document.form.cmd.value='viewAttribute';
						document.form.attributeID.value=attributeID;
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsupdateAttribute() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateAttribute';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertImage() {
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				
				$().ready(function() {
					$("#attributeTypes").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								
								var oldOrder = $(item).attr("data-order");
								$("ul#attributeTypes").children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if (parseInt(newOrder) > parseInt(oldOrder))
								{
									sortType = "DESC";
								}
								else if (parseInt(newOrder) < parseInt(oldOrder))
								{
									sortType = "ASC";
								}
								
								dataArray[i] = {};
								dataArray[i]["typeID"] = $(item).attr("id");
								dataArray[i]["order"] = $(item).attr("data-order");
								dataArray[i]["cmd"] = "reorder";
								console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									console.log("sort ascending");
									return parseInt(a.order) - parseInt(b.order);
								}
								else if (sortType == "DESC")
								{
									console.log("sort descending");
									return parseInt(b.order) - parseInt(a.order);
								}
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_attributetypemanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
			</script>
			<?php
		
			print ("<form action='catalogue-attributes.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				print ("<input type='hidden' name='attributeTypeID' id='attributeTypeID' value='$attributeTypeID'/>");
				print ("<input type='hidden' name='attributeID' id='attributeID' value='$attributeID'/>");
				
				switch($strcmd)
				{
					case "viewAttributeType":
					case "addAttributeType":
						
						if ($strcmd == "viewAttributeType")
						{
							$strdbsql = "SELECT * FROM stock_attribute_type WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $attributeTypeID);
							$attributeTypeDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							
							/*print ("<pre>");
							print_r($attributeTypeDetails);
							print ("</pre>");&*/
							
							print ("<fieldset class='inlineLabels'> <legend>Change Attribute Type Details</legend>");
						}
						elseif ($strcmd == "addAttributeType")
						{
							$attributeTypeDetails = array( "description" => "" );
							
							print ("<fieldset class='inlineLabels'> <legend>Add Attribute Type Details</legend>");
						}
							
								print ("<div class='form-group'>
										<label for='frm_typedescription' class='col-sm-2 control-label'>Description</label>
										<div class='col-sm-10'>
										  <input style='width:200px;' type='text' class='form-control' id='frm_typedescription' name='frm_typedescription' value='".$attributeTypeDetails['description']."'>
										</div>
									  </div>");
								print ("<div class='form-group'>
											<div class='col-sm-10'>");
											if ($strcmd == "addAttributeType")
											{
											  print ("<button onclick='return jsinsertAttributeType();' type='submit' class='btn btn-success'>Update Attribute Type</button> ");
											}
											elseif ($strcmd == "viewAttributeType")
											{
											  print ("<button onclick='return jsupdateAttributeType();' type='submit' class='btn btn-success'>Update Attribute Type</button> ");
											}
											print  ("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>
											</div>
										</div>");
							print ("</fieldset>");
						if ($strcmd == "viewAttributeType")
						{
							print ("<br/>");
							print ("<br/>");
							
							$strdbsql = "SELECT * FROM stock_attributes WHERE attributeTypeID = :attributeTypeID";
							$strType = "multi";
							$arrdbparams = array("attributeTypeID" => $attributeTypeID);
							$attributes = query($conn, $strdbsql, $strType, $arrdbparams);
							
							print ("<fieldset class='inlineLabels'> <legend>Add Attribute</legend>");
								print ("<button onclick='return jsaddAttribute();' type='submit' class='btn btn-success'>Add New</button>");
								print ("<br/>");
								print ("<br/>");
								print ("<table class='table table-striped table-bordered table-hover table-condensed' id='attributes-table'>");
									print ("<thead>");
										print ("<th>Attribute</th>");
										print ("<th></th>");
									print ("</thead>");
									print ("<tbody>");
									if (count($attributes) > 0)
									{
										foreach($attributes AS $attribute)
										{
											print ("<tr>");
												print ("<td>".$attribute['description']."</td>");
												print ("<td style='width: 165px;'><button onclick='return jsviewAttribute(\"".$attribute['recordID']."\");' type='submit' class='btn btn-primary'>Update Attribute</button></td>");
											print ("</tr>");
										}
									}
									print ("</tbody>");
								print ("</table>");
								
								print ("<button onclick='return jsaddAttribute();' type='submit' class='btn btn-success'>Add New</button>");
							print ("</fieldset>");
						}
					
						break;
						
					case "viewAttribute":
					case "addAttribute":
						
						if ($strcmd == "viewAttribute")
						{
							$strdbsql = "SELECT * FROM stock_attributes WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $attributeID);
							$attributeDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							
							/*print ("<pre>");
							print_r($attributeDetails);
							print ("</pre>");&*/
							
							print ("<fieldset class='inlineLabels'> <legend>Change Attribute Details</legend>");
						}
						elseif ($strcmd == "addAttribute")
						{
							$attributeDetails = array(
													"description" => "",
													"swatchImage" => "",
													"shortCode" => "",
													"hexCode" => ""
												);
							
							print ("<fieldset class='inlineLabels'> <legend>Add Attribute Details</legend>");
						}
							
								print ("<div class='form-group'>
										<label for='frm_attributedescription' class='col-sm-2 control-label'>Description</label>
										<div class='col-sm-10'>
										  <input style='width:200px;' type='text' class='form-control' id='frm_attributedescription' name='frm_attributedescription' value='".$attributeDetails['description']."'>
										</div>
									  </div>");
								if ($attributeTypeID == 3)
								{
								print ("<div class='form-group'>
										<label for='frm_shortcode' class='col-sm-2 control-label'>Short Code</label>
										<div class='col-sm-10'>
										  <input style='width:200px;' type='text' class='form-control' id='frm_shortcode' name='frm_shortcode' value='".$attributeDetails['shortCode']."'>
										</div>
									  </div>
									  <div class='form-group'>
										<label for='frm_hexcode' class='col-sm-2 control-label'>Hex Code</label>
										<div class='col-sm-10'>
										  <input style='width:200px;' type='text' class='form-control' id='frm_hexcode' name='frm_hexcode' value='".$attributeDetails['hexCode']."'>
										</div>
									  </div>");
								}
								
								print ("<div class='form-group'>
									<label for='frm_swatchimage' class='col-sm-2 control-label'>Swatch Image</label>
									<div class='col-sm-10'>
									  <select name='frm_swatchimage' id='frm_swatchimage' class='form-control valid' style='width: 300px;'>
										<option value=''>None</option>");
										$dh = opendir("../images/swatches/");
										while (false !== ($image = readdir($dh))) {
											if ($image != "." && $image != "..") {
												if($image == $attributeDetails['swatchImage'])
												{
													$strselected = " selected";
												}
												else
												{
													$strselected = "";
												}
												print ("<option value='".$image."'".$strselected.">".$image."</option>");
											}
										}
								print ("</select>
									</div>
								</div>");
						print ("<div class='form-group'>
									<div class='col-sm-10'>");
									if ($strcmd == "addAttribute")
									{
									  print ("<button onclick='return jsinsertAttribute();' type='submit' class='btn btn-success'>Update Attribute</button> ");
									}
									elseif ($strcmd == "viewAttribute")
									{
									  print ("<button onclick='return jsupdateAttribute();' type='submit' class='btn btn-success'>Update Attribute</button> ");
									}
									print  ("<button onclick='return jscancel(\"viewAttributeType\");' class='btn btn-danger'>Cancel</button>
									</div>
								</div>");
								
								if ($strcmd == "viewAttribute")
								{
									print("<br/>");
									print("<legend>Add New Image</legend>");
									
									print ("<div class='form-group'>
												<label for='frm_image' class='col-sm-2 control-label'>Image</label>
												<div class='col-sm-10'>
												  <input style='width:600px;' type='file' class='form-control' id='frm_image' name='frm_image' />
												</div>
											</div>
											<div class='form-group'>
												<label for='frm_replacecurrent' class='col-sm-2 control-label'>Replace current swatch image</label>
												<div class='col-sm-1'>");
													print ("<input type='checkbox' id='frm_replacecurrent' name='frm_replacecurrent' value='1' />
												</div>
											</div>
											<div class='form-group'>
												<div class='col-sm-offset-2 col-sm-10'>");
													print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success'>Insert Image</button>
												</div>
											</div>");
								}
							print ("</fieldset>");
						
						break;
						
					default:
						
						$strdbsql = "SELECT * FROM stock_attribute_type ORDER BY attributeTypeOrder ASC";
						$strType = "multi";
						$attributeTypes = query($conn, $strdbsql, $strType);
						
						print ("<button onclick='return jsaddAttributeType();' type='submit' class='btn btn-success'>Add New</button>");
						print ("<br/>");
						print ("<br/>");
						if (count($attributeTypes) > 0)
						{
							print("<ul id='attributeTypes'>");
							foreach($attributeTypes AS $attributeType)
							{
								print ("<li id='".$attributeType['recordID']."' data-order='".$attributeType['attributeTypeOrder']."'>");
									print ("<span>".$attributeType['description']." <a href='#' onclick='return jsviewAttributeType(\"".$attributeType['recordID']."\"); return false;'>Update Attribute Type</a></span>");
								print ("</li>");
							}
							print ("</ul>");
						}
						else
						{
							print ("No attribute types were found");
						}
						
						print ("<br/>");
						
						print ("<button onclick='return jsaddAttributeType();' type='submit' class='btn btn-success'>Add New</button>");
						
						break;
				}
				
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#attributes-table').DataTable();
		});
	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>
