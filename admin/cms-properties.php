<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * cms-properties.php					                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

	// ************* Common page setup ******************** //
	//=====================================================//

	// use function inc\connect;
	// use function inc\query;

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "cms-properties"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	//details submitted
	if(isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if(isset($_REQUEST['pageRecordID'])) $pageRecordID = $_REQUEST['pageRecordID']; else $pageRecordID = "";
	if(isset($_REQUEST['deleteImages'])) $deleteImages = $_REQUEST['deleteImages']; else $deleteImages = "";
	if(isset($_REQUEST['pageRSID'])) $pageRSID = $_REQUEST['pageRSID']; else $pageRSID = "";
	if(isset($_REQUEST['cmn-visible'])) $visible = "yes"; else $visible = "";
	if(isset($_REQUEST['cmn-status'])) $status = "Under offer"; else $status = "";
	if(isset($_REQUEST['cmn-SSTC'])) $status = "SSTC"; else $status = $status;
	if(isset($_REQUEST['cmn-sold'])) $status = "Sold"; else $status = $status;
	
	
	switch($strcmd)
	{
		case "insertPage":
		case "updatePage":
			
			// Get total images assigned to the same property
			if($strcmd == "insertPage") {	
				$strdbsql = "SELECT rs_id FROM properties ORDER BY rs_id DESC LIMIT 1";
				$lastProp = query($conn, $strdbsql, "single", null);
				$propLast = $lastProp['rs_id'] + 1;
			}
			
			// Get params for page create/update
			$arrdbparams = array(
				"propname" => $_POST['frm_propname'],
				"propaddress" => $_POST['frm_propaddress'],
				"proppostcode" => $_POST['frm_proppostcode'],
				"proptype" => $_POST['frm_proptype'],
				"propprice" => $_POST['frm_propprice'],
				"proptenure" => $_POST['frm_proptenure'],
				"propbeds" => $_POST['frm_propbeds'],
				"propbaths" => $_POST['frm_propbaths'],
				"metatitle" => $_POST['frm_metatitle'],
				"metadescription" => $_POST['frm_metadescription'],
				"propshortdesc" => $_POST['frm_propshortdesc'],
				"proplongdesc" => $_POST['frm_proplongdesc'],
				"property_location_id" => $_POST['frm_property_location_id'],
				"displayOrder" => $_POST['frm_displayOrder'],
				"visible" => $visible,
				"status" => $status
			);
			
			// Get query for page create/update
			if($strcmd == "insertPage") {	
				$strdbsql = "INSERT INTO properties (p_name, p_area, postcode, prop_type, prop_price, prop_tenure, No_beds, No_baths, metatitle, metadescription, short_description, long_description, make_live, sold, rs_id, property_location_id, displayOrder) VALUES (:propname, :propaddress, :proppostcode, :proptype, :propprice, :proptenure, :propbeds, :propbaths, :metatitle, :metadescription, :propshortdesc, :proplongdesc, :visible, :status, :propLast, :property_location_id, :displayOrder)";
				$strType = "insert";
				$arrdbparams['propLast'] = $propLast;
				
				
				
			} elseif($strcmd == "updatePage") {
				$strdbsql = "UPDATE properties SET p_name = :propname, p_area = :propaddress, postcode = :proppostcode, prop_type = :proptype, prop_price = :propprice, prop_tenure = :proptenure, No_beds = :propbeds, No_baths = :propbaths, metatitle = :metatitle, metadescription = :metadescription, short_description = :propshortdesc, long_description = :proplongdesc, make_live = :visible, sold = :status, property_location_id = :property_location_id, displayOrder = :displayOrder WHERE egnID = :recordID";
				$strType = "update";
				$arrdbparams['recordID'] = $pageRecordID;
			}
			
			$updatePage = query($conn, $strdbsql, $strType, $arrdbparams);
			
			
			
			// Set images order
			$arrimages = $_POST['scrollerOrder'];
			foreach($arrimages AS $key=>$value) {
				$strdbsql = "UPDATE property_images SET image_order = :value WHERE egnID = :key";
				$result = query($conn,$strdbsql,"update",Array("value"=>$value,"key"=>$key));
			}
			
		//	foreach($arrimages AS $key=>$value) {
		//		$strdbsql = "UPDATE tbl_stockphotos SET fld_order = :value WHERE fld_counter = :key";
		//		$result = query($conn,$strdbsql,"update",Array("value"=>$value,"key"=>$key));
		//	}
			
			// Get success/error messages for page create/update
			if($strcmd == "insertPage") {
				$pageRecordID = $updatePage;
				if($updatePage > 0) { $strsuccess = "Property successfully added";
					$strdbsql = "UPDATE properties SET rs_id = egnID WHERE egnID = :egnID";
					$result = query($conn,$strdbsql,"update",Array("egnID"=>$updatePage));
				}
				elseif($updatePage == 0) {$strerror = "An error occurred while adding the property";}
				
				
			} elseif($strcmd == "updatePage") {
				if($updatePage <= 1) $strsuccess = "Property successfully updated";
				elseif($updatePage > 1) $strwarning = "An error may have occurred while updating this property";
			}
			
			$strcmd = "viewPage";
			
			break;
		
		
		case "duplicateProperty":
			

			$strdbsql = "INSERT INTO properties (p_name, p_area, postcode, prop_type, prop_price, prop_tenure, No_beds, No_baths, metatitle, metadescription, short_description, long_description, make_live, sold, rs_id, property_location_id) SELECT p_name, p_area, postcode, prop_type, prop_price, prop_tenure, No_beds, No_baths, metatitle, metadescription, short_description, long_description, make_live, sold, rs_id, property_location_id FROM properties WHERE egnID = :pageRecordID";
			$strType = "insert";
			$result = query($conn, $strdbsql, "insert", array("pageRecordID"=>$pageRecordID));
			
			$oldPropertyID = $pageRecordID;

			$pageRecordID = $result;
			if($pageRecordID > 0) { $strsuccess = "Property successfully duplicating";
				$strdbsql = "UPDATE properties SET rs_id = egnID WHERE egnID = :egnID";
				$result = query($conn,$strdbsql,"update",Array("egnID"=>$pageRecordID));
				
				
				$strdbsql = "INSERT INTO property_images (rs_id, image_ref, image_order, main_image, make_live, egnID) SELECT $pageRecordID, image_ref, image_order, main_image, make_live, $pageRecordID FROM property_images WHERE egnID = :pageRecordID";
				$strType = "insert";
				$result = query($conn, $strdbsql, "insert", array("pageRecordID"=>$oldPropertyID));
				
			}
			elseif($pageRecordID == 0) {$strerror = "An error occurred while duplicating the property";}
				
				
			
			
			$strcmd = "viewPage";
			
			break;
		
		case "deletePage":
			
			// Deleting page from database
			$strdbsql = "DELETE FROM properties WHERE egnID = :pageRecordID";
			$deletePage = query($conn, $strdbsql, "delete", array("pageRecordID"=>$pageRecordID));
			
			// Deleting images from database
			$strdbsql = "DELETE FROM property_images WHERE rs_id = :galleryID";
			$deleteImages = query($conn, $strdbsql, "delete", array("galleryID"=>$pageRSID));
			
			$strcmd = "";
			
			break;
		
		case "insertImage":
			
			// Get image name
			$total = count($_FILES['frm_image']['name']);
			for( $i=0 ; $i < $total ; $i++ ) {
			
				$newfilename = strtolower($_FILES['frm_image']['name'][$i]);
				$strfileextn = getExtension($newfilename);
				
				// Get intended directory
				$strimguploadpath = "images/";
				$struploaddir = $strrootpath.$strimguploadpath;
				
				// Get image name and directory
				$upfile = $struploaddir.$newfilename;
				
				if ($_FILES['frm_image']['error'][$i] > 0)
				{
					switch ($_FILES['frm_image']['error'][$i]) {
						case 1: $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file."; break;
						case 2: $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file."; break;
						case 3: $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file."; break;
						case 4: $strerror = "Problem: No file selected. Please try again."; break;
					}
				} else {
					// Does the file have the right extension ?
					if (strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0) {
						$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_newimage']['type'][$i]." file.<br/><br/>Please convert the picture to the required format and try again.";
					} else {
						if (is_uploaded_file($_FILES['frm_image']['tmp_name'][$i])) {
							if (!move_uploaded_file($_FILES['frm_image']['tmp_name'][$i], $upfile)) {
								$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_image']['tmp_name'][$i]." and could not move file to ".$upfile.". Please try again.";
							} else {
								//scale to maximum that will fix in box 1024x768 pixels (smaller files will be enlarged, larger files will be shrunk, aspect ratio of file remains the same)
								$resizedfile = image_scaletomax($upfile, $upfile, 1200, 800);
							}
						} else {
							$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_image']['name'][$i].". Please try again.<br/>";
						}
					}
				}
				
				if ($strerror != "") $strcommand = "ERRORMSG";
				else {
					
					
					// Get new image directory
					$strpagepath = $strimguploadpath."properties/";
					$strpagedir = $strrootpath.$strpagepath;
					
					// Get image name and new directory
					$newfilename = strtolower($_REQUEST['galleryID']."_".$i."_".time().".".$strfileextn);
					$newfile = $strpagedir.$newfilename;

					//use build in JPEG  compression if available to reduce image to 70% quality (
				//	image_compress($pathtoimage, $pathtoimage,70);

					//use build in PNG  compression if available to reduce image size using max compression level (compression levels are 0 to 9)
				//	image_compress($pathtoimage, $pathtoimage,null, 9);
					
					$range = copy($upfile,$newfile);
					
					// Change image permissions to rewrite
					chmod($newfile, 0777);
					
					// Delete file if already exists
					unlink($upfile);
					
				//	print($newfile);
					
					// Get total images assigned to the same property
					$strdbsql = "SELECT COUNT(*) AS max_order FROM property_images WHERE rs_id = :galleryID";
					$maxOrder = query($conn, $strdbsql, "single", array("galleryID"=>$_REQUEST['galleryID']));
					
					if($maxOrder['max_order'] > 0){ 
						$order = $maxOrder['max_order'] + 1;
						$main = NULL;
					} else {
						$order = 1;
						$main = "yes";
					}
					
					// Insert image into database as last
					$strdbsql = "INSERT INTO property_images (rs_id, image_ref, image_order, main_image, make_live, egnID) VALUES (:id, :imageLocation, :galleryOrder, :main, 'yes', :egnID)";
					$updatePageImage = query($conn, $strdbsql, "insert", array("id"=>$_REQUEST['galleryID'], "imageLocation"=>str_replace(".png", "", str_replace(".jpg", "", $newfilename)), "main"=>$main, "galleryOrder"=>$order, "egnID"=>$pageRecordID));
				
				}
			}
			if ($strerror != "") $strsuccess = "New page image added";
			
			$strcmd = "viewPage";
			
		break;
		case "addGalleryImage":
	
			$strdbsql = "SELECT COUNT(*) AS max_order FROM property_images WHERE rs_id = :galleryID";
			$strType = "single";
			$arrdbparam = array( "galleryID" => $_REQUEST['galleryID'] );
			$maxOrder = query($conn, $strdbsql, $strType, $arrdbparam);
			$order = $maxOrder['max_order'] + 1;
			
			$arrdbparams = array(
				"id" => $_REQUEST['galleryID'],
				"imageLocation" => str_replace(".png", "", str_replace(".jpg", "", $_POST['unusedImages'])),
				"galleryOrder" => $order
			);
			
			$strdbsql = "INSERT INTO property_images (rs_id, image_ref, image_order, make_live) VALUES (:id, :imageLocation, :galleryOrder, 'yes')";
			$strType = "insert";
			$addImage = query($conn, $strdbsql, $strType, $arrdbparams);
			
			var_dump($addImage);
			
			if($addImage < 0) $strsuccess = "Image successfully added";
			else if($addImage == 0) $strerror = "An error occurred while adding the image";
			
			$strcmd = "viewPage";
			
		break;
		case "deleteImages":
			
			
			foreach(explode(",",$deleteImages) AS $deletedImageID){
				$strdbsql = "DELETE FROM property_images WHERE recordID = :imageRecordID";
				$removeImages = query($conn, $strdbsql, "delete", array("imageRecordID"=>$deletedImageID));
			}
			
			$strdbsql = "SELECT egnID, recordID FROM property_images WHERE rs_id = :galleryID ORDER BY image_order";
			$galleryImages = query($conn,$strdbsql,"multi",array("galleryID"=>$_REQUEST['galleryID']));
			
			$i = 1;
			
			foreach($galleryImages AS $galleryImage)
			{
				$strdbsql = "UPDATE property_images SET image_order = ".$i." WHERE recordID = :recordID";
				query($conn, $strdbsql, "update", array("recordID"=>$galleryImage['recordID']));
				$i++;
			}
			
			$strcmd = "viewPage";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Manage Properties</h1>");
			
			//Print out debug and error messages
			if($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddProperty() {
					document.form.cmd.value="addPage";
					document.form.submit();
				}
				function jsviewProperty(pageRecordID) {
					document.form.cmd.value="viewPage";
					document.form.pageRecordID.value=pageRecordID;
					document.form.submit();
				}
				function jsdeleteProperty(pageRecordID,rs_id) {
					if(confirm("Are you sure you want to delete this page?"))
					{
						document.form.cmd.value="deletePage";
						document.form.pageRecordID.value=pageRecordID;
						document.form.pageRSID.value=rs_id;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				
				function jsduplicateProperty(pageRecordID,rs_id) {
					if(confirm("Are you sure you want to duplicate this page?"))
					{
						document.form.cmd.value="duplicateProperty";
						document.form.pageRecordID.value=pageRecordID;
						document.form.pageRSID.value=rs_id;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				
				
				
				function jsinsertProperty() {
					document.form.cmd.value='insertPage';
					document.form.submit();
				}
				function jsupdateProperty() {
					if($('#form').valid()) {
						document.form.cmd.value='updatePage';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertImage() {
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jsaddSiteBlock() {
					if($('#form').valid()) {
						document.form.cmd.value='addSiteBlock';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsdeleteSiteBlocks() {
					if($(".blockPosList li.selected").length > 0)
					{
						if(confirm("Are you sure you want to delete this block(s)?"))
						{
							var deleteBlocks = "";
							$(".blockPosList li.selected").each(function (index, item) {
								deleteBlocks += $(item).attr("id")+",";
							});
							$("#blockPositionID").val($(".blockPosList li.selected").attr("data-pos"));
							//console.log($(".blockPosList li.selected").parent().attr("id"));
							deleteBlocks = deleteBlocks.replace(/,\s*$/, "");
							//console.log(deleteImages);
							document.form.cmd.value="deleteBlocks";
							document.form.deleteBlocks.value=deleteBlocks;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the block(s) you wish to delete");
					}
					
				}
				function jsaddGalleryImage() {
					document.form.cmd.value='addGalleryImage';
					document.form.submit();
				}
				function jsdeleteImages() {
					if($("#sortableGalleryImages li.selected").length > 0)
					{
						
						if(confirm("Are you sure you want to delete this image(s)?"))
						{
							var deleteImages = "";
							$("#sortableGalleryImages li.selected").each(function (index, item) {
								deleteImages += $(item).attr("id")+",";
							});
							deleteImages = deleteImages.replace(/,\s*$/, "");
							//console.log(deleteImages);
							document.form.cmd.value="deleteImages";
							document.form.deleteImages.value=deleteImages;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the image(s) you wish to delete");
					}
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				
				$().ready(function() {
					$(".blockPosList").on('click', 'li', function (e) {
						if(e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
						$(this).parent().parent().siblings().children("ul").children().removeClass('selected');
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if(!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							//console.log(item);
							
							//Clone the selected items into an array
							//console.log(item.parent().children('.selected'));
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								//console.log($(item));
								//console.log($(item).attr('data-order'));
								var oldOrder = $(item).attr("data-order");
								
								$(item).parent().children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if(parseInt(newOrder) > parseInt(oldOrder))
								{
									sortType = "DESC";
								}
								else if(parseInt(newOrder) < parseInt(oldOrder))
								{
									sortType = "ASC";
								}
								
								dataArray[i] = {};
								dataArray[i]["pageOrder"] = $(item).attr("data-order");
								dataArray[i]["positionID"] = $(item).attr("data-pos");
								dataArray[i]["recordID"] = $(item).attr("id");
								dataArray[i]["sitePageID"] = $("#pageRecordID").attr("value");
								dataArray[i]["type"] = "page";
								dataArray[i]["cmd"] = "reorder";
								//console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if(sortType == "ASC")
								{
									//console.log("sort ascending");
									return parseInt(a.pageOrder) - parseInt(b.pageOrder);
								}
								else if(sortType == "DESC")
								{
									//console.log("sort descending");
									return parseInt(b.pageOrder) - parseInt(a.pageOrder);
								}
							});
							//console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_siteblockrelationmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
				$(document).ready(function(){
					$(".show-toggle").click(function(){
					
						var idstring = $(this).attr('id');
					
						if(this.checked) {
							var show = "yes";
						}
						else {
							var show = "";
						}
						
						var idsplit = idstring.split('-')[2];
						
						$.ajax({
							type: "GET",
							url: "/admin/includes/ajax_propertyvisible.php?pageID="+idsplit+"&visible="+show
						});
						
					});
				});
				$().ready(function() {
					$("#sortableGalleryImages").on('click', 'li', function (e) {
						if(e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						placeholder: "ui-state-highlight",
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if(!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								
								var oldOrder = $(item).attr("data-order");
								$("ul#sortableGalleryImages").children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
									$(item).find(".order").val($(item).index() + 1);
								});
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if(parseInt(newOrder) > parseInt(oldOrder)) sortType = "DESC";
								else if(parseInt(newOrder) < parseInt(oldOrder)) sortType = "ASC";
								
								dataArray[i] = {};
								dataArray[i]["galleryOrder"] = $(item).attr("data-order");
								dataArray[i]["imageLocation"] = /[^/]*$/.exec($(item).children("img").attr("src"))[0];
								dataArray[i]["recordID"] = $(item).attr("id");
								dataArray[i]["galleryID"] = $("#galleryID").attr("value");
								dataArray[i]["cmd"] = "reorder";
								//console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if(sortType == "ASC")
								{
									//console.log("sort ascending");
									return parseInt(a.galleryOrder) - parseInt(b.galleryOrder);
								}
								else if(sortType == "DESC")
								{
									//console.log("sort descending");
									return parseInt(b.galleryOrder) - parseInt(a.galleryOrder);
								}
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_stockimagemanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
			</script>
			<?php
		
			print("<form action='cms-properties.php' class='uniForm col-sm-12' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='pageRecordID' id='pageRecordID' value='".$pageRecordID."'/>");
				print("<input type='hidden' name='imageRecordID' id='imageRecordID' value='".$imageRecordID."'/>");
				print("<input type='hidden' name='pageRSID' id='pageRSID' value='".$pageRSID."'/>");
				print("<input type='hidden' name='blockPositionID' id='blockPositionID' />");
				
				switch($strcmd)
				{
					case "viewPage":
					case "addPage":
						
						if($strcmd == "viewPage")
						{
							$strdbsql = "SELECT * FROM properties WHERE egnID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $pageRecordID);
							$pageDetails = query($conn, $strdbsql, $strType, $arrdbparams);
						} elseif($strcmd == "addPage") {
							$pageDetails = array(
								"propname" => "",
								"propaddress" => "",
								"proppostcode" => "",
								"proptype" => "",
								"proptenure" => "",
								"propbeds" => "",
								"propbaths" => "",
								"metatitle" => "",
								"metadescription" => "",
								"propshortdesc" => "",
								"proplongdesc" => "",
								"property_location_id" => "",
								"sold" => "",
								"displayOrder" => 1
							);
						}
						
						print("<input type='hidden' name='galleryID' id='galleryID' value='".$pageDetails['rs_id']."' />");
						
						print("<div class='row'>");
							print("<div class='col-sm-6 crop-sm-right'>");
								print("<div class='section'>");
									print("<fieldset class='inlineLabels'>");
										print("<legend>Details</legend>");
										print("<div class='row'>");
											print("<div class='form-group col-md-8 crop-lg-right'>");
												print("<label for='frm_propname' class='control-label'>Name</label>");
												print("<input type='text' class='form-control' id='frm_propname' name='frm_propname' value='".htmlentities($pageDetails['p_name'], ENT_QUOTES, 'UTF-8')."'>");
											print("</div>");
											print("<div class='form-group col-md-4 crop-lg-left'>");
												print("<label for='frm_proppostcode' class='control-label'>Postcode</label>");
												print("<input type='text' class='form-control' id='frm_proppostcode' name='frm_proppostcode' value='".$pageDetails['postcode']."'>");
											print("</div>");
										print("</div>");
										
										print("<div class='row'>");
											print("<div class='form-group col-md-8 crop-lg-right'>");
												print("<label for='frm_propaddress' class='control-label'>Address</label>");
												print("<input type='text' class='form-control' id='frm_propaddress' name='frm_propaddress' value='".htmlentities($pageDetails['p_area'], ENT_QUOTES, 'UTF-8')."'>");
											print("</div>");
											print("<div class='form-group col-md-4 crop-lg-left'>");
												print("<label for='frm_propprice' class='control-label'>Price</label>");
												print("<input type='text' class='form-control' id='frm_propprice' name='frm_propprice' value='".$pageDetails['prop_price']."'>");
											print("</div>");
										print("</div>");
										
										print("<div class='row'>");
											print("<div class='form-group col-md-3 crop-lg-right'>");
												print("<label for='frm_proptype' class='control-label'>Property Type</label>");
												print("<input type='text' class='form-control' id='frm_proptype' name='frm_proptype' value='".$pageDetails['prop_type']."'>");
											print("</div>");
											print("<div class='form-group col-md-3 crop-lg-both'>");
												print("<label for='frm_proptenure' class='control-label'>Tenure</label>");
												print("<input type='text' class='form-control' id='frm_proptenure' name='frm_proptenure' value='".$pageDetails['prop_tenure']."'>");
											print("</div>");
											print("<div class='form-group col-md-3 crop-lg-both'>");
												print("<label for='frm_propbeds' class='control-label'>Bedrooms</label>");
												print("<input type='number' class='form-control' id='frm_propbeds' name='frm_propbeds' value='".$pageDetails['No_beds']."'>");
											print("</div>");
											print("<div class='form-group col-md-3 crop-lg-left'>");
												print("<label for='frm_propbaths' class='control-label'>Bathrooms</label>");
												print("<input type='number' class='form-control' id='frm_propbaths' name='frm_propbaths' value='".$pageDetails['No_baths']."'>");
											print("</div>");
										print("</div>");
										
										
										print("<div class='row'>");
											print("<div class='form-group col-md-12 crop-lg-right'>");
												print("<label for='frm_property_location_id' class='control-label'>Property Location</label>");
												print("<select id='frm_property_location_id' name='frm_property_location_id' class='form-control' >");
														//print("<option value=''>None</option>");
															
														$strdbsql2 = "SELECT * FROM property_locations"; 
														$arrparams = array();
														$strType = "multi";
														$property_locations = query($conn, $strdbsql2, $strType, $arrparams);
														
														foreach($property_locations AS $property_location){
															
															if($property_location['recordID'] == $pageDetails['property_location_id'])
															{
																$strselected = " selected";
															}
															else
															{
																$strselected = "";
															}
															print("<option value='".$property_location['recordID']."'".$strselected.">".$property_location['description']."</option>");
														}
													print("</select>");
											
											print("</div>");
										print("</div>");
											
										print("<div class='row'>");
											print("<div class='form-group col-md-12 crop-lg-right'>");
												print("<label for='frm_displayOrder' class='control-label'>Display Order</label>");
												print("<select id='frm_displayOrder' name='frm_displayOrder' class='form-control' >");
														//print("<option value=''>None</option>");

														
														for($i = 1; $i < 40; $i++){
															
															if($i == $pageDetails['displayOrder'])
															{
																$strselected = " selected";
															}
															else
															{
																$strselected = "";
															}
															print("<option value='".$i."'".$strselected.">".$i."</option>");
														}
													print("</select>");
											
											print("</div>");
											
										print("</div>");
										
										
										
					
									print("</fieldset>");
								print("</div>");
								
								print("<div class='section'>");
									print("<fieldset class='inlineLabels'>");
										
										print("<legend style='margin:0;'>Enable");
										print("<div class='switch pull-right' style='margin:0;'>");
											if($pageDetails['make_live'] == "yes") print("<input id='cmn-visible' class='cmn-toggle cmn-toggle-round' type='checkbox' name='cmn-visible' value='1' checked>");
											else print("<input id='cmn-visible' class='cmn-toggle cmn-toggle-round' type='checkbox' name='cmn-visible' value='1'>");
											print("<label for='cmn-visible'></label>");
										print("</div>");
										print("</legend>");
										
										print("<legend style='margin:0;'>Under Offer");
											print("<div class='switch pull-right' style='margin:0;'>");
												if(strtolower($pageDetails['sold']) == "under offer") print("<input id='cmn-status' class='cmn-toggle cmn-toggle-round' type='checkbox' name='cmn-visible' value='1' checked>");
												else print("<input id='cmn-status' class='cmn-toggle cmn-toggle-round' type='checkbox' name='cmn-status' value='1'>");
												print("<label for='cmn-status'></label>");
											print("</div>");
										print("</legend>");
										
										print("<legend style='margin:0;'>SSTC");
											print("<div class='switch pull-right' style='margin:0;'>");
												if(strtolower($pageDetails['sold']) == "sstc") print("<input id='cmn-SSTC' class='cmn-toggle cmn-toggle-round' type='checkbox' name='cmn-SSTC' value='1' checked>");
												else print("<input id='cmn-SSTC' class='cmn-toggle cmn-toggle-round' type='checkbox' name='cmn-SSTC' value='1'>");
												print("<label for='cmn-SSTC'></label>");
											print("</div>");
										print("</legend>");
										
										print("<legend style='margin:0;'>Sold");
											print("<div class='switch pull-right' style='margin:0;'>");
												if(strtolower($pageDetails['sold']) == "sold") print("<input id='cmn-sold' class='cmn-toggle cmn-toggle-round' type='checkbox' name='cmn-sold' value='1' checked>");
												else print("<input id='cmn-sold' class='cmn-toggle cmn-toggle-round' type='checkbox' name='cmn-sold' value='1'>");
												print("<label for='cmn-sold'></label>");
											print("</div>");
										print("</legend>");
										
									print("</fieldset>");
								print("</div>");
								
								print("<div class='section'>");
									print("<fieldset class='inlineLabels'>");
									
										print("<legend>Meta Info</legend>");
								
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_metatitle' class='control-label'>Meta Title <i class='fa fa-info-circle pull-right' data-toggle='tooltip' title='This should range from 50-60 characters.'></i></label>");
												print("<input type='text' class='form-control' id='frm_metatitle' name='frm_metatitle' value='".$pageDetails['metatitle']."'>");
											print("</div>");
										print("</div>");
										
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_metadescription' class='control-label'>Meta Description <i class='fa fa-info-circle pull-right' data-toggle='tooltip' title='This should range from 155-165 characters.'></i></label>");
												print("<textarea class='form-control' id='frm_metadescription' name='frm_metadescription' rows='3'>".$pageDetails['metadescription']."</textarea>");
											print("</div>");
										print("</div>");
										
									print("</fieldset>");
								print("</div>");
							print("</div>");
										
							print("<div class='col-sm-6 crop-sm-left'>");																		
								
								print("<div class='section'>");
									print("<fieldset class='inlineLabels'>");
										
										print("<legend>Content</legend>");
										
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_propshortdesc' class='control-label'>Short Description</label>");
												print("<textarea class='form-control' id='frm_propshortdesc' name='frm_propshortdesc' rows='5'>".$pageDetails['short_description']."</textarea>");
											print("</div>");
										print("</div>");
										
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_proplongdesc' class='control-label'>Long Description</label>");
												print("<textarea class='form-control tinymce' id='frm_proplongdesc' name='frm_proplongdesc' rows='11'>".$pageDetails['long_description']."</textarea>");
											print("</div>");
										print("</div>");
										
									print("</fieldset>");
								print("</div>");
								
								if($strcmd == "viewPage") {
								
									$strdbsql = "SELECT * FROM property_images WHERE rs_id = :recordID ORDER BY image_order ASC";
									$strType = "multi";
									$arrdbparam = array("recordID" => $pageDetails['rs_id']);
									$galleryImages = query($conn, $strdbsql, $strType, $arrdbparam);
									
									$unusedImages = array();
									$dh = opendir("../images/properties");
									while (false !== ($galleryImg = readdir($dh))) {
										if($galleryImg != "." && $galleryImg != "..") {
											$unusedImages[] = $galleryImg;
										}
									}
									
									$assignedImages = "";
									if(!empty($galleryImages))
									{
										foreach($galleryImages AS $galleryImage)
										{
											$assignedImages .= "<li id='".$galleryImage['recordID']."' data-order='".$galleryImage['image_order']."'>";
												$assignedImages .= "<input type='hidden' name='scrollerOrder[".$galleryImage['egnID']."]' class='order' value='".$galleryImage['image_order']."' />";
												if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/properties/".$galleryImage['image_ref'].".png")) $assignedImages .= "<img src='/images/properties/".$galleryImage['image_ref'].".png' />";
												else if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/properties/".$galleryImage['image_ref'].".jpg")) $assignedImages .= "<img src='/images/properties/".$galleryImage['image_ref'].".jpg' />";
											$assignedImages .= "</li>";
											$key = array_search($galleryImage['image_ref'], $unusedImages);
											if($key !== false)
											{
												unset($unusedImages[$key]);
											}
										}
									}
								
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
									
											print("<legend>Add New Image</legend>");
									
											print("<div class='row'>");
													print("<div class='form-group'>");
														print("<label for='frm_image' class='control-label'>Image</label>");
														print("<input type='file' class='form-control' id='frm_image' name='frm_image[]' multiple/>");
													print("</div>");
												print("</div>");
											/*	print("<div class='row'>");
													print("<div class='form-group'>");
														print("<label for='frm_addcurrent' class='control-label'>Add to current gallery</label>");
														print("<input type='checkbox' id='frm_addcurrent' name='frm_addcurrent' value='1' />");
													print("</div>");
												print("</div>");*/
												print("<div class='row'>");
													print("<div class='form-group' style='text-align:right;'>");
														print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success' style='display:inline-block;'>Insert</button>");
													print("</div>");
												print("</div>");
											
										print("</fieldset>");
									print("</div>");
								
									if(count($unusedImages) > 0 || $assignedImages != "")
									{
									/*	if(count($unusedImages) > 0)
										{
											print("<div class='section'>");
												print("<fieldset class='inlineLabels'>");
										
													print("<legend>Unused Gallery Images</legend>");
											
													print("<div class='row'>");
														print("<div class='form-group'>");
															print("<label for='unusedGalleryImages' control-label'>Images</label>");
															print("<select id='unusedGalleryImages' name='unusedImages' class='form-control valid'>");
																foreach ($unusedImages AS $unusedImage)
																{
																	print("<option value='".$unusedImage."'>".$unusedImage."</option>");
																}
															print("</select>");
														print("</div>");
													print("</div>");
													
													print("<div class='row'>");
														print("<div class='form-group' style='text-align:right;'>");
															print("<button style='display:inline-block;' onclick='return jsaddGalleryImage();' type='submit' class='btn btn-success'>Add Image</button> ");
														print("</div>");
													print("</div>");
															
												print("</fieldset>");
											print("</div>");
											
										}*/
										
										print("<div class='section'>");
											print("<fieldset class='inlineLabels'>");
												print("<legend>Images <div class='pull-right'><button onclick='return jsdeleteImages();' type='button' class='btn btn-danger circle'><i class='fa fa-trash' style='color:#FFF;'></i></button></div><input type='hidden' name='deleteImages' id='deleteImages' /></legend>");
												print("<div class='row'>");
													print("<div class='form-group'>");
														print("<ul id='sortableGalleryImages'>");
															print($assignedImages);
														print("</ul>");
													print("</div>");
												print("</div>");
											print("</fieldset>");
										print("</div>");

									}
								}
								
							print("</div>");
						print("</div>");
							
						print("<div class='row'>");
							print("<div class='col-xs-6' text-align:left;>");
								print("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>");
							print("</div>");
							print("<div class='col-xs-6' style='text-align:right;'>");
							
								if($strcmd == "addPage")
								{
								  print("<button onclick='return jsinsertProperty();' type='submit' class='btn btn-success' style='display:inline-block;'>Create</button> ");
								}
								elseif($strcmd == "viewPage")
								{
								  print("<button onclick='return jsupdateProperty();' type='submit' class='btn btn-success' style='display:inline-block;'>Save</button> ");
								}
								
							print("</div>");
						print("</div>");
					
					break;
					default:
						
						$strdbsql = "SELECT * FROM properties ORDER BY egnID DESC";
						$strType = "multi";
						$properties = query($conn, $strdbsql, $strType);
						
						print("<div class='section'>");
							print("<table id='pages-table' class='table table-striped table-bordered table-hover table-condensed' >");
								print("<thead><tr>");
									print("<th class='hide'></th>");
									print("<th width='15%'>Name</th>");
									print("<th width='25%' class='media-out'>Address</th>");
									print("<th width='20%' class='media-out'>Type</th>");
									print("<th width='10%' class='media-out'>Bedrooms</th>");
									print("<th width='10%' style='text-align:center;'>Show/Hide</th>");
									print("<th width='10%' style='text-align:center;'>Update</th>");
									print("<th width='10%' style='text-align:center;'>Duplicate</th>");
									print("<th width='10%' style='text-align:center;'>Delete</th>");
								print("</tr></thead><tbody>");
								foreach($properties AS $property)
								{
									print("<tr>");
										print("<td class='hide'>".$property['egnID']."</td>");
										print("<td>".$property['p_name']."</td>");
										print("<td class='media-out'>".$property['p_area'].", ".$property['postcode']."</td>");
										print("<td class='media-out'>".$property['prop_type']."</td>");
										print("<td class='media-out'>".$property['No_beds']."</td>");
										print("<td style='text-align:center;'>");
											print("<div class='switch' style='display:inline-block;'>");
												if($property['make_live'] != "yes")
												{
													print("<input id='show-toggle-".$property['egnID']."' class='show-toggle cmn-toggle-round' type='checkbox'>");
												}
												else
												{
													print("<input id='show-toggle-".$property['egnID']."' class='show-toggle cmn-toggle-round' type='checkbox' checked>");
												}
												print("<label for='show-toggle-".$property['egnID']."'></label>");
											print("</div>");
										print("</td>");
										print("<td style='text-align:center;'><button onclick='return jsviewProperty(\"".$property['egnID']."\");' type='submit' class='btn btn-primary circle' style='display:inline-block;'><i class='fa fa-pencil'></i></button></td>");
										print("<td style='text-align:center;'><button onclick='return jsduplicateProperty(\"".$property['egnID']."\",\"".$property['rs_id']."\");' type='submit' class='btn btn-primary circle' style='display:inline-block;'><i class='fa fa-plus'></i></button></td>");
										print("<td style='text-align:center;'><button onclick='return jsdeleteProperty(\"".$property['egnID']."\",\"".$property['rs_id']."\");' type='submit' class='btn btn-danger circle' style='display:inline-block;'><i class='fa fa-trash'></i></button></td>");
									print("</tr>");
								}
								print("</tbody>");
							print("</table>");
						print("</div>");
						
						print("<div class='buttons' style='text-align:right;'>");
							print("<button onclick='return jsaddProperty();' type='submit' class='btn btn-success' style='display:inline-block;'>Add</button>");					
						print("<div>");
							
						break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#pages-table').DataTable({
				"order": [[ 0, "desc"]]
			});
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>