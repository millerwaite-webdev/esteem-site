<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * catalogue-manage-categories.php                                    * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "catalogue-manage-ranges"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['rangeID'])) $rangeID = $_REQUEST['rangeID']; else $rangeID = "";
	
	switch($strcmd)
	{
		case "insertRange":
		case "updateRange":
			if (isset($_REQUEST['frm_showproducts'])) { $strshowProducts = 1; } else { $strshowProducts = 0;}
			if (isset($_REQUEST['frm_showfilters'])) { $strshowFilters = 1; } else { $strshowFilters = 0;}
			
			$arrdbparams = array(
								"rangeName" => $_POST['frm_rangename'],
								"rangeDescription" => $_POST['frm_rangedescription'],
								"rangeSidebar" => $_POST['frm_rangesidebar'],
								"rangeBanner" => $_POST['frm_rangebanner'],
								"rangeImage" => $_POST['frm_rangeimage'],
								"metaTitle" => $_POST['frm_metatitle'],
								"metaPageLink" => $_POST['frm_metapagelink'],
								"metaDescription" => $_POST['frm_metadescription'],
								"metaKeywords" => $_POST['frm_metakeywords'],
								"showProducts" => $strshowProducts,
								"showFilters" => $strshowFilters
							);
			
			if ($strcmd == "insertRange")
			{	
				$strdbsql = "INSERT INTO stock_ranges (rangeName, rangeDescription, rangeBanner, rangeImage, rangeSidebar, metaTitle, metaPageLink, metaDescription, metaKeywords, showProducts, showFilters) 
							VALUES (:rangeName, :rangeDescription, :rangeBanner, :rangeImage, :rangeSidebar, :metaTitle, :metaPageLink, :metaDescription, :metaKeywords, :showProducts,:showFilters)";
				$strType = "insert";
			}
			elseif ($strcmd == "updateRange")
			{
				$strdbsql = "UPDATE stock_ranges SET rangeName = :rangeName, rangeDescription = :rangeDescription, rangeBanner = :rangeBanner, rangeImage = :rangeImage, rangeSidebar = :rangeSidebar, 
							metaTitle = :metaTitle, metaPageLink = :metaPageLink, metaDescription = :metaDescription, metaKeywords = :metaKeywords, showProducts = :showProducts, 
							showFilters = :showFilters WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $rangeID;
				$strType = "update";
			}
			
			$updateRange = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertRange")
			{
				$rangeID = $updateRange;
			}
			
			if ($strcmd == "insertRange")
			{
				if ($updateRange > 0)
				{
					$strsuccess = "Range successfully added";
				}
				elseif ($updateRange == 0)
				{
					$strerror = "An error occurred while adding the range";
				}
			}
			elseif ($strcmd == "updateRange")
			{
				if ($updateRange <= 1)
				{
					$strsuccess = "Range successfully updated";
				}
				elseif ($updateRange > 1)
				{
					$strwarning = "An error may have occurred while updating this range";
				}
			}
			
			$strcmd = "viewRange";
			
		break;
		
		case "insertImage":
			$newfilename = $_FILES['frm_image']['name'];
			$strfileextn = getExtension($newfilename);
			
			$strimguploadpath = "images/";
			$struploaddir = $strrootpath.$strimguploadpath; // where to put full size image
			$upfile = $struploaddir.$newfilename;
			
			//        print ("$upfile - saving image<br>");

			if ($_FILES['frm_image']['error'] > 0)
			{
				switch ($_FILES['frm_image']['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				//   print ("$strfileextn - checking extensions<br>");
				// Does the file have the right extension ?
				//if ($strfileextn != 'jpg' && $strfileextn != 'jpeg' && $strfileextn != 'png' && $strfileextn != 'gif')
				if (strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0)
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_image']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
				}
				else
				{
					//      print ("Uploading to products image directory<br>");
					if (is_uploaded_file($_FILES['frm_image']['tmp_name']))
					{
						if (!move_uploaded_file($_FILES['frm_image']['tmp_name'], $upfile))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_image']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_image']['name'].". Please try again.<br/>";
					}
				}
			}
			
			if ($strerror != "")
			{
				$strcommand = "ERRORMSG";
				//   print ("Got an Error - $strcommand - $strerror<br>");
			}
			else
			{
				$strrangepath = $strimguploadpath."ranges/";
				
				$strrangedir = $strrootpath.$strrangepath;
				
				$newfile = $strrangedir.$newfilename;
				$range=copy($upfile,$newfile);
				chmod ($newfile, 0777);
				
				
				unlink($upfile);
				
				if (isset($_REQUEST['frm_replacecurrent'])) $replaceCurrent = $_REQUEST['frm_replacecurrent']; else $replaceCurrent = 0;
				
				if($replaceCurrent)
				{
					$updateRangeImageQuery = "UPDATE stock_ranges SET rangeImage = :rangeImage WHERE recordID = :recordID";
					$strType = "update";
					$arrdbparams = array (
									"rangeImage" => $newfilename,
									"recordID" => $rangeID
								);
					$updateRangeImage = query($conn, $updateRangeImageQuery, $strType, $arrdbparams);
				}
			}
			
			$strcmd = "viewRange";
			
		break;
		
		case "addSiteBlock":
			
			$strdbsql = "SELECT COUNT(*) AS max_order FROM site_block_relations WHERE rangeID = :rangeID AND positionID = :positionID";
			$strType = "single";
			$arrdbparams = array(
								"rangeID" => $rangeID,
								"positionID" => $_POST['frm_siteblockposition']
							);
			$maxOrder = query($conn, $strdbsql, $strType, $arrdbparams);
			$order = $maxOrder['max_order'] + 1;
			
			foreach($_POST['frm_siteblocks'] AS $siteBlock)
			{
				$strdbsql = "INSERT INTO site_block_relations (blockID, positionID, rangeID, pageOrder) VALUES (:blockID, :positionID, :rangeID, :pageOrder)";
				$strType = "insert";
				$arrdbparams = array( 
									"blockID" => $siteBlock,
									"positionID" => $_POST['frm_siteblockposition'],
									"rangeID" => $rangeID,
									"pageOrder" => $order
								);
				$insertBlock = query($conn, $strdbsql, $strType, $arrdbparams);
				$order++;
			}
			
			$strcmd = "viewRange";
			
		break;
		
		case "deleteBlocks":
			
			$strdbsql = "DELETE FROM site_block_relations WHERE recordID IN (".$_POST['deleteBlocks'].")";
			$strType = "delete";
			$removeImages = query($conn, $strdbsql, $strType);
			
			$strdbsql = "SELECT recordID FROM site_block_relations WHERE rangeID = :rangeID AND positionID = :positionID ORDER BY pageOrder";
			$strType = "multi";
			$arrdbparams = array(
							"rangeID" => $rangeID,
							"positionID" => $_POST['blockPositionID']
						);
			//echo "get products\n";
			$pageBlocks = query($conn, $strdbsql, $strType, $arrdbparams);
			//var_dump($categories);
			
			$i = 1;
			foreach($pageBlocks AS $pageBlock)
			{
				$strdbsql = "UPDATE site_block_relations SET pageOrder = ".$i." WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparam = array(
								"recordID" => $pageBlock['recordID']
							);
				//echo "update final ordering\n";
				query($conn, $strdbsql, $strType, $arrdbparam);
				$i++;
			}
			
			$strcmd = "viewRange";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Range Control</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddRange() {
					document.form.cmd.value='addRange';
					document.form.submit();
				}
				function jsinsertRange() {
					document.form.cmd.value='insertRange';
					document.form.submit();
				}
				function jsviewRange(rangeID) {
					if ($('#form').valid()) {
						document.form.cmd.value='viewRange';
						document.form.rangeID.value=rangeID;
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsupdateRange() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateRange';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertImage() {
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jsaddSiteBlock() {
					if ($('#form').valid()) {
						document.form.cmd.value='addSiteBlock';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsdeleteSiteBlocks() {
					if ($(".blockPosList li.selected").length > 0)
					{
						if(confirm("Are you sure you want to delete this block(s)?"))
						{
							var deleteBlocks = "";
							$(".blockPosList li.selected").each(function (index, item) {
								deleteBlocks += $(item).attr("id")+",";
							});
							$("#blockPositionID").val($(".blockPosList li.selected").attr("data-pos"));
							//console.log($(".blockPosList li.selected").parent().attr("id"));
							deleteBlocks = deleteBlocks.replace(/,\s*$/, "");
							//console.log(deleteImages);
							document.form.cmd.value="deleteBlocks";
							document.form.deleteBlocks.value=deleteBlocks;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the block(s) you wish to delete");
					}
					
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				
				$().ready(function() {
					$("#assignedProducts").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#unassignedProducts").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#unassignedProducts'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								if ($(item).parent().is("ul#assignedProducts")) {
									var oldOrder = $(item).attr("data-order");
									$("ul#assignedProducts").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									var newOrder = $(item).attr("data-order");
									//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
									if (parseInt(newOrder) > parseInt(oldOrder))
									{
										sortType = "DESC";
									}
									else if (parseInt(newOrder) < parseInt(oldOrder))
									{
										sortType = "ASC";
									}
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["stockID"] = $(item).attr('id');
									dataArray[i]["rangeID"] = $("#rangeID").attr("value");
									dataArray[i]["cmd"] = "reorder";
									//console.log(dataArray);$(item).find(".expand_collapse").remove();
								} else if ($(item).parent().is("ul#unassignedProducts")) {
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["stockID"] = $(item).attr('id');
									dataArray[i]["rangeID"] = $("#rangeID").attr("value");
									dataArray[i]["cmd"] = "delete";
									//console.log(dataArray);$(item).find(".expand_collapse").remove();
									
									
									$(item).removeAttr("data-order");
									//console.log($.trim($(item).children().first().text()));
									$.trim($(item).children().first());
									//console.log(item);
									$("ul#assignedProducts").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
								}
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									console.log("sort ascending");
									return parseInt(a.order) - parseInt(b.order);
								}
								else if (sortType == "DESC")
								{
									console.log("sort descending");
									return parseInt(b.order) - parseInt(a.order);
								}
							});
							console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_rangeproductmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();

					$("#unassignedProducts").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#assignedProducts").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#assignedProducts'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function(event, ui) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							$(elements).each(function(index, item) {
								if ($(item).parent().is("ul#assignedProducts")) {
									$(item).parent().children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["stockID"] = $(item).attr('id');
									dataArray[i]["rangeID"] = $("#rangeID").attr("value");
									dataArray[i]["cmd"] = "add";
								}
								i++;
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_rangeproductmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
					
					$(".blockPosList").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
						$(this).parent().parent().siblings().children("ul").children().removeClass('selected');
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							//console.log(item);
							
							//Clone the selected items into an array
							//console.log(item.parent().children('.selected'));
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								//console.log($(item));
								//console.log($(item).attr('data-order'));
								var oldOrder = $(item).attr("data-order");
								
								$(item).parent().children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if (parseInt(newOrder) > parseInt(oldOrder))
								{
									sortType = "DESC";
								}
								else if (parseInt(newOrder) < parseInt(oldOrder))
								{
									sortType = "ASC";
								}
								
								dataArray[i] = {};
								dataArray[i]["pageOrder"] = $(item).attr("data-order");
								dataArray[i]["positionID"] = $(item).attr("data-pos");
								dataArray[i]["recordID"] = $(item).attr("id");
								dataArray[i]["sitePageID"] = $("#rangeID").attr("value");
								dataArray[i]["type"] = "range";
								dataArray[i]["cmd"] = "reorder";
								//console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									//console.log("sort ascending");
									return parseInt(a.pageOrder) - parseInt(b.pageOrder);
								}
								else if (sortType == "DESC")
								{
									//console.log("sort descending");
									return parseInt(b.pageOrder) - parseInt(a.pageOrder);
								}
							});
							//console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_siteblockrelationmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
			</script>
			<?php
		
			print ("<form action='catalogue-manage-ranges.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				print ("<input type='hidden' name='rangeID' id='rangeID' value='$rangeID'/>");
				print ("<input type='hidden' name='blockPositionID' id='blockPositionID' />");
				
				switch($strcmd)
				{
					case "viewRange":
					case "addRange":
						
						if ($strcmd == "viewRange")
						{
							$strdbsql = "SELECT * FROM stock_ranges WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $rangeID);
							$rangeDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							
							/*print ("<pre>");
							print_r($rangeDetails);
							print ("</pre>");&*/
							
							print ("<fieldset class='inlineLabels'> <legend>Change Range Details</legend>");
						}
						elseif ($strcmd == "addRange")
						{
							$rangeDetails = array(
												"rangeName" => "",
												"rangeDescription" => "",
												"rangeSidebar" => "",
												"rangeBanner" => "",
												"rangeImage" => "",
												"metaTitle" => "",
												"metaPageLink" => "",
												"metaDescription" => "",
												"metaKeywords" => "",
												"showProducts" => 0,
												"showFilters" => 0
											);
							
							print ("<fieldset class='inlineLabels'> <legend>Add Range Details</legend>");
						}
						print ("<div class='form-group'>
							<label for='frm_rangename' class='col-sm-2 control-label'>Name</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_rangename' name='frm_rangename' value='".$rangeDetails['rangeName']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_metapagelink' class='col-sm-2 control-label'>Meta Link</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_metapagelink' name='frm_metapagelink' value='".$rangeDetails['metaPageLink']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_metatitle' class='col-sm-2 control-label'>Meta Doc Title</label>
							<div class='col-sm-10'>
							  <input style='width:655px;' type='text' class='form-control' id='frm_metatitle' name='frm_metatitle' value='".$rangeDetails['metaTitle']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_metakeywords' class='col-sm-2 control-label'>Meta Keywords</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_metakeywords' name='frm_metakeywords' value='".$rangeDetails['metaKeywords']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_rangeimage' class='col-sm-2 control-label'>Range Image</label>
							<div class='col-sm-10'>
							  <select name='frm_rangeimage' id='frm_rangeimage' class='form-control valid' style='width: 300px;'>
								<option value=''>None</option>");
							    $dh = opendir("../images/ranges/");
								while (false !== ($image = readdir($dh))) {
									if ($image != "." && $image != "..") {
										if($image == $rangeDetails['rangeImage'])
										{
											$strselected = " selected";
										}
										else
										{
											$strselected = "";
										}
										print ("<option value='".$image."'".$strselected.">".$image."</option>");
									}
								}
						print ("</select>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_rangebanner' class='col-sm-2 control-label'>Banner</label>
							<div class='col-sm-10'>
							  <textarea class='form-control' id='frm_rangebanner' name='frm_rangebanner' rows='5' style='width: 654px;'>".$rangeDetails['rangeBanner']."</textarea>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_metadescription' class='col-sm-2 control-label'>Meta Description</label>
							<div class='col-sm-5'>
							  <textarea class='form-control' id='frm_metadescription' name='frm_metadescription' rows='3'>".$rangeDetails['metaDescription']."</textarea>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_rangedescription' class='col-sm-2 control-label'>Description</label>
							<div class='col-sm-5'>
							  <textarea class='form-control' id='frm_rangedescription' name='frm_rangedescription' rows='5'>".$rangeDetails['rangeDescription']."</textarea>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_rangesidebar' class='col-sm-2 control-label'>Sidebar</label>
							<div class='col-sm-5'>
							  <textarea class='form-control' id='frm_rangesidebar' name='frm_rangesidebar' rows='5'>".$rangeDetails['rangeSidebar']."</textarea>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_productdescription' class='col-sm-2 control-label'>Show Options</label>
						  </div>
						  <div class='form-group'>
							<label for='frm_showproducts' class='col-sm-1 control-label'>Products</label>
							<div class='col-sm-1'>");
							  if($rangeDetails['showProducts'])
							  {
								$strChecked = " checked";
							  }
							  else
							  {
								$strChecked = "";
							  }
							  print ("<input type='checkbox' id='frm_showproducts' name='frm_showproducts' value='1' ".$strChecked." />
							</div>
							<label for='frm_showfilters' class='col-sm-1 control-label'>Filters</label>
							<div class='col-sm-1'>");
							  if($rangeDetails['showFilters'])
							  {
								$strChecked = " checked";
							  }
							  else
							  {
								$strChecked = "";
							  }
							  print ("<input type='checkbox' id='frm_showfilters' name='frm_showfilters' value='1' ".$strChecked." />
							</div>
						  </div>
						  <div class='form-group'>
							<div class='col-sm-10'>");
							if ($strcmd == "addRange")
							{
							  print ("<button onclick='return jsinsertRange();' type='submit' class='btn btn-success'>Update Range</button> ");
							}
							elseif ($strcmd == "viewRange")
							{
							  print ("<button onclick='return jsupdateRange();' type='submit' class='btn btn-success'>Update Range</button> ");
							}
							print  ("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>
							</div>
						  </div>");
						print ("</fieldset>");
						
						print ("<br/>");
						
						
						if ($strcmd == "viewRange")
						{
							print("<br/>");
							print("<legend>Add New Image</legend>");
							
							print ("<div class='form-group'>
										<label for='frm_image' class='col-sm-2 control-label'>Image</label>
										<div class='col-sm-10'>
										  <input style='width:600px;' type='file' class='form-control' id='frm_image' name='frm_image' />
										</div>
									</div>
									<div class='form-group'>
										<label for='frm_replacecurrent' class='col-sm-2 control-label'>Replace current range image</label>
										<div class='col-sm-1'>");
											print ("<input type='checkbox' id='frm_replacecurrent' name='frm_replacecurrent' value='1' />
										</div>
									</div>
									<div class='form-group'>
										<div class='col-sm-offset-2 col-sm-10'>");
											print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success'>Insert Image</button>
										</div>
									</div>");
							print ("<fieldset class='inlineLabels'> <legend>Manage Associated Products</legend>");
							
							$getProductsQuery = "SELECT * FROM stock_group_information ORDER BY name ASC";
							$strType = "multi";
							$unassignedProducts = query($conn, $getProductsQuery, $strType);
							
							$getAssignedProductsQuery = "SELECT stock_group_information.*, stock_range_relations.stockOrder FROM stock_group_information INNER JOIN stock_range_relations ON stock_group_information.recordID = stock_range_relations.stockID WHERE rangeID = :rangeID ORDER BY stock_range_relations.stockOrder";
							$strType = "multi";
							$arrdbparam = array("rangeID" => $rangeID);
							$assignedProducts = query($conn, $getAssignedProductsQuery, $strType, $arrdbparam);
							$assignedProductOptions = "";
							
							foreach ($assignedProducts AS $assignedProduct)
							{
								$key = array_search($assignedProduct, $unassignedProducts);
								if($key !== false)
								{
									unset($unassignedProducts[$key]);
								}
								$assignedProductOptions .= "<li id='".$assignedProduct['recordID']."' data-order='".$assignedProduct['stockOrder']."'><span>".$assignedProduct['name']." (".$assignedProduct['metaLink'].")</span></li>";
							}
							
							
							print("<div class='form-group'>
									<div class='col-sm-4'>
										<label for='frm_stocklevels' class='control-label'>Products</label>
										<br/>
										<ul id='unassignedProducts'>");
											foreach($unassignedProducts AS $unassignedProduct)
											{
												print("<li id='".$unassignedProduct['recordID']."'><span>".$unassignedProduct['name']." (".$unassignedProduct['metaLink'].")</span></li>");																
											}
									print ("</ul>");
								print ("</div>");
								print ("<div class='col-sm-4'>
										<label for='frm_stocklevels' class='control-label'>Assigned Products</label>
										<br/>
										<ul id='assignedProducts'>");
											print ($assignedProductOptions);
								print ("</ul>");
							print ("</div>
							</div>
							</fieldset>");
							
							print ("<fieldset class='inlineLabels'> <legend>Site Blocks</legend>");
								$strdbsql = "SELECT * FROM site_block_position";
								$strType = "multi";
								$siteBlockPositions = query($conn, $strdbsql, $strType);
								$siteBlockPositionSelectOptions = "";
								
								print ("<div class='form-group'>");
									foreach($siteBlockPositions AS $siteBlockPosition)
									{
										print ("<div class='col-sm-2'>");
											print ("<h3>".$siteBlockPosition['description']."</h3>");
											print ("<ul class='blockPosList' id='pos-".$siteBlockPosition['recordID']."'>");
												$strdbsql = "SELECT * FROM site_blocks INNER JOIN site_block_relations ON site_block_relations.blockID = site_blocks.recordID WHERE site_block_relations.rangeID = :rangeID AND site_block_relations.positionID = :positionID ORDER BY site_block_relations.pageOrder";
												$strType = "multi";
												$arrdbparams = array(
																	"rangeID" => $rangeID,
																	"positionID" => $siteBlockPosition['recordID']
																);
												$siteBlocks = query($conn, $strdbsql, $strType, $arrdbparams);
												foreach($siteBlocks AS $siteBlock)
												{
													print ("<li id='".$siteBlock['recordID']."' data-order='".$siteBlock['pageOrder']."' data-pos='".$siteBlockPosition['recordID']."'><span>".$siteBlock['description']."</span></li>");
												}
											print ("</ul>");
										print ("</div>");
										$siteBlockPositionSelectOptions .= "<option value='".$siteBlockPosition['recordID']."'>".$siteBlockPosition['description']."</option>";
									}
								print ("</div>");
								print ("<div class='form-group'>");
									print ("<button onclick='return jsdeleteSiteBlocks();' type='button' class='btn btn-danger'>Delete Selected</button>");
									print ("<input type='hidden' name='deleteBlocks' id='deleteBlocks' />");
								print ("</div>");
								
								print ("<br/>");
								print ("<legend>Add Blocks to Range</legend>");
								print ("<div class='form-group'>");
									print ("<label for='frm_siteblocks' class='col-sm-2 control-label'>Available Blocks</label>");
									print ("<br/>");
									print ("<br/>");
									$strdbsql = "SELECT * FROM site_blocks ORDER BY description ASC";
									$strType = "multi";
									$siteBlocks = query($conn, $strdbsql, $strType);
									
									print ("<select name='frm_siteblocks[]' id='frm_siteblocks' class='form-control valid' style='width: 300px;' multiple>");
										foreach($siteBlocks AS $siteBlock)
										{
											print ("<option value='".$siteBlock['recordID']."'>".$siteBlock['description']."</option>");
										}
									print ("</select>");
									
								print ("</div>");
								print ("<div class='form-group'>");
									print ("<label for='frm_siteblockposition' class='col-sm-2 control-label'>Block Position</label>");
									print ("<br/>");
									print ("<br/>");
									
									print ("<select name='frm_siteblockposition' id='frm_siteblockposition' class='form-control valid' style='width: 300px;'>");
										print ($siteBlockPositionSelectOptions);
									print ("</select>");
									
								print ("</div>");
								print ("<div class='form-group'>
											<div class='col-sm-10'>");
												print ("<button onclick='return jsaddSiteBlock();' type='submit' class='btn btn-success'>Add Site Block</button> ");
									print ("</div>
										</div>");
							print ("</fieldset>");
						}
						
						break;
						
					default:
						
						$strdbsql = "SELECT * FROM stock_ranges ORDER BY rangeName ASC";
						$strType = "multi";
						$ranges = query($conn, $strdbsql, $strType);
						
						print ("<button onclick='return jsaddRange();' type='submit' class='btn btn-success'>Add New</button>");
						print ("<br/>");
						print ("<br/>");
						print ("<table class='table table-striped table-bordered table-hover table-condensed' id='ranges-table'>");
							print ("<thead>");
								print ("<th>Range Name</th>");
								print ("<th>Page Link</th>");
								print ("<th></th>");
							print ("</thead>");
							print ("<tbody>");
							if (count($ranges) > 0)
							{
								foreach($ranges AS $range)
								{
									print ("<tr>");
										print ("<td>".$range['rangeName']."</td>");
										print ("<td>".$range['metaPageLink']."</td>");
										print ("<td><button onclick='return jsviewRange(\"".$range['recordID']."\");' type='submit' class='btn btn-primary'>Update Range</button></td>");
									print ("</tr>");
								}
							}
							print ("</tbody>");
						print ("</table>");
						print ("<button onclick='return jsaddRange();' type='submit' class='btn btn-success'>Add New</button>");
					
						break;
				}
				
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#ranges-table').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>