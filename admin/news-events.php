<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * news-events.php					                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

// ************* Common page setup ******************** //
	//=====================================================//
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "news-events"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['eventItemID'])) $eventItemID = $_REQUEST['eventItemID']; else $eventItemID = "";
	$strItemType = 2;
	
	switch($strcmd)
	{
		case "insertEventItem":
		case "updateEventItem":
			
			$formattedDate = str_replace("/", "-", $_POST['frm_date']);
			//echo $formattedDate;
			$dateTimestamp = strtotime($formattedDate);
			
			$arrdbparams = array(
								"title" => $_POST['frm_title'],
								"content" => $_POST['frm_content'],
								"date" => $dateTimestamp
							);
							
							//echo strtotime(str_replace("/", "-", $_POST['frm_date']));
			
			if ($strcmd == "insertEventItem")
			{	
				$strdbsql = "INSERT INTO site_news_events (title, content, date, type) 
							VALUES (:title, :content, :date, ".$strItemType.")";
				$strType = "insert";
			}
			elseif ($strcmd == "updateEventItem")
			{
				$strdbsql = "UPDATE site_news_events SET title = :title, content = :content, date = :date, type = ".$strItemType." WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $eventItemID;
				$strType = "update";
			}
			
			$updateEventItem = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertEventItem")
			{
				$eventItemID = $updateEventItem;
			}
			
			if ($strcmd == "insertEventItem")
			{
				if ($updateEventItem > 0)
				{
					$strsuccess = "Event item successfully added";
				}
				elseif ($updateEventItem == 0)
				{
					$strerror = "An error occurred while adding the brand";
				}
			}
			elseif ($strcmd == "updateEventItem")
			{
				if ($updateEventItem <= 1)
				{
					$strsuccess = "Event item successfully updated";
				}
				elseif ($updateEventItem > 1)
				{
					$strwarning = "An error may have occurred while updating this event item";
				}
			}
			
			$strcmd = "viewEventItem";
			
		break;
		
		case "deleteEventItem":
			
			$strdbsql = "DELETE FROM site_news_events WHERE recordID = :eventItemID";
			$strType = "delete";
			$arrdbparams = array( "eventItemID" => $eventItemID );
			$deleteEventItem = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strcmd = "";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Events Control</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddEventItem() {
					document.form.cmd.value="addEventItem";
					document.form.submit();
				}
				function jsviewEventItem(eventItemID) {
					document.form.cmd.value="viewEventItem";
					document.form.eventItemID.value=eventItemID;
					document.form.submit();
				}
				function jsdeleteEventItem(eventItemID) {
					if(confirm("Are you sure you want to delete this event item?"))
					{
						document.form.cmd.value="deleteEventItem";
						document.form.eventItemID.value=eventItemID;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertEventItem() {
					document.form.cmd.value='insertEventItem';
					document.form.submit();
				}
				function jsupdateEventItem() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateEventItem';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
			</script>
			<?php
		
			print ("<form action='news-events.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				print ("<input type='hidden' name='eventItemID' id='eventItemID' value='".$eventItemID."' />");
				print ("<input type='hidden' name='itemType' id='itemType' value='".$strItemType."' />");
				
				switch($strcmd)
				{
					case "viewEventItem":
					case "addEventItem":
						
						if ($strcmd == "viewEventItem")
						{
							$strdbsql = "SELECT * FROM site_news_events WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $eventItemID);
							$eventItemDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							/*print ("<pre>");
							print_r($eventItemDetails);
							print ("</pre>");*/
							$date = date("d/m/Y", $eventItemDetails['date']);
							
							print ("<fieldset class='inlineLabels'> <legend>Change Event Item Details</legend>");
						}
						elseif ($strcmd == "addEventItem")
						{
							$eventItemDetails = array(
												"title" => "",
												"date" => "",
												"content" => ""
											);
							$date = "";
							print ("<fieldset class='inlineLabels'> <legend>Add Event Item Details</legend>");
						}
						
						print ("<div class='form-group'>
							<label for='frm_title' class='col-sm-2 control-label'>Title</label>
							<div class='col-sm-10'>
							  <input style='width:800px;' type='text' class='form-control' id='frm_title' name='frm_title' value='".$eventItemDetails['title']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_date' class='col-sm-2 control-label'>Date</label>
							<div class='col-sm-10'>
							  <input style='width:800px;' type='text' class='form-control' id='frm_date' name='frm_date' value='".$date."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_content' class='col-sm-2 control-label'>Content</label>
							<div class='col-sm-8'>
							  <textarea class='form-control' id='frm_content' name='frm_content' rows='12'>".$eventItemDetails['content']."</textarea>
							</div>
						  </div>");
					print ("<div class='form-group'>
								<div class='col-sm-10'>");
								if ($strcmd == "addEventItem")
								{
								  print ("<button onclick='return jsinsertEventItem();' type='submit' class='btn btn-success'>Update Event Item</button> ");
								}
								elseif ($strcmd == "viewEventItem")
								{
								  print ("<button onclick='return jsupdateEventItem();' type='submit' class='btn btn-success'>Update Event Item</button> ");
								}
								print  ("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>
								</div>
						  </div>");
						print ("</fieldset>");
					
						break;
						
					default:
						
						$strdbsql = "SELECT * FROM site_news_events WHERE type = '".$strItemType."'";
						$strType = "multi";
						$eventItems = query($conn, $strdbsql, $strType);
						
						print ("<button onclick='return jsaddEventItem();' type='submit' class='btn btn-success'>Add New</button>");
						print ("<br/>");
						print ("<br/>");
						print ("<table id='events-table' class='table table-striped table-bordered table-hover table-condensed' >");
							print ("<thead><tr>");
								print ("<th>Title</th>");
								print ("<th>Date</th>");
								print ("<th>Edit</th>");
								print ("<th>Delete</th>");
							print ("</tr></thead><tbody>");
							foreach($eventItems AS $eventItem)
							{
								print ("<tr>");
									print ("<td>".$eventItem['title']."</td>");
									print ("<td>".date("d/m/y", $eventItem['date'])."</td>");
									print ("<td><button onclick='return jsviewEventItem(\"".$eventItem['recordID']."\");' type='submit' class='btn btn-primary'>Update Event Item</button></td>");
									print ("<td><button onclick='return jsdeleteEventItem(\"".$eventItem['recordID']."\");' type='submit' class='btn btn-danger'>Delete Event Item</button></td>");
								print ("</tr>");
							}
							print ("</tbody>");
						print ("</table>");
						print ("<br/>");
						print ("<button onclick='return jsaddEventItem();' type='submit' class='btn btn-success'>Add New</button>");
						
						
						break;
				}
				
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#events-table').DataTable();
			
			$( "#frm_date" ).datepicker({
				minDate: 0,
				dateFormat: 'dd/mm/yy'
			});
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>