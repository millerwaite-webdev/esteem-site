<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * index.php                                                          * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "contact"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database

	
	if (isset($_REQUEST['contentType'])) $contentType = $_REQUEST['contentType']; else $contentType = "";
	$metaPageLink = "contact";
	
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//

	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	switch($strcmd)
	{
		case "":
			
		break;
	}
	
	//get the page information
	$strdbsql = "SELECT * FROM site_pages WHERE metaPageLink = :metaPageLink";
	$returnType = "single";
	$dataParam = array("metaPageLink"=>$metaPageLink);
	$meta = query ($conn, $strdbsql, $returnType, $dataParam);
	
	
	

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	//include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//

	//print("<div id='mainContent'>");

		//Print out debug and error messages
		if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
		if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
		if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

?>		<div class='slider'>
			<div id='wrapper'>
				<iframe style='width: 100%; height: 280px;' frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDN9yYtcvzduLKNtoSaKROpogx9el6w4mM&q=Liverpool,+Merseyside+L1+3DS" allowfullscreen>
				</iframe>
			</div>
		</div>
<?php
		print ("<div class='content'>");
			print ("<h1 style='padding-top: 20px;'>Contact Us</h1>");
		print ("</div>");
		print ("<div class='content'>");
	//print("</div>");

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>