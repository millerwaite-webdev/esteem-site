<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * export-files.php	                                               * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "generate-export-file"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	
	// output headers so that the file is downloaded rather than displayed
	//header('Content-Type: text/csv; charset=utf-8');
	switch($_POST['frm_exportFormatType'])
	{
		case "stock":
			
			$strFileName = "stockExport_".date("d-m-Y_H-i-s", time()).".csv";
			
		break;
		
		case "catalogueAll":
		case "catalogueCategories":
		case "catalogueRanges":
			
			$strFileName = "catalogueExport_".date("d-m-Y_H-i-s", time()).".csv";
			
		break;
	}
	
	//header('Content-Disposition: attachment; filename='.$strFileName.'');
	

	// create a file pointer connected to the output stream
	//$output = fopen('php://output', 'w');
	
	if(!empty($_POST['frm_stock']))
	{
		$strStockIDs = implode(", ",$_POST['frm_stock']);
	}
	else
	{
		$strStockIDs = null;
	}
	
	if(!empty($_POST['frm_cats']))
	{
		$strCatIDs = implode(", ",$_POST['frm_cats']);
	}
	else
	{
		$strCatIDs = null;
	}
	
	if(!empty($_POST['frm_ranges']))
	{
		$strRangeIDs = implode(", ",$_POST['frm_ranges']);
	}
	else
	{
		$strRangeIDs = null;
	}
	
	$exportedRows = array();
	
	switch($_POST['frm_exportFormatType'])
	{
		case "stock":
			
			// output the column headings
			//fputcsv($output, array('metaLink', 'name', 'description', 'retailPrice', 'metaDoctitle', 'metaDescription', 'metaKeywords', 'status', 'brand', 'brandOrder', 'stockCode', 'price', 'priceControl', 'width', 'height', 'depth', 'diameter', 'dimensionsDescription', 'base', 'finish', 'colour', 'fabric'));
			$exportedRows[] = array('metaLink', 'name', 'description', 'retailPrice', 'metaDoctitle', 'metaDescription', 'metaKeywords', 'status', 'brand', 'brandOrder', 'stockCode', 'price', 'priceControl', 'width', 'height', 'depth', 'diameter', 'dimensionsDescription', 'base', 'finish', 'colour', 'fabric');
			//var_dump($exportedRows);
			// fetch the data
			$groupsExportQuery = "SELECT sgi.recordID, sgi.optionsList, sgi.metaLink, sgi.name, sgi.description, sgi.retailPrice, sgi.metaDoctitle, sgi.metaDescription, sgi.metaKeywords, stock_status.description AS status, metaPageLink, brandOrder FROM stock_group_information sgi INNER JOIN stock_status ON sgi.statusID = stock_status.recordID LEFT JOIN stock_brands ON sgi.brandID = stock_brands.recordID";
			
			if(!empty($strStockIDs) || !empty($strCatIDs) || !empty($strRangeIDs))
			{
				$groupsExportQueryWhere = " WHERE ";
				$boolPreCriteria = false;
				
				if(!empty($strStockIDs))
				{
					$groupsExportQueryWhere .= "sgi.recordID IN (".$strStockIDs.")";
					$boolPreCriteria = true;
				}
				if(!empty($strCatIDs))
				{
					$groupsExportQuery .= " INNER JOIN stock_category_relations scr ON sgi.recordID = scr.stockID";
					if($boolPreCriteria)
					{
						$groupsExportQueryWhere .= " AND ";
					}
					$groupsExportQueryWhere .= "scr.categoryID IN (".$strCatIDs.")";
					$boolPreCriteria = true;
				}
				if(!empty($strRangeIDs))
				{
					$groupsExportQuery .= " INNER JOIN stock_range_relations sr ON sgi.recordID = sr.stockID";
					if($boolPreCriteria)
					{
						$groupsExportQueryWhere .= " AND ";
					}
					$groupsExportQueryWhere .= "sr.rangeID IN (".$strRangeIDs.")";
					$boolPreCriteria = true;
				}
				
				$groupsExportQuery .= $groupsExportQueryWhere;
			}
			
			$strType = "multi";
			
			//echo $groupsExportQuery;
			$groupsExport = query($conn, $groupsExportQuery, $strType);
			//var_dump($groupsExport);
			
			// loop over the rows, outputting them
			foreach($groupsExport AS $groupExport)
			{
				if($groupExport['optionsList'] != "")
				{
					$arrGroupOptions = explode(",", $groupExport['optionsList']);
				}
				else
				{
					$arrGroupOptions = array();
				}
				$getStockQuery = "SELECT stock.recordID, stock.stockCode, stock.price, stock.priceControl, stock_dimensions.width, stock_dimensions.height, stock_dimensions.depth, stock_dimensions.diameter, stock_dimensions.description AS dimensionsDescription FROM stock LEFT JOIN stock_dimensions ON stock.dimensionID = stock_dimensions.recordID WHERE stock.groupID = :groupID";
				$strType = "multi";
				$arrdbparam = array("groupID" => $groupExport['recordID']);
				$stockItems = query($conn, $getStockQuery, $strType, $arrdbparam);
				//var_dump($stockItems);
				
				$i = 1;
				foreach($stockItems AS $stockItem)
				{
					$options = array();
					
					if(!empty($arrGroupOptions))
					{
						foreach($arrGroupOptions AS $arrGroupOption)
						{
							$getStockAttributeQuery = "SELECT stock_attributes.description FROM stock_attributes INNER JOIN stock_attribute_relations ON stock_attributes.recordID = stock_attribute_relations.stockAttributeID WHERE stock_attribute_relations.stockID = :stockID AND stock_attributes.attributeTypeID = :attributeType";
							$strType = "single";
							$arrdbparams = array(
												"stockID" => $stockItem['recordID'],
												"attributeType" => $arrGroupOption
											);
							//echo $getStockAttributeQuery;
							//var_dump($arrdbparams);
							$stockAttribute = query($conn, $getStockAttributeQuery, $strType, $arrdbparams);
							
							$options[$arrGroupOption] = $stockAttribute['description'];
						}
					}
					
					$exportRow = array();
					
					if($i == 1)
					{
						$exportRow[] = $groupExport['metaLink'];
						$exportRow[] = $groupExport['name'];
						$exportRow[] = $groupExport['description'];
						$exportRow[] = $groupExport['retailPrice'];
						$exportRow[] = $groupExport['metaDoctitle'];
						$exportRow[] = $groupExport['metaDescription'];
						$exportRow[] = $groupExport['metaKeywords'];
						$exportRow[] = $groupExport['status'];
						$exportRow[] = $groupExport['metaPageLink'];
						$exportRow[] = $groupExport['brandOrder'];
					}
					else
					{
						$exportRow[] = "";
						$exportRow[] = "";
						$exportRow[] = "";
						$exportRow[] = "";
						$exportRow[] = "";
						$exportRow[] = "";
						$exportRow[] = "";
						$exportRow[] = "";
						$exportRow[] = "";
						$exportRow[] = "";
					}
					
					$exportRow[] = $stockItem['stockCode'];
					$exportRow[] = $stockItem['price'];
					$exportRow[] = $stockItem['priceControl'];
					$exportRow[] = $stockItem['width'];
					$exportRow[] = $stockItem['height'];
					$exportRow[] = $stockItem['depth'];
					$exportRow[] = $stockItem['diameter'];
					$exportRow[] = $stockItem['dimensionsDescription'];
					$exportRow[] = $options[1];
					$exportRow[] = $options[2];
					$exportRow[] = $options[3];
					$exportRow[] = $options[4];
					
					
					//fputcsv($output, $exportRow);
					$exportedRows[] = $exportRow;
					
					$i++;
				}
			}
			
		break;
		
		case "catalogueAll":
		case "catalogueCategories":
		case "catalogueRanges":
			
			// output the column headings
			$exportedRows[] = array('stockGroupMetaLink', 'catMetaPageLink', 'stockOrder', 'catType');
			
			if($_POST['frm_exportFormatType'] == "catalogueAll" || $_POST['frm_exportFormatType'] == "catalogueCategories")
			{
				$catRelationsExportQuery = "SELECT sgi.metaLink, category.metaPageLink, scr.stockOrder FROM stock_category_relations scr INNER JOIN stock_group_information sgi ON scr.stockID = sgi.recordID INNER JOIN category ON scr.categoryID = category.recordID";
				
				if(!empty($strStockIDs) || !empty($strCatIDs))
				{
					$catRelationsExportQueryWhere = " WHERE ";
					$boolPreCriteria = false;
					
					if(!empty($strStockIDs))
					{
						$catRelationsExportQueryWhere .= "sgi.recordID IN (".$strStockIDs.")";
						$boolPreCriteria = true;
					}
					if(!empty($strCatIDs))
					{
						if($boolPreCriteria)
						{
							$catRelationsExportQueryWhere .= " AND ";
						}
						$catRelationsExportQueryWhere .= "scr.categoryID IN (".$strCatIDs.")";
						$boolPreCriteria = true;
					}
					
					$catRelationsExportQuery .= $catRelationsExportQueryWhere;
				}
				
				$catRelationsExportQuery .= " ORDER BY scr.stockOrder ASC";
				$strType = "multi";
				echo $catRelationsExportQuery;
				$catRelationsExport = query($conn, $catRelationsExportQuery, $strType);
				//var_dump($catRelationsExport);
				
				// loop over the rows, outputting them
				foreach($catRelationsExport AS $catRelationExport)
				{
					$exportRow = array();
					$exportRow[] = $catRelationExport['metaLink'];
					$exportRow[] = $catRelationExport['metaPageLink'];
					$exportRow[] = $catRelationExport['stockOrder'];
					$exportRow[] = "category";
					$exportedRows[] = $exportRow;
				}
			}
			
			if($_POST['frm_exportFormatType'] == "catalogueAll" || $_POST['frm_exportFormatType'] == "catalogueRanges")
			{
				$rangeRelationsExportQuery = "SELECT sgi.metaLink, stock_ranges.metaPageLink, srr.stockOrder FROM stock_range_relations srr INNER JOIN stock_group_information sgi ON srr.stockID = sgi.recordID INNER JOIN stock_ranges ON srr.rangeID = stock_ranges.recordID";
				
				if(!empty($strStockIDs) || !empty($strRangeIDs))
				{
					$rangeRelationsExportQueryWhere = " WHERE ";
					$boolPreCriteria = false;
					
					if(!empty($strStockIDs))
					{
						$rangeRelationsExportQueryWhere .= "sgi.recordID IN (".$strStockIDs.")";
						$boolPreCriteria = true;
					}
					if(!empty($strRangeIDs))
					{
						if($boolPreCriteria)
						{
							$rangeRelationsExportQueryWhere .= " AND ";
						}
						$rangeRelationsExportQueryWhere .= "srr.rangeID IN (".$strRangeIDs.")";
						$boolPreCriteria = true;
					}
					
					$rangeRelationsExportQuery .= $rangeRelationsExportQueryWhere;
				}
				
				$rangeRelationsExportQuery .= " ORDER BY srr.stockOrder ASC";				
				$strType = "multi";
				//echo $rangeRelationsExportQuery;
				$rangeRelationsExport = query($conn, $rangeRelationsExportQuery, $strType);
				//var_dump($rangeRelationsExport);
				
				// loop over the rows, outputting them
				foreach($rangeRelationsExport AS $rangeRelationExport)
				{
					$exportRow = array();
					$exportRow[] = $rangeRelationExport['metaLink'];
					$exportRow[] = $rangeRelationExport['metaPageLink'];
					$exportRow[] = $rangeRelationExport['stockOrder'];
					$exportRow[] = "range";
					$exportedRows[] = $exportRow;
				}
			}
			
		break;
	}
	//var_dump($exportedRows);
	if(!empty($exportedRows))
	{
		dataexportcsv($exportedRows, $strFileName);
	}
	
	$conn = null;
?>