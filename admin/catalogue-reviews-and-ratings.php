<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * catalogue-reviews-and-ratings.php                                  * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "catalogue-reviews-and-ratings"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['reviewID'])) $reviewID = $_REQUEST['reviewID']; else $reviewID = "";
	
	switch($strcmd)
	{
		case "disable":
		case "enable":
			
			if ($strcmd == "enable")
			{
				$shownOnSite = 1;
			}
			elseif ($strcmd == "disable")
			{
				$shownOnSite = 0;
			}
			
			$toggleReviewQuery = "UPDATE stock_reviews SET shownOnSite = :shownOnSite WHERE recordID = :recordID";
			$strType = "update";
			$arrdbparams = array(
							"shownOnSite" => $shownOnSite,
							"recordID" => $reviewID
						);
			$toggleReview = query($conn, $toggleReviewQuery, $strType, $arrdbparams);
			
			if ($toggleReview <= 1)
			{
				$strsuccess = "Review toggled";
			}
			elseif ($toggleReview > 1)
			{
				$strwarning = "Review may not have been toggled";
			}
			
			
			$strcmd = "";
			$reviewID = "";
		
		break;
		
		case "saveResponse":
			
			if($_POST['frm_reviewresponse'] == "")
			{
				$dateResponse = 0;
			}
			else
			{
				$dateResponse = time();
			}
			
			$saveResponseQuery = "UPDATE stock_reviews SET response = :response, dateResponse = '".$dateResponse."' WHERE recordID = :recordID";
			$strType = "update";
			$arrdbparams = array(
							"response" => $_POST['frm_reviewresponse'],
							"recordID" => $reviewID
						);
			$saveResponse = query($conn, $saveResponseQuery, $strType, $arrdbparams);
			
			if ($saveResponse <= 1)
			{
				$strsuccess = "Response saved";
			}
			elseif ($saveResponse > 1)
			{
				$strwarning = "Response may not have been saved";
			}
			
			
			$strcmd = "manageResponse";
		
		break;
		
		case "deleteReview":
			
			$deleteReviewQuery = "DELETE FROM stock_reviews WHERE recordID = :recordID";
			$strType = "delete";
			$arrdbparams = array(
							"recordID" => $reviewID
						);
			$deleteReview = query($conn, $deleteReviewQuery, $strType, $arrdbparams);
		
			if ($deleteReview <= 1)
			{
				$strsuccess = "Review deleted.";
			}
			elseif ($deleteReview > 1)
			{
				$strwarning = "Review may not have been deleted.";
			}
			
			
			$strcmd = "";
		
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Customer Reviews</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jstoggleReview(reviewID, cmd) {
					document.form.cmd.value=cmd;
					document.form.reviewID.value=reviewID;
					document.form.submit();
				}
				function jsrespondReview(reviewID) {
					document.form.cmd.value="manageResponse";
					document.form.reviewID.value=reviewID;
					document.form.submit();
				}
				function jssaveResponse(reviewID) {
					document.form.cmd.value="saveResponse";
					document.form.reviewID.value=reviewID;
					document.form.submit();
				}
				function jsdeleteReview(reviewID) {
					if(confirm("Are you sure you want to delete this page?"))
					{
						document.form.cmd.value="deleteReview";
						document.form.reviewID.value=reviewID;
						document.form.submit();
					}
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				$(document).ready(function(){
					$(".cmn-toggle").click(function(){
					
						var idstring = $(this).attr('id');
					
						if(this.checked) {
							var show = 1;
						}
						else {
							var show = 0;
						}
						
						var idsplit = idstring.split('-')[2];
						
						$.ajax({
							type: "GET",
							url: "/admin/includes/ajax_reviewsvisibility.php?reviewID="+idsplit+"&visibility="+show
						});
						
					});
				});
			</script>
			<?php
		
			print("<form action='catalogue-reviews-and-ratings.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='reviewID' id='reviewID' value='$reviewID' />");
				
				switch($strcmd)
				{						
					default:
						
						print("<div class='section'>");
							print("<table id='reviews-table' class='table table-striped table-bordered table-hover table-condensed' >");
								print("<thead>");
									print("<tr>");
										print("<th width='10%'>Date</th>");
										print("<th width='20%'>Title</th>");
										print("<th width='40%'>Comments</th>");
										print("<th width='10%' style='text-align:center;'>Rating</th>");
										print("<th width='10%' style='text-align:center;'>Show/Hide</th>");
										print("<th width='10%' style='text-align:center;'>Delete</th>");
									print("</tr>");
								print("</thead>");
								print("<tbody>");
								
								$getReviewsQuery = "SELECT stock_reviews.*, customer.title AS custTitle, customer.firstname, customer.surname FROM stock_reviews LEFT JOIN customer ON stock_reviews.memberID = customer.recordID";
								$strType = "multi";
								$reviews = query($conn, $getReviewsQuery, $strType);
								
								if (count($reviews) != 0) {
									foreach ($reviews as $review) {
										print("<tr>");
											print("<td>".date("d/m/Y", $review['dateReview'])."</td>");
											print("<td><p class='cut'>".$review['title']."</p></td>");
											print("<td><p class='cut'>".$review['description']."</p></td>");
											print("<td style='text-align:center;'>".$review['numStars']."</td>");
										//	if ($review['dateResponse'] > 0)
										//	{
											//	$strDate = date("d/m/Y", $review['dateResponse']);
										//	}
										//	else
										//	{
											//	$strDate = "N/A";
										//	}
										//	print("<td>".$strDate."</td>");
										print("<td style='text-align:center;'>");
											print("<div class='switch' style='display:inline-block;'>");
												if($review['shownOnSite'])
												{
													print("<input id='cmn-toggle-".$review['recordID']."' class='cmn-toggle cmn-toggle-round' type='checkbox' checked>");
												}
												else
												{
													print("<input id='cmn-toggle-".$review['recordID']."' class='cmn-toggle cmn-toggle-round' type='checkbox'>");
												}
												print("<label for='cmn-toggle-".$review['recordID']."' class='cmn-toggle-label'></label>");
											print("</div>");
										print("</td>");
										print("<td style='text-align:center;'><button onclick='jsdeleteReview(".$review['recordID']."); return false;' class='center btn btn-danger circle' type='submit' style='display:inline-block;'><i class='fa fa-trash'></i></button></td>");				
									print("</tr>");
									}
								}/* else {
									print("<tr><td colspan='7'>Currently No Reviews</td></tr>");
								}*/
								print("</tbody>");
							print("</table>");
						print("</div>");
						
						break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
				
			$('#reviews-table').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>