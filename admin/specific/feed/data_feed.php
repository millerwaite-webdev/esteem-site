<?php namespace Feed;

require("../../classes/Feed/ProductLine.php");
require("../../classes/Feed/CustomerLine.php");
require("../../classes/Feed/CategoryLine.php");
require("../../classes/Feed/ThreeSixtyPath.php");
require("../../classes/database/Brand.php");
require("../../classes/database/Category.php");
require("../../classes/database/Customer.php");
require("../../classes/database/CustomerAddress.php");
require("../../classes/database/Stock.php");
require("../../classes/database/StockAttribute.php");
require("../../classes/database/StockCategory.php");
require("../../classes/database/StockCustomField.php");
require("../../classes/database/StockGroup.php");
require("../../classes/database/StockImage.php");

use Classes\CategoryLine;
use Classes\CustomerLine;
use Classes\ProductLine;
use Classes\ThreeSixtyPath;
use Classes\NotImplementedException;
use Database\Brand;
use Database\Category;
use Database\CategoryDetail;
use Database\Customer;
use Database\CustomerAddress;
use Database\CustomerAddressDetail;
use Database\CustomerDetail;
use Database\Stock;
use Database\StockAttribute;
use Database\StockCategory;
use Database\StockCustomField;
use Database\StockGroup;
use Database\StockGroupInformationDetail;
use Database\StockImage;
use Database\StockImageDetail;
use DateTime;
use Goodby\CSV\Export\Standard\CsvFileObject;
use Goodby\CSV\Export\Standard\Exporter;
use Goodby\CSV\Import\Standard\Lexer;
use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\LexerConfig;
use Goodby\CSV\Export\Standard\ExporterConfig;
use SplObjectStorage;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class FeedProcessor
{
    /**
     * Magento Feed Processor (for Magento 1.3.2.1)
     * @param $csvFileName // e.g. 'data.csv'
     * @param $importPath // e.g. 'C:\Development\Edwards\Files\edwardsfiles_split~~\httpdocs\media\catalog\product\'
     * @param $exportPath //e.g. 'C:\Development\JHBenson\Website\images\products\'
     */
    function __construct($csvFileName, $importPath, $exportPath)
    {
        $this->importPath = $importPath;
        $this->exportPath = $exportPath;
        $this::Initialise($csvFileName);
    }

    /**
     * @param $csvFileName
     */
    public function Initialise($csvFileName)
    {
        $this->csvFileName = $csvFileName;
        // setup CVS import
        $this->config = new LexerConfig();
        $this->config
            ->setDelimiter(",") // Customize delimiter. Default value is comma(,)
            ->setEscape("\\") // Customize escape character. Default value is backslash(\)
            ->setToCharset('UTF-8'); // Customize target encoding. Default value is null, no converting.
        // setup logger
        // create a log channel
        $this->log = new Logger('Data Feed Logger');
        $this->log->pushHandler(new StreamHandler('php://stdout', Logger::INFO));
        $this->log->addInfo("Setup Stream Handler");
        // check export path exists
        $this->checkFolderExistsIfNotCreate($this->importPath);
    }

    #region Properties
    /**
     * @var string Image import path
     */
    private $importPath;
    /**
     * @var string Image export path
     */
    private $exportPath;
    /**
     * @var string Name of CSV file to import
     */
    private $csvFileName;
    /**
     * @var Lexer LexerConfig
     */
    public $config;
    /**
     * @var SplObjectStorage Collection of categories
     */
    public $categories;
    /**
     * @var SplObjectStorage Collection of products
     */
    public $products;
    /**
     * @var SplObjectStorage Collection of customer
     */
    public $customers;
    /**
     * @var int pointer in product/category/customer collection
     */
    private $count = 0;
    /**
     * @var Logger Log Channel
     */
    private $log;
    /**
     * @var SplObjectStorage Collection of 360 (spin) objects
     */
    private $threesixtyobjects;
    #endregion

    /**
     * Import Categores
     */
    public function importCategories()
    {
        $this->getCategoryRecords();
        $this->pushCategoryRecords();
    }

    /**
     * Import Magento CSV file into database
     */
    public function importProducts()
    {
        $this->getProductRecords();
        $this->pushProductCollectionToDatabase();
    }

    public function importCustomers()
    {
        $this->getCustomerRecords();
        $this->pushCustomersToDatabase();
    }

    /**
     * Export CSV file from database
     */
    public function export()
    {
        throw new NotImplementedException();
    }

    #region Categories
    /**
     * Get category records from CSV
     *
     * @return int // Return how many records where processed
     */
    public function getCategoryRecords()
    {
        $this->log->addInfo("Reading category records");
        $this->count = 0;
        // build array of records
        $this->categories = new SplObjectStorage();
        $lexer = new Lexer($this->config);
        $interpreter = new Interpreter();
        $interpreter->addObserver(function (array $row) use (&$product)
        {
            // skip over headings
            if ($this->count > 0) {
                // only import categories with a value
                if (strlen($row[15]) == 0)
                    $this->log->addInfo(sprintf("Skipping blank category; Id: %d", $row[0]));
                else {
                    $this->log->addInfo(sprintf("Adding Category Row: %d, Id: %s, Value: %s", $this->count, $row[0], $row[15]));
                    $this->categories->attach(new CategoryLine($row[0], $row[3], $row[4], $row[5], $row[6], $row[8], $row[15]));
                    $this->count++;
                }
            }
            $this->count++;
        });
        $lexer->parse($this->csvFileName, $interpreter);
        // return record count
        $this->log->addInfo(sprintf("Added %d categories", $this->categories->count()));
        return $this->categories->count();
    }

    /**
     * Process imported hierarchical category records
     */
    public function pushCategoryRecords()
    {
        $this->log->addInfo(sprintf("Processing %d category records", count($this->categories)));
        // current level/depth we are dealing with
        $currentlevel = 0;
        while ($currentlevel <= 4)
        {
            $this->categories->rewind();
            $ordering = 1;
            $parentid = 0;
            $this->log->addInfo(sprintf("Processing level; %d", $currentlevel));
            // iterate through categories
            while($this->categories->valid())
            {
                $category = $this->categories->current();
                // is the category at the current level
                if ($category->level == $currentlevel)
                {
                    $thevalue = $category->value; // debugging use only
                    $this->log->addInfo(sprintf("Checking Category; %s", $category));
                    if (!Category::checkCategoryExists(Category::getMagentoNameToEnglishCategoryName($category->value)))
                    {
                        $createdcategory = Category::createCategory(
                            new CategoryDetail(
                                Category::getMagentoNameToEnglishCategoryName($category->value),
                                '', '', '', '', '', $category->value, '', '', 1, 1, 1, 1, 1));
                        $this->log->addInfo(sprintf("Created category %s", $createdcategory));
                        // decide parent
                        if ($currentlevel == 0) {
                            $parentid = $createdcategory;
                        } else {
                            // find parent category
                            $parentid = $category->parentId;
                            $parentcategory = $this->findCategory($parentid);
                            if ($parentcategory != null)
                            {
                                $returnedcategory = Category::getCategoryByName(
                                    Category::getMagentoNameToEnglishCategoryName($category->value));
                                $parentid = $returnedcategory->recordId;
                            }
                            else {
                                $this->log->addInfo(sprintf("Skipped as parent category could not be found; Magento Parent Id %d", $parentid));
                            }
                        }
                        // add category relation
                        $this->log->addInfo(sprintf("Adding category relation; Parent Id: %d, Child Id: %d, Ordering: %d, Current Level: %d",
                            $parentid, $createdcategory, $ordering, $currentlevel));
                        StockCategory::createCategoryRelation($parentid, $createdcategory, $ordering, $currentlevel);
                        // TODO: *** Fudge ***, and makes the process much slower - SplStorage current reference gets broken when calling Category::createCategory and skips around 100 entities
                        $this->categories->rewind();
                    }
                    $ordering++;
                }
                $this->categories->next();
            }
            $currentlevel++;
        }
        $this->log->addInfo("Incrementing all levels by one");
        StockCategory::amendCategoryRelationValues();
        $this->log->addInfo("Finished processing category records");
    }

    private function findCategoryByValue($value)
    {
        foreach ($this->categories as $category)
        {
            if ($category->value == $value)
                return $category;
        }
        return null;
    }

    private function findCategory($entityid)
    {
        foreach ($this->categories as $category)
        {
            if ($category->entityid == $entityid)
                return $category;
        }
        return null;
    }
    #endregion

    #region Products
    /**
     * Pull records from CSV file
     */
    public function getProductRecords()
    {
        $this->log->addInfo("Reading product records");
        $this->count = 0;
        // build array of records
        $this->products = new SplObjectStorage();
        // parse CSV file
        $lexer = new Lexer($this->config);
        $interpreter = new Interpreter();
        $interpreter->addObserver(function (array $row) use (&$product)
        {
            // skip over headings
            if ($this->count > 0)
            {
                $this->log->addInfo(sprintf("Processing Product Row: %d, Name: %s, Type: %d", $this->count, $row[22], $row[3]));
                $this->products->attach(new ProductLine(
                    $row[0], // store
                    $row[1], // websites,
                    $row[2], // attribute_set
                    $row[3], // type
                    $row[4], // sku
                    $row[5], // category_ids
                    $row[6], // has_options
                    $row[7], // manufacturer
                    $row[8], // color
                    $row[9], // status
                    $row[10], // tax_class_id
                    $row[11], // visibility
                    $row[12], // size
                    $row[13], // home_page_featured
                    $row[14], // enable_googlecheckout
                    $row[15], // gender
                    $row[16], // fit
                    $row[17], // sole
                    $row[18], // product_type
                    $row[19], // web_colour
                    $row[20], // price
                    $row[21], // weight
                    $row[22], // name
                    $row[23], // meta_title
                    $row[24], // meta_description
                    $row[25], // image
                    $row[26], // small_image
                    $row[27], // thumbnail
                    $row[28], // url_key
                    $row[29], // url_path
                    $row[30], // custom_design
                    $row[31], // options_container
                    $row[32], // gift_message_available
                    $row[33], // image_label
                    $row[34], // small_image_label
                    $row[35], // thumbnail_label
                    $row[36], // page_layout
                    $row[37], // occasion
                    $row[38], // description
                    $row[39], // short_description
                    $row[40], // meta_keyword
                    $row[41], // custom_layout_update
                    $row[42], // qty
                    $row[43], // min_qty
                    $row[44], // use_config_min_qty
                    $row[45], // is_qty_decimal
                    $row[46], // backorders
                    $row[47], // use_config_backorders
                    $row[48], // min_sale_qty
                    $row[49], // use_config_min_sale_qty
                    $row[50], // max_sale_qty
                    $row[51], // use_config_max_sale_qty
                    $row[52], // is_in_stock
                    $row[53], // low_stock_date
                    $row[54], // notify_stock_qty
                    $row[55], // use_config_notify_stock_qty
                    $row[56], // manage_stock
                    $row[57], // use_config_manage_stock
                    $row[58], // stock_status_changed_automatically
                    $row[59], // product_name
                    $row[60], // store_id
                    $row[61], // product_type_id
                    $row[62], // product_status_changed
                    $row[63], // product_changed_websites
                    $row[64], // special_price
                    $row[65], // special_from_date
                    $row[66], // news_from_date
                    $row[67], // news_to_date
                    $row[68], // shoe_tree_size
                    $row[69], // cost
                    $row[70]  // bottle_size
                ));
            }
            $this->count++;
        });
        $lexer->parse($this->csvFileName, $interpreter);
        // return record count
        $this->log->addInfo(sprintf("Process %d products", $this->products->count()));
        return $this->products->count();
    }

    private function pushProductCollectionToDatabase()
    {
        $this->log->addInfo("Processing Brands");
        $this->pushBrands();
        $this->log->addInfo("Pushing Products");
        $this->pushProducts();
    }

    public function pushBrands()
    {
        // build brands
        $brands = array();
        foreach ($this->products as $product)
        {
            array_push($brands, $product->manufacturer);
        }
        $uniquebrands = array_unique($brands);
        $this->log->addInfo(sprintf("Found %d brands", count($uniquebrands)));
        // push into database
        // push brands to the database
        foreach ($uniquebrands as $uniquebrand)
        {
            $result = Brand::checkBrandExists($uniquebrand);
            if (!$result)
            {
                $this->log->addInfo(sprintf("Brand: %s doesn't exist - creating", $uniquebrand));
                // create brand
                $brandId = Brand::createBrand($uniquebrand);
                if ($brandId != null)
                    $this->log->addInfo(sprintf("Brand created Id: %d", $brandId));
            }
        }
    }

    private function pushProducts()
    {
        foreach ($this->products as $product)
        {
            $this->log->addInfo(sprintf("Processing Product: %s, SKU: %s", $product->product_name, $product->sku));
            // check whether product exist by sku or name
            $existbyname = Stock::checkStockExistsByName($product->product_name);
            $existbysku = Stock::checkStockExistsBySku($product->sku);
            if ($existbyname && $existbysku)
            {
                $this->log->addInfo("Product already exists, skipping");
                continue;
            }
            $groupId = $this->DecideStockGroup($product);
            // create product/stock
            $newstock = Stock::createStock(
                $product->price,
                $product->price,
                $product->sku,
                $product->qty,
                $product->sku,
                $groupId,
                $product->name);
            $this->log->addInfo(sprintf("Created new stock item: %d", $newstock));
            // associate with brand
            $brandexists = Brand::checkBrandExists($product->manufacturer);
            if (!$brandexists)
            {
                $this->log->addInfo(sprintf("Processing Brand: %s doesn't exist - creating", $product->manufacturer));
                Brand::createBrand($product->manufacturer);
            }
            $this->log->addInfo("Pushing Product Categories");
            $this->pushProductCategories($product, $groupId);
            $this->log->addInfo("Pushing Product Attributes");
            $this->pushProductAttributes($product, $newstock);
            $this->log->addInfo("Pushing Custom Field Values");
            $this->pushProductCustomFieldValues($product, $newstock);
            if ($product->image == null)
                $this->log->addInfo("Product has no associated images");
            else {
                $this->log->addInfo("Pushing Images");
                $this->pushProductImages($product, $newstock);
            }
            $this->log->addInfo(sprintf("Finished processing Product: %s, SKU: %s", $product->product_name, $product->sku));
        }
    }

    /**
     * Decide Product Group
     * @param $product
     * @return array|string
     */
    private function DecideStockGroup($product)
    {
        $tempgroupname = $this->DecideGroupName($product->name);
        $group = StockGroup::getGroupByName($tempgroupname);
        // create group as it
        if ($group == null)
        {
            $this->log->addInfo(sprintf("Group %s doesn't exist", $product->meta_title));
            $groupId = $this->pushProductGroupInformation($product);
            return $groupId;
        }
        else
        {
            $groupId = $group->recordId;
            return $groupId;
        }
    }

    /**
     * Decide Group Name
     * @param $productName
     * @return mixed
     */
    public function DecideGroupName($productName)
    {
        // attempt to create a good group name
        $strsplit = explode("-", $productName);
        if (count($strsplit) == 2)
            return $strsplit[0];
        else if (count($strsplit) > 2)
        {
            // add all parts of split together minus the last
            $finalstr = "";
            for ($x = 0; $x < count($strsplit) - 1;  $x++)
            {
                $finalstr .= $strsplit[$x] . '-';
            }
            return rtrim($finalstr, "-");
        }
        else
            return $productName;
    }

    private function pushProductCategories(ProductLine $product, $stockGroupId)
    {
        // now split the category_ids in component parts
        $categoryIds = $product->category_ids;
        $categorybreakdown = explode(",", $categoryIds);
        if (count($categorybreakdown) == 0)
            $this->log->addInfo("Found no product categories");
        $this->log->addInfo(sprintf("Found %d categories", count($categorybreakdown)));
        // push into database
        $catcount = 1;
        foreach ($categorybreakdown as $category)
        {
            $importedcategory = $this->findCategory($category);
            if ($importedcategory == null)
            {
                $this->log->addInfo(sprintf("Skipping Category %s not found", $category));
                continue;
            }
            $existingcategory = Category::getCategoryByName($importedcategory->value);
            if ($existingcategory == null)
            {
                $this->log->addInfo(
                    sprintf("Category %s doesn't exist", $importedcategory->value));
                continue;
            }
            $this->log->addInfo(
                sprintf("Adding Category Id: %s to Product/Stock; Stock Group Id: %d, Name: %s",
                    $existingcategory->recordId, $stockGroupId, $product->name));
            StockCategory::createCategoryStockRelation($stockGroupId, $existingcategory->recordId, $catcount);
            $catcount++;
        }
    }

    /**
     * @param ProductLine $productLine
     * @param $stockId
     * @return bool
     */
    private function pushProductAttributes(ProductLine $productLine, $stockId)
    {
        // colour
        $colourattribute = StockAttribute::getAttributeType("Colour");
        if ($colourattribute != null && strlen($productLine->color) > 0)
            $this->logCreatedProductAttribute(
                StockAttribute::assignProductAttribute($stockId, $productLine->color, $colourattribute));
        // web colour
        $webcolourattribute = StockAttribute::getAttributeType("WebColour");
        if ($webcolourattribute != null && strlen($productLine->web_colour) > 0)
            $this->logCreatedProductAttribute(
                StockAttribute::assignProductAttribute($stockId, $productLine->web_colour, $webcolourattribute));
    }

    /**
     * @param $createdids
     */
    private function logCreatedProductAttribute($createdids)
    {
        $this->log->addInfo(sprintf("Assigned product attribute; Attribute: %d, Relation: %d", $createdids[0], $createdids[1]));
    }

    private function pushProductCustomFieldValues(ProductLine $productLine, $stockId)
    {
        // TaxClass
        $taxfield = StockCustomField::getField("Tax Class");
        if ($taxfield != null)
            $this->logCustomFieldValue(
                StockCustomField::assignProductField($stockId, $taxfield, $productLine->tax_class_id));
        // Size
        $sizefield = StockCustomField::getField("Size");
        if ($sizefield != null)
            $this->logCustomFieldValue(
                StockCustomField::assignProductField($stockId, $sizefield, $productLine->size));
        // Shoe Tree Size
        $shoetreesizefield = StockCustomField::getField("Shoe Tree Size");
        if ($shoetreesizefield != null)
            $this->logCustomFieldValue(
                StockCustomField::assignProductField($stockId, $shoetreesizefield, $productLine->shoe_tree_size));
        // EnableGoogleCheckOut
        $enablegooglecheckoutfield = StockCustomField::getField("Enable Google CheckOut");
        if ($enablegooglecheckoutfield != null)
            $this->logCustomFieldValue(
                StockCustomField::assignProductField($stockId, $enablegooglecheckoutfield, $productLine->enable_googlecheckout));
        // Gender
        $genderfield = StockCustomField::getField("Gender");
        if ($genderfield != null)
            $this->logCustomFieldValue(
                StockCustomField::assignProductField($stockId, $genderfield, $productLine->gender));
        // Fit
        $fitfield = StockCustomField::getField("Fit");
        if ($fitfield != null)
            $this->logCustomFieldValue(
                StockCustomField::assignProductField($stockId, $fitfield, $productLine->fit));
        // Sole
        $solefield = StockCustomField::getField("Sole");
        if ($solefield != null)
            $this->logCustomFieldValue(
                StockCustomField::assignProductField($stockId, $solefield, $productLine->sole));
        // ProductType
        $producttypefield = StockCustomField::getField("Product Type");
        if ($producttypefield != null)
            $this->logCustomFieldValue(
                StockCustomField::assignProductField($stockId, $producttypefield, $productLine->product_type));
        // Complexity
        $complexityfield = StockCustomField::getField("Complexity");
        if ($complexityfield != null)
            $this->logCustomFieldValue(
                StockCustomField::assignProductField($stockId, $complexityfield, $productLine->product_type_id));
        // Occasion
        if (!$this->isNullOrEmptyString($productLine->occasion))
        {
            $occasionbreakdown = explode(",", $productLine->occasion);
            foreach ($occasionbreakdown as $o)
            {
                $customfield = StockCustomField::getField(trim($o));
                $this->logCustomFieldValue(
                    StockCustomField::assignProductField($stockId, $customfield, $productLine->product_type_id));
            }
        }
    }

    /**
     * @param $newfield
     */
    private function logCustomFieldValue($newfield)
    {
        if (count($newfield) > 0)
            $this->log->addInfo(sprintf("Assigned product field %d", $newfield));
    }

    /**
     * @param ProductLine $productLine
     * @return array|string
     */
    private function pushProductGroupInformation(ProductLine $productLine)
    {
        // create date
        $date = new DateTime();
        $timestamp = $date->getTimestamp();
        // create stock group
        $groupId = StockGroup::createGroupInformation(new StockGroupInformationDetail(
            $productLine->meta_title,
            $productLine->meta_description,
            $productLine->price,
            // meta
            // -- metaLink
            $productLine->url_path,
            // -- metaDocTitle
            $productLine->meta_title,
            // -- metaDescription
            $productLine->meta_description,
            // -- metaKeywords
            $productLine->meta_keyword,
            // modified
            $timestamp,
            // created
            $timestamp,
            // status
            $productLine->getProductStatusId(),
            // brand
            $productLine->getBrandId(),
            // brandorder
            NULL,
            // options list
            NULL
        ));
        if ($groupId > 0)
        {
            $this->log->addInfo(sprintf("Created Group Information; Group Id: %d", $groupId));
        }
        return $groupId;
    }
    #endregion

    #region Customers
    public function getCustomerRecords()
    {
        $this->log->addInfo("Retrieving customer records");
        $this->count = 0;
        // build array of records
        $this->customers = new SplObjectStorage();
        // parse CSV file
        $lexer = new Lexer($this->config);
        $interpreter = new Interpreter();
        $interpreter->addObserver(function (array $row) use (&$product)
        {
            // skip over headings
            if ($this->count > 0)
            {
                $this->log->addInfo(sprintf("Customer Row: %d Name: %s", $this->count, $row[3] . " " . $row[4]));
                $this->customers->attach(new CustomerLine(
                    $row[0], // website
                    $row[1], // email,
                    $row[2], // group_id
                    $row[3], // firstname
                    $row[4], // lastname
                    $row[5], // password_hash
                    $row[6], // billing_prefix
                    $row[7], // billing_firstname
                    $row[8], // billing_middlename
                    $row[9], // billing_lastname
                    $row[10], // billing_suffix
                    $row[11], // billing_street_full
                    $row[12], // billing_city
                    $row[13], // billing_region
                    $row[14], // billing_country
                    $row[15], // billing_postcode
                    $row[16], // billing_telephone
                    $row[17], // billing_company
                    $row[18], // billing_fax
                    $row[19], // shipping_prefix
                    $row[20], // shipping_firstname
                    $row[21], // shipping_middlename
                    $row[22], // shipping_lastname
                    $row[23], // shipping_suffix
                    $row[24], // shipping_street_full
                    $row[25], // shipping_city
                    $row[26], // shipping_region
                    $row[27], // shipping_country
                    $row[28], // shipping_postcode
                    $row[29], // shipping_telephone
                    $row[30], // shipping_company
                    $row[31], // shipping_fax
                    $row[32], // created_in
                    $row[33] // is_subscribed
                    //$row[34], // prefix
                    //$row[35], // middlename
                    //$row[36], // suffix
                    //$row[37]  // taxvat
                ));
            }
            $this->count++;
        });
        $lexer->parse($this->csvFileName, $interpreter);
        // return record count
        $this->log->addInfo(sprintf("Finished retrieval of %d customer records", $this->customers->count()));
        return $this->customers->count();
    }

    private function pushCustomersToDatabase()
    {
        $this->log->addInfo("Processing customer records");
        $this->count = 0;
        foreach ($this->customers as $customer)
        {
            $this->log->addInfo(sprintf("Customer; Row: %d, Customer: %s", $this->count, $customer));
            // customer
            $customerdetail = new CustomerDetail(
                1,
                $customer->email,
                $customer->password_hash,
                // title
                '',
                // first name
                $customer->firstname,
                // surname
                $customer->lastname,
                // company
                $customer->billing_company,
                // telephone
                $customer->billing_telephone,
                // mobile
                '',
                $customer->email,
                // fax
                $customer->billing_fax,
                // notes
                '');
            $customerid = Customer::addCustomer($customerdetail);
            $this->log->addInfo(sprintf("Added customer; Id: %d", $customerid));
            $this->assignCustomerAddress($customer, $customerid);

            $this->count++;
        }
        $this->log->addInfo("Finished processing customer records");
    }

    /**
     * @param $customer
     * @param $customerid
     */
    private function assignCustomerAddress($customer, $customerid)
    {
        // billing address
        $existingcustomerbillingaddress = CustomerAddress::getCustomerAddress($customer->billing_street_full);
        if ($existingcustomerbillingaddress != null)
            $billingaddressid = $existingcustomerbillingaddress->recordId;
        else
        {
            $billingaddress = new CustomerAddressDetail(
                'Billing Address',
                $customer->billing_company,
                // first name
                $customer->billing_firstname,
                // surname
                $customer->billing_lastname,
                // add1
                $customer->billing_street_full,
                // add2
                '',
                // add3
                '',
                // town
                $customer->billing_city,
                // county
                $customer->billing_region,
                // country
                $customer->billing_country,
                // postcode
                $customer->billing_postcode,
                // notes
                sprintf("Telephone: %s, Fax: %s",
                    !$this->isNullOrEmptyString($customer->billing_telephone) ? $customer->billing_telephone : "None given",
                    !$this->isNullOrEmptyString($customer->billing_fax) ? $customer->billing_fax : "None given"));
            $billingaddressid = CustomerAddress::addCustomerAddress($customerid, $billingaddress);
            $this->log->addInfo(sprintf("Customer customer billing address; %s Id : %d", $customer->billing_street_full, $billingaddressid));
        }
        // shipping address
        $existingcustomershippingaddress = CustomerAddress::getCustomerAddress($customer->shipping_street_full);
        if ($existingcustomershippingaddress != null) {
            $shippingaddressid = $existingcustomershippingaddress->recordId;
        }
        else
        {
            // delivery address
            $shippingaddress = new CustomerAddressDetail(
                'Delivery Address',
                $customer->shipping_company,
                // firstname
                $customer->shipping_firstname,
                // lastname
                $customer->shipping_lastname,
                // add1
                $customer->shipping_street_full,
                // add2
                '',
                // add3
                '',
                // town
                $customer->shipping_city,
                // county
                $customer->shipping_region,
                // country
                $customer->shipping_country,
                // postcode
                $customer->shipping_postcode,
                // notes
                sprintf("Telephone: %s, Fax: %s",
                    !$this->isNullOrEmptyString($customer->shipping_telephone) ? $customer->shipping_telephone : "None given",
                    !$this->isNullOrEmptyString($customer->shipping_fax) ? $customer->shipping_fax : "None given"));
            $shippingaddressid = CustomerAddress::addCustomerAddress($customerid, $shippingaddress);
            $this->log->addInfo(sprintf("Customer customer shipping address: %s Id: %d", $customer->shipping_street_full, $shippingaddressid));
        }
        $affectedrows = CustomerAddress::updateCustomerAddressDefaults($customerid, $billingaddressid, $shippingaddressid);
        $this->log->addInfo(sprintf("Updated customer with default shipping addresses; %s", $affectedrows));
    }
    #endregion

    #region Images
    private function pushProductImages(ProductLine $productLine, $stockId)
    {
        // new filename
        $stockfilename = sprintf("%d.jpg", $stockId);
        // create Stock Id folders
        $stockfolder = sprintf("%s%d\\", $this->exportPath, $stockId);
        $this->checkFolderExistsIfNotCreate($stockfolder);
        $stockfoldermain = sprintf("%smain\\", $stockfolder);
        $this->checkFolderExistsIfNotCreate($stockfoldermain);
        $stockfolderlist = sprintf("%slist\\", $stockfolder);
        $this->checkFolderExistsIfNotCreate($stockfolderlist);
        $stockfolderzoom = sprintf("%szoom\\", $stockfolder);
        $this->checkFolderExistsIfNotCreate($stockfolderzoom);
        $stockfolderspin = sprintf("%sspin\\", $stockfolder);
        $this->checkFolderExistsIfNotCreate($stockfolderspin);
        // standard
        if (!$this->isNullOrEmptyString($productLine->image))
        {
            $this->copyFile(
                sprintf("%s%s", $this->importPath, $productLine->image),
                sprintf("%s%d.jpg", $stockfoldermain, $stockfilename));
            $this->logNewProductImage("list",
                StockImage::createStockImage(new StockImageDetail(
                    (count($productLine->image_label) > 0) ? $productLine->image_label : $productLine->name,
                    $stockfilename,
                    1,
                    $stockId,
                    1)));
        }
        // thumbnail
        if (!$this->isNullOrEmptyString($productLine->thumbnail))
        {
            $this->copyFile(
                sprintf("%s%s", $this->importPath, $productLine->image),
                sprintf("%s%d.jpg", $stockfolderlist, $stockfilename));
            $this->logNewProductImage("main",
                StockImage::createStockImage(new StockImageDetail(
                    (count($productLine->thumbnail_label) > 0) ? $productLine->thumbnail_label : $productLine->name,
                    $stockfilename,
                    7,
                    $stockId,
                    1)));
        }
        // spin
        $this->import360Image($productLine, $stockfolderspin, $stockId);
    }

    /**
     * @param $imageType
     * @param $newimageid
     */
    private function logNewProductImage($imageType, $newimageid)
    {
        $this->log->addInfo(sprintf("Created Stock Image: Type: %s, Record Id: %d", $imageType, $newimageid));
    }
    #endregion

    #region 360 images (aka Spin)
    /**
     * Import Object Names and Url keys for later use
     */
    public function getAllObjectNames()
    {
        $threeSixtyPath = "C:\\Development\\Edwards\\Files\\edwardsfiles_split~~\\httpdocs\\360";
        $this->threesixtyobjects = new SplObjectStorage();
        $this->count = 0;
        // list folders in path
        $directories = glob($threeSixtyPath . '/*' , GLOB_ONLYDIR);
        $this->log->addInfo(sprintf("Found %d folders to process", count($directories)));
        foreach ($directories as $directory)
        {
            $this->log->addInfo(sprintf("Processing folder: %s", $directory));
            // load profile details
            $xmlfilename = "Profile.xml";
            $filepath = sprintf("%s\\%s", $directory, $xmlfilename);
            if (!file_exists($filepath))
            {
                $this->log->addInfo(sprintf("No %s exists", $xmlfilename));
                continue;
            }
            $xml = simplexml_load_file($filepath)
                or die("Error: Cannot read xml file");
            // get object name
            $attr = $xml->Object[0]->attributes();
            $objectname = $attr['Name'];
            // if the name is not default
            if ($objectname == "default" || $this->isNullOrEmptyString($objectname))
            {
                $this->log->addInfo(
                    sprintf("Skipping product folder, object name is: %s",
                        $this->isNullOrEmptyString($objectname) ? "Not given" : $objectname));
                continue;
            }
            // format object name
            $urlkey = strtolower($objectname);
            $urlkey = str_replace(" ", "-", $urlkey);
            $this->threesixtyobjects->attach(new ThreeSixtyPath($directory, $objectname, $urlkey));
            $this->count++;
        }
        $this->log->addInfo(sprintf("Finished initial import of 360 images; %d imported", $this->count));
    }

    /**
     * Match 360 image against product li
     * @param ProductLine $productLine
     * @param $path string path to export
     * @param stockId
     */
    public function import360Image(ProductLine $productLine, $path, $stockId)
    {
        // find within imported 360 objects
        $threesixtyobject = $this->find360ObjectByUrlKey($productLine->url_key);
        // copy images
        if ($threesixtyobject != null)
        {
            $imgcount = 1;
            if ($threesixtyobject->imagefiles != null)
            {
                foreach ($threesixtyobject->imagefiles as $imagestr) {
                    // get filename portion of image path
                    $filename = basename($imagestr);
                    // copy file
                    $this->copyFile(
                        sprintf("%s", $imagestr),
                        sprintf("%s%s", $path, $filename));
                    // create record in database
                    $this->logNewProductImage("spin",
                        StockImage::createStockImage(new StockImageDetail(
                            sprintf("Image %d", $imgcount),
                            $filename,
                            8,
                            $stockId,
                            $imgcount)));
                    $imgcount++;
                }
            }
        }
    }

    /**
     * @param $urlKey
     * @return mixed|null|object
     */
    private function find360ObjectByUrlKey($urlKey)
    {
        foreach ($this->threesixtyobjects as $threesixtyobject)
        {
            //$this->log->addInfo($threesixtyobject->urlKey);
            if ($threesixtyobject->urlKey == $urlKey)
            {
                return $threesixtyobject;
            }
        }
        return null;
    }

    /**
     * @param $urlKey
     * @return SplObjectStorage
     */
    private function findProductByUrlKey($urlKey)
    {
        $products = new SplObjectStorage();
        foreach ($this->products as $product)
        {
            if ($product->url_key == $urlKey)
                $products->attach($product);
        }
        return $products;
    }

    /**
     * @param $imagestr
     * @return SplObjectStorage
     */
    private function findProductByImageFileName($imagestr)
    {
        $products = new SplObjectStorage();
        foreach ($this->products as $product)
        {
            if (strpos($product->image, $imagestr))
                $products->attach($product);
        }
        return $products;
    }

    /*
     * Find (alienated) images without any obvious associations
     * @param $exportFileName
     */
    public function findAlienatedProducts($exportFileName)
    {
        $this->log->addInfo("Initiated find all alienated products");
        // get all the files
        $this->log->addInfo(sprintf("Getting all files from folder: %s", $this->importPath));
        $files = $this->getDirContents($this->importPath);
        // setup exporter
        $config = new ExporterConfig();
        $config
            ->setDelimiter("\t") // Customize delimiter. Default value is comma(,)
            ->setEnclosure("'")  // Customize enclosure. Default value is double quotation(")
            ->setEscape("\\")    // Customize escape character. Default value is backslash(\)
            ->setToCharset('SJIS-win') // Customize file encoding. Default value is null, no converting.
            ->setFromCharset('UTF-8') // Customize source encoding. Default value is null.
            ->setFileMode(CsvFileObject::FILE_MODE_APPEND); // Customize file mode and choose either write or append. Default value is write ('w'). See fopen() php docs
        $exporter = new Exporter($config);
        $array = array("Row", "Path", "FileName");
        // check each file exists within CSV export
        $rowcount = 1;
        foreach ($files as $file)
        {
            // prepare filename
            $justfilename = basename($file);
            $this->log->addInfo(sprintf("Processing file %s", $justfilename));

            $products = $this->findProductByImageFileName($justfilename);
            if (count($products) == 0)
            {
                $array = array_merge($array, array($rowcount, $file, $justfilename));
            }
            $rowcount += 1;
        }
        $exporter->export($exportFileName, $array);
    }
    #endregion

    #region System
    private function copyFile($srcfile, $dstfile)
    {
        $result = copy($srcfile, $dstfile);
        $messageverb = ($result) ? "Successfully" : "Unsuccessfully";
        $this->log->addInfo(sprintf("%s copied file from: %s to: %s", $messageverb, $srcfile, $dstfile));
    }

    private function isNullOrEmptyString($question)
    {
        return (!isset($question) || trim($question)==='');
    }

    private function checkFolderExistsIfNotCreate($path)
    {
        if (!file_exists($path))
        {
            $result = mkdir($path, 0777, true);
            $messageverb = ($result) ? "Successfully" : "Unsuccessfully";
            $this->log->addInfo(sprintf("%s created folder: %s", $messageverb, $path));
            return $result;
        }
        return true;
    }

    function getDirContents($dir, &$results = array()){
        $files = scandir($dir);

        foreach($files as $key => $value){
            $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
            if(!is_dir($path)) {
                $results[] = $path;
            } else if(is_dir($path) && $value != "." && $value != "..") {
                $this->getDirContents($path, $results);
                $results[] = $path;
            }
        }

        return $results;
    }
    #endregion
}