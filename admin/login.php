<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' * Project - Siteadmin Framework				                       * '
//	' *                                                                    * '
//	' * login.php                                                          * '
//	' ********************************************************************** '
//	' * Parameters required                                                * '
//	' * ===================                                                * '
//	' * optional redirect                                                  * '
//	' *--------------------------------------------------------------------* '
//	' * Called from which pages                                            * '
//	' * =======================                                            * '
//	' * any when not logged in                                             * '
//	' ********************************************************************** '
//	' * Description                                                        * '
//	' * ===========                                                        * '
//	' ********************************************************************** '

use Login\LoginMethods;
use Login\SessionObject;

require_once('includes/inc_loginmethods.php');

$strpage = "login"; //define the current page

include("includes/inc_sitecommon.php"); // Standard include used throughout site

	// ************* Common page setup ******************** //
	//=====================================================//

session_start(); //stores session variables such as access levels and logon details

$sessionobject = new SessionObject();
//get submitted data before we load inc_sitecommon
if (isset($_REQUEST['cmd'])) {
    $strcmd = $_REQUEST['cmd'];
    $sessionobject->stripaddress = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : "Unknown";
    $sessionobject->strusername = (!empty($_REQUEST['username'])) ? $_REQUEST['username'] : "";
    $sessionobject->stremail = (!empty($_REQUEST['email'])) ? $_REQUEST['email'] : "";
    $sessionobject->strpassword = (!empty($_REQUEST['password'])) ? md5($_REQUEST['password']) : md5("");
    $sessionobject->memorable1 = (!empty($_REQUEST['character1'])) ? $_REQUEST['character1'] : "";
    $sessionobject->memorable2 = (!empty($_REQUEST['character2'])) ? $_REQUEST['character2'] : "";
    $sessionobject->memorable3 = (!empty($_REQUEST['character3'])) ? $_REQUEST['character3'] : "";
    $sessionobject->memorablePos1 = (!empty($_REQUEST['position1'])) ? $_REQUEST['position1'] : "";
    $sessionobject->memorablePos2 = (!empty($_REQUEST['position2'])) ? $_REQUEST['position2'] : "";
    $sessionobject->memorablePos3 = (!empty($_REQUEST['position3'])) ? $_REQUEST['position3'] : "";
} else {
    $strcmd = "";
}
$conn = connect();

#region Custom Page Processing
$loginmethods = new LoginMethods($sessionobject, $conn,
    getCompanyDetails($companyAddress, $companyPhone, $companyFax, $companyEmail, $companyName));
switch($strcmd)
{
    case "forgotUsername":
        $loginmethods->forgotUsername();
    break;

    case "forgotPassword":
        $loginmethods->forgotPassword();
    break;

    case "forgotMemorableInfo":
        $loginmethods->forgotMemorableInfo();
    break;

    case "login":
        $loginmethods->login($datnow, $strcookiedom);
    break;
}
#endregion

// ************* Common page setup ******************** //
//=====================================================//

// ************* Custom Page Code ******************** //
//=====================================================//
print ("<!DOCTYPE html>");
print ("<html lang='en'>");
print ("<head>");
    print ("<meta charset='UTF-8'>");
    print ("<title>Login</title>");
	print("<meta name='language' id='language' content='en-gb' />");
    print("<meta name='author' content='Miller Waite' />");
    print("<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0' />");
	print("<meta name='theme-color' content='#262B33'>");
	print("<meta name='msapplication-navbutton-color' content='#262B33'>");
	print("<meta name='apple-mobile-web-app-status-bar-style' content='#262B33'>");
	
    print ("<link rel='stylesheet' type='text/css' href='css/login.css'>");
    print ("<link rel='stylesheet' type='text/css' href='/css/bootstrap.css'>");
    print ("<link rel='stylesheet' type='text/css' href='/css/font-awesome.css'>");
    print ("<script src='js/jquery-1.8.3.js' type='text/javascript' ></script>");
    print ("<script src='js/jquery.validate.js' type='text/javascript' ></script>");
    print ("<script type='text/javascript'>
        function jsforgotPass() {
            document.frmlogin.view.value='forgotPass';
            document.frmlogin.cmd.value='';
            document.frmlogin.submit();
        }
        function jsforgotUser() {
            document.frmlogin.view.value='forgotUser';
            document.frmlogin.cmd.value='';
            document.frmlogin.submit();
        }
        function jsforgotMemorable() {
            document.frmlogin.view.value='forgotMemorableInfo';
            document.frmlogin.cmd.value='';
            document.frmlogin.submit();
        }
    </script>");
print ("</head>");

print ("<body class='small login'>");

    print ("<div id='container'>");
        print ("<div id='content'>");
            print ("<div class='wrapper'>");

                print ("<h1 style='margin-bottom:40px;'><a href=''>Miller Waite</a></h1>");

                if ($loginmethods->errorMessage != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>".$loginmethods->errorMessage."</p></div>"); }
                if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>".$loginmethods->warningMessage."</p></div>"); }
                if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>".$loginmethods->successMessage."</p></div>"); }

                print ("<form action='login.php' method='post' id='frmlogin' name='frmlogin'>");
                print ("<input type='hidden' value='' name='view'>");

                if (isset($_REQUEST['view'])) $strview = $_REQUEST['view']; else $strview = "";
                switch ($strview) {

                    case "forgotUser":

                        print ("<input type='hidden' value='forgotUsername' name='cmd'>");
						
						print("<div class='row'>");
							print("<div class='form-group'>");
								print("<label for='email' class='control-label'>Email Address</label>");
								print("<input id='email' tabindex='2' class='form-control' name='email' type='email'/>");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='form-group'>");
								print("<label for='password' class='control-label'>Password</label>");
								print("<input id='password' tabindex='3' class='form-control' name='password' type='password'/>");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='form-group' style='margin-bottom:0;text-align:right;'>");
								print("<button class='public-button' tabindex='4' type='submit'>Reset</button>");
							print("</div>");
						print("</div>");
						
					print("</div>");
				print("</div>");
				
				print ("<p class='forgot'><a onclick='jsforgotPass(); return false;' href=''>Forgot password?</a> | <a onclick='jsforgotMemorable(); return false;' href=''>Forgot memorable information?</a></p>");

                    break;
                    case "forgotPass":
					
                        $positionArray = getRandChars(3, $string = 'xxxxxxxx'); //get 3 random characters from a string of 8
                        array_multisort($positionArray); //sort array into numerical order

                        print ("<input type='hidden' value='forgotPassword' name='cmd'>");
						
						print("<div class='row'>");
							print("<div class='form-group'>");
								print("<label for='username' class='control-label'>Username</label>");
								print("<input id='username' tabindex='2' class='form-control' name='username' type='text'/>");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='form-group'>");
							
								$options = '';
                                
								foreach (range('A', 'Z') as $char) {
                                   
								   $options .= "<option value='".$char."'>".$char."</option>";
								   
                                }
								
								print("<div class='row'>");
									print("<div class='col-md-4'>");
										print("<input type='hidden' value='".$positionArray[0]."' name='position1'><label for='character1'>Character ".$positionArray[0]."</label>");
										print("<select tabindex='3' class='inputtext' name='character1' id='character1'><option value=''>Select</option>$options</select>");
									print("</div>");
									
									print("<div class='col-md-4'>");
										 print("<input type='hidden' value='".$positionArray[1]."' name='position2'><label for='character2'>Character ".$positionArray[1]."</label>");
										print("<select tabindex='4' class='inputtext' name='character2' id='character2'><option value=''>Select</option>$options</select>");
									print("</div>");

									print("<div class='col-md-4'>");
										 print("<input type='hidden' value='".$positionArray[2]."' name='position3'><label for='character3'>Character ".$positionArray[2]."</label>");
										print("<select tabindex='5' class='inputtext' name='character3' id='character3'><option value=''>Select</option>$options</select>");
									print("</div>");
								print("</div>");

							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='form-group' style='margin-bottom:0;text-align:right;'>");
								print("<button class='public-button' tabindex='6' type='submit'>Reset</button>");
							print("</div>");
						print("</div>");
						
					print("</div>");
				print("</div>");
				
				print ("<p class='forgot'><a onclick='jsforgotUser(); return false;' href=''>Forgot username?</a> | <a onclick='jsforgotMemorable(); return false;' href=''>Forgot memorable information?</a></p>");

                    break;
                    case "forgotMemorableInfo":

                        print ("<input type='hidden' value='forgotMemorableInfo' name='cmd'>");
						
						print("<div class='row'>");
							print("<div class='form-group'>");
								print("<label for='username' class='control-label'>Username</label>");
								print("<input id='username' tabindex='2' class='form-control' name='username' type='text'/>");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='form-group'>");
								print("<label for='password' class='control-label'>Password</label>");
								print("<input id='password' tabindex='3' class='form-control' name='password' type='password'/>");
							print("</div>");
						print("</div>");
					
						print("<div class='row'>");
							print("<div class='form-group' style='margin-bottom:0;text-align:right;'>");
								print("<button class='public-button' tabindex='4' type='submit'>Reset</button>");
							print("</div>");
						print("</div>");
						
					print("</div>");
				print("</div>");
				
				print ("<p class='forgot'><a onclick='jsforgotPass(); return false;' href=''>Forgot password?</a> | <a onclick='jsforgotUser(); return false;' href=''>Forgot username?</a></p>");

                    break;
                    default:

                        $positionArray = getRandChars(3, $string = 'xxxxxxxx'); //get 3 random characters
                        array_multisort($positionArray); //sort array into numerical order

                        print ("<input type='hidden' value='login' name='cmd'>");
						
						print("<div class='row'>");
							print("<div class='form-group'>");
								print("<label for='username' class='control-label'>Username</label>");
								print("<input id='username' tabindex='2' class='form-control' name='username' type='text'/>");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='form-group'>");
								print("<label for='password' class='control-label'>Password</label>");
								print("<input id='password' tabindex='3' class='form-control' name='password' type='password'/>");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='form-group'>");
								print("<p>Please enter characters ".$positionArray[0].", ".$positionArray[1]." and ".$positionArray[2]." from your memorable information:</p>");
							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='form-group'>");
							
								$options = '';
                                
								foreach (range('A', 'Z') as $char) {
                                   
								   $options .= "<option value='".$char."'>".$char."</option>";
								   
                                }
								
								print("<div class='row'>");
									print("<div class='col-md-4'>");
										print("<input type='hidden' value='".$positionArray[0]."' name='position1'><label for='character1'>Character ".$positionArray[0]."</label>");
										print("<select tabindex='4' class='inputtext' name='character1' id='character1'><option value=''>Select</option>$options</select>");
									print("</div>");
									
									print("<div class='col-md-4'>");
										 print("<input type='hidden' value='".$positionArray[1]."' name='position2'><label for='character2'>Character ".$positionArray[1]."</label>");
										print("<select tabindex='5' class='inputtext' name='character2' id='character2'><option value=''>Select</option>$options</select>");
									print("</div>");

									print("<div class='col-md-4'>");
										 print("<input type='hidden' value='".$positionArray[2]."' name='position3'><label for='character3'>Character ".$positionArray[2]."</label>");
										print("<select tabindex='6' class='inputtext' name='character3' id='character3'><option value=''>Select</option>$options</select>");
									print("</div>");
								print("</div>");

							print("</div>");
						print("</div>");
						
						print("<div class='row'>");
							print("<div class='form-group' style='margin-bottom:0;text-align:right;'>");
								print("<button class='public-button' tabindex='7' type='submit' >Login</button>");
							print("</div>");
						print("</div>");
					
					print("</div>");
				print("</div>");
			
				print ("<p class='forgot'><a onclick='jsforgotPass(); return false;' href=''>Forgot password?</a> | <a onclick='jsforgotUser(); return false;' href=''>Forgot username?</a> | <a onclick='jsforgotMemorable(); return false;' href=''>Forgot memorable information?</a></p>");

                    break;
                }
                print ("</form>");
            print ("</div>");
        print ("</div>");
    print ("</div>");

/*  print ("<div id='footer'>");
        print ("<div class='wrapper'>");
            print ("<p id='address'>$companyAddress<br/><span>Phone:</span> $companyPhone <span>Fax:</span> $companyFax <br/><span>Email:</span><a title='' href='mailto:$companyEmail'>$companyEmail</a></p>");
            print ("<p id='copyright'>Copyright &copy; ".date("Y")." <a href=''>$companyName</a>. All rights reserved.</p>");
        print ("</div>");
    print ("</div>");*/
?>
<script type='text/javascript'>
    $().ready(function() {
        // validate signup form on keyup and submit
        $("#frmlogin").validate({
            errorPlacement: function(error,element) {
                if (element.attr("name") == "character1" || element.attr("name") == "character2" || element.attr("name") == "character3") {
                    return true;
                }  else {
                    error.insertAfter(element);
                }
            },
            rules: {
                username: {
                    required: true,
                    minlength: 2
                },
                password: {
                    required: false
                    //minlength: 5
                },
                character1: {
                    required: true
                },
                character2: {
                    required: true
                },
                character3: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                username: {
                    required: "Please enter a username",
                    minlength: "Your username must be at least 2 characters"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                email: "Please enter a valid email address"
                //agree: "Please accept our policy"
            }
        });
    });
</script>
<?php
print ("</body></html>");

