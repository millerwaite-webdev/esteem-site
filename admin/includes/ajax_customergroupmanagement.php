<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_customergroupmanagement"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_POST);
	foreach($_POST AS $customersData)
	{
		//var_dump($customersData);
		$i = 0;
		$intBrands = count($customersData);
		foreach($customersData AS $customerData)
		{
			$strcmd = $customerData['cmd'];
			//var_dump($_POST);
			switch ($strcmd)
			{
				case "add":
				
					//echo "add to level\n";
					$groupCustomerQuery = "UPDATE customer SET groupID = :groupID WHERE recordID = :customerID";
					$strType = "update";
					$arrdbparams = array(
									"customerID" => $customerData['customerID'],
									"groupID" => $customerData['groupID']
								);
								
					//echo "addition\n";
					$groupCustomer = query($conn, $groupCustomerQuery, $strType, $arrdbparams);
					
					break;
					
				case "delete":
					
					//var_dump($customerData);
					$ungroupCustomerQuery = "UPDATE customer SET groupID = NULL WHERE recordID = :customerID AND groupID = :groupID";
					$strType = "update";
					$arrdbparams = array(
									"customerID" => $customerData['customerID'],
									"groupID" => $customerData['groupID']
								);
					//echo "deletion\n";
					$ungroupCustomer = query($conn, $ungroupCustomerQuery, $strType, $arrdbparams);
					
					//echo $ungroupCustomerQuery;
					
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
