<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_categoryproductmanagement"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_POST);
	foreach($_POST AS $categoriesData)
	{
		//var_dump($categoriesData);
		$i = 0;
		$intCategories = count($categoriesData);
		foreach($categoriesData AS $categoryData)
		{
			$strcmd = $categoryData['cmd'];
			//var_dump($_POST);
			switch ($strcmd)
			{
				case "add":
				case "reorder":
					
					//var_dump($_POST);
					$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM stock_category_relations WHERE categoryID = :categoryID";
					$strType = "single";
					$arrdbparam = array( "categoryID" => $categoryData['categoryID'] );
					$maxOrder = query($conn, $maxOrderQuery, $strType, $arrdbparam);
					
					if ($strcmd == "reorder")
					{
						$currOrderQuery = "SELECT stockOrder FROM stock_category_relations WHERE categoryID = :categoryID AND stockID = :stockID";
						$strType = "single";
						$arrdbparam = array( "categoryID" => $categoryData['categoryID'], "stockID" => $categoryData['stockID'] );
						$currOrder = query($conn, $currOrderQuery, $strType, $arrdbparam);
						
						if ($currOrder['stockOrder'] != $categoryData['order'])
						{
							if (($currOrder['stockOrder'] < $categoryData['order']))
							{
								//echo "move down\n";
								$updateOrdersQuery = "UPDATE stock_category_relations SET stockOrder = stockOrder - 1 WHERE categoryID = :categoryID AND stockOrder <= :newOrder AND stockOrder >= :prevOrder";
								$strType = "update";
								$arrdbparam = array(
												"categoryID" => $categoryData['categoryID'],
												"prevOrder" => $currOrder['stockOrder'],
												"newOrder" => $categoryData['order']
											);
								
								if ($categoryData['order'] <= $maxOrder['max_order'])
								{
									//echo "move down before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							elseif ($currOrder['stockOrder'] > $categoryData['order'])
							{
								
								//echo "move up\n";
								$updateOrdersQuery = "UPDATE stock_category_relations SET stockOrder = stockOrder + 1 WHERE categoryID = :categoryID AND stockOrder <= :prevOrder AND stockOrder >= :newOrder";
								$strType = "update";
								$arrdbparam = array(
												"categoryID" => $categoryData['categoryID'],
												"prevOrder" => $currOrder['stockOrder'],
												"newOrder" => $categoryData['order']
											);
								if ($categoryData['order'] > 0)
								{
									//echo "move up before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							
							if ($categoryData['order'] > 0 && $categoryData['order'] <= $maxOrder['max_order'])
							{
								//echo "move level\n";
								$updateOrderQuery = "UPDATE stock_category_relations SET stockOrder = :order WHERE categoryID = :categoryID AND stockID = :stockID";
								$strType = "update";
								$arrdbparams = array(
												"order" => $categoryData['order'],
												"categoryID" => $categoryData['categoryID'],
												"stockID" => $categoryData['stockID']
											);
								
								//echo "reorder\n";
								$updateOrder = query($conn, $updateOrderQuery, $strType, $arrdbparams);
							}
						}
					}
					elseif ($strcmd == "add")
					{
						$currOrder['stockOrder'] = $categoryData['order'];
						
						if ($categoryData['order'] > 0)
						{
							if ($categoryData['order'] <= $maxOrder['max_order'])
							{
								//echo "add\n";
								$updateOrdersQuery = "UPDATE stock_category_relations SET stockOrder = stockOrder + 1 WHERE categoryID = :categoryID AND stockOrder >= :newOrder";
								$strType = "update";
								$arrdbparam = array(
												"categoryID" => $categoryData['categoryID'],
												"newOrder" => $categoryData['order']
											);
								
								//echo "move down before addition\n";
								$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
							}
							//echo "add to level\n";
							$insertOrderQuery = "INSERT INTO stock_category_relations (categoryID, stockID, stockOrder) VALUES (:categoryID, :stockID, :order)";
							$strType = "insert";
							$arrdbparams = array(
											"categoryID" => $categoryData['categoryID'],
											"stockID" => $categoryData['stockID'],
											"order" => $categoryData['order']
										);
										
							//echo "addition\n";
							$insertOrder = query($conn, $insertOrderQuery, $strType, $arrdbparams);
						}
					}
					//echo $currOrder['stockOrder']." ".$categoryData['order']."\n";
					//echo $maxOrder['max_order']." ".$categoryData['order']."\n";
					//echo "Order is: ".$categoryData['order']."\n";
					
					break;
					
				case "delete":
					
					//var_dump($categoryData);
					$deleteRelationQuery = "DELETE FROM stock_category_relations WHERE categoryID = :categoryID AND stockID = :stockID AND stockOrder = :order";
					$strType = "delete";
					$arrdbparams = array(
									"categoryID" => $categoryData['categoryID'],
									"stockID" => $categoryData['stockID'],
									"order" => $categoryData['order']
								);
					//echo "deletion\n";
					$deleteOrder = query($conn, $deleteRelationQuery, $strType, $arrdbparams);
					
					//echo $deleteOrderQuery;
					//var_dump($deleteOrder);
					
					$i++;
					
					if ($i == $intCategories)
					{
						
						$strdbsql = "SELECT recordID FROM stock_category_relations WHERE categoryID = :categoryID ORDER BY stockOrder";
						$strType = "multi";
						$arrdbparams = array(
										"categoryID" => $categoryData["categoryID"]
									);
						echo "get products\n";
						$associatedProducts = query($conn, $strdbsql, $strType, $arrdbparams);
						//var_dump($categories);
						
						$y = 1;
						foreach($associatedProducts AS $associatedProduct)
						{
							$strdbsql = "UPDATE stock_category_relations SET stockOrder = ".$y." WHERE recordID = :recordID";
							$strType = "update";
							$arrdbparam = array(
											"recordID" => $associatedProduct['recordID']
										);
							echo "update final ordering\n";
							query($conn, $strdbsql, $strType, $arrdbparam);
							$y++;
						}
					}
					
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
