<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_galleryimagemanagement"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_POST);
	foreach($_POST AS $imagesData)
	{
		//var_dump($imagesData);
		foreach($imagesData AS $imageData)
		{
			$strcmd = $imageData['cmd'];
			//var_dump($_POST);
			switch ($strcmd)
			{
				case "reorder":
					
					//var_dump($_POST);
					$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM site_gallery_images WHERE galleryID = :galleryID";
					$strType = "single";
					$arrdbparam = array( "galleryID" => $imageData['galleryID'] );
					$maxOrder = query($conn, $maxOrderQuery, $strType, $arrdbparam);
					
					$currOrderQuery = "SELECT galleryOrder FROM site_gallery_images WHERE recordID = :recordID";
					$strType = "single";
					$arrdbparam = array( "recordID" => $imageData['recordID'] );
					$currOrder = query($conn, $currOrderQuery, $strType, $arrdbparam);
					
					if ($currOrder['galleryOrder'] != $imageData['galleryOrder'])
					{
						if (($currOrder['galleryOrder'] < $imageData['galleryOrder']))
						{
							//echo "move down\n";
							$updateOrdersQuery = "UPDATE site_gallery_images SET galleryOrder = galleryOrder - 1 WHERE galleryID = :galleryID AND galleryOrder <= :newOrder AND galleryOrder >= :prevOrder";
							$strType = "update";
							$arrdbparam = array(
											"galleryID" => $imageData['galleryID'],
											"prevOrder" => $currOrder['galleryOrder'],
											"newOrder" => $imageData['galleryOrder']
										);
							
							if ($imageData['galleryOrder'] <= $maxOrder['max_order'])
							{
								//echo "move down before reorder\n";
								$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
							}
						}
						elseif ($currOrder['galleryOrder'] > $imageData['galleryOrder'])
						{
							
							//echo "move up\n";
							$updateOrdersQuery = "UPDATE site_gallery_images SET galleryOrder = galleryOrder + 1 WHERE galleryID = :galleryID AND galleryOrder <= :prevOrder AND galleryOrder >= :newOrder";
							$strType = "update";
							$arrdbparam = array(
											"galleryID" => $imageData['galleryID'],
											"prevOrder" => $currOrder['galleryOrder'],
											"newOrder" => $imageData['galleryOrder']
										);
							if ($imageData['galleryOrder'] > 0)
							{
								//echo "move up before reorder\n";
								$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
							}
						}
						
						if ($imageData['galleryOrder'] > 0 && $imageData['galleryOrder'] <= $maxOrder['max_order'])
						{
							//echo "move level\n";
							$updateOrderQuery = "UPDATE site_gallery_images SET galleryOrder = :galleryOrder WHERE recordID = :recordID";
							$strType = "update";
							$arrdbparams = array(
											"galleryOrder" => $imageData['galleryOrder'],
											"recordID" => $imageData['recordID']
										);
							
							//echo "reorder\n";
							$updateOrder = query($conn, $updateOrderQuery, $strType, $arrdbparams);
						}
					}
					//echo $currOrder['menuOrder']." ".$imageData['galleryOrder']."\n";
					//echo $maxOrder['max_order']." ".$imageData['galleryOrder']."\n";
					//echo "Order is: ".$imageData['galleryOrder']."\n";
					
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
