<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_stockdtattablepagination"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	
	// DB table to use
	$table = 'stock_group_information';
	 
	// Table's primary key
	$primaryKey = 'recordID';
	 
	// Array of database columns which should be read and sent back to DataTables.
	// The `db` parameter represents the column name in the database, while the `dt`
	// parameter represents the DataTables column identifier. In this case simple
	// indexes
	$columns = array(
		array( 'db' => 'name', 'dt' => 0 ),
		array( 'db' => 'recordID', 'dt' => 1 )/*,
		array( 'db' => 'stockCode',  'dt' => 1 )*/
	);
	 
	// SQL server connection information
	$sql_details = array(
		'user' => USER,
		'pass' => PASS,
		'db'   => DB,
		'host' => HOST
	);
	 
	 
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP
	 * server-side, there is no need to edit below this line.
	 */
	 
	require( 'ssp.class.php' );
	 
	/*echo json_encode(
		SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
	);*/
	
	$ajaxDataItems = SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns );
	$intAjaxDataItems = count($ajaxDataItems);
	
	$i = 1;
	print ("{");
		foreach($ajaxDataItems AS $ajaxDataIndex => $ajaxDataItem)
		{
			print ("\"".$ajaxDataIndex."\": ");
			if(!is_array($ajaxDataItem))
			{
				print ("\"".$ajaxDataItem."\"");
			}
			else
			{
				print ("[");
						$intAjaxDataItem = count($ajaxDataItem);
						$y = 1;
						foreach($ajaxDataItem AS $ajaxNestedDataItem)
						{
					print ("[");
							print ("\"".str_replace(array("\"", "/", "\n"), array("\\\"", "\/", ""), $ajaxNestedDataItem[0])."\",");
							print ("\"<button onclick='jsviewStockGroup(\\\"".$ajaxNestedDataItem['1']."\\\"); return false;' class='center btn btn-primary' type='submit'>Modify</button>\"");
					print ("]");
							if($y < $intAjaxDataItem)
							{
								print (",");
							}
							$y++;
						}
				print ("]");
			}
			if($i < $intAjaxDataItems)
			{
				print (",");
			}
			$i++;
		}
	print ("}");
	// ************* Common page setup ******************** //
	//=====================================================//
	$conn = null; // close the Database connection after all processing
?>
