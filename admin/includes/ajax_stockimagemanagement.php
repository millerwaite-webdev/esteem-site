<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_stockimagemanagement"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_POST);
	foreach($_POST AS $imagesData)
	{
		//var_dump($imagesData);
		$i = 0;
		$intImages = count($imagesData);
		foreach($imagesData AS $imageData)
		{
			$strcmd = $imageData['cmd'];
			//var_dump($_POST);
			switch ($strcmd)
			{
				case "add":
				case "reorder":

					//var_dump($_POST);
					$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM property_images WHERE rs_id = :rs_id";
					$strType = "single";
					$arrdbparam = array("rs_id" => $imageData['galleryID']);
					$maxOrder = query($conn, $maxOrderQuery, $strType, $arrdbparam);
					
					if ($strcmd == "reorder")
					{
						$currOrderQuery = "SELECT image_order FROM property_images WHERE recordID = :recordID";
						$strType = "single";
						$arrdbparam = array( "recordID" => $imageData['recordID']);
						$currOrder = query($conn, $currOrderQuery, $strType, $arrdbparam);
						
						if ($currOrder['image_order'] != $imageData['galleryOrder'])
						{
							if (($currOrder['image_order'] < $imageData['galleryOrder']))
							{
								//echo "move down\n";
								$updateOrdersQuery = "UPDATE property_images SET image_order = image_order - 1 WHERE rs_id = :rs_id AND image_order <= :newOrder AND image_order >= :prevOrder";
								$strType = "update";
								$arrdbparam = array(
									"rs_id" => $imageData['galleryID'],
									"prevOrder" => $currOrder['image_order'],
									"newOrder" => $imageData['galleryOrder']
								);
								
								if ($imageData['galleryOrder'] <= $maxOrder['max_order'])
								{
									//echo "move down before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							elseif ($currOrder['image_order'] > $imageData['galleryOrder'])
							{
								
								//echo "move up\n";
								$updateOrdersQuery = "UPDATE property_images SET image_order = image_order + 1 WHERE rs_id = :rs_id AND image_order <= :prevOrder AND image_order >= :newOrder";
								$strType = "update";
								$arrdbparam = array(
												"rs_id" => $imageData['galleryID'],
												"prevOrder" => $currOrder['image_order'],
												"newOrder" => $imageData['galleryOrder']
											);
								if ($imageData['galleryOrder'] > 0)
								{
									//echo "move up before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							
							if ($imageData['galleryOrder'] > 0 && $imageData['galleryOrder'] <= $maxOrder['max_order'])
							{
								//echo "move level\n";
								$updateOrderQuery = "UPDATE property_images SET image_order = :order WHERE recordID = :recordID";
								$strType = "update";
								$arrdbparams = array(
												"order" => $imageData['galleryOrder'],
												"recordID" => $imageData['recordID']
											);
								
								//echo "reorder\n";
								$updateOrder = query($conn, $updateOrderQuery, $strType, $arrdbparams);
							}
						}
					}
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
