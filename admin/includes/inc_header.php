<?php
	header('Content-type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset='UTF-8'>
	
	<title><?php print $strwebtitle;?></title>

	<meta name="language" id="language" content="en-gb" />
	<meta name="author" content="Miller Waite" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
	<meta name="theme-color" content="#262B33">
	<meta name="msapplication-navbutton-color" content="#262B33">
	<meta name="apple-mobile-web-app-status-bar-style" content="#262B33">
	
	<link rel="stylesheet" type='text/css' href="/admin/css/normalize.css">
	<link rel="stylesheet" type='text/css' href="/admin/css/jquery-ui-smooth.css" /> <!-- jQuery CSS -->
	<link rel="stylesheet" type='text/css' href="/admin/css/bootstrap.css" /> <!-- FORM CSS TEMPLATE -->
	<link rel="stylesheet" type='text/css' href="/admin/css/colourpicker.css" /> <!-- COLOUR PICKER CSS TEMPLATE -->
	<link rel="stylesheet" type='text/css' href="/admin/css/switch.css" /> <!-- COLOUR PICKER CSS TEMPLATE -->
	<link rel="stylesheet" type='text/css' href="/admin/css/style.css"> <!-- MAIN CSS FILE -->
	<link rel="stylesheet" type='text/css' href="/admin/css/jquery.dataTables.css"> <!-- data tables FILE -->
	<link rel="stylesheet" type='text/css' href="/css/font-awesome.css"> <!-- data tables FILE -->
	<link rel="stylesheet" type='text/css' href="http://fonts.googleapis.com/css?family=Roboto:400,100,500,700,300,300italic,500italic|Roboto+Condensed:400,300">
	
    <!--[if lte IE 7]><link rel="stylesheet" type='text/css' media="screen" href="/css/ie.css" /><![endif]-->
	
	<script type="text/javascript" src="/admin/js/jquery-1.8.3.js" ></script>
	<!--<script src="/js/jquery-1.9.1.min.js" type="text/javascript" ></script>
	<script type="text/javascript" src="/js/jquery.dataTables.js" ></script>-->
	<script type="text/javascript" src="/admin/js/jquery-ui.js" ></script>
	<script type='text/javascript' src='/admin/js/jquery.validate.js' ></script>
	<script type="text/javascript" src="/admin/js/script.js" ></script>
	<script type='text/javascript' src='/admin/js/accordion.js'></script>
	<script type='text/javascript' src='/admin/js/jquery.dataTables.min.js'></script>
	<script type='text/javascript' src='/admin/js/colourpicker.js'></script>
	<script type='text/javascript' src='/admin/js/colourpicker.docs.js'></script>
	<script type='text/javascript' src='/admin/js/dropdown.js'></script>
	<script type='text/javascript' src="http://cdn.tinymce.com/4/tinymce.min.js"></script>
	<script>
	tinymce.init({
		selector: '.tinymce',
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		relative_urls : 0,
		remove_script_host : 0
	});
	</script>	
</head>
<body>
	<div id='topMenu'>

		<?php
		
		$getEnquiriesQueryRead = "SELECT count(recordID) AS readEnquiries FROM site_contact_history WHERE `read` = 0";
		$arrParams = array();
		$enquiriesRead = query($conn, $getEnquiriesQueryRead, "single", $arrParams);
		
		print("<div class='col-xs-2' style='text-align:left;padding:0;'>");
			print("<a title='Toggle' class='toggle media-in'><i class='fa fa-bars'></i></a>");
			if($enquiriesRead['readEnquiries'] > 0){
				print("<a href='/admin/cms-contact.php' title='Enquiries' class='media-out'>");
					print("<i class='fa fa-envelope'></i>");
					print("<div class='count'>".$enquiriesRead['readEnquiries']."</div>");
				print("</a>");
			}
			else{
				print("<a href='/admin/cms-contact.php' title='Enquiries' class='media-out'><i class='fa fa-envelope'></i></a>");
			}
		print("</div>");
		print("<div class='col-xs-8' style='padding:0;text-align:center;'>");
			print("<span class='center-block;' style='font-size:14px;font-style:italic;line-height:64px;color:#1D75BC;display:inline-block;width:100%;overflow:hidden;text-overflow:ellipsis;'>".$strsiteurl."</span>");
		print("</div>");
		print("<div class='col-xs-2' style='padding:0;'>");
			print("<ul class='nav navbar-nav pull-right'>");
				print("<li class='dropdown'>");
					print("<a href='#' class='dropdown-toggle user' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'><i class='fa fa-user'></i> ".$_SESSION['username']."</a>");
					print("<ul class='dropdown-menu'>");
						print("<li><a href='/admin/index.php'><i class='fa fa-home'></i> Dashboard</a></li>");
						print("<li class='media-in'><a href='/admin/cms-contact.php'><span class='badge number'>".$enquiriesRead['readEnquiries']."</span> Enquiries </a></li>");
						print("<li><a href='/admin/operator.php'><i class='fa fa-cog'></i> Settings</a></li>");
						print("<li><a href='/admin/?logout'><i class='fa fa-sign-out'></i> Log Out</a></li>");
					print("</ul>");
				print("</li>");
			print("</ul>");
		print("</div>");
		
		
		?>
		
	</div>
	<div id='main'>