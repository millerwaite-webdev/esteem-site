<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_attributetypemanagement"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_POST);
	foreach($_POST AS $attributeTypesData)
	{
		//var_dump($attributeTypesData);
		$i = 0;
		$intAttributeTypes = count($attributeTypesData);
		foreach($attributeTypesData AS $attributeTypeData)
		{
			$strcmd = $attributeTypeData['cmd'];
			//var_dump($_POST);
			switch ($strcmd)
			{
				case "reorder":
					
					//var_dump($_POST);
					$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM stock_attribute_type";
					$strType = "single";
					$maxOrder = query($conn, $maxOrderQuery, $strType, $arrdbparam);
					
					if ($strcmd == "reorder")
					{
						$currOrderQuery = "SELECT attributeTypeOrder FROM stock_attribute_type WHERE recordID = :attributeTypeID";
						$strType = "single";
						$arrdbparam = array( "attributeTypeID" => $attributeTypeData['typeID'] );
						$currOrder = query($conn, $currOrderQuery, $strType, $arrdbparam);
						
						if ($currOrder['attributeTypeOrder'] != $attributeTypeData['order'])
						{
							if (($currOrder['attributeTypeOrder'] < $attributeTypeData['order']))
							{
								//echo "move down\n";
								$updateOrdersQuery = "UPDATE stock_attribute_type SET attributeTypeOrder = attributeTypeOrder - 1 WHERE attributeTypeOrder <= :newOrder AND attributeTypeOrder >= :prevOrder";
								$strType = "update";
								$arrdbparam = array(
												"prevOrder" => $currOrder['attributeTypeOrder'],
												"newOrder" => $attributeTypeData['order']
											);
								
								if ($attributeTypeData['order'] <= $maxOrder['max_order'])
								{
									//echo "move down before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							elseif ($currOrder['attributeTypeOrder'] > $attributeTypeData['order'])
							{
								
								//echo "move up\n";
								$updateOrdersQuery = "UPDATE stock_attribute_type SET attributeTypeOrder = attributeTypeOrder + 1 WHERE attributeTypeOrder <= :prevOrder AND attributeTypeOrder >= :newOrder";
								$strType = "update";
								$arrdbparam = array(
												"prevOrder" => $currOrder['attributeTypeOrder'],
												"newOrder" => $attributeTypeData['order']
											);
								if ($attributeTypeData['order'] > 0)
								{
									//echo "move up before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							
							if ($attributeTypeData['order'] > 0 && $attributeTypeData['order'] <= $maxOrder['max_order'])
							{
								//echo "move level\n";
								$updateOrderQuery = "UPDATE stock_attribute_type SET attributeTypeOrder = :order WHERE recordID = :attributeTypeID";
								$strType = "update";
								$arrdbparams = array(
												"order" => $attributeTypeData['order'],
												"attributeTypeID" => $attributeTypeData['typeID']
											);
								
								//echo "reorder\n";
								$updateOrder = query($conn, $updateOrderQuery, $strType, $arrdbparams);
							}
						}
					}
					//echo $currOrder['menuOrder']." ".$rangeData['order']."\n";
					//echo $maxOrder['max_order']." ".$rangeData['order']."\n";
					//echo "Order is: ".$rangeData['order']."\n";
					
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
