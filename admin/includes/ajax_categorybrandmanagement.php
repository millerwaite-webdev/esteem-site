<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_categorybrandmanagement"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_POST);
	foreach($_POST AS $categoriesData)
	{
		//var_dump($categoriesData);
		$i = 0;
		$intCategories = count($categoriesData);
		foreach($categoriesData AS $categoryData)
		{
			$strcmd = $categoryData['cmd'];
			//var_dump($_POST);
			switch ($strcmd)
			{
				case "add":
				case "reorder":
					
					//var_dump($_POST);
					$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM category_brand_relations WHERE categoryID = :categoryID";
					$strType = "single";
					$arrdbparam = array( "categoryID" => $categoryData['categoryID'] );
					$maxOrder = query($conn, $maxOrderQuery, $strType, $arrdbparam);
					
					if ($strcmd == "reorder")
					{
						$currOrderQuery = "SELECT menuOrder FROM category_brand_relations WHERE categoryID = :categoryID AND brandID = :brandID";
						$strType = "single";
						$arrdbparam = array( "categoryID" => $categoryData['categoryID'], "brandID" => $categoryData['brandID'] );
						$currOrder = query($conn, $currOrderQuery, $strType, $arrdbparam);
						
						if ($currOrder['menuOrder'] != $categoryData['order'])
						{
							if (($currOrder['menuOrder'] < $categoryData['order']))
							{
								//echo "move down\n";
								$updateOrdersQuery = "UPDATE category_brand_relations SET menuOrder = menuOrder - 1 WHERE categoryID = :categoryID AND menuOrder <= :newOrder AND menuOrder >= :prevOrder";
								$strType = "update";
								$arrdbparam = array(
												"categoryID" => $categoryData['categoryID'],
												"prevOrder" => $currOrder['menuOrder'],
												"newOrder" => $categoryData['order']
											);
								
								if ($categoryData['order'] <= $maxOrder['max_order'])
								{
									//echo "move down before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							elseif ($currOrder['menuOrder'] > $categoryData['order'])
							{
								
								//echo "move up\n";
								$updateOrdersQuery = "UPDATE category_brand_relations SET menuOrder = menuOrder + 1 WHERE categoryID = :categoryID AND menuOrder <= :prevOrder AND menuOrder >= :newOrder";
								$strType = "update";
								$arrdbparam = array(
												"categoryID" => $categoryData['categoryID'],
												"prevOrder" => $currOrder['menuOrder'],
												"newOrder" => $categoryData['order']
											);
								if ($categoryData['order'] > 0)
								{
									//echo "move up before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							
							if ($categoryData['order'] > 0 && $categoryData['order'] <= $maxOrder['max_order'])
							{
								//echo "move level\n";
								$updateOrderQuery = "UPDATE category_brand_relations SET menuOrder = :order WHERE categoryID = :categoryID AND brandID = :brandID";
								$strType = "update";
								$arrdbparams = array(
												"order" => $categoryData['order'],
												"categoryID" => $categoryData['categoryID'],
												"brandID" => $categoryData['brandID']
											);
								
								//echo "reorder\n";
								$updateOrder = query($conn, $updateOrderQuery, $strType, $arrdbparams);
							}
						}
					}
					elseif ($strcmd == "add")
					{
						$currOrder['menuOrder'] = $categoryData['order'];
						
						if ($categoryData['order'] > 0)
						{
							if ($categoryData['order'] <= $maxOrder['max_order'])
							{
								//echo "add\n";
								$updateOrdersQuery = "UPDATE category_brand_relations SET menuOrder = menuOrder + 1 WHERE categoryID = :categoryID AND menuOrder >= :newOrder";
								$strType = "update";
								$arrdbparam = array(
												"categoryID" => $categoryData['categoryID'],
												"newOrder" => $categoryData['order']
											);
								
								//echo "move down before addition\n";
								$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
							}
							//echo "add to level\n";
							$insertOrderQuery = "INSERT INTO category_brand_relations (categoryID, brandID, menuOrder) VALUES (:categoryID, :brandID, :order)";
							$strType = "insert";
							$arrdbparams = array(
											"categoryID" => $categoryData['categoryID'],
											"brandID" => $categoryData['brandID'],
											"order" => $categoryData['order']
										);
										
							//echo "addition\n";
							$insertOrder = query($conn, $insertOrderQuery, $strType, $arrdbparams);
						}
					}
					//echo $currOrder['menuOrder']." ".$categoryData['order']."\n";
					//echo $maxOrder['max_order']." ".$categoryData['order']."\n";
					//echo "Order is: ".$categoryData['order']."\n";
					
					break;
					
				case "delete":
					
					//var_dump($categoryData);
					$deleteRelationQuery = "DELETE FROM category_brand_relations WHERE categoryID = :categoryID AND brandID = :brandID AND menuOrder = :order";
					$strType = "delete";
					$arrdbparams = array(
									"categoryID" => $categoryData['categoryID'],
									"brandID" => $categoryData['brandID'],
									"order" => $categoryData['order']
								);
					//echo "deletion\n";
					$deleteOrder = query($conn, $deleteRelationQuery, $strType, $arrdbparams);
					
					//echo $deleteOrderQuery;
					//var_dump($deleteOrder);
					
					$i++;
					
					if ($i == $intCategories)
					{
						
						$strdbsql = "SELECT recordID FROM category_brand_relations WHERE categoryID = :categoryID ORDER BY menuOrder";
						$strType = "multi";
						$arrdbparams = array(
										"categoryID" => $categoryData["categoryID"]
									);
						//echo "get brands\n";
						$associatedBrands = query($conn, $strdbsql, $strType, $arrdbparams);
						//var_dump($categories);
						
						$y = 1;
						foreach($associatedBrands AS $associatedBrand)
						{
							$strdbsql = "UPDATE category_brand_relations SET menuOrder = ".$y." WHERE recordID = :recordID";
							$strType = "update";
							$arrdbparam = array(
											"recordID" => $associatedBrand['recordID']
										);
							//echo "update final ordering\n";
							query($conn, $strdbsql, $strType, $arrdbparam);
							$y++;
						}
					}
					
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
