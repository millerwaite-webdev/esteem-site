<?php
//include("../lib/swift_required.php");

//test if operator has permission to view this page
function pagePermission($currentpage,$conn) {

	$location = "";
	if ($currentpage == "") {
		$currentpage = curPageName(); //this is a function that is near the bottom of this page. It gets the pagename of the current page
	}

	$strdbsql = "SELECT * FROM admin_pages WHERE link=:pagename";
	$arrType = "single";
	$strdbparams = array("pagename" => $currentpage);
	$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
	

	//if the pagename cannot be found in the database then bounce back to the index or login pages
	if ($resultdata != null) {
		if (isset($resultdata['permissionGroup'])) $permGroup = $resultdata['permissionGroup']; else $permGroup = "";
		$pagePermissionGroup = permissionGroup($permGroup,$conn);
		$operatorPermissionGroup =
			array_key_exists('permissionGroup', $_SESSION)
				? permissionGroup($_SESSION['permissionGroup'],$conn)
				: NULL;

		//checks to see if the user has permission to view this page
		if ($pagePermissionGroup['groupID'] > $operatorPermissionGroup['groupID']) {
			if ($currentpage != 'index' AND $currentpage != 'login') {
				$location = ("Location: /admin/index.php?error=You do not have permission to access this page");
			} else {
				$location = ("Location: /admin/login.php");
			}
		}


	} else {
		if ($currentpage != 'index') {
			$location = ("Location: /admin/index.php?error=1");
		} else {
			$location = ("Location: /admin/login.php?error=1");
		}
	}
	return $location;
}


function permissionGroup($permissionGroupID,$conn) {
	$strdbsql = "SELECT * FROM admin_permission_groups WHERE groupID=:groupID";
	$arrType = "single";
	$strdbparams = array("groupID" => $permissionGroupID);
	$resultdata = query($conn, $strdbsql, $arrType, $strdbparams);
	return $resultdata;
}

// Written by Carl on 14 April 2014 - Called from config-letters.php
function stripCharactersFilename($filename){
	$filename = preg_replace("([^\w\s\d\-_~,;:\[\]\(\].]|[\.]{2,})", '', $filename);
	return $filename;
}

//Re-Arranges the array when multiple files are imported
function reArrayFiles( $arr ){foreach( $arr as $key => $all ){foreach( $all as $i => $val ){$new[$i][$key] = $val;}} return $new; }

//function to loop though a XML document and create a CSV
function createCsv($xml,$f){foreach ($xml->children() as $item) { $hasChild = (count($item->children()) > 0)?true:false; if( ! $hasChild){$put_arr = array($item->getName(),$item); fputcsv($f, $put_arr ,',','"');} else {createCsv($item, $f);}}}

/**
 * Encodes HTML safely for UTF-8. Use instead of htmlentities.
 *
 * @param string $var
 * @return string
 */
function html_encode($var)
{
	return htmlentities($var, ENT_QUOTES, 'UTF-8') ;
}

// Written by Carl on 14 April 2014 - Called from config-letters.php
function getMailMerge(&$wts, $index, $dataarray, $mergerecord){
	$loop = true;
	$startfield = false;
	$setval = false;
	$counter = $index;
	$newcount = 0;
	while( $loop )
	{
		//find any nodes that are named w:fldCharType
		if( $wts->item( $counter )->attributes->item(0)->nodeName == 'w:fldCharType' )
		{
			$nodeName = '';
			$nodeValue = '';

			//switch the found nodes value
			switch( $wts->item( $counter )->attributes->item(0)->nodeValue )
			{
				//the first value will be begin - this tells us what type of merge we are looking at
				case 'begin':
					if( $startfield )
					{
						$counter = getMailMerge( $wts, $counter, $dataarray, $mergerecord);
					}
					$startfield = true;
					//look for the next sibling of our parent node
					if( $wts->item( $counter )->parentNode->nextSibling )
					{
						$nodeName = $wts->item( $counter )->parentNode->nextSibling->childNodes->item(1)->nodeName;
						$nodeValue = $wts->item( $counter )->parentNode->nextSibling->childNodes->item(1)->nodeValue;
					} else {
						// No sibling so check next node
						$nodeName = $wts->item( $counter + 1 )->parentNode->previousSibling->childNodes->item(1)->nodeName;
						$nodeValue = $wts->item( $counter + 1 )->parentNode->previousSibling->childNodes->item(1)->nodeValue;
					}

					//once found the nodes will then have a value - we need to switch these and replace the values
					if( $nodeValue == 'date \@ "MMMM d, yyyy"' )
					{
						$setval = true;
						$newval = date( "F j, Y" );
					}
					if( substr( $nodeValue, 0, 11 ) == ' MERGEFIELD' )
					{
						$setval = true;
						$newval = $dataarray[strtolower(str_replace('"', '', trim(substr($nodeValue, 12 ))))];
						$array['data'] = strtolower(str_replace('"', '', trim(substr($nodeValue, 12 ))));
					}
					if( substr( $nodeValue, 0, 9 ) == ' MERGEREC' )
					{
						$setval = true;
						$newval = $mergerecord;
					}
					$counter++;

					//not sure why this is in here but it doesnt have a end after it so we need to ignore.
					if($nodeValue == ' IF ' )
					{
						if( $startfield )
						{
							$startfield = false;
						}
						$loop = false;
					}

				break;
				case 'separate':
					if( $wts->item( $counter )->parentNode->nextSibling )
					{
						$nodeName = $wts->item( $counter )->parentNode->nextSibling->childNodes->item(1)->nodeName;
						$nodeValue = $wts->item( $counter )->parentNode->nextSibling->childNodes->item(1)->nodeValue;
					} else {
						// No sibling
						// check next node
						$nodeName = $wts->item( $counter + 1 )->parentNode->previousSibling->childNodes->item(1)->nodeName;
						$nodeValue = $wts->item( $counter + 1 )->parentNode->previousSibling->childNodes->item(1)->nodeValue;
					}
					if( $setval )
					{
						$wts->item( $counter )->parentNode->nextSibling->childNodes->item(1)->nodeValue = $newval;
						$setval = false;
						$newval = '';
					}
					$counter++;
				break;
				case 'end':
					if( $startfield )
					{
						$startfield = false;
					}
					$loop = false;
			}
		}
	}
	$array['counter'] = $counter;
	return $array;
}


// Written by Carl on 22nd April 2014 - Called from config-letters.php / communication-letters.php
function printLetter($letterID, $lookupData, $datnow, $conn){

	//get document details
	$strdbsql = "SELECT * FROM letters WHERE letterID = :letterID";
	$arrdbvalues = array("letterID" => $letterID);
	$strType = "single";
	$letterDetails = query($conn, $strdbsql, $strType, $arrdbvalues);

	//create directory if it does not currently exist
	$directory = "/files/".$_SESSION['lottery']."/exports/letters/";
	$filename = fnconvertunixtime($datnow,"H:i-d-m-Y")."_".stripCharactersFilename($letterDetails['description'])."_ver_".$letterDetails['version'].".docx";
	if (!file_exists($directory)){$makedir = recursive_mkdir($directory);}

	$oldfile = $_SERVER['DOCUMENT_ROOT'].$letterDetails['fileLocation'];
	$newfile = $_SERVER['DOCUMENT_ROOT'].$directory.$filename;
	$moveResult = copy($oldfile, $newfile);

	if ($moveResult != true) {
		$strerror = ("Could not move file to the exports directory ($newfile)");
	} else {

		$mergerecord = 1; //this will increment if more than once letter being produced
		$requiredData = unserialize($letterDetails['requiredData']);

		foreach ($requiredData['tables'] AS $table) {
			$strdbsql = "SELECT * FROM exportTables WHERE databaseTable = :table";
			$strType = "single";
			$arrdbvalues = array("table"=>$table);
			$exportData = query($conn, $strdbsql, $strType, $arrdbvalues);

			//loop around each record that we need to produce a letter for
			for($i = 0; $i < count($lookupData); $i++) {
				$recordID = $lookupData[$i][$exportData['lookupRecord']];
				$strType = "multi";
				$arrdbvalues = array("recordID"=>$recordID); //test data for now
				$datafields[$i][$table] = query($conn, $exportData['query'], $strType, $arrdbvalues);
			}
		}

		//replace the merge fields with actual data
		for($i = 0; $i < count($lookupData); $i++) {
			foreach ($requiredData['columns'] AS $key => $columns) {
				$split = explode(".", $columns);

				//Start of custom rules
				if ($key == "firstorlastname") {
					$letterdata[$i][$key] = $datafields[$i]['member'][0]['salutation']." ".$datafields[$i]['member'][0]['firstname']." ".$datafields[$i]['member'][0]['surname'];
				} else {
					$letterdata[$i][$key] = $datafields[$i][$split[0]][0][$split[1]];
				}
			}
		}

		$zip = new ZipArchive();
		if( $zip->open($newfile, ZIPARCHIVE::CHECKCONS ) !== TRUE ) { echo "failed to open template"; exit; }
		$file = 'word/document.xml';
		$data = $zip->getFromName($file);

		$doc = new DOMDocument();
		$doc->loadXML( $data );

		//$pagebreak = $doc->createDocumentFragment();
		//$pagebreak->createAttributeNS ('http://schemas.openxmlformats.org/wordprocessingml/2006/main', 'w:attr');

		$root = $doc->getElementsByTagName("document")->item(0);
		$node = $doc->getElementsByTagName("body")->item(0);

		//$pagebreak1 = $doc->createElement('w:p','');
		//$pagebreak2 = $doc->createElement('w:p','');
		$pagebreak1 = $doc->createElement('w:br','');
		$pagebreak2 = $doc->createElement('w:lastRenderedPageBreak','');
		/*$r1 = $doc->createElement('w:r','');
		$r2 = $doc->createElement('w:r','');
		$br = $doc->createElement('w:br','');
		$lastRenderedPageBreak = $doc->createElement('w:lastRenderedPageBreak','');

		$r1->appendChild($br);
		$r2->appendChild($lastRenderedPageBreak);
		$pagebreak1->appendChild($r1);
		$pagebreak2->appendChild($r2);	*/


		//$pagebreak->appendXML('<w:p w:rsidR="00801FEC" w:rsidRDefault="00801FEC"><w:r><w:br w:type="page"/></w:r></w:p><w:p w:rsidR="00284F46" w:rsidRDefault="00801FEC"><w:r><w:lastRenderedPageBreak/></w:r></w:p>');

		//loop around each record that we need to produce a letter for
		for($i = 0; $i < count($lookupData); $i++) {

			//if it is our first loop then newnode is node (the original) otherwise we clone the original and modify that.
			if ($i != 0) {
				$newnode = $node->cloneNode(true);
			} else {
				$newnode = $node;
			}
			$wts = $newnode->getElementsByTagNameNS('http://schemas.openxmlformats.org/wordprocessingml/2006/main', 'fldChar');

			for( $x = 0; $x < $wts->length; $x++ )
			{
				if( $wts->item( $x )->attributes->item(0)->nodeName == 'w:fldCharType' && $wts->item( $x )->attributes->item(0)->nodeValue == 'begin' )
				{
					$newcount = getMailMerge( $wts, $x, $letterdata[$i], $mergerecord);
					$x = $newcount['counter'];
				}
			}

			//if the record isnt the first letter then append another copy
			if ($i != 0) {
				$node->appendChild($pagebreak1);
				$node->appendChild($pagebreak2);
				$root->appendChild($newnode);
			}
			$mergerecord++;
			$doc->saveXML();
		}

		$zip->deleteName($file);
		$zip->addFromString( $file, $doc->saveXML() );
		$zip->close();

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header("Content-Disposition: attachment; filename=\"{$filename}.docx\"");
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($newfile));
		ob_clean();
		flush();
		readfile($newfile);
	}
}

// Written by Nige 17 April 2014
// When generating DD claim files you need to find the 1st working day after a date
// You need to generate a DD claim file x working days before the payment date
// This function will find that date - calculating weekends and bank holidays
function fn_findworkingdays($datknowndate,$intdays,$intdirection) {
	// $datknowndate = The date we know
	// $intdays = how many working days to move
	// $intdirection = -1 = $intdays before $datknowndate
	// $intdirection = 1  = $intdays after $datknowndate

	// working variables
	$intseconds = $intdirection * 86400;
	$intcountdays = 0;
	$intworkingday = 0;

	// find hour mins and secs of known date, and use these to generate holidays
	$inthour = date("H",$datknowndate);
	$intmins = date("i",$datknowndate);
	$intsecs = date("s",$datknowndate);

	// generate array of bank holidays
	$arrholidays = array();
		// normal bank holidays
		$datholiday = strtotime("first Monday of May ".date("Y",$datknowndate)) + 7200;
		$datholiday = gmmktime($inthour,$intmins,$intsecs,date("m",$datholiday),date("d",$datholiday),date("Y",$datholiday));
		array_push($arrholidays, $datholiday);
		$datholiday = strtotime("last Monday of May ".date("Y",$datknowndate)) + 7200;
		$datholiday = gmmktime($inthour,$intmins,$intsecs,date("m",$datholiday),date("d",$datholiday),date("Y",$datholiday));
		array_push($arrholidays, $datholiday);
		$datholiday = strtotime("last Monday of August ".date("Y",$datknowndate)) + 7200;
		$datholiday = gmmktime($inthour,$intmins,$intsecs,date("m",$datholiday),date("d",$datholiday),date("Y",$datholiday));
		array_push($arrholidays, $datholiday);

		// easter
		$intplusdays = easter_days(date("Y",$datknowndate));
		$datholiday = gmmktime($inthour,$intmins,$intsecs,3,21+$intplusdays-2,date("Y",$datknowndate));
		array_push($arrholidays, $datholiday);
		$datholiday = gmmktime($inthour,$intmins,$intsecs,3,21+$intplusdays+1,date("Y",$datknowndate));
		array_push($arrholidays, $datholiday);

		// christmas this year and last, just in case
		$datholiday = gmmktime($inthour,$intmins,$intsecs,12,25,date("Y",$datknowndate));
		while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
		array_push($arrholidays, $datholiday);
		$datholiday = gmmktime($inthour,$intmins,$intsecs,12,25,date("Y",$datknowndate)-1);
		while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
		array_push($arrholidays, $datholiday);

		// boxing day this year and last, just in case
		$datholiday = gmmktime($inthour,$intmins,$intsecs,12,26,date("Y",$datknowndate));
		while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
		array_push($arrholidays, $datholiday);
		$datholiday = gmmktime($inthour,$intmins,$intsecs,12,26,date("Y",$datknowndate)-1);
		while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
		array_push($arrholidays, $datholiday);

		// new years day this year and next, just in case
		$datholiday = gmmktime($inthour,$intmins,$intsecs,1,1,date("Y",$datknowndate));
		while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
		array_push($arrholidays, $datholiday);
		$datholiday = gmmktime($inthour,$intmins,$intsecs,1,1,date("Y",$datknowndate)+1);
		while ((date("w",$datholiday)) < 1 or (date("w",$datholiday)) > 5 or in_array($datholiday, $arrholidays)) { $datholiday = $datholiday + $intseconds; }
		array_push($arrholidays, $datholiday);

	// loop through counting the days
	do {
		if ((date("w",$datknowndate)) > 0 and (date("w",$datknowndate)) < 6 and !in_array($datknowndate, $arrholidays)) { $intcountdays++; }
		$datknowndate = $datknowndate + $intseconds;
	} while ($intcountdays < $intdays);


	// The end date here may be a w/e or holiday - so make sure it is a working day
	while ($intworkingday == 0) {
		if ((date("w",$datknowndate)) > 0 and (date("w",$datknowndate)) < 6 and !in_array($datknowndate, $arrholidays)) { $intworkingday = 1; } else { $datknowndate = $datknowndate + $intseconds;	}
	}

	return $datknowndate;
}




// Written by Nige on 31st March 2014 - Called from various pages including draw-process.php
// send $conn, date and record ID - returns new date
// $intdir : 1 = forwards / 2 = backwards
function fn_newdate($conn,$datdate,$intrecordID,$intdir=1) {
	$intmodifier = 1;
	switch ($intrecordID) {
		case 1: // weekly
			// add 7 * 24 * 60 * 60 seconds to last draw epoch
			$intdatechange = 604800;
			break;
		case 2: // monthly
			// need to break down $intdrawepoch and increment the month
			$intmodifier = 0;
			$intmonth = date("n",$datdate);
			$intyear = date("Y",$datdate);
			switch ($intdir) {
				case 1:
					// forward
					$intmonth++;
					break;
				case 2:
					//backward
					$intmonth--;
					break;
			}
			if ($intmonth > 12) {
				$intmonth = 1;
				$intyear++;
			}
			if ($intmonth < 1) {
				$intmonth = 12;
				$intyear--;
			}
			$datdate = gmmktime(date("H",$datdate),date("i",$datdate),date("s",$datdate),$intmonth,date("j",$datdate),$intyear);
			break;
		case 3:	// 2 weeks
			$intdatechange = (2 * 604800);
			break;
		case 4:	// 3 weeks
			$intdatechange = (3 * 604800);
			break;
		case 5:	// 4 weeks
			$intdatechange = (4 * 604800);
			break;
		case 6:	// 5 weeks
			$intdatechange = (5 * 604800);
			break;
		case 7:	// 6 weeks
			$intdatechange = (6 * 604800);
			break;
	}

	if ($intmodifier == 1) {
		switch ($intdir) {
			case 1:
				$datdate += $intdatechange;
				break;
			case 2:
				$datdate -= $intdatechange;
				break;
		}
	}


	return $datdate;
}


// Written by Nige on 14th March 2014 - This is used in SOP / Pathway 4 - Called from various
// Encrypts Data and returns encrypted string - if key not available, returns blank
function fn_encrypt($strdata,$iv) {
	$iv = substr("3185296328795229".$iv, -16);
	$strfullkey = $_SESSION['mwldecrypt'];
	$strpartkey = "Gmj*cfd3";

	$strfirstdata = substr($strdata,0,-4);
	$strseconddata = substr($strdata,-4);

	if ($strfullkey == "") {
		$strreturn = "-x-";
	} else {
		$strreturn = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $strfullkey, $strfirstdata, MCRYPT_MODE_CBC, $iv)."####".mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $strpartkey, $strseconddata, MCRYPT_MODE_CBC, $iv);
	}

	return $strreturn;
}

// Decrypt full string
function fn_decrypt($strdata,$iv) {
	$iv = substr("3185296328795229".$iv, -16);
	$strfullkey = $_SESSION['mwldecrypt'];
	$strpartkey = "Gmj*cfd3";

	$intlocation = strpos($strdata, "####");
	$strfirstdata = substr($strdata,0,$intlocation);
	$strseconddata = substr($strdata,$intlocation+4);

	if ($strfullkey == "") {
		$strreturn = "xxxx".mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $strpartkey, $strseconddata, MCRYPT_MODE_CBC, $iv);
		$intlocation = strpos($strreturn, chr(0));
		$strreturn = substr($strreturn,0,$intlocation);
	} else {
		$strreturn = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $strfullkey, $strfirstdata, MCRYPT_MODE_CBC, $iv);
		$intlocation = strpos($strreturn, chr(0));
		$strreturn = substr($strreturn,0,$intlocation);
		$strreturn .= mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $strpartkey, $strseconddata, MCRYPT_MODE_CBC, $iv);
		$intlocation = strpos($strreturn, chr(0));
		$strreturn = substr($strreturn,0,$intlocation);
	}

	return $strreturn;
}

// Decrypt part string
function fn_partdecrypt($strdata,$iv) {
	$iv = substr("3185296328795229".$iv, -16);
	$strpartkey = "Gmj*cfd3";

	$intlocation = strpos($strdata, "####");
	if ($intlocation < 1) {
		$strreturn = "";
	} else {
		$strseconddata = substr($strdata,$intlocation+4);
		$strreturn = "xxxx".mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $strpartkey, $strseconddata, MCRYPT_MODE_CBC, $iv);
	}

	$intlocation = strpos($strreturn, chr(0));
	$strreturn = substr($strreturn,0,$intlocation);

	return $strreturn;
}


// Written by Nige on 8th April 2014
// Function which takes a string and a number
// and returns string number characters long - either truncated or padded with spaces
function fn_fixedlength($strstring,$intlength,$intpaddirection=STR_PAD_RIGHT,$strpadchr=" ",$strquote="") {
	$strstring = str_pad($strstring, $intlength, $strpadchr, $intpaddirection);
	$strstring = substr($strstring, 0, $intlength);
	return $strquote.$strstring.$strquote;
}


// Written by Nige on 10th April 2014
function fn_delimited($strstring,$intmin,$intmax,$inttype,$strdelimiter,$intnumberFields,$intthisfield,$intpaddirection=STR_PAD_RIGHT,$strpadchr=" ",$strquote="",$strprefixchr="") {
	if ($strprefixchr == "~") { $strprefixchr = "£"; }
	switch ($inttype) {
		case "0":
			// fixed
			$strstring = fn_fixedlength($strprefixchr.$strstring,$intmax, $intpaddirection, $strpadchr, $strquote);
			break;
		case "1":
		case "2":
			// Delimited
			$strstring = str_pad($strprefixchr.$strstring, $intmin, $strpadchr, $intpaddirection);
			$strstring = substr($strstring, 0, $intmax);
			$strstring = $strquote.$strstring.$strquote;
			if ($intthisfield < $intnumberFields) { $strstring .= $strdelimiter; }
			break;
	}
	return $strstring;
}



// Email Function - easy function that sets up the headers and such for emails, it will return an array to say if successful or error.
function sendemail($from, $fromemail, $replyto, $EmailTo, $subject, $textmessage, $htmlmessage, $useTemplate, $sendtoself) {

	// Create the mail transport configuration
	$transport = Swift_MailTransport::newInstance();

	// Create the message
	$message = Swift_Message::newInstance();
	$message->setTo(array(
	  "$EmailTo" => "$EmailTo"
	));
	$message->setSubject("$subject");
	$message->setBody("$textmessage");
	if ($useTemplate == 1) {
		$message->addPart(email_template($htmlmessage, $subject), 'text/html');
	} else {
		$message->addPart("$htmlmessage", 'text/html');
	}
	$message->setFrom("$fromemail", "$from");

	// Send the email
	$mailer = Swift_Mailer::newInstance($transport);
	if ($mailer->send($message)){
		if ($sendtoself) mail($fromemail, $Subject, $Body, $headers);
		$strmessage["success"] = 'Email successfully sent.';
		return $strmessage;
	} else {
		$strmessage["error"] = 'Email could not be sent. Please try again';
		return $strmessage;
	}
}

function email_template ($content, $subject){

	global $companyName, $companyPhone, $companyEmail;

	$template = ("<!doctype html>
		<html xmlns='http://www.w3.org/1999/xhtml'>
		<head>

		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

		<title>$companyName</title>

		<style type='text/css'>
			.ReadMsgBody {width: 100%; background-color: #ffffff;}
			.ExternalClass {width: 100%; background-color: #ffffff;}
			body	 {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Georgia, Times, serif}
			table {border-collapse: collapse;}

			@media only screen and (max-width: 640px)  {
							body[yahoo] .deviceWidth {width:440px!important; padding:0;}
							body[yahoo] .center {text-align: center!important;}
					}

			@media only screen and (max-width: 479px) {
							body[yahoo] .deviceWidth {width:280px!important; padding:0;}
							body[yahoo] .center {text-align: center!important;}
					}

		</style>
		</head>

		<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0' yahoo='fix' style='font-family: Georgia, Times, serif'>

		<!-- Wrapper -->
		<table width='640px' border='0' cellpadding='0' cellspacing='0' align='center'>
			<tr>
				<td width='100%' valign='top' bgcolor='#ffffff' style='padding-top:20px'>

					<!-- Start Header-->
					<table width='580' border='0' cellpadding='0' cellspacing='0' align='center' class='deviceWidth'>
						<tr>
							<td width='100%' bgcolor='#ffffff'>

									<!-- Logo -->
									<table border='0' cellpadding='0' cellspacing='0' align='left' class='deviceWidth'>
										<tr>
											<td style='padding:10px 20px' class='center'>
												<a href='#'><img src='' alt='' border='0' /></a>
											</td>
										</tr>
									</table><!-- End Logo -->

									<!-- Nav -->
									<table border='0' cellpadding='0' cellspacing='0' align='right' class='deviceWidth'>
										<tr>
											<td class='center' style='font-size: 13px; color: #272727; font-weight: light; text-align: right; font-family: Georgia, Times, serif; line-height: 20px; vertical-align: middle; padding:10px 20px; font-style:italic'>
												<p href='#' style='text-decoration: none; color: #3b3b3b;'></p>
											</td>
										</tr>
									</table><!-- End Nav -->

							</td>
						</tr>
					</table><!-- End Header -->

					<!-- One Column -->
					<table width='580'  class='deviceWidth' border='0' cellpadding='0' cellspacing='0' align='center' bgcolor='#eeeeed'>
						<tr>
							<td style='font-size: 13px; color: #959595; font-weight: normal; text-align: left; font-family: Georgia, Times, serif; line-height: 24px; vertical-align: top; padding:20px' bgcolor='#eeeeed'>

								<table>
									<tr>
										<td valign='middle' style='padding:0 10px 10px 0'><a href='#' style='text-decoration: none; color: #272727; font-size: 16px; color: #272727; font-weight: bold; font-family:Arial, sans-serif '>$subject</a>
										</td>
									</tr>
								</table>

								$content
							</td>
						</tr>
					</table><!-- End One Column -->

					<!-- 4 Columns -->
					<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
						<tr>
							<td bgcolor='#363636' style='padding:20px;'>
								<table width='540' border='0' cellpadding='0' cellspacing='0' align='center' class='deviceWidth'>
									<tr>
										<td>
											<table width='45%' cellpadding='0' cellspacing='0'  border='0' align='left' class='deviceWidth'>
												<tr>
													<td valign='top' style='font-size: 11px; color: #f1f1f1; color:#999; font-family: Arial, sans-serif; padding-bottom:20px' class='center'>
														<span style='color: #999999; font-size: 24px; '></span><br/>
														<b style='color: #5b5b5b;'></b><br/><br/>
													</td>
												</tr>
											</table>

											<table width='40%' cellpadding='0' cellspacing='0'  border='0' align='right' class='deviceWidth'>
												<tr>
													<td valign='top' style='font-size: 11px; color: #f1f1f1; font-weight: normal; font-family: Georgia, Times, serif; line-height: 26px; vertical-align: top; text-align:right' class='center'>


														<a href='#' style='text-decoration: none; color: #848484; font-weight: normal;'>$companyPhone</a><br/>
														<a href='#' style='text-decoration: none; color: #848484; font-weight: normal;'>$companyEmail</a>
													</td>
												</tr>
											</table>
										</td>
									</tr>

								</table>
							</td>
						</tr>
					</table><!-- End 4 Columns -->

				</td>
			</tr>
		</table> <!-- End Wrapper -->
		</body>
		</html>");
	return $template;
}

//Function to get a random character/s from the string that is passed - used for login memorable information
function getRandChars ($numPositions = 1, $string = 'xxxxxxxx') {
	$positionArray = array();
	for ($i=0; $i < $numPositions; $i++) {
		$position = rand(1, strlen($string));
		if (in_array($position, $positionArray)) {
			$i--;
		} else {
			$positionArray[$i] = $position;
		}
	}
	return $positionArray;
}

//Function to represent a number like '2nd', '10th', '101st' etc
function text_number($n){
	$teen_array = array(11, 12, 13, 14, 15, 16, 17, 18, 19);
	$single_array = array(1 => 'st', 2 => 'nd', 3 => 'rd', 4 => 'th', 5 => 'th', 6 => 'th', 7 => 'th', 8 => 'th', 9 => 'th', 0 => 'th');
	$if_teen = substr($n, -2, 2);
	$single = substr($n, -1, 1);
	if ( in_array($if_teen, $teen_array) ){
		$new_n = $n . 'th';
	} else if ( $single_array[$single] ){
		$new_n = $n . $single_array[$single];
	}
	return $new_n;
}

function remote_file_exists($path){
	return (@fopen($path,"r")==true);
}

//Same as strlength except stops at fullstop
function abbr($str, $maxLen) {
	if (strlen($str) > $maxLen && $maxLen > 1) {
		preg_match("#^.{1,".$maxLen."}\.#s", $str, $matches);
		return $matches[0];
	} else {
		return $str;
	}
}

function imageupload($location, $name, $inputname) {

	//upload image to images folder
	$strerror = "";
	$arrallowlist = array("jpg", "jpeg", "png", "gif");
	$newfilename = strtolower($_FILES[$inputname]['name']);
	$strfileextn = trim(strtolower(substr($newfilename,-4)),".");

	$filename = "/files/images/".$location;
	//create directory if it does not currently exist
	if (!file_exists($filename)){
		$makedir = recursive_mkdir($filename);
	}
	if ($makedir) {
		$filename = $_SERVER['DOCUMENT_ROOT'].$filename.$name.".".$strfileextn;

		if ($_FILES[$inputname]['error'] > 0)
		{
			switch ($_FILES[$inputname]['error'])
			{
				case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
				case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
				case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
				case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
			}
		}
		else
		{
			// Does the file have the right extension
			if (!in_array($strfileextn,$arrallowlist))
			{
				$strerror = "Problem: The file you selected is not an acceptable format.<br>You may only upload image files in jpeg, gif or png format.<br><br>The file you are trying to upload is a ".$_FILES[$inputname]['type']." file.<br><br>Please convert the picture to the required format and try again.";
			}
			else
			{
				if (is_uploaded_file($_FILES[$inputname]['tmp_name']))
				{
					if (!move_uploaded_file($_FILES[$inputname]['tmp_name'], $filename))
					{
						$strerror = "Problem: File is uploaded to :- ".$_FILES[$inputname]['tmp_name']." and could not move file to destination directory ($filename). Please try again.";
					}
				}
				else
				{
					$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES[$inputname]['name'].". Please try again.<br>";
				}
			}
		}
	} else {
		$strerror = "Problem: Could not find or create the directory";
	}

	$imagedetails['location'] = $filename;
	$imagedetails['extension'] = $strfileextn;
	$imagedetails['error'] = $strerror;
	return $imagedetails;
}

function curPageName() {
	return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
}

function getSubCategories($parentID, $level, $maxLevel, $conn) {

	$level++;

	$style = "style='padding-left: ".(($level + 1) * 10)."px; display: none; border-left: 1px dotted; min-height: 17px;'";

	$getsubcatsQuery = "SELECT category.* FROM category INNER JOIN category_relations ON category.recordID = category_relations.childID WHERE parentID = :parentID AND level = ".$level." ORDER BY category_relations.menuOrder";
	$strType = "multi";
	$arrdbparam = array("parentID" => $parentID);
	$subcats = query($conn, $getsubcatsQuery, $strType, $arrdbparam);

	$i = 1;

	foreach ($subcats as $subcat) {
		if ($level <= $maxLevel)
		{
			$numNestedQuery = "SELECT COUNT(*) AS num_nested FROM category_relations WHERE parentID = :categoryID AND level = ".($level + 1);
			$strType = "single";
			$numnesteddbparam = array( "categoryID" => $subcat['recordID'] );
			$numNested = query($conn, $numNestedQuery, $strType, $numnesteddbparam);
			//echo $numNested;
			//var_dump($numnesteddbparam);
		}


		print ("<li id='".$subcat['recordID']."' data-level='".$level."' data-order='".$i."' data-parentid='".$parentID."'>");
			print ("<span><span class='expand_collapse'>+</span> ".$subcat['contentName']." (".$subcat['metaPageLink'].") <a href='#' onclick='jsviewCategory(".$subcat['recordID']."); return false;'>Edit</a></span>");
			if ($level <= $maxLevel)
			{
				print ("<ul class='sortableCategoryList sortableSubcategoryList' ".$style.">");
				if ($numNested['num_nested'] > 0)
				{
					getSubCategories($subcat['recordID'], $level, $maxLevel, $conn);
				}
				print ("</ul>");
				$i++;
			}
		print ("</li>");
	}

}

//creates a pdf of the html content sent to it
function pdfprint ($html,$strpdforientation) {

	// Include the main TCPDF library (search for installation path).
	require_once('tcpdf_include.php');

	// create new PDF document
	$pdf = new TCPDF($strpdforientation, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Author');
	$pdf->SetTitle('Test');
	$pdf->SetSubject('Testing');

	// turn headers and footers off
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(5, 15, 5);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------

	// set font
	$pdf->SetFont('helvetica', '', 10);

	// add a page
	$pdf->AddPage();

	$css = ("<!-- EXAMPLE OF CSS STYLE -->
	<style>
	table {
		border: 0 none;
		border-collapse: collapse;
		padding: 0;
	}
	th {
		border-bottom: 1px solid #E1E1E1;
		font-size: 10px;
		font-weight: normal;
		padding: 5px;
	}
	table.tabTable {
		background: none repeat scroll 0 0 #FFFFFF;
		border-left: 1px solid #E1E1E1;
		border-top: 1px solid #E1E1E1;
		margin: 14px 0 5px;
	}
	table.tabTable th {
		background: none repeat scroll 0 0 #FCFCFC;
		border-bottom: 1px solid #E1E1E1;
		border-right: 1px solid #E1E1E1;
		font-size: 10px;
		font-weight: normal;
		padding: 5px;
	}
	table.tabTable th a {
		font-weight: bold;
		line-height: 20px;
		text-decoration: none;
	}
	table.tabTable td {
		border-bottom: 1px solid #E1E1E1;
		border-right: 1px solid #E1E1E1;
		padding: 5px;
	}
	table.tabTable td .listingDescription {
		font-size: 12px;
		line-height: 17px;
		padding: 5px 0;
	}
	table.tabTable td a {
		line-height: 20px;
		text-decoration: none;
	}</style>");

	$html = $css.$html;
	// output the HTML content
	$pdf->writeHTML($html, true, false, true, false, '');

	// reset pointer to the last page
	$pdf->lastPage();

	// ---------------------------------------------------------
	ob_clean();
	//Close and output PDF document
	$pdf->Output($documentname.".pdf", 'I');
}

//logs the user out of the system. The function is called when someone goes to ?logout - the code which does this is up above.
function logout() {
	session_destroy();
	header ("Location: /admin/login.php");
}

//format a name
function FormatName($name=NULL){
	if (empty($name))
	return false;

	$name = strtolower($name);
	$name = preg_replace("[\-]", " - ",$name); // Surround hyphens with our delimiter so our strncmp is accurate

	//if (preg_match("/^[a-z]{2,}$/i",$name))  // Simple preg_match if statement
	//{
		$names_array = explode(' ',$name);  // Set the delimiter as a space.

		for ($i = 0; $i < count($names_array); $i++)
		{
			if (strncmp($names_array[$i],'mc',2) == 0 || ereg('^[oO]\'[a-zA-Z]',$names_array[$i]))
			{
			   $names_array[$i][2] = strtoupper($names_array[$i][2]);
			}
			$names_array[$i] = ucfirst($names_array[$i]);
		}
		$name = implode(' ',$names_array);

		$name = preg_replace("[ \- ]", "-",$name); //  Remove the extra instances of our delimiter
		return ucwords($name);
	//}
}

//create directories on the server
function recursive_mkdir($path, $mode = 0777) {
	$dirs = explode(DIRECTORY_SEPARATOR , $path);
	$count = count($dirs);
	$path = '.';
	for ($i = 0; $i < $count; ++$i) {
		$path .= DIRECTORY_SEPARATOR . $dirs[$i];
		if (!is_dir($path) && !mkdir($path, $mode)) {
			return false;
		}
		//chmod($path, 0777);
	}
	return true;
}

// Take a date string (yyyy-mm-dd hh:mm:ss) and return the epoch time (or 0 if empty). (flag = 0 = usetime : 1 = 12:00:00)
function fntimeconvert($strdate,$intflag) {
	if (strlen($strdate) == 0) {
		$intepoch = 0;
	} else {
		if ($intflag == 0) {
			// Use time
			$intepoch = gmmktime(substr($strdate,11,2),substr($strdate,14,2),substr($strdate,17,2),substr($strdate,5,2),substr($strdate,8,2),substr($strdate,0,4));
		} else {
			// use 12:00:00 for time
			$intepoch = gmmktime(12,0,0,substr($strdate,5,2),substr($strdate,8,2),substr($strdate,0,4));
		}
	}

	return $intepoch;
}

//very simple function to convert a unix timestamp
function fnconvertunixtime ($strtime, $strformat = "d/m/Y") {
	return date($strformat, $strtime);
}

// Checked by Nige on 6th March 2014 - used in fngenerateNumber() in this file
// generates a random number that can be used as a new account number
function fngetRandomString($length) {
	$validCharacters = "0123456789";
	$validCharNumber = strlen($validCharacters);
	$result = "";
	for ($i = 0; $i < $length; $i++) {
		$index = mt_rand(0, $validCharNumber - 1);
		$result .= $validCharacters[$index];
	}
	return $result;
}

// Checked by Carl on 27th March 2014 - used in forgotmemorableinfo
// generates a random string of characters that can be used as memorable info
function fngetRandomStringChar($length) {
	$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$size = strlen( $chars );
	for( $i = 0; $i < $length; $i++ ) {
		$str .= $chars[ rand( 0, $size - 1 ) ];
	}
	return $str;
}


// Written by Nige on 8th April 2014
// Dump an array of fixed length lines (in array) to a file
function fndataexportfixed($dataarray, $filename="export.txt", $streol=PHP_EOL) {
	ob_end_clean();
	$f = fopen('php://memory', 'w');
	foreach ($dataarray as $line) {
		fputs($f,$line.$streol);
	}
	// rewrite the "file" with the txt lines
	fseek($f, 0);
	// tell the browser it's going to be a txt file
	header('Content-Type: application/txt');
	// tell the browser we want to save it instead of displaying it
	header('Content-Disposition: attachement; filename="'.$filename.'"');
	// make php send the generated txt lines to the browser
	fpassthru($f);
	exit();
}


// IMPORT FUNCTIONS
// ================
function importDataValidation($rules, $data, $importDataRowID, $conn) {
	$importRowErrors = array();

	$i = 0;
	foreach($rules AS $rule)
	{
		switch ($rule) {
			case "stockGroupMetaLink":

				if ($data['stockGroupMetaLink'] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stock Group Meta Link Missing</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Stock Group Meta Link</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;

			case "stockGroupMetaLinkExists":

				$checkGroupExistsQuery = "SELECT * FROM stock_group_information WHERE metaLink = :stockGroupMetaLink";
				$strType = "single";
				$arrdbparam = array("stockGroupMetaLink" => $data['stockGroupMetaLink']);
				$checkGroupExists = query($conn, $checkGroupExistsQuery, $strType, $arrdbparam);

				if ($data['stockGroupMetaLink'] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stock Group Meta Link Missing</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Stock Group Meta Link</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

				} else if ($data['stockGroupMetaLink'] != "" AND empty($checkGroupExists) AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stock Group Meta Link Not Valid</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Stock Group Meta Link</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['stockGroupMetaLink']."' />";

				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;

			case "stockCode":

				if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['stockCode'] = $data['stockCode'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stockcode Missing</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Stockcode</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;

			case "stockPrice":

				if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['stockCode'] = $data['stockCode'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stock Price Missing</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Stock Price</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;

			case "stockPriceControl":

				if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['stockCode'] = $data['stockCode'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stock Price Control Missing</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Stock Control Price</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;

			case "assocCatMetaPageLink":

				$checkCatExistsQuery = "SELECT * FROM category WHERE metaPageLink = :assocCatMetaPageLink";
				$strType = "single";
				$arrdbparam = array("assocCatMetaPageLink" => $data['assocGroupMetaLink']);
				$checkCatExists = query($conn, $checkCatExistsQuery, $strType, $arrdbparam);

				if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['catMetaLink'] = $data['assocGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Meta Link Missing</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Category Meta Link</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

				} else if ($data[$rule] != "" AND empty($checkCatExists) AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['catMetaLink'] = $data['assocGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Meta Link Not Valid</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Category Meta Link</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['assocGroupMetaLink']."' />";

				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;

			case "assocCatMetaRangeLink":

				$checkRangeExistsQuery = "SELECT * FROM stock_ranges WHERE metaPageLink = :assocRangeMetaPageLink";
				$strType = "single";
				$arrdbparam = array("assocRangeMetaPageLink" => $data['assocGroupMetaLink']);
				$checkCatExists = query($conn, $checkRangeExistsQuery, $strType, $arrdbparam);

				if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['catMetaLink'] = $data['assocGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Meta Link Missing</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Category Meta Link</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

				} else if ($data[$rule] != "" AND empty($checkCatExists) AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['catMetaLink'] = $data['assocGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Meta Link Not Valid</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Category Meta Link</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['assocGroupMetaLink']."' />";

				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;

			case "assocGroupMetaLink":

				$checkCatExistsQuery = "SELECT * FROM category WHERE metaPageLink = :assocCatMetaPageLink";
				$strType = "single";
				$arrdbparam = array("assocCatMetaPageLink" => $data['assocGroupMetaLink']);
				$checkCatExists = query($conn, $checkCatExistsQuery, $strType, $arrdbparam);

				if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['catMetaLink'] = $data['assocGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Meta Link Missing</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Category Meta Link</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

				} else if ($data[$rule] != "" AND empty($checkCatExists) AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['catMetaLink'] = $data['assocGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Meta Link Not Valid</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Category Meta Link</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['assocGroupMetaLink']."' />";

				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;

			case "assocCatType":

				if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['assocCatType'] = $data['assocCatType'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category type ('category' or 'range') not stated</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Category Type</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

				} else if ($data[$rule] != "category" AND $data[$rule] != "range") { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['assocCatType'] = $data['assocCatType'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Category Type Not Valid, must be 'category' or 'range'</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Category Type</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['assocCatType']."' />";

				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;

			case "groupName":

				if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Group Name Missing</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Group Name</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;

			case "groupDescription":

				if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Group Description Missing</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Group Description</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;

			case "groupRetailPrice":

				if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Group Retail Price Missing</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Group Retail Price</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;

			case "groupStatus":

				$checkStatusExistsQuery = "SELECT * FROM stock_status WHERE description = :groupStatus";
				$strType = "single";
				$arrdbparam = array("groupStatus" => $data['groupStatus']);
				$checkStatusExists = query($conn, $checkStatusExistsQuery, $strType, $arrdbparam);

				if ($data[$rule] == "" AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Group Status Missing</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Enter Stock Group Status</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='' />";

				} else if ($data[$rule] != "" AND empty($checkStatusExists) AND empty($data['error-'.$rule.'-'.$importDataRowID])) { //if there is an error this is how to show it in each row

					$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
					$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Stock Group Status Not Valid</label>";
					$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Stock Group Status (must be 'Deleted', 'Do not show on site', 'Standard', 'Web Exclusive' or 'Catalogue Only')</label>";
					$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['groupStatus']."' />";

				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;

			case "groupBrandMetaLink":

				if ($data[$rule] != "") { //if there is an error this is how to show it in each row

					$checkBrandExistsQuery = "SELECT * FROM stock_brands WHERE metaPageLink = :groupBrandMetaLink";
					$strType = "single";
					$arrdbparam = array("groupBrandMetaLink" => $data['groupBrandMetaLink']);
					$checkBrandExists = query($conn, $checkBrandExistsQuery, $strType, $arrdbparam);

					if(empty($checkBrandExists) AND empty($data['error-'.$rule.'-'.$importDataRowID])) {

						$importRowErrors[$i][$importDataRowID]['stockGroupMetaLink'] = $data['stockGroupMetaLink'];
						$importRowErrors[$i][$importDataRowID]['message'] = "<label for='error-".$rule."-".$importDataRowID."'>Brand Meta Link Not Valid</label>";
						$importRowErrors[$i][$importDataRowID]['action'] = "<label for='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."'>Re-enter Brand Meta Link</label>";
						$importRowErrors[$i][$importDataRowID]['input'] = "<input id='error-".$rule."-".$importDataRowID."' name='error-".$rule."-".$importDataRowID."' type='text' value='".$data['groupBrandMetaLink']."' />";

					} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
						$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
						unset($data['error-'.$rule.'-'.$importDataRowID]);
					}
				} else if (!empty($data['error-'.$rule.'-'.$importDataRowID])) {
					$data[$rule] = $data['error-'.$rule.'-'.$importDataRowID];
					unset($data['error-'.$rule.'-'.$importDataRowID]);
				}
			break;
		}
		$i++;
	}
	return $importRowErrors;
}

// EXPORT FUNCTIONS
// ================
function dataexportcsv($dataarray, $filename = "export.csv", $delimiter=",") {

	ob_end_clean();
	$f = fopen('php://memory', 'w');
	// loop over the input array
	foreach ($dataarray as $line) {
		// generate csv lines from the inner arrays
		fputcsv($f, $line, $delimiter);
	}
	// rewrite the "file" with the csv lines
	fseek($f, 0);
	// tell the browser it's going to be a csv file
	header('Content-Type: application/csv;charset=UTF-8');
	// tell the browser we want to save it instead of displaying it
	header('Content-Disposition: attachement; filename="'.$filename.'"');
	// make php send the generated csv lines to the browser
	fpassthru($f);
	exit();
}

//Class to convert fixed width files into CSV format
class fixed2CSV extends SplFileObject{
	public function __construct( $filename ){parent::__construct( $filename );}

	/*
	* Settor, is called when trying to assign a value to non-existing property
	*
	* @access    public
	* @param    string    $name    The name of the property to set
	* @param    mixed    $value    The value of the property
	* @throw    Excption if property is not able to be set
	*
	*/
	public function __set( $name, $value )
	{
		switch( $name )
		{
			case 'eol':
			case 'fields':
			case 'separator':
			$this->$name = $value;
			break;

			default:
			throw new Exception("Unable to set $name");
		}
	}

	/**
	*
	* Gettor This is called when trying to access a non-existing property
	*
	* @access    public
	* @param    string    $name    The name of the property
	* @throw    Exception if proplerty cannot be set
	* @return    string
	*
	*/
	public function __get( $name )
	{
		switch( $name )
		{
			case 'eol':
			return "\n";

			case 'fields':
			return array();

			case 'separator':
			return ',';

			default:
			throw new Exception("$name cannot be set");
		}
	}

	/**
	*
	* Over ride the parent current method and convert the lines
	*
	* @access    public
	* @return    string    The line as a CSV representation of the fixed width line, false otherwise
	*
	*/
	public function current()
	{
		if( parent::current() )
		{
			$csv = '';
			$fields = new cachingIterator( new ArrayIterator( $this->fields ) );
			foreach( $fields as $f )
			{
				$csv .= trim( substr( parent::current(), $fields->key(), $fields->current()  ) );
				$csv .= $fields->hasNext() ? $this->separator : $this->eol;
			}
			return $csv;
		}
		return false;
	}
}


// DATABASE FUNCTIONS
// ================
function connect($driver = DRIVER, $username = USER, $password = PASS, $host = HOST, $db = DB) {
	try
	{
		// Define new PHP Data Object connection
		$conn = new PDO(''.$driver.':host='.$host.';dbname='.$db, $username, $password);
		// Sets error reporting level
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
		// Sets charset
		$conn->exec("set NAMES utf8");
		$conn->exec("SET CHARSET utf8");
		return $conn;
	}
	catch(PDOException $e)
	{
		// If failed, display error
		$strdebug = "ERROR: " . $e->getMessage();
	}
}

/**
 * @param $conn
 * @param $querystring
 * @param $querytype
 * @param null $values
 * @return array|string
 */
function query($conn, $querystring, $querytype, $values = null){
		if (is_object($conn) && method_exists($conn, 'prepare')) {
			// Prepares query
			try {
				$query = $conn->prepare($querystring);
				
				// Flag for if this statement to be executed multiple times
				$boolMultiQuery = false;
				
				if(!empty($values)) {
					
					// A little crude, but basically checking if $values is a multidimensional array
					if(isset($values[0]) && is_array($values[0])) {
						// If so, iterate over $values as $paramSet...
						foreach($values AS $paramSet) {
							// ... then over $paramSet as $paramValue, calling bindParameter on each (see below) and set multiple statement flag to true
							$boolMultiQuery = true;
							if(method_exists($query, 'bindParam')) {
								foreach($paramSet AS $paramName => $paramValue) {
									bindParameter($query, $paramName, $paramValue);

								}
								// Execute query with binded values
								$query->execute();
							} else {
								$query->execute($paramSet);
							}
						}
					} else {
						if(method_exists($query, 'bindParam')) {
							// $values single dimension array, so iterate over as $paramValue and call bindParameter on each
							foreach($values AS $paramName => $paramValue) {
								bindParameter($query, $paramName, $paramValue);
							}
						}
					}
				}
				
				// Executes query once if $boolMultiQuery still false
				if(!$boolMultiQuery) {
					if(method_exists($query, 'bindParam')) {
						$query->execute();
					} else {
						$query->execute($values);
					}
				}
			} catch (PDOException $e){
				return 'Database Query failed: ' . $e->getMessage();
			}
	
			try {	
				switch ($querytype)
				{
					case "single":
						// Returns associative array of single result
						$result = $query->fetch(PDO::FETCH_ASSOC);
						break;

					case "multi":
						// Returns multidimensional associative array of all results
						$result = $query->fetchAll(PDO::FETCH_ASSOC);
						break;

					case "create":
						// Return boolean true to confirm it worked (exception sent as text on execute if failed)
						$result = true;
						break;

					case "count":
						// Returns number of selected rows
						$result = $query->rowCount();
						break;

					case "insert":
						// Insert new row and return is fld_counter (auto increment) of the inserted line
						$result = $conn->lastInsertID();
						break;

					case "update":
						// Return number of rows updated by query
						$result = $query->rowCount();
						break;

					case "delete":
						// Return number of rows deleted by query
						$result = $query->rowCount();
						break;

					case "columns":
						// Returns associative array of columns - to test dynamic field names against database columns 
						$result = $query->fetchAll(PDO::FETCH_COLUMN);
						break;

					default:
						// Invalid query type
						$result = "Invalid !";
						break;
				}

				return $result;
			} catch (PDOException $e){
				return 'Database Result failed: ' . $e->getMessage();
			}
		}
		else
		{
			return "Failure - Database Connection Invalid";
		}
}
		
function bindParameter($query, $paramName, $paramValue)
{
	// Prepend ':' to $paramName
	$paramName = ":".$paramName;
	
	// Find type of $paramValue
	$paramType = gettype($paramValue);
	
	// Test value of $paramType and bind as correct data type based on this
	switch($paramType) {
		case "string":
		case "double":
			$query->bindParam($paramName, $paramValue, PDO::PARAM_STR);
			break;
		case "integer":
			$query->bindParam($paramName, $paramValue, PDO::PARAM_INT);
			break;
		case "boolean":
			$query->bindParam($paramName, $paramValue, PDO::PARAM_BOOL);
			break;
		case "null":
			$query->bindParam($paramName, $paramValue, PDO::PARAM_NULL);
			break;
		default: 
			$query->bindParam($paramName, $paramValue);
			break;
					
	}
}

?>