<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_reviewsvisibility"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_GET);
	
	$strdbsql = "UPDATE stock_reviews SET shownOnSite = :shownOnSite WHERE recordID = :recordID";
	$arrdbparams = array('recordID' => $_GET['reviewID'], 'shownOnSite' => $_GET['visibility']);
	$strType = "update";
	
	query($conn, $strdbsql, $strType, $arrdbparams);
	
	$conn = null; // close the Database connection after all processing
?>
