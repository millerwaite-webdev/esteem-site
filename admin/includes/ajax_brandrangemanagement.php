<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_brandrangemanagement"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_POST);
	foreach($_POST AS $brandsData)
	{
		//var_dump($brandsData);
		$i = 0;
		$intBrands = count($brandsData);
		foreach($brandsData AS $brandData)
		{
			$strcmd = $brandData['cmd'];
			//var_dump($_POST);
			switch ($strcmd)
			{
				case "add":
				case "reorder":
					
					//var_dump($_POST);
					$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM stock_ranges WHERE brandID = :brandID";
					$strType = "single";
					$arrdbparam = array( "brandID" => $brandData['brandID'] );
					$maxOrder = query($conn, $maxOrderQuery, $strType, $arrdbparam);
					
					if ($strcmd == "reorder")
					{
						$currOrderQuery = "SELECT menuOrder FROM stock_ranges WHERE recordID = :rangeID AND brandID = :brandID";
						$strType = "single";
						$arrdbparam = array( "rangeID" => $brandData['rangeID'], "brandID" => $brandData['brandID'] );
						$currOrder = query($conn, $currOrderQuery, $strType, $arrdbparam);
						
						if ($currOrder['menuOrder'] != $brandData['order'])
						{
							if (($currOrder['menuOrder'] < $brandData['order']))
							{
								//echo "move down\n";
								$updateOrdersQuery = "UPDATE stock_ranges SET menuOrder = menuOrder - 1 WHERE brandID = :brandID AND menuOrder <= :newOrder AND menuOrder >= :prevOrder";
								$strType = "update";
								$arrdbparam = array(
												"brandID" => $brandData['brandID'],
												"prevOrder" => $currOrder['menuOrder'],
												"newOrder" => $brandData['order']
											);
								
								if ($brandData['order'] <= $maxOrder['max_order'])
								{
									//echo "move down before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							elseif ($currOrder['menuOrder'] > $brandData['order'])
							{
								
								//echo "move up\n";
								$updateOrdersQuery = "UPDATE stock_ranges SET menuOrder = menuOrder + 1 WHERE brandID = :brandID AND menuOrder <= :prevOrder AND menuOrder >= :newOrder";
								$strType = "update";
								$arrdbparam = array(
												"brandID" => $brandData['brandID'],
												"prevOrder" => $currOrder['menuOrder'],
												"newOrder" => $brandData['order']
											);
								if ($brandData['order'] > 0)
								{
									//echo "move up before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							
							if ($brandData['order'] > 0 && $brandData['order'] <= $maxOrder['max_order'])
							{
								//echo "move level\n";
								$updateOrderQuery = "UPDATE stock_ranges SET menuOrder = :order WHERE recordID = :rangeID AND brandID = :brandID";
								$strType = "update";
								$arrdbparams = array(
												"order" => $brandData['order'],
												"rangeID" => $brandData['rangeID'],
												"brandID" => $brandData['brandID']
											);
								
								//echo "reorder\n";
								$updateOrder = query($conn, $updateOrderQuery, $strType, $arrdbparams);
							}
						}
					}
					elseif ($strcmd == "add")
					{
						$currOrder['menuOrder'] = $brandData['order'];
						
						if ($brandData['order'] > 0)
						{
							if ($brandData['order'] <= $maxOrder['max_order'])
							{
								//echo "add\n";
								$updateOrdersQuery = "UPDATE stock_ranges SET menuOrder = menuOrder + 1 WHERE brandID = :brandID AND menuOrder >= :order";
								$strType = "update";
								$arrdbparam = array(
												"brandID" => $brandData['brandID'],
												"order" => $brandData['order']
											);
								
								//echo "move down before addition\n";
								$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
							}
							//echo "add to level\n";
							$addOrderQuery = "UPDATE stock_ranges SET brandID = :brandID, menuOrder = :order WHERE recordID = :rangeID";
							$strType = "update";
							$arrdbparams = array(
											"brandID" => $brandData['brandID'],
											"order" => $brandData['order'],
											"rangeID" => $brandData['rangeID']
										);
										
							//echo "addition\n";
							$addOrder = query($conn, $addOrderQuery, $strType, $arrdbparams);
						}
					}
					//echo $currOrder['menuOrder']." ".$brandData['order']."\n";
					//echo $maxOrder['max_order']." ".$brandData['order']."\n";
					//echo "Order is: ".$brandData['order']."\n";
					
					break;
					
				case "delete":
					
					//var_dump($brandData);
					$removeOrderQuery = "UPDATE stock_ranges SET brandID = NULL, menuOrder = NULL WHERE recordID = :rangeID AND brandID = :brandID AND menuOrder = :order";
					$strType = "update";
					$arrdbparams = array(
									"rangeID" => $brandData['rangeID'],
									"brandID" => $brandData['brandID'],
									"order" => $brandData['order']
								);
					//echo "deletion\n";
					$removeOrder = query($conn, $removeOrderQuery, $strType, $arrdbparams);
					
					//echo $deleteOrderQuery;
					//var_dump($deleteOrder);
					
					$i++;
					
					if ($i == $intBrands)
					{
						
						$strdbsql = "SELECT recordID FROM stock_ranges WHERE brandID = :brandID ORDER BY menuOrder";
						$strType = "multi";
						$arrdbparams = array(
										"brandID" => $brandData["brandID"]
									);
						//echo "get products\n";
						$associatedRanges = query($conn, $strdbsql, $strType, $arrdbparams);
						//var_dump($categories);
						
						$y = 1;
						foreach($associatedRanges AS $associatedRange)
						{
							$strdbsql = "UPDATE stock_ranges SET menuOrder = ".$y." WHERE recordID = :recordID";
							$strType = "update";
							$arrdbparam = array(
											"recordID" => $associatedRange['recordID']
										);
							//echo "update final ordering\n";
							query($conn, $strdbsql, $strType, $arrdbparam);
							$y++;
						}
					}
					
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
