<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_brandproductmanagement"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_POST);
	foreach($_POST AS $brandsData)
	{
		//var_dump($brandsData);
		$i = 0;
		$intBrands = count($brandsData);
		foreach($brandsData AS $brandData)
		{
			$strcmd = $brandData['cmd'];
			//var_dump($_POST);
			switch ($strcmd)
			{
				case "add":
				case "reorder":
					
					//var_dump($_POST);
					$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM stock_group_information WHERE brandID = :brandID";
					$strType = "single";
					$arrdbparam = array( "brandID" => $brandData['brandID'] );
					$maxOrder = query($conn, $maxOrderQuery, $strType, $arrdbparam);
					
					if ($strcmd == "reorder")
					{
						$currOrderQuery = "SELECT brandOrder FROM stock_group_information WHERE recordID = :productID AND brandID = :brandID";
						$strType = "single";
						$arrdbparam = array( "productID" => $brandData['productID'], "brandID" => $brandData['brandID'] );
						$currOrder = query($conn, $currOrderQuery, $strType, $arrdbparam);
						
						if ($currOrder['brandOrder'] != $brandData['order'])
						{
							if (($currOrder['brandOrder'] < $brandData['order']))
							{
								//echo "move down\n";
								$updateOrdersQuery = "UPDATE stock_group_information SET brandOrder = brandOrder - 1 WHERE brandID = :brandID AND brandOrder <= :newOrder AND brandOrder >= :prevOrder";
								$strType = "update";
								$arrdbparam = array(
												"brandID" => $brandData['brandID'],
												"prevOrder" => $currOrder['brandOrder'],
												"newOrder" => $brandData['order']
											);
								
								if ($brandData['order'] <= $maxOrder['max_order'])
								{
									//echo "move down before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							elseif ($currOrder['brandOrder'] > $brandData['order'])
							{
								
								//echo "move up\n";
								$updateOrdersQuery = "UPDATE stock_group_information SET brandOrder = brandOrder + 1 WHERE brandID = :brandID AND brandOrder <= :prevOrder AND brandOrder >= :newOrder";
								$strType = "update";
								$arrdbparam = array(
												"brandID" => $brandData['brandID'],
												"prevOrder" => $currOrder['brandOrder'],
												"newOrder" => $brandData['order']
											);
								if ($brandData['order'] > 0)
								{
									//echo "move up before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							
							if ($brandData['order'] > 0 && $brandData['order'] <= $maxOrder['max_order'])
							{
								//echo "move level\n";
								$updateOrderQuery = "UPDATE stock_group_information SET brandOrder = :order WHERE recordID = :productID AND brandID = :brandID";
								$strType = "update";
								$arrdbparams = array(
												"order" => $brandData['order'],
												"productID" => $brandData['productID'],
												"brandID" => $brandData['brandID']
											);
								
								//echo "reorder\n";
								$updateOrder = query($conn, $updateOrderQuery, $strType, $arrdbparams);
							}
						}
					}
					elseif ($strcmd == "add")
					{
						$currOrder['brandOrder'] = $brandData['order'];
						
						if ($brandData['order'] > 0)
						{
							if ($brandData['order'] <= $maxOrder['max_order'])
							{
								//echo "add\n";
								$updateOrdersQuery = "UPDATE stock_group_information SET brandOrder = brandOrder + 1 WHERE brandID = :brandID AND brandOrder >= :order";
								$strType = "update";
								$arrdbparam = array(
												"brandID" => $brandData['brandID'],
												"order" => $brandData['order']
											);
								
								//echo "move down before addition\n";
								$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
							}
							//echo "add to level\n";
							$addOrderQuery = "UPDATE stock_group_information SET brandID = :brandID, brandOrder = :order WHERE recordID = :productID";
							$strType = "update";
							$arrdbparams = array(
											"brandID" => $brandData['brandID'],
											"order" => $brandData['order'],
											"productID" => $brandData['productID']
										);
										
							//echo "addition\n";
							$addOrder = query($conn, $addOrderQuery, $strType, $arrdbparams);
						}
					}
					//echo $currOrder['menuOrder']." ".$brandData['order']."\n";
					//echo $maxOrder['max_order']." ".$brandData['order']."\n";
					//echo "Order is: ".$brandData['order']."\n";
					
					break;
					
				case "delete":
					
					//var_dump($brandData);
					$removeOrderQuery = "UPDATE stock_group_information SET brandID = NULL, brandOrder = NULL WHERE recordID = :productID AND brandID = :brandID AND brandOrder = :order";
					$strType = "update";
					$arrdbparams = array(
									"productID" => $brandData['productID'],
									"brandID" => $brandData['brandID'],
									"order" => $brandData['order']
								);
					//echo "deletion\n";
					$removeOrder = query($conn, $removeOrderQuery, $strType, $arrdbparams);
					
					//echo $deleteOrderQuery;
					//var_dump($deleteOrder);
					
					$i++;
					
					if ($i == $intBrands)
					{
						
						$strdbsql = "SELECT recordID FROM stock_group_information WHERE brandID = :brandID ORDER BY brandOrder";
						$strType = "multi";
						$arrdbparams = array(
										"brandID" => $brandData["brandID"]
									);
						//echo "get products\n";
						$associatedProducts = query($conn, $strdbsql, $strType, $arrdbparams);
						//var_dump($categories);
						
						$y = 1;
						foreach($associatedProducts AS $associatedProduct)
						{
							$strdbsql = "UPDATE stock_group_information SET brandOrder = ".$y." WHERE recordID = :recordID";
							$strType = "update";
							$arrdbparam = array(
											"recordID" => $associatedProduct['recordID']
										);
							//echo "update final ordering\n";
							query($conn, $strdbsql, $strType, $arrdbparam);
							$y++;
						}
					}
					
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
