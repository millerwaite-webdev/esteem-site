<?php namespace Login;

use CompanyDetails;
use PDO;

class LoginMethods
{
    public $sessionobject;
    public $conn;
    public $companydetails;
	public $successMessage;
	public $errorMessage;
	public $warningMessage;

    /**
     * @param SessionObject $sessObj
     * @param PDO $conn
     * @param CompanyDetails $companyDetails
     */
    function __construct(SessionObject $sessObj, PDO $conn, CompanyDetails $companyDetails) {
        $this->sessionobject = $sessObj;
        $this->conn = $conn;
        $this->companydetails = $companyDetails;
    }

    public function forgotUsername()
    {
        //check if username and password combo are valid
        $strdbsql = "SELECT * FROM admin_operator WHERE email=:email AND password=:password";
        $arrType = "single";
        $strdbparams = array("email" => $this->sessionobject->stremail, "password" => $this->sessionobject->strpassword);
        $resultdata = query($this->conn, $strdbsql, $arrType, $strdbparams);
        if (empty($resultdata)) $localAccount = false; else $localAccount = true;

        //the entered information matches either a local account or a master account
        if ($localAccount) {

            $emailTitle = "$this->companydetails->companyName Username Request";
            $emailMessage = "Your $this->companydetails->companyName username is ".$resultdata['username'].". If you did not make this request please contact $this->companydetails->->companyName on $this->companydetails->companyPhone or email $this->companydetails->companyEmail.";
            $emailHtmlMessage = "Your $this->companydetails->companyName username is ".$resultdata['username']."<br/><br/>If you did not make this request please contact $this->companydetails->companyName on $this->companydetails->companyPhone or email $this->companydetails->companyEmail.";

            $emailresult = sendemail($this->companydetails->companyName, $this->companydetails->companyEmail, $this->companydetails->companyEmail, $resultdata['email'], $emailTitle, $emailMessage, $emailHtmlMessage, 1, 0);
            if (isset ($emailresult["success"])) $this->successMessage = "Your username has been emailed to you.";
            else if (isset ($emailresult["error"])) $this->errorMessage = $emailresult["error"];
            $_REQUEST['view'] = "";

        } else {
            $this->errorMessage = "Email address or password is incorrect";
            unset ($_SESSION['operatorID']);
            $_REQUEST['view'] = "forgotUser";
        }
    }

    public function forgotPassword()
    {
        //check if username and password combo are valid
        $strdbsql = "SELECT * FROM admin_operator WHERE username=:username";
        $arrType = "single";
        $strdbparams = array("username" => $this->sessionobject->strusername);
        $resultdata = query($this->conn, $strdbsql, $arrType, $strdbparams);
        $localAccount = false;
        //check the memorable information is correct (LOCAL)
        if (!empty($resultdata)) {
            $memorableInfo = strtoupper($resultdata['memorableInformation']);

            if ($this->sessionobject->memorable1 == $memorableInfo[$this->sessionobject->memorablePos1-1]
                AND $this->sessionobject->memorable2 == $memorableInfo[$this->sessionobject->memorablePos2-1]
                AND $this->sessionobject->memorable3 == $memorableInfo[$this->sessionobject->memorablePos3-1]) {
                $localAccount = true;
            }
        }

        //the entered information matches either a local account or a master account
        if ($localAccount) {

            //write code to reset password d41d8cd98f00b204e9800998ecf8427e
            $strdbsql = "UPDATE admin_operator SET password = :password WHERE username = :username";
            $arrType = "update";
            $strdbparams = array("username" => $this->sessionobject->strusername, "password" => "d41d8cd98f00b204e9800998ecf8427e");
            $resultupdate = query($this->conn, $strdbsql, $arrType, $strdbparams);

            if ($resultupdate != null)
            {
                $emailTitle = "$this->companydetails->companyName Password Reset";
                $emailMessage = "Your password has been reset, if you did not request a password reset please contact $this->companydetails->companyName on $this->companydetails->companyPhone or email $this->companydetails->companyEmail. To login to the system please leave your password blank, you will then be prompted to change your password.";
                $emailHtmlMessage = "Your password has been reset, if you did not request a password reset please contact $this->companydetails->companyName on $this->companydetails->companyPhone or email $this->companydetails->companyEmail. <br/><br/>To login to the system please leave your password blank, you will then be prompted to change your password.";
                $emailresult = sendemail($this->companydetails->companyName, $this->companydetails->companyEmail, $this->companydetails->companyEmail, $resultdata['email'], $emailTitle, $emailMessage, $emailHtmlMessage, 1, 0);
                $this->successMessage = ("Your password has been reset, please leave the password blank when logging in.");
                $_REQUEST['view'] = "";

            } else {
                $this->errorMessage = "Failed to update password. Please try again and if this error re-occurs please contact $this->sessionobject->supportemail";
                $_REQUEST['view'] = "forgotPass";
            }

        } else {
            $this->errorMessage = "Username or memorable information is incorrect";
            unset ($_SESSION['operatorID']);
            $_REQUEST['view'] = "forgotPass";
        }
    }

    public function forgotMemorableInfo()
    {
        //check if username and password combo are valid
        $strdbsql = "SELECT * FROM admin_operator WHERE username=:username AND password=:password";
        $arrType = "single";
        $strdbparams = array("username" => $this->sessionobject->strusername, "password" => $this->sessionobject->strpassword);
        $resultdata = query($this->conn, $strdbsql, $arrType, $strdbparams);
        if (empty($resultdata)) $localAccount = false; else $localAccount = true;

        //the entered information matches either a local account or a master account
        if ($localAccount) {

            //write code to update memorable information
            $memorableInformation = fngetRandomStringChar(8);
            $strdbsql = "UPDATE admin_operator SET memorableInformation = :memorableInformation WHERE username = :username";
            $arrType = "update";
            $strdbparams = array("username" => $this->sessionobject->strusername, "memorableInformation" => $memorableInformation);
            $resultupdate = query($this->conn, $strdbsql, $arrType, $strdbparams);

            if ($resultupdate != null)
            {
                $this->successMessage = ("Your memorable information has been reset to: $memorableInformation");
                $_REQUEST['view'] = "";

            } else {
                $this->errorMessage = "Failed to update your memorable information. Please try again and if this error re-occurs please contact $this->sessionobject->supportemail";
                $_REQUEST['view'] = "forgotMemorableInfo";
            }

        } else {
            $this->errorMessage = "Username or password is incorrect";
            unset ($_SESSION['operatorID']);
            $_REQUEST['view'] = "forgotMemorableInfo";
        }
    }

    public function login($datnow, $strcookiedom)
    {
        //log a connection attempt to the master database
        $strdbsql = "INSERT INTO admin_operator_connections (username,ipaddress,timestamp) VALUES (:username,:ipaddress,:timestamp)";
        $arrType = "insert";
        $strdbparams = array("username" => $this->sessionobject->strusername, "ipaddress" => $this->sessionobject->stripaddress, "timestamp" => $datnow);
        $connectionAttemptID = query($this->conn, $strdbsql, $arrType, $strdbparams);

        //check if username and password combo are valid
        $strdbsql = "SELECT * FROM admin_operator WHERE username=:username AND password=:password";
        $arrType = "single";
        $strdbparams = array("username" => $this->sessionobject->strusername, "password" => $this->sessionobject->strpassword);
        $resultdata = query($this->conn, $strdbsql, $arrType, $strdbparams);
        $localAccount = false;
		
        //check the memorable information is correct
        if (!empty($resultdata)) {
            $memorableInfo = strtoupper($resultdata['memorableInformation']);
            if ($this->sessionobject->memorable1 == $memorableInfo[$this->sessionobject->memorablePos1-1] AND $this->sessionobject->memorable2 == $memorableInfo[$this->sessionobject->memorablePos2-1] AND $this->sessionobject->memorable3 == $memorableInfo[$this->sessionobject->memorablePos3-1]) {
                $localAccount = true;
            }
        }

        //the entered information matches a local account
        if ($localAccount) {

            $strdbsql = "SELECT * FROM admin_operator WHERE username=:username";
            $arrType = "single";
            $strdbparams = array("username" => $this->sessionobject->strusername);
            $resultdata = query($this->conn, $strdbsql, $arrType, $strdbparams);

            //update the connection log to say it has been accepted and to set the operatorID
            $strdbsql = "UPDATE admin_operator_connections SET accepted = 1, operatorID = :operatorID WHERE recordID = :connectionAttemptID";
            $arrType = "update";
            $strdbparams = array("connectionAttemptID" => $connectionAttemptID, "operatorID" => $resultdata['operatorID']);
            $resultupdate = query($this->conn, $strdbsql, $arrType, $strdbparams);

            if ($resultdata['enabled'] == 1) {

                setcookie ("ADMINConnection", $connectionAttemptID, $datnow + 60*60*10, "/", $strcookiedom);
                $_SESSION['operatorID'] = $resultdata['operatorID'];
                $_SESSION['username'] = $resultdata['username'];
                $_SESSION['permissionGroup'] = $resultdata['permissionGroup'];
                $_SESSION['enabled'] = $resultdata['enabled'];

                if ($this->sessionobject->strpassword == "d41d8cd98f00b204e9800998ecf8427e" ) {
                    $_SESSION['blankpwd'] = 1;
                    header ("Location: /admin/operator.php");
                } else {
                    unset($_SESSION['blankpwd']);
                    header ("Location: /admin/index.php");
                }
            } else {
                $this->errorMessage = "Your account has been disabled";
                unset ($_SESSION['operatorID']);
            }

        } else {
            $this->errorMessage = "Username, password or memorable information is incorrect";
            unset ($_SESSION['operatorID']);
        }
    }
}

class SessionObject
{
    public $stripaddress;
    public $strusername;
    public $stremail;
    public $strpassword;
    public $memorable1;
    public $memorable2;
    public $memorable3;
    public $memorablePos1;
    public $memorablePos2;
    public $memorablePos3;
}