<?php
//	' ********************************************************************** '
//	' * by Miller Waite                                                    * '
//	' * Email address: webteam@millerwaite.com                             * '
//	' *                                                                    * '
//	' * inc_footer.php                                                     * '
//	' ********************************************************************** '
//  ' $strthisversion = "V0.0a  - Jan 2014";                               * '
//	' ********************************************************************** '

print("<div id='sidebar' class='maximise'>");
	
	print("<div class='logo'>");
		print("<img src='/admin/images/logo.png'/>");
	print("</div>");
	
	print("<div id='menuWrapper'>");
		print("<ul id='menu'>");
		
			//get information about current page
			$strdbsql = "SELECT * FROM admin_pages WHERE link=:pagename";
			$arrType = "single";
			$strdbparams = array("pagename" => $strpage);
			$pageInformation = query($conn, $strdbsql, $arrType, $strdbparams); 
			//get all of the menu options that are not hidden and are a parent (top level)
			$strdbsql = "SELECT * FROM admin_pages WHERE parentElement=0 AND mainMenu = 1 AND visible = 1 ORDER BY displayOrder";
			$arrType = "multi";
			$resultdata = query($conn, $strdbsql, $arrType);
			$i = 1;

			//loop through each top level menu
			foreach ($resultdata as $row) {
			
				$pageID = $row['recordID'];
				$name = $row['name'];
				$icon = $row['icon'];
				if ($row['link'] != '') $link = "".$row['link'].".php"; else $link = "";
	
				//get all sub menu items for this parent -  exactly the same as the loop above except limited to just sub items
				$strdbsql = "SELECT * FROM admin_pages WHERE parentElement='$pageID' AND mainMenu = 1 AND visible = 1 ORDER BY displayOrder";
				$arrType = "multi";
				$resultdata2 = query($conn, $strdbsql, $arrType);
				$boolnestedelements = !empty($resultdata2);

				$clicked=""; $current = ""; $content = "";
				if ($pageInformation['groupParentElement'] == $pageID || $pageInformation['recordID'] == $pageID) {
					$clicked = "clicked";
					$current = "current";
					$content = "display: block;";
				}
				
				if($boolnestedelements) {
					print ("<li class='item$i $current'><a href='$link' class='expand-collapse $clicked'>$icon$name</a>");
				} else {
					print ("<li class='item$i $current'><a href='$link' class='$clicked'>$icon$name</a>");
				}
													

				if ($boolnestedelements) {
					print("<div class='content' style='$content'>");
						print ("<ul>");
						$x = 1;
						//loop through all the options found
						foreach ($resultdata2 as $row2) {
							
							$subpageID = $row2['recordID'];
							$subdescription = $row2['name'];
							if ($row2['link'] != '') $sublink = "".$row2['link'].".php"; else $sublink = "";							
							
							$active = "";
							//print out the menu option
							print ("<li class='subitem$x'><a href='$sublink' >$subdescription</a>");
							
							//get all sub menu items for this parent -  exactly the same as the loop above except limited to just sub items
							$strdbsql = "SELECT * FROM admin_pages WHERE parentElement='$subpageID' AND mainMenu = 1 AND visible = 1 ORDER BY displayOrder";
							$arrType = "multi";
							$resultdata3 = query($conn, $strdbsql, $arrType);
							if (!empty($resultdata3)) {
								print ("<ul class='flyout'>");
								$y = 1;
								//loop through all the options found
								foreach ($resultdata3 as $row3) {
									
									$nestedsubdescription = $row3['name'];
									if ($row3['link'] != '') $nestedsublink = "".$row3['link'].".php"; else $nestedsublink = "";
									//print out the menu option
									print ("<li class='subitem$y'><a href='$nestedsublink' >$nestedsubdescription</a></li>");
									$y++;
								}
								print ("</ul>");
							}
							print("</li>");
							$x++;
						}
						print ("</ul>");
					print ("</div>");
				}
				
				print ("</li>");
				$i++;
			}
			$resultdata = null;
			
		print("</ul>");
	print("</div>");
	
	$strdbsql = "SELECT * FROM site_company_details";
	$strType = "single";
	$company = query($conn, $strdbsql, $strType);
	
	print("<div class='adminInfo'>");
		print("<p>Powered by <a href='http://www.millerwaite.com'>Miller Waite</a><br/>Licensed to ".$company['companyName']." (Software Version: $strsoftwareversion)</p>");
	print("</div>");
	
print("</div>");