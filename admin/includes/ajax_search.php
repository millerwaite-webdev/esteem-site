<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_search"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to lottery Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//

	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	
	if (isset($_REQUEST['frm_fromjournaltype'])) $fromjournaltype = $_REQUEST['frm_fromjournaltype']; else $fromjournaltype = "";
	if (isset($_REQUEST['frm_tojournaltype'])) $tojournaltype = $_REQUEST['frm_tojournaltype']; else $tojournaltype = "";
	
	if (isset($_REQUEST['frm_accountto'])) $accountto = $_REQUEST['frm_accountto']; else $accountto = "";
	if (isset($_REQUEST['frm_accountfrom'])) $accountfrom = $_REQUEST['frm_accountfrom']; else $accountfrom = "";

	if(isset($_REQUEST['q']) AND $_REQUEST['q'] == 'search')
	{
	
		if(isset($_REQUEST['searchtype']))
		{
			switch ($_REQUEST['searchtype']) {
				
				case "number":
				
					if (isset($_REQUEST['number'])) {
						
						$strdbsql = "SELECT accountID FROM number WHERE number = :number";
						$strType = "multi";
						$arrdbparams = array("number" => $_REQUEST['number']);
						$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
						
						if (count($resultdata) == 0) {
							return false;
						} else if (count($resultdata) > 1) {
							return false;
						} else {
							
							$strdbsql = "SELECT account.balance, member.membershipID, member.salutation, member.firstname, member.surname FROM account INNER JOIN member ON account.membershipID = member.membershipID WHERE account.accountID = :accountID";
							$strType = "single";
							$arrdbparams = array("accountID" => $resultdata[0]['accountID']);
							$resultdata = query($conn, $strdbsql, $strType, $arrdbparams);
						
							print ("<input name='frm_accounttoval' id='frm_accounttoval' value='".$resultdata['membershipID']." - ".$resultdata['salutation']." ".$resultdata['firstname']." ".$resultdata['surname'].". Account Balance: &pound;".number_format($resultdata['balance'],2)."' type='text' class='form-control' disabled />");
						}
					
					} else {
						return false;
					}
				
				break;
			}
		}
	}
	$resultdata = null;

	// ************* Common page setup ******************** //
	//=====================================================//
	$conn = null; // close the lottery Database connection after all processing
	$connmaster = null; // close the master Database connection after all processing
?>

