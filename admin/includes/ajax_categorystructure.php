<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_categorystructure"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_POST);
	foreach($_POST AS $categoriesData)
	{
		//var_dump($categoriesData);
		$i = 0;
		$intCategories = count($categoriesData);
		foreach($categoriesData AS $categoryData)
		{
			//var_dump($categoryData);
			//echo "Order is: ".$categoryData['order']." Child ID is: ".$categoryData['childID']." Parent ID is: ".$categoryData['parentID']." Level is: ".$categoryData['level']." Command is: ".$categoryData['cmd']."\n";
			$strcmd = $categoryData['cmd'];
				
			if (!is_numeric($categoryData['parentID']))
			{
				$categoryData['parentID'] = $categoryData['childID'];
			}
			
			switch ($strcmd)
			{
				case "add":
				case "reorder":
					
					if ($categoryData['level'] == 1)
					{
						$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM category_relations WHERE parentID = childID AND level = :level";
						$strType = "single";
						$arrdbparam = array( "level" => $categoryData['level'] );
						$maxOrder = query($conn, $maxOrderQuery, $strType, $arrdbparam);	
					}
					elseif ($categoryData['level'] > 1)
					{
						$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM category_relations WHERE parentID = :parentID AND level = :level";
						$strType = "single";
						$arrdbparam = array( "parentID" => $categoryData['parentID'], "level" => $categoryData['level'] );
						$maxOrder = query($conn, $maxOrderQuery, $strType, $arrdbparam);
					}
					//echo $currOrder['menuOrder']." ".$categoryData['order']."\n";
					//echo $maxOrder['max_order']." ".$categoryData['order']."\n";
					
					if ($strcmd == "reorder")
					{
						if ($categoryData['order'] > 0 && $categoryData['order'] <= $maxOrder['max_order'])
						{
							$currOrderQuery = "SELECT menuOrder FROM category_relations WHERE parentID = :parentID AND childID = :childID AND level = :level";
							$strType = "single";
							$arrdbparam = array( "parentID" => $categoryData['parentID'], "childID" => $categoryData['childID'], "level" => $categoryData['level'] );
							$currOrder = query($conn, $currOrderQuery, $strType, $arrdbparam);
							
							if ($currOrder['menuOrder'] != $categoryData['order'])
							{
								if ($currOrder['menuOrder'] < $categoryData['order'])
								{
									if ($categoryData['level'] == 1)
									{
										//echo "move top level down\n";
										$updateOrdersQuery = "UPDATE category_relations SET menuOrder = menuOrder - 1 WHERE parentID = childID AND level = :level AND menuOrder <= :newOrder AND menuOrder >= :prevOrder";
										$strType = "update";
										$arrdbparam = array(
														"level" => $categoryData['level'],
														"prevOrder" => $currOrder['menuOrder'],
														"newOrder" => $categoryData['order']
													);
									}
									elseif ($categoryData['level'] > 1)
									{
										//echo "move sublevel down\n";
										$updateOrdersQuery = "UPDATE category_relations SET menuOrder = menuOrder - 1 WHERE parentID = :parentID AND level = :level AND menuOrder <= :newOrder AND menuOrder >= :prevOrder";
										$strType = "update";
										$arrdbparam = array(
														"parentID" => $categoryData['parentID'],
														"level" => $categoryData['level'],
														"prevOrder" => $currOrder['menuOrder'],
														"newOrder" => $categoryData['order']
													);
									}
									if ($categoryData['order'] <= $maxOrder['max_order'])
									{
										//echo "move level down\n";
										$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
									}
								}
								elseif ($currOrder['menuOrder'] > $categoryData['order'])
								{
									if ($categoryData['level'] == 1)
									{
										//echo "move top level up\n";
										$updateOrdersQuery = "UPDATE category_relations SET menuOrder = menuOrder + 1 WHERE parentID = parentID AND level = :level AND menuOrder <= :prevOrder AND menuOrder >= :newOrder";
										$strType = "update";
										$arrdbparam = array(
														"level" => $categoryData['level'],
														"prevOrder" => $currOrder['menuOrder'],
														"newOrder" => $categoryData['order']
													);
									}
									elseif ($categoryData['level'] > 1)
									{
										//echo "move sub level up\n";
										$updateOrdersQuery = "UPDATE category_relations SET menuOrder = menuOrder + 1 WHERE parentID = :parentID AND level = :level AND menuOrder <= :prevOrder AND menuOrder >= :newOrder";
										$strType = "update";
										$arrdbparam = array(
														"parentID" => $categoryData['parentID'],
														"level" => $categoryData['level'],
														"prevOrder" => $currOrder['menuOrder'],
														"newOrder" => $categoryData['order']
													);
									}
									if ($categoryData['order'] > 0)
									{
										//echo "move level up\n";
										$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
									}
								}
								//echo "move order\n";
								$updateOrderQuery = "UPDATE category_relations SET menuOrder = :order WHERE parentID = :parentID AND childID = :childID AND level = :level";
								$strType = "update";
								$arrdbparams = array(
												"order" => $categoryData['order'],
												"parentID" => $categoryData['parentID'],
												"childID" => $categoryData['childID'],
												"level" => $categoryData['level']
											);
								$updateOrder = query($conn, $updateOrderQuery, $strType, $arrdbparams);
							}
						}
					}
					elseif ($strcmd == "add")
					{
						if ($categoryData['order'] > 0)
						{
							if ($categoryData['order'] <= $maxOrder['max_order'])
							{
								if ($categoryData['level'] == 1)
								{
									//echo "add to top level\n";
									$updateOrdersQuery = "UPDATE category_relations SET menuOrder = menuOrder + 1 WHERE parentID = childID AND level = :level AND menuOrder >= :newOrder";
									$strType = "update";
									$arrdbparam = array(
													"level" => $categoryData['level'],
													"newOrder" => $categoryData['order']
												);
								}
								elseif ($categoryData['level'] > 1)
								{
									//echo "add to sub level\n";
									$updateOrdersQuery = "UPDATE category_relations SET menuOrder = menuOrder + 1 WHERE parentID = :parentID AND childID = :childID AND level = :level AND menuOrder >= :newOrder";
									$strType = "update";
									$arrdbparam = array(
													"parentID" => $categoryData['parentID'],
													"childID" => $categoryData['childID'],
													"level" => $categoryData['level'],
													"newOrder" => $categoryData['order']
												);
								}
								$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
							}
							//echo "add to level\n";
							$insertOrderQuery = "INSERT INTO category_relations (parentID, childID, menuOrder, level) VALUES (:parentID, :childID, :order, :level)";
							$strType = "insert";
							$arrdbparams = array(
											"parentID" => $categoryData['parentID'],
											"childID" => $categoryData['childID'],
											"order" => $categoryData['order'],
											"level" => $categoryData['level']
										);
							$insertOrder = query($conn, $insertOrderQuery, $strType, $arrdbparams);
						}
					}
						
					//echo "<br/>Order is: ".$categoryData['order']."\n";
					
					break;
				
				case "delete":
					
					//echo "delete\n";
					//var_dump($categoryData);
					
					$deleteOrderQuery = "DELETE FROM category_relations WHERE parentID = :parentID AND childID = :childID AND level = :level AND menuOrder = :order";
					$strType = "delete";
					$arrdbparams = array(
									"parentID" => $categoryData['parentID'],
									"childID" => $categoryData['childID'],
									"level" => $categoryData['level'],
									"order" => $categoryData['order']
								);
					$deleteOrder = query($conn, $deleteOrderQuery, $strType, $arrdbparams);
					
					$i++;
					
					if ($i == $intCategories)
					{
						if($categoryData['level'] == 1)
						{
							//echo "get parent cats\n";
							$strdbsql = "SELECT recordID FROM category_relations WHERE parentID = childID AND level = :level ORDER BY menuOrder";
							$strType = "multi";
							$arrdbparams = array(
											"level" => $categoryData['level']
										);
						}
						else
						{
							//echo "get subcats\n";
							$strdbsql = "SELECT recordID FROM category_relations WHERE parentID = :parentID AND level = :level ORDER BY menuOrder";
							$strType = "multi";
							$arrdbparams = array(
											"parentID" => $categoryData["parentID"],
											"level" => $categoryData["level"]
										);
						}
						$associatedCategories = query($conn, $strdbsql, $strType, $arrdbparams);
						//var_dump($categories);
						
						$y = 1;
						foreach($associatedCategories AS $associatedCategory)
						{
							$strdbsql = "UPDATE category_relations SET menuOrder = ".$y." WHERE recordID = :recordID";
							$strType = "update";
							$arrdbparam = array(
											"recordID" => $associatedCategory['recordID']
										);
							//echo "update final ordering\n";
							query($conn, $strdbsql, $strType, $arrdbparam);
							$y++;
						}
					}
					
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>