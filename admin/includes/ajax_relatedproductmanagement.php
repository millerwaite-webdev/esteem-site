<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_relatedproductmanagement"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	//var_dump($_POST);
	foreach($_POST AS $productsData)
	{
		//var_dump($productsData);
		$i = 0;
		$intProducts = count($productsData);
		foreach($productsData AS $productData)
		{
			$strcmd = $productData['cmd'];
			//var_dump($_POST);
			switch ($strcmd)
			{
				case "add":
				case "reorder":
					
					//var_dump($_POST);
					$maxOrderQuery = "SELECT COUNT(*) AS max_order FROM stock_related WHERE stockCode1 = :stockCode1";
					$strType = "single";
					$arrdbparam = array( "stockCode1" => $productData['stockCode1'] );
					$maxOrder = query($conn, $maxOrderQuery, $strType, $arrdbparam);
					
					if ($strcmd == "reorder")
					{
						$currOrderQuery = "SELECT relatedOrder FROM stock_related WHERE stockCode1 = :stockCode1 AND stockCode2 = :stockCode2";
						$strType = "single";
						$arrdbparam = array( "stockCode1" => $productData['stockCode1'], "stockCode2" => $productData['stockCode2'] );
						$currOrder = query($conn, $currOrderQuery, $strType, $arrdbparam);
						
						if ($currOrder['relatedOrder'] != $productData['order'])
						{
							if (($currOrder['relatedOrder'] < $productData['order']))
							{
								//echo "move down\n";
								$updateOrdersQuery = "UPDATE stock_related SET relatedOrder = relatedOrder - 1 WHERE stockCode1 = :stockCode1 AND relatedOrder <= :newOrder AND relatedOrder >= :prevOrder";
								$strType = "update";
								$arrdbparam = array(
												"stockCode1" => $productData['stockCode1'],
												"prevOrder" => $currOrder['relatedOrder'],
												"newOrder" => $productData['order']
											);
								
								if ($productData['order'] <= $maxOrder['max_order'])
								{
									//echo "move down before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							elseif ($currOrder['relatedOrder'] > $productData['order'])
							{
								
								//echo "move up\n";
								$updateOrdersQuery = "UPDATE stock_related SET relatedOrder = relatedOrder + 1 WHERE stockCode1 = :stockCode1 AND relatedOrder <= :prevOrder AND relatedOrder >= :newOrder";
								$strType = "update";
								$arrdbparam = array(
												"stockCode1" => $productData['stockCode1'],
												"prevOrder" => $currOrder['relatedOrder'],
												"newOrder" => $productData['order']
											);
								if ($productData['order'] > 0)
								{
									//echo "move up before reorder\n";
									$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
								}
							}
							
							if ($productData['order'] > 0 && $productData['order'] <= $maxOrder['max_order'])
							{
								//echo "move level\n";
								$updateOrderQuery = "UPDATE stock_related SET relatedOrder = :order WHERE stockCode1 = :stockCode1 AND stockCode2 = :stockCode2";
								$strType = "update";
								$arrdbparams = array(
												"order" => $productData['order'],
												"stockCode1" => $productData['stockCode1'],
												"stockCode2" => $productData['stockCode2']
											);
								
								//echo "reorder\n";
								$updateOrder = query($conn, $updateOrderQuery, $strType, $arrdbparams);
							}
						}
					}
					elseif ($strcmd == "add")
					{
						$currOrder['relatedOrder'] = $productData['order'];
						
						if ($productData['order'] > 0)
						{
							if ($productData['order'] <= $maxOrder['max_order'])
							{
								//echo "add\n";
								$updateOrdersQuery = "UPDATE stock_related SET relatedOrder = relatedOrder + 1 WHERE stockCode1 = :stockCode1 AND relatedOrder >= :newOrder";
								$strType = "update";
								$arrdbparam = array(
												"stockCode1" => $productData['stockCode1'],
												"newOrder" => $productData['order']
											);
								
								//echo "move down before addition\n";
								$updateOrders = query($conn, $updateOrdersQuery, $strType, $arrdbparam);
							}
							//echo "add to level\n";
							$insertOrderQuery = "INSERT INTO stock_related (stockCode1, stockCode2, relatedOrder) VALUES (:stockCode1, :stockCode2, :order)";
							$strType = "insert";
							$arrdbparams = array(
											"stockCode1" => $productData['stockCode1'],
											"stockCode2" => $productData['stockCode2'],
											"order" => $productData['order']
										);
										
							//echo "addition\n";
							$insertOrder = query($conn, $insertOrderQuery, $strType, $arrdbparams);
						}
					}
					//echo $currOrder['relatedOrder']." ".$productData['order']."\n";
					//echo $maxOrder['max_order']." ".$productData['order']."\n";
					//echo "Order is: ".$productData['order']."\n";
					
					break;
					
				case "delete":
					
					//var_dump($productData);
					$deleteRelationQuery = "DELETE FROM stock_related WHERE stockCode1 = :stockCode1 AND stockCode2 = :stockCode2 AND relatedOrder = :order";
					$strType = "delete";
					$arrdbparams = array(
									"stockCode1" => $productData['stockCode1'],
									"stockCode2" => $productData['stockCode2'],
									"order" => $productData['order']
								);
					//echo "deletion\n";
					$deleteOrder = query($conn, $deleteRelationQuery, $strType, $arrdbparams);
					
					//echo $deleteOrderQuery;
					//var_dump($deleteOrder);
					
					$i++;
					
					if ($i == $intProducts)
					{
						
						$strdbsql = "SELECT recordID FROM stock_related WHERE stockCode1 = :stockCode1 ORDER BY relatedOrder";
						$strType = "multi";
						$arrdbparams = array(
										"stockCode1" => $productData["stockCode1"]
									);
						//echo "get products\n";
						$associatedProducts = query($conn, $strdbsql, $strType, $arrdbparams);
						//var_dump($categories);
						
						$y = 1;
						foreach($associatedProducts AS $associatedProduct)
						{
							$strdbsql = "UPDATE stock_related SET relatedOrder = ".$y." WHERE recordID = :recordID";
							$strType = "update";
							$arrdbparam = array(
											"recordID" => $associatedProduct['recordID']
										);
							//echo "update final ordering\n";
							query($conn, $strdbsql, $strType, $arrdbparam);
							$y++;
						}
					}
					
					break;
			}
		}
	}
	
	$conn = null; // close the Database connection after all processing
?>
