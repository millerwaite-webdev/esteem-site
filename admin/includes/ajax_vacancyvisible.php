<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_vacancyvisible"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	
	$strdbsql = "UPDATE site_staff_vacancies SET fld_visible = :showValue WHERE recordID = :recordID";
	$arrdbparams = array('recordID' => $_REQUEST['vacancyID'], 'showValue' => $_REQUEST['enabled']);
	$strType = "update";
	
	$queryResult = query($conn, $strdbsql, $strType, $arrdbparams);
	
	$conn = null; // close the Database connection after all processing
?>
