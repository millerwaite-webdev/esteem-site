function accordion(section) {
	$(section).css('outline', 'none');
	/* If section already clicked, collapse and remove current class from <li> and clicked from <a> */
	if($(section).parent().hasClass('current') && $(section).hasClass('clicked')) {
		$(section).siblings('div.content').slideUp(function() {
			$(section).parent().removeClass('current');
			$(section).removeClass('clicked');
		});
	} else {
		// As new section clicked, collapse previous section and remove clicked/current classes
		$(section).parent().siblings('.current').children().removeClass('clicked');
		$(section).parent().siblings('.current').removeClass('current').children('div.content').slideUp();
		
		// Add clicked class to clicked button
		$(section).siblings('div.content').parent().addClass('current');
		
		// Assign classes to clicked div and slide down
		$(section).addClass('clicked');
		$(section).siblings('div.content').slideDown(function() {
			$(section).siblings('div.content').css("overflow", "initial");
		});
	}
}