$(document).ready(function() {
	$('ul#menu a.expand-collapse').click(function(event) {
		accordion(this);
		event.preventDefault();
	});
});
//following functions are used to display a popup lightbox window. - uses jquery
function loadPopup(){
	$("#popupBackground").css({ "opacity": "0.7" });
	$("#popupBackground").fadeIn("slow");
	$("#popup").fadeIn("slow");
}
function disablePopup(){
	$("#popupBackground").fadeOut("slow", function() { $(this).remove(); });
	$("#popup").fadeOut("slow", function() { $(this).remove(); });
}
function centerPopup(){
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#popup").height();
	var popupWidth = $("#popup").width();
	if(windowHeight > popupHeight) {
		$("#popup").css({ "position": "absolute", "top": windowHeight/2-popupHeight/2, "left": windowWidth/2-popupWidth/2 });
	} else {
		$("#popup").css({ "position": "absolute", "top": 20, "left": windowWidth/2-popupWidth/2 });
	}
	$("#popupBackground").css({ "height": windowHeight });
}
function popupRequest(page, strq) {
	var request = "/includes/"+page+".php"+strq;
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("GET",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var response = xmlhttp.responseText;
			$(document.body).append(response);
			$("#popupClose").click(function(){disablePopup();});
			$("#popupBackground").click(function(){disablePopup();});
			$(document).keypress(function(e){ if(e.keyCode==27 && cookieStatus<2){ disablePopup(); } });
			centerPopup();
			loadPopup();
		}
	}
	xmlhttp.send();
}

//Ajax call so the page does not need to be refreshed. This is a generic call, it takes the pagename, a query string, destination to put the code that is returned, wait time (1 for 1 second etc) and a true or false if need to wait for the response e.g. if it is processing an SQL statement.
function jsajaxcall(ajaxpage, querystring, destination, wait, torf)
{
	querystring += "&ie="+new Date();

	if (destination != "")
	{
		if (wait == 1) { document.getElementById(destination).innerHTML = "Please wait....."; }
	}

	var request = ajaxpage+".php"+querystring;

	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)
	{
		return "Your browser doesn't seem to support XMLHttpRequests.";
	}
	else
	{
		if (torf == 1)
		{
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					if (destination != "")
					{
						document.getElementById(destination).innerHTML = xmlhttp.responseText;
					}
				}
			}
			xmlhttp.open("GET",request,true);
			xmlhttp.send();
		}
		else
		{
			xmlhttp.open("GET",request,false);
			xmlhttp.send();
			if (destination != "")
			{
				document.getElementById(destination).innerHTML = xmlhttp.responseText;
				document.getElementById(destination).value = xmlhttp.responseText;
			}
		}
	}
}

//used to validate
function jsajaxcallValidate(ajaxpage, querystring, destination, wait, torf)
{
	querystring += "&ie="+new Date();

	if (destination != "")
	{
		if (wait == 1) { document.getElementById(destination).innerHTML = "Please wait..."; }
	}

	var request = ajaxpage+".php"+querystring;

	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)
	{
		return "Your browser doesn't seem to support XMLHttpRequests.";
	}
	else
	{
		if (torf == 1)
		{
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					if (destination != "")
					{
						document.getElementById(destination).innerHTML = xmlhttp.responseText;
					}
				}
			}
			xmlhttp.open("GET",request,true);
			xmlhttp.send();
		}
		else
		{
			xmlhttp.open("GET",request,false);
			xmlhttp.send();
			if (destination != "")
			{
				document.getElementById(destination).innerHTML = xmlhttp.responseText;
				document.getElementById(destination).value = xmlhttp.responseText;
			}
		}
		return xmlhttp.responseText;
	}
}


//code to lookup a draw number
function jsfindnumber () {

	strq = "?q=search&searchtype=number";
	strq += "&frm_number="+document.getElementById("frm_number").value;
	
	searchswitch = document.getElementById("searchtype2").value
	switch (searchswitch) {
		case 'full':
			strq += "&frm_beginning=";
			strq += "&frm_end=";
		break;
		case 'partial':
			strq += "&frm_beginning=%";
			strq += "&frm_end=%";
		break;
		case 'begins':
			strq += "&frm_beginning=";
			strq += "&frm_end=%";
		break;
		case 'ends':
			strq += "&frm_beginning=%";
			strq += "&frm_end=";
		break;
	}
	
	jsajaxcall("/includes/ajax_player_lookup",strq,"ajax_existing_players",0,0);
		
	$('#list').dataTable( {
		"bJQueryUI": true,
	} );
}

//code to lookup a bqr
function jsfindbqr () {

	strq = "?q=search&searchtype=bqr";
	strq += "&frm_bqr="+document.getElementById("frm_bqr").value;
	strq += "&frm_accountid="+document.getElementById("frm_accountid").value;
	
	searchswitch = document.getElementById("searchtype3").value
	switch (searchswitch) {
		case 'full':
			strq += "&frm_beginning=";
			strq += "&frm_end=";
		break;
		case 'partial':
			strq += "&frm_beginning=%";
			strq += "&frm_end=%";
		break;
		case 'begins':
			strq += "&frm_beginning=";
			strq += "&frm_end=%";
		break;
		case 'ends':
			strq += "&frm_beginning=%";
			strq += "&frm_end=";
		break;
	}
	
	jsajaxcall("/includes/ajax_player_lookup",strq,"ajax_existing_players",0,0);
		
	$('#list').dataTable( {
		"bJQueryUI": true,
	} );
}

//code to lookup and search for a player - uses ajax.
function jsfinduser (usepostcode) {
	strq = "?q=search&frm_firstname="+document.getElementById("frm_firstname").value;
	strq += "&frm_sterlingid="+document.getElementById("frm_sterlingid").value;
	strq += "&frm_surname="+document.getElementById("frm_surname").value;
	strq += "&frm_postcode="+document.getElementById("frm_postcode").value;
	strq += "&frm_email="+document.getElementById("frm_email").value;
	strq += "&frm_pathwayid="+document.getElementById("frm_pathwayid").value;
	
	
	searchswitch = document.getElementById("searchtype1").value
	switch (searchswitch) {
		case 'full':
			strq += "&frm_beginning=";
			strq += "&frm_end=";
		break;
		case 'partial':
			strq += "&frm_beginning=%";
			strq += "&frm_end=%";
		break;
		case 'begins':
			strq += "&frm_beginning=";
			strq += "&frm_end=%";
		break;
		case 'ends':
			strq += "&frm_beginning=%";
			strq += "&frm_end=";
		break;
	}

	jsajaxcall("/includes/ajax_player_lookup",strq,"ajax_existing_players",0,0);
	//document.getElementById("ajax_existing_players").className = "notification-info";
	
	$('#list').dataTable( {
		"bJQueryUI": true
	} );
	document.getElementById("ajax_address").innerHTML = "";
	document.getElementById("ajax_address").className = "";
	if (document.getElementById('frm_postcode').value != 'Postcode' && document.getElementById('frm_postcode').value != '' && usepostcode == '1') {
		jsfindaddress ();
	}
	
}

//code to lookup and search for a players address - uses ajax.
function jsfindaddress ()
{

	//update the modules table to increment the number of uses
	strq = "?tab=tbl_config_modules&fld=fld_uses&val=1&wfld=fld_refcode&wval=PAF";
	jsajaxcall("/includes/ajax_moduleincrement",strq,"",0,0);

	document.getElementById("ajax_address").style.display = "block";
	var postcode = document.getElementById('frm_postcode').value;
	var request = "/includes/ajax_address_lookup.php";
	var vars = "postcode="+postcode;
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("POST",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var xml = xmlhttp.responseXML;
			if(xml.getElementsByTagName("ErrorNumber")[0].childNodes[0].nodeValue == 0) {
				if(xml.getElementsByTagName("Address2").length != 0) {
					document.getElementById('frm_add2').value = xml.getElementsByTagName("Address2")[0].childNodes[0].nodeValue;
					if(xml.getElementsByTagName("Address3").length != 0)document.getElementById('frm_add2').value += xml.getElementsByTagName("Address3")[0].childNodes[0].nodeValue;
				}
				document.getElementById('frm_add3').value= xml.getElementsByTagName("Town")[0].childNodes[0].nodeValue;
				document.getElementById('frm_county').value= xml.getElementsByTagName("County")[0].childNodes[0].nodeValue;
				var resultlist = xml.getElementsByTagName("PremiseData")[0].childNodes[0].nodeValue;
				var arrlist = resultlist.split(";");
				var addlist = "<h3>Post Code lookup - Click an address to add</h3><ul >";
				for (var i = 0; i < arrlist.length; i++) {
					if(arrlist[i] != "") {
						arrlist[i] = arrlist[i].replace("||","|");
						arrlist[i] = arrlist[i].replace("|",", ");
						arrlist[i] = arrlist[i].replace("|",", ");
						var straddress =arrlist[i]+" "+xml.getElementsByTagName("Address1")[0].childNodes[0].nodeValue;
						straddress = straddress.replace(/(^,)|(,$)/g, "");
						straddress = straddress.replace(/(^ )|(,$)/g, "");
						addlist += "<li><a href='javascript:jsaddsel(\""+straddress+"\")'>"+straddress+"</a></li>";
					}
				}
				addlist += "</ul>";
				document.getElementById("ajax_address").innerHTML = addlist;
				//document.getElementById("ajax_address").className = "notification-info";

			} else {
				document.getElementById("ajax_address").innerHTML = xml.getElementsByTagName("ErrorMessage")[0].childNodes[0].nodeValue;
				document.getElementById("ajax_address").className = "notification-warning";
			}
		} else if (xmlhttp.readyState==4) {
			document.getElementById("ajax_address").innerHTML = "There has been an error finding your address please try again.";
			document.getElementById("ajax_address").className = "notification-error";
		}
	}
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(vars);
	document.getElementById("ajax_address").innerHTML = " <img src='/images/ajax-loader.gif' /> <span> Search...</span> ";
}

//submits form when a user clicks on an address that is found from the postcode lookup
function jsaddsel(address)
{
	document.getElementById('frm_add1').value = address;
	document.getElementById("ajax_address").style.display = "none";
	document.checkadduser.submit();
}

//code to lookup and search for a players address - uses ajax // different to the original as the list is not actually show
function jsfindaddressnolist ()
{

	//update the modules table to increment the number of uses
	strq = "?tab=tbl_config_modules&fld=fld_uses&val=1&wfld=fld_refcode&wval=PAF";
	jsajaxcall("/includes/ajax_moduleincrement",strq,"",0,0);

	document.getElementById("ajax_address").style.display = "block";
	var postcode = document.getElementById('frm_postcode').value;
	var request = "/includes/ajax_address_lookup.php";
	var vars = "postcode="+postcode;
	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	if(!xmlhttp)return alert("Your browser doesn't seem to support XMLHttpRequests.");

	xmlhttp.open("POST",request,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var xml = xmlhttp.responseXML;
			if(xml.getElementsByTagName("ErrorNumber")[0].childNodes[0].nodeValue == 0) {
				document.getElementById('frm_add1').value = xml.getElementsByTagName("Address1")[0].childNodes[0].nodeValue;
				if(xml.getElementsByTagName("Address2").length != 0) {
					document.getElementById('frm_add2').value = xml.getElementsByTagName("Address2")[0].childNodes[0].nodeValue;
					if(xml.getElementsByTagName("Address3").length != 0)document.getElementById('frm_add2').value += xml.getElementsByTagName("Address3")[0].childNodes[0].nodeValue;
				}
				document.getElementById('frm_add3').value= xml.getElementsByTagName("Town")[0].childNodes[0].nodeValue;
				document.getElementById('frm_county').value= xml.getElementsByTagName("County")[0].childNodes[0].nodeValue;
				
				$('#frm_add1').focus();
				
			} else {
				document.getElementById("ajax_address").innerHTML = xml.getElementsByTagName("ErrorMessage")[0].childNodes[0].nodeValue;
				document.getElementById("ajax_address").className = "notification-warning";
			}
		} else if (xmlhttp.readyState==4) {
			document.getElementById("ajax_address").innerHTML = "There has been an error finding your address please try again.";
			document.getElementById("ajax_address").className = "notification-error";
		}
	}
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(vars);
}

//Masked the input phone number to display in "123-123-1234" format
function maskInput(input, textbox) {

    //Get the delimiter positons
	var location = '2,5';
    var locs = location.split(',');
	
	delimiter = '-';

    //Iterate until all the delimiters are placed in the textbox
    for (var delimCount = 0; delimCount <= locs.length; delimCount++) {
        for (var inputCharCount = 0; inputCharCount <= input.length; inputCharCount++) {

            //Check for the actual position of the delimiter
            if (inputCharCount == locs[delimCount]) {

                //Confirm that the delimiter is not already present in that position
                if (input.substring(inputCharCount, inputCharCount + 1) != delimiter) {
                    input = input.substring(0, inputCharCount) + delimiter + input.substring(inputCharCount, input.length);
                }
            }
        }
    }
    textbox.value = input;
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})