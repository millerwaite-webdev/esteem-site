<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * cms-static-blocks.php			                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "cms-static-blocks"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['siteBlockTypeID'])) $siteBlockTypeID = $_REQUEST['siteBlockTypeID']; else $siteBlockTypeID = "";
	if (isset($_REQUEST['siteBlockID'])) $siteBlockID = $_REQUEST['siteBlockID']; else $siteBlockID = "";
	if (isset($_REQUEST['galleryID'])) $galleryID = $_REQUEST['galleryID']; else $galleryID = "";
	
	switch($strcmd)
	{
		case "insertBlockType":
		case "updateBlockType":
		
			$arrdbparams = array(
								"description" => $_POST['frm_blocktypedescription']
							);
			if ($strcmd == "insertBlockType")
			{	
				$strdbsql = "INSERT INTO site_block_types (description) VALUES (:description)";
				$strType = "insert";
			}
			elseif ($strcmd == "updateBlockType")
			{
				$strdbsql = "UPDATE site_block_types SET description = :description WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $siteBlockTypeID;
				$strType = "update";
			}
			
			$updateBlockType = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertBlockType")
			{
				$siteBlockTypeID = $updateBlockType;
			}
			
			if ($strcmd == "insertBlockType")
			{
				if ($updateBlockType > 0)
				{
					$strsuccess = "Block Type successfully added";
				}
				elseif ($updateBlockType == 0)
				{
					$strerror = "An error occurred while adding the block type";
				}
			}
			elseif ($strcmd == "updateBlockType")
			{
				if ($updateBlockType <= 1)
				{
					$strsuccess = "Block type successfully updated";
				}
				elseif ($updateBlockType > 1)
				{
					$strwarning = "An error may have occurred while updating this block type";
				}
			}
			
			$strcmd = "viewBlockType";
			
		break;
		
		case "deleteBlockType":
			
			$strdbsql = "DELETE FROM site_block_types WHERE recordID = :recordID";
			$strType = "delete";
			$arrdbparams['recordID'] = $siteBlockTypeID;
			$deletePage = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strcmd = "";
			
		break;
		
		case "insertBlock":
		case "updateBlock":
			
			if($_POST['frm_includename'] == "null") { $_POST['frm_includename'] = null; }
			$arrdbparams = array(
								"description" => $_POST['frm_blockdescription'],
								"content" => $_POST['frm_blockcontent'],
								"type" => $siteBlockTypeID,
								"includeName" => $_POST['frm_includename'],
								"includePos" => $_POST['frm_includepos']
							);
			if ($siteBlockTypeID == 3)
			{
				$arrdbparams['galleryID'] = $_POST['frm_gallery'];
			}
			if ($strcmd == "insertBlock")
			{	
				$strdbsql = "INSERT INTO site_blocks (description, content, type, includeName, includePos";
				
				if ($siteBlockTypeID == 3)
				{
					$strdbsql .= ", galleryID";
				}
				
				$strdbsql .= ") VALUES (:description, :content, :type, :includeName, :includePos";
				
				if ($siteBlockTypeID == 3)
				{
					$strdbsql .= ", :galleryID";
				}
				
				$strdbsql .= ")";
				$strType = "insert";
			}
			elseif ($strcmd == "updateBlock")
			{
				$strdbsql = "UPDATE site_blocks SET description = :description, content = :content, includeName = :includeName, includePos = :includePos";
				
				if ($siteBlockTypeID == 3)
				{
					$strdbsql .= ", galleryID = :galleryID";
				}
				
				$strdbsql .= " WHERE recordID = :recordID AND type = :type";
				
				$arrdbparams['recordID'] = $siteBlockID;
				$strType = "update";
			}
			
			$updateBlock = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertBlock")
			{
				$siteBlockID = $updateBlock;
			}
			
			if ($strcmd == "insertBlock")
			{
				if ($updateBlock > 0)
				{
					$strsuccess = "Block successfully added";
				}
				elseif ($updateBlock == 0)
				{
					$strerror = "An error occurred while adding the block";
				}
			}
			elseif ($strcmd == "updateBlock")
			{
				if ($updateBlock <= 1)
				{
					$strsuccess = "Block successfully updated";
				}
				elseif ($updateBlock > 1)
				{
					$strwarning = "An error may have occurred while updating this block";
				}
			}
			
			$strcmd = "viewBlock";
		
		break;
		
		case "insertGallery":
		case "updateGallery":
		
			$arrdbparams = array(
								"description" => $_POST['frm_gallerydescription']
							);
			if ($strcmd == "insertGallery")
			{	
				$strdbsql = "INSERT INTO site_gallery (description) VALUES (:description)";
				$strType = "insert";
			}
			elseif ($strcmd == "updateGallery")
			{
				$strdbsql = "UPDATE site_gallery SET description = :description WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $galleryID;
				$strType = "update";
			}
			
			$updateGallery = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertGallery")
			{
				$galleryID = $updateGallery;
			}
			
			if ($strcmd == "insertGallery")
			{
				if ($updateGallery != 0)
				{
					$strsuccess = "Gallery successfully added";
				}
				elseif ($updateGallery == 0)
				{
					$strerror = "An error occurred while adding the gallery";
				}
			}
			elseif ($strcmd == "updateGallery")
			{
				if ($updateGallery <= 1)
				{
					$strsuccess = "Gallery successfully updated";
				}
				elseif ($updateGallery > 1)
				{
					$strwarning = "An error may have occurred while updating this gallery";
				}
			}
			
			$strcmd = "viewGallery";
			
		break;
		
		case "addGalleryImage":
		
			$strdbsql = "SELECT COUNT(*) AS max_order FROM site_gallery_images WHERE galleryID = :galleryID";
			$strType = "single";
			$arrdbparam = array( "galleryID" => $galleryID );
			$maxOrder = query($conn, $strdbsql, $strType, $arrdbparam);
		
			$arrdbparams = array(
								"galleryID" => $galleryID,
								"title" => $_POST['frm_galleryimgtitle'],
								"description" => $_POST['frm_galleryimgdescription'],
								"imageLocation" => $_POST['unusedImages'],
								"linkLocation" => $_POST['frm_galleryimglink'],
								"galleryOrder" => $maxOrder['max_order'] + 1
							);
			$strdbsql = "INSERT INTO site_gallery_images (galleryID, title, description, imageLocation, linkLocation, galleryOrder) VALUES (:galleryID, :title, :description, :imageLocation, :linkLocation, :galleryOrder)";
			$strType = "insert";
			
			$addImage = query($conn, $strdbsql, $strType, $arrdbparams);
			if ($addImage < 0)
			{
				$strsuccess = "Image successfully added";
			}
			elseif ($addImage == 0)
			{
				$strerror = "An error occurred while adding the image";
			}
			
			$strcmd = "viewGallery";
			
		break;
		
		case "insertImage":
			
			$newfilename = $_FILES['frm_image']['name'];
			$strfileextn = getExtension($newfilename);
			
			$strimguploadpath = "images/";
			$struploaddir = $strrootpath.$strimguploadpath; // where to put full size image
			$upfile = $struploaddir.$newfilename;
			
			//        print("$upfile - saving image<br>");

			if ($_FILES['frm_image']['error'] > 0)
			{
				switch ($_FILES['frm_image']['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				//   print("$strfileextn - checking extensions<br>");
				// Does the file have the right extension ?
				//if ($strfileextn != 'jpg' && $strfileextn != 'jpeg' && $strfileextn != 'png' && $strfileextn != 'gif')
				if (strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0)
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_image']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
				}
				else
				{
					//      print("Uploading to products image directory<br>");
					if (is_uploaded_file($_FILES['frm_image']['tmp_name']))
					{
						if (!move_uploaded_file($_FILES['frm_image']['tmp_name'], $upfile))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_image']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_image']['name'].". Please try again.<br/>";
					}
				}
			}
			
			if ($strerror != "")
			{
				$strcommand = "ERRORMSG";
				//   print("Got an Error - $strcommand - $strerror<br>");
			}
			else
			{
				
				$strgallerypath = $strimguploadpath."gallery/";
				
				$strgallerydir = $strrootpath.$strgallerypath;
				
				//$newfilename = "img_".$stockOptionID."_".$_REQUEST['frm_imagetype'].".".$strfileextn;
				
				//overlay
				$newfile = $strgallerydir.$newfilename;
				$overlay=copy($upfile,$newfile);
				chmod ($newfile, 0777);
				
				unlink($upfile);
				
				if (isset($_REQUEST['frm_addcurrent'])) $addCurrent = $_REQUEST['frm_addcurrent']; else $addCurrent = 0;
				
				if($addCurrent)
				{
					$strdbsql = "SELECT COUNT(*) AS max_order FROM site_gallery_images WHERE galleryID = :galleryID";
					$strType = "single";
					$arrdbparam = array( "galleryID" => $galleryID );
					$maxOrder = query($conn, $strdbsql, $strType, $arrdbparam);
					$order = $maxOrder['max_order'] + 1;
					
					$insertImageQuery = "INSERT INTO site_gallery_images (galleryID, title, description, imageLocation, linkLocation, galleryOrder) VALUES (:galleryID, :title, :description, :imageLocation, :linkLocation, :galleryOrder)";
					$strType = "insert";
					$arrdbparams = array (
										"galleryID" => $galleryID,
										"title" => $_POST['frm_imagetitle'],
										"description" => $_POST['frm_imagedescrip'],
										"imageLocation" => $newfilename,
										"linkLocation" => $_POST['frm_imagelink'],
										"galleryOrder" => $order
									);
					$insertImage = query($conn, $insertImageQuery, $strType, $arrdbparams);
				}
			}
			
			$strcmd = "viewGallery";
			
		break;
		
		case "deleteImages":
			
			$strdbsql = "DELETE FROM site_gallery_images WHERE recordID IN (".$_POST['deleteImages'].")";
			$strType = "delete";
			$removeImages = query($conn, $strdbsql, $strType);
			
			$strdbsql = "SELECT recordID FROM site_gallery_images WHERE galleryID = :galleryID ORDER BY galleryOrder";
			$strType = "multi";
			$arrdbparams = array(
							"galleryID" => $galleryID
						);
			//echo "get products\n";
			$galleryImages = query($conn, $strdbsql, $strType, $arrdbparams);
			//var_dump($categories);
			
			$i = 1;
			foreach($galleryImages AS $galleryImage)
			{
				$strdbsql = "UPDATE site_gallery_images SET galleryOrder = ".$i." WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparam = array(
								"recordID" => $galleryImage['recordID']
							);
				//echo "update final ordering\n";
				query($conn, $strdbsql, $strType, $arrdbparam);
				$i++;
			}
			
			$strcmd = "viewGallery";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Static Blocks</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddBlockType() {
					document.form.cmd.value='addBlockType';
					document.form.submit();
				}
				function jsinsertBlockType() {
					document.form.cmd.value='insertBlockType';
					document.form.submit();
				}
				function jsviewBlockType(blockTypeID) {
					if ($('#form').valid()) {
						document.form.cmd.value='viewBlockType';
						document.form.blockTypeID.value=blockTypeID;
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsdeleteBlockType(blockTypeID) {
					if(confirm("Are you sure you want to delete this page?"))
					{
						document.form.cmd.value="deleteBlockType";
						document.form.blockTypeID.value=blockTypeID;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsupdateBlockType() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateBlockType';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsaddBlock() {
					document.form.cmd.value='addBlock';
					document.form.submit();
				}
				function jsinsertBlock() {
					document.form.cmd.value='insertBlock';
					document.form.submit();
				}
				function jsviewBlock(siteBlockID) {
					if ($('#form').valid()) {
						document.form.cmd.value='viewBlock';
						document.form.siteBlockID.value=siteBlockID;
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsupdateBlock() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateBlock';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsaddGallery() {
					document.form.cmd.value='addGallery';
					document.form.submit();
				}
				function jsinsertGallery() {
					document.form.cmd.value='insertGallery';
					document.form.submit();
				}
				function jsviewGallery(galleryID) {
					if ($('#form').valid()) {
						document.form.cmd.value='viewGallery';
						document.form.galleryID.value=galleryID;
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsupdateGallery() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateGallery';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsaddGalleryImage() {
					document.form.cmd.value='addGalleryImage';
					document.form.submit();
				}
				function jsinsertImage() {
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				function jsdeleteImages() {
					if ($("#sortableGalleryImages li.selected").length > 0)
					{
						if(confirm("Are you sure you want to delete this image(s)?"))
						{
							var deleteImages = "";
							$("#sortableGalleryImages li.selected").each(function (index, item) {
								deleteImages += $(item).attr("id")+",";
							});
							deleteImages = deleteImages.replace(/,\s*$/, "");
							//console.log(deleteImages);
							document.form.cmd.value="deleteImages";
							document.form.deleteImages.value=deleteImages;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the image(s) you wish to delete");
					}
					
				}
				
				$().ready(function() {
					$("#sortableGalleryImages").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								
								var oldOrder = $(item).attr("data-order");
								$("ul#sortableGalleryImages").children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if (parseInt(newOrder) > parseInt(oldOrder))
								{
									sortType = "DESC";
								}
								else if (parseInt(newOrder) < parseInt(oldOrder))
								{
									sortType = "ASC";
								}
								
								dataArray[i] = {};
								dataArray[i]["galleryOrder"] = $(item).attr("data-order");
								dataArray[i]["imageLocation"] = /[^/]*$/.exec($(item).children("img").attr("src"))[0];
								dataArray[i]["recordID"] = $(item).attr("id");
								dataArray[i]["galleryID"] = $("#galleryID").attr("value");
								dataArray[i]["cmd"] = "reorder";
								//console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									//console.log("sort ascending");
									return parseInt(a.galleryOrder) - parseInt(b.galleryOrder);
								}
								else if (sortType == "DESC")
								{
									//console.log("sort descending");
									return parseInt(b.galleryOrder) - parseInt(a.galleryOrder);
								}
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_galleryimagemanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
			</script>
			<?php
		
			print("<form action='cms-static-blocks.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='siteBlockTypeID' id='blockTypeID' value='".$siteBlockTypeID."' />");
				print("<input type='hidden' name='siteBlockID' id='siteBlockID' value='".$siteBlockID."' />");
				print("<input type='hidden' name='galleryID' id='galleryID' value='".$galleryID."' />");
				
				switch($strcmd)
				{
					case "viewBlockType":
					case "addBlockType":
						
						if ($strcmd == "viewBlockType")
						{
							$strdbsql = "SELECT * FROM site_block_types WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $siteBlockTypeID);
							$blockTypeDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							
							/*print("<pre>");
							print_r($blockTypeDetails);
							print("</pre>");&*/
							
							print("<div class='row'>");
								print("<div class='col-lg-6'>");
									print("<div class='section col-sm-12'>");
										print("<fieldset class='inlineLabels'>");
										
											print("<legend>Change Site Block Type</legend>");
						}
						elseif ($strcmd == "addBlockType")
						{
							$blockTypeDetails = array(
													"description" => "",
													"content" => "",
													"type" => ""
												);
							
							print("<div class='row'>");
								print("<div class='col-lg-6'>");
									print("<div class='section col-sm-12'>");
										print("<fieldset class='inlineLabels'>");
										
											print("<legend>Add Site Block Type</legend>");
						}
							
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_blocktypedescription' class='control-label'>Description</label>");
													print("<input type='text' class='form-control' id='frm_blocktypedescription' name='frm_blocktypedescription' value='".$blockTypeDetails['description']."'>");
												print("</div>");
											print("</div>");
										
										print("</fieldset>");	
									print("</div>");	
										
									if ($strcmd == "viewBlockType")
									{
										
										$strdbsql = "SELECT * FROM site_blocks WHERE type = :type";
										$strType = "multi";
										$arrdbparams = array("type" => $siteBlockTypeID);
										$siteBlocks = query($conn, $strdbsql, $strType, $arrdbparams);
										
										print("<div class='section col-sm-12'>");
											print("<table class='table table-striped table-bordered table-hover table-condensed' id='blocks-table'>");
												print("<thead>");
													print("<th width='90%'>Description</th>");
													print("<th width='10%' style='text-align:center;'>Update</th>");
												print("</thead>");
												print("<tbody>");
												if (count($siteBlocks) > 0)
												{
													foreach($siteBlocks AS $siteBlock)
													{
														print("<tr>");
															print("<td>".$siteBlock['description']."</td>");
															print("<td style='text-align:center;'><button onclick='return jsviewBlock(\"".$siteBlock['recordID']."\");' type='submit' class='btn btn-primary circle' style='display:inline-block;'><i class='fa fa-pencil'></i></button></td>");
														print("</tr>");
													}
												}
												print("</tbody>");
											print("</table>");
										
											print("<button onclick='return jsaddBlock();' type='submit' class='btn btn-success circle' style='position:absolute;width:30px;height:30px;line-height:30px;bottom:21px;right:182px;'><i class='fa fa-plus'></i></button>");
											
										print("</div>");	
											
									}
									
								print("</div>");
							print("</div>");
						
						print("<div class='row'>");
							print("<div class='col-md-6'>");
								print("<button onclick='return jscancel(\"\");' class='btn btn-danger pull-left'>Cancel</button>");
								if ($strcmd == "addBlockType")
								{
									print("<button onclick='return jsinsertBlockType();' type='submit' class='btn btn-success pull-right'>Save</button> ");
								}
								elseif ($strcmd == "viewBlockType")
								{
									print("<button onclick='return jsupdateBlockType();' type='submit' class='btn btn-success pull-right'>Save</button> ");
								}
							print("</div>");
						print("</div>");
						
						break;
						
					case "viewGallery":
					case "addGallery":
						
						if ($strcmd == "viewGallery")
						{
							$strdbsql = "SELECT * FROM site_gallery WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $galleryID);
							$galleryDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							
							/*print("<pre>");
							print_r($galleryDetails);
							print("</pre>");&*/
							
							print("<div class='row'>");
								print("<div class='col-sm-6 crop-sm-right'>");
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
										
										print("<legend>Details</legend>");
						}
						elseif ($strcmd == "addGallery")
						{
							$galleryDetails = array(
													"description" => ""
												);
							
							print("<div class='row'>");
								print("<div class='col-sm-6 crop-sm-right'>");
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
										
											print("<legend>Details</legend>");
							}	
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_gallerydescription' class='control-label'>Description</label>");
													print("<input type='text' class='form-control' id='frm_gallerydescription' name='frm_gallerydescription' value='".$galleryDetails['description']."'>");
												print("</div>");
											print("</div>");
										
										print("</fieldset>");
									print("</div>");
									
									if ($strcmd == "viewGallery") {
									
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
									
											print("<legend>Add New Image</legend>");
									
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_image' class='control-label'>Image</label>");
													print("<input type='file' class='form-control' id='frm_image' name='frm_image' />");
												print("</div>");
											print("</div>");
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_imagetitle' class='control-label'>Description</label>");
													print("<input type='text' class='form-control' id='frm_imagetitle' name='frm_imagetitle' />");
												print("</div>");
											print("</div>");
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_imagedescrip' class='control-label'>Description</label>");
													print("<input type='text' class='form-control' id='frm_imagedescrip' name='frm_imagedescrip' />");
												print("</div>");
											print("</div>");
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_imagelink' class='control-label'>Link</label>");
													print("<input type='text' class='form-control' id='frm_imagelink' name='frm_imagelink' />");
												print("</div>");
											print("</div>");
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_addcurrent' class='control-label'>Add to current gallery</label>");
													print("<input type='checkbox' id='frm_addcurrent' name='frm_addcurrent' value='1' />");
												print("</div>");
											print("</div>");
											print("<div class='row'>");
												print("<div class='form-group' style='text-align:right;'>");
													print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success' style='display:inline-block;'>Insert</button>");
												print("</div>");
											print("</div>");
											
										print("</fieldset>");
									print("</div>");
									
									}
									
								print("</div>");
							
								if ($strcmd == "viewGallery") {
									
								print("<div class='col-sm-6 crop-sm-left'>");
								
									$strdbsql = "SELECT * FROM site_gallery_images WHERE galleryID = :galleryID ORDER BY galleryOrder ASC";
									$strType = "multi";
									$arrdbparam = array( "galleryID" => $galleryID );
									$galleryImages = query($conn, $strdbsql, $strType, $arrdbparam);
									
									$unusedImages = array();
									$dh = opendir("../images/gallery");
									while (false !== ($galleryImg = readdir($dh))) {
										if ($galleryImg != "." && $galleryImg != "..") {
											$unusedImages[] = $galleryImg;
										}
									}
									
									$assignedImages = "";
									if(!empty($galleryImages))
									{
										foreach($galleryImages AS $galleryImage)
										{
											$assignedImages .= "<li id='".$galleryImage['recordID']."' data-order='".$galleryImage['galleryOrder']."'><img src='../images/gallery/".$galleryImage['imageLocation']."' style='width: 212px;' /></li>";
											$key = array_search($galleryImage['imageLocation'], $unusedImages);
											if($key !== false)
											{
												unset($unusedImages[$key]);
											}
										}
									}
								
									if (count($unusedImages) > 0 || $assignedImages != "")
									{
										
										if(count($unusedImages) > 0)
										{
											
											print("<div class='section col-sm-12'>");
												print("<fieldset class='inlineLabels'>");
										
													print("<legend>Unused Gallery Images</legend>");
											
													print("<div class='row'>");
														print("<div class='form-group'>");
															print("<label for='unusedGalleryImages' control-label'>Images</label>");
															print("<select id='unusedGalleryImages' name='unusedImages' class='form-control valid'>");
																foreach ($unusedImages AS $unusedImage)
																{
																	print("<option value='".$unusedImage."'>".$unusedImage."</option>");
																}
															print("</select>");
														print("</div>");
													print("</div>");
													
													print("<div class='row'>");
														print("<div class='form-group'>");
															print("<label for='frm_galleryimgtitle' class='control-label'>Title</label>");
															print("<input type='text' class='form-control' id='frm_galleryimgtitle' name='frm_galleryimgtitle' />");
														print("</div>");
													print("</div>");
													
													print("<div class='row'>");
														print("<div class='form-group'>");
															print("<label for='frm_galleryimgdescription' class='control-label'>Description</label>");
															print("<input type='text' class='form-control' id='frm_galleryimgdescription' name='frm_galleryimgdescription' />");
														print("</div>");
													print("</div>");
													
													print("<div class='row'>");
														print("<div class='form-group'>");
															print("<label for='frm_galleryimglink' class='control-label'>Link</label>");
															print("<input type='text' class='form-control' id='frm_galleryimglink' name='frm_galleryimglink' />");
														print("</div>");
													print("</div>");
													
													print("<div class='row'>");
														print("<div class='form-group' style='text-align:right;'>");
															print("<button style='display:inline-block;' onclick='return jsaddGalleryImage();' type='submit' class='btn btn-success'>Add Image</button> ");
														print("</div>");
													print("</div>");
															
												print("</fieldset>");
											print("</div>");
											
										}
										
										print("<div class='section col-sm-12'>");
											print("<fieldset class='inlineLabels'>");
										
												print("<legend>Images <div class='pull-right'><button onclick='return jsdeleteImages();' type='button' class='btn btn-danger circle'><i class='fa fa-trash' style='color:#FFF;'></i></button></div><input type='hidden' name='deleteImages' id='deleteImages' /></legend>");
										
												print("<div class='row'>");
													print("<div class='form-group'>");
														print("<ul id='sortableGalleryImages' style='height:124px;'>");
															print($assignedImages);
														print("</ul>");
													print("</div>");
												print("</div>");
											
											print("</fieldset>");
										print("</div>");

									}

								print("</div>");
							print("</div>");
							
							print("<div class='row'>");
								print("<div class='col-xs-6'>");
									print("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>");
								print("</div>");
								print("<div class='col-xs-6'>");
									if ($strcmd == "addGallery")
									{
									  print("<button onclick='return jsinsertGallery();' type='submit' class='btn btn-success pull-right'>Save</button> ");
									}
									elseif ($strcmd == "viewGallery")
									{
									  print("<button onclick='return jsupdateGallery();' type='submit' class='btn btn-success pull-right'>Save</button> ");
									}
								print("</div>");
							print("</div>");

								}
						
					break;
						
					case "viewBlock":
					case "addBlock":
						
						if ($strcmd == "viewBlock")
						{
							$strdbsql = "SELECT * FROM site_blocks WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $siteBlockID);
							$blockDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							
							/*print("<pre>");
							print_r($blockDetails);
							print("</pre>");&*/
							
							print("<div class='row'>");
								print("<div class='col-sm-6'>");
									print("<div class='section col-sm-12'>");
										print("<fieldset class='inlineLabels'>");
										
										print("<legend>Change Site Block Details</legend>");
						}
						elseif ($strcmd == "addBlock")
						{
							$blockDetails = array(
												"description" => "",
												"content" => "",
												"type" => "",
												"galleryID" => "",
												"includeName" => "",
												"includePos" => ""
											);
							
							print("<div class='row'>");
								print("<div class='col-sm-6'>");
									print("<div class='section col-sm-12'>");
										print("<fieldset class='inlineLabels'>");
										
											print("<legend>Add Site Block Details</legend>");
						}
							
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_blockdescription' class='control-label'>Description</label>");
													print("<input type='text' class='form-control' id='frm_blockdescription' name='frm_blockdescription' value='".$blockDetails['description']."'>");
												print("</div>");
											print("</div>");
											
											if ($siteBlockTypeID == 3)
											{
												$strdbsql = "SELECT * FROM site_gallery";
												$strType = "multi";
												$galleries = query($conn, $strdbsql, $strType);
												
												print("<div class='row'>");
													print("<div class='form-group'>");
														print("<label for='frm_gallery' class='control-label'>Gallery</label>");
														print("<select type='text' class='form-control' id='frm_gallery' name='frm_gallery'>");
															foreach($galleries AS $gallery)
															{
																$strSelected = "";
																if ($gallery['recordID'] == $blockDetails['galleryID'])
																{
																	$strSelected = " selected";
																}
															
																print("<option value='".$gallery['recordID']."'".$strSelected.">".$gallery['description']."</option>");
															}
														print("</select>");
													print("</div>");
												print("</div>");
											}
											
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_blockcontent' class='control-label'>Content</label>");
													print("<textarea class='form-control tinymce' id='frm_blockcontent' name='frm_blockcontent' rows='3'>".$blockDetails['content']."</textarea>");
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_includename' class='control-label'>Include</label>");
													print("<select class='form-control' id='frm_includename' name='frm_includename'>");
														print("<option value='null'>None</option>");
									
														$path = $_SERVER['DOCUMENT_ROOT']."/includes";
														if ($handle = opendir($path)) {
															$disallowedIncludes = array(".", "..", "ajax_webbookings.php", "footer.php", "formhandler.php", "header.php", "inc_bookings.php", "incemailtest.php", "incsitecommon.php", "sidebar.php");
															while (false !== ($file = readdir($handle))) {
																if (!in_array($file, $disallowedIncludes))
																{
																	if($file == $blockDetails['includeName'])
																	{
																		$strSelected = "selected";
																	}
																	else
																	{
																		$strSelected = "";
																	}
																	print("<option value='".$file."'".$strSelected.">".$file."</option>");
																}
															}
															closedir($handle);
														}
									
													print("</select>");
												print("</div>");
											print("</div>");
											
											$includePositions = array("0" => "Before Text", "1" => "After Text");
							
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_includepos' class='control-label'>Include Position</label>");
													print("<select class='form-control' id='frm_includepos' name='frm_includepos'>");
														
														foreach($includePositions AS $value => $includePosition)
														{
															if($value == $blockDetails['includePos'])
															{
																$strSelected = "selected";
															}
															else
															{
																$strSelected = "";
															}
															print("<option value='".$value."'".$strSelected.">".$includePosition."</option>");
														}
														
													print("</select>");
												print("</div>");
											print("</div>");
										
										print("</fieldset>");
									print("</div>");
								print("</div>");
							print("</div>");
							
							print("<div class='row'>");
								print("<div class='col-md-6'>");
									print("<button onclick='return jscancel(\"viewBlockType\");' class='btn btn-danger pull-left'>Cancel</button>");
									if ($strcmd == "addBlock")
									{
									  print("<button onclick='return jsinsertBlock();' type='submit' class='btn btn-success pull-right'>Save</button> ");
									}
									elseif ($strcmd == "viewBlock")
									{
									  print("<button onclick='return jsupdateBlock();' type='submit' class='btn btn-success pull-right'>Save</button> ");
									}
								print("</div>");
							print("</div>");
					
						break;
						
					default:
						
					$strdbsql = "SELECT * FROM site_block_types ORDER BY description ASC";
					$strType = "multi";
					$blockTypes = query($conn, $strdbsql, $strType);
					
					print("<div class='row'>");
						print("<div class='col-xs-12'>");
							print("<div class='section'>");
								print("<table class='table table-striped table-bordered table-hover table-condensed' id='blocks-table'>");
									print("<thead>");
										print("<th width='80%'>Site Block Type</th>");
										print("<th width='10%' style='text-align:center;'>Update</th>");
										print("<th width='10%' style='text-align:center;'>Delete</th>");
									print("</thead>");
									print("<tbody>");
									if (count($blockTypes) > 0)
									{
										foreach($blockTypes AS $blockType)
										{
											print("<tr>");
												print("<td>".$blockType['description']."</td>");
												print("<td style='text-align:center;'><button onclick='return jsviewBlockType(\"".$blockType['recordID']."\");' type='submit' class='btn btn-primary circle' style='display:inline-block;'><i class='fa fa-pencil'></i></i></button></td>");
												print("<td style='text-align:center;'><button onclick='return jsdeleteBlockType(\"".$blockType['recordID']."\");' type='submit' class='btn btn-danger circle' style='display:inline-block;'><i class='fa fa-trash'></i></i></button></td>");
											print("</tr>");
										}
									}
									print("</tbody>");
								print("</table>");
							print("</div>");
						print("</div>");
					print("</div>");
					
					print("<div class='row' style='margin-bottom:15px;'>");
						print("<div class='col-xs-12'>");
							print("<button onclick='return jsaddBlockType();' type='submit' class='btn btn-success pull-right'>Add</button>");
						print("</div>");	
					print("</div>");	
					
					$strdbsql = "SELECT * FROM site_gallery ORDER BY description ASC";
					$strType = "multi";
					$galleries = query($conn, $strdbsql, $strType);
					
					print("<div class='row'>");
						print("<div class='col-xs-12'>");
							print("<div class='section'>");
								print("<table class='table table-striped table-bordered table-hover table-condensed' id='galleries-table'>");
									print("<thead><tr>");
										print("<th width='90%'>Gallery</th>");
										print("<th width='10%' style='text-align:center;'>Update</th>");
									print("</thead>");
									print("</tr><tbody>");
									if (count($galleries) > 0)
									{
										foreach($galleries AS $gallery)
										{
											print("<tr>");
												print("<td>".$gallery['description']."</td>");
												print("<td style='text-align:center;'><button onclick='return jsviewGallery(\"".$gallery['recordID']."\");' type='submit' class='btn btn-primary circle' style='display:inline-block;'><i class='fa fa-pencil'></i></i></button></td>");
											print("</tr>");
										}
									}
									print("</tbody>");
								print("</table>");
							print("</div>");
						print("</div>");
					print("</div>");
					
					print("<div class='row'>");
						print("<div class='col-xs-12'>");
							print("<button onclick='return jsaddGallery();' type='submit' class='btn btn-success pull-right'>Add</button>");
						print("</div>");
					print("</div>");
						
					break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
		});
			
		$('#blocks-table').DataTable();
		$('#galleries-table').DataTable();

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>