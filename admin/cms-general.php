<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * cms-general.php					                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

//	use function inc\connect;
//use function inc\query;

session_start(); //stores session variables such as access levels and logon details
	$strpage = "cms-general"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	REQUIRE_ONCE("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['pageRecordID'])) $pageRecordID = $_REQUEST['pageRecordID']; else $pageRecordID = "";
	
	$pageRecordID = 1;
	
	switch($strcmd)
	{
		case "insertPage":
		case "updatePage":
			
			$arrdbparams = array(
								"companyName" => $_POST['frm_companyname'],
								"companyLogo" => $_POST['frm_companylogo'],
								"companyFax" => $_POST['frm_companyfax'],
								"companyEmail" => $_POST['frm_companyemail'],
								"companyEmail2" => $_POST['frm_companyemail2'],
								"companyPhone" => $_POST['frm_companyphone'],
								"addressNumber" => $_POST['frm_addressnumber'],
								"addressStreet" => $_POST['frm_addressstreet'],
								"addressCity" => $_POST['frm_addresscity'],
								"addressCounty" => $_POST['frm_addresscounty'],
								"addressCountry" => $_POST['frm_addresscountry'],
								"addressPostCode" => $_POST['frm_addresspostcode'],
								"companyNumber" => $_POST['frm_companynumber']
							);
			
				$strdbsql = "UPDATE site_company_details SET companyName = :companyName, companyLogo = :companyLogo, companyFax = :companyFax, companyEmail = :companyEmail, companyEmail2 = :companyEmail2, companyPhone = :companyPhone, addressNumber = :addressNumber, 
							addressStreet = :addressStreet, addressCity = :addressCity, addressCounty = :addressCounty, addressCountry = :addressCountry, addressPostCode = :addressPostCode, companyNumber = :companyNumber WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $pageRecordID;
				$strType = "update";
			
			$updatePage = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($updatePage <= 1)
			{
				$strsuccess = "Page successfully updated";
			}
			elseif ($updatePage > 1)
			{
				$strwarning = "An error may have occurred while updating this page";
			}
			
			$strcmd = "";
			
		break;
		
		case "insertImage":
			$newfilename = $_FILES['frm_newimage']['name'];
			$strfileextn = getExtension($newfilename);
			
			$strimguploadpath = "images/company/";
			$struploaddir = $strrootpath.$strimguploadpath; // where to put full size image
			$upfile = $struploaddir.$newfilename;
			
			//        print("$upfile - saving image<br>");

			
			if ($_FILES['frm_newimage']['error'] > 0)
			{
				switch ($_FILES['frm_newimage']['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				   print("$newfilename $strfileextn - checking extensions<br>");
				// Does the file have the right extension ?
				//if ($strfileextn != 'jpg' && $strfileextn != 'jpeg' && $strfileextn != 'png' && $strfileextn != 'gif')
				if (strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0)
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_newimage']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
				}
				else
				{
					//      print("Uploading to products image directory<br>");
					if (is_uploaded_file($_FILES['frm_newimage']['tmp_name']))
					{
						if (!move_uploaded_file($_FILES['frm_newimage']['tmp_name'], $upfile))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_newimage']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_newimage']['name'].". Please try again.<br/>";
					}
				}
			}
			
			if ($strerror != "")
			{
				$strcommand = "ERRORMSG";
				//   print("Got an Error - $strcommand - $strerror<br>");
			}
			else
			{
				$strpagepath = $strimguploadpath."company/";
				
				$strpagedir = $strrootpath.$strpagepath;
				
				$newfile = $strpagedir.$newfilename;
				$range=copy($upfile,$newfile);
				chmod ($newfile, 0777);
				
				unlink($upfile);
				
				$updatePageImageQuery = "UPDATE site_company_details SET companyLogo = :image WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparams = array (
								"image" => $newfilename,
								"recordID" => $pageRecordID
							);
				$updatePageImage = query($conn, $updatePageImageQuery, $strType, $arrdbparams);
			}
			
			$strsuccess = "New page image added";
			
			$strcmd = "";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");
		
			print("<h1>General</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddPage() {
					document.form.cmd.value="addPage";
					document.form.submit();
				}
				function jsviewPage(pageRecordID) {
					document.form.cmd.value="viewPage";
					document.form.pageRecordID.value=pageRecordID;
					document.form.submit();
				}
				function jsdeletePage(pageRecordID) {
					if(confirm("Are you sure you want to delete this page?"))
					{
						document.form.cmd.value="deletePage";
						document.form.pageRecordID.value=pageRecordID;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertPage() {
					document.form.cmd.value='insertPage';
					document.form.submit();
				}
				function jsupdatePage() {
					if ($('#form').valid()) {
						document.form.cmd.value='updatePage';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertImage() {
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jsaddSiteBlock() {
					if ($('#form').valid()) {
						document.form.cmd.value='addSiteBlock';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsdeleteSiteBlocks() {
					if ($(".blockPosList li.selected").length > 0)
					{
						if(confirm("Are you sure you want to delete this block(s)?"))
						{
							var deleteBlocks = "";
							$(".blockPosList li.selected").each(function (index, item) {
								deleteBlocks += $(item).attr("id")+",";
							});
							$("#blockPositionID").val($(".blockPosList li.selected").attr("data-pos"));
							//console.log($(".blockPosList li.selected").parent().attr("id"));
							deleteBlocks = deleteBlocks.replace(/,\s*$/, "");
							//console.log(deleteImages);
							document.form.cmd.value="deleteBlocks";
							document.form.deleteBlocks.value=deleteBlocks;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the block(s) you wish to delete");
					}
					
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				
				$().ready(function() {
					$(".blockPosList").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
						$(this).parent().parent().siblings().children("ul").children().removeClass('selected');
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							//console.log(item);
							
							//Clone the selected items into an array
							//console.log(item.parent().children('.selected'));
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								//console.log($(item));
								//console.log($(item).attr('data-order'));
								var oldOrder = $(item).attr("data-order");
								
								$(item).parent().children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if (parseInt(newOrder) > parseInt(oldOrder))
								{
									sortType = "DESC";
								}
								else if (parseInt(newOrder) < parseInt(oldOrder))
								{
									sortType = "ASC";
								}
								
								dataArray[i] = {};
								dataArray[i]["pageOrder"] = $(item).attr("data-order");
								dataArray[i]["positionID"] = $(item).attr("data-pos");
								dataArray[i]["recordID"] = $(item).attr("id");
								dataArray[i]["sitePageID"] = $("#pageRecordID").attr("value");
								dataArray[i]["type"] = "page";
								dataArray[i]["cmd"] = "reorder";
								//console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									//console.log("sort ascending");
									return parseInt(a.pageOrder) - parseInt(b.pageOrder);
								}
								else if (sortType == "DESC")
								{
									//console.log("sort descending");
									return parseInt(b.pageOrder) - parseInt(a.pageOrder);
								}
							});
							//console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_siteblockrelationmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
			</script>
			<?php
		
			print("<form action='cms-general.php' class='uniForm col-sm-12' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='pageRecordID' id='pageRecordID' value='$pageRecordID'/>");
				print("<input type='hidden' name='blockPositionID' id='blockPositionID' />");
					
				$strdbsql = "SELECT * FROM site_company_details WHERE recordID = 1";
				$strType = "single";
				$pageDetails = query($conn, $strdbsql, $strType);
				/*print("<pre>");
				print_r($pageDetails);
				print("</pre>");*/
				
				print("<div class='row'>");
					print("<div class='col-sm-6 crop-sm-right'>");
						print("<div class='section col-sm-12'>");
							print("<fieldset class='inlineLabels'>");
							
								print("<legend>Details</legend>");
								
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_companyname' class='control-label'>Company Name</label>");
										print("<input type='text' class='form-control' id='frm_companyname' name='frm_companyname' value='".$pageDetails['companyName']."'>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group col-md-6 crop-lg-right'>");
										print("<label for='frm_companylogo' class='control-label'>Logo <i class='fa fa-info-circle pull-right' data-toggle='tooltip' data-placement='top' title='Use the ADD NEW IMAGE section to add images to this list.'></i></label>");
										print("<select id='frm_companylogo' name='frm_companylogo' class='form-control' >");
											print("<option value=''>None</option>");
												
												$dh = opendir("../images/company/");
												
												while (false !== ($image = readdir($dh))) {
													if ($image != "." && $image != "..") {
														if($image == $pageDetails['companyLogo'])
														{
															$strselected = " selected";
														}
														else
														{
															$strselected = "";
														}
														print("<option value='".$image."'".$strselected.">".$image."</option>");
													}
												}
												
										print("</select>");
									print("</div>");
									print("<div class='form-group col-md-6 crop-lg-left'>");
										print("<label for='frm_companyfax' control-label'>Fax Number</label>");
										print("<input type='text' class='form-control' id='frm_companyfax' name='frm_companyfax' value='".$pageDetails['companyFax']."'>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_companynumber' class='control-label'>Company Number</label>");
										print("<input type='text' class='form-control' id='frm_companynumber' name='frm_companynumber' value='".$pageDetails['companyNumber']."'>");
									print("</div>");
								print("</div>");
							print("</fieldset>");
						print("</div>");						
					print("</div>");								
								
					print("<div class='col-sm-6 crop-sm-left'>");										
						print("<div class='section col-sm-12'>");
							print("<fieldset class='inlineLabels'>");
								
								print("<legend>Address</legend>");
								
								print("<div class='row'>");
									print("<div class='form-group col-md-6 crop-lg-right'>");
										print("<label for='frm_addressnumber'control-label'>House Number</label>");
										print("<input type='text' class='form-control' id='frm_addressnumber' name='frm_addressnumber' value='".$pageDetails['addressNumber']."'>");
									print("</div>");
									print("<div class='form-group col-md-6 crop-lg-left'>");
										print("<label for='frm_addressstreet' class='control-label'>Street Name</label>");
										print("<input type='text' class='form-control' id='frm_addressstreet' name='frm_addressstreet' value='".$pageDetails['addressStreet']."'>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group col-md-6 crop-lg-right'>");
										print("<label for='frm_addresscity' class='control-label'>Town/City</label>");
										print("<input type='text' class='form-control' id='frm_addresscity' name='frm_addresscity' value='".$pageDetails['addressCity']."'>");
									print("</div>");
									print("<div class='form-group col-md-6 crop-lg-left'>");
										print("<label for='frm_addresscounty' class='control-label'>County</label>");
										print("<input type='text' class='form-control' id='frm_addresscounty' name='frm_addresscounty' value='".$pageDetails['addressCounty']."'>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group col-md-6 crop-lg-right'>");
										print("<label for='frm_addresscountry' class='control-label'>Country</label>");
										print("<input type='text' class='form-control' id='frm_addresscountry' name='frm_addresscountry' value='".$pageDetails['addressCountry']."'>");
									print("</div>");
									print("<div class='form-group col-md-6 crop-lg-left'>");
										print("<label for='frm_addresspostcode' class='control-label'>Post Code <i class='fa fa-info-circle pull-right' data-toggle='tooltip'  title='This will set the starting location when providing directions to local attractions.'></i></label>");
										print("<input type='text' class='form-control' id='frm_addresspostcode' name='frm_addresspostcode' value='".$pageDetails['addressPostCode']."'>");
									print("</div>");
								print("</div>");
								
							print("</fieldset>");
						print("</div>");
					print("</div>");
				print("</div>");
				
				print("<div class='row'>");
					print("<div class='col-sm-6 crop-sm-right'>");										
						print("<div class='section col-sm-12'>");
							print("<fieldset class='inlineLabels'>");
								
								print("<legend>Add New Image</legend>");
								
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_newimage' class='control-label'>Image</label>");
										print("<div class='row'>");
											print("<div class='col-xs-9' style='padding-right: 7.5px;'>");
												print("<input type='file' class='form-control' id='frm_newimage' name='frm_newimage' style='padding:6px 12px;'/>");
											print("</div>");
											print("<div class='col-xs-3 text-left' style='padding-left: 7.5px;'>");
												print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success circle pull-left'><i class='fa fa-plus'></i></button>");
											print("</div>");
										print("</div>");
									print("</div>");
								print("</div>");
								
							print("</fieldset>");
						print("</div>");
					print("</div>");
					print("<div class='col-sm-6' style='padding-left:7.5px;'>");										
						print("<div class='section col-sm-12'>");
							print("<fieldset class='inlineLabels'>");
								
								print("<legend>Contact</legend>");
								
								print("<div class='row'>");
									print("<div class='form-group col-md-6 crop-lg-right'>");
										print("<label for='frm_companyphone' class='control-label'>Phone Number</label>");
										print("<input type='text' class='form-control' id='frm_companyphone' name='frm_companyphone' value='".$pageDetails['companyPhone']."'>");
									print("</div>");
									print("<div class='form-group col-md-6 crop-lg-left'>");
										print("<label for='frm_companyemail' class='control-label'>Email <i class='fa fa-info-circle pull-right' data-toggle='tooltip' title='Submitted contact forms will be sent to this email address.'></i></label>");
										print("<input type='text' class='form-control' id='frm_companyemail' name='frm_companyemail' value='".$pageDetails['companyEmail']."'>");
									print("</div>");
								print("</div>");
								
							print("</fieldset>");
						print("</div>");
					print("</div>");
				print("</div>");
				
			/*	print("<div class='row'>");
					print("<div class='col-sm-12'>");
						print("<div class='section col-sm-12'>");
							print("<fieldset class='inlineLabels'>");
							
								print("<legend>Online</legend>");
							
								print("<div class='row'>");
									print("<div class='form-group col-md-6'>");
										print("<label for='frm_addressnumber'control-label'>Property Number</label>");
										print("<input type='text' class='form-control' id='frm_addressnumber' name='frm_addressnumber' value='".$pageDetails['addressNumber']."'>");
									print("</div>");
									print("<div class='form-group col-md-6'>");
										print("<label for='frm_addressstreet' class='control-label'>Street Name</label>");
										print("<input type='text' class='form-control' id='frm_addressstreet' name='frm_addressstreet' value='".$pageDetails['addressStreet']."'>");
									print("</div>");
								print("</div>");
							
							print("</fieldset>");
						print("</div>");
					print("</div>");
				print("</div>");*/
				
				print("<div class='row'>");
					print("<div class='col-sm-6'>");
						//	Blank Space
					print("</div>");
					print("<div class='col-sm-6'>");
						print("<button onclick='return jsupdatePage();' type='submit' class='btn btn-success pull-right'>Save</button> ");
					print("</div>");
				print("</div>");
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#pages-table').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>