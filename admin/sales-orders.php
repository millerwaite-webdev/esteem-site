<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * sales-orders.php				                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

// ************* Common page setup ******************** //
	//=====================================================//
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "sales-orders"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['orderID'])) $orderID = $_REQUEST['orderID']; else $orderID = "";
	
	if($strcmd == "updateOrderStatus")
	{
		$updateOrderStatusQuery = "UPDATE order_header SET orderStatus = :orderStatusID WHERE recordID = :orderID";
		$strType = "update";
		$arrdbparam = array( "orderStatusID" => $_POST['orderStatus'], "orderID" => $orderID );
		$updateOrderStatus = query($conn, $updateOrderStatusQuery, $strType, $arrdbparam);
		
		if (count($updateOrderStatus) <= 1)
		{
			$strsuccess = "Order status successfully updated";
		}
		elseif (count($updateOrderStatus) > 1)
		{
			$strwarning = "Order status updated but errors may have occurred";
		}
		$strcmd = "viewDetails";
	}
	
	switch($strcmd)
	{	
		case "viewDetails":
			
			$orderDetailsQuery = "SELECT order_header.*, customer.recordID AS customerID, customer.title, customer.firstname, customer.surname, order_payment_details.* FROM order_header INNER JOIN customer ON order_header.customerID = customer.recordID INNER JOIN order_payment_details ON order_header.recordID = order_payment_details.orderHeaderID WHERE order_header.recordID = :orderID";
			$strType = "single";
			$arrdbparam = array( "orderID" => $orderID );
			$orderDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparam);
			
			if (count($orderDetails) < 1)
			{
				$strerror = "Matching order not found";
			}
			else
			{
				if(!empty($orderDetails['firstname']) && !empty($orderDetails['surname']))
				{
					$strName = $orderDetails['firstname']." ".$orderDetails['surname'];
					if(!empty($orderDetails['custTitle']))
					{
						$strName = $orderDetails['custTitle']." ".$strName;
					}
				}
			}
			
			$orderItemsDetailsQuery = "SELECT * FROM order_items WHERE orderHeaderID = :orderID";
			$strType = "multi";
			$arrdbparam = array ( "orderID" => $orderID );
			$orderItemsDetails = query($conn, $orderItemsDetailsQuery, $strType, $arrdbparam);
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Sale Orders</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script type='text/javascript'>
				function jsorderHistory(orderID) {
					document.form.orderID.value = orderID;
					document.form.action="sales-orders.php";
					document.form.cmd.value='viewDetails';
					document.form.submit();
				}
				function jsfullDetails(orderID) {
					document.form.orderID.value = orderID;
					document.form.cmd.value='viewDetails';
					document.form.submit();
				}
				function jsupdateOrderStatus() {
					document.form.cmd.value='updateOrderStatus';
					document.form.submit();
				}
				function jscancel(cmdValue) {
					document.form.orderID.value='';
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
			</script>
			<?php
		
			print ("<form action='sales-orders.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				print ("<input type='hidden' name='orderID' id='orderID' value='".$orderID."'/>");
				
				switch($strcmd)
				{
					case "viewDetails":
						print ("<fieldset class='inlineLabels'> <legend>JH Benson and Co. Order Information</legend>
								<div class='form-group'>
									<label class='col-sm-2 control-label'>Transaction Status</label>
									<div class='col-sm-10'>".
										$orderDetails['paymentStatus']
									."</div>
								</div>
								<div class='form-group'>
									<label class='col-sm-2 control-label'>Transaction ID</label>
									<div class='col-sm-10'>".
										$orderDetails['transactionID']
									."</div>
								</div>
								<div class='form-group'>
									<label class='col-sm-2 control-label'>Paid via</label>
									<div class='col-sm-10'>".
										$orderDetails['transactionSource']
									."</div>
								</div>
								<div class='form-group'>
									<label class='col-sm-2 control-label'>Email</label>
									<div class='col-sm-10'>".
										$orderDetails['email']
									."</div>
								</div>
								<div class='form-group'>
									<label class='col-sm-2 control-label'>Order Status</label>
									<div class='col-sm-10'>");
										$getOrderStatusesQuery = "SELECT * FROM order_status";
										$strType = "multi";
										$orderStatuses = query($conn, $getOrderStatusesQuery, $strType);
										print ("<select id='orderStatus' name='orderStatus'>");
										
										foreach($orderStatuses AS $orderStatus)
										{
											$strSelected = "";
											if($orderStatus['recordID'] == $orderDetails['orderStatus'])
											{
												$strSelected = " selected";
											}
											print ("<option value='".$orderStatus['recordID']."'".$strSelected.">".
														$orderStatus['description']
													."</option>");
										}
										
										print ("</select>
									</div>
								</div>
								<div class='form-group'>
									<div class='col-sm-offset-2 col-sm-10'><button onclick='return jsupdateOrderStatus();' type='submit' class='btn btn-success'>Update Order Status</button>
									</div>
								</div>");
								
								print ("<legend>Addresses</legend>");
								print ("<table class='order-list table table-striped table-bordered table-hover table-condensed' >");
									print ("<tbody>");
										print ("<tr>");
											print ("<th>Delivery Address</th>");
										print ("</tr>");
										print ("<tr>");
											print ("<td>");
												print ($orderDetails['addDeliveryName']."<br/>");
												print ($orderDetails['addDelivery1']."<br/>");
												if (!empty($orderDetails['addDelivery2']))
												{
													print ($orderDetails['addDelivery2']."<br/>");
												}
												if (!empty($orderDetails['addDelivery3']))
												{
													print ($orderDetails['addDelivery3']."<br/>");
												}
												if (!empty($orderDetails['addDelivery4']))
												{
													print ($orderDetails['addDelivery4']."<br/>");
												}
												if (!empty($orderDetails['addDelivery5']))
												{
													print ($orderDetails['addDelivery5']."<br/>");
												}
												print ($orderDetails['addDeliveryPostcode']."<br/>");
												if (!empty($orderDetails['addDeliveryMessage']))
												{
													print ($orderDetails['addDeliveryMessage']."<br/>");
												}
											print ("</td>");
										print ("</tr>");
									print ("</tbody>");
								print ("</table>");
								print ("<br/>");
								print ("<table class='order-list table table-striped table-bordered table-hover table-condensed' >");
									print ("<tbody>");
										print ("<tr>");
											print ("<th>Billing Address</th>");
										print ("</tr>");
										print ("<tr>");
											print ("<td>");
												print ($orderDetails['addBillingName']."<br/>");
												print ($orderDetails['addBilling1']."<br/>");
												if (!empty($orderDetails['addBilling2']))
												{
													print ($orderDetails['addBilling2']."<br/>");
												}
												if (!empty($orderDetails['addBilling3']))
												{
													print ($orderDetails['addBilling3']."<br/>");
												}
												if (!empty($orderDetails['addBilling4']))
												{
													print ($orderDetails['addBilling4']."<br/>");
												}
												if (!empty($orderDetails['addBilling5']))
												{
													print ($orderDetails['addBilling5']."<br/>");
												}
												print ($orderDetails['addBillingPostcode']."<br/>");
											print ("</td>");
										print ("</tr>");
									print ("</tbody>");
								print ("</table>");
								
								print ("<br/>");
								print ("<legend>Order Summary</legend>");
								print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
									print ("<thead><tr>");
										print ("<th>Stockcode</th>");
										print ("<th>Name</th>");
										print ("<th>Size</th>");
										print ("<th>Colour</th>");
										print ("<th>Quantity</th>");
										print ("<th>Price</th>");
									print ("</tr></thead><tbody>");
									
									$totalQuantity = 0;
									foreach ($orderItemsDetails AS $orderItem)
									{
										$orderItemDetails = unserialize($orderItem['stockDetailsArray']);
										
										if(empty($orderItemDetails['name']))
										{
											$getOrderItemName = "SELECT stock_group_information.name FROM stock_group_information INNER JOIN stock ON stock_group_information.recordID = stock.groupID WHERE stock.stockCode = :stockCode";
											$strType = "single";
											$arrdbparam = array("stockCode" => $orderItemDetails['stockCode']);
											$orderItemName = query($conn, $getOrderItemName, $strType, $arrdbparam);
											$orderItemDetails['name'] = $orderItemName['name'];
										}
										
										print ("<tr>");
											print ("<td>".$orderItemDetails['stockCode']."</td>");
											print ("<td>".$orderItemDetails['name']."</td>");
											print ("<td>".$orderItemDetails['deliverySize']."</td>");
											print ("<td>".$orderItemDetails['colour']."</td>");
											print ("<td>".$orderItem['quantity']."</td>");
											print ("<td>&pound;".$orderItem['price']."</td>");
										print ("</tr>");
										$totalQuantity += $orderItem['quantity'];
									}
									print ("</tbody>
											<tfoot>
												<tr>
													<th colspan='4'>Subtotal</th>
													<th>".$totalQuantity." Items</th>
													<th>&pound;".$orderDetails['amountStock']."</th>
												</tr>
											</tfoot>
										</table>");
									
								print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
									print ("<thead><tr>");
										print ("<th>Item</th>");
										print ("<th>Cost</th>");
									print ("</tr></thead><tbody>");
									print ("<tr>
												<td>Goods</td>
												<td>&pound;".$orderDetails['amountStock']."</td>
											</tr>
											<tr>
												<td>Standard Delivery</td>
												<td>&pound;".$orderDetails['amountDelivery']."</td>
											</tr><tr>
												<td>VAT @ ".$orderDetails['vatRate']."</td>
												<td>&pound;".$orderDetails['amountVat']."</td>
											</tr>");
									print ("</tbody>
											<tfoot>
												<tr>
													<th>Total</th>
													<th>&pound;".$orderDetails['amountTotal']."</th>
												</tr>
											</tfoot>
										</table>");	
										
								print ("<div class='form-group'>
											<div class='col-sm-10'>
												<button onclick='return jscancel(\"\")' class='btn btn-primary' type='submit'>Return to List</button>
											</div>
										</div>
							</fieldset>");
						break;
						
					default:
						$orderDetailsQuery = "SELECT order_header.*, customer.recordID AS customerID, customer.title, customer.firstname, customer.surname, order_payment_details.*, order_status.description AS orderStatus FROM order_header INNER JOIN customer ON order_header.customerID = customer.recordID INNER JOIN order_payment_details ON order_header.recordID = order_payment_details.orderHeaderID INNER JOIN order_status ON order_header.orderStatus = order_status.recordID";
						$strType = "multi";
						$arrdbparam = array();
						
						if (!empty($_REQUEST['userID']))
						{
							$orderDetailsQuery .= " AND order_header.customerID = :customerID";
							$arrdbparam['customerID'] = $_REQUEST['userID'];
						}
						$ordersDetails = query($conn, $orderDetailsQuery, $strType, $arrdbparam);
						
						if (!empty($_REQUEST['userID']))
						{
							print ("<h3>Showing orders for ".$orderDetails[0]['firstname']." ".$orderDetails[0]['surname']."</h3>");
						}
						
						print ("<table class='order-list table table-striped table-bordered table-hover table-condensed' >");
							print ("<thead><tr>");
								print ("<th>Order Number</th>");
								print ("<th>Full Name</th>");
								print ("<th>Email Address</th>");
								print ("<th>Transaction Number</th>");
								print ("<th>Transaction ID</th>");
								print ("<th>Transaction Date</th>");
								print ("<th>Transaction Amount</th>");
								print ("<th>Transaction Status</th>");
								print ("<th>Order Status</th>");
								print ("<th>Details</th>");
							print ("</tr></thead><tbody>");
							foreach($ordersDetails AS $orderDetails)
							{
								print ("<tr>
											<td>".$orderDetails['recordID']."</td>
											<td>".$orderDetails['addDeliveryName']."</td>
											<td><a href='mailto:".$orderDetails['email']."'>".$orderDetails['email']."</a></td>
											<td>".$orderDetails['transactionID']."</td>
											<td>".$orderDetails['transactionSource']."</td>");
									print ("<td>".date("d/m/Y H:i:s", $orderDetails['transactionTimestamp'])."</td>
											<td>&pound;".$orderDetails['transactionAmount']."</td>
											<td>".$orderDetails['paymentStatus']."</td>
											<td>".$orderDetails['orderStatus']."</td>
											<td><button onclick='return jsfullDetails(\"".$orderDetails['recordID']."\")' class='btn btn-primary' type='submit'>Full Order Details</button></td>
									</tr>");
									
							}
							print ("</tbody>");
						print ("</table>");
						
						break;
				}
				
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script type='text/javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('.order-list').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>