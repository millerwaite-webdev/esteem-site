<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * cms-attractions.php					                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

//	use function inc\connect;
//use function inc\query;

session_start(); //stores session variables such as access levels and logon details
	$strpage = "cms-attractions"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['pageRecordID'])) $pageRecordID = $_REQUEST['pageRecordID']; else $pageRecordID = "";
	
	switch($strcmd)
	{
		case "insertPage":
		case "updatePage":
			
			$arrdbparams = array(
								"localTitle" => $_POST['frm_title'],
								"localDescription" => $_POST['frm_description'],
								"localImage" => $_POST['frm_image'],
								"localLocation" => $_POST['frm_postcode']
							);
			
			if ($strcmd == "insertPage")
			{	
				$strdbsql = "INSERT INTO site_local_attractions (localTitle, localDescription, localLocation, localImage) 
							VALUES (:localTitle, :localDescription, :localLocation, :localImage)";
				$strType = "insert";
			}
			elseif ($strcmd == "updatePage")
			{
				$strdbsql = "UPDATE site_local_attractions SET localTitle = :localTitle, localDescription = :localDescription, localLocation = :localLocation, localImage = :localImage WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $pageRecordID;
				$strType = "update";
			}
			
			$updatePage = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertPage")
			{
				$pageRecordID = $updatePage;
			}
			
			if ($strcmd == "insertPage")
			{
				if ($updatePage > 0)
				{
					$strsuccess = "Page successfully added";
				}
				elseif ($updatePage == 0)
				{
					$strerror = "An error occurred while adding the brand";
				}
			}
			elseif ($strcmd == "updatePage")
			{
				if ($updatePage <= 1)
				{
					$strsuccess = "Page successfully updated";
				}
				elseif ($updatePage > 1)
				{
					$strwarning = "An error may have occurred while updating this page";
				}
			}
			
			$strcmd = "viewPage";
			
		break;
		
		case "deletePage":
			
			$strdbsql = "DELETE FROM site_local_attractions WHERE recordID = :pageRecordID";
			$strType = "delete";
			$arrdbparams = array( "pageRecordID" => $pageRecordID );
			$deletePage = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strcmd = "";
			
		break;
		
		case "addSiteBlock":
			
			$strdbsql = "SELECT COUNT(*) AS max_order FROM site_block_relations WHERE pageID = :pageRecordID AND positionID = :positionID";
			$strType = "single";
			$arrdbparams = array(
								"pageRecordID" => $pageRecordID,
								"positionID" => $_POST['frm_siteblockposition']
							);
			$maxOrder = query($conn, $strdbsql, $strType, $arrdbparams);
			$order = $maxOrder['max_order'] + 1;
			
			foreach($_POST['frm_siteblocks'] AS $siteBlock)
			{
				$strdbsql = "INSERT INTO site_block_relations (blockID, positionID, pageID, pageOrder) VALUES (:blockID, :positionID, :pageRecordID, :pageOrder)";
				$strType = "insert";
				$arrdbparams = array( 
									"blockID" => $siteBlock,
									"positionID" => $_POST['frm_siteblockposition'],
									"pageRecordID" => $pageRecordID,
									"pageOrder" => $order
								);
				$insertBlock = query($conn, $strdbsql, $strType, $arrdbparams);
				$order++;
			}
			
			$strcmd = "viewPage";
			
		break;
		
		case "deleteBlocks":
			
			$strdbsql = "DELETE FROM site_block_relations WHERE recordID IN (".$_POST['deleteBlocks'].")";
			$strType = "delete";
			$removeImages = query($conn, $strdbsql, $strType);
			
			$strdbsql = "SELECT recordID FROM site_block_relations WHERE pageID = :pageRecordID AND positionID = :positionID ORDER BY pageOrder";
			$strType = "multi";
			$arrdbparams = array(
							"pageRecordID" => $pageRecordID,
							"positionID" => $_POST['blockPositionID']
						);
			//echo "get products\n";
			$pageBlocks = query($conn, $strdbsql, $strType, $arrdbparams);
			//var_dump($categories);
			
			$i = 1;
			foreach($pageBlocks AS $pageBlock)
			{
				$strdbsql = "UPDATE site_block_relations SET pageOrder = ".$i." WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparam = array(
								"recordID" => $pageBlock['recordID']
							);
				//echo "update final ordering\n";
				query($conn, $strdbsql, $strType, $arrdbparam);
				$i++;
			}
			
			$strcmd = "viewPage";
			
		break;
		
		case "insertImage":
			$newfilename = $_FILES['frm_newimage']['name'];
			$strfileextn = getExtension($newfilename);
			
			$strimguploadpath = "images/";
			$struploaddir = $strrootpath.$strimguploadpath; // where to put full size image
			$upfile = $struploaddir.$newfilename;
			
			//        print("$upfile - saving image<br>");

			if ($_FILES['frm_newimage']['error'] > 0)
			{
				switch ($_FILES['frm_newimage']['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				// Does the file have the right extension ?
				//if ($strfileextn != 'jpg' && $strfileextn != 'jpeg' && $strfileextn != 'png' && $strfileextn != 'gif')
				
				if (strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0)
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_newimage']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
				}
				else
				{
					//      print("Uploading to products image directory<br>");
					if (is_uploaded_file($_FILES['frm_newimage']['tmp_name']))
					{
						if (!move_uploaded_file($_FILES['frm_newimage']['tmp_name'], $upfile))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_newimage']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_newimage']['name'].". Please try again.<br/>";
					}
				}
			}
			
			if ($strerror != "")
			{
				$strcommand = "ERRORMSG";
				//   print("Got an Error - $strcommand - $strerror<br>");
			}
			else
			{
			
				$strpagepath = $strimguploadpath."local-attractions/";
				
				$strpagedir = $strrootpath.$strpagepath;
				
				$newfile = $strpagedir.$newfilename;
				$range=copy($upfile,$newfile);
				chmod ($newfile, 0777);
				
				
				unlink($upfile);
				
				$updatePageImageQuery = "UPDATE site_local_attractions SET localImage = :image WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparams = array (
								"image" => $newfilename,
								"recordID" => $pageRecordID
							);
				$updatePageImage = query($conn, $updatePageImageQuery, $strType, $arrdbparams);
				
				$strsuccess = "New page image added";
			}
			
			$strcmd = "viewPage";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Local Attractions Control</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddPage() {
					document.form.cmd.value="addPage";
					document.form.submit();
				}
				function jsviewPage(pageRecordID) {
					document.form.cmd.value="viewPage";
					document.form.pageRecordID.value=pageRecordID;
					document.form.submit();
				}
				function jsdeletePage(pageRecordID) {
					if(confirm("Are you sure you want to delete this page?"))
					{
						document.form.cmd.value="deletePage";
						document.form.pageRecordID.value=pageRecordID;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertPage() {
					document.form.cmd.value='insertPage';
					document.form.submit();
				}
				function jsupdatePage() {
					if ($('#form').valid()) {
						document.form.cmd.value='updatePage';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertImage() {
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jsaddSiteBlock() {
					if ($('#form').valid()) {
						document.form.cmd.value='addSiteBlock';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsdeleteSiteBlocks() {
					if ($(".blockPosList li.selected").length > 0)
					{
						if(confirm("Are you sure you want to delete this block(s)?"))
						{
							var deleteBlocks = "";
							$(".blockPosList li.selected").each(function (index, item) {
								deleteBlocks += $(item).attr("id")+",";
							});
							$("#blockPositionID").val($(".blockPosList li.selected").attr("data-pos"));
							//console.log($(".blockPosList li.selected").parent().attr("id"));
							deleteBlocks = deleteBlocks.replace(/,\s*$/, "");
							//console.log(deleteImages);
							document.form.cmd.value="deleteBlocks";
							document.form.deleteBlocks.value=deleteBlocks;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the block(s) you wish to delete");
					}
					
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				
				$().ready(function() {
					$(".blockPosList").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
						$(this).parent().parent().siblings().children("ul").children().removeClass('selected');
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							//console.log(item);
							
							//Clone the selected items into an array
							//console.log(item.parent().children('.selected'));
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								//console.log($(item));
								//console.log($(item).attr('data-order'));
								var oldOrder = $(item).attr("data-order");
								
								$(item).parent().children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if (parseInt(newOrder) > parseInt(oldOrder))
								{
									sortType = "DESC";
								}
								else if (parseInt(newOrder) < parseInt(oldOrder))
								{
									sortType = "ASC";
								}
								
								dataArray[i] = {};
								dataArray[i]["pageOrder"] = $(item).attr("data-order");
								dataArray[i]["positionID"] = $(item).attr("data-pos");
								dataArray[i]["recordID"] = $(item).attr("id");
								dataArray[i]["sitePageID"] = $("#pageRecordID").attr("value");
								dataArray[i]["type"] = "page";
								dataArray[i]["cmd"] = "reorder";
								//console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									//console.log("sort ascending");
									return parseInt(a.pageOrder) - parseInt(b.pageOrder);
								}
								else if (sortType == "DESC")
								{
									//console.log("sort descending");
									return parseInt(b.pageOrder) - parseInt(a.pageOrder);
								}
							});
							//console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_siteblockrelationmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
			</script>
			<?php
		
			print("<form action='cms-attractions.php' class='uniForm col-sm-12' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='pageRecordID' id='pageRecordID' value='$pageRecordID'/>");
				print("<input type='hidden' name='blockPositionID' id='blockPositionID' />");
				
				switch($strcmd)
				{
					case "viewPage":
					case "addPage":
						
						if ($strcmd == "viewPage")
						{
							$strdbsql = "SELECT * FROM site_local_attractions WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $pageRecordID);
							$pageDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							/*print("<pre>");
							print_r($pageDetails);
							print("</pre>");*/
							
							print("<div class='row'>");
								print("<div class='col-md-6'>");
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
										
											print("<legend>Attraction</legend>");
						}
						elseif ($strcmd == "addPage")
						{
							$pageDetails = array(
												"localTitle" => "",
												"localDescription" => "",
												"localLocation" => "",
												"localImage" => ""
											);
							
							print("<div class='row'>");
								print("<div class='col-md-6'>");
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
										
											print("<legend>Attraction</legend>");
						}
						
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_title' class='control-label'>Local Attraction</label>");
													print("<input type='text' class='form-control' id='frm_title' name='frm_title' value='".$pageDetails['localTitle']."'>");
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_description' class='control-label'>Description</label>");
													print("<textarea class='form-control' id='frm_description' name='frm_description' rows='3'>".$pageDetails['localDescription']."</textarea>");
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group col-md-6 crop-lg-right'>");
													print("<label for='frm_image' class='control-label'>Image</label>");
													print("<select id='frm_image' name='frm_image' class='form-control' >");
														print("<option value=''>None</option>");
													
														$dh = opendir("../images/local-attractions/");
														
														while (false !== ($image = readdir($dh))) {
															if ($image != "." && $image != "..") {
																if($image == $pageDetails['localImage'])
																{
																	$strselected = " selected";
																}
																else
																{
																	$strselected = "";
																}
														
																print("<option value='".$image."'".$strselected.">".$image."</option>");
														
															}
														}
													
													print("</select>");
												print("</div>");
												
												print("<div class='form-group col-md-6 crop-lg-left'>");
													print("<label for='frm_postcode' class='control-label'>Post Code</label>");
													print("<input type='text' class='form-control' id='frm_postcode' name='frm_postcode' value='".$pageDetails['localLocation']."'>");
												print("</div>");
											print("</div>");
										
										print("</fieldset>");
									print("</div>");
								
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");

											print("<legend>Add New Image</legend>");
									
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_newimage' class='control-label'>Image</label>");
													print("<div class='row'>");
														print("<div class='col-xs-9' style='padding-right:7.5px;'>");
															print("<input type='file' class='form-control' id='frm_newimage' name='frm_newimage' style='padding:6px 12px;'/>");
														print("</div>");
														print("<div class='col-xs-3' style='padding-left:7.5px;'>");
															print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success circle pull-left'><i class='fa fa-plus'></i></button>");
														print("</div>");
													print("</div>");
												print("</div>");
											print("</div>");
											
										print("</fieldset>");
									print("</div>");
									
								print("</div>");
							print("</div>");
							
							print("<div class='row'>");
								print("<div class='col-md-6'>");
								
									print("<button onclick='return jscancel(\"\");' class='btn btn-danger pull-left'>Cancel</button>");
								
									if ($strcmd == "addPage") {
										print("<button onclick='return jsinsertPage();' type='submit' class='btn btn-success pull-right'>Create</button> ");
									}
									elseif ($strcmd == "viewPage") {
										print("<button onclick='return jsupdatePage();' type='submit' class='btn btn-success pull-right'>Save</button> ");
									}
									
								print("</div>");
							print("</div>");
					
					break;
						
					default:
						
						$strdbsql = "SELECT * FROM site_local_attractions";
						$strType = "multi";
						$attractions = query($conn, $strdbsql, $strType);
						
						print("<div class='section'>");
							print("<table id='pages-table' class='table table-striped table-bordered table-hover table-condensed' >");
								print("<thead><tr>");
									print("<th width='15%'>Local Attraction</th>");
									print("<th width='65%' class='media-out'>Description</th>");
									print("<th width='10%' style='text-align:center;'>Update</th>");
									print("<th width='10%' style='text-align:center;'>Delete</th>");
								print("</tr></thead><tbody>");
								foreach($attractions AS $attraction)
								{							
									print("<tr>");
										print("<td>".$attraction['localTitle']."</td>");
										print("<td class='media-out'>".$attraction['localDescription']."</td>");
										print("<td style='text-align:center;'><button onclick='return jsviewPage(\"".$attraction['recordID']."\");' type='submit' class='btn btn-primary circle' style='display:inline-block;'><i class='fa fa-pencil'></i></button></td>");
										print("<td style='text-align:center;'><button onclick='return jsdeletePage(\"".$attraction['recordID']."\");' type='submit' class='btn btn-danger circle' style='display:inline-block;'><i class='fa fa-trash'></i></button></td>");
									print("</tr>");
								}
								print("</tbody>");
							print("</table>");
						print("</div>");
						
						print("<div class='buttons' style='text-align:right;'>");
							print("<button onclick='return jsaddPage();' type='submit' class='btn btn-success' style='display:inline-block;'>Add</button>");
						print("</div>");
							
						break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#pages-table').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>