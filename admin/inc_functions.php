<?php
	
	// STANDARD FUNCTIONS
	// ================
	
	//test if operator has permission to view this page
	function pagePermission($currentpage,$conn) {
		
		$location = "";
		if ($currentpage == "") {
			$currentpage = curPageName(); //this is a function that is near the bottom of this page. It gets the pagename of the current page
		}

		$strdbsql = "SELECT * FROM admin_pages WHERE link=:pagename";
		$arrType = "single";
		$strdbparams = array("pagename" => $currentpage);
		$resultdata = query($conn, $strdbsql, $arrType, $strdbparams); 
		
		//if the pagename cannot be found in the database then bounce back to the index or login pages
		if ($resultdata != null) {
			if (isset($resultdata['permissionGroup'])) $permGroup = $resultdata['permissionGroup']; else $permGroup = "";
			$pagePermissionGroup = permissionGroup($permGroup,$conn);
			$operatorPermissionGroup = permissionGroup($_SESSION['permissionGroup'],$conn);
			
			//checks to see if the user has permission to view this page
			if ($pagePermissionGroup > $operatorPermissionGroup) {
				if ($currentpage != 'index' AND $currentpage != 'login') {
					$location = ("Location: /admin/index.php?error=You do not have permission to access this page"); 
				} else {
					$location = ("Location: /admin/login.php"); 
				}
			}


		} else {
			if ($currentpage != 'index') {
				$location = ("Location: /admin/index.php?error=1");
			} else {
				$location = ("Location: /admin/login.php?error=1");
			}
		}
		return $location;
	}
		
	function permissionGroup($permissionGroupID,$conn) {
		$strdbsql = "SELECT * FROM admin_permission_groups WHERE groupID=:groupID";
		$arrType = "single";
		$strdbparams = array("groupID" => $permissionGroupID);
		$resultdata = query($conn, $strdbsql, $arrType, $strdbparams); 
		return $resultdata;
	}

	// Written by Carl on 14 April 2014 - Called from config-letters.php
	function stripCharactersFilename($filename){
		$filename = preg_replace("([^\w\s\d\-_~,;:\[\]\(\].]|[\.]{2,})", '', $filename);
		return $filename;
	}
	
	/**
	 * Encodes HTML safely for UTF-8. Use instead of htmlentities.
	 *
	 * @param string $var
	 * @return string
	 */
	function html_encode($var)
	{
		return htmlentities($var, ENT_QUOTES, 'UTF-8');
	}
	
 	//Re-Arranges the array when multiple files are imported
	function reArrayFiles( $arr ){foreach( $arr as $key => $all ){foreach( $all as $i => $val ){$new[$i][$key] = $val;}} return $new; }
	
	//function to loop though a XML document and create a CSV
	function createCsv($xml,$f){foreach ($xml->children() as $item) { $hasChild = (count($item->children()) > 0)?true:false; if( ! $hasChild){$put_arr = array($item->getName(),$item); fputcsv($f, $put_arr ,',','"');} else {createCsv($item, $f);}}}
	
	// Encrypts Data and returns encrypted string - if key not available, returns blank
	function fn_encrypt($strdata,$iv) {
		$iv = substr("3185296328795229".$iv, -16);
		$strfullkey = $_SESSION['mwldecrypt'];
		$strpartkey = "Gmj*cfd3";

		$strfirstdata = substr($strdata,0,-4);
		$strseconddata = substr($strdata,-4);

		if ($strfullkey == "") { 
			$strreturn = "-x-";
		} else {
			$strreturn = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $strfullkey, $strfirstdata, MCRYPT_MODE_CBC, $iv)."####".mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $strpartkey, $strseconddata, MCRYPT_MODE_CBC, $iv);
		}

		return $strreturn;
	}

	// Decrypt full string
	function fn_decrypt($strdata,$iv) {
		$iv = substr("3185296328795229".$iv, -16);
		$strfullkey = $_SESSION['mwldecrypt'];
		$strpartkey = "Gmj*cfd3";

		$intlocation = strpos($strdata, "####");
		$strfirstdata = substr($strdata,0,$intlocation);
		$strseconddata = substr($strdata,$intlocation+4);

		if ($strfullkey == "") { 
			$strreturn = "xxxx".mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $strpartkey, $strseconddata, MCRYPT_MODE_CBC, $iv);
			$intlocation = strpos($strreturn, chr(0));
			$strreturn = substr($strreturn,0,$intlocation);
		} else {
			$strreturn = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $strfullkey, $strfirstdata, MCRYPT_MODE_CBC, $iv);
			$intlocation = strpos($strreturn, chr(0));
			$strreturn = substr($strreturn,0,$intlocation);
			$strreturn .= mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $strpartkey, $strseconddata, MCRYPT_MODE_CBC, $iv);
			$intlocation = strpos($strreturn, chr(0));
			$strreturn = substr($strreturn,0,$intlocation);
		}

		return $strreturn;
	}

	// Decrypt part string
	function fn_partdecrypt($strdata,$iv) {
		$iv = substr("3185296328795229".$iv, -16);
		$strpartkey = "Gmj*cfd3";

		$intlocation = strpos($strdata, "####");
		if ($intlocation < 1) {
			$strreturn = "";
		} else {
			$strseconddata = substr($strdata,$intlocation+4);
			$strreturn = "xxxx".mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $strpartkey, $strseconddata, MCRYPT_MODE_CBC, $iv);
		}

		$intlocation = strpos($strreturn, chr(0));
		$strreturn = substr($strreturn,0,$intlocation);

		return $strreturn;
	}

	//function which takes a string and a number and returns string number characters long - either truncated or padded with spaces
	function fn_fixedlength($strstring,$intlength,$intpaddirection=STR_PAD_RIGHT,$strpadchr=" ",$strquote="") {
		$strstring = str_pad($strstring, $intlength, $strpadchr, $intpaddirection);
		$strstring = substr($strstring, 0, $intlength);
		return $strquote.$strstring.$strquote;
	}

	function fn_delimited($strstring,$intmin,$intmax,$inttype,$strdelimiter,$intnumberFields,$intthisfield,$intpaddirection=STR_PAD_RIGHT,$strpadchr=" ",$strquote="",$strprefixchr="") {
		if ($strprefixchr == "~") { $strprefixchr = "£"; }
		switch ($inttype) {
			case "0":
				// fixed
				$strstring = fn_fixedlength($strprefixchr.$strstring,$intmax, $intpaddirection, $strpadchr, $strquote);
				break;
			case "1":
			case "2":
				// Delimited
				$strstring = str_pad($strprefixchr.$strstring, $intmin, $strpadchr, $intpaddirection);
				$strstring = substr($strstring, 0, $intmax);
				$strstring = $strquote.$strstring.$strquote;
				if ($intthisfield < $intnumberFields) { $strstring .= $strdelimiter; }
				break;
		}
		return $strstring;
	}

	// Email Function - easy function that sets up the headers and such for emails, it will return an array to say if successful or error.
	function sendemail($from, $fromemail, $replyto, $EmailTo, $subject, $textmessage, $htmlmessage, $useTemplate, $sendtoself) {
		 
		// Create the mail transport configuration
		$transport = Swift_MailTransport::newInstance();
		 
		// Create the message
		$message = Swift_Message::newInstance();
		$message->setTo(array(
		  "$EmailTo" => "$EmailTo"
		));
		$message->setSubject("$subject");
		$message->setBody("$textmessage");
		if ($useTemplate == 1) {
			$message->addPart(email_template($htmlmessage, $subject), 'text/html');
		} else {
			$message->addPart("$htmlmessage", 'text/html');
		}
		$message->setFrom("$fromemail", "$from");

		// Send the email
		$mailer = Swift_Mailer::newInstance($transport);
		if ($mailer->send($message)){
			if ($sendtoself) mail($fromemail, $Subject, $Body, $headers);	
			$strmessage["success"] = 'Email successfully sent.';
			return $strmessage;
		} else {
			$strmessage["error"] = 'Email could not be sent. Please try again';
			return $strmessage;
		}
	}
	
	function email_template ($content, $subject){
	
		global $companyName, $companyPhone, $companyEmail;
	
		$template = ("<!doctype html>
			<html xmlns='http://www.w3.org/1999/xhtml'>
			<head>

			<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

			<title>$companyName</title>
																																																																																																					
			<style type='text/css'>
				.ReadMsgBody {width: 100%; background-color: #ffffff;}
				.ExternalClass {width: 100%; background-color: #ffffff;}
				body	 {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Georgia, Times, serif}
				table {border-collapse: collapse;}
				
				@media only screen and (max-width: 640px)  {
								body[yahoo] .deviceWidth {width:440px!important; padding:0;}	
								body[yahoo] .center {text-align: center!important;}	 
						}
						
				@media only screen and (max-width: 479px) {
								body[yahoo] .deviceWidth {width:280px!important; padding:0;}	
								body[yahoo] .center {text-align: center!important;}	 
						}

			</style>
			</head>

			<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0' yahoo='fix' style='font-family: Georgia, Times, serif'>

			<!-- Wrapper -->
			<table width='640px' border='0' cellpadding='0' cellspacing='0' align='center'>
				<tr>
					<td width='100%' valign='top' bgcolor='#ffffff' style='padding-top:20px'>
					
						<!-- Start Header-->
						<table width='580' border='0' cellpadding='0' cellspacing='0' align='center' class='deviceWidth'>
							<tr>
								<td width='100%' bgcolor='#ffffff'>

										<!-- Logo -->
										<table border='0' cellpadding='0' cellspacing='0' align='left' class='deviceWidth'>
											<tr>
												<td style='padding:10px 20px' class='center'>
													<a href='#'><img src='' alt='' border='0' /></a>
												</td>
											</tr>
										</table><!-- End Logo -->
										
										<!-- Nav -->
										<table border='0' cellpadding='0' cellspacing='0' align='right' class='deviceWidth'>
											<tr>
												<td class='center' style='font-size: 13px; color: #272727; font-weight: light; text-align: right; font-family: Georgia, Times, serif; line-height: 20px; vertical-align: middle; padding:10px 20px; font-style:italic'>
													<p href='#' style='text-decoration: none; color: #3b3b3b;'></p>
												</td>
											</tr>
										</table><!-- End Nav -->

								</td>
							</tr>
						</table><!-- End Header -->
						
						<!-- One Column -->
						<table width='580'  class='deviceWidth' border='0' cellpadding='0' cellspacing='0' align='center' bgcolor='#eeeeed'>
							<tr>
								<td style='font-size: 13px; color: #959595; font-weight: normal; text-align: left; font-family: Georgia, Times, serif; line-height: 24px; vertical-align: top; padding:20px' bgcolor='#eeeeed'>
									
									<table>
										<tr>
											<td valign='middle' style='padding:0 10px 10px 0'><a href='#' style='text-decoration: none; color: #272727; font-size: 16px; color: #272727; font-weight: bold; font-family:Arial, sans-serif '>$subject</a>
											</td>
										</tr>
									</table>

									$content
								</td>
							</tr>              
						</table><!-- End One Column -->

						<!-- 4 Columns -->
						<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>
							<tr>
								<td bgcolor='#363636' style='padding:20px;'>
									<table width='540' border='0' cellpadding='0' cellspacing='0' align='center' class='deviceWidth'>
										<tr>
											<td>                    
												<table width='45%' cellpadding='0' cellspacing='0'  border='0' align='left' class='deviceWidth'>
													<tr>
														<td valign='top' style='font-size: 11px; color: #f1f1f1; color:#999; font-family: Arial, sans-serif; padding-bottom:20px' class='center'>
															<span style='color: #999999; font-size: 24px; '></span><br/>
															<b style='color: #5b5b5b;'></b><br/><br/>
														</td>
													</tr>
												</table>
								
												<table width='40%' cellpadding='0' cellspacing='0'  border='0' align='right' class='deviceWidth'>
													<tr>
														<td valign='top' style='font-size: 11px; color: #f1f1f1; font-weight: normal; font-family: Georgia, Times, serif; line-height: 26px; vertical-align: top; text-align:right' class='center'>


															<a href='#' style='text-decoration: none; color: #848484; font-weight: normal;'>$companyPhone</a><br/>
															<a href='#' style='text-decoration: none; color: #848484; font-weight: normal;'>$companyEmail</a>
														</td>
													</tr>
												</table>
											</td>
										</tr>

									</table>                                                              		
								</td>
							</tr>
						</table><!-- End 4 Columns -->
									
					</td>
				</tr>
			</table> <!-- End Wrapper -->
			</body>
			</html>");
		return $template;
	}
	
	//Function to get a random character/s from the string that is passed - used for login memorable information
	function getRandChars ($numPositions = 1, $string = 'xxxxxxxx') {
		$positionArray = array();
		for ($i=0; $i < $numPositions; $i++) {
			$position = rand(1, strlen($string));
			if (in_array($position, $positionArray)) {
				$i--;
			} else {
				$positionArray[$i] = $position;
			}
		}
		return $positionArray;
	}
	
	//Function to represent a number like '2nd', '10th', '101st' etc
	function text_number($n){
		$teen_array = array(11, 12, 13, 14, 15, 16, 17, 18, 19);
	    $single_array = array(1 => 'st', 2 => 'nd', 3 => 'rd', 4 => 'th', 5 => 'th', 6 => 'th', 7 => 'th', 8 => 'th', 9 => 'th', 0 => 'th');
		$if_teen = substr($n, -2, 2);
	    $single = substr($n, -1, 1);
	    if ( in_array($if_teen, $teen_array) ){
			$new_n = $n . 'th';
		} else if ( $single_array[$single] ){
			$new_n = $n . $single_array[$single];   
		}
		return $new_n;
	}
	
	function remote_file_exists($path){
		return (@fopen($path,"r")==true);
	}
	
	//Same as strlength except stops at fullstop
	function abbr($str, $maxLen) {
		if (strlen($str) > $maxLen && $maxLen > 1) {
			preg_match("#^.{1,".$maxLen."}\.#s", $str, $matches);
			return $matches[0];
		} else {
			return $str;
		}
	}
	
	function imageupload($location, $name, $inputname) {
		
		//upload image to images folder
		$strerror = "";
		$arrallowlist = array("jpg", "jpeg", "png", "gif");
		$newfilename = strtolower($_FILES[$inputname]['name']);
		$strfileextn = trim(strtolower(substr($newfilename,-4)),".");
	
		$filename = "/files/images/".$location;
		//create directory if it does not currently exist
		if (!file_exists($filename)){
			$makedir = recursive_mkdir($filename);
		}
		if ($makedir) {
			$filename = $_SERVER['DOCUMENT_ROOT'].$filename.$name.".".$strfileextn;

			if ($_FILES[$inputname]['error'] > 0)
			{
				switch ($_FILES[$inputname]['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				// Does the file have the right extension
				if (!in_array($strfileextn,$arrallowlist))
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br>You may only upload image files in jpeg, gif or png format.<br><br>The file you are trying to upload is a ".$_FILES[$inputname]['type']." file.<br><br>Please convert the picture to the required format and try again.";
				}
				else
				{
					if (is_uploaded_file($_FILES[$inputname]['tmp_name']))
					{
						if (!move_uploaded_file($_FILES[$inputname]['tmp_name'], $filename))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES[$inputname]['tmp_name']." and could not move file to destination directory ($filename). Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES[$inputname]['name'].". Please try again.<br>";
					}
				}
			}
		} else {
			$strerror = "Problem: Could not find or create the directory";
		}
		
		$imagedetails['location'] = $filename;
		$imagedetails['extension'] = $strfileextn;
		$imagedetails['error'] = $strerror;
		return $imagedetails;
	}
	
	function curPageName() {
		return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
	}
	
	//creates a pdf of the html content sent to it
	function pdfprint ($html,$strpdforientation) {
		
		// Include the main TCPDF library (search for installation path).
		require_once('tcpdf_include.php');

		// create new PDF document
		$pdf = new TCPDF($strpdforientation, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Author');
		$pdf->SetTitle('Test');
		$pdf->SetSubject('Testing');

		// turn headers and footers off
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(5, 15, 5);
		
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page
		$pdf->AddPage();
		
		$css = ("<!-- EXAMPLE OF CSS STYLE -->
		<style>
		table {
			border: 0 none;
			border-collapse: collapse;
			padding: 0;
		}
		th {
			border-bottom: 1px solid #E1E1E1;
			font-size: 10px;
			font-weight: normal;
			padding: 5px;
		}
		table.tabTable {
			background: none repeat scroll 0 0 #FFFFFF;
			border-left: 1px solid #E1E1E1;
			border-top: 1px solid #E1E1E1;
			margin: 14px 0 5px;
		}
		table.tabTable th {
			background: none repeat scroll 0 0 #FCFCFC;
			border-bottom: 1px solid #E1E1E1;
			border-right: 1px solid #E1E1E1;
			font-size: 10px;
			font-weight: normal;
			padding: 5px;
		}
		table.tabTable th a {
			font-weight: bold;
			line-height: 20px;
			text-decoration: none;
		}
		table.tabTable td {
			border-bottom: 1px solid #E1E1E1;
			border-right: 1px solid #E1E1E1;
			padding: 5px;
		}
		table.tabTable td .listingDescription {
			font-size: 12px;
			line-height: 17px;
			padding: 5px 0;
		}
		table.tabTable td a {
			line-height: 20px;
			text-decoration: none;
		}</style>");
		
		$html = $css.$html;
		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		// reset pointer to the last page
		$pdf->lastPage();

		// ---------------------------------------------------------
		ob_clean();
		//Close and output PDF document
		$pdf->Output($documentname.".pdf", 'I');
	}
	
	//logs the user out of the system. The function is called when someone goes to ?logout - the code which does this is up above.
	function logout() {
		session_destroy();
		header ("Location: /admin/login.php");
	}
	
	//format a name 
	function FormatName($name=NULL){
		if (empty($name))
		return false;

		$name = strtolower($name);
		$name = preg_replace("[\-]", " - ",$name); // Surround hyphens with our delimiter so our strncmp is accurate
		
		//if (preg_match("/^[a-z]{2,}$/i",$name))  // Simple preg_match if statement
		//{
			$names_array = explode(' ',$name);  // Set the delimiter as a space.

			for ($i = 0; $i < count($names_array); $i++)
			{
				if (strncmp($names_array[$i],'mc',2) == 0 || ereg('^[oO]\'[a-zA-Z]',$names_array[$i]))
				{
				   $names_array[$i][2] = strtoupper($names_array[$i][2]);
				}
				$names_array[$i] = ucfirst($names_array[$i]);
			}
			$name = implode(' ',$names_array);
			
			$name = preg_replace("[ \- ]", "-",$name); //  Remove the extra instances of our delimiter
			return ucwords($name);
		//}
	}
	
	//create directories on the server
	function recursive_mkdir($path, $mode = 0777) {
		$dirs = explode(DIRECTORY_SEPARATOR , $path);
		$count = count($dirs);
		$path = '.';
		for ($i = 0; $i < $count; ++$i) {
			$path .= DIRECTORY_SEPARATOR . $dirs[$i];
			if (!is_dir($path) && !mkdir($path, $mode)) {
				return false;
			}
			//chmod($path, 0777);
		}
		return true;
	}
	
	// Take a date string (yyyy-mm-dd hh:mm:ss) and return the epoch time (or 0 if empty). (flag = 0 = usetime : 1 = 12:00:00)
	function fntimeconvert($strdate,$intflag) {
		if (strlen($strdate) == 0) {
			$intepoch = 0;
		} else {
			if ($intflag == 0) {
				// Use time
				$intepoch = gmmktime(substr($strdate,11,2),substr($strdate,14,2),substr($strdate,17,2),substr($strdate,5,2),substr($strdate,8,2),substr($strdate,0,4));
			} else {
				// use 12:00:00 for time
				$intepoch = gmmktime(12,0,0,substr($strdate,5,2),substr($strdate,8,2),substr($strdate,0,4));
			}
		}

		return $intepoch;
	}
	
	//very simple function to convert a unix timestamp 
	function fnconvertunixtime ($strtime, $strformat = "d/m/Y") {
		return date($strformat, $strtime);
	}

	// generates a random number that can be used as a new account number 
	function fngetRandomString($length) {
        $validCharacters = "0123456789";
        $validCharNumber = strlen($validCharacters);
        $result = "";
        for ($i = 0; $i < $length; $i++) {
            $index = mt_rand(0, $validCharNumber - 1);
            $result .= $validCharacters[$index];
        }
        return $result;
    }
	
	// generates a random string of characters that can be used as memorable info 
	function fngetRandomStringChar($length) {
		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
		return $str;
	}

	// Dump an array of fixed length lines (in array) to a file
	function fndataexportfixed($dataarray, $filename="export.txt", $streol=PHP_EOL) {
		ob_end_clean();
		$f = fopen('php://memory', 'w');
		foreach ($dataarray as $line) {
			fputs($f,$line.$streol);
		}
		// rewrite the "file" with the txt lines
		fseek($f, 0);
		// tell the browser it's going to be a txt file
		header('Content-Type: application/txt');
		// tell the browser we want to save it instead of displaying it
		header('Content-Disposition: attachement; filename="'.$filename.'"');
		// make php send the generated txt lines to the browser
		fpassthru($f);
		exit();
	}


	
	// EXPORT FUNCTIONS
	// ================
	function dataexportcsv($dataarray, $filename = "export.csv", $delimiter=",") {
	
		ob_end_clean();
		$f = fopen('php://memory', 'w'); 
		// loop over the input array
		foreach ($dataarray as $line) {
			// generate csv lines from the inner arrays
			fputcsv($f, $line, $delimiter); 
		} 
		// rewrite the "file" with the csv lines
		fseek($f, 0);
		// tell the browser it's going to be a csv file
		header('Content-Type: application/csv');
		// tell the browser we want to save it instead of displaying it
		header('Content-Disposition: attachement; filename="'.$filename.'"');
		// make php send the generated csv lines to the browser
		fpassthru($f);
		exit();
	}
	
	//Class to convert fixed width files into CSV format
	class fixed2CSV extends SplFileObject{
		public function __construct( $filename ){parent::__construct( $filename );}

		/*
		* Settor, is called when trying to assign a value to non-existing property
		*
		* @access    public
		* @param    string    $name    The name of the property to set
		* @param    mixed    $value    The value of the property
		* @throw    Excption if property is not able to be set
		*
		*/
		public function __set( $name, $value )
		{
			switch( $name )
			{
				case 'eol':
				case 'fields':
				case 'separator':
				$this->$name = $value;
				break;

				default:
				throw new Exception("Unable to set $name");
			}
		}

		/**
		*
		* Gettor This is called when trying to access a non-existing property
		*
		* @access    public
		* @param    string    $name    The name of the property
		* @throw    Exception if proplerty cannot be set
		* @return    string
		*
		*/
		public function __get( $name )
		{
			switch( $name )
			{
				case 'eol':
				return "\n";

				case 'fields':
				return array();

				case 'separator':
				return ',';

				default:
				throw new Exception("$name cannot be set");
			}
		}
			
		/**
		*
		* Over ride the parent current method and convert the lines
		*
		* @access    public
		* @return    string    The line as a CSV representation of the fixed width line, false otherwise
		*
		*/
		public function current()
		{
			if( parent::current() )
			{
				$csv = '';
				$fields = new cachingIterator( new ArrayIterator( $this->fields ) );
				foreach( $fields as $f )
				{
					$csv .= trim( substr( parent::current(), $fields->key(), $fields->current()  ) );
					$csv .= $fields->hasNext() ? $this->separator : $this->eol;
				}
				return $csv;
			}
			return false;
		}
	}
	
	// DATABASE FUNCTIONS
	// ================
	function connect($driver = DRIVER, $username = USER, $password = PASS, $host = HOST, $db = DB) {
		try
		{
			// Define new PHP Data Object connection
			$conn = new PDO(''.$driver.':host='.$host.';dbname='.$db, $username, $password);
			// Sets error reporting level
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
			// Sets charset
			$conn->exec("SET NAMES utf8");
			$conn->exec("SET CHARSET utf8");
			return $conn;
		}
		catch(PDOException $e)
		{
			// If failed, display error
			$strdebug = "ERROR: " . $e->getMessage();
		}
	}

	function query($conn, $querystring, $querytype, $values = null){
		if ($conn) {
				// Prepares query
				$query = $conn->prepare($querystring);

				// Exexcutes query and binds $values array
				$query->execute($values);

				switch ($querytype)
				{
					case "single":
							// Returns associative array of single result
							$result = $query->fetch(PDO::FETCH_ASSOC);
							break;

					case "multi":
							// Returns multidimensional associative array of all results
							$result = $query->fetchAll(PDO::FETCH_ASSOC);
							break;

					case "count":
							// Returns number of selected rows
							$result = $query->rowCount();
							break;
							
					case "columns":
						//returns column information
						foreach(range(0, $query->columnCount() - 1) as $column_index)
						{
							$columns = $query->getColumnMeta($column_index);
							$columnsname[] = $columns['name'];
						}
						$result = $columnsname;
						break;
							
					case "insert":
							// Insert new row and return is fld_counter (auto increment) of the inserted line
							$result = $conn->lastInsertID();
							break;

					case "update":
							// Return number of rows updated by query
							$result = $query->rowCount(); 
							break;

					case "delete":
							// Return number of rows deleted by query
							$result = $query->rowCount();
							break;

					default:
							// Invalid query type
							$result = "Invalid !";
							break;
				}

				return $result;
		}
		else
		{
				return "Failure !";
		}
	}
	
	function fnGetSiteBlocks($lookupID,$lookupType,$position,$conn){
			
		$strdbsql = "SELECT site_blocks.*, site_block_position.description AS position FROM site_block_relations INNER JOIN site_blocks ON site_block_relations.blockID = site_blocks.recordID INNER JOIN site_block_position ON site_block_relations.positionID = site_block_position.recordID WHERE site_block_relations.$lookupType = :lookupID AND site_block_position.description = :position";
		$returnType = "multi";
		$dataParam = array("lookupID"=>$lookupID,"position"=>$position);
		$blockRelations = query ($conn, $strdbsql, $returnType, $dataParam);
		
		//error_log("Lookup ID: ".$lookupID." Position: ".$position);
		
		if (count($blockRelations) != 0) {
			print ("<div class='".str_replace(" ","",$position)."'>");
			foreach ($blockRelations AS $relation) {
				
				if ($relation['type'] == 3) { //gallery
					
					$strdbsql = "SELECT * FROM site_gallery_images WHERE galleryID = :galleryID ORDER BY galleryOrder";
					$returnType = "multi";
					$dataParam = array("galleryID"=>$relation['galleryID']);
					$galleryImages = query ($conn, $strdbsql, $returnType, $dataParam);
					
					print ("<div class='slider-wrapper theme-default'>");
						print ("<div id='slider' class='nivoSlider'>");
				
							foreach ($galleryImages AS $image) {
								print ("<a href='".$image['linkLocation']."' title=''>");
									print ("<img src='/images/gallery/".$image['imageLocation']."' alt = '".str_replace("&", "&amp;",$image['description'])."' title='' width='700' height='320'/>");
								print ("</a>");
							}

						print ("</div>");
					print ("</div>");
					
				} else {
				
					print ($relation['content']);
					
				}				
			}
			print ("</div>");
		}
	}
	
	function fnItemList($stockItems, $conn) {
		$strItemList = "";
		$strrootpath = $_SERVER['DOCUMENT_ROOT'];
		if(!empty($stockItems))
		{
			foreach($stockItems AS $stockItem)
			{
				//$new_string = trim(preg_replace('/[^a-z0-9.-]+/', '_', strtolower($resultdata->fld_ptitle)), '_');
				$strlink = getItemHref($stockItem);
				$intitem = $stockItem['stockCode'];
				//if ($resultdata->fld_logoimage != "") $strlogopath = $strimageurl."product_logos/".$resultdata->fld_logoimage;
				//else unset($strlogopath);
				
				
				$thumbnailImg = getThumbnailImage($stockItem['stockRecordID'], $conn);
				$overlayImg = getOverlayImage($stockItem['stockRecordID'], $conn);
				
				if ($thumbnailImg['imageLink'] != "" && file_exists($strrootpath."/images/products/".$stockItem['stockRecordID']."/list/".$thumbnailImg['imageLink'])) $strimgpath = $strsiteurl."/images/products/".$stockItem['stockRecordID']."/list/".$thumbnailImg['imageLink'];
				else $strimgpath = $strsiteurl."/images/no_image.gif";
				
				$strItemList .= ("\n\n\n<div class='itemListing'>\n");
					$strItemList .= ("<hr />\n");

					/********************************************** Images ****************************************/
					$strItemList .= ("<a id='imagelink_$intitem' href='$strlink'>");
						if ($overlayImg['imageLink'] != "" && file_exists($strrootpath."/images/overlays/".$overlayImg['imageLink']))
						{
							$stroverlayimgpath = $strsiteurl."/images/overlays/".$overlayImg['imageLink'];
							$strItemList .= ("<img src='".$stroverlayimgpath."' class='productshopoverlaytopleft' alt='".$stockItem['name']."'/>\n");
						}
						$strItemList .= ("<img src='".$strimgpath."' class='itemImage' alt='".$stockItem['name']." ".$thumbnailImg['description']."'/>");
					$strItemList .= ("</a>\n");

					$strItemList .= ("<div class='itemdescription'>\n");//open description
						/****************************************** Title ****************************************/
						$strItemList .=("<div style='clear:both; overflow:auto;'>");
							$strItemList .= ("<h3 class='itemTitle'><a id='titlelink_$intitem' href='$strlink'>".$stockItem['name']."</a></h3>\n\n\n");
							//$strItemList .= ("<h3 class='itemTitle'><a id='titlelink_$intitem' href='$strlink'>".$stockItem['name']."</a></h3>\n\n\n");


							/****************************************** Price ****************************************/
							/*$getPricesQuery = "SELECT DISTINCT price FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE recordID = :groupID AND stock.price != 0 AND stock_group_information.statusID != 1 AND stock_group_information.statusID != 2 ORDER BY stock.price LIMIT 1";
							$strType = "multi";
							
							$prices = query($conn, $getPricesQuery, $strType);
							if (mysql_num_rows($result2) > 1)
							{
								while ($resultdata2 = mysql_fetch_object($result2))
								{
									print("<span class='sizelink' style=''>");
									print("<span style='font-size:10px'>from</span> &pound;".number_format($resultdata2->fld_price,2)."</span>\n");
								}
							} else if (mysql_num_rows($result2) == 1) {
								$resultdata2 = mysql_fetch_object($result2);
								print("<span class='sizelink' style=''> &pound;".number_format($resultdata2->fld_price,2)."</span>\n");
							}*/
							if($stockItem['stock_price'] != 0.00)
							{
								$strItemList .=("<span class='sizelink' style=''>");
								if(($stockItem['priceControl'] > $stockItem['stock_price']) || ($stockItem['retailPrice'] > $stockItem['stock_price']))
								{
									$strItemList .= ("<span class='rrp'>&pound;");
										if($stockItem['priceControl'] > $stockItem['retailPrice'])
										{
											$strItemList .= (number_format($stockItem['priceControl'],2));
										}
										else
										{
											$strItemList .= (number_format($stockItem['retailPrice'],2));
										}
									$strItemList .= ("</span> ");
								}
								$strItemList .= ("&pound;".number_format($stockItem['stock_price'],2)."</span>\n");
							}
						$strItemList .= ("</div>\n\n");

					$strItemList .= ("</div>\n\n\n");
				$strItemList .= ("</div>\n\n\n\n");//close product listing
			}
		}
		else
		{
			$strItemList .= ("<h3>Sorry, we have no items matching your criteria</h3>");
		}
		
		return $strItemList;
	}
	
	function getReviewInfo($productID, $filter, $conn) {
		switch($filter)
		{
			case "avg":
				$getAvgReviewInfo = "SELECT AVG(numStars) AS average_rating, count(recordID) AS count FROM stock_reviews WHERE stockGroupID = :stockGroupID AND shownOnSite = 1";
				$returnType = "single";
				$dataParam = array("stockGroupID" => $productID);
				$reviewDetails = query($conn, $getAvgReviewInfo, $returnType, $dataParam);
				$reviewDetails['average_rating'] = round(($reviewDetails['average_rating'] * 2), 0);
				break;
			case "all":
				$getAllReviewInfo = "SELECT stock_reviews.* FROM stock_reviews WHERE stock_reviews.stockGroupID = :stockGroupID AND stock_reviews.shownOnSite = 1";
				$returnType = "multi";
				$dataParam = array("stockGroupID" => $productID);
				$reviewDetails = query($conn, $getAllReviewInfo, $returnType, $dataParam);
				$totalReviews = count($reviewDetails);
				for($i = 0; $i < $totalReviews; $i++)
				{
					$reviewDetails[$i]['numStars'] = round(($reviewDetails[$i]['numStars'] * 2), 0);
				}
				break;
		}
		return $reviewDetails;
	}
	
	function getBrandInfo($brandID, $filter, $conn) {
		switch($filter)
		{
			case "product":
					$getProductBrand = "SELECT brandImage, brandName, brandDescription FROM stock_brands WHERE recordID = :brandID";
					$returnType = "single";
					$dataParam = array("brandID" => $brandID);
					$brandDetails = query ($conn, $getProductBrand, $returnType, $dataParam);
				break;
		}
		return $brandDetails;
	}
	
	function getStockGroupItems($groupID, $conn) {
		$getStockGroupItemsQuery = "SELECT recordID, price, priceControl, stockCode, stockLevels, groupID, dimensionID FROM stock WHERE groupID = :groupID";
		$returnType = "multi";
		$dataParam = array( "groupID" => $groupID );
		$stockGroupItemsData = query($conn, $getStockGroupItemsQuery, $returnType, $dataParam);
		$stockGroupItems = array();
		foreach($stockGroupItemsData AS $stockGroupItemData)
		{			
			$stockGroupItems[$stockGroupItemData['recordID']] = $stockGroupItemData;
			$stockGroupItems[$stockGroupItemData['recordID']]['stockAttributes'] = getStockItemAttributes($stockGroupItemData['recordID'], $conn);
			
			$arrStockGroupItemID[] = array("recordID" => $stockGroupItemData['recordID']);
		}
		return $stockGroupItems;
	}
	
	function getStockItemAttributes($stockItemID, $conn) {
		$getStockItemAttributesQuery = "SELECT stock_attributes.*, stock_attribute_type.recordID AS attributeTypeID FROM stock_attributes INNER JOIN stock_attribute_type ON stock_attributes.attributeTypeID = stock_attribute_type.recordID INNER JOIN stock_attribute_relations ON stock_attributes.recordID = stock_attribute_relations.stockAttributeID WHERE stock_attribute_relations.stockID = :stockItemID";
		$returnType = "multi";
		$dataParam = array( "stockItemID" => $stockItemID );
		$stockItemAttributesData = query($conn, $getStockItemAttributesQuery, $returnType, $dataParam);
		$stockItemAttributes = array();
		foreach($stockItemAttributesData AS $stockItemAttributeData)
		{
			$stockItemAttributes[$stockItemAttributeData['attributeTypeID']] = $stockItemAttributeData;
		}
		return $stockItemAttributes;
	}
	
	function fnAjaxItemImages($selectedItem, $stockGroupName, $conn) {
		
		$strrootpath = $_SERVER['DOCUMENT_ROOT'];
		$ajaxItemImages = "";
		//main image - may need to change via ajax
		$getStockImage = "SELECT imageLink, description FROM stock_images WHERE stockID = :stockID AND imageTypeID = 1 AND imageOrder = 1";
		$returnType = "single";
		$dataParam = array("stockID" => $selectedItem);
		$stockImage = query($conn, $getStockImage, $returnType, $dataParam);
		if ($stockImage['imageLink'] != "" && file_exists($strrootpath."/images/products/".$selectedItem."/main/".$stockImage['imageLink'])) $strimgpath = $strsiteurl."/images/products/".$selectedItem."/main/".$stockImage['imageLink'];
		else $strimgpath = $strsiteurl."/images/no_image.gif";
		
		if ($stockImage['imageLink'] != "" && file_exists($strrootpath."/images/products/".$selectedItem."/zoom/".$stockImage['imageLink'])) $strzoomimgpath = $strsiteurl."/images/products/".$selectedItem."/zoom/".$stockImage['imageLink'];
		else $strzoomimgpath = $strimgpath;
		
		$ajaxItemImages .= "<a class='fancybox' id='zoomlink' href='".$strzoomimgpath."' style='display:block;'>";
			$ajaxItemImages .= "<img src='".$strimgpath."' id='itemImage' alt='".$stockGroupName." ".$stockImage['description']."' />";
			$getOverlayImages = "SELECT imageLink, stock_images.description, stock_image_types.description AS typeDescription FROM stock_images INNER JOIN stock_image_types ON stock_images.imageTypeId = stock_image_types.recordID WHERE stockID = :stockID AND imageTypeID > 1";
			$returnType = "multi";
			$dataParam = array("stockID" => $selectedItem);
			$overlayImages = query($conn, $getOverlayImages, $returnType, $dataParam);
			
			foreach($overlayImages AS $overlayImage)
			{
				if ($overlayImage['imageLink'] != '' && file_exists($strrootpath."/images/overlays/".$overlayImage['imageLink'])) {
					$ajaxItemImages .= "<img src='images/overlays/".$overlayImage['imageLink']."' class='overlay ".strtolower(str_replace(" ", "_", $overlayImage['typeDescription']))."' alt='".$overlayImage['description']."' />";
				}
			}
		$ajaxItemImages .= "</a>";
		$ajaxItemImages .= "<br />";
		
		//thumbnails - may need to change via ajax
		$getThumbnailImages = "SELECT imageLink, description FROM stock_images WHERE stockID = :stockID AND imageTypeID = 1";
		$returnType = "multi";
		$dataParam = array("stockID" => $selectedItem);
		$thumbnailImages = query($conn, $getThumbnailImages, $returnType, $dataParam);
		
		if(count($thumbnailImages) > 1)
		{
			foreach($thumbnailImages AS $thumbnailImage)
			{
				if ($thumbnailImage['imageLink'] != '' && file_exists($strrootpath."/images/products/".$selectedItem."/list/".$stockImage['imageLink'])) {
					$strselected = "";
					if ($thumbnailImage['imageLink'] == $stockImage['imageLink']) {
						$strselected = " selected";
					}
					$ajaxItemImages .= "<a onclick='return false;' class='thumbnail".$strselected."' rel='group' href='".$strsiteurl."/images/products/".$selectedItem."/main/".$thumbnailImage['imageLink']."'>";
						$ajaxItemImages .= "<img src='".$strsiteurl."/images/products/".$selectedItem."/list/".$thumbnailImage['imageLink']."' alt='".$thumbnailImage['description']."' title='".$thumbnailImage['description']."' />";
					$ajaxItemImages .= "</a>";
				}
			}
		}
		
		return $ajaxItemImages;
	}
	
	function fnAjaxItemDetails($selectedItem,$groupItemDetails,$retailPrice,$statusID,$qty,$conn) {
		
		$strrootpath = $_SERVER['DOCUMENT_ROOT'];
		$ajaxItemDetails = "";
		//price
		if ($groupItemDetails[$selectedItem]['price'] != 0.00) {
			if ($groupItemDetails[$selectedItem]['price'] < $groupItemDetails[$selectedItem]['priceControl']) {
				$ajaxItemDetails .= "<h3 class='price'><span id='ajaxPrice'>Now &pound;".$groupItemDetails[$selectedItem]['price']."</span> <small>Was: &pound;".$groupItemDetails[$selectedItem]['priceControl']."</small></h3>";
			} else {
				$ajaxItemDetails .= "<h3 class='price'><span id='ajaxPrice'>&pound;".$groupItemDetails[$selectedItem]['price']."</span>";
				if($groupItemDetails[$selectedItem]['price'] < $retailPrice)
				{
					$ajaxItemDetails .= "<small>RRP: &pound;".$retailPrice."</small>";
				}
				$ajaxItemDetails .= "</h3>";
			}
		}
		$ajaxItemDetails .= "<br />";
		
		//product code
		$ajaxItemDetails .= "<label>Product Code:</label> <span id='stock_code' class='stock_details'>".$groupItemDetails[$selectedItem]['stockCode']."</span><br />";
		
		$ajaxItemDetails .= "<br />";
		
		//stock levels (if required)
		if(!empty($groupItemDetails[$selectedItem]['stockLevels']))
		{
			//$ajaxItemDetails .= "<label>Stock Levels:</label> <span class='stock_details'>".$groupItemDetails[$selectedItem]['stockLevels']."</span><br />";
			//$ajaxItemDetails .= "<br />";
		}
		
		//size / dimensions
		if(!empty($groupItemDetails[$selectedItem]['dimensionID']))
		{
			$stockGroupDimensions = getStockGroupDimensions($groupItemDetails, $conn);
			
			$ajaxItemDetails .= "<label>Size:</label> ";
			if(count($stockGroupDimensions) > 1)
			{
				$ajaxItemDetails .= "<select name='dimensions' id='dimensions' class='stock_details' onchange='updateFilters(\"-\", \"dimensions\")'>";
					foreach($stockGroupDimensions AS $selectedStockGroupDimensions => $stockGroupDimension)
					{
						$selectedFlag = "";
						if($selectedStockGroupDimensions == $groupItemDetails[$selectedItem]['dimensionID'])
						{
							$selectedFlag = " selected";
							$selectedDimensions = $selectedStockGroupDimensions;
						}
						$ajaxItemDetails .= "<option value='".$selectedStockGroupDimensions."'".$selectedFlag.">".$stockGroupDimension['description']."</option>";
					}
				$ajaxItemDetails .= "</select>";
			}
			else
			{
				if($stockGroupDimensions[$groupItemDetails[$selectedItem]['dimensionID']]['description'] != "")
				{
					$dimensionDescription = $stockGroupDimensions[$groupItemDetails[$selectedItem]['dimensionID']]['description'];
				}
				else
				{
					$dimensionDescription = "Measurements as Follows";
				}
				$ajaxItemDetails .= "<span class='stock_details'>".$dimensionDescription."</span>";
				$selectedStockGroupDimensions = $groupItemDetails[$selectedItem]['dimensionID'];
				$ajaxItemDetails .= "<input type='hidden' name='dimensions' id='dimensions' value='".$selectedStockGroupDimensions."' />";
				$selectedDimensions = $groupItemDetails[$selectedItem]['dimensionID'];
			}
			
			$ajaxItemDetails .= "<br />";
			$ajaxItemDetails .= "<br />";
			
			$ajaxItemDetails .= "<span class='stock_details dimension_info'>";
			if($stockGroupDimensions[$selectedDimensions]['depth'] != "")
			{
				$ajaxItemDetails .= "Depth: ".$stockGroupDimensions[$selectedDimensions]['depth']." ";
			}
			if($stockGroupDimensions[$selectedDimensions]['height'] != "")
			{
				$ajaxItemDetails .= "Height: ".$stockGroupDimensions[$selectedDimensions]['height']." ";
			}
			if($stockGroupDimensions[$selectedDimensions]['width'] != "")
			{
				$ajaxItemDetails .= "Width: ".$stockGroupDimensions[$selectedDimensions]['width'];
			}
			$ajaxItemDetails .= "</span><br />";
			$ajaxItemDetails .= "<br />";
		}
		
		$getAttributeTypesQuery = "SELECT * FROM stock_attribute_type ORDER BY attributeTypeOrder";
		$strType = "multi";
		$attributeTypes = query($conn, $getAttributeTypesQuery, $strType);
		
		foreach($attributeTypes AS $attributeType)
		{
			$stockAttributeSwatches = getStockAttributes($groupItemDetails, $attributeType['recordID'], $conn);
			if (count($groupItemDetails[$selectedItem]['stockAttributes'][$attributeType['recordID']]) != 0) {
				$ajaxItemDetails .= "<div id='attBoxType".$attributeType['recordID']."' class='attributeBox'>";
					$ajaxItemDetails .= "<label>".$attributeType['description'].":</label> <span class='stock_details'>".$groupItemDetails[$selectedItem]['stockAttributes'][$attributeType['recordID']]['description']."</span>";
					$ajaxItemDetails .= "<br />";
					$ajaxItemDetails .= "<br />";
					$ajaxItemDetails .= "<input type='hidden' id='attType".$attributeType['recordID']."' name='attType".$attributeType['recordID']."' class='attType' value='".$attributeType['recordID']."' />";
					foreach($stockAttributeSwatches AS $selectedStockAttributeSwatch => $stockAttributeSwatch)
					{
						if($selectedStockAttributeSwatch == $groupItemDetails[$selectedItem]['stockAttributes'][$attributeType['recordID']]['recordID'])
						{
							$strSelected = " selectedSwatch";
						}
						else
						{
							$strSelected = "";
						}
							if ($stockAttributeSwatch['swatchImage'] != '' && file_exists($strrootpath."/images/swatches/".$stockAttributeSwatch['swatchImage'].".jpg")) {
								$ajaxItemDetails .= "<div class='swatchbox'>";
									$ajaxItemDetails .= "<input type='hidden' id='attID".$attributeType['recordID'].$selectedStockAttributeSwatch."' name='attID".$attributeType['recordID'].$selectedStockAttributeSwatch."' class='attID' value='".$selectedStockAttributeSwatch."' />";
									$ajaxItemDetails .= "<a onclick='updateFilters(\"".$selectedStockAttributeSwatch."\", \"".$attributeType['recordID']."\")' id='".$selectedStockAttributeSwatch."' class='swatch".$strSelected."' title='".$stockAttributeSwatch['description']."'>";
										$ajaxItemDetails .= "<span class='swatchImage' title='".$stockAttributeSwatch['description']."' style=\"background: url('images/swatches/".$stockAttributeSwatch['swatchImage'].".jpg') no-repeat;\"></span>";
									$ajaxItemDetails .= "</a>";
								$ajaxItemDetails .= "</div>";
							}
							else
							{
								$ajaxItemDetails .= "<div class='swatchText'>";
									$ajaxItemDetails .= "<input type='hidden' id='attID".$attributeType['recordID'].$selectedStockAttributeSwatch."' name='attID".$attributeType['recordID'].$selectedStockAttributeSwatch."' class='attID' value='".$selectedStockAttributeSwatch."' />";
									$ajaxItemDetails .= "<a onclick='updateFilters(\"".$selectedStockAttributeSwatch."\", \"".$attributeType['recordID']."\")' id='".$selectedStockAttributeSwatch."' title='".$stockAttributeSwatch['description']."' class='".$strSelected."'>";
										$ajaxItemDetails .= "<span class='swatchTitle'>".$stockAttributeSwatch['description']."</span>";
									$ajaxItemDetails .= "</a>";
								$ajaxItemDetails .= "</div>";
							}
					}
				$ajaxItemDetails .= "</div>";
				$ajaxItemDetails .= "<br />";
				$ajaxItemDetails .= "<br />";
			}
		}
		
		//qty / add to basket
		$stockLevels = intval($groupItemDetails[$selectedItem]['stockLevels']);
		if(!empty($groupItemDetails[$selectedItem]['stockLevels']) && $stockLevels == 0)
		{
			$ajaxItemDetails .= "This item is currently out of stock";
		}
		elseif($statusID == 5)
		{
			$ajaxItemDetails .= "This item is not available for purchase through our website. Please visit us in store, call us on 01229 820679 or <a href='/contact.php'>email us</a> for more further details.";
		}
		else
		{
			/*if(!empty($groupItemDetails[$selectedItem]['stockLevels']) && $stockLevels > 0)
			{*/
				$ajaxItemDetails .= "<label>Qty:</label> <select name='qty' id='qty' class='stock_details'>";
				//for($i = 1; $i <= $groupItemDetails[$selectedItem]['stockLevels']; $i++)
				for($i = 1; $i <= 10; $i++)
				{
					if($i == $qty)
					{
						$strSelect = " selected";
					}
					else
					{
						$strSelect = "";
					}
					$ajaxItemDetails .= "<option value='".$i."'".$strSelect.">".$i."</option>";
				}
				$ajaxItemDetails .= "</select>";
			//}
			//$ajaxItemDetails .= "<a href='#' onclick='jsaddmain(); return false;' class='add-basket'>Add to Basket</a>";
			$ajaxItemDetails .= "<button onclick='jsaddmain(); return false;' class='add-basket'>Add to Basket</button>";
			$ajaxItemDetails .= "<span id='invalidSelectionText'></span>";
		}
		$ajaxItemDetails .= "<br />";
		$ajaxItemDetails .= "<br />";
		
		return $ajaxItemDetails;
	}
	
	function getStockGroupItemInfo($groupID, $conn) {
		$getStockGroupItemPriceQuery = "SELECT recordID, price FROM stock WHERE groupID = :groupID ORDER BY recordID LIMIT 1";
		$returnType = "single";
		$dataParam = array("groupID" => $groupID);
		$stockGroupItemPrice = query($conn, $getStockGroupItemPriceQuery, $returnType, $dataParam);
		return $stockGroupItemPrice;
	}
	
	function getThumbnailImage($stockID, $conn) {
		$getThumbnailImageQuery = "SELECT * FROM stock_images WHERE stockID = :stockID AND imageTypeID = 1 AND imageOrder = 1";
		$returnType = "single";
		$param = array( "stockID" => $stockID );
		$thumbnailImage = query($conn, $getThumbnailImageQuery, $returnType, $param);
		return $thumbnailImage;
	}
	
	function getOverlayImage($stockID, $conn) {
		$getOverlayImageQuery = "SELECT * FROM stock_images WHERE stockID = :stockID AND imageTypeID = 2";
		$returnType = "single";
		$param = array( "stockID" => $stockID );
		$overlayImage = query($conn, $getOverlayImageQuery, $returnType, $param);
		return $overlayImage;
	}
	
	function getStockGroupDimensions($groupItemDetails, $conn) {
		$getStockGroupDimensionsData = "SELECT DISTINCT * FROM stock_dimensions WHERE recordID IN (";
		$stockGroupDimensionIDs = "";
		foreach($groupItemDetails AS $itemDetails)
		{
			$stockGroupDimensionIDs .= $itemDetails['dimensionID'].", ";
		}
		$stockGroupDimensionIDs = rtrim($stockGroupDimensionIDs, ", ");
		$getStockGroupDimensionsData .= $stockGroupDimensionIDs.")";
		$returnType = "multi";
		$stockGroupDimensionsData = query($conn, $getStockGroupDimensionsData, $returnType);
		$stockGroupDimensions = array();
		foreach($stockGroupDimensionsData AS $stockGroupDimensionData)
		{
			$stockGroupDimensions[$stockGroupDimensionData['recordID']] = array(
				"width" => $stockGroupDimensionData['width'],
				"height" => $stockGroupDimensionData['height'],
				"depth" => $stockGroupDimensionData['depth'],
				"diameter" => $stockGroupDimensionData['diameter'],
				"description" => $stockGroupDimensionData['description']
			);
		}
		return $stockGroupDimensions;
	}
	
	function getStockAttributes($groupItemDetails, $attributeType, $conn) {
		
		$attributes = array();
		$strdbsql = "SELECT stock_attributes.* FROM stock_attribute_relations INNER JOIN stock_attributes ON stock_attribute_relations.stockAttributeID = stock_attributes.recordID WHERE stock_attributes.attributeTypeID = $attributeType AND stock_attribute_relations.stockID IN (";
		
		$stockGroupIDs = "";
		foreach($groupItemDetails AS $itemDetails)
		{
			$stockGroupIDs .= $itemDetails['recordID'].", ";
		}
		$stockGroupIDs = rtrim($stockGroupIDs, ", ");
		$strdbsql .= $stockGroupIDs.")";
		
		//error_log($strdbsql);
		
		$returnType = "multi";
		$resultdata = query($conn, $strdbsql, $returnType);

		foreach($resultdata AS $row)
		{
			$attributes[$row['recordID']] = array(
				"description" => $row['description'],
				"shortCode" => $row['shortCode'],
				"hexCode" => $row['hexCode'],
				"swatchImage" => $row['swatchImage'],
				"attributeTypeID" => $row['attributeTypeID']
			);
		}
		return $attributes;
	}
	
	function getItemHref($itemArr)
	{
		$formattedName = str_replace(' ', '-', $itemArr['name']); // Replaces all spaces with hyphens.
		$formattedName = preg_replace('/[^A-Za-z0-9\-]/', '', $formattedName); // Removes special chars.
		$formattedName = preg_replace('/-+/', '-', $formattedName); // Replaces multiple hyphens with single one.
		$formattedName = rtrim($formattedName, "-");
		return "item_".$itemArr['stockCode']."_".strtolower($formattedName);
		//return "item_".$itemArr['stockCode'];
	}
	
	function checkUserLogin($username, $password, $conn)
	{
		$checkLoginQuery = "SELECT COUNT(*) AS num_valid, recordID, email FROM `customer` WHERE `email` = :email AND `password` = :password GROUP BY recordID, email";
		$params = array( "email" => $username, "password" => md5($password) );
		$queryType = "single";
		
		$basicCustomerDetails = query($conn, $checkLoginQuery, $queryType, $params);
		
		return $basicCustomerDetails;
	}
	
	function getUserDetails($criteria, $view, $conn)
	{
		switch($view)
		{
			case "basic":
				$getBasicDetailsQuery = "SELECT * FROM customer WHERE recordID = :userhash";
				$returnType = "single";
				$param = array( "userhash" => $criteria );
				$userDetails = query($conn, $getBasicDetailsQuery, $returnType, $param);
				break;
			case "deladdress":
				$getDelAddressQuery = "SELECT * FROM customer_address AS deladd INNER JOIN customer AS cust ON deladd.recordID = cust.defaultDeliveryAdd WHERE cust.recordID = :userhash";
				$returnType = "single";
				$param = array( "userhash" => $criteria );
				$userDetails = query($conn, $getDelAddressQuery, $returnType, $param);
				break;
			case "billaddress":
				$getBillAddressQuery = "SELECT * FROM customer_address AS billadd INNER JOIN `customer` AS cust ON billadd.recordID = cust.defaultBillingAdd WHERE cust.recordID = :userhash";
				$returnType = "single";
				$param = array( "userhash" => $criteria );
				$userDetails = query($conn, $getBillAddressQuery, $returnType, $param);
				break;
			case "byemail":
				$getBasicDetailsQuery = "SELECT * FROM customer WHERE email = :email";
				$returnType = "multi";
				$param = array( "email" => $criteria );
				$userDetails = query($conn, $getBasicDetailsQuery, $returnType, $param);
				
				if(count($userDetails) != 1)
				{
					$userDetails = array();
				}
				else
				{
					$userDetails = $userDetails[0];
				}
				break;
			case "byid":
				$getBasicDetailsQuery = "SELECT * FROM customer WHERE recordID = :custID";
				$returnType = "single";
				$param = array( "custID" => $criteria );
				$userDetails = query($conn, $getBasicDetailsQuery, $returnType, $param);
				break;
			case "byemailandnotid":
				$getBasicDetailsQuery = "SELECT * FROM customer WHERE email = :email AND recordID != :custID";
				$returnType = "single";
				$param = array(
					"email" => $criteria['email'],
					"custID" => $criteria['custid']
				);
				$userDetails = query($conn, $getBasicDetailsQuery, $returnType, $param);
				break;
		}
		return $userDetails;
	}
	
	function deleteUser($username, $password, $conn)
	{
		$deleteUserQuery = "DELETE FROM customer WHERE email = :email AND password = :password";
		$queryType = "delete";		
		$params = array(
			"email" => $username,
			"password" => md5($password)
		);
		$deleteCustomerDetails = query($conn, $deleteUserQuery, $queryType, $params);
		
		return $deleteCustomerDetails;
	}
	
	function updateUserDetails($values, $criteria, $conn)
	{
		switch($criteria)
		{
			case "byid":
				$updateUserQuery = "UPDATE customer SET firstname = :firstname, surname = :surname, title = :title, telephone = :telephone, email = :email, mobile = :mobile, fax = :fax, company = :company WHERE recordID = :custid";
				$queryType = "update";
				$params = array(
					"firstname" => $values['firstname'],
					"surname" => $values['surname'],
					"title" => $values['title'],
					"telephone" => $values['telephone'],
					"email" => $values['email'],
					"mobile" => $values['mobile'],
					"fax" => $values['fax'],
					"company" => $values['company'],
					"custid" => $values['custid']
				);
				$updateCustomerDetails = query($conn, $updateUserQuery, $queryType, $params);
				break;
		}
		return $updateCustomerDetails;
	}
	
	function updateUserPassword($password, $custID, $conn)
	{
		$updatePasswordQuery = "UPDATE customer SET password = :password WHERE recordID = :custid";
		$queryType = "update";
		$params = array(
			"password" => md5($password),
			"custid" => $custID
		);
		$updateCustomerPassword = query($conn, $updatePasswordQuery, $queryType, $params);
		
		return $updateCustomerPassword;
	}
	
	function updateUserBillDelAddresses($values, $criteria, $conn)
	{
		switch($criteria)
		{
			case "billanddel":
				$updateBillDellAddressesQuery = "UPDATE customer SET defaultDeliveryAdd = :deladd, defaultBillingAdd = :billadd WHERE recordID = :custid";
				$queryType = "update";
				$params = array(
					"billadd" => $values['billingadd'],
					"deladd" => $values['deliveryadd'],
					"custid" => $values['custid']
				);
				$updateUserBillDelAddresses = query($conn, $updateBillDellAddressesQuery, $queryType, $params);
				break;
			case "bill":
				$updateBillDellAddressesQuery = "UPDATE customer SET defaultBillingAdd = :billadd WHERE recordID = :custid";
				$queryType = "update";
				$params = array(
					"billadd" => $values['billingadd'],
					"custid" => $values['custid']
				);
				$updateUserBillDelAddresses = query($conn, $updateBillDellAddressesQuery, $queryType, $params);
				break;
			case "del":
				$updateBillDellAddressesQuery = "UPDATE customer SET defaultDeliveryAdd = :deladd WHERE recordID = :custid";
				$queryType = "update";
				$params = array(
					"deladd" => $values['deliveryadd'],
					"custid" => $values['custid']
				);
				$updateUserBillDelAddresses = query($conn, $updateBillDellAddressesQuery, $queryType, $params);
				break;
			case "byid":
				$updateBillDellAddressesQuery = "UPDATE customer_address SET firstname = :firstname, surname = :surname, title = :title, add1 = :add1, add2 = :add2, add3 = :add3, town = :town, county = :county, country = :country, postcode = :postcode WHERE recordID = :custid";
				$queryType = "update";
				$params = array(
					"firstname" => $values['firstname'],
					"surname" => $values['surname'],
					"title" => $values['title'],
					"add1" => $values['add1'],
					"add2" => $values['add2'],
					"add3" => $values['add3'],
					"town" => $values['town'],
					"county" => $values['county'],
					"country" => $values['country'],
					"postcode" => $values['postcode'],
					"custid" => $values['custid']
				);
				$updateUserBillDelAddresses = query($conn, $updateBillDellAddressesQuery, $queryType, $params);
				break;
		}
		
		return $updateUserBillDelAddresses;
	}
	
	function getUserAddresses($id, $view, $conn)
	{
		switch($view)
		{
			case "all":
				$getCustomerAddressesQuery = "SELECT * FROM customer_address WHERE customerID = :custid";
				$returnType = "multi";
				$param = array( "custid" => $id );
				$customerAddresses = query($conn, $getCustomerAddressesQuery, $returnType, $param);
				break;
			case "byid":
				$getCustomerAddressesQuery = "SELECT * FROM customer_address WHERE recordID = :addid";
				$returnType = "single";
				$param = array( "addid" => $id );
				$customerAddresses = query($conn, $getCustomerAddressesQuery, $returnType, $param);
				break;
		}
		return $customerAddresses;
	}
	
	function deleteUserAddress($addressid, $custid, $conn)
	{
		$deleteCustomerAddressQuery = "DELETE FROM customer_address WHERE recordID = :addressid AND customerID = :custid";
		$queryType = "delete";
		$params = array(
			"addressid" => $addressid,
			"custid" => $custid
		);
		
		return query($conn, $deleteCustomerAddressQuery, $queryType, $params);
	}
	
	function insertUserAddress($values, $conn)
	{
		$insertUserAddressQuery = "INSERT INTO customer_address (customerID, title, firstname, surname, add1, add2, add3, town, county, postcode, country) VALUES (:custid, :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :postcode, :country)";
		$queryType = "insert";
		$params = array(
			"title" => $values['title'],
			"firstname" => $values['firstname'],
			"surname" => $values['surname'],
			"add1" => $values['add1'],
			"add2" => $values['add2'],
			"add3" => $values['add3'],
			"town" => $values['town'],
			"county" => $values['county'],
			"postcode" => $values['postcode'],
			"country" => $values['country'],
			"custid" => $values['custid']
		);
		
		return query($conn, $insertUserAddressQuery, $queryType, $params);
	}
	
	function getBasketDetails($id, $view, $conn)
	{
		switch($view)
		{
			case "recent":
				$inttime = strtotime("-6 months"); // last six months
				$getRecentBasketDetailsQuery = "SELECT basket_header.*, basket_items.basketHeaderID, count(basket_items.recordID) AS itemCount FROM basket_header INNER JOIN basket_items ON basket_header.recordID = basket_items.basketHeaderID WHERE basket_header.customerID = :userID AND basket_header.recordID != 2 AND basket_header.modifiedTimestamp > ".$inttime;
				if(!empty($_COOKIE['basketID']) && $_COOKIE['basketID'] != -1 && isset($_COOKIE['basketID']))
				{
					$getRecentBasketDetailsQuery .= " AND basket_header.recordID != :currBasketID";
					$params['currBasketID'] = $_COOKIE['basketID'];
				}
				$getRecentBasketDetailsQuery .= " GROUP BY basket_items.basketHeaderID ORDER BY basket_header.recordID DESC LIMIT 15";
				$returnType = "multi";
				$params['userID'] = $id;
				$basketDetails = query($conn, $getRecentBasketDetailsQuery, $returnType, $params);
				break;
			case "completed":
				$getCompletedBasketDetailsQuery = "SELECT * FROM order_header WHERE customerID = :userID ORDER BY recordID DESC LIMIT 15";
				$returnType = "multi";
				$param = array( "userID" => $id);
				$basketDetails = query($conn, $getCompletedBasketDetailsQuery, $returnType, $param);
				break;
			case "byid":
				$getSpecifiedBasketDetailsQuery = "SELECT * FROM basket_header WHERE recordID = :basketID";
				$returnType = "multi";
				$param = array( "basketID" => $id);
				$basketDetails = query($conn, $getSpecifiedBasketDetailsQuery, $returnType, $param);
				break;
			case "productandbasketid":
				$getSpecifiedBasketDetailsQuery = "SELECT * FROM basket_items WHERE basketHeaderID = :basketID AND stockID = :stockID";
				$returnType = "multi";
				$params = array(
					"basketID" => intval($id['basketID']),
					"stockID" => $id['stockID']
				);
				$basketDetails = query($conn, $getSpecifiedBasketDetailsQuery, $returnType, $params);
				break;
			case "byheaderid":
				$getSpecifiedBasketDetailsQuery = "SELECT * FROM basket_items WHERE basketHeaderID = :basketHeaderID";
				$returnType = "multi";
				$param = array( "basketHeaderID" => $id);
				$basketDetails = query($conn, $getSpecifiedBasketDetailsQuery, $returnType, $param);
				break;
		}
		return $basketDetails;
	}
	
	function deleteBasketDetails($baskHeadID, $conn)
	{
		$param = array( "baskheadid" => intval($baskHeadID) );
		$deleteBaskHeaderQuery = "DELETE FROM basket_header WHERE recordID = :baskheadid";
		$returnType = "delete";
		query($conn, $deleteBaskHeaderQuery, $returnType, $param);
		
		$deleteBasketQuery = "DELETE FROM basket_items WHERE basketHeaderID = :baskheadid";
		$returnType = "delete";
		query($conn, $deleteBasketQuery, $returnType, $param);
	}
	
	function updateBasketDetails($values, $cmd, $conn)
	{
		switch($cmd)
		{
			case "addqty":
				$params = array(
					"qtytoadd" => intval($values['qtytoadd']),
					"baskheadid" => intval($values['baskheadid']),
					"productid" => intval($values['productid'])
				);
				$updateBasketItemQtyQuery = "UPDATE basket_items SET quantity = quantity + :qtytoadd WHERE basketHeaderID = :baskheadid AND stockID = :productid";
				$returnType = "update";
				query($conn, $updateBasketItemQtyQuery, $returnType, $params);
				break;
			case "addproduct":
				$params = array(
					"productid" => $values['productid'],
					"qtytoadd" => intval($values['qtytoadd']),
					"baskheadid" => intval($values['baskheadid']),
					"price" => intval(number_format($values['price'],2))
				);
				$addToBasketQuery = "INSERT INTO basket_items (stockID, quantity, basketHeaderID, price) VALUES (:productid, :qtytoadd, :baskheadid, :price)";
				$returnType = "insert";
				query($conn, $addToBasketQuery, $returnType, $params);
				break;
		}
	}
	
	function getStockItemDetails($id, $conn)
	{
		$param = array( "stockid" => $id );
		$getStockItemDetailsQuery = "SELECT stock.*, stock_group_information.statusID FROM stock INNER JOIN stock_group_information ON stock.groupID = stock_group_information.recordID WHERE stock.recordID = :stockid";
		$returnType = "single";
		$stockItemDetails = query($conn, $getStockItemDetailsQuery, $returnType, $param);
		return $stockItemDetails;
	}
	
	function getRelatedItems($id, $conn)
	{
		$param = array( "stockgroupid" => $id );
		$getNumRelatedItemsQuery = "SELECT COUNT(*) AS num_related FROM stock_related WHERE stockCode1 = :stockgroupid";
		$returnType = "single";
		$numRelatedItems = query($conn, $getNumRelatedItemsQuery, $returnType, $param);
		
		$getRelatedItemsQuery = "SELECT stock_group_information.*, stock.stockCode FROM stock_group_information INNER JOIN stock_related ON stock_related.stockCode2 = stock_group_information.recordID INNER JOIN stock ON stock_group_information.recordID = stock.groupID AND stock.recordID = (SELECT MIN(s.recordID) FROM stock s WHERE groupID = stock_group_information.recordID) WHERE stock_related.stockCode1 = :stockgroupid ORDER BY stock_related.relatedOrder";
		
		if(intval($numRelatedItems['num_related']) > 3)
		{
			$offset = mt_rand(0, intval($numRelatedItems['num_related'] - 3));
		}
		else
		{
			$offset = 0;
		}
		$getRelatedItemsQuery .= " LIMIT ".$offset.", 3";
		$returnType = "multi";
		$relatedItems = query($conn, $getRelatedItemsQuery, $returnType, $param);
		
		return $relatedItems;
	}
	
	function getNewsletterSubscription($criteria, $cmd, $conn)
	{
		switch($cmd)
		{
			case "bycustid":
				$getNewsletterSubscriptionQuery = "SELECT * FROM customer_newsletter WHERE customerID = :custid";
				$returnType = "single";
				$param = array( "custid" => $criteria );
				$newsletterSubscription = query($conn, $getNewsletterSubscriptionQuery, $returnType, $param);
				break;
			case "byemail":
				$getNewsletterSubscriptionQuery = "SELECT * FROM customer_newsletter WHERE emailAddress = :emailadd";
				$returnType = "single";
				$param = array( "emailadd" => $criteria );
				$newsletterSubscription = query($conn, $getNewsletterSubscriptionQuery, $returnType, $param);
				break;
		}
		return $newsletterSubscription;
	}
	
	function insertNewsletterSubscription($id, $name, $emailAdd, $conn)
	{
		$insertNewsletterSubscriptionQuery = "INSERT INTO customer_newsletter (customerID, name, emailAddress) VALUES (:custid, :name, :emailadd)";
		$returnType = "insert";
		$params = array(
			"custid" => $id,
			"name" => $name,
			"emailadd" => $emailAdd
		);
		return query($conn, $insertNewsletterSubscriptionQuery, $returnType, $params);
	}
	
	function updateNewsletterSubscription($emailAdd, $name, $id, $conn)
	{
		$updateNewsletterSubscriptionQuery = "UPDATE customer_newsletter SET emailAddress = :emailadd, name = :name WHERE customerID = :id LIMIT 1";
		$returnType = "update";
		$params = array(
			"emailadd" => $emailAdd,
			"id" => $id,
			"name" => $name
		);
		return query($conn, $updateNewsletterSubscriptionQuery, $returnType, $params);
	}
	
	function removeNewsletterSubscription($emailAdd, $conn)
	{
		$removeNewsletterSubscriptionQuery = "DELETE FROM customer_newsletter WHERE emailAddress = :emailadd LIMIT 1";
		$returnType = "delete";
		$param = array( "emailadd" => $emailAdd );
		return query($conn, $removeNewsletterSubscriptionQuery, $returnType, $param);
	}
	
	function check_email_address($email)
	{
		// First, we check that there's one @ symbol, and that the lengths are right
		if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email))
		{
			// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			return false;
		}
		// Split it into sections to make life easier
		$email_array = explode("@", $email);
		$local_array = explode(".", $email_array[0]);
		for ($i = 0; $i < sizeof($local_array); $i++)
		{
			if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i]))
			{
				return false;
			}
		}
		if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1]))
		{
			// Check if domain is IP. If not, it should be valid domain name
			$domain_array = explode(".", $email_array[1]);
			if (sizeof($domain_array) < 2)
			{
				return false;
				// Not enough parts to domain
			}
			for ($i = 0; $i < sizeof($domain_array); $i++)
			{
				if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i]))
				{
					return false;
				}
			}
		}
		return true;
	}
	
	//Function to redirect browser
	function redirect($url)
	{
	   if (!headers_sent())
			header('Location: '.$url);
	   else
	   {
			echo '<script type="text/javascript">';
			echo 'window.location.href="'.$url.'";';
			echo '</script>';
			echo '<noscript>';
			echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
			echo '</noscript>';
	   }
	}
	
	function fnGetBasket ($basketHeader, $basketItems, $companyPhone, $companyEmail, $conn) {
		
		$strdiscountdesc = "";
		$arrdiscountcats = array();
		$arrdiscountprod = array();
		$strcode = "";
		//get offer code details
		if ($basketHeader['offerCodeID'] > 0) {
			
			$strdbsql = "SELECT * FROM site_offer_codes WHERE recordID = :offerCodeID";
			$strType = "single";
			$arrdbparams = array("offerCodeID"=>$basketHeader['offerCodeID']);
			$siteOffers = query($conn,$strdbsql,$strType,$arrdbparams);
			
			$strcode = $siteOffers['code'];
			$strtype = $siteOffers['discountType'];
			$decdisvalue = $siteOffers['discountAmount'];
			$decminamt = $siteOffers['minimumSpend'];
			$strdiscountdesc = $siteOffers['description'];
			$arrdiscountcats = explode(",", $siteOffers['categoryList']);
			$arrdiscountprod = explode(",", $siteOffers['stockList']);
		
		} else {
			if ($strdiscountdesc == "") $strdiscountdesc = "Please enter your code";
		}
		
		if (count($basketItems) > 0) {
		
			print ("<div class='basketOptions'>");
				print ("<div class='leftOption'><a class='short_button' href='/'>Continue Shopping</a></div>");
				print ("<div class='rightOption'><a class='short_button continue' href='#' onclick='fnLoadAccountTypePage(); return false;' >Continue to Checkout</a></div>");
				print ("<div id='ajax_status'></div>");
			print ("</div>");
		
			print ("<div style='clear:both;'>");
				print ("<table cellspacing='0' cellpadding='3' id='baskettable'>");
					print ("<tbody>");
						print ("<tr>");
							print ("<td class='linehead'> </td>");
							print ("<td class='linehead' style='width: 10%;'>Stockcode</td>");
							print ("<td class='linehead' style='width: 30%;'>Name</td>");
							print ("<td class='linehead' style='width: 30%;'>Details</td>");
							print ("<td class='linehead' style='width: 15%;'>Quantity</td>");
							print ("<td class='linehead' align='right' style='width: 15%;'>Price</td>");
						print ("</tr>");
					
						$strordersize = "";
						$blnoddeven = false;
						$dectotcost = 0;
						$intnoitems = 0;
						foreach ($basketItems AS $basketItem) {
						
							//styling for changing rows
							if ($blnoddeven) $strtdclass = "class='evenrow line'";
							else $strtdclass = "class='line'";											
							$blnoddeven = !$blnoddeven;
							
							//get stock image
							$strdbsql = "SELECT * FROM stock_images WHERE stockID = :stockID ORDER BY imageOrder ASC LIMIT 1";
							$strType = "single";
							$arrdbparams = array("stockID"=>$basketItem['stockID']);
							$stockImage = query($conn,$strdbsql,$strType,$arrdbparams);
							
							if ($stockImage['imageLink'] != ""){
								$strimgpath = "/images/product_images/".$basketItem['stockID']."/list/".$stockImage['imageLink'];
							} else {
								$strimgpath = "/images/no_image.jpg";
							}
							
							//unserialize stock details
							$stockDetails = unserialize($basketItem['stockDetailsArray']);


							print ("<tr id='row_".$basketItem['recordID']."'>");
							print ("<td $strtdclass><img src='/css/images/delete.png' name='butr".$basketItem['recordID']."' value='Remove' alt='Remove this item' onclick='jsnewqty(\"".$basketItem['recordID']."\",\"r\",\"".$basketItem['quantity']."\")'/></td>");
							print ("<td $strtdclass>".$stockDetails['stockCode']."</td>");
							print ("<td $strtdclass>".$stockDetails['name']."</td>");
							print ("<td $strtdclass>");

							if ($stockDetails['width'] != "") print ("Width: ".$stockDetails['width']."<br/>");
							if ($stockDetails['height'] != "") print ("Height: ".$stockDetails['height']."<br/>");
							if ($stockDetails['depth'] != "") print ("Depth: ".$stockDetails['depth']."<br/>");
							if ($stockDetails['diameter'] != "") print ("Diameter: ".$stockDetails['diameter']."<br/>");
							if ($stockDetails['description'] != "") print ("Dimensions: ".$stockDetails['description']."<br/>");
							if (isset($stockDetails['Finish']) && $stockDetails['Finish']['description'] != "") print ("Finish: ".$stockDetails['Finish']['description']."<br/>");
							if (isset($stockDetails['Colour']) && $stockDetails['Colour']['description'] != "") print ("Colour: ".$stockDetails['Colour']['description']."<br/>");
							if (isset($stockDetails['Fabric']) && $stockDetails['Fabric']['description'] != "") print ("Fabric: ".$stockDetails['Fabric']['description']."<br/>");
							if (isset($stockDetails['Base']) && $stockDetails['Base']['description'] != "") print ("Base: ".$stockDetails['Base']['description']."<br/>");

							print ("</td>");
							print ("<td $strtdclass id='qty_".$basketItem['recordID']."'><input type='button' name='butm".$basketItem['recordID']."' value='-' title='Remove 1' onclick='jsnewqty(\"".$basketItem['recordID']."\",\"m\",\"".$basketItem['quantity']."\")'/>&nbsp;".number_format($basketItem['quantity'], 0)."&nbsp;<input type='button' name='buta".$basketItem['recordID']."' value='+' title='Add 1 more' onclick='jsnewqty(\"".$basketItem['recordID']."\",\"a\",\"".$basketItem['quantity']."\")' /></td>");
							
							for ($i = 0; $i < $basketItem['quantity']; $i = $i + 1)
							{
								$strordersize .= $stockDetails['deliverySize'];
							}

							$discountmark = '';
							$booldiscountapplies = false;
							if (!in_array('all', $arrdiscountcats)) //if offer doesn't apply to everything
							{
								//test if this product is in an offer
								if (in_array($basketItem['stockID'], $arrdiscountprod))
								{
									$booldiscountapplies = true;
									$decqualifytot = $decqualifytot + ($basketItem['quantity'] * $basketItem['price']);
									$discountmark = '*';
								}
								else
								{
									//$strdbsql = "SELECT * FROM stock_category_relations WHERE stockID = :stockID";
									$strdbsql = "SELECT stock_category_relations.* FROM stock_category_relations INNER JOIN stock ON stock_category_relations.stockID = stock.groupID WHERE stock.recordID = :stockID";
									$strType = "multi";
									$arrdbparams = array("stockID"=>$basketItem['stockID']);
									$stockRelations = query($conn,$strdbsql,$strType,$arrdbparams);
									
									foreach ($stockRelations AS $stockRelation) {
										if (in_array($stockRelation['categoryID'], $arrdiscountcats) && $discountmark == '')
										{
											$booldiscountapplies = true;
											$decqualifytot = $decqualifytot + ($basketItem['quantity'] * $basketItem['price']);
											$discountmark = '*';
										}
									}
								}
							}
							else
							{
								$booldiscountapplies = true;
								$decqualifytot = $decqualifytot + ($basketItem['quantity'] * $basketItem['price']);
								$discountmark = '*';
							}
							print ("<td $strtdclass align='right' id='price_".$basketItem['recordID']."' style='font-weight:bold'>&pound;&nbsp;".number_format(($basketItem['quantity'] * $basketItem['price']),2).$discountmark."</td>");
							print ("</tr>");
							$dectotcost += $basketItem['quantity'] * $basketItem['price'];
							$intnoitems += $basketItem['quantity'];
						}
						print ("<tr>");
							if ($booldiscountapplies) print ("<td class='linehead' id='ajax_getsdiscount' colspan='3'>*Item qualifies for discount</td>");
							else print ("<td class='linehead' colspan='3' id='ajax_getsdiscount' > </td>");
							print ("<td class='linehead'>Sub Total:</td>");
							print ("<td class='linehead' id='ajax_items'>".$intnoitems." Items</td>");
							print ("<td class='linehead' align='right' id='ajax_goodstotal'>&pound;&nbsp;".number_format($dectotcost,2)."</td>");
						print ("</tr>");
					print ("</tbody>");
				print ("</table>");
				print ("<br/>");							
				
				//enter special offers
				print ("<table style='float:left; margin: 20px 0;' cellspacing='0' cellpadding='3' id='offertable'>");
					print ("<tbody>");
						print ("<tr>");
							print ("<td class='linehead'>Special Offers</td>");
							print ("<td class='linehead' align='right'>Value</td>");
						print ("</tr>");
						print ("<tr>");
						if ($strcode == "") {
							print ("<td class='line' colspan='2' id='ajax_offertext'>".$strdiscountdesc.".</td></tr><tr>");
							print ("<td class='line' id='ajax_offercode'></td>");
							print ("<td class='line' align='right' id='ajax_offeramt'></td>");
							print ("</tr>");
							print ("<tr>");
							print ("<td class='line' ><input type='text' name='discount'></td>");
							print ("<td class='line' align='right'><a class='short_button' onclick='jsoffer()'>Process Code</a></td>");
						}
						else
						{
							if ($decqualifytot < $decminamt && $decminamt > 0)
							{
								$strdbsql = "UPDATE basket_header SET offerCodeID = 0 WHERE recordID = :basketID";
								$strType = "update";
								$arrdbparams = array("basketID"=>$basketHeader['recordID']);
								$updateResult = query($conn,$strdbsql,$strType,$arrdbparams);

								print ("<td class='line' colspan='2' id='ajax_offertext'><span style='color:red'>You need to spent a minimum of &pound;".number_format($decminamt,2)." on qualifying products to recieve this discount. (You are only spending &pound;".number_format($decqualifytot,2)."  on qualifying products.)</span></td></tr><tr>");
								print ("<td class='line' id='ajax_offercode'></td>");
								print ("<td class='line' align='right' id='ajax_offeramt'></td>");
								print ("</tr><tr><td class='line'><input type='text' name='discount'></td>");
								print ("<td class='line' align='right'><a href='#' class='short_button' onclick='jsoffer()'>Process Code</a></td>");

							} else if ($booldiscountapplies) {
								
								switch($strtype)
								{
									case 'postage': $decdiscount = $decpostageamt; break;
									case 'percent': $decdiscount = $decqualifytot*($decdisvalue/100); break;
									case 'pound': $decdiscount = $decdisvalue; break;
								}

								print ("<td class='line' colspan='2' id='ajax_offertext'>".$strdiscountdesc.".</td></tr><tr>");
								print ("<td class='line' id='ajax_offercode'>".$strcode."</td>");
								print ("<td class='line' align='right' id='ajax_offeramt'> - &pound; ".number_format($decdiscount, 2)."</td>");
								print ("</tr><tr><td class='line'><input type='text' name='discount'></td>");
								print ("<td class='line' align='right'><a href='#' class='short_button' onclick='jsoffer()'>Process Code</a></td>");

								$dectotcost = $dectotcost - $decdiscount;
							}
							else
							{
								$strdbsql = "UPDATE basket_header SET offerCodeID = 0 WHERE recordID = :basketID";
								$strType = "update";
								$arrdbparams = array("basketID"=>$basketHeader['recordID']);
								$updateResult = query($conn,$strdbsql,$strType,$arrdbparams);

								print ("<td class='line' colspan='2' id='ajax_offertext'><span style='color:red'>This order does not qualify for this discount.</span></td></tr><tr>");
								print ("<td class='line' id='ajax_offercode'></td>");
								print ("<td class='line' align='right' id='ajax_offeramt'></td>");
								print ("</tr><tr><td class='line'><input type='text' name='discount'></td>");
								print ("<td class='line' align='right'><a href='#' class='short_button' onclick='jsoffer()'>Process Code</a></td>");
							}
						}
						print ("</tr>");
					print ("</tbody>");
				print ("</table>");
				
				//get VAT rate
				$strdbsql = "SELECT * FROM site_lookup WHERE description = 'VAT'";
				$strType = "single";
				$arrdbparams = array();
				$vatDetails = query($conn,$strdbsql,$strType,$arrdbparams);
				$decvatrate = $vatDetails['value'];
				$decvatamt = $dectotcost * ($decvatrate/100);
				
				print ("<table id='totaltable'  style='float:right; clear:right; margin: 20px 0;' cellspacing='0' cellpadding='3'>");
					print ("<tbody>");
						print ("<tr>");
							print ("<td class='linehead'>&nbsp;</td>");
							print ("<td class='linehead' align='right'>Price</td>");
						print ("</tr>");
						print ("<tr>");
							print ("<td class='line'>Sub Total:</td>");
							print ("<td class='line' align='right' id='ajax_subtotal'>&pound;&nbsp;".number_format($dectotcost,2)."</td>");
						print ("</tr>");
						$dectotcost = $dectotcost + $decvatamt;
						print ("<tr>");
							print ("<td class='linehead'>Total:</td>");
							print ("<td class='linehead' align='right' id='ajax_total'>&pound;&nbsp;".number_format($dectotcost,2)."</td>");
						print ("</tr>");
					print ("</tbody>");
				print ("</table>");
				print ("<div style='clear:both;' class='basketOptions'>");
					print ("<div class='leftOption'><a class='short_button' href='/'>Continue Shopping</a></div>");
					print ("<div class='rightOption'><a class='short_button continue' href='#' onclick='fnLoadAccountTypePage(); return false;' >Continue to Checkout</a></div>");
				print ("</div>");
			print ("</div>");
		}
		else
		{
			print ("<div id='basketEmpty' class='basketDetail'>");
				print ("<div class='notification-info'>");
					print ("<h3>Your Basket is empty</h3><p>Please return to the shop and add some items to the shopping basket. If you are having difficulties adding items please contact us on ".$companyPhone." or email ".$companyEmail."</p>");
					print ("<a class='short_button' href='/'>Return to shop</a>");
				print ("</div>");
			print ("</div>");
		}
	}
	function fnGetAccountType ($strloginmessage) {
		
		if(isset($strloginmessage) && $strloginmessage != "") print($strloginmessage);

		print ("<div class='extCust'>\n");
			print ("<div style='min-height: 170px;'><h3 style='margin-bottom: 8px;'>Existing Customers</h3>\n");
			print ("<label style='display:inline-block;width:70px'>Username: </label><input type='text' style='width: 180px;' name='user' />");
			print ("<label style='display:inline-block;width:70px'>Password: </label><input type='password' style='width: 180px;' name='password' /></p>");
			//print ("<p><a style='font-size:10px;' href='#' onclick='fnForgotPassword();return false;' >Forgotten your password?</a></p>");
			print ("</div>\n");
			print ("<a style='margin:0; float:right;' class='short_button continue' href='#' onclick='fnLogin();return false;'>Login</a>\n");
		print ("</div>\n");

		print ("<div class='newCust'>\n");
			print ("<div style='min-height: 170px;'><h3 style='margin-bottom: 0;'>New Customer</h3>\n");
			print ("<p>Create a new customer account with us which will make it easier to order in the future. Click the button below to continue.</p></div>");
			print ("<a style='margin:0; float:right;' class='short_button continue' href='#' onclick='fnLoadAccountDetailsPage(1);return false;'>Register for an Account</a>\n");
		print ("</div>\n");
		print ("<div class='guestCust'>\n");
			print ("<div style='min-height: 170px;'><h3 style='margin-bottom: 0;'>Checkout as a Guest</h3>\n");
			print ("<p>You can now checkout as a guest user without having to create an account. Click the button below to continue.</p></div>");
			print ("<a style='margin:0; float:right;' class='short_button continue' href='#' onclick='fnLoadAccountDetailsPage(0);return false;'>Continue as a Guest</a>\n");
		print ("</div>\n");
	}
	function fnGetAccountDetails($accountType, $POSTDATA, $conn){
						
		print ("<div style='display:block;padding:10px;'>\n");

			if(!isset($POSTDATA['addtype']) || $POSTDATA['addtype'] == 0) {
				$straddchecked = "checked='checked'";
				$strclass="-hidden";
				$intaddtype = 0;
			} else {
				$straddchecked = "";
				$strclass="-show";
				$intaddtype = 1;
			}
			if(!isset($POSTDATA['regtype']) || $POSTDATA['regtype'] == 1) {
				$strregchecked = "checked='checked'";
				$strregclass="-show";
				$intregtype = 1;
			} else {
				$strregchecked = "";
				$strregclass="-hidden";
				$intregtype = 0;
			}

			print ("<input type='hidden' name='addtype' value='".$intaddtype."'/>");
			print ("<input type='hidden' name='regtype' value='".$intregtype."'/>");
			print ("<div style='width:100%;overflow:auto;'></div>");
			print("<div style='overflow:auto;'>");
					print("<div id='deladdress' style='float:left; width:460px;'>");
					print ("<h3>Delivery Details</h3>");
					print ("<label for='title' style='display:inline-block;width:110px'>Title</label><input id='title' name='title' size='4' value='".$POSTDATA['title']."'/><br/>\n");
					print ("<label for='firstname' style='display:inline-block;width:110px'>First name <span style='color:red;'>*</span></label><input id='firstname' name='firstname' value='".$POSTDATA['firstname']."'/><br/>\n");
					print ("<label for='surname' style='display:inline-block;width:110px;'>Surname <span style='color:red;'>*</span></label><input id='surname' name='surname' value='".$POSTDATA['surname']."'/><br/><br/>\n");
					print ("<label for='postcode' style='display:inline-block;width:110px'>Post Code <span style='color:red;'>*</span></label><input id='postcode' name='postcode' value='".strip_tags($POSTDATA['postcode'])."'/><a class='short_button' style='display: inline;' href='Javascript:jsfindaddress()'/>Find your Address</a>\n<br/>\n");
					print ("<div id='ajax_address'></div><br/>");
					print ("<label for='address' style='display:inline-block;width:110px'>Address <span style='color:red;'>*</span></label><input id='address' name='address' value='".strip_tags($POSTDATA['address'])."' /><br/>\n");
					print ("<label for='address2' style='display:inline-block;width:110px'>Address 2</label><input id='address2' name='address2' value='".strip_tags($POSTDATA['address2'])."' /><br/>\n");
					print ("<label for='address3' style='display:inline-block;width:110px'>Address 3</label><input id='address3' name='address3' value='".strip_tags($POSTDATA['address3'])."' /><br/>\n");
					print ("<label for='town' style='display:inline-block;width:110px'>Town</label><input id='town' name='town' value='".strip_tags($POSTDATA['town'])."' /><br/>\n");
					print ("<label for='county' style='display:inline-block;width:110px'>County <span style='color:red;'>*</span></label><input id='county' name='county' value='".strip_tags($POSTDATA['county'])."' /><br/>\n");
					print ("<label for='tel' style='display:inline-block;width:110px'>Tel <span style='color:red;'>*</span></label><input id='tel' name='tel' value='".$POSTDATA['tel']."'/><br/>\n");
					print ("<label for='email' style='display:inline-block;width:110px'>Email <span style='color:red;'>*</span></label><input id='email' name='email' value='".$POSTDATA['email']."'/><br/>\n");
					print ("<label for='cemail' style='display:inline-block;width:110px'>Confirm Email <span style='color:red;'>*</span></label><input id='cemail' name='cemail' value='".$POSTDATA['cemail']."'/><br/><br/>\n");
					print ("<div><label for='addcheck' style='display:inline-block;'>Billing address is the same as delivery address </label><input type='checkbox' id='addcheck' name='addcheck' $straddchecked onchange='jsaddtype(this)'/></div>\n");
				print("</div>");

				print ("<div style='float:left; width:450px;' class='combi$strclass' id='billaddress'>");
					print ("<h3>Billing Address</h3>");
					print ("<label for='btitle' style='display:inline-block;width:110px'>Title</label><input id='btitle' name='btitle' size='4' value='".$POSTDATA['btitle']."'/><br/>\n");
					print ("<label for='bfirstname' style='display:inline-block;width:110px'>First name <span style='color:red;'>*</span></label><input id='bfirstname' name='bfirstname' value='".$POSTDATA['bfirstname']."'/><br/>\n");
					print ("<label for='bsurname' style='display:inline-block;width:110px;'>Surname <span style='color:red;'>*</span></label><input id='bsurname' name='bsurname' value='".$POSTDATA['bsurname']."'/><br/><br/>\n");
					print ("<label for='bpostcode' style='display:inline-block;width:110px'>Post Code <span style='color:red;'>*</span></label><input id='bpostcode' name='bpostcode' value='".strip_tags($POSTDATA['bpostcode'])."'/><a style='display: inline;' class='short_button' href='Javascript:jsfindbaddress()'/>Find your Address</a>\n<br/>\n");
					print ("<div id='ajax_baddress'></div><br/>");
					print ("<label for='baddress' style='display:inline-block;width:110px'>Address <span style='color:red;'>*</span></label><input id='baddress' name='baddress' value='".strip_tags($POSTDATA['baddress'])."' /><br/>\n");
					print ("<label for='baddress2' style='display:inline-block;width:110px'>Address 2</label><input id='baddress2' name='baddress2' value='".strip_tags($POSTDATA['baddress2'])."' /><br/>\n");
					print ("<label for='baddress3' style='display:inline-block;width:110px'>Address 3</label><input id='baddress3' name='baddress3' value='".strip_tags($POSTDATA['baddress3'])."' /><br/>\n");
					print ("<label for='btown' style='display:inline-block;width:110px'>Town</label><input id='btown' name='btown' value='".strip_tags($POSTDATA['btown'])."' /><br/>\n");
					print ("<label for='bcounty' style='display:inline-block;width:110px'>County <span style='color:red;'>*</span></label><input id='bcounty' name='bcounty' value='".strip_tags($POSTDATA['bcounty'])."' /><br/>\n");
				print("</div>");
			print("</div>");
		
			if ($accountType == 1) { //register for account
				print ("<div style='float:left; width:460px;' class='guest$strregclass'>");
					print("<h3>Account Details</h3>");
					print ("<label for='username' style='display:inline-block;width:134px'>Username <span style='color:red;'>*</span></label><input name='username' value='".$POSTDATA['username']."' /><br/>\n");
					print ("<label for='password' style='display:inline-block;width:134px'>Password <span style='color:red;'>*</span></label><input name='password' type='password'/><br/>\n");
					print ("<label for='cpassword' style='display:inline-block;width:134px'>Confirm Password <span style='color:red;'>*</span></label><input name='cpassword' type='password' /><br/>\n");
					print ("<br/><label for='newsletter' >Sign up for our newsletter</label><input type='checkbox' value='1' id='newsletter' name='newsletter' checked='checked' />\n");
				print("</div>");
			}
			
			print("<span class='buttons' style='width:100%; clear:right; margin-top:20px;'>");
				print("<a style='float:left;'class='short_button' href='#' onclick='fnLoadAccountTypePage();return false;'>Change Account Type</a>");
				print("<a style='float:right;' class='short_button continue' href='#' onclick='fnSaveAccountDetails(); return false;'>Continue</a>");
			print("</span>");
		print ("</div>");
		
		print ("<style type='text/css'>
		.combi-show, .guest-show {display:block;}
		.combi-hidden, .guest-hidden {display:none;}
		</style>");
	}
	function fnUpdateBasketHeader ($intbasketid, $intuserid, $REQUEST, $conn) {
		
		$billingDetails = array();
		$deliveryDetails = array();
		$contactDetails = array();
		if ($intuserid == 1) { //guest so use the $REQUEST info
			
			$contactDetails['company'] = $REQUEST['company'];
			$contactDetails['telephone'] = $REQUEST['tel'];
			$contactDetails['mobile'] = "";
			$contactDetails['email'] = $REQUEST['email'];
			$contactDetails['fax'] = "";
			$contactDetails['customerID'] = 1;
			$contactDetails['customerName'] = "Guest";
			
			$deliveryDetails['recordID'] = 0;
			$deliveryDetails['customerID'] = 1;
			$deliveryDetails['addDescription'] = "";
			$deliveryDetails['title'] = $REQUEST['title'];
			$deliveryDetails['firstname'] = $REQUEST['firstname'];
			$deliveryDetails['surname'] = $REQUEST['surname'];
			$deliveryDetails['add1'] = $REQUEST['address'];
			$deliveryDetails['add2'] = $REQUEST['address2'];
			$deliveryDetails['add3'] = $REQUEST['address3'];
			$deliveryDetails['town'] = $REQUEST['town'];
			$deliveryDetails['county'] = $REQUEST['county'];
			$deliveryDetails['country'] = "";
			$deliveryDetails['postcode'] = $REQUEST['postcode'];
			$deliveryDetails['notes'] = "";
			
			if(isset($REQUEST['addcheck']))
			{
				$billingDetails['recordID'] = 0;
				$billingDetails['customerID'] = 1;
				$billingDetails['addDescription'] = "";
				$billingDetails['title'] = $REQUEST['title'];
				$billingDetails['firstname'] = $REQUEST['firstname'];
				$billingDetails['surname'] = $REQUEST['surname'];
				$billingDetails['add1'] = $REQUEST['address'];
				$billingDetails['add2'] = $REQUEST['address2'];
				$billingDetails['add3'] = $REQUEST['address3'];
				$billingDetails['town'] = $REQUEST['town'];
				$billingDetails['county'] = $REQUEST['county'];
				$billingDetails['country'] = "";
				$billingDetails['postcode'] = $REQUEST['postcode'];
				$billingDetails['notes'] = "";
			}
			else
			{
				$billingDetails['recordID'] = 0;
				$billingDetails['customerID'] = 1;
				$billingDetails['addDescription'] = "";
				$billingDetails['title'] = $REQUEST['btitle'];
				$billingDetails['firstname'] = $REQUEST['bfirstname'];
				$billingDetails['surname'] = $REQUEST['bsurname'];
				$billingDetails['add1'] = $REQUEST['baddress'];
				$billingDetails['add2'] = $REQUEST['baddress2'];
				$billingDetails['add3'] = $REQUEST['baddress3'];
				$billingDetails['town'] = $REQUEST['btown'];
				$billingDetails['county'] = $REQUEST['bcounty'];
				$billingDetails['country'] = "";
				$billingDetails['postcode'] = $REQUEST['bpostcode'];
				$billingDetails['notes'] = "";
			}
			
		} else {
			$strdbsql = "SELECT customer_address.* FROM customer INNER JOIN customer_address ON customer.defaultBillingAdd = customer_address.recordID WHERE customer.recordID = :customerID";
			$strType = "single";
			$arrdbparams = array("customerID"=>$intuserid);
			$billingDetails = query($conn,$strdbsql,$strType,$arrdbparams);
			
			$strdbsql = "SELECT customer_address.* FROM customer INNER JOIN customer_address ON customer.defaultDeliveryAdd = customer_address.recordID WHERE customer.recordID = :customerID";
			$strType = "single";
			$arrdbparams = array("customerID"=>$intuserid);
			$deliveryDetails = query($conn,$strdbsql,$strType,$arrdbparams);
			
			$strdbsql = "SELECT username AS customerName, recordID AS customerID, company, telephone, mobile, email, fax FROM customer WHERE recordID = :customerID";
			$strType = "single";
			$arrdbparams = array("customerID"=>$intuserid);
			$contactDetails = query($conn,$strdbsql,$strType,$arrdbparams);
		}
		
		//update basket header
		$strdbsql = "UPDATE basket_header SET billingAddressArray = :billingDetails, deliveryAddressArray = :deliveryDetails, contactDetailsArray = :contactDetails, customerID = :customerID, modifiedTimestamp = UNIX_TIMESTAMP() WHERE recordID = :basketID";
		$strType = "update";
		$arrdbparams = array("basketID"=>$intbasketid, "customerID"=>$intuserid, "contactDetails"=>serialize($contactDetails), "deliveryDetails"=>serialize($deliveryDetails), "billingDetails"=>serialize($billingDetails));
		$resultdata = query($conn,$strdbsql,$strType,$arrdbparams);
	}
	
	function fnGetFinalCustomerDetails($basketHeader,$intuserid, $conn){
	
		$assignAddressDetails = false;
		$assignAddressParams = array();
		$strBaskHeadUpdateCols = "";
		
		if($basketHeader['contactDetailsArray'] != null)
		{
			$contactDetails = unserialize($basketHeader['contactDetailsArray']);
		}
		else
		{
			$getContactDetails = "SELECT username AS customerName, recordID AS customerID, company, telephone, mobile, email, fax FROM customer WHERE recordID = :customerID";
			$strType = "single";
			$arrdbparams = array("customerID"=>$intuserid);
			$contactDetails = query($conn,$getContactDetails,$strType,$arrdbparams);
			//var_dump($contactDetails);
			print ("<br/><br/>");
			$assignAddressParams['contactDetails'] = serialize($contactDetails);
			$strBaskHeadUpdateCols .= "contactDetailsArray = :contactDetails";
			$assignAddressDetails = true;
		}
		if($basketHeader['billingAddressArray'] != null)
		{
			$billingAddressDetails = unserialize($basketHeader['billingAddressArray']);
		}
		else
		{
			$getBillingAddressDetails = "SELECT customer_address.* FROM customer INNER JOIN customer_address ON customer.defaultBillingAdd = customer_address.recordID WHERE customer.recordID = :customerID";
			$strType = "single";
			$arrdbparams = array("customerID"=>$intuserid);
			$billingAddressDetails = query($conn,$getBillingAddressDetails,$strType,$arrdbparams);
			//var_dump($billingAddressDetails);
			print ("<br/><br/>");
			$assignAddressParams['billingAddressDetails'] = serialize($billingAddressDetails);
			if($assignAddressDetails)
			{
				$strBaskHeadUpdateCols .= ", ";
			}
			$strBaskHeadUpdateCols .= "billingAddressArray = :billingAddressDetails ";
			$assignAddressDetails = true;
		}
		if($basketHeader['deliveryAddressArray'] != null)
		{
			$deliveryAddressDetails = unserialize($basketHeader['deliveryAddressArray']);
		}
		else
		{
			$getDeliveryAddressDetails = "SELECT customer_address.* FROM customer INNER JOIN customer_address ON customer.defaultDeliveryAdd = customer_address.recordID WHERE customer.recordID = :customerID";
			$strType = "single";
			$arrdbparams = array("customerID"=>$intuserid);
			$deliveryAddressDetails = query($conn,$getDeliveryAddressDetails,$strType,$arrdbparams);
			//var_dump($deliveryAddressDetails);
			print ("<br/><br/>");
			$assignAddressParams['deliveryAddressDetails'] = serialize($deliveryAddressDetails);
			if($assignAddressDetails)
			{
				$strBaskHeadUpdateCols .= ", ";
			}
			$strBaskHeadUpdateCols .= "deliveryAddressArray = :deliveryAddressDetails";
			$assignAddressDetails = true;
		}
		
		if($assignAddressDetails)
		{
			print ("update basket header with addresses");
			$assignAddressDetailsQuery = "UPDATE basket_header SET ".$strBaskHeadUpdateCols." WHERE recordID = :baskHeaderID";
			$strType = "update";
			$assignAddressParams['baskHeaderID'] = $basketHeader['recordID'];
			$assignAddressDetails = query($conn, $assignAddressDetailsQuery, $strType, $assignAddressParams);
		}
		/*var_dump($billingAddressDetails);
		print ("<br/><br/>");
		var_dump($deliveryAddressDetails);
		print ("<br/><br/>");
		var_dump($contactDetails);*/
		print ("<div style='overflow:hidden;'>");
				print ("<table cellspacing='0' cellpadding='3' id='custDetailsTable'>");
					print ("<tbody><tr>");
					print ("<td class='linehead'>Customer Details</td>");
					print ("<td class='linehead'>Billing Details</td>");
					print ("<td class='linehead'>Delivery Details</td>");
					print("</tr><tr>");
					print ("<td class='line'>".$contactDetails['customerName']."</td>");
					print("<td class='line'>");
						print ("<div id='current_bil'><address class='accountaddress'>");
						print (trim($billingAddressDetails['title']." ".$billingAddressDetails['firstname']." ".$billingAddressDetails['surname'])."<br/>");
						if($billingAddressDetails['add1'] != "") print ($billingAddressDetails['add1']."<br />");
						if($billingAddressDetails['add2'] != "") print ($billingAddressDetails['add2']."<br />");
						if($billingAddressDetails['add3'] != "") print ($billingAddressDetails['add3']."<br />");
						print ($billingAddressDetails['town']."<br />");
						print ($billingAddressDetails['county']."<br />");
						print ($billingAddressDetails['postcode']."</address><br /></div>");

						print ("<input type='hidden' name='account' value='$intuserid' />");
						print ("<div id='hidden_bil' style='display:none;top:20px;left:0px;padding:5px;text-align:left;width:320px;border:solid 1px #11406A; background-color: #ACCAE2'>");
							print ("<label for='btitle' style='display:inline-block;width:110px'>Title</label><input id='btitle' name='btitle' value='".$billingAddressDetails['title']."'/><br/>\n");
								print ("<label for='bfirstname' style='display:inline-block;width:110px'>First Name*</label><input id='bfirstname' name='bfirstname' value='".$billingAddressDetails['firstname']."'/><br/>\n");
								print ("<label for='bsurname' style='display:inline-block;width:110px'>Surname*</label><input id='bsurname' name='bsurname' value='".$billingAddressDetails['surname']."'/><br/>\n");
								print ("<label for='baddress1' style='display:inline-block;width:110px'>Address 1*</label><input id='baddress1' name='baddress1' value='".$billingAddressDetails['add1']."'/><br/>\n");
								print ("<label for='baddress2' style='display:inline-block;width:110px'>Address 2</label><input id='baddress2' name='baddress2' value='".$billingAddressDetails['add2']."'/><br/>\n");
								print ("<label for='baddress3' style='display:inline-block;width:110px'>Address 3</label><input id='baddress3' name='baddress3' value='".$billingAddressDetails['add3']."'/><br/>\n");
								print ("<label for='btown' style='display:inline-block;width:110px'>Town*</label><input id='btown' name='btown' value='".$billingAddressDetails['town']."' /><br/>\n");
								print ("<label for='bcounty' style='display:inline-block;width:110px'>County</label><input id='bcounty' name='bcounty' value='".$billingAddressDetails['county']."'/><br/>\n");
								print ("<label for='bpostcode' style='display:inline-block;width:110px'>Post Code*</label><input id='bpostcode' name='bpostcode' value='".$billingAddressDetails['postcode']."'/><br/><br/>\n");
								if (strtoupper(substr($billingAddressDetails['postcode'], 0, 2)) == "IM" || strtoupper(substr($billingAddressDetails['postcode'], 0, 2)) == "BT" ) print ("<script type='text/javascript'>alert('Your postcode indicates you are in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.'); window.location = 'http://jhbenson.co.uk/checkout.php';</script>\n");
							print ("<a class='short_button' onclick='if(jsupdate(\"changebilladdress\")) jsupdateadd(\"bill\");' >Update</a>");
							print ("<a class='short_button' onclick='jscancel(\"bil\");return false;' >Cancel</a>\n");
						print("</div>\n");
					print("</td>");
					
					print("<td class='line'>");
						print ("<div id='current_del'><address class='accountaddress'>");
						print (trim($deliveryAddressDetails['title']." ".$deliveryAddressDetails['firstname']." ".$deliveryAddressDetails['surname'])."<br/>");
						if($deliveryAddressDetails['add1'] != "") print ($deliveryAddressDetails['add1']."<br />");
						if($deliveryAddressDetails['add2'] != "") print ($deliveryAddressDetails['add2']."<br />");
						if($deliveryAddressDetails['add3'] != "") print ($deliveryAddressDetails['add3']."<br />");
						print ($deliveryAddressDetails['town']."<br />");
						print ($deliveryAddressDetails['county']."<br />");
						print ($deliveryAddressDetails['postcode']."</address><br /></div>");

						print ("<div id='hidden_del' style='display:none;top:20px;left:0px;padding:5px;text-align:left;width:320px;border:solid 1px #11406A; background-color: #ACCAE2'>");
								print ("<label for='title' style='display:inline-block;width:110px'>Title</label><input id='title' name='title' value='".$deliveryAddressDetails['title']."'/><br/>\n");
								print ("<label for='firstname' style='display:inline-block;width:110px'>First Name*</label><input id='firstname' name='firstname' value='".$deliveryAddressDetails['firstname']."'/><br/>\n");
								print ("<label for='surname' style='display:inline-block;width:110px'>Surname*</label><input id='surname' name='surname' value='".$deliveryAddressDetails['surname']."'/><br/>\n");
								print ("<label for='address1' style='display:inline-block;width:110px'>Address 1*</label><input id='address1' name='address1' value='".$deliveryAddressDetails['add1']."'/><br/>\n");
								print ("<label for='address2' style='display:inline-block;width:110px'>Address 2</label><input id='address2' name='address2' value='".$deliveryAddressDetails['add2']."'/><br/>\n");
								print ("<label for='address3' style='display:inline-block;width:110px'>Address 3</label><input id='address3' name='address3' value='".$deliveryAddressDetails['add3']."'/><br/>\n");
								print ("<label for='town' style='display:inline-block;width:110px'>Town*</label><input id='town' name='town' value='".$deliveryAddressDetails['town']."' /><br/>\n");
								print ("<label for='county' style='display:inline-block;width:110px'>County</label><input id='county' name='county' value='".$deliveryAddressDetails['county']."'/><br/>\n");
								print ("<label for='postcode' style='display:inline-block;width:110px'>Post Code*</label><input id='postcode' name='postcode' value='".$deliveryAddressDetails['postcode']."'/><br/><br/>\n");
								if (strtoupper(substr($deliveryAddressDetails['postcode'], 0, 2)) == "IM" || strtoupper(substr($deliveryAddressDetails['postcode'], 0, 2)) == "BT" ) print ("<script type='text/javascript'>alert('Your postcode indicates you are in an area where extra postage costs will apply. Please contact 01229 820679 to discuss this order.'); window.location = 'http://jhbenson.co.uk/checkout.php';</script>\n");
							print ("<a class='short_button' onclick='if(jsupdate(\"changedeladdress\")) jsupdateadd(\"del\");' >Update</a>");
							print ("<a class='short_button' type='button' onclick='jscancel(\"del\");return false;'>Cancel</a>\n");
						print("</div>");
					print("</td>");
					print("</tr><tr>");
					print ("<td class='line'></td><td class='line'><a style='margin:0;' class='short_button' href='javascript:jsaddress(\"bil\")'>Change Billing Address</a></td>");
					print ("<td class='line'><a style='margin:0;' class='short_button' href='javascript:jsaddress(\"del\")'>Change Delivery Address</a></td>");
					print("</tr></tbody></table>");
			
			$delpostcode = $deliveryAddressDetails['postcode'];
		print ("</div>");
	}
	function fnGetFinalBasket($basketHeader,$basketItems,$strselectedpostage, $strView, $conn){

		//get offer code details
		if ($basketHeader['offerCodeID'] > 0) {
			
			$strdbsql = "SELECT * FROM site_offer_codes WHERE recordID = :offerCodeID";
			$strType = "single";
			$arrdbparams = array("offerCodeID"=>$basketHeader['offerCodeID']);
			$siteOffers = query($conn,$strdbsql,$strType,$arrdbparams);
			
			$strcode = $siteOffers['code'];
			$strtype = $siteOffers['discountType'];
			$decdisvalue = $siteOffers['discountAmount'];
			$decminamt = $siteOffers['minimumSpend'];
			$strdiscountdesc = $siteOffers['description'];
			$arrdiscountcats = explode(" ", $siteOffers['categoryList']);
			$arrdiscountprod = explode(" ", $siteOffers['stockList']);
		
		}
		
		$strdbsql = "SELECT * FROM site_postage WHERE isActive = 1";
		$strType = "multi";
		$arrdbparams = array();
		$postageOffers = query($conn,$strdbsql,$strType,$arrdbparams);
		
		if (count($basketItems) > 0) {
			
			$deliveryAddressDetails = unserialize($basketHeader['deliveryAddressArray']);
			$delpostcode = $deliveryAddressDetails['postcode'];
			
			print ("<div style='clear:both;'>");
				print ("<table cellspacing='0' cellpadding='3' id='baskettable'>");
					print ("<tbody>");
						print ("<tr>");
							print ("<td class='linehead' style='width: 10%;'>Stockcode</td>");
							print ("<td class='linehead' style='width: 30%;'>Name</td>");
							print ("<td class='linehead' style='width: 30%;'>Details</td>");
							print ("<td class='linehead' style='width: 15%;'>Quantity</td>");
							print ("<td class='linehead' align='right' style='width: 15%;'>Price</td>");
						print ("</tr>");
					
						$strordersize = "";
						$blnoddeven = false;
						$booldiscountapplies = false;
						$dectotcost = 0;
						$intnoitems = 0;
						foreach ($basketItems AS $basketItem) {
						
							//styling for changing rows
							if ($blnoddeven) $strtdclass = "class='evenrow line'";
							else $strtdclass = "class='line'";											
							$blnoddeven = !$blnoddeven;
							
							//get stock image
							$strdbsql = "SELECT * FROM stock_images WHERE stockID = :stockID ORDER BY imageOrder ASC LIMIT 1";
							$strType = "single";
							$arrdbparams = array("stockID"=>$basketItem['stockID']);
							$stockImage = query($conn,$strdbsql,$strType,$arrdbparams);
							
							if ($stockImage['imageLink'] != ""){
								$strimgpath = "/images/product_images/".$basketItem['stockID']."/list/".$stockImage['imageLink'];
							} else {
								$strimgpath = "/images/no_image.jpg";
							}
							
							//unserialize stock details
							$stockDetails = unserialize($basketItem['stockDetailsArray']);


							print ("<tr id='row_".$basketItem['recordID']."'>");
							print ("<td $strtdclass>".$stockDetails['stockCode']."</td>");
							print ("<td $strtdclass>".$stockDetails['name']."</td>");
							print ("<td $strtdclass>");

							if ($stockDetails['width'] != "") print ("Width: ".$stockDetails['width']."<br/>");
							if ($stockDetails['height'] != "") print ("Height: ".$stockDetails['height']."<br/>");
							if ($stockDetails['depth'] != "") print ("Depth: ".$stockDetails['depth']."<br/>");
							if ($stockDetails['diameter'] != "") print ("Diameter: ".$stockDetails['diameter']."<br/>");
							if ($stockDetails['description'] != "") print ("Dimensions: ".$stockDetails['description']."<br/>");
							if (isset($stockDetails['Finish']) && $stockDetails['Finish']['description'] != "") print ("Finish: ".$stockDetails['Finish']['description']."<br/>");
							if (isset($stockDetails['Colour']) && $stockDetails['Colour']['description'] != "") print ("Colour: ".$stockDetails['Colour']['description']."<br/>");
							if (isset($stockDetails['Fabric']) && $stockDetails['Fabric']['description'] != "") print ("Fabric: ".$stockDetails['Fabric']['description']."<br/>");
							if (isset($stockDetails['Base']) && $stockDetails['Base']['description'] != "") print ("Base: ".$stockDetails['Base']['description']."<br/>");

							print ("</td>");
							print ("<td $strtdclass id='qty_".$basketItem['recordID']."'>&nbsp;".$basketItem['quantity']."&nbsp;</td>");
							
							for ($i = 0; $i < $basketItem['quantity']; $i = $i + 1)
							{
								$strordersize .= $stockDetails['deliverySize'];
							}

							$discountmark = '';
							if (!in_array('all', $arrdiscountcats)) //if offer doesn't apply to everything
							{
								//test if this product is in an offer
								if (in_array($stockDetails['stockCode'], $arrdiscountprod))
								{
									$booldiscountapplies = true;
									$decqualifytot = $decqualifytot + ($basketItem['quantity'] * $basketItem['price']);
									$discountmark = '*';
								}
								else
								{
									$strdbsql = "SELECT * FROM stock_category_relations WHERE stockID = :stockID";
									$strType = "multi";
									$arrdbparams = array("stockID"=>$basketItem['stockID']);
									$stockRelations = query($conn,$strdbsql,$strType,$arrdbparams);
									
									foreach ($stockRelations AS $stockRelation) {
										if (in_array($stockRelation['categoryID'], $arrdiscountcats) && $discountmark == '')
										{
											$booldiscountapplies = true;
											$decqualifytot = $decqualifytot + ($basketItem['quantity'] * $basketItem['price']);
											$discountmark = '*';
										}
									}
								}
							}
							else
							{
								$booldiscountapplies = true;
								$decqualifytot = $decqualifytot + ($basketItem['quantity'] * $basketItem['price']);
								$discountmark = '*';
							}
							
							
						
						
							// New postage calculator
							// Declare arrays for population later
							//$itemCategories = array();
							$mainCategories = array();
							$arrcantorderitems = array();
							
							//categories
							$strdbsql = "SELECT * FROM stock_category_relations WHERE stockID = :stockID";
							$strType = "multi";
							$arrdbparams = array("stockID"=>$stockDetails['groupID']);
							$stockCategories = query($conn,$strdbsql,$strType,$arrdbparams);
							
							foreach ($stockCategories AS $stockCategory) {
							
								$mainCategories[] = $stockCategory['categoryID'];								
								$strdbsql = "SELECT * FROM category_relations WHERE childID = :categoryID";
								$strType = "multi";
								$arrdbparams = array("categoryID"=>$stockCategory['categoryID']);
								$categoryRelations = query($conn,$strdbsql,$strType,$arrdbparams);
								
								foreach($categoryRelations AS $categoryRelation){
								
									$mainCategories[] = $categoryRelation['parentID'];
									$parentID = $categoryRelation['parentID'];
									$categoryLevel = $categoryRelation['level'];
									while($categoryLevel != 1){
										
										$strdbsql = "SELECT * FROM category_relations WHERE childID = :categoryID";
										$strType = "single";
										$arrdbparams = array("categoryID"=>$parentID);
										$categoryRelation2 = query($conn,$strdbsql,$strType,$arrdbparams);
										
										$mainCategories[] = $categoryRelation2['parentID'];
										$parentID = $categoryRelation2['parentID'];
										$categoryLevel = $categoryRelation2['level'];
									}
								}
							}
							//var_dump($mainCategories);
							
							//ranges
							$strdbsql = "SELECT rangeID FROM stock_range_relations WHERE stockID = :stockID";
							$strType = "single";
							$arrdbparams = array("stockID"=>$stockDetails['groupID']);
							$stockRange = query($conn,$strdbsql,$strType,$arrdbparams);
							
							//brands
							$strdbsql = "SELECT brandID FROM stock_group_information WHERE recordID = :stockID";
							$strType = "single";
							$arrdbparams = array("stockID"=>$stockDetails['groupID']);
							$stockBrand = query($conn,$strdbsql,$strType,$arrdbparams);
							
							// check there are postage rules set
							if(!empty($postageOffers))
							{
								// Loop over postage rules
								foreach($postageOffers AS $postageOffer)
								{
									// set to false as item not yet found to qualify for postage rule
									$boolPostageItemRule = false;
									#echo "<br/>".$postageOffer['fld_desc']."<br/>";
									// set $minspend to that of current offer
									$minspend = $postageOffer['minspend'];
									
									// determine total cost of that item by multiplying quantity by price
									$dectotcost2 = number_format(($basketItem['quantity'] * $basketItem['price']),2,".","");
									// set to false before testing
									$boolpostcodequal = false;
									
									// if there are postcodes specified by the postage rule
									if(!empty($postageOffer['postcodeList']))
									{
										// take fld_postcodes and build array from it based on separation on commas
										$arrofferpostcodes = explode(",", $postageOffer['postcodeList']);
										
										// format $delpostcode for testing - ensure string is capitalised and has no spaces
										$delpostcode = strtoupper(str_replace(" ", "", $delpostcode));
										
										// loop over post codes in postage rule
										foreach($arrofferpostcodes AS $offerpostcode)
										{
											// test for match of each post code in ofer against $delpostcode, matching 2, 3 or 4 characters based on length of $offerpostcode
											switch(strlen($offerpostcode))
											{
												case 2:
													#echo $offerpostcode . " = " . substr($delpostcode, 0, 2) . "<br/>";
													if($offerpostcode == substr($delpostcode, 0, 2)) {
														// post code in offer so $boolpostcodequal set to true
														$boolpostcodequal = true;
														#echo "true!<br/>";
													}
													break;
												case 3:
													#echo $offerpostcode . " = " . substr($delpostcode, 0, 3) . "<br/>";
													if(strlen($delpostcode) == 6 && $offerpostcode == substr($delpostcode, 0, 3)) {
														// post code in offer so $boolpostcodequal set to true
														$boolpostcodequal = true;
														#echo "true!<br/>";
													}
													break;
												case 4:
													#echo $offerpostcode . " = " . substr($delpostcode, 0, 4) . "<br/>";
													if(strlen($delpostcode) == 7 && $offerpostcode == substr($delpostcode, 0, 4)) {
														// post code in offer so $boolpostcodequal set to true
														$boolpostcodequal = true;
														#echo "true!<br/>";
													}
													break;
											}
										}
									}
									else
									{
										// no post codes specified by postage rule, so set $boolpostcodequal to true regardless
										$boolpostcodequal = true;
									}
									// test if post code qualifies
									if($boolpostcodequal) {
										// set default to false
										$boolmaincategory = false;
										$postageCatIDArr = explode(",", $postageOffer['categoryIDList']);
										$postageBrandIDArr = explode(",", $postageOffer['brandIDList']);
										$postageRangeIDArr = explode(",", $postageOffer['rangeIDList']);
										//var_dump($postageCatIDArr);
										//var_dump($mainCategories);
										//echo "<br/>";
										//echo "Boolmaincategory: ".$boolmaincategory."<br/>";
										// loop over item categories/sub categories
										foreach($mainCategories AS $mainCategory)
										{
											//echo $mainCategory."<br/>";
											// if item has a range, brand or type in offer, $boolmaincategory set to true
											if(!empty($postageOffer['categoryIDList']) && in_array($mainCategory, $postageCatIDArr)) {
												$boolmaincategory = true;
											}
										}
										
										if(!empty($postageOffer['brandIDList']) && in_array($stockBrand['brandID'], $postageBrandIDArr)) {
											$boolmaincategory = true;
										}
										
										if(!empty($postageOffer['rangeIDList']) && in_array($stockRange['rangeID'], $postageRangeIDArr)) {
											$boolmaincategory = true;
										}
										//echo "Boolmaincategory: ".$boolmaincategory."<br/>";
										
										// test $boolmaincategory true, or if item ID in postage offer product IDs, or if $itemCategory category/sub category match postage offer brand, type or range
										if($boolmaincategory || in_array($stockDetails['groupID'], explode(",", $postageOffer['stockIDList'])))
										{
											// if $productID not already equal to current product ID or $postageID not already equal to current postage rule ID - basically don't add the same item in the same rule twice
											if ($productID != $basketItem['recordID'] || $postageID != $postageOffer['recordID']) {
												
												// set $productID and $postageID to current iteration equivalents
												$productID = $basketItem['recordID'];
												$postageID = $postageOffer['recordID'];
												
												// add total for current postage rule to $postageArray (this is recorded as a running total of all products in the rule)
												$postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['total'] += $dectotcost2;
												$postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['cost'] = $postageOffer['cost'];
												$postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['minspend'] = $postageOffer['minspend'];
												$postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['products'][] = $productID;
											}
											
											// item qualifies for this postage rule
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["totalSpend"] = $postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['total'];
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["productSpend"] = $dectotcost2;
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]['minspend'] = $postageOffer['minspend'];
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]['cost'] = $postageOffer['cost'];
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]['modifierthreshold'] = $postageOffer['modifierThreshold'];
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]['modifiercost'] = $postageOffer['modifierCost'];
											if ($postageOffer['isGlobal'] == 1) $scope = "Global"; else $scope = "Specific";
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]['scope'] = $scope;
											$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]["ruleValid"] = "N";
											
											//check if rule applies if over minimum spend - may need to change if they base global rules on entire spend rather than just product spend!
											if($dectotcost2 >= $minspend || $minspend == "" || ($postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['total'] >= $minspend AND $postageOffer['isGlobal'] != 1))
											//if($dectotcost2 >= $minspend || $minspend == "" || $postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['total'] >= $minspend)
											{
												$postageItem[$productID]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]["ruleValid"] = "Y";
												
												//update all other products that have this rule:
												foreach ($postageItem AS $key => $productPostage) {
													//set valid and totalSpend
													if (isset($productPostage["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']])) {
													
														//the next line may need to be removed depending on if they want to apply the lowest per product or base it on total order value!
														if ($productPostage["Group-".$postageOffer['groupID']]["productSpend"] >= $postageOffer['minspend'] || $postageOffer['isGlobal'] != 1) {
															$postageItem[$key]["Group-".$postageOffer['groupID']]["Rule-".$postageOffer['recordID']]["ruleValid"] = "Y";
															$postageItem[$key]["Group-".$postageOffer['groupID']]["totalSpend"] = $postageArray[$postageOffer['groupID']][$postageOffer['recordID']]['total'];
														}
													}
												}
											}
										} // End of test if product, range, brand or type in offer
									} // End of test if post code qualifies
									else
									{
										// if post code does not qualify and item can only be delivered to post codes in the rule, add item to $arrcantorderitems
										if($postageOffer['isLocalDeliveryOnly'] == 1 && in_array($basketItem['stockID'], explode(",", $postageOffer['stockIDList'])))
										{
											$arrcantorderitems[] = $stockDetails['name'];
										}
									}
								} // End of loop over postage rules
							}

							
							
							print ("<td $strtdclass align='right' id='price_".$basketItem['recordID']."' style='font-weight:bold'>&pound;&nbsp;".number_format(($basketItem['quantity'] * $basketItem['price']),2).$discountmark."</td>");
							print ("</tr>");
							$dectotcost += $basketItem['quantity'] * $basketItem['price'];
							$intnoitems += $basketItem['quantity'];

						} //end of product loop
						
						print ("<tr>");
							if ($booldiscountapplies) print ("<td class='linehead' id='ajax_getsdiscount' colspan='2'>*Item qualifies for discount</td>");
							else print ("<td class='linehead' colspan='2' id='ajax_getsdiscount' > </td>");
							print ("<td class='linehead'>Sub Total:</td>");
							print ("<td class='linehead' id='ajax_items'>".$intnoitems." Items</td>");
							print ("<td class='linehead' align='right' id='ajax_goodstotal'>&pound;&nbsp;".number_format($dectotcost,2)."</td>");
						print ("</tr>");
					print ("</tbody>");
				print ("</table>");
				print ("<br/>");	
			print ("</div>");	
		}

		
		if($strView == "checkout")
		{
		
			// declare for population in loop over postage rules
			$validPostageRates = array(); //this is an array of prices for delivery of each product
			
			//$postageItem is an array of products with all postage rules that could apply for that product
			if(isset($postageItem) && is_array($postageItem))
			{
				foreach ($postageItem AS $key => $productPostage) { //loop around all products
				
					$validProductPostageRules = array(); //reset products postage rules for each loop
					$validMinSpends = array(); //reset minimum spend for this item
				
					foreach ($productPostage AS $groupID => $productPostageGroups) { //loop around each group of each product
						foreach ($productPostageGroups AS $productPostageRules) { //loop around each rule in each group
							if (is_array($productPostageRules)) { //check the rule is an actual array as we also store some information e.g. total spend for the group
								if ($productPostageGroups['totalSpend'] >= $productPostageRules['minspend'] AND $productPostageRules['ruleValid'] == "Y") { //if the rule is valid and totalspend for this group is greater than the minimum for this rule
									
									if((empty($validProductPostageRules[$productPostageRules['scope']][$groupID]) && empty($validMinSpends[$productPostageRules['scope']][$groupID])) || $productPostageRules['minspend'] > $validMinSpends[$productPostageRules['scope']][$groupID])
									{
										$validMinSpends[$productPostageRules['scope']][$groupID] = $productPostageRules['minspend']; //keep track of the minimum spend for the current selected rule so we dont apply lower minumum spend rules
										if ($productPostageRules['modifierthreshold'] <= $dectotcost AND $productPostageRules['modifierthreshold'] > 0) {
											$validProductPostageRules[$productPostageRules['scope']][$groupID] = $productPostageRules['modifiercost']; //set cost for postage for this group 
										} else {
											$validProductPostageRules[$productPostageRules['scope']][$groupID] = $productPostageRules['cost']; //set cost for postage for this group 
										}
									}
								}
							}
						}
						
						//if this product contains specific rule groups then only add them otherwise add the global rule groups
						if (isset($validProductPostageRules['Specific'])) {
							foreach ($validProductPostageRules['Specific'] AS $validProductPostageRule) {
								$validPostageRates[] = $validProductPostageRule;
							}
						} else {
							foreach ($validProductPostageRules['Global'] AS $validProductPostageRule) {
								$validPostageRates[] = $validProductPostageRule;
							}
						}
					}
				}
			}

			// test if any postage rules qualified for
			if(empty($validPostageRates))
			{
				// if not, set to £25 default
				$postageCost = 25;
			}
			else
			{
				// otherwise get highest value from array of offers qualified for
				$postageCost = max($validPostageRates);
			}
			
			// update basket header with postage cost
			$updatePostageQuery = "UPDATE basket_header SET postageAmount = :postageAmount WHERE recordID = :basketHeaderID";
			$strType = "update";
			$arrdbparams = array(
								"postageAmount" => $postageCost,
								"basketHeaderID" => $basketHeader['recordID']
							);
			$updatePostage = query($conn, $updatePostageQuery, $strType, $arrdbparams);
			

			if(!empty($arrcantorderitems))
			{
				print ("<div class='postage'>");
				print ("<table style='float:right; clear:right; margin: 20px 0;' cellspacing='0' cellpadding='3'>");
				print ("<tbody>");
				print ("<tr>");
				print ("<td class='linehead'>Sorry - we currently only offer local delivery for the following products</td>");
				print ("</tr>");
				foreach($arrcantorderitems AS $cantorderitem)
				{
					print("<tr>");
						print("<td class='line'>" . $cantorderitem . "</td>");
					print("</tr>");
				}
				print ("<tr>");
				print ("<td><br/><p>Please <a href='/contact.php'>contact us</a> if you wish to enquire further.</p></td>");
				print ("</tr>");
				print ("</tbody>");
				print ("</table>");
				print ("</div>");
				print ("<div style='clear:right; margin-top: 20px;' ><div class='leftOption'><a class='short_button' href='http://jhbenson.co.uk/index.php'>Continue Shopping</a></div>");
				print ("<div class='rightOption'><a class='short_button' href='/checkout.php'>Return to Basket</a></div>");
				print ("</div>");
			} else {
				print ("<div class='postage'>");
				print ("<table style='float:right; clear:right; margin: 20px 0;' cellspacing='0' cellpadding='3'>");
				print ("<tbody>");
				print ("<tr>");
				print ("<td class='linehead'>Item</td>");
				print ("<td class='linehead' align='right'>Value</td>");
				print ("</tr>");
				
				if ($basketHeader['offerCodeID'] > 0) {
					$dectotcost = $dectotcost - $basketHeader['discountAmount'];
					print ("<tr><td class='line'>".$strdiscountdesc.".</td>");
					print ("<td class='line' align='right'> - &pound; ".number_format($basketHeader['discountAmount'], 2)."</td></tr>");
				}

				if ($postageCost != 0)
				{
					print ("<tr><td class='evenrow line'>Delivery</td>");
					print ("<td class='evenrow line' align='right' id='ajax_stanpostage'>&pound;&nbsp;".number_format($postageCost,2)."</td>");
					print ("</tr>");
				}
				else
				{
					print ("<tr><td class='evenrow line'>FREE DELIVERY</td>");
					print ("<td class='evenrow line' align='right' id='ajax_stanpostage'>&pound;&nbsp;".number_format($postageCost,2)."</td>");
					print ("</tr>");
				}
				$dectotcost = $dectotcost + $postageCost;
				
				//get VAT rate
				$strdbsql = "SELECT * FROM site_lookup WHERE description = 'VAT'";
				$strType = "single";
				$arrdbparams = array();
				$vatDetails = query($conn,$strdbsql,$strType,$arrdbparams);
				$decvatrate = $vatDetails['value'];
				$decvatamt = $dectotcost * ($decvatrate/100);
				$dectotcost = $dectotcost + $decvatamt;

				print ("<tr>");
				print ("<td class='linehead'>Total:</td>");
				print ("<td class='linehead' align='right'>&pound;&nbsp;".number_format($dectotcost,2)."</td>");
				print ("</tr>");
				print ("</tbody>");
				print ("</table>");
				print ("</div>");
				print ("<div style='clear:right; margin-top: 20px;' ><div class='basketOptions'><div class='leftOption'><a class='short_button' href='http://jhbenson.co.uk/index.php'>Continue Shopping</a></div>");
				print ("<div class='rightOption'><a href='pplaunch.php' class='short_button continue'>Pay at Paypal</a></div>");
				print ("<a class='short_button' href='/checkout.php'>Return to Basket</a></div>");
				print ("<br/><br/><br/><div class='basketOptions' style='text-align:right'><a href='pplaunch.php'><img style='border:0px' src='/images/horizontal_solution_PPeCheck.png'></a></div>");
				print ("</div>");
			}
			//print("</div>");
		}
		elseif($strView == "viewBasket")
		{
			print ("<div style='clear:both;overflow:hidden;'>");
				print ("<div class='leftOption'>");
					print ("<a onclick='jsdeletebasket(".$basketHeader['recordID'].")' class='short_button'>");
						print ("Delete");
					print ("</a>");
				print ("</div>");
				print ("<div class='rightOption'>");
					print ("<a onclick='jscopybasket(".$basketHeader['recordID']."); return false;' href='#' class='short_button'>");
						print ("Copy to Current Basket");
					print ("</a>");
				print ("</div>");
			print ("</div>");
		}
	}
	
	
	
	function fnUpdateBasketAfterPaypal($basketID, $orderHeaderID, $conn){
		$strdbsql = "SELECT * FROM basket_items WHERE basketHeaderID = :basketID";
		$strType = "multi";
		$arrdbparams = array("basketID"=>$basketID);
		$basketItems = query($conn,$strdbsql,$strType,$arrdbparams);

		// Move basket contents to complete folder
		foreach ($basketItems AS $basketItem) {
			
			$strdbsql = "INSERT INTO order_items (orderHeaderID, stockID, stockDetailsArray, quantity, price) VALUES (:orderHeaderID, :stockID, :stockDetailsArray, :quantity, :price);";
			$strType = "insert";
			$arrdbparams = array("orderHeaderID"=>$orderHeaderID, "stockID"=>$basketItem['stockID'], "stockDetailsArray"=>$basketItem['stockDetailsArray'], "quantity"=>$basketItem['quantity'], "price"=>$basketItem['price']);
			$insertResult = query($conn,$strdbsql,$strType,$arrdbparams);

			$strdbsql = "SELECT * FROM stock WHERE recordID = :stockID";
			$strType = "single";
			$arrdbparams = array("stockID"=>$basketItem['stockID']);
			$stockDetails = query($conn,$strdbsql,$strType,$arrdbparams);
			
			if ($stockDetails['stockLevels'] >= $basketItem['quantity']) {
				$stockLevel = $stockDetails['stockLevels'] - $basketItem['quantity'];
			} else {
				$stockLevel = 0;
			}
			
			$strdbsql = "UPDATE stock SET stockLevels = :stockLevel WHERE recordID = :stockID";
			$strType = "update";
			$arrdbparams = array("stockID"=>$basketItem['stockID'], "stockLevel"=>$stockLevel);
			$updateResult = query($conn,$strdbsql,$strType,$arrdbparams);
		}

		// Delete from basket tables
		$strdbsql = "DELETE FROM basket_items WHERE basketHeaderID = :basketID";
		$strType = "delete";
		$arrdbparams = array("basketID"=>$basketID);
		$deleteResult = query($conn,$strdbsql,$strType,$arrdbparams);
		
		$strdbsql = "DELETE FROM basket_header WHERE recordID = :basketID";
		$strType = "delete";
		$arrdbparams = array("basketID"=>$basketID);
		$deleteResult = query($conn,$strdbsql,$strType,$arrdbparams);
	}
	
	function fnSendOrderConfirmationEmail($orderHeaderID, $paymentDetailsID, $notifyEmail, $companyEmail, $conn){
		
		$getOrderHeaderQuery = "SELECT * FROM order_header WHERE recordID = :orderHeaderID";
		$strType = "single";
		$arrdbparam = array(
							"orderHeaderID" => $orderHeaderID
						);
		$orderHeader = query($conn, $getOrderHeaderQuery, $strType, $arrdbparam);
		
		
		$orderTimestamp = $orderHeader['timestampOrder'];
		$strbname = $orderHeader['addBillingName'];
		$strbadd1 = $orderHeader['addBilling1'];
		$strbadd2 = $orderHeader['addBilling2'];
		$strbadd3 = $orderHeader['addBilling3'];
		$strbadd4 = $orderHeader['addBilling4'];
		$strbadd5 = $orderHeader['addBilling5'];
		$strbpostcode = $orderHeader['addBillingPostcode'];
		$strname = $orderHeader['addDeliveryName'];
		$stradd1 = $orderHeader['addDelivery1'];
		$stradd2 = $orderHeader['addDelivery2'];
		$stradd3 = $orderHeader['addDelivery3'];
		$stradd4 = $orderHeader['addDelivery4'];
		$stradd5 = $orderHeader['addDelivery5'];
		$strpostcode = $orderHeader['addDeliveryPostcode'];
		$stremail = $orderHeader['email'];
		$strtel = $orderHeader['telephone'];
		
		$strcusttitle = 'jhbensonandco.co.uk Order Confirmation';
		$stradmintitle = 'jhbensonandco.co.uk Order Notification';
		$strmessagehead = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>";
		$strmessagehead = $strmessagehead."<html><head><title></title></head><body><a href='http://jhbenson.co.uk'><img border=0 src='http://jhbenson.co.uk/images/site/stollers_web_logo.jpg' /></a><br/><br/><br/>";

		$strmessageheadcust = $strmessagehead."<p><b>Thank you for your order.</b></p>";
		$strmessageheadcust = $strmessageheadcust."<p>Your order details are given below. Please print a copy of this email for your records.</p>";

		// send notification email to JH Benson & Co.
		$strmessageheadadmin = $strmessagehead."<p><b>Order notification</b></p>";
		$strmessageheadadmin = $strmessageheadadmin."<p>The following order has just been placed on jhbenson.co.uk using Paypal.</p>";

		//confirm basic details
		$strmessage = "<table width='500px'>";
		$strmessage = $strmessage."<tr><td>Order Number</td><td>$orderHeaderID</td></tr>";
		$strmessage = $strmessage."<tr><td>Transaction ID</td><td>$paymentDetailsID</td></tr>";
		$strmessage = $strmessage."<tr><td>Order Date</td><td>".date("d/m/Y H:i",$orderTimestamp)."</td></tr>";
		$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";

		//Confirm delivery details
		$strmessage = $strmessage."<tr><td>Billing Address</td><td>$strbname</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$strbadd1</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$strbadd2</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$strbadd3</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$strbadd4</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$strbadd5</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$strbpostcode</td></tr>";
		$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";
		$strmessage = $strmessage."<tr><td>Delivery Address</td><td>$strname</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$stradd1</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$stradd2</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$stradd3</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$stradd4</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$stradd5</td></tr>";
		$strmessage = $strmessage."<tr><td></td><td>$strpostcode</td></tr>";
		$strmessage = $strmessage."<tr><td colspan='2' height='20px'></td></tr>";
		$strmessage = $strmessage."</table><br/><br/>";

		//order break down
		$strmessage = $strmessage."<table width='500px' style='text-align:left'>";
		$strmessage = $strmessage."<thead><tr><th>Product Name</th><th>Stock Code</th><th>Quantity</th><th>Price</th></tr></thead>";
		$strmessage = $strmessage."<tbody>";
		
		//$getOrderItemsQuery = "SELECT recordID, orderHeaderID, stockID, stockDetailsArray, quantity, price FROM order_items".$strtable." WHERE orderHeaderID = :orderHeaderID ORDER BY recordID";
		$getOrderItemsQuery = "SELECT recordID, orderHeaderID, stockID, stockDetailsArray, quantity, price FROM order_items WHERE orderHeaderID = :orderHeaderID ORDER BY recordID";
		$strType = "multi";
		$arrdbparam = array("orderHeaderID" => $orderHeaderID);
		$orderItems = query($conn, $getOrderItemsQuery, $strType, $arrdbparam);

		foreach ($orderItems AS $orderItem)
		{
			$stockItemDetails = unserialize($orderItem['stockDetailsArray']);
			$intPrice = $orderItem['quantity'] * $orderItem['price'];
			$strmessage = $strmessage."<tr>";
			$strmessage = $strmessage."<td>".$stockItemDetails['name']."</td>";
			$strmessage = $strmessage."<td>".$stockItemDetails['stockCode']."</td>";
			$strmessage = $strmessage."<td>".$orderItem['quantity']."</td>";
			$strmessage = $strmessage."<td>&pound;&nbsp;".number_format($intPrice,2)."</td>";
			$strmessage = $strmessage."</tr>";
			$intitems = $intitems + $orderItem['quantity'];
		}

		$strmessage = $strmessage."</tbody>";

		$strmessage = $strmessage."<tfoot>";
		$strmessage = $strmessage."<tr>";
		$strmessage = $strmessage."<th colspan='2'>Sub Total:</th>";
		$strmessage = $strmessage."<th>$intitems Items</th>";
		
		$dectotalstock = $orderHeader['amountStock'];
		$decvatamt = $orderHeader['amountVat'];
		$dectotalorder = $orderHeader['amountTotal'];
		$decpostageamt = $orderHeader['amountDelivery'];
		$decdiscount = $orderHeader['amountOffer'];
		
		$strmessage = $strmessage."<th>&pound;&nbsp;".number_format($dectotalstock,2)."</th>";
		$strmessage = $strmessage."</tr>";
		$strmessage = $strmessage."</tfoot>";

		$strmessage = $strmessage."</table><br/><br/>";
		
		$strmessage = $strmessage."<table id='totalTable' style='text-align:left'>";
		$strmessage = $strmessage."<thead><tr><th style='width:75%'>Item</th><th>Cost</th></tr></thead>";
		$strmessage = $strmessage."<tbody>";
		$strmessage = $strmessage."<tr><td class='evenrow'>Goods</td><td class='evenrow' align='right'>&pound;&nbsp;".number_format($dectotalstock,2)."</td></tr>";
		if ($strcode != "") $strmessage = $strmessage."<tr><td>Discount ($strcode)</td><td align='right'>&pound;&nbsp;".number_format($decdiscount,2)."</td></tr>";
		if ($boolkfcp) $strmessage = $strmessage."<tr><td>$strpostdesc</td><td align='right'>&pound;&nbsp;".number_format($decpostageamt,2)."</td></tr>";
		$strmessage = $strmessage."</tbody>";
		$strmessage = $strmessage."<tfoot><tr><th style='width:25%'>Total</th><th align='right'>&pound;&nbsp;".number_format($dectotalorder,2)."</th></tr></tfoot>";
		$strmessage = $strmessage."</table>";
		$strmessage = $strmessage."</div><!--end of order-->";
		$strmessage = $strmessage."</div>";

		//what to do if there is an error
		$strmessagefootcust = "<p>If any of these details appear to be incorrect please contact us on 01229 820 679. To help us deal with any problems quickly please have a copy of this email to hand.</p>";
		$strmessagefootcust = $strmessagefootcust."</body></html>";

		$strmessagefootadmin = "<p>If there is a problem with this order the customers contact details are:</p>";
		$strmessagefootadmin = $strmessagefootadmin."<center><h2>$strtel</h2>";
		$strmessagefootadmin = $strmessagefootadmin."<h2>$stremail</h2></center>";
		$strmessagefootadmin = $strmessagefootadmin."</body></html>";

		//customer email $stremail
		if(mail("$stremail", $strcusttitle, $strmessageheadcust.$strmessage.$strmessagefootcust, "MIME-Version: 1.0".PHP_EOL."X-Mailer: PHP/" . phpversion().PHP_EOL."Sensitivity: Personal".PHP_EOL."Return-Path:jhbenson.co.uk".PHP_EOL."From: Stollers UK <no-reply@jhbenson.co.uk>".PHP_EOL."Content-Type: text/html; charset=iso-8859-1"))
		{
			$recordOrderEmailConfirmationQuery = "UPDATE order_header SET sentEmailConfirmation = 1 WHERE recordID = :orderHeaderID";
			$strType = "update";
			$arrdbparam = array("orderHeaderID" => $orderHeaderID);
			$recordOrderEmailConfirmation = query($conn, $recordOrderEmailConfirmationQuery, $strType, $arrdbparam);
		}
		mail("$notifyEmail", $strcusttitle, $strmessageheadcust.$strmessage.$strmessagefootcust, "MIME-Version: 1.0".PHP_EOL."X-Mailer: PHP/" . phpversion().PHP_EOL."Sensitivity: Personal".PHP_EOL."Return-Path:jhbenson.co.uk".PHP_EOL."From: Stollers UK <no-reply@jhbenson.co.uk>".PHP_EOL."Content-Type: text/html; charset=iso-8859-1");
		
		//email "Stollersuk@tiscali.co.uk"
		mail($companyEmail, $stradmintitle, $strmessageheadadmin.$strmessage.$strmessagefootadmin, "MIME-Version: 1.0".PHP_EOL."X-Mailer: PHP/" . phpversion().PHP_EOL."Sensitivity: Personal".PHP_EOL."Return-Path:jhbenson.co.uk".PHP_EOL."From: Stollers UK <no-reply@jhbenson.co.uk>".PHP_EOL."Content-Type: text/html; charset=iso-8859-1");

	}

?>