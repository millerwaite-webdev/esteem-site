<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * catalogue-reviews-and-ratings.php                                  * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "cms-subscribe"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['reviewID'])) $reviewID = $_REQUEST['reviewID']; else $reviewID = "";
	
	switch($strcmd)
	{		
		case "deleteReview":
			
			$deleteReviewQuery = "DELETE FROM customer_newsletter WHERE recordID = :recordID";
			$strType = "delete";
			$arrdbparams = array(
							"recordID" => $reviewID
						);
			$deleteReview = query($conn, $deleteReviewQuery, $strType, $arrdbparams);
		
			if ($deleteReview <= 1)
			{
				$strsuccess = "Contact deleted.";
			}
			elseif ($deleteReview > 1)
			{
				$strwarning = "Contact may not have been deleted.";
			}
			
			
			$strcmd = "";
		
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>Newsletter Subscriptions</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jstoggleReview(reviewID, cmd) {
					document.form.cmd.value=cmd;
					document.form.reviewID.value=reviewID;
					document.form.submit();
				}
				function jsdeleteReview(reviewID) {
					if(confirm("Are you sure you want to delete this page?"))
					{
						document.form.cmd.value="deleteReview";
						document.form.reviewID.value=reviewID;
						document.form.submit();
					}
				}
			</script>
			<?php
		
			print("<form action='cms-subscribe.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='reviewID' id='reviewID' value='$reviewID' />");
				
				switch($strcmd)
				{						
					default:
						
						print("<div class='section'>");
							print("<table id='reviews-table' class='table table-striped table-bordered table-hover table-condensed' >");
								print("<thead>");
									print("<tr>");
										print("<th width='15%'>Date</th>");
										print("<th width='75%'>Email</th>");
										print("<th width='10%' style='text-align:center;'>Delete</th>");
									print("</tr>");
								print("</thead>");
								print("<tbody>");
								
								$getReviewsQuery = "SELECT * FROM customer_newsletter";
								$strType = "multi";
								$subscriptions = query($conn, $getReviewsQuery, $strType);
								
								if (count($subscriptions) != 0) {
									foreach ($subscriptions as $subscription) {
										print("<tr>");
											print("<td>".date("d/m/Y", $subscription['date'])."</td>");
											print("<td><p class='cut'>".$subscription['emailAddress']."</p></td>");
											print("<td style='text-align:center;'><button onclick='jsdeleteReview(".$subscription['recordID']."); return false;' class='center btn btn-danger circle' type='submit' style='display:inline-block;'><i class='fa fa-trash'></i></button></td>");				
									print("</tr>");
									}
								}/* else {
									print("<tr><td colspan='7'>Currently No Reviews</td></tr>");
								}*/
								print("</tbody>");
							print("</table>");
						print("</div>");
						
						break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
				
			$('#reviews-table').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>