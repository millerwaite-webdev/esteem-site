<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '



	// ************* Common page setup ******************** //
	//=====================================================//
	
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "ajax_newsvisible"; //define the current page
	include("inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database
	
	$strdbsql = "UPDATE site_news_events SET enabled = :showValue WHERE recordID = :recordID";
	$arrdbparams = array('recordID' => $_REQUEST['newsID'], 'showValue' => $_REQUEST['enabled']);
	$strType = "update";
	
	$queryResult = query($conn, $strdbsql, $strType, $arrdbparams);
	
	$conn = null; // close the Database connection after all processing
?>
