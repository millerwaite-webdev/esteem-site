<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * index.php                                                          * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "index"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to database

	
	if (isset($_REQUEST['contentType'])) $contentType = $_REQUEST['contentType']; else $contentType = "";
	if (isset($_REQUEST['metaPageLink'])) $metaPageLink = $_REQUEST['metaPageLink']; else $metaPageLink = "index";
	
	
	// *********** Custom Page Processing ***************** //
	//=====================================================//

	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	switch($strcmd)
	{
		case "":
			
		break;
	}
	
	//get the page information
	$strdbsql = "SELECT * FROM site_pages WHERE metaPageLink = :metaPageLink";
	$returnType = "single";
	$dataParam = array("metaPageLink"=>$metaPageLink);
	$meta = query ($conn, $strdbsql, $returnType, $dataParam);
	
	
	

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");

		//Print out debug and error messages
		if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
		if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
		if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
		
		print($meta['pageContent']);
		
		

		
		switch ($contentType){
			case "standardpage":
				
				
				
			break;
			default: //homepage
			
				print("<div class='whitePage'>");
					
					print("<h1>Dashboard</h1>");			
					
					print("<div class='row'>");
					
						$getPagesQueryCount = "SELECT count(recordID) AS totalPages FROM site_pages";
						$pagesCount = query($conn, $getPagesQueryCount, "single", $arrParams);
						
						$getPagesQueryCount = "SELECT count(recordID) AS totalNav FROM site_pages WHERE displayInNav = 1";
						$navCount = query($conn, $getPagesQueryCount, "single", $arrParams);
					
						$nav = $navCount['totalNav'];
						$pages = $pagesCount['totalPages'];
						$value = 100 / $pages;
						$navpages = $nav * $value;
					
						print("<div class='col-sm-3 crop-sm-right'>");
							print("<div class='btn-group' style='right:7px;'>");
								print("<a href='cms-pages.php'>");
									print("<i class='fa fa-bars'></i>");
								print("</a>");
							print("</div>");
							print("<div class='panel-box item1'>");
								print("<div class='panel-top'>");
									print("<i class='fa fa-file-text-o'></i>");
								print("</div>");
								print("<div class='panel-info'>");
									print("<span>Pages</span>");
								print("</div>");
								print("<div class='panel-bottom'>");
									print("<span>".$pagesCount['totalPages']."</span>");
								print("</div>");
							print("</div>");
						print("</div>");
						
						$getContactQueryCount = "SELECT count(recordID) AS totalContacts FROM site_contact_history";
						$contactCount = query($conn, $getContactQueryCount, "single", $arrParams);
						
						$getEnquiriesQueryCount = "SELECT count(recordID) AS totalEnquiries FROM site_contact_history";
						$enquiriesCount = query($conn, $getEnquiriesQueryCount, "single", $arrParams);
										
						$getEnquiriesQueryRead = "SELECT count(recordID) AS readEnquiries FROM site_contact_history WHERE `read` = 1";
						$arrParams = array();
						$enquiriesRead = query($conn, $getEnquiriesQueryRead, "single", $arrParams);
						
						$read = $enquiriesRead['readEnquiries'];
						$total = $enquiriesCount['totalEnquiries'];
						$value = 100 / $total;
						$query = $read * $value;
						$difference = $total - $read;
						
						print("<div class='col-sm-3 crop-sm-both'>");
							print("<div class='btn-group' style='right:7px;'>");
								print("<a href='cms-contact.php'>");
									print("<i class='fa fa-bars'></i>");
								print("</a>");
							print("</div>");
							print("<div class='panel-box item2'>");
								if($difference > 0){
									print("<div class='corner-ribbon shadow top-left blue'>New</div>");
								}
								print("<div class='panel-top'>");
									print("<i class='fa fa-user'></i>");
								print("</div>");
								print("<div class='panel-info'>");
									print("<span>Enquiries</span>");
								print("</div>");
								print("<div class='panel-bottom'>");
									print("<span>".$contactCount['totalContacts']."</span>");
								print("</div>");
							print("</div>");
						print("</div>");
						
						$getNewsletterQueryCount = "SELECT count(recordID) AS totalNewsletters FROM customer_newsletter";
						$newsletterCount = query($conn, $getNewsletterQueryCount, "single", $arrParams);
						
						$timestamp = mktime(0, 0, 0, date("n"), date("j") - date("N") + 1);
						
						$getNewsletterQueryCount = "SELECT count(recordID) AS recentNewsletters FROM customer_newsletter WHERE date > ".$timestamp."";
						$newsletterRecent = query($conn, $getNewsletterQueryCount, "single", $arrParams);
						
						$recentSubs = $newsletterRecent['recentNewsletters'];
						$totalSubs = $newsletterCount['totalNewsletters'];
						$value = 100 / $totalSubs;
						$resultSubs = $recentSubs * $value;
						
						print("<div class='col-sm-3 crop-sm-both'>");
							print("<div class='btn-group' style='right:7px;'>");
								print("<a href='cms-subscribe.php'>");
									print("<i class='fa fa-bars'></i>");
								print("</a>");
							print("</div>");
							print("<div class='panel-box item3'>");
								print("<div class='panel-top'>");
									print("<i class='fa fa-check-square-o'></i>");
								print("</div>");
								print("<div class='panel-info'>");
									print("<span>Newsletter</span>");
								print("</div>");
								print("<div class='panel-bottom'>");
									print("<span>".$newsletterCount['totalNewsletters']."</span>");
								print("</div>");
							print("</div>");
						print("</div>");
						
						$getReviewsQueryCount = "SELECT count(recordID) AS totalReviews FROM stock_reviews";
						$reviewsCount = query($conn, $getReviewsQueryCount, "single", $arrParams);
						
						$getReviewsQuery5 = "SELECT count(recordID) AS numReviews FROM stock_reviews WHERE numStars = 5";
						$arrParams = array();
						$reviews5 = query($conn, $getReviewsQuery5, "single", $arrParams);
						
						$getReviewsQueryHidden = "SELECT count(recordID) AS hiddenReviews FROM stock_reviews WHERE shownOnSite = 0";
						$arrParams = array();
						$reviewsHidden = query($conn, $getReviewsQueryHidden, "single", $arrParams);
						
						$fiveStars = $reviews5['numReviews'];
						$totalReviews = $reviewsCount['totalReviews'];
						$hiddenReviews = $reviewsHidden['hiddenReviews'];
						$value = 100 / $totalReviews;
						$percent = $fiveStars * $value;
						
						print("<div class='col-sm-3 crop-sm-left'>");
							print("<div class='btn-group'>");
								print("<a href='catalogue-reviews-and-ratings.php'>");
									print("<i class='fa fa-bars'></i>");
								print("</a>");
							print("</div>");
							print("<div class='panel-box item4'>");
								if($hiddenReviews > 0){
									print("<div class='corner-ribbon shadow top-left red'>New</div>");
								}
								print("<div class='panel-top'>");
									print("<i class='fa fa-star-half-o'></i>");
								print("</div>");
								print("<div class='panel-info'>");
									print("<span>Reviews</span>");
								print("</div>");
								print("<div class='panel-bottom'>");
									print("<span>".$reviewsCount['totalReviews']."</span>");
								print("</div>");
							print("</div>");
						print("</div>");
					print("</div>");
					
					print("<div class='row'>");
						print("<div class='col-lg-3 crop-sm-right'>");
							print("<div class='section social'>");
								print("<legend>Social</legend>");
								
								$strdbsql = "SELECT site_social_media.socialLink, site_social_media.socialName, site_social_sites.siteIcon, site_social_sites.siteColour, site_social_sites.siteText FROM site_social_media INNER JOIN site_social_sites ON site_social_media.socialName = site_social_sites.siteSite";
								$strType = "multi";
								$socials = query($conn, $strdbsql, $strType);
								
								if(count($socials) > 0){
									
									print("<ul class='list-group'>");
								
									foreach($socials AS $social){
									
										print("<li class='list-group-item'>");
											print("<a href='".$social['socialLink']."' target='_blank'>");
												print($social['socialName']);
												print("<span class='badge' style='background-color:".$social['siteColour'].";color:".$social['siteText'].";'>".$social['siteIcon']."</span>");
											print("</a>");
										print("</li>");
										
									}
									
									print("</ul>");
									
								}
								
								else{
								
									print("<p style='padding:0 15px;'>There is currently no social media on your site.</p>");
									
									print("<div style='padding:0 15px;text-align:right;position:absolute;right:15px;bottom:45px;'>");
										print("<a href='cms-social.php' class='btn btn-success' style='display:inline-block;'>Add</a>");
									print("</div>");
								
								}
								
								print("</ul>");
							print("</div>");
						print("</div>");
						print("<div class='col-lg-9 crop-sm-left'>");
							print("<div class='section'>");
							
								$strdbsql = "SELECT * FROM site_company_details";
								$strType = "single";
								$details = query($conn, $strdbsql, $strType);
								
								print("<div class='row'>");
									print("<div class='col-md-6 crop-sm-right'>");
										
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<legend class='none-both'>".$details['companyName']." <div class='pull-right media-out'><a href='cms-general.php'><i class='fa fa-bars' title='Edit'></i></a></div></legend>");
											print("</div>");
										print("</div>");
										
										print("<div class='row'>");
											print("<div class='form-group col-xs-12'>");
												
												print("<div class='company-box'>");
													print("<p>E: ".$details['companyEmail']."");
													if($details['companyEmail2'] != "")print(", ".$details['companyEmail2']."</p>");
												print("</div>");
												
												print("<div class='company-box'>");
													print("<h2>Address</h2>");
													print("<ul>");
														print("<li><strong>".$details['companyName']."</strong></li>");
														print("<li>".$details['addressNumber']." ".$details['addressStreet'].", ".$details['addressCity']."</li>");
														print("<li>".$details['addressCounty'].", ".$details['addressPostCode']."</li>");
														print("<li>T: ".$details['companyPhone']."</li>");
													print("</ul>");
												print("</div>");
												
											print("</div>");
										print("</div>");
										
									print("</div>");
									
									print("<div class='col-md-6 crop-sm-left'>");
										
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<legend class='none-both'>Statistics</legend>");
											print("</div>");
										print("</div>");
										
										print("<div class='row'>");
											print("<div class='form-group col-xs-12 data-bars'>");
											
												print("<div class='progress green'>");
													print("<div class='progress-bar' role='progressbar' aria-valuenow='70'");
														print("aria-valuemin='0' aria-valuemax='100' style='width:".round($navpages)."%'>");
													print("</div>");
												print("</div>");
												print("<div class='progress-labels green'>");
													print("<span class='progress-rating'>Pages in Navbar</span>");
													print("<span class='progress-percent'>".round($navpages)."%</span>");
												print("</div>");
												
												print("<div class='progress blue'>");
													print("<div class='progress-bar' role='progressbar' aria-valuenow='70'");
														print("aria-valuemin='0' aria-valuemax='100' style='width:".round($query)."%'>");
													print("</div>");
												print("</div>");
												print("<div class='progress-labels blue'>");
													print("<span class='progress-rating'>Read Enquiries</span>");
													print("<span class='progress-percent'>".round($query)."%</span>");
												print("</div>");
												
												print("<div class='progress yellow'>");
													print("<div class='progress-bar' role='progressbar' aria-valuenow='70'");
														print("aria-valuemin='0' aria-valuemax='100' style='width:".round($resultSubs)."%'>");
													print("</div>");
												print("</div>");
												print("<div class='progress-labels yellow'>");
													print("<span class='progress-rating'>Subscriptions this Week</span>");
													print("<span class='progress-percent'>".round($resultSubs)."%</span>");
												print("</div>");
												
												print("<div class='progress red'>");
													print("<div class='progress-bar' role='progressbar' aria-valuenow='70'");
														print("aria-valuemin='0' aria-valuemax='100' style='width:".round($percent)."%'>");
													print("</div>");
												print("</div>");
												print("<div class='progress-labels red'>");
													print("<span class='progress-rating'>Five Star Reviews</span>");
													print("<span class='progress-percent'>".round($percent)."%</span>");
												print("</div>");
												
											print("</div>");
										print("</div>");
										
									print("</div>");
									
								print("</div>");
							
							print("</div>");	
						print("</div>");
						
					print("</div>");
					
					print("<div class='row media-out'>");
						print("<div class='col-xs-12'>");
							print("<div class='section loading' style='min-height:530px;position:relative;margin-bottom:0;'>");	
								print("<fieldset>");
									print("<legend>Page Views</legend>");
									print("<div id='embed-api-auth-container'></div>");
									print("<div id='chart-container'></div>");
									print("<div id='view-selector-container' style='display:none;'></div>");
								print("</fieldset>");
							print("</div>");
						print("</div>");
					print("</div>");
					
?>

					<script>
					(function(w,d,s,g,js,fjs){
					  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
					  js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
					  js.src='https://apis.google.com/js/platform.js';
					  fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
					}(window,document,'script'));
					</script>

					<script>

					gapi.analytics.ready(function() {
						// Authorize the user immediately if the user has already granted access. If no access has been created, render an authorize button inside the element with the ID "embed-api-auth-container". */
						gapi.analytics.auth.authorize({
							container: 'embed-api-auth-container',
							clientid: '327127969131-l31qe696cu43abmklo1n8oq92ul4200v.apps.googleusercontent.com',
							userInfoLabel: ''
						});
						// Create a new ViewSelector instance to be rendered inside of an element with the id "view-selector-container". */
						var viewSelector = new gapi.analytics.ViewSelector({
							container: 'view-selector-container'
						});
						// Render the view selector to the page.
						viewSelector.execute();
						// Create a new DataChart instance with the given query parameters and Google chart options. It will be rendered inside an element with the id "chart-container".
						var dataChart = new gapi.analytics.googleCharts.DataChart({
							query: {
								metrics: 'ga:pageviews',
								dimensions: 'ga:date', 'start-date': '30daysAgo', 'end-date': 'yesterday'
							},
							chart: {
								container: 'chart-container',
								type: 'LINE',
								options: {
									width: '100%',
									height: '500',
									lineWidth: 5,
									pointSize: 0,
									curveType: 'function',
									colors: ['#2C7'],
									chartArea: {
										top: '0',
										left: '15',
										width: '98%',
										height: '97%'
									},
									legend: {position: 'none'},
									vAxis: {
										baselineColor: '#F3F3F3',
										textPosition: 'in',
										gridlines: {
											color: '#F3F3F3',
											count: 5,
										},
										textStyle: {
											color: '#3D4051',
											fontSize: 12,
											bold: true,
											fontName: 'Roboto'
										}
									},
									hAxis: {
										baselineColor: '#F3F3F3',
										textPosition: 'in',
										gridlines: {
											count: 10
										},
										textStyle: {
											color: '#3D4051',
											fontSize: 12,
											bold: true,
											fontName: 'Roboto',
											background: '#FAB011'
										}
									}				
								}
							}		
						});
						// Render the dataChart on the page whenever a new view is selected.
						viewSelector.on('change', function(ids) {
							dataChart.set({query: {ids: 'ga:140430634'}}).execute();
							console.log(ids);
						});
					});
					</script>

<?php
				
				print("</div>");

			break;
		}			

	print("</div>");

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the database connection after all processing
?>