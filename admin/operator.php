<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * index.php                                                          * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//
	
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "operator"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	switch($strcmd)
	{
		case "updateOperator":
			
			$strdbsql = "UPDATE admin_operator SET firstname = :firstname, surname = :surname, phonenumber = :phonenumber, email = :email, memorableInformation = :meminfo WHERE operatorID = :operatorID";
			$arrType = "update";
			$strdbparams = array("operatorID" => $_SESSION['operatorID'], "firstname" => $_REQUEST['frm_firstname'], "surname" => $_REQUEST['frm_surname'], "phonenumber" => $_REQUEST['frm_ddi'], "email" => $_REQUEST['frm_email'], "meminfo" => $_REQUEST['frm_meminfo']);
			$resultupdate = query($conn, $strdbsql, $arrType, $strdbparams);

			if ($resultupdate != null) 
			{
				$strsuccess = "Successfully updated details<br/>";

			} else {
				$strerror = "Failed to update details. If you have made a change please try again and if this error re-occurs please contact $supportemail";
			}
			
		break;
		
		case "updatePassword":
			
			$strdbsql = "UPDATE admin_operator SET password = :password WHERE operatorID = :operatorID";
			$arrType = "update";
			$strdbparams = array("operatorID" => $_SESSION['operatorID'], "password" => md5($_REQUEST["frm_password"]));
			$resultupdate = query($conn, $strdbsql, $arrType, $strdbparams);

			if ($resultupdate != null) 
			{
				$strsuccess = "Successfully updated password<br/>";
				unset($_SESSION['blankpwd']); 

			} else {
				$strerror = "Failed to update password. If you have made a change please try again and if this error re-occurs please contact $supportemail";
			}
			
		break;
	}

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Settings</h1>");
		
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }

			//get operator details from either the master database or the local database
			$strdbsql = "SELECT * FROM admin_operator WHERE operatorID = :operatorID";
			$arrType = "single";
			$strdbparams = array("operatorID" => $_SESSION['operatorID']);
			$staffdetails = query($conn, $strdbsql, $arrType, $strdbparams);
		
			?> <script language='Javascript'>
					function jsupdatePass() {
						if (document.getElementById('frm_password').value == document.getElementById('frm_passrepeat').value ) {
							document.form.cmd.value='updatePassword';
							document.form.submit();
						} else {
							alert ('Passwords do not match');
							return false;
						}
					}
					function jsupdateOperator() {
						if ($('#form').valid()) {
							document.form.cmd.value='updateOperator';
							document.form.submit();
						} else {
							return false;
						}
					}
				</script>
					
			<?php
		
			print ("<form action='operator.php' class='uniForm' method='post' name='form' id='form'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");

				print("<div class='row'>");
					print("<div class='col-md-6' style='padding-right:7.5px;'>");
						print("<div class='section'>");
							print("<fieldset>");
								print("<legend>Change Password</legend>");
				
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_password' class='control-label'>Password <span class='help-block pull-right'><span id='strength' name='strength'></span></label>");
											print("<input onkeyup='passwordStrength(this.value); return false;' type='password' class='form-control' id='frm_password' name='frm_password' placeholder='Password'>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_passrepeat' class='control-label'>Repeat Password <span class='help-block pull-right'><span id='match' name='match'></span></span></label>");
										print("<input onkeyup='passMatch(); return false;' type='password' class='form-control' id='frm_passrepeat' name='frm_passrepeat' placeholder='Repeat Password'>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group' style='text-align:right;'>");
										print("<button onclick='return jsupdatePass();' type='submit' class='btn btn-success' style='display:inline-block;'>Save</button>");
									print("</div>");
								print("</div>");
								
							print("</fieldset>");
						print("</div>");
					print("</div>");
				print("</div>");

				print("<div class='row'>");
					print("<div class='col-md-6' style='padding-right:7.5px;'>");
						print("<div class='section'>");
							print("<fieldset>");
								print("<legend>Acount Details</legend>");
					
								print("<div class='row'>");
									print ("<div class='form-group'>");
										print("<label for='frm_firstname' class='control-label'>Firstname</label>");
										print("<input name='frm_firstname' id='frm_firstname' value='".$staffdetails['firstname']."' size='35' maxlength='50' type='text' class='form-control'/>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_surname' class='control-label'>Surname</label>");
										print("<input name='frm_surname' id='frm_surname' value='".$staffdetails['surname']."' size='35' maxlength='50' type='text' class='form-control'/>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_ddi' class='control-label'>Direct Dial No.</label>");
										print("<input name='frm_ddi' id='frm_ddi' value='".$staffdetails['phonenumber']."' size='35' maxlength='50' type='text' class='form-control'/>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_email' class='control-label'>Email Address</label>");
										print("<input name='frm_email' id='frm_email' value='".$staffdetails['email']."' size='35' maxlength='50' type='text' class='form-control'/>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_meminfo' class='control-label'>Memorable Information</label>");
										print("<input name='frm_meminfo' id='frm_meminfo' value='".$staffdetails['memorableInformation']."' size='35' maxlength='8' type='text' class='form-control'/>");
									print("</div>");
								print("</div>");
					
								print("<div class='row'>");
									print("<div class='form-group' style='text-align:right;'>");
										print("<button onclick='return jsupdateOperator();' type='submit' class='btn btn-success' style='display:inline-block;'>Save</button>");
									print("</div>");
								print("</div>");
				  
							print("</fieldset>");
						print("</div>");
					print("</div>");
				print("</div>");
				
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		function passwordStrength(password)
		{
				var desc = new Array();

				desc[0] = "<span style='color: #ff0000'>Very Weak</span>";
				desc[1] = "<span style='color: #ff0000'>Weak</span>";
				desc[2] = "<span style='color: #ff0000'>Better</span>";
				desc[3] = "<span style='color: #ff0000'>Medium</span>";
				desc[4] = "<span style='color: #228822'>Strong</span>";
				desc[5] = "<span style='color: #228822'>Strongest</span>";

				var score   = 0;

				//if password bigger than 7 give 1 point
				if (password.length > 7) score++;

				//if password has both lower and uppercase characters give 1 point      
				if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) score++;

				//if password has at least one number give 1 point
				if (password.match(/\d+/)) score++;

				//if password has at least one special caracther give 1 point
				if ( password.match(/[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) ) score++;

				//if password bigger than 11 give another 1 point
				if (password.length > 11) score++;
				 document.getElementById("strength").innerHTML = desc[score];
				 
				passMatch();

		 //        document.getElementById("strength").className = "strength" + score;

		} 

		function passmeetsrequired(upper,lower,symbol,numeric,length)
		{
			var ret=true;
			var password=document.getElementById("frm_password").value;
			var confirm=document.getElementById("frm_passrepeat").value;

			if (password!=confirm) ret=false;	
			if (password.length < length) ret=false;
			if ((upper==1) && (!password.match(/[A-Z]/))) ret=false;
			if ((lower==1) && (!password.match(/[a-z]/))) ret=false;
			if ((symbol==1) && (!password.match(/[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))) ret=false;
			if ((numeric==1) && (!password.match(/\d+/))) ret=false;
			return ret;
		}


		function passMatch()
		{
			var status = "<span style='color: #ff0000'>Passwords Don't Match</span>";
			if (document.getElementById("frm_password").value == document.getElementById("frm_passrepeat").value ) status = "<span style='color: #228822'>Passwords Match</span>";
			document.getElementById("match").innerHTML = status;
		}
	
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
					frm_firstname: {
						required: true
					},
					frm_surname: {
						required: true
					},
					frm_meminfo: {
						required: true,
						minlength: 8
					}
				},
				messages: {
					frm_firstname: {
						required: "Please enter your firstname"
					},
					frm_surname: {
						required: "Please enter your surname"
					},
					frm_meminfo: {
						required: "Please enter your memorable information"
					}
				}
			});
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>