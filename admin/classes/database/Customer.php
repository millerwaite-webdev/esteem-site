<?php namespace Database;
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 13/07/2015
 * Time: 12:27
 */

class Customer
{
    public static function addCustomer(CustomerDetail $customerDetail)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        // customer
        $strdbsql = "INSERT INTO customer (groupID, username, password, title, firstname, surname, company, telephone, mobile, email, fax, notes)
                     VALUES (:groupID, :username, :password, :title, :firstname, :surname, :company, :telephone, :mobile, :email, :fax, :notes);";
        $arrdbparams['groupID'] = $customerDetail->groupID;
        $arrdbparams['username'] = $customerDetail->username;
        $arrdbparams['password'] = $customerDetail->password;
        $arrdbparams['title'] = $customerDetail->title;
        $arrdbparams['firstname'] = $customerDetail->firstname;
        $arrdbparams['surname'] = $customerDetail->surname;
        $arrdbparams['company'] = $customerDetail->company;
        $arrdbparams['telephone'] = $customerDetail->telephone;
        $arrdbparams['mobile'] = $customerDetail->mobile;
        $arrdbparams['email'] = $customerDetail->email;
        $arrdbparams['fax'] = $customerDetail->fax;
        $arrdbparams['notes'] = $customerDetail->notes;
        $strType = "insert";
        return query($conn, $strdbsql, $strType, $arrdbparams);
    }
}

class CustomerDetail
{
    public $recordID;
    public $groupID;
    public $username;
    public $password;
    public $title;
    public $firstname;
    public $surname;
    public $company;
    public $telephone;
    public $mobile;
    public $email;
    public $fax;
    public $defaultBillingAdd;
    public $defaultDeliveryAdd;
    public $notes;

    /**
     * @param $groupID
     * @param $username
     * @param $password
     * @param $title
     * @param $firstname
     * @param $surname
     * @param $company
     * @param $telephone
     * @param $mobile
     * @param $email
     * @param $fax
     * @param $notes
     */
    public function __construct(
        $groupID, $username, $password, $title, $firstname, $surname, $company, $telephone, $mobile, $email, $fax, $notes)
    {
        $this->groupID = $groupID;
        $this->username = $username;
        $this->password = $password;
        $this->title = $title;
        $this->firstname = $firstname;
        $this->surname = $surname;
        $this->company = $company;
        $this->telephone = $telephone;
        $this->mobile = $mobile;
        $this->email = $email;
        $this->fax = $fax;
        $this->notes = $notes;
    }
}

