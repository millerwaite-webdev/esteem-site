<?php namespace Database;
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 10/07/2015
 * Time: 16:28
 */

class StockImage
{
    /**
     * @param StockImageDetail $stockImageDetail
     * @return array|string
     */
    public static function createStockImage(StockImageDetail $stockImageDetail)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "INSERT INTO stock_images (description, imageLink, imageTypeID, stockID, imageOrder)
                     VALUES (:description, :imageLink, :imageTypeId, :stockId, :imageOrder)";
        $arrdbparams['description'] = $stockImageDetail->description;
        $arrdbparams['imageLink'] = $stockImageDetail->imageLink;
        $arrdbparams['imageTypeId'] = $stockImageDetail->imageTypeId;
        $arrdbparams['stockId'] = $stockImageDetail->stockId;
        $arrdbparams['imageOrder'] = $stockImageDetail->imageOrder;
        $strType = "insert";
        return query($conn, $strdbsql, $strType, $arrdbparams);
    }
}

class StockImageDetail
{
    public $recordId;
    public $description;
    public $imageLink;
    public $imageTypeId;
    public $stockId;
    public $imageOrder;

    public function __construct($description, $imageLink, $imageTypeID, $stockID, $imageOrder)
    {
        $this->description = $description;
        $this->imageLink = $imageLink;
        $this->imageTypeId = $imageTypeID;
        $this->stockId = $stockID;
        $this->imageOrder = $imageOrder;
    }
}