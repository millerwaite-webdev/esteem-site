<?php namespace Database;
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 09/07/2015
 * Time: 16:03
 */

class Stock
{
    /**
     * @param $price
     * @param $priceControl
     * @param $stockCode
     * @param $stockLevels
     * @param $sku
     * @param groupId
     * @param modelReference
     * @return array|string
     */
    public static function createStock($price, $priceControl, $stockCode, $stockLevels, $sku, $groupId, $modelReference)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "INSERT INTO stock (price, priceControl, stockCode, stockLevels, sku, groupID, modelReference)
                     VALUES (:price, :priceControl, :stockCode, :stockLevels, :sku, :groupID, :modelReference)";
        $arrdbparams['price'] = $price;
        $arrdbparams['priceControl'] = $priceControl;
        $arrdbparams['stockCode'] = $stockCode;
        $arrdbparams['stockLevels'] = $stockLevels;
        $arrdbparams['sku'] = $sku;
        $arrdbparams['groupID'] = $groupId;
        $arrdbparams['modelReference'] = $modelReference;
        $strType = "insert";
        $affectedrows = query($conn, $strdbsql, $strType, $arrdbparams);
        return $affectedrows;
    }

    public static function updateStock()
    {
        return true;
    }

    public static function checkStockExistsByName($name)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "SELECT
                gi.recordID, price, priceControl, stockCode, stockLevels, groupID, dimensionID, sku, modelReference, deliverySize,
                gi.name
            FROM stock st
            INNER JOIN stock_group_information gi
                ON st.recordID = gi.recordID
            WHERE gi.name LIKE :name";
        $arrdbparams['name'] = $name;
        $strType = "multi";
        $products = query($conn, $strdbsql, $strType, $arrdbparams);
        return (count($products) != 0);
    }

    public static function checkStockExistsBySku($sku)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "SELECT stockCode FROM stock WHERE stockCode = :sku";
        $arrdbparams['sku'] = $sku;
        $strType = "multi";
        $products = query($conn, $strdbsql, $strType, $arrdbparams);
        return (count($products) > 0);
    }
}