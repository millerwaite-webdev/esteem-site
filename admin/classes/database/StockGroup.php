<?php namespace Database;
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 10/07/2015
 * Time: 16:28
 */

class StockGroup
{
    public static function createGroupInformation(StockGroupInformationDetail $stockGroupInformation)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "INSERT INTO stock_group_information (name, description, retailPrice, metaLink, metaDoctitle, metaDescription, metaKeywords, dateLastModified, dateCreated, statusID, brandID, brandOrder, optionsList)
                     VALUES (:name, :description, :retailPrice, :metaLink, :metaDoctitle, :metaDescription, :metaKeywords, :dateLastModified, :dateCreated, :statusID, :brandID, :brandOrder, :optionsList)";
        $arrdbparams['name'] = $stockGroupInformation->name;
        $arrdbparams['description'] = $stockGroupInformation->description;
        $arrdbparams['retailPrice'] = $stockGroupInformation->retailPrice;
        $arrdbparams['metaLink'] = $stockGroupInformation->metaLink;
        $arrdbparams['metaDoctitle'] = $stockGroupInformation->metaDocTitle;
        $arrdbparams['metaDescription'] = $stockGroupInformation->metaDescription;
        $arrdbparams['metaKeywords'] = $stockGroupInformation->metaKeywords;
        $arrdbparams['dateLastModified'] = $stockGroupInformation->dateLastModified;
        $arrdbparams['dateCreated'] = $stockGroupInformation->dateCreated;
        $arrdbparams['statusID'] = $stockGroupInformation->statusId;
        $arrdbparams['brandID'] = $stockGroupInformation->brandId;
        $arrdbparams['brandOrder'] = $stockGroupInformation->brandOrder;
        $arrdbparams['optionsList'] = $stockGroupInformation->optionsList;
        $strType = "insert";
        return query($conn, $strdbsql, $strType, $arrdbparams);
    }

    public static function getGroupById($groupId)
    {
        // get data
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "SELECT recordID, `name`, description, retailPrice, metaLink, metaDoctitle, metaDescription, metaKeywords, dateLastModified, dateCreated, statusID, brandID, brandOrder, optionsList FROM stock_group_information WHERE recordID = :groupId";
        $arrdbparams['groupId'] = $groupId;
        $strType = "multi";
        $returnedcategories = query($conn, $strdbsql, $strType, $arrdbparams);
        // build object
        if (count($returnedcategories) == 0)
            return null;
        $createdcategory = $returnedcategories[0];
        $categorydetail = new StockGroupInformationDetail(
            $createdcategory["name"],
            $createdcategory["description"],
            $createdcategory["retailPrice"],
            $createdcategory["metaLink"],
            $createdcategory["metaDoctitle"],
            $createdcategory["metaDescription"],
            $createdcategory["metaKeywords"],
            $createdcategory["dateLastModified"],
            $createdcategory["dateCreated"],
            $createdcategory["statusID"],
            $createdcategory["brandID"],
            $createdcategory["brandOrder"],
            $createdcategory["optionsList"]
        );
        $categorydetail->recordId = $createdcategory["recordID"];
        return $categorydetail;
    }

    public static function getGroupByName($groupName)
    {
        // get data
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "SELECT recordID, `name`, description, retailPrice, metaLink, metaDoctitle, metaDescription, metaKeywords, dateLastModified, dateCreated, statusID, brandID, brandOrder, optionsList FROM stock_group_information WHERE `name` = :name";
        $arrdbparams['name'] = $groupName;
        $strType = "multi";
        $returnedcategories = query($conn, $strdbsql, $strType, $arrdbparams);
        // build object
        if (count($returnedcategories) == 0)
            return null;
        $createdcategory = $returnedcategories[0];
        $categorydetail = new StockGroupInformationDetail(
            $createdcategory["name"],
            $createdcategory["description"],
            $createdcategory["retailPrice"],
            $createdcategory["metaLink"],
            $createdcategory["metaDoctitle"],
            $createdcategory["metaDescription"],
            $createdcategory["metaKeywords"],
            $createdcategory["dateLastModified"],
            $createdcategory["dateCreated"],
            $createdcategory["statusID"],
            $createdcategory["brandID"],
            $createdcategory["brandOrder"],
            $createdcategory["optionsList"]
        );
        $categorydetail->recordId = $createdcategory["recordID"];
        return $categorydetail;
    }

    public static function checkGroupExistsById($groupId)
    {
        $group = StockGroup::getGroupById($groupId);
        return ($group != null);
    }

    public static function checkGroupExistsByName($groupId)
    {
        $group = StockGroup::getGroupByName($groupId);
        return ($group != null);
    }
}

class StockGroupInformationDetail
{
    public $recordId;
    public $name;
    public $description;
    public $retailPrice;
    public $metaLink;
    public $metaDocTitle;
    public $metaDescription;
    public $metaKeywords;
    public $dateLastModified;
    public $dateCreated;
    public $statusId;
    public $brandId;
    public $brandOrder;
    public $optionsList;

    /**
     * @param $name
     * @param $description
     * @param $retailPrice
     * @param $metaLink
     * @param $metaDoctitle
     * @param $metaDescription
     * @param $metaKeywords
     * @param $dateLastModified
     * @param $dateCreated
     * @param $statusID
     * @param $brandID
     * @param $brandOrder
     * @param $optionsList
     */
    public function __construct(
        $name, $description, $retailPrice, $metaLink, $metaDoctitle, $metaDescription,
        $metaKeywords, $dateLastModified, $dateCreated, $statusID, $brandID, $brandOrder, $optionsList)
    {
        $this->name = $name;
        $this->description = $description;
        $this->retailPrice = $retailPrice;
        $this->metaLink = $metaLink;
        $this->metaDocTitle = $metaDoctitle;
        $this->metaDescription = $metaDescription;
        $this->metaKeywords = $metaKeywords;
        $this->dateLastModified = $dateLastModified;
        $this->dateCreated = $dateCreated;
        $this->statusId = $statusID;
        $this->brandId = $brandID;
        $this->brandOrder = $brandOrder;
        $this->optionsList = $optionsList;
    }
}