<?php
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 14/08/2015
 * Time: 16:09
 */

namespace Database;

class CustomerAddress
{
    public static function updateCustomerAddressDefaults($customerId, $defaultBillingAdd, $defaultDeliveryAdd)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "UPDATE customer SET defaultDeliveryAdd = :defaultDeliveryAdd, defaultBillingAdd = :defaultBillingAdd WHERE recordID = :customerId";
        $arrdbparams['customerId'] = $customerId;
        $arrdbparams['defaultBillingAdd'] = $defaultBillingAdd;
        $arrdbparams['defaultDeliveryAdd'] = $defaultDeliveryAdd;
        $strType = "update";
        return query($conn, $strdbsql, $strType, $arrdbparams);
    }

    public static function addCustomerAddress($customerId, CustomerAddressDetail $customerAddressDetail)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "INSERT INTO customer_address (customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, country, postcode, notes)
                     VALUES (:customerID, :addDescription, :title, :firstname, :surname, :add1, :add2, :add3, :town, :county, :country, :postcode, :notes);";
        $arrdbparams['customerID'] = $customerId;
        $arrdbparams['addDescription'] = $customerAddressDetail->addDescription;
        $arrdbparams['title'] = $customerAddressDetail->title;
        $arrdbparams['firstname'] = $customerAddressDetail->firstname;
        $arrdbparams['surname'] = $customerAddressDetail->surname;
        $arrdbparams['add1'] = $customerAddressDetail->add1;
        $arrdbparams['add2'] = $customerAddressDetail->add2;
        $arrdbparams['add3'] = $customerAddressDetail->add3;
        $arrdbparams['town'] = $customerAddressDetail->town;
        $arrdbparams['county'] = $customerAddressDetail->county;
        $arrdbparams['country'] = $customerAddressDetail->country;
        $arrdbparams['postcode'] = $customerAddressDetail->postcode;
        $arrdbparams['notes'] = $customerAddressDetail->notes;
        $strType = "insert";
        return query($conn, $strdbsql, $strType, $arrdbparams);
    }

    public static function getCustomerAddress($add1)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "SELECT recordID, customerID, addDescription, title, firstname, surname, add1, add2, add3, town, county, country, postcode, notes FROM customer_address WHERE add1 = :add1;";
        $arrdbparams['add1'] = $add1;
        $strType = "multi";
        $fieldrows = query($conn, $strdbsql, $strType, $arrdbparams);
        if (count($fieldrows) == 0)
            return null;
        $fieldrow = $fieldrows[0];
        $customeraddressdetail = new CustomerAddressDetail(
            $fieldrow['addDescription'],
            $fieldrow['title'],
            $fieldrow['firstname'],
            $fieldrow['surname'],
            $fieldrow['add1'],
            $fieldrow['add2'],
            $fieldrow['add3'],
            $fieldrow['town'],
            $fieldrow['county'],
            $fieldrow['country'],
            $fieldrow['postcode'],
            $fieldrow['notes']
        );
        $customeraddressdetail->recordId = $fieldrow['recordID'];
        $customeraddressdetail->customerID = $fieldrow['customerID'];
        return $customeraddressdetail;
    }

    public static function checkCustomerAddressExists($add1)
    {
        $customer = CustomerAddress::getCustomerAddress($add1);
        return ($customer != null);
    }
}

class CustomerAddressDetail
{
    public $recordId;
    public $customerID;
    public $addDescription;
    public $title;
    public $firstname;
    public $surname;
    public $add1;
    public $add2;
    public $add3;
    public $town;
    public $county;
    public $country;
    public $postcode;
    public $notes;

    /**
     * @param $addDescription
     * @param $title
     * @param $firstname
     * @param $surname
     * @param $add1
     * @param $add2
     * @param $add3
     * @param $town
     * @param $county
     * @param $country
     * @param $postcode
     * @param $notes
     */
    public function __construct(
        $addDescription, $title, $firstname, $surname,
        $add1, $add2, $add3, $town, $county, $country, $postcode, $notes)
    {
        $this->addDescription = $addDescription;
        $this->title = $title;
        $this->firstname = $firstname;
        $this->surname = $surname;
        $this->add1 = $add1;
        $this->add2 = $add2;
        $this->add3 = $add3;
        $this->town = $town;
        $this->county = $county;
        $this->country = $country;
        $this->postcode = $postcode;
        $this->notes = $notes;
    }
}