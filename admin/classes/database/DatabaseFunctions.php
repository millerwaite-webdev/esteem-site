<?php namespace Database;
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 09/07/2015
 * Time: 17:12
 */

use DatabaseSettings;
use PDO;
use PDOException;

class DatabaseFunctions
{
    static function connectByClass(DatabaseSettings $database)
    {
        return DatabaseFunctions::connect(
            $database->driver,
            $database->user,
            $database->pass,
            $database->host,
            $database->db);
    }

    static function connect($driver = DRIVER, $username = USER, $password = PASS, $host = HOST, $db = DB)
    {
        try {
            // Define new PHP Data Object connection
            $conn = new PDO('' . $driver . ':host=' . $host . ';dbname=' . $db, $username, $password);
            // Sets error reporting level
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            // Sets charset
            $conn->exec("set NAMES utf8");
            $conn->exec("SET CHARSET utf8");
            return $conn;
        } catch (PDOException $e) {
            // If failed, display error
            $strdebug = "ERROR: " . $e->getMessage();
        }
    }

    static function query($conn, $querystring, $querytype, $values = null)
    {
        if ($conn) {
            // Prepares query
            $query = $conn->prepare($querystring);

            // Exexcutes query and binds $values array
            $query->execute($values);

            switch ($querytype) {
                case "single":
                    // Returns associative array of single result
                    $result = $query->fetch(PDO::FETCH_ASSOC);
                    break;

                case "multi":
                    // Returns multidimensional associative array of all results
                    $result = $query->fetchAll(PDO::FETCH_ASSOC);
                    break;

                case "count":
                    // Returns number of selected rows
                    $result = $query->rowCount();
                    break;

                case "columns":
                    //returns column information
                    foreach (range(0, $query->columnCount() - 1) as $column_index) {
                        $columns = $query->getColumnMeta($column_index);
                        $columnsname[] = $columns['name'];
                    }
                    $result = $columnsname;
                    break;

                case "insert":
                    // Insert new row and return is fld_counter (auto increment) of the inserted line
                    $result = $conn->lastInsertID();
                    break;

                case "update":
                    // Return number of rows updated by query
                    $result = $query->rowCount();
                    break;

                case "delete":
                    // Return number of rows deleted by query
                    $result = $query->rowCount();
                    break;

                default:
                    // Invalid query type
                    $result = "Invalid !";
                    break;
            }

            return $result;
        } else {
            return "Failure !";
        }
    }
}