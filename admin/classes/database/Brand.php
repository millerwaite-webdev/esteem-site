<?php namespace Database;
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 09/07/2015
 * Time: 15:58
 */

use SplObjectStorage;

require(__DIR__."/DatabaseFunctions.php");
require("../../includes/inc_sitecommon.php");

class Brand
{
    /**
     * @param $brandName
     * @return array|string // Return the created brand ids
     */
    public static function createBrand($brandName)
    {
        $conn = DatabaseFunctions::connectByClass(getDatabaseDetails());
        $strdbsql = "INSERT INTO stock_brands (brandName, brandDescription, brandSidebar, brandBanner, brandImage, metaTitle, metaPageLink, metaDescription, metaKeywords, showProducts, showFilters, showRanges)
                     VALUES (:brandName, :brandDescription, :brandSidebar, :brandImage, :brandBanner, :metaTitle, :metaPageLink, :metaDescription, :metaKeywords, :showProducts, :showFilters, :showRanges)";
        $arrdbparams['brandName'] = $brandName;
        $arrdbparams['brandDescription'] = '';
        $arrdbparams['brandSidebar'] = '';
        $arrdbparams['brandImage'] = '';
        $arrdbparams['brandBanner'] = '';
        $arrdbparams['metaTitle'] = '';
        $arrdbparams['metaPageLink'] = '';
        $arrdbparams['metaDescription'] = '';
        $arrdbparams['metaKeywords'] = '';
        $arrdbparams['showProducts'] = 1;
        $arrdbparams['showFilters'] = 1;
        $arrdbparams['showRanges'] = 1;
        $strType = "insert";
        return query($conn, $strdbsql, $strType, $arrdbparams);
    }

    public static function updateBrand()
    {
        return true;
    }

    /**
     * @param $brandName -- search by brand name
     * @return SplObjectStorage -- selection of matching brands
     */
    public static function getBrand($brandName)
    {
        $databasedetails = getDatabaseDetails();
        $conn = DatabaseFunctions::connectByClass($databasedetails);
        $strdbsql = "SELECT recordID, brandName, brandImage, brandDescription, brandBanner, brandSidebar, metaTitle, metaPageLink, metaDescription, metaKeywords, showProducts, showFilters, showRanges FROM stock_brands WHERE brandName = :brandName";
        $arrdbparams['brandName'] = $brandName;
        $strType = "multi";
        $brandrows = query($conn, $strdbsql, $strType, $arrdbparams);
        // create return
        $brands = new SplObjectStorage();
        foreach ($brandrows as $brandrow)
        {
            $brands->attach(new BrandDetail(
                $brandrow["recordID"], $brandrow["brandName"], $brandrow["brandImage"], $brandrow["brandDescription"], $brandrow["brandBanner"], $brandrow["brandSidebar"],
                $brandrow["metaTitle"], $brandrow["metaPageLink"], $brandrow["metaDescription"], $brandrow["metaKeywords"], $brandrow["showProducts"], $brandrow["showFilters"],
                $brandrow["showRanges"]));
        }
        return $brands;
    }

    /**
     * @param $brandName
     * @return int
     */
    public static function getBrandId($brandName)
    {
        $brands = Brand::getBrand($brandName);
        if ($brands == null || count($brands) == 0)
            return 0;
        // return id now we have the object
        $brands->rewind();
        return $brands->current()->recordID;
    }

    public static function checkBrandExists($brandName)
    {
        $brands = Brand::getBrand($brandName);
        return (count($brands) > 0);
    }
}

class BrandDetail
{
    public $recordID;
    public $brandName;
    public $brandImage;
    public $brandDescription;
    public $brandBanner;
    public $brandSidebar;
    public $metaTitle;
    public $metaPageLink;
    public $metaDescription;
    public $metaKeywords;
    public $showProducts;
    public $showFilters;
    public $showRanges;

    public function __construct($recordID, $brandName, $brandImage, $brandDescription, $brandBanner, $brandSidebar, $metaTitle, $metaPageLink, $metaDescription, $metaKeywords, $showProducts, $showFilters, $showRanges)
    {
        $this->recordID = $recordID;
        $this->brandName = $brandName;
        $this->brandImage = $brandImage;
        $this->brandDescription = $brandDescription;
        $this->brandBanner = $brandBanner;
        $this->brandSidebar = $brandSidebar;
        $this->metaTitle = $metaTitle;
        $this->metaPageLink = $metaPageLink;
        $this->metaDescription = $metaDescription;
        $this->metaKeywords = $metaKeywords;
        $this->showProducts = $showProducts;
        $this->showFilters = $showFilters;
        $this->showRanges = $showRanges;
    }
}