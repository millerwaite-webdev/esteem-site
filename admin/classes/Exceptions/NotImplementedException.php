<?php
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 09/07/2015
 * Time: 15:13
 */

namespace Classes;


use Exception;
use RuntimeException;

class NotImplementedException extends RuntimeException
{
    public function notImplementedMethod()
    {
        throw new Exception('Not implemented');
    }
}