<?php
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 17/07/2015
 * Time: 11:05
 */

namespace Classes;


class ThreeSixtyPath
{
    public $path;
    public $objectName;
    public $urlKey;
    /**
     * @var Array Images files associated with product
     */
    public $imagefiles;

    public function __construct($path, $objectName, $urlKey)
    {
        $this->path = $path;
        $this->objectName = $objectName;
        $this->urlKey = $urlKey;
        $this->enumerateFiles();
    }

    public function enumerateFiles()
    {
        $level1 = "Lv1";
        $directories = $this->getAllSubDirectories($this->path, "\\");
        foreach ($directories as $directory)
        {
            if (strpos($directory, $level1))
            {
                $this->imagefiles = glob(
                    $directory."*.{jpg,jpeg,gif,png}",GLOB_BRACE);
            }
        }
    }

    /**
     * @param $directory
     * @param $directory_seperator
     * @return array
     */
    function getAllSubDirectories( $directory, $directory_seperator )
    {
        $dirs = array_map( function($item)use($directory_seperator){ return $item . $directory_seperator;}, array_filter( glob( $directory . '*' ), 'is_dir') );

        foreach( $dirs AS $dir )
        {
            $dirs = array_merge( $dirs, $this->getAllSubDirectories( $dir, $directory_seperator ) );
        }

        return $dirs;
    }

    public function __ToString()
    {
        return sprintf("Path: %s, Object Name: %s, URL Key: %s, Image File Count: %d",
            $this->path, $this->objectName, $this->urlKey,
            ($this->imagefiles != null) ? count($this->imagefiles) : 0);
    }
}