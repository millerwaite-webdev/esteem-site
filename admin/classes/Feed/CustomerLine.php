<?php namespace Classes;
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 13/07/2015
 * Time: 12:07
 */

class CustomerLine
{
    public $website;
    public $email;
    public $group_id;
    public $firstname;
    public $lastname;
    public $password_hash;
    public $billing_prefix;
    public $billing_firstname;
    public $billing_middlename;
    public $billing_lastname;
    public $billing_suffix;
    public $billing_street_full;
    public $billing_city;
    public $billing_region;
    public $billing_country;
    public $billing_postcode;
    public $billing_telephone;
    public $billing_company;
    public $billing_fax;
    public $shipping_prefix;
    public $shipping_firstname;
    public $shipping_middlename;
    public $shipping_lastname;
    public $shipping_suffix;
    public $shipping_street_full;
    public $shipping_city;
    public $shipping_region;
    public $shipping_country;
    public $shipping_postcode;
    public $shipping_telephone;
    public $shipping_company;
    public $shipping_fax;
    public $created_in;
    public $is_subscribed;
    //public $prefix;
    //public $middlename;
    //public $suffix;
    //public $taxvat;

    public function __construct(
        $website, $email, $group_id, $firstname, $lastname, $password_hash, $billing_prefix, $billing_firstname, $billing_middlename, $billing_lastname, $billing_suffix, $billing_street_full, $billing_city, $billing_region,
        $billing_country, $billing_postcode, $billing_telephone, $billing_company, $billing_fax, $shipping_prefix, $shipping_firstname, $shipping_middlename, $shipping_lastname, $shipping_suffix, $shipping_street_full, $shipping_city,
        $shipping_region, $shipping_country, $shipping_postcode, $shipping_telephone, $shipping_company, $shipping_fax, $created_in, $is_subscribed)
    {
        $this->website = $website;
        $this->email = $email;
        $this->group_id = $group_id;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->password_hash = $password_hash;
        $this->billing_prefix = $billing_prefix;
        $this->billing_firstname = $billing_firstname;
        $this->billing_middlename = $billing_middlename;
        $this->billing_lastname = $billing_lastname;
        $this->billing_suffix = $billing_suffix;
        $this->billing_street_full = $billing_street_full;
        $this->billing_city = $billing_city;
        $this->billing_region = $billing_region;
        $this->billing_country = $billing_country;
        $this->billing_postcode = $billing_postcode;
        $this->billing_telephone = $billing_telephone;
        $this->billing_company = $billing_company;
        $this->billing_fax = $billing_fax;
        $this->shipping_prefix = $shipping_prefix;
        $this->shipping_firstname = $shipping_firstname;
        $this->shipping_middlename = $shipping_middlename;
        $this->shipping_lastname = $shipping_lastname;
        $this->shipping_suffix = $shipping_suffix;
        $this->shipping_street_full = $shipping_street_full;
        $this->shipping_city = $shipping_city;
        $this->shipping_region = $shipping_region;
        $this->shipping_country = $shipping_country;
        $this->shipping_postcode = $shipping_postcode;
        $this->shipping_telephone = $shipping_telephone;
        $this->shipping_company = $shipping_company;
        $this->shipping_fax = $shipping_fax;
        $this->created_in = $created_in;
        $this->is_subscribed = $is_subscribed;
    }

    public function __toString()
    {
        return $this->firstname . " " . $this->lastname;
    }
}