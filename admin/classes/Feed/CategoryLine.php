<?php
/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 14/07/2015
 * Time: 11:43
 */

namespace Classes;


class CategoryLine
{
    public $entityid;
    public $parentId;
    public $createdWhen;
    public $lastUpdatedWhen;
    /**
     * @var // path to category e.g. 1/2/108/113/14
     */
    public $path;
    /**
     * @var // level in hierarchical structure
     */
    public $level;
    public $value;

    /**
     * @param $entityid
     * @param $parentId
     * @param $createdWhen
     * @param $lastUpdatedWhen
     * @param $path
     * @param $level
     * @param $value
     */
    public function __construct($entityid, $parentId, $createdWhen, $lastUpdatedWhen, $path, $level, $value)
    {
        $this->entityid = $entityid;
        $this->parentId = $parentId;
        $this->createdWhen = $createdWhen;
        $this->lastUpdatedWhen = $lastUpdatedWhen;
        $this->path = $path;
        $this->level = $level;
        $this->value = $value;
    }

    public function __ToString()
    {
        return sprintf("Entity Id: %s, Parent Id: %s, Level: %s, Value: %s", $this->entityid, $this->parentId, $this->level, $this->value);
    }
}