<?php namespace Classes;

use Database\Brand;

/**
 * Created by PhpStorm.
 * User: gareth
 * Date: 09/07/2015
 * Time: 11:26
 */

class ProductLine
{
    #region Base Properties
    public $store;
    public $websites;
    public $attribute_set;
    public $type;
    public $sku;
    public $category_ids;
    public $has_options;
    public $manufacturer;
    public $color;
    public $status;
    public $tax_class_id;
    public $visibility;
    public $size;
    public $home_page_featured;
    public $enable_googlecheckout;
    public $gender;
    public $fit;
    public $sole;
    public $product_type;
    public $web_colour;
    public $price;
    public $weight;
    public $name;
    public $meta_title;
    public $meta_description;
    public $image;
    public $small_image;
    public $thumbnail;
    public $url_key;
    public $url_path;
    public $custom_design;
    public $options_container;
    public $gift_message_available;
    public $image_label;
    public $small_image_label;
    public $thumbnail_label;
    public $page_layout;
    public $occasion;
    public $description;
    public $short_description;
    public $meta_keyword;
    public $custom_layout_update;
    public $qty;
    public $min_qty;
    public $use_config_min_qty;
    public $is_qty_decimal;
    public $backorders;
    public $use_config_backorders;
    public $min_sale_qty;
    public $use_config_min_sale_qty;
    public $max_sale_qty;
    public $use_config_max_sale_qty;
    public $is_in_stock;
    public $low_stock_date;
    public $notify_stock_qty;
    public $use_config_notify_stock_qty;
    public $manage_stock;
    public $use_config_manage_stock;
    public $stock_status_changed_automatically;
    public $product_name;
    public $store_id;
    public $product_type_id;
    public $product_status_changed;
    public $product_changed_websites;
    public $special_price;
    public $special_from_date;
    public $news_from_date;
    public $news_to_date;
    public $shoe_tree_size;
    public $cost;
    public $bottle_size;
    #endregion

    public function __construct(
        $store, $websites, $attribute_set, $type, $sku, $category_ids,
        $has_options, $manufacturer, $color, $status, $tax_class_id,
        $visibility, $size, $home_page_featured, $enable_googlecheckout,
        $gender, $fit, $sole, $product_type, $web_colour, $price, $weight,
        $name, $meta_title, $meta_description, $image, $small_image, $thumbnail,
        $url_key, $url_path, $custom_design, $options_container, $gift_message_available,
        $image_label, $small_image_label, $thumbnail_label, $page_layout, $occasion,
        $description, $short_description, $meta_keyword, $custom_layout_update, $qty,
        $min_qty, $use_config_min_qty, $is_qty_decimal, $backorders, $use_config_backorders,
        $min_sale_qty, $use_config_min_sale_qty, $max_sale_qty, $use_config_max_sale_qty,
        $is_in_stock, $low_stock_date, $notify_stock_qty, $use_config_notify_stock_qty, $manage_stock,
        $use_config_manage_stock, $stock_status_changed_automatically, $product_name, $store_id,
        $product_type_id, $product_status_changed, $product_changed_websites, $special_price,
        $special_from_date, $news_from_date, $news_to_date, $shoe_tree_size, $cost, $bottle_size)
    {
        $this->store = $store;
        $this->websites = $websites;
        $this->attribute_set = $attribute_set;
        //if (!$this->IsNullOrEmptyString($type))
//            $type->type = $type;
        $this->sku = $sku;
        $this->category_ids = $category_ids;
        $this->has_options = $has_options;
        $this->manufacturer = $manufacturer;
        $this->color = $color;
        $this->status = $status;
        $this->tax_class_id = $tax_class_id;
        $this->visibility = $visibility;
        $this->size = $size;
        $this->home_page_featured = $home_page_featured;
        $this->enable_googlecheckout = $enable_googlecheckout;
        $this->gender = $gender;
        $this->fit = $fit;
        $this->sole = $sole;
        $this->product_type = $product_type;
        $this->web_colour = $web_colour;
        $this->price = $price;
        $this->weight = $weight;
        $this->name = $name;
        $this->meta_title = $meta_title;
        $this->meta_description = $meta_description;
        $this->image = $image;
        $this->small_image = $small_image;
        $this->thumbnail = $thumbnail;
        $this->url_key = $url_key;
        $this->url_path = $url_path;
        $this->custom_design = $custom_design;
        $this->options_container = $options_container;
        $this->gift_message_available = $gift_message_available;
        $this->image_label = $image_label;
        $this->small_image_label = $small_image_label;
        $this->thumbnail_label = $thumbnail_label;
        $this->page_layout = $page_layout;
        $this->occasion = $occasion;
        $this->description = $description;
        $this->short_description = $short_description;
        $this->meta_keyword = $meta_keyword;
        $this->custom_layout_update = $custom_layout_update;
        $this->qty = $qty;
        $this->min_qty = $min_qty;
        $this->use_config_min_qty = $use_config_min_qty;
        $this->is_qty_decimal = $is_qty_decimal;
        $this->backorders = $backorders;
        $this->use_config_backorders = $use_config_backorders;
        $this->min_sale_qty = $min_sale_qty;
        $this->use_config_min_sale_qty = $use_config_min_sale_qty;
        $this->max_sale_qty = $max_sale_qty;
        $this->use_config_max_sale_qty = $use_config_max_sale_qty;
        $this->is_in_stock = $is_in_stock;
        $this->low_stock_date = $low_stock_date;
        $this->notify_stock_qty = $notify_stock_qty;
        $this->use_config_notify_stock_qty = $use_config_notify_stock_qty;
        $this->manage_stock = $manage_stock;
        $this->use_config_manage_stock = $use_config_manage_stock;
        $this->stock_status_changed_automatically = $stock_status_changed_automatically;
        $this->product_name = $product_name;
        $this->store_id = $store_id;
        $this->product_type_id = $product_type_id;
        $this->product_status_changed = $product_status_changed;
        $this->product_changed_websites = $product_changed_websites;
        $this->special_price = $special_price;
        $this->special_from_date = $special_from_date;
        $this->news_from_date = $news_from_date;
        $this->news_to_date = $news_to_date;
        $this->shoe_tree_size = $shoe_tree_size;
        $this->cost = $cost;
        $this->bottle_size = $bottle_size;
    }

    /**
     * @return bool Returns whether the product is enabled or nots
     */
    public function getProductStatusId()
    {
        switch ($this->visibility)
        {
            case 'Nowhere' : return 2;
            case 'Catalog' : return 5;
            case 'Catalog, Search' : return 3;
            default : return 2;
        }
    }

    public function getBrandId()
    {
        return Brand::getBrandId($this->manufacturer);
    }

    private function IsNullOrEmptyString($question){
        return (!isset($question) || trim($question)==='');
    }

    public function __toString()
    {
        return $this->name . " description " . $this->short_description;
    }
}