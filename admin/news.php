<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * news.php						                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "news"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['newsCategoryID'])) $newsCategoryID = $_REQUEST['newsCategoryID']; else $newsCategoryID = "";
	if (isset($_REQUEST['newsItemID'])) $newsItemID = $_REQUEST['newsItemID']; else $newsItemID = "";
	$strshowArticle = (isset($_REQUEST['cmn-visible'])) ? 1 : 0;
	$strItemType = 1;
	
	switch($strcmd)
	{
		case "insertNewsCategory":
		case "updateNewsCategory":
			
			$arrdbparams = array("description" => $_POST['frm_description']);
			
			if ($strcmd == "insertNewsCategory")
			{	
				$strdbsql = "INSERT INTO site_news_categories (description, type) 
							VALUES (:description, ".$strItemType.")";
				$strType = "insert";
			}
			elseif ($strcmd == "updateNewsCategory")
			{
				$strdbsql = "UPDATE site_news_categories SET description = :description, type = ".$strItemType." WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $newsCategoryID;
				$strType = "update";
			}
			
			$updateNewsCategory = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertNewsCategory")
			{
				$newsCategoryID = $updateNewsCategory;
			}
			
			if ($strcmd == "insertNewsCategory")
			{
				if ($updateNewsCategory > 0)
				{
					$strsuccess = "News category successfully added";
				}
				elseif ($updateNewsCategory == 0)
				{
					$strerror = "An error occurred while adding the news category";
				}
			}
			elseif ($strcmd == "updateNewsCategory")
			{
				if ($updateNewsCategory <= 1)
				{
					$strsuccess = "News category successfully updated";
				}
				elseif ($updateNewsCategory > 1)
				{
					$strwarning = "An error may have occurred while updating this news category";
				}
			}
			
			$strcmd = "viewNewsCategory";
			
		break;
		
		case "deleteNewsCategory":
			
			$strdbsql = "DELETE FROM site_news_categories WHERE recordID = :newsCategoryID";
			$strType = "delete";
			$arrdbparams = array( "newsCategoryID" => $newsCategoryID );
			$deleteNewsItem = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strcmd = "";
			
		break;
		
		case "insertNewsItem":
		case "updateNewsItem":
			
			if($_POST['frm_category'] == 0)
			{
				$_POST['frm_category'] = null;
			}
			$arrdbparams = array(
								"title" => $_POST['frm_title'],
								"content" => $_POST['frm_content'],
								"type" => $_POST['frm_type'],
								"categoryID" => $_POST['frm_category'],
								"date" => time(),
								"image" => $_POST['frm_image'],
								"author" => $_POST['frm_author'],
								"start" => strtotime(str_replace('/', '-', $_POST['frm_start'])),
								"end" => strtotime(str_replace('/', '-', $_POST['frm_end'])),
								"enabled" => 1
							);
			
			if ($strcmd == "insertNewsItem")
			{	
				$strdbsql = "INSERT INTO site_news_events (title, content, date, type, categoryID, image, author, start, end, enabled) 
							VALUES (:title, :content, :date, :type, :categoryID, :image, :author, :start, :end, :enabled)";
				$strType = "insert";
			}
			elseif ($strcmd == "updateNewsItem")
			{
				$strdbsql = "UPDATE site_news_events SET title = :title, content = :content, date = :date, type = :type, categoryID = :categoryID, image = :image, author = :author, start = :start, end = :end, enabled = :enabled WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $newsItemID;
				$strType = "update";
			}
			
			$updateNewsItem = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertNewsItem")
			{
				$newsItemID = $updateNewsItem;
			}
			
			if ($strcmd == "insertNewsItem")
			{
				if ($updateNewsItem > 0)
				{
					$strsuccess = "News item successfully added";
				}
				elseif ($updateNewsItem == 0)
				{
					$strerror = "An error occurred while adding the news item";
				}
			}
			elseif ($strcmd == "updateNewsItem")
			{
				if ($updateNewsItem <= 1)
				{
					$strsuccess = "News item successfully updated";
				}
				elseif ($updateNewsItem > 1)
				{
					$strwarning = "An error may have occurred while updating this news item";
				}
			}
			
			$strcmd = "viewNewsItem";
			
		break;
		
		case "deleteNewsItem":
			
			$strdbsql = "DELETE FROM site_news_events WHERE recordID = :newsItemID";
			$strType = "delete";
			$arrdbparams = array( "newsItemID" => $newsItemID );
			$deleteNewsItem = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strcmd = "";
			
		break;
		
		case "insertImage":
			$newfilename = $_FILES['frm_newimage']['name'];
			$strfileextn = getExtension($newfilename);
			
			$strimguploadpath = "images/";
			$struploaddir = $strrootpath.$strimguploadpath; // where to put full size image
			$upfile = $struploaddir.$newfilename;
			
			//        print("$upfile - saving image<br>");

			if ($_FILES['frm_newimage']['error'] > 0)
			{
				switch ($_FILES['frm_newimage']['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				//   print("$strfileextn - checking extensions<br>");
				// Does the file have the right extension ?
				//if ($strfileextn != 'jpg' && $strfileextn != 'jpeg' && $strfileextn != 'png' && $strfileextn != 'gif')
				if (strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0)
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_newimage']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
				}
				else
				{
					//      print("Uploading to products image directory<br>");
					if (is_uploaded_file($_FILES['frm_newimage']['tmp_name']))
					{
						if (!move_uploaded_file($_FILES['frm_newimage']['tmp_name'], $upfile))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_newimage']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_newimage']['name'].". Please try again.<br/>";
					}
				}
			}
			
			if ($strerror != "")
			{
				$strcommand = "ERRORMSG";
				//   print("Got an Error - $strcommand - $strerror<br>");
			}
			else
			{
				$strnewspath = $strimguploadpath."news/";
				
				$strbranddir = $strrootpath.$strnewspath;
				
				$newfile = $strbranddir.$newfilename;
				$range=copy($upfile,$newfile);
				chmod ($newfile, 0777);
				
				
				unlink($upfile);
				
				if (isset($_REQUEST['frm_replacecurrent'])) $replaceCurrent = $_REQUEST['frm_replacecurrent']; else $replaceCurrent = 0;
				
				if($replaceCurrent)
				{
					$updateNewsImageQuery = "UPDATE site_news_events SET image = :image WHERE recordID = :recordID";
					$strType = "update";
					$arrdbparams = array (
									"image" => $newfilename,
									"recordID" => $newsItemID
								);
					$updateBrandImage = query($conn, $updateNewsImageQuery, $strType, $arrdbparams);
				}
			}
			
			$strsuccess = "New news image added";
			
			$strcmd = "viewNewsItem";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print("<h1>News and Events</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddNewsCategory() {
					document.form.cmd.value="addNewsCategory";
					document.form.submit();
				}
				function jsviewNewsCategory(newsCategoryID) {
					document.form.cmd.value="viewNewsCategory";
					document.form.newsCategoryID.value=newsCategoryID;
					document.form.submit();
				}
				function jsdeleteNewsCategory(newsCategoryID) {
					if(confirm("Are you sure you want to delete this news category?"))
					{
						document.form.cmd.value="deleteNewsCategory";
						document.form.newsCategoryID.value=newsCategoryID;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertNewsCategory() {
					document.form.cmd.value='insertNewsCategory';
					document.form.submit();
				}
				function jsupdateNewsCategory() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateNewsCategory';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertImage() {
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jsaddNewsItem() {
					document.form.cmd.value="addNewsItem";
					document.form.submit();
				}
				function jsviewNewsItem(newsItemID) {
					document.form.cmd.value="viewNewsItem";
					document.form.newsItemID.value=newsItemID;
					document.form.submit();
				}
				function jsdeleteNewsItem(newsItemID) {
					if(confirm("Are you sure you want to delete this news item?"))
					{
						document.form.cmd.value="deleteNewsItem";
						document.form.newsItemID.value=newsItemID;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertNewsItem() {
					document.form.cmd.value='insertNewsItem';
					document.form.submit();
				}
				function jsupdateNewsItem() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateNewsItem';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				$(document).ready(function(){
					$(".show-toggle").click(function(){
					
						var idstring = $(this).attr('id');
					
						if(this.checked) {
							var show = 1;
						}
						else {
							var show = 0;
						}
						
						var idsplit = idstring.split('-')[2];
						
						$.ajax({
							type: "GET",
							url: "/admin/includes/ajax_newsvisible.php?newsID="+idsplit+"&enabled="+show
						});
						
					});
				});
			</script>
			<?php
		
			print("<form action='news.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print("<input type='hidden' name='cmd' id='cmd'/>");
				print("<input type='hidden' name='newsCategoryID' id='newsCategoryID' value='".$newsCategoryID."' />");
				print("<input type='hidden' name='newsItemID' id='newsItemID' value='".$newsItemID."' />");
				print("<input type='hidden' name='itemType' id='itemType' value='".$strItemType."' />");
				
				switch($strcmd)
				{
					case "viewNewsCategory":
					case "addNewsCategory":
						
						print("<div class='row'>");
							print("<div class='col-md-6'>");
						
								if ($strcmd == "viewNewsCategory")
								{
									$strdbsql = "SELECT * FROM site_news_categories WHERE recordID = :recordID";
									$strType = "single";
									$arrdbparams = array("recordID" => $newsCategoryID);
									$newsCategoryDetails = query($conn, $strdbsql, $strType, $arrdbparams);
									/*print("<pre>");
									print_r($newsItemDetails);
									print("</pre>");*/
									
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
											print("<legend>Change News Category Details</legend>");
								}
								elseif ($strcmd == "addNewsCategory")
								{
									$pageDetails = array(
														"description" => ""
													);
									
									print("<div class='section'>");
										print("<fieldset class='inlineLabels'>");
											print("<legend>Add News Category Details</legend>");
								}
											
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_description' class='control-label'>Description</label>");
													print("<input type='text' class='form-control' id='frm_description' name='frm_description' value='".$newsCategoryDetails['description']."'>");
												print("</div>");
											print("</div>");
										print("</fieldset>");
									print("</div>");
									
									print("<div class='row'>");
										print("<div class='col-sm-6'>");
											print("<button onclick='return jscancel(\"\");' class='btn btn-danger pull-left'>Cancel</button>");
										print("</div>");
										print("<div class='col-sm-6'>");
											if ($strcmd == "addNewsCategory")
											{
												print("<button onclick='return jsinsertNewsCategory();' type='submit' class='btn btn-success pull-right'>Add</button> ");
											}
											elseif ($strcmd == "viewNewsCategory")
											{
												print("<button onclick='return jsupdateNewsCategory();' type='submit' class='btn btn-success pull-right'>Update</button> ");
											}
										print("</div>");
									print("</div>");
									
							print("</div>");
						print("</div>");
					
						break;
						
					case "viewNewsItem":
					case "addNewsItem":
						
						print("<div class='row'>");
							print("<div class='col-md-6'>");
								print("<div class='section'>");
						
									if ($strcmd == "viewNewsItem")
									{
										$strdbsql = "SELECT * FROM site_news_events WHERE recordID = :recordID";
										$strType = "single";
										$arrdbparams = array("recordID" => $newsItemID);
										$newsItemDetails = query($conn, $strdbsql, $strType, $arrdbparams);
										$newsItemCategory = $newsItemDetails['categoryID'];
										$newsImage = $newsItemDetails['image'];
										/*print("<pre>");
										print_r($newsItemDetails);
										print("</pre>");*/
										
										print("<fieldset class='inlineLabels'>");
											print("<legend>Details</legend>");
									}
									elseif ($strcmd == "addNewsItem")
									{
										$pageDetails = array(
															"title" => "",
															"content" => ""
														);
										$newsItemCategory = 0;
										$newsImage = "";
										
										print("<fieldset class='inlineLabels'>");
											print("<legend>Details</legend>");
									}
									
											print("<div class='row'>");
												print("<div class='form-group'>");
													print("<label for='frm_title' class=control-label'>Title</label>");
													print("<input type='text' class='form-control' id='frm_title' name='frm_title' value='".$newsItemDetails['title']."'>");
												print("</div>");
											print("</div>");

											print("<div class='row'>");
												print("<div class='form-group col-lg-6 crop-lg-right'>");
													print("<label for='frm_type' class='control-label'>Type</label>");
													print("<select id='frm_type' name='frm_type' class='form-control'>");	
													
														$strdbsql = "SELECT DISTINCT type FROM site_news_events";
														$strType = "multi";
														$newsTypes = query($conn, $strdbsql, $strType);
														
														foreach($newsTypes AS $newsType) {

															$strSelected = "";
														
															if($newsType['type'] == $newsItemDetails['type'])
															{
																$strSelected = " selected";
															}
														
															print("<option value='".$newsType['type']."'".$strSelected.">".($newsType['type'] == 1 ? "News" : "Event")."</option>");																																					

														}
														
													print("</select>");
												print("</div>");
												print("<div class='form-group col-lg-6 crop-lg-left'>");
													print("<label for='frm_category' class='control-label'>Category</label>");
													print("<select id='frm_category' name='frm_category' class='form-control'>");
														print("<option value='0'>None</option>");
											
														$strdbsql = "SELECT * FROM site_news_categories WHERE type = ".$strItemType;
														$strType = "multi";
														$newsCategories = query($conn, $strdbsql, $strType);
														foreach($newsCategories AS $newsCategory)
														{
															$strSelected = "";
															if($newsItemCategory == $newsCategory['recordID'])
															{
																$strSelected = " selected";
															}
															print("<option value='".$newsCategory['recordID']."'".$strSelected.">".$newsCategory['description']."</option>");
														}
													
													print("</select>");
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group col-lg-6 crop-lg-right'>");
													print("<label for='frm_author' class=control-label'>Author</label>");
													print("<input type='text' class='form-control' id='frm_author' name='frm_author' value='".$newsItemDetails['author']."'>");
												print("</div>");
												print("<div class='form-group col-lg-6 crop-lg-left'>");
													print("<label for='frm_image' class='control-label'>Image <i class='fa fa-info-circle pull-right' data-toggle='tooltip' title='Want to add an image to this list? Go to the ADD NEW IMAGE section and add your own.'></i></label>");
													print("<select id='frm_image' name='frm_image' class='form-control'>");
														print("<option value=''>None</option>");
															
														$dh = opendir("../images/news/");
														while (false !== ($image = readdir($dh))) {
															if ($image != "." && $image != "..") {
																if($image == $newsImage)
																{
																	$strselected = " selected";
																}
																else
																{
																	$strselected = "";
																}
																print("<option value='".$image."'".$strselected.">".$image."</option>");
															}
														}
														
													print("</select>");
												print("</div>");
											print("</div>");
											
											print("<div class='row'>");
												print("<div class='form-group col-lg-6 crop-lg-right'>");
													print("<label for='frm_start' class=control-label'>Date/Start Date<i class='fa fa-info-circle pull-right' data-toggle='tooltip' title='Enter a START DATE only, to set a single day event (Leave END DATE empty). Leave empty to set news article as current date.'></i></label>");
													print("<input type='text' class='form-control' id='frm_start' name='frm_start' placeholder='DD/MM/YYYY' value='".date('d/m/Y', $newsItemDetails['start'])."'>");
												print("</div>");
												print("<div class='form-group col-lg-6 crop-lg-left'>");
													print("<label for='frm_end' class=control-label'>End Date</label>");
													print("<input type='text' class='form-control' id='frm_end' name='frm_end' placeholder='DD/MM/YYYY' value='".date('d/m/Y', $newsItemDetails['end'])."'>");
												print("</div>");
											print("</div>");
											
										print("</fieldset>");
										
								print("</div>");
								
								print("<div class='section'>");
									print("<fieldset class='inlineLabels'>");
										
										print("<legend>Add New Image</legend>");
								
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<label for='frm_newimage' class='control-label'>Image</label>");
												print("<div class='row'>");
													print("<div class='col-xs-9' style='padding-right: 7.5px;'>");
														print("<input type='file' class='form-control' id='frm_newimage' name='frm_newimage' style='padding:6px 12px;'/>");
													print("</div>");
													print("<div class='col-xs-3' style='padding-left: 7.5px;'>");
														print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success circle pull-left'><i class='fa fa-plus'></i></button>");
													print("</div>");
												print("</div>");
											print("</div>");
										print("</div>");
										
									print("</fieldset>");
								print("</div>");
							
							print("</div>");
									
							print("<div class='col-md-6'>");
								print("<div class='section'>");
									print("<fieldset class='inlineLabels'>");
										print("<legend>Content</legend>");
										print("<div class='row'>");
											print("<div class='form-group'>");
												print("<textarea class='form-control tinymce' id='frm_content' name='frm_content' rows='12'>".$newsItemDetails['content']."</textarea>");
											print("</div>");
										print("</div>");
									print("</fieldset>");
								print("</div>");
							print("</div>");
							
						print("</div>");
										
						print("<div class='row'>");
							print("<div class='col-md-6' style='text-align:left;'>");
								print("<button onclick='return jscancel(\"\");' class='btn btn-danger' style='display:inline-block;'>Cancel</button>");
							print("</div>");
							print("<div class='col-md-6' style='text-align:right;'>");
								if ($strcmd == "addNewsItem")
								{
									print("<button onclick='return jsinsertNewsItem();' type='submit' class='btn btn-success' style='display:inline-block;'>Add</button>");
								}
								else if ($strcmd == "viewNewsItem")
								{
									print("<button onclick='return jsupdateNewsItem();' type='submit' class='btn btn-success' style='display:inline-block;'>Update</button>");
								}
							print("</div>");
						print("</div>");
					
					break;
						
					default:
						
						$strdbsql = "SELECT * FROM site_news_categories";
						$strType = "multi";
						$newsCategories = query($conn, $strdbsql, $strType);
						
						print("<div class='section'>");
							print("<table id='newscategory-table' class='table table-striped table-bordered table-hover table-condensed' >");
								print("<thead><tr>");
									print("<th width='70%'>Category</th>");
									print("<th width='10%' style='text-align:center;'>Edit</th>");
									print("<th width='10%' style='text-align:center;'>Delete</th>");
								print("</tr></thead><tbody>");
								foreach($newsCategories AS $newsCategory)
								{
									print("<tr>");
										print("<td>".$newsCategory['description']."</td>");
										print("<td style='text-align:center;'><button onclick='return jsviewNewsCategory(\"".$newsCategory['recordID']."\");' style='display:inline-block;' type='submit' class='btn btn-primary circle'><i class='fa fa-pencil' aria-hidden='true'></i></button></td>");
										print("<td style='text-align:center;'><button onclick='return jsdeleteNewsCategory(\"".$newsCategory['recordID']."\");' style='display:inline-block;' type='submit' class='btn btn-danger circle'><i class='fa fa-trash' aria-hidden='true'></i></button></td>");
									print("</tr>");
								}
								print("</tbody>");
							print("</table>");
						print("</div>");
						
						print("<div class='row' style='margin-bottom: 15px;'>");
							print("<div class='col-xs-12'>");
								print("<button onclick='return jsaddNewsCategory();' type='submit' class='btn btn-success pull-right'>Add</button>");
							print("</div>");
						print("</div>");
						
						$strdbsql = "SELECT * FROM site_news_events";
						$strType = "multi";
						$newsItems = query($conn, $strdbsql, $strType);
						
						print("<div class='section'>");
							print("<table id='news-table' class='table table-striped table-bordered table-hover table-condensed' >");
								print("<thead><tr>");
									print("<th width='55%'>Article Title</th>");
									print("<th width='15%'>Date</th>");
									print("<th width='10%' style='text-align:center;'>Show/Hide</th>");
									print("<th width='10%' style='text-align:center;'>Edit</th>");
									print("<th width='10%' style='text-align:center;'>Delete</th>");
								print("</tr></thead><tbody>");
								foreach($newsItems AS $newsItem)
								{
									print("<tr>");
										print("<td>".$newsItem['title']."</td>");
										print("<td>".date("j/m/y \a\\t H:i", $newsItem['date'])."</td>");
										print("<td style='text-align:center;'>");
										
											print("<div class='switch' style='display:inline-block;'>");
												if($newsItem['enabled'] == 0)
												{
													print("<input id='show-toggle-".$newsItem['recordID']."' class='show-toggle cmn-toggle-round' type='checkbox'>");
												}
												else
												{
													print("<input id='show-toggle-".$newsItem['recordID']."' class='show-toggle cmn-toggle-round' type='checkbox' checked>");
												}
												print("<label for='show-toggle-".$newsItem['recordID']."'></label>");
											print("</div>");
										
										print("</td>");
										print("<td style='text-align:center;'><button onclick='return jsviewNewsItem(\"".$newsItem['recordID']."\");' style='display:inline-block;' type='submit' class='btn btn-primary circle'><i class='fa fa-pencil' aria-hidden='true'></i></button></td>");
										print("<td style='text-align:center;'><button onclick='return jsdeleteNewsItem(\"".$newsItem['recordID']."\");' style='display:inline-block;' type='submit' class='btn btn-danger circle'><i class='fa fa-trash' aria-hidden='true'></i></button></td>");
									print("</tr>");
								}
								print("</tbody>");
							print("</table>");
						print("</div>");
						
						print("<div class='row' style='margin-bottom: 15px;'>");
							print("<div class='col-xs-12'>");
								print("<button onclick='return jsaddNewsItem();' type='submit' class='btn btn-success pull-right'>Add</button>");
							print("</div>");
						print("</div>");						
						
						break;
				}
				
			print("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#newscategory-table').DataTable();
			$('#news-table').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>