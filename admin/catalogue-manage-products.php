<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * catalogue-manage-products.php                                      * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

session_start(); //stores session variables such as access levels and logon details
	$strpage = "catalogue-manage-products"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['stockGroupID']))
	{
		$stockGroupID = $_REQUEST['stockGroupID'];
	}
	elseif(isset($_REQUEST['searchStockGroupID']))
	{
		$stockGroupID = $_REQUEST['searchStockGroupID'];
	}
	else
	{
		$stockGroupID = "";
	}
	if (isset($_REQUEST['stockOptionID'])) $stockOptionID = $_REQUEST['stockOptionID']; else $stockOptionID = "";
	if (isset($_REQUEST['overlayType'])) $overlayType = $_REQUEST['overlayType']; else $overlayType = "";
	
	switch($strcmd)
	{
		case "insertStockGroup":
		case "updateStockGroup":
			$strdbsql = "SELECT * FROM stock_attribute_type";
			$arrType = "multi";
			$attributeTypes = query($conn, $strdbsql, $arrType);
			
			$attributesList = "";
			$key = "frm_config";
			
			foreach($attributeTypes AS $attributeType)
			{
				$optionKey = $key.$attributeType['description'];
				if (isset($_REQUEST[$optionKey])) { $attributesList .= $attributeType['recordID'].","; }
			}
			$attributesList = rtrim($attributesList, ",");
			
			if ($strcmd == "insertStockGroup")
			{
				$strdbsql = "INSERT INTO stock_group_information (name, description, retailPrice, metaLink, metaDoctitle, metaDescription, metaKeywords, statusID, brandID, brandOrder, optionsList, dateCreated) VALUES (:name, :description, :retailPrice, :metaLink, :metaDoctitle, :metaDescription, :metaKeywords, :statusID, :brandID, :brandOrder, :optionsList, ".time().")";
				$strType = "insert";
			}
			elseif ($strcmd == "updateStockGroup")
			{
				$strdbsql = "UPDATE stock_group_information SET name = :name, description = :description, retailPrice = :retailPrice, metaLink = :metaLink, metaDoctitle = :metaDoctitle, metaDescription = :metaDescription, metaKeywords = :metaKeywords, statusID = :statusID, brandID = :brandID, brandOrder = :brandOrder, optionsList = :optionsList, dateLastModified = ".time()." WHERE recordID = :recordID";
				$strType = "update";
			}
			
			$arrdbparams = array(
								"name" => $_POST['frm_productname'],
								"description" => $_POST['frm_productdescription'],
								"retailPrice" => $_POST['frm_retailprice'],
								"metaLink" => $_POST['frm_metalink'],
								"metaDoctitle" => $_POST['frm_metadoctitle'],
								"metaDescription" => $_POST['frm_metadescription'],
								"metaKeywords" => $_POST['frm_metakeywords'],
								"statusID" => $_POST['frm_status'],
								"optionsList" => $attributesList
							);
			if($_POST['frm_brand'] == "null")
			{
				$arrdbparams["brandID"] = null;
				$arrdbparams["brandOrder"] = null;
			}
			else
			{
				$arrdbparams["brandID"] = $_POST['frm_brand'];
				$getMaxBrandOrder = "SELECT COUNT(recordID) AS max_brand_order FROM stock_group_information WHERE brandID = :brandID";
				$maxBrandOrderType = "single";
				$maxBrandOrderParam = array("brandID" => $_POST['frm_brand']);
				$maxBrandOrder = query($conn, $getMaxBrandOrder, $maxBrandOrderType, $maxBrandOrderParam);
				$brandOrder = intval($maxBrandOrder['max_brand_order'] + 1);
				$arrdbparams["brandOrder"] = $brandOrder;
			}
			
			if ($strcmd == "updateStockGroup")
			{
				$arrdbparams['recordID'] = $stockGroupID;
				
				/*$strnumoptionsql = "SELECT COUNT(*) AS num_options FROM stock WHERE groupID = :groupID";
				$strNumOptionType = "single";
				$arroptionsdbparam = array ( "groupID" => $stockGroupID );
				$numOptions = query($conn, $strnumoptionsql, $strNumOptionType, $arroptionsdbparam);
				
				if ($numOptions['num_options'] > 0)
				{
					if ($strcolours == 0 || $strfabrics == 0 || $strbases == 0 || $strfinishes == 0)
					{
						$strupdatesql = "UPDATE stock SET ";
						$strupdates = "";
						if ($strcolours == 0)
						{
							if ($strupdates != "")
							{
								$strupdates .= ", ";
							}
							$strupdates .= "colourID = NULL";
						}
						if ($strfabrics == 0)
						{
							if ($strupdates != "")
							{
								$strupdates .= ", ";
							}
							$strupdates .= "fabricID = NULL";
						}
						if ($strbases == 0)
						{
							if ($strupdates != "")
							{
								$strupdates .= ", ";
							}
							$strupdates .= "baseID = NULL";
						}
						if ($strfinishes == 0)
						{
							if ($strupdates != "")
							{
								$strupdates .= ", ";
							}
							$strupdates .= "finishID = NULL";
						}
						$strupdatesql .= $strupdates." WHERE groupID = :groupID";
						//echo $strupdatesql;
						$strUpdateOptionsType = "update";
						$updateStockOptions = query($conn, $strupdatesql, $strUpdateOptionsType, $arroptionsdbparam);
					}
				}*/
			}
			
			if ($strcmd == "updateStockGroup")
			{
				$arrdbparams['recordID'] = $stockGroupID;
			}
			//echo $strdbsql;
			//echo "<pre>".print_r($arrdbparams)."</pre>";
			$updateStockGroup = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertStockGroup")
			{
				if ($updateStockGroup > 0)
				{
					$strsuccess = "Stock group successfully added";
				}
				elseif ($updateStockGroup == 0)
				{
					$strerror = "An error occurred while adding the stock group";
				}
				$stockGroupID = $updateStockGroup;
			}
			elseif ($strcmd == "updateStockGroup")
			{
				if ($updateStockGroup <= 1)
				{
					$strsuccess = "Stock group successfully updated";
				}
				elseif ($updateStockGroup > 1)
				{
					$strwarning = "An error may have occurred while updating this stock group";
				}
			}
			
			$strcmd = "viewStockGroup";
			
		break;
		
		case "deleteStockOption":
			
			$strdbsql = "DELETE FROM stock WHERE recordID = :recordID";
			$strType = "delete";
			$arrdbparams = array(
								"recordID" => $stockOptionID
							);
							
			$deleteStockOption = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($deleteStockOption == 1)
			{
				$strsuccess = "Stock option successfully deleted";
			}
			elseif ($deleteStockOption == 0)
			{
				$strerror = "An error occurred while deleting the stock option";
			}
			elseif ($deleteStockOption > 0)
			{
				$strwarning = "An error may have occurred while deleting this stock option";
			}
			
			$strcmd = "viewStockGroup";
			
		break;
		
		case "insertStockOption":
		case "updateStockOption":
		
			if ($strcmd == "insertStockOption")
			{
				$strdbsqlcolumns = "INSERT INTO stock (stockCode, stockLevels, priceControl, price, dimensionID";
				$strdbsqlvalues = "VALUES (:stockCode, :stockLevels, :priceControl, :price, :dimensionID";
				$strType = "insert";
			}
			else
			{
				$strdbsql = "UPDATE stock SET stockCode = :stockCode, stockLevels = :stockLevels, priceControl = :priceControl, price = :price, dimensionID = :dimensionID";
				$strType = "update";
			}
			
			$arrdbparams = array(
								"stockCode" => $_POST['frm_stockcode'],
								"stockLevels" => $_POST['frm_stocklevels'],
								"priceControl" => $_POST['frm_pricecontrol'],
								"price" => $_POST['frm_price']
							);
			if($_POST['frm_dimension'] == "null")
			{
				$arrdbparams["dimensionID"] = null;
			}
			else
			{
				$arrdbparams["dimensionID"] = $_POST['frm_dimension'];
			}
			
			/*if (isset($_POST['frm_fabric']))
			{
				if ($strcmd == "insertStockOption")
				{
					$strdbsqlcolumns .= ", fabricID";
					$strdbsqlvalues .= ", :fabricID";
				}
				else
				{
					$strdbsql .= ", fabricID = :fabricID";
				}
				$arrdbparams['fabricID'] = $_POST['frm_fabric'];
			}
			
			if (isset($_POST['frm_dimension']))
			{
				if ($strcmd == "insertStockOption")
				{
					$strdbsqlcolumns .= ", dimensionID";
					$strdbsqlvalues .= ", :dimensionID";
				}
				else
				{
					$strdbsql .= ", dimensionID = :dimensionID";
				}
				$arrdbparams['dimensionID'] = $_POST['frm_dimension'];
			}
			
			if (isset($_POST['frm_base']))
			{
				if ($strcmd == "insertStockOption")
				{
					$strdbsqlcolumns .= ", baseID";
					$strdbsqlvalues .= ", :baseID";
				}
				else
				{
					$strdbsql .= ", baseID = :baseID";
				}
				$arrdbparams['baseID'] = $_POST['frm_base'];
			}
			
			if (isset($_POST['frm_finish']))
			{
				if ($strcmd == "insertStockOption")
				{
					$strdbsqlcolumns .= ", finishID";
					$strdbsqlvalues .= ", :finishID";
				}
				else
				{
					$strdbsql .= ", finishID = :finishID";
				}
				$arrdbparams['finishID'] = $_POST['frm_finish'];
			}*/
			
			if ($strcmd == "insertStockOption")
			{
				$strdbsql = $strdbsqlcolumns.", groupID) ".$strdbsqlvalues.", :groupID)";
				$arrdbparams['groupID'] = $stockGroupID;
			}
			else
			{
				$arrdbparams['recordID'] = $stockOptionID;
				$strdbsql .= " WHERE recordID = :recordID";
			}
			$updateStockOption = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertStockOption")
			{
				if ($updateStockOption > 0)
				{
					$strsuccess = "Stock option successfully added";
					
					$dirProduct = mkdir("../images/products/".$updateStockOption, 0777, true);
					$dirList = mkdir("../images/products/".$updateStockOption."/list", 0777, true);
					$dirMain = mkdir("../images/products/".$updateStockOption."/main", 0777, true);
					$dirZoom = mkdir("../images/products/".$updateStockOption."/zoom", 0777, true);
					
					if(!$dirProduct || !$dirList || !$dirMain || !$dirZoom)
					{
						$strwarning = "Not all image directories could be created for this new stock option";
					}
				}
				elseif ($updateStockOption == 0)
				{
					$strerror = "An error occurred while adding the stock option";
				}
				$stockOptionID = $updateStockOption;
			}
			elseif ($strcmd == "updateStockOption")
			{
				if ($updateStockOption <= 1)
				{
					$strsuccess = "Stock option successfully updated";
				}
				elseif ($updateStockOption > 1)
				{
					$strwarning = "An error may have occurred while updating this stock option";
				}
			}
			
			
			$strdbsql = "SELECT * FROM stock_attribute_type";
			$strType = "multi";
			$attributeTypes = query($conn, $strdbsql, $strType);
			
			$key = "frm_";
			foreach($attributeTypes AS $attributeType)
			{
				$attributeKey = $key.$attributeType['description'];
				if (isset($_POST[$attributeKey]))
				{
					$arrdbparams = array(
										"stockAttributeID" => $_POST[$attributeKey],
										"stockID" => $stockOptionID
									);
					if($strcmd == "updateStockOption")
					{
						$strdbsql = "SELECT stock_attribute_relations.stockAttributeID FROM stock_attribute_relations INNER JOIN stock_attributes ON stock_attribute_relations.stockAttributeID = stock_attributes.recordID WHERE stock_attributes.attributeTypeID = :stockAttributeTypeID AND stock_attribute_relations.stockID = :stockID";
						$strType = "single";
						$arrdbattparams = array(
											"stockAttributeTypeID" => $attributeType['recordID'],
											"stockID" => $stockOptionID
										);
						$optionAssigned = query($conn, $strdbsql, $strType, $arrdbattparams);
					}
					
					if($strcmd == "insertStockOption" || empty($optionAssigned['stockAttributeID']))
					{
						$strdbsql = "INSERT INTO stock_attribute_relations (stockAttributeID, stockID) VALUES (:stockAttributeID, :stockID)";
						$strType = "insert";
					}
					elseif($strcmd == "updateStockOption" && !empty($optionAssigned['stockAttributeID']))
					{
						$strdbsql = "UPDATE stock_attribute_relations SET stockAttributeID = :stockAttributeID WHERE stockID = :stockID AND stockAttributeID = :oldStockAttributeID";
						$strType = "update";
						$arrdbparams['oldStockAttributeID'] = $optionAssigned['stockAttributeID'];
					}
					$updateStockAttribute = query($conn, $strdbsql, $strType, $arrdbparams);
				}
			}
			
			$strcmd = "editStockOption";
			
			break;
		
		case "insertImage":
			
			$newfilename = $_FILES['frm_image']['name'];
			$strfileextn = getExtension($newfilename);
			
			$strimguploadpath = "images/";
			$struploaddir = $strrootpath.$strimguploadpath."products/"; // where to put full size image
			$upfile = $struploaddir.$newfilename;
			
			//        print ("$upfile - saving image<br>");

			if ($_FILES['frm_image']['error'] > 0)
			{
				switch ($_FILES['frm_image']['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				//   print ("$strfileextn - checking extensions<br>");
				// Does the file have the right extension ?
				//if ($strfileextn != 'jpg' && $strfileextn != 'jpeg' && $strfileextn != 'png' && $strfileextn != 'gif')
				if (strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0)
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_image']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
				}
				else
				{
					//      print ("Uploading to products image directory<br>");
					if (is_uploaded_file($_FILES['frm_image']['tmp_name']))
					{
						if (!move_uploaded_file($_FILES['frm_image']['tmp_name'], $upfile))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_image']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_image']['name'].". Please try again.<br/>";
					}
				}
			}
			
			if ($strerror != "")
			{
				$strcommand = "ERRORMSG";
				//   print ("Got an Error - $strcommand - $strerror<br>");
			}
			else
			{
			
				if (isset($_REQUEST['frm_applyall'])) { $strapplyall = 1; } else { $strapplyall = 0;}
				
				if($strapplyall)
				{
					$getStockItemsQuery = "SELECT * FROM stock WHERE groupID = :groupID";
					$strType = "multi";
					$arrdbparam = array( "groupID" => $stockGroupID );
					$stockItems = query($conn, $getStockItemsQuery, $strType, $arrdbparam);
				}
				else
				{
					$stockItems[0]['recordID'] = $stockOptionID;
				}
				
				$arrImageInfo = array();
				
				if ($_REQUEST['frm_imagetype'] == 1)
				{
					foreach($stockItems AS $stockItem)
					{
						$strzoomimgpath = $strimguploadpath."products/".$stockItem['recordID']."/zoom/";
						$strlistimgpath = $strimguploadpath."products/".$stockItem['recordID']."/list/";
						$strmainimgpath = $strimguploadpath."products/".$stockItem['recordID']."/main/";
						
						$strzoomdir = $strrootpath.$strzoomimgpath;     //max width size 800
						$strlistdir = $strrootpath.$strlistimgpath;     //max width size 146
						$strmaindir = $strrootpath.$strmainimgpath;   //max width size 470
						
						$getMaxOrderQuery = "SELECT COUNT(*) AS max_order FROM stock_images WHERE imageTypeID = :imageTypeID AND stockID = :stockID";
						$strType = "single";
						$arrdbparam = array(
											"imageTypeID" => $_REQUEST['frm_imagetype'],
											"stockID" => $stockItem['recordID']
										);
						$maxOrder = query($conn, $getMaxOrderQuery, $strType, $arrdbparam);
						$order = $maxOrder['max_order'] + 1;
						
						$newfilename = "img_".$stockItem['recordID']."_".$_REQUEST['frm_imagetype']."_".intval($order).".".$strfileextn;
						$arrImageInfo[$stockItem['recordID']] = array( "name" => $newfilename, "order" => $order );
						$arrFileNames[$stockItem['recordID']] = $newfilename;
						
						//zoom
						$newfile = $strzoomdir.$newfilename;
						$zoom=image_scaletowidth($upfile,$newfile,800);
						chmod ($newfile, 0777);

						//thumb
						$newfile = $strlistdir.$newfilename;
						$list=image_scaletowidth($upfile,$newfile,146);
						chmod ($newfile, 0777);
						
						//main
						$newfile = $strmaindir.$newfilename;
						$main=image_scaletowidth($upfile,$newfile,470);
						chmod ($newfile, 0777);
					}
				}
				elseif ($_REQUEST['frm_imagetype'] > 1)
				{
					$stroverlaypath = $strimguploadpath."overlays/";
					
					$stroverlaydir = $strrootpath.$stroverlaypath;
					
					//$newfilename = "img_".$stockOptionID."_".$_REQUEST['frm_imagetype'].".".$strfileextn;
					$arrImageInfo[0] = $newfilename;
					
					//overlay
					$newfile = $stroverlaydir.$newfilename;
					$overlay=copy($upfile,$newfile);
					chmod ($newfile, 0777);
				}
				unlink($upfile);
				
				foreach($stockItems AS $stockItem)
				{
					if ($_REQUEST['frm_imagetype'] == 1)
					{
						
						$insertImageQuery = "INSERT INTO stock_images (description, imageLink, imageTypeID, stockID, imageOrder) VALUES (:description, :imageLink, :imageTypeID, :stockID, :imageOrder)";
						$strType = "insert";
						$arrdbparams = array (
											"description" => $_POST['frm_imagedescrip'],
											"imageLink" => $arrImageInfo[$stockItem['recordID']]['name'],
											"imageTypeID" => $_POST['frm_imagetype'],
											"stockID" => $stockItem['recordID'],
											"imageOrder" => $arrImageInfo[$stockItem['recordID']]['order']
										);
						$insertImage = query($conn, $insertImageQuery, $strType, $arrdbparams);
					}
					elseif ($_REQUEST['frm_imagetype'] > 1)
					{
						$getIntOverlayImagesQuery = "SELECT COUNT(*) AS num_overlays FROM stock_images WHERE stockID = :stockID AND imageTypeID = :imageTypeID";
						$strType = "single";
						$arrdbparams = array (
											"stockID" => $stockItem['recordID'],
											"imageTypeID" => $_POST['frm_imagetype']
										);
						$intOverlayImages = query($conn, $getIntOverlayImagesQuery, $strType, $arrdbparams);
						
						if($intOverlayImages['num_overlays'] == 0)
						{
							$insertImageQuery = "INSERT INTO stock_images (description, imageLink, imageTypeID, stockID) VALUES (:description, :imageLink, :imageTypeID, :stockID)";
							$strType = "insert";
							$arrdbparams = array (
												"description" => $_POST['frm_imagedescrip'],
												"imageLink" => $arrImageInfo[0],
												"imageTypeID" => $_POST['frm_imagetype'],
												"stockID" => $stockItem['recordID']
											);
							$insertImage = query($conn, $insertImageQuery, $strType, $arrdbparams);
						}
						else
						{
							$insertImageQuery = "UPDATE stock_images SET description = :description, imageLink = :imageLink WHERE stockID = :stockID AND imageTypeID = :imageTypeID";
							$strType = "update";
							$arrdbparams = array (
												"description" => $_POST['frm_imagedescrip'],
												"imageLink" => $arrImageInfo[0],
												"stockID" => $stockItem['recordID'],
												"imageTypeID" => $_POST['frm_imagetype']
											);
							$insertImage = query($conn, $insertImageQuery, $strType, $arrdbparams);
						}
					}
				}
			}
			
			$strcmd = "editStockOption";
			
		break;
		
		case "updateOverlay":
			
			$key = "frm_overlay_".$overlayType;
			$numOverlaysQuery = "SELECT COUNT(*) AS num_overlays FROM stock_images WHERE imageTypeID = :imageTypeID AND stockID = :stockID";
			$strType = "single";
			$arrdbparams = array (
								"imageTypeID" => $overlayType,
								"stockID" => $stockOptionID
							);
			$numOverlays = query($conn, $numOverlaysQuery, $strType, $arrdbparams);
			
			if($numOverlays['num_overlays'] == 1)
			{
				if ($_POST[$key] != "0")
				{
					$updateOverlayQuery = "UPDATE stock_images SET imageLink = :imageLink WHERE imageTypeID = :imageTypeID AND stockID = :stockID";
					$arrdbparams = array (
									"imageLink" => $_POST[$key],
									"imageTypeID" => $overlayType,
									"stockID" => $stockOptionID
								);
				}
				else
				{
					$updateOverlayQuery = "UPDATE stock_images SET imageLink = NULL WHERE imageTypeID = :imageTypeID AND stockID = :stockID";
					$arrdbparams = array (
									"imageTypeID" => $overlayType,
									"stockID" => $stockOptionID
								);
				}
				$strType = "update";
				$updateOverlay = query($conn, $updateOverlayQuery, $strType, $arrdbparams);
			}
			elseif($numOverlays['num_overlays'] == 0)
			{
				if ($_POST[$key] != "0")
				{
					$insertOverlayQuery = "INSERT INTO stock_images (imageLink, imageTypeID, stockID) VALUES (:imageLink, :imageTypeID, :stockID)";
					$strType = "insert";
					$arrdbparams = array (
										"imageLink" => $_POST[$key],
										"imageTypeID" => $overlayType,
										"stockID" => $stockOptionID
									);
					$insertOverlay = query($conn, $insertOverlayQuery, $strType, $arrdbparams);
				}
			}
			
			$strcmd = "editStockOption";
			
		break;
		
		case "deleteImages":
			
			$deleteImages = explode(",", $_POST['deleteImages']);
			$rootPath = "../images/products/".$stockOptionID."/";
			
			foreach($deleteImages AS $deleteImage)
			{
				unlink($rootPath."zoom/".$deleteImage);
				unlink($rootPath."main/".$deleteImage);
				unlink($rootPath."list/".$deleteImage);
			}
			
			$strcmd = "editStockOption";
			
		break;
		
		case "addSiteBlock":
			
			$strdbsql = "SELECT COUNT(*) AS max_order FROM site_block_relations WHERE stockGroupID = :stockGroupID AND positionID = :positionID";
			$strType = "single";
			$arrdbparams = array(
								"stockGroupID" => $stockGroupID,
								"positionID" => $_POST['frm_siteblockposition']
							);
			$maxOrder = query($conn, $strdbsql, $strType, $arrdbparams);
			$order = $maxOrder['max_order'] + 1;
			
			foreach($_POST['frm_siteblocks'] AS $siteBlock)
			{
				$strdbsql = "INSERT INTO site_block_relations (blockID, positionID, stockGroupID, pageOrder) VALUES (:blockID, :positionID, :stockGroupID, :pageOrder)";
				$strType = "insert";
				$arrdbparams = array( 
									"blockID" => $siteBlock,
									"positionID" => $_POST['frm_siteblockposition'],
									"stockGroupID" => $stockGroupID,
									"pageOrder" => $order
								);
				$insertBlock = query($conn, $strdbsql, $strType, $arrdbparams);
				$order++;
				
				}
			$strcmd = "viewStockGroup";
			
		break;
		
		case "deleteBlocks":
			
			$strdbsql = "DELETE FROM site_block_relations WHERE recordID IN (".$_POST['deleteBlocks'].")";
			$strType = "delete";
			$removeImages = query($conn, $strdbsql, $strType);
			
			$strdbsql = "SELECT recordID FROM site_block_relations WHERE stockGroupID = :stockGroupID AND positionID = :positionID ORDER BY pageOrder";
			$strType = "multi";
			$arrdbparams = array(
							"pageRecordID" => $pageRecordID,
							"positionID" => $_POST['blockPositionID']
						);
			//echo "get products\n";
			$pageBlocks = query($conn, $strdbsql, $strType, $arrdbparams);
			//var_dump($categories);
			
			$i = 1;
			foreach($pageBlocks AS $pageBlock)
			{
				$strdbsql = "UPDATE site_block_relations SET pageOrder = ".$i." WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparam = array(
								"recordID" => $pageBlock['recordID']
							);
				//echo "update final ordering\n";
				query($conn, $strdbsql, $strType, $arrdbparam);
				$i++;
			}
			
			$strcmd = "viewStockGroup";
			
		break;
	}

	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//

	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Stock Control</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddStockGroup() {
					document.form.cmd.value='addStockGroup';
					document.form.submit();
				}
				function jsviewStockGroup(stockGroupID) {
					if ($('#form').valid()) {
						document.form.cmd.value='viewStockGroup';
						document.form.stockGroupID.value=stockGroupID;
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertStockGroup() {
					if ($('#form').valid()) {
						document.form.cmd.value='insertStockGroup';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsupdateStockGroup() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateStockGroup';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsaddStockOption() {
					document.form.cmd.value='addStockOption';
					document.form.submit();
				}
				function jsinsertStockOption() {
					document.form.cmd.value='insertStockOption';
					document.form.submit();
				}
				function jsupdateStockOption() {
					document.form.cmd.value='updateStockOption';
					document.form.submit();
				}
				function jseditStockOption(stockOptionID) {
					document.form.stockOptionID.value=stockOptionID;
					document.form.cmd.value='editStockOption';
					document.form.submit();
				}
				function jsdeleteStockOption(stockOptionID) {
					if(confirm("Are you sure you want to delete this stock option?"))
					{
						document.form.stockOptionID.value=stockOptionID;
						document.form.cmd.value='deleteStockOption';
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertImage() {
					document.form.cmd.value="insertImage";
					document.form.submit();
				}
				function jsupdateOverlay(overlayType) {
					document.form.overlayType.value=overlayType;
					document.form.cmd.value="updateOverlay";
					document.form.submit();
				}
				function jsdeleteImages() {
					if ($("#unusedImages li.selected").length > 0)
					{
						if(confirm("Are you sure you want to delete this image(s)?"))
						{
							var deleteImages = "";
							$("#unusedImages li.selected").each(function (index, item) {
								deleteImages += /[^/]*$/.exec($(item).children("img").attr("src"))[0]+",";
							});
							deleteImages = deleteImages.replace(/,\s*$/, "");
							//console.log(deleteImages);
							document.form.cmd.value="deleteImages";
							document.form.deleteImages.value=deleteImages;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the image(s) you wish to delete");
					}
					
				}
				function jsaddSiteBlock() {
					if ($('#form').valid()) {
						document.form.cmd.value='addSiteBlock';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsdeleteSiteBlocks() {
					if ($(".blockPosList li.selected").length > 0)
					{
						if(confirm("Are you sure you want to delete this block(s)?"))
						{
							var deleteBlocks = "";
							$(".blockPosList li.selected").each(function (index, item) {
								deleteBlocks += $(item).attr("id")+",";
							});
							$("#blockPositionID").val($(".blockPosList li.selected").attr("data-pos"));
							//console.log($(".blockPosList li.selected").parent().attr("id"));
							deleteBlocks = deleteBlocks.replace(/,\s*$/, "");
							//console.log(deleteImages);
							document.form.cmd.value="deleteBlocks";
							document.form.deleteBlocks.value=deleteBlocks;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the block(s) you wish to delete");
					}
					
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				$().ready(function() {
					// Return a helper with preserved width of cells
					var fixHelper = function(e, ui) {
						ui.children().each(function() {
							$(this).width($(this).width());
						});
						return ui;
					};

					$("#relatedProducts").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#unrelatedProducts").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#unrelatedProducts'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								
								if ($(item).parent().is("ul#relatedProducts")) {
									var oldOrder = $(item).attr("data-order");
									$("ul#relatedProducts").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									var newOrder = $(item).attr("data-order");
									//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
									if (parseInt(newOrder) > parseInt(oldOrder))
									{
										sortType = "DESC";
									}
									else if (parseInt(newOrder) < parseInt(oldOrder))
									{
										sortType = "ASC";
									}
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["stockCode1"] = $("#stockGroupID").attr("value");
									dataArray[i]["stockCode2"] = $(item).attr('id');
									dataArray[i]["cmd"] = "reorder";
									//console.log(dataArray);
								} else if ($(item).parent().is("ul#unrelatedProducts")) {
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["stockCode1"] = $("#stockGroupID").attr("value");
									dataArray[i]["stockCode2"] = $(item).attr('id');
									dataArray[i]["cmd"] = "delete";
									//console.log(dataArray);
									
									
									$(item).removeAttr("data-order");
									//console.log($.trim($(item).children().first().text()));
									$.trim($(item).children().first());
									//console.log(item);
									$("ul#relatedProducts").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
								}
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									console.log("sort ascending");
									return parseInt(a.order) - parseInt(b.order);
								}
								else if (sortType == "DESC")
								{
									console.log("sort descending");
									return parseInt(b.order) - parseInt(a.order);
								}
							});
							console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_relatedproductmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
					
					$("#unrelatedProducts").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#relatedProducts").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#relatedProducts'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function(event, ui) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							$(elements).each(function(index, item) {
								if ($(item).parent().is("ul#relatedProducts")) {
									$(item).parent().children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["stockCode1"] = $("#stockGroupID").attr("value");
									dataArray[i]["stockCode2"] = $(item).attr('id');
									dataArray[i]["cmd"] = "add";
									console.log(dataArray);
								}
								i++;
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_relatedproductmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();

					$("#sortableStockImages").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#unusedImages").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#unusedImages'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								
								if ($(item).parent().is("ul#sortableStockImages")) {
									var oldOrder = $(item).attr("data-order");
									$("ul#sortableStockImages").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									var newOrder = $(item).attr("data-order");
									//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
									if (parseInt(newOrder) > parseInt(oldOrder))
									{
										sortType = "DESC";
									}
									else if (parseInt(newOrder) < parseInt(oldOrder))
									{
										sortType = "ASC";
									}
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["imageLink"] = /[^/]*$/.exec($(item).children("img").attr("src"))[0];
									dataArray[i]["stockID"] = $("#stockOptionID").attr("value");
									dataArray[i]["cmd"] = "reorder";
									//console.log(dataArray);
								} else if ($(item).parent().is("ul#unusedImages")) {
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["imageLink"] = /[^/]*$/.exec($(item).children("img").attr("src"))[0];
									dataArray[i]["stockID"] = $("#stockOptionID").attr("value");
									dataArray[i]["cmd"] = "delete";
									//console.log(dataArray);
									
									
									$(item).removeAttr("data-order");
									//console.log($.trim($(item).children().first().text()));
									$.trim($(item).children().first());
									//console.log(item);
									$("ul#sortableStockImages").children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
								}
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									console.log("sort ascending");
									return parseInt(a.order) - parseInt(b.order);
								}
								else if (sortType == "DESC")
								{
									console.log("sort descending");
									return parseInt(b.order) - parseInt(a.order);
								}
							});
							console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_stockimagemanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
					
					$("#unusedImages").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
							$("#sortableStockImages").children().removeClass('selected');
						}
					}).sortable({
						connectWith: ['#sortableStockImages'],
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							
							//Clone the selected items into an array
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function(event, ui) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							$(elements).each(function(index, item) {
								if ($(item).parent().is("ul#sortableStockImages")) {
									$(item).parent().children("li").each(function(index, item) {
										$(item).attr("data-order", $(item).index() + 1);
									});
									
									dataArray[i] = {};
									dataArray[i]["order"] = $(item).attr("data-order");
									dataArray[i]["imageLink"] = /[^/]*$/.exec($(item).children("img").attr("src"))[0];
									dataArray[i]["stockID"] = $("#stockOptionID").attr("value");
									dataArray[i]["cmd"] = "add";
									console.log(dataArray);
								}
								i++;
							});
							$.ajax({
								type: "POST",
								url: "includes/ajax_stockimagemanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
					
					$(".blockPosList").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
						$(this).parent().parent().siblings().children("ul").children().removeClass('selected');
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							//console.log(item);
							
							//Clone the selected items into an array
							//console.log(item.parent().children('.selected'));
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								//console.log($(item));
								//console.log($(item).attr('data-order'));
								var oldOrder = $(item).attr("data-order");
								
								$(item).parent().children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if (parseInt(newOrder) > parseInt(oldOrder))
								{
									sortType = "DESC";
								}
								else if (parseInt(newOrder) < parseInt(oldOrder))
								{
									sortType = "ASC";
								}
								
								dataArray[i] = {};
								dataArray[i]["pageOrder"] = $(item).attr("data-order");
								dataArray[i]["positionID"] = $(item).attr("data-pos");
								dataArray[i]["recordID"] = $(item).attr("id");
								dataArray[i]["sitePageID"] = $("#stockGroupID").attr("value");
								dataArray[i]["type"] = "stockGroup";
								dataArray[i]["cmd"] = "reorder";
								//console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									//console.log("sort ascending");
									return parseInt(a.pageOrder) - parseInt(b.pageOrder);
								}
								else if (sortType == "DESC")
								{
									//console.log("sort descending");
									return parseInt(b.pageOrder) - parseInt(a.pageOrder);
								}
							});
							//console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_siteblockrelationmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
			</script>
			<?php
		
			print ("<form action='catalogue-manage-products.php' class='uniForm' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				print ("<input type='hidden' name='stockGroupID' id='stockGroupID' value='$stockGroupID'/>");
				print ("<input type='hidden' name='stockOptionID' id='stockOptionID' value='$stockOptionID'/>");
				print ("<input type='hidden' name='overlayType' id='overlayType' value='$overlayType'/>");
				print ("<input type='hidden' name='blockPositionID' id='blockPositionID' />");
				
				switch($strcmd)
				{
					case "addStockGroup":
					case "viewStockGroup":
					
						if ($strcmd == "viewStockGroup")
						{
							//$strdbsql = "SELECT stock_group_information.*, stock_status.colour FROM stock_group_information INNER JOIN stock_status ON stock_group_information.statusID = stock_status.recordID WHERE stock_group_information.recordID = :recordID";
							$strdbsql = "SELECT * FROM stock_group_information WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $stockGroupID);
							$stockGroupDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							
							/*print ("<pre>");
							print_r($stockGroupDetails);
							print ("</pre>");*/
							
							print ("<fieldset class='inlineLabels'> <legend>Change Item Details</legend>");
						}
						elseif ($strcmd == "addStockGroup")
						{
							$stockGroupDetails = array(
												"name" => "",
												"retailPrice" => "",
												"brandID" => "",
												"statusID" => "",
												"metaLink" => "",
												"metaDoctitle" => "",
												"metaKeywords" => "",
												"metaDescription" => "",
												"description" => "" /*,
												"colourOption" => 0,
												"fabricOption" => 0,
												"baseOption" => 0,
												"finishOption" => 0,
												"dimensionOption" => 0*/
											);
							
							print ("<fieldset class='inlineLabels'> <legend>Add Item Details</legend>");
						}
						
						if(!empty($stockGroupDetails['dateCreated']) && $stockGroupDetails['dateCreated'] > 0)
						{
							print ("<div class='form-group'>
								<label class='col-sm-2 control-label'>Date Created:</label>
								<div class='col-sm-2'>".
								  date("d/m/Y", $stockGroupDetails['dateCreated'])
								."</div>
							</div>");
						}
						if(!empty($stockGroupDetails['dateLastModified']) && $stockGroupDetails['dateLastModified'] > 0)
						{
							print ("<div class='form-group'>
								<label class='col-sm-2 control-label'>Date Last Modified:</label>
								<div class='col-sm-2'>".
								  date("d/m/Y", $stockGroupDetails['dateLastModified'])
								."</div>
							</div>");
						}
						print ("<div class='form-group'>
							<label for='frm_productname' class='col-sm-2 control-label'>Name</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_productname' name='frm_productname' value='".$stockGroupDetails['name']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_retailprice' class='col-sm-2 control-label'>Retail Price (&pound;)</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_retailprice' name='frm_retailprice' value='".$stockGroupDetails['retailPrice']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_brand' class='col-sm-2 control-label'>Product Brand</label>
							<div class='col-sm-10'>
								<select name='frm_brand' id='frm_brand' class='form-control' style='width: 200px;'>
									<option value='null'>None</option>");
									$strdbsql = "SELECT * FROM stock_brands";
									$strType = "multi";
									$stockBrands = query($conn, $strdbsql, $strType);								  
									foreach ($stockBrands AS $stockBrand)
									{
										$strSelected = "";
										if($stockBrand['recordID'] == $stockGroupDetails['brandID'])
										{
											$strSelected = " selected";
										}
										print("<option value='".$stockBrand['recordID']."'".$strSelected.">".$stockBrand['brandName']."</option>");
									}
								print ("</select>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_status' class='col-sm-2 control-label'>Product Status</label>
							<div class='col-sm-10'>
								<select name='frm_status' id='frm_status' class='form-control' style='width: 200px;'>");
									$strdbsql = "SELECT * FROM stock_status";
									$strType = "multi";
									$stockStatuses = query($conn, $strdbsql, $strType);								  
									foreach ($stockStatuses AS $stockStatus)
									{
										$strSelected = "";
										if($stockStatus['recordID'] == $stockGroupDetails['statusID'])
										{
											$strSelected = " selected";
										}
										print("<option value='".$stockStatus['recordID']."'".$strSelected.">".$stockStatus['description']."</option>");
									}
								print("</select>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_metalink' class='col-sm-2 control-label'>Meta Link</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_metalink' name='frm_metalink' value='".$stockGroupDetails['metaLink']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_metadoctitle' class='col-sm-2 control-label'>Meta Doc Title</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_metadoctitle' name='frm_metadoctitle' value='".$stockGroupDetails['metaDoctitle']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_metakeywords' class='col-sm-2 control-label'>Meta Keywords</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_metakeywords' name='frm_metakeywords' value='".$stockGroupDetails['metaKeywords']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_metadescription' class='col-sm-2 control-label'>Meta Description</label>
							<div class='col-sm-5'>
							  <textarea class='form-control' id='frm_metadescription' name='frm_metadescription' rows='3'>".$stockGroupDetails['metaDescription']."</textarea>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_productdescription' class='col-sm-2 control-label'>Description</label>
							<div class='col-sm-5'>
							  <textarea class='form-control' id='frm_productdescription' name='frm_productdescription' rows='5'>".$stockGroupDetails['description']."</textarea>
							</div>
						  </div>
						  <div class='form-group'>
							<label class='col-sm-2 control-label'>Configurable Options</label>
						  </div>");
						  $strdbsql = "SELECT * FROM stock_attribute_type";
						  $strType = "multi";
						  $attributeTypes = query($conn, $strdbsql, $strType);
						  
						  $attributesArray = explode(",", $stockGroupDetails['optionsList']);
						  
						  print ("<div class='form-group'>");
						  
							  foreach($attributeTypes AS $attributeType)
							  {
								print ("<label for='frm_config".$attributeType['description']."' class='col-sm-1 control-label'>".$attributeType['description']."</label>
									<div class='col-sm-1'>");
									  if(in_array($attributeType['recordID'], $attributesArray))
									  {
										$strChecked = " checked";
									  }
									  else
									  {
										$strChecked = "";
									  }
									  print ("<input type='checkbox' id='frm_config".$attributeType['description']."' name='frm_config".$attributeType['description']."' value='1' ".$strChecked." />
									</div>");
							  }
						  
						  print ("</div>");
						
						/*print ("<div class='form-group'>
							<label for='frm_configcolours' class='col-sm-1 control-label'>Colours</label>
							<div class='col-sm-1'>");
							  if($stockGroupDetails['colourOption'])
							  {
								$strChecked = " checked";
							  }
							  else
							  {
								$strChecked = "";
							  }
							  print ("<input type='checkbox' id='frm_configcolours' name='frm_configcolours' value='1' ".$strChecked." />
							</div>
							<label for='frm_configfabrics' class='col-sm-1 control-label'>Fabrics</label>
							<div class='col-sm-1'>");
							  if($stockGroupDetails['fabricOption'])
							  {
								$strChecked = " checked";
							  }
							  else
							  {
								$strChecked = "";
							  }
							  print ("<input type='checkbox' id='frm_configfabrics' name='frm_configfabrics' value='1' ".$strChecked." />
							</div>
							<label for='frm_configbases' class='col-sm-1 control-label'>Base</label>
							<div class='col-sm-1'>");
							  if($stockGroupDetails['baseOption'])
							  {
								$strChecked = " checked";
							  }
							  else
							  {
								$strChecked = "";
							  }
							  print ("<input type='checkbox' id='frm_configbases' name='frm_configbases' value='1' ".$strChecked." />
							</div>
							<label for='frm_configfinishes' class='col-sm-1 control-label'>Finishes</label>
							<div class='col-sm-1'>");
							  if($stockGroupDetails['finishOption'])
							  {
								$strChecked = " checked";
							  }
							  else
							  {
								$strChecked = "";
							  }
							  print ("<input type='checkbox' id='frm_configfinishes' name='frm_configfinishes' value='1' ".$strChecked." />
							</div>");
							/*<label for='frm_configdimensions' class='col-sm-1 control-label'>Dimensions</label>
							<div class='col-sm-1'>");
							  if($stockGroupDetails['dimensionOption'])
							  {
								$strChecked = " checked";
							  }
							  else
							  {
								$strChecked = "";
							  }
							  print ("<input type='checkbox' id='frm_configdimensions' name='frm_configdimensions' value='1' ".$strChecked." />
							</div>
						print("</div>");*/
				print ("<div class='form-group'>
							<div class='col-sm-10'>");
							if ($strcmd == "viewStockGroup")
							{
								print("<button onclick='return jsupdateStockGroup();' type='submit' class='btn btn-success'>Update Stock Group</button> ");
							}
							elseif ($strcmd == "addStockGroup")
							{
								print("<button onclick='return jsinsertStockGroup();' type='submit' class='btn btn-success'>Add Stock Group</button> ");
							}
							  
							  print ("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>
							</div>
						  </div>");
						print ("</fieldset>");
						
						print ("<br/>");
						
						if ($strcmd == "viewStockGroup")
						{
							print ("<fieldset class='inlineLabels'> <legend>Manage Stock Options</legend>");						
							
							$strdbsql = "SELECT * FROM stock WHERE groupID = :groupID";
							$strType = "multi";
							$arrdbparams = array("groupID" => $stockGroupID);
							$stockOptions = query($conn, $strdbsql, $strType, $arrdbparams);
							
							/*print ("<pre>");
							print_r($stockOptions);
							print ("</pre>");*/
							
							if (count($stockOptions) != 0) {
								print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
									print ("<tr><thead>");
										//print ("<th>SKU</th>");
										print ("<th>Stock Code</th>");
										//print ("<th>Model Reference</th>");
										print ("<th>Price (&pound;)</th>");
										print ("<th colspan='2'></th>");
									print ("</tr></thead><tbody>");
									foreach ($stockOptions as $stockOption) {
										print ("<tr>");
											//print ("<td>".$stockOption['sku']."</td>");
											print ("<td>".$stockOption['stockCode']."</td>");
											//print ("<td>".$stockOption['modelReference']."</td>");
											print ("<td>".$stockOption['price']."</td>");
											print ("<td style='width: 83px;'><button onclick='return jseditStockOption(\"".$stockOption['recordID']."\", \"".$stockGroupDetails['recordID']."\");' type='submit' class='btn btn-primary'>Update</button></td>");
											print ("<td style='width: 78px;'><button onclick='return jsdeleteStockOption(\"".$stockOption['recordID']."\", \"".$stockGroupDetails['recordID']."\");' type='submit' class='btn btn-danger'>Delete</button></td>");
										print ("</tr>");
									}
									print ("</tbody>");
								print ("</table>");
							}
							print ("<div class='form-group'>
								<div class='col-sm-10'>
								  <button onclick='return jsaddStockOption();' type='submit' class='btn btn-success'>Add</button>
								</div>
							  </div>");
							  
							print ("</fieldset>");
							print ("<fieldset class='inlineLabels'> <legend>Manage Related Products</legend>");
							
								$getProductsQuery = "SELECT * FROM stock_group_information WHERE recordID != :recordID ORDER BY name ASC";
								$strType = "multi";
								$arrdbparam = array("recordID" => $stockGroupID);
								$unrelatedProducts = query($conn, $getProductsQuery, $strType, $arrdbparam);
								
								$getRelatedProductsQuery = "SELECT stock_group_information.*, stock_related.relatedOrder FROM stock_group_information INNER JOIN stock_related ON stock_group_information.recordID = stock_related.stockCode2 WHERE stock_related.stockCode1 = :recordID ORDER BY stock_related.relatedOrder";
								$strType = "multi";
								$arrdbparam = array("recordID" => $stockGroupID);
								$relatedProducts = query($conn, $getRelatedProductsQuery, $strType, $arrdbparam);
								$relatedProductOptions = "";
								
								foreach ($relatedProducts AS $relatedProduct)
								{
									$relatedProductOptions .= "<li id='".$relatedProduct['recordID']."' data-order='".$relatedProduct['relatedOrder']."'><span>".$relatedProduct['name']." (".$relatedProduct['metaLink'].")</span></li>";
									unset($relatedProduct['relatedOrder']);
									$key = array_search($relatedProduct, $unrelatedProducts);
									if($key !== false)
									{
										unset($unrelatedProducts[$key]);
									}
								}
								
								
								print("<div class='form-group'>
										<div class='col-sm-4'>
											<label for='frm_stocklevels' class='control-label'>Products</label>
											<br/>
											<ul id='unrelatedProducts'>");
												foreach($unrelatedProducts AS $unrelatedProduct)
												{
													print("<li id='".$unrelatedProduct['recordID']."'><span>".$unrelatedProduct['name']." (".$unrelatedProduct['metaLink'].")</span></li>");																
												}
										print ("</ul>");
									print ("</div>");
									print ("<div class='col-sm-4'>
											<label for='frm_stocklevels' class='control-label'>Assigned Products</label>
											<br/>
											<ul id='relatedProducts'>");
												print ($relatedProductOptions);
									print ("</ul>");
								print ("</div>
								</div>
							</fieldset>");
							
							print ("<fieldset class='inlineLabels'> <legend>Site Blocks</legend>");
								$strdbsql = "SELECT * FROM site_block_position";
								$strType = "multi";
								$siteBlockPositions = query($conn, $strdbsql, $strType);
								$siteBlockPositionSelectOptions = "";
								
								print ("<div class='form-group'>");
									foreach($siteBlockPositions AS $siteBlockPosition)
									{
										print ("<div class='col-sm-2'>");
											print ("<h3>".$siteBlockPosition['description']."</h3>");
											print ("<ul class='blockPosList' id='pos-".$siteBlockPosition['recordID']."'>");
												$strdbsql = "SELECT * FROM site_blocks INNER JOIN site_block_relations ON site_block_relations.blockID = site_blocks.recordID WHERE site_block_relations.stockGroupID = :stockGroupID AND site_block_relations.positionID = :positionID ORDER BY site_block_relations.pageOrder";
												$strType = "multi";
												$arrdbparams = array(
																	"stockGroupID" => $stockGroupID,
																	"positionID" => $siteBlockPosition['recordID']
																);
												$siteBlocks = query($conn, $strdbsql, $strType, $arrdbparams);
												foreach($siteBlocks AS $siteBlock)
												{
													print ("<li id='".$siteBlock['recordID']."' data-order='".$siteBlock['pageOrder']."' data-pos='".$siteBlockPosition['recordID']."'><span>".$siteBlock['description']."</span></li>");
												}
											print ("</ul>");
										print ("</div>");
										$siteBlockPositionSelectOptions .= "<option value='".$siteBlockPosition['recordID']."'>".$siteBlockPosition['description']."</option>";
									}
								print ("</div>");
								print ("<div class='form-group'>");
									print ("<button onclick='return jsdeleteSiteBlocks();' type='button' class='btn btn-danger'>Delete Selected</button>");
									print ("<input type='hidden' name='deleteBlocks' id='deleteBlocks' />");
								print ("</div>");
								
								print ("<br/>");
								print ("<legend>Add Blocks to Stock Group</legend>");
								print ("<div class='form-group'>");
									print ("<label for='frm_siteblocks' class='col-sm-2 control-label'>Available Blocks</label>");
									print ("<br/>");
									print ("<br/>");
									$strdbsql = "SELECT * FROM site_blocks ORDER BY description ASC";
									$strType = "multi";
									$siteBlocks = query($conn, $strdbsql, $strType);
									
									print ("<select name='frm_siteblocks[]' id='frm_siteblocks' class='form-control valid' style='width: 300px;' multiple>");
										foreach($siteBlocks AS $siteBlock)
										{
											print ("<option value='".$siteBlock['recordID']."'>".$siteBlock['description']."</option>");
										}
									print ("</select>");
									
								print ("</div>");
								print ("<div class='form-group'>");
									print ("<label for='frm_siteblockposition' class='col-sm-2 control-label'>Block Position</label>");
									print ("<br/>");
									print ("<br/>");
									
									print ("<select name='frm_siteblockposition' id='frm_siteblockposition' class='form-control valid' style='width: 300px;'>");
										print ($siteBlockPositionSelectOptions);
									print ("</select>");
									
								print ("</div>");
								print ("<div class='form-group'>
											<div class='col-sm-10'>");
												print ("<button onclick='return jsaddSiteBlock();' type='submit' class='btn btn-success'>Add Site Block</button> ");
									print ("</div>
										</div>");
							print ("</fieldset>");
							
							
						}
						
					break;
					
					case "addStockOption":
					case "editStockOption":
					
						$strdbsql = "SELECT * FROM stock_group_information WHERE recordID = :recordID";
						$strType = "single";
						$arrdbparams = array("recordID" => $stockGroupID);
						$stockGroupDetails = query($conn, $strdbsql, $strType, $arrdbparams);
						
						if ($strcmd == "editStockOption")
						{
							$strdbsql = "SELECT * FROM stock WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $stockOptionID);
							$stockOptionDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							
							/*print ("<pre>");
							print_r($stockOptionDetails);
							print ("</pre>");*/
							
							print ("<fieldset class='inlineLabels'> <legend>Change Item Option Details</legend>");
						}
						elseif ($strcmd == "addStockOption")
						{
							$stockOptionDetails = array(
												"stockCode" => "",
												"stockLevels" => 0,
												"priceControl" => "0.00",
												"price" => "0.00",
												"colourID" => 0,
												"fabricID" => 0,
												"dimensionID" => 0,
												"baseID" => 0
											);
							
							print ("<fieldset class='inlineLabels'> <legend>Add Item Option Details</legend>");
						}
						
						print ("<div class='form-group'>
							<label for='frm_stockcode' class='col-sm-2 control-label'>Stock Code</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_stockcode' name='frm_stockcode' value='".$stockOptionDetails['stockCode']."' />
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_stocklevels' class='col-sm-2 control-label'>Stock Levels</label>
							<div class='col-sm-10'>
							  <select name='frm_stocklevels' id='frm_stocklevels' class='form-control' style='width: 75px;'>");
							  for ($i = 1; $i < 50; $i++)
							  {
								$strselected = "";
								if($i == $stockOptionDetails['stockLevels'])
								{
									$strselected = " selected";
								}
								print ("<option value='".$i."'".$strselected.">".$i."</option>");
							  }							  
							print ("</select>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_pricecontrol' class='col-sm-2 control-label'>Price Control (&pound;)</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_pricecontrol' name='frm_pricecontrol' value='".$stockOptionDetails['priceControl']."' />
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_price' class='col-sm-2 control-label'>Current Price (&pound;)</label>
							<div class='col-sm-10'>
							  <input style='width:200px;' type='text' class='form-control' id='frm_price' name='frm_price' value='".$stockOptionDetails['price']."' />
							</div>
						  </div>");
						  
						  if(!empty($stockGroupDetails['optionsList']))
						  {
							  $attributesArray = explode(",", $stockGroupDetails['optionsList']);
							  $strdbsql = "SELECT * FROM stock_attribute_type WHERE recordID IN (".$stockGroupDetails['optionsList'].")";
							  $strType = "multi";
							  $attributeTypes = query($conn, $strdbsql, $strType);
							  
							  $stockAttributeValues = array();
							  if ($strcmd == "editStockOption")
							  {
								  $strdbsql = "SELECT stockAttributeID FROM stock_attribute_relations WHERE stockID = :stockID";
								  $strType = "multi";
								  $arrdbparams = array( "stockID" => $stockOptionID );
								  $stockAttributeValuesMulti = query($conn, $strdbsql, $strType, $arrdbparams);
								  
								  foreach($stockAttributeValuesMulti AS $stockAttributeValuesSingle)
								  {
									$stockAttributeValues[] = $stockAttributeValuesSingle['stockAttributeID'];
								  }
							  }
								 
							  
							  foreach($attributeTypes AS $attributeType)
							  {
								 print ("<div class='form-group'>
									<label for='frm_".$attributeType['description']."' class='col-sm-2 control-label'>".$attributeType['description']."</label>
									<div class='col-sm-10'>
										<select name='frm_".$attributeType['description']."' id='frm_".$attributeType['description']."' class='form-control' style='width: 200px;'>");
											$strdbsql = "SELECT * FROM stock_attributes WHERE attributeTypeID = :typeID";
											$strType = "multi";
											$arrdbparams = array( "typeID" => $attributeType['recordID'] );
											$stockAttributes = query($conn, $strdbsql, $strType, $arrdbparams);								  
											foreach ($stockAttributes AS $stockAttribute)
											{
												$strSelected = "";
												if(in_array($stockAttribute['recordID'], $stockAttributeValues))
												{
													$strSelected = " selected";
												}
												print("<option value='".$stockAttribute['recordID']."'".$strSelected.">".$stockAttribute['description']."</option>");
											}
										print("</select>
									</div>
								  </div>");
							  }
							 }
						  
						  /*if ($stockGroupDetails['colourOption'] == 1)
						  {
							  print ("<div class='form-group'>
								<label for='frm_colour' class='col-sm-2 control-label'>Colour</label>
								<div class='col-sm-10'>
									<select name='frm_colour' id='frm_colour' class='form-control' style='width: 200px;'>");
										$strdbsql = "SELECT * FROM stock_colours";
										$strType = "multi";
										$stockColours = query($conn, $strdbsql, $strType);								  
										foreach ($stockColours AS $stockColour)
										{
											$strSelected = "";
											if($stockColour['recordID'] == $stockOptionDetails['colourID'])
											{
												$strSelected = " selected";
											}
											print("<option value='".$stockColour['recordID']."'".$strSelected.">".$stockColour['description']."</option>");
										}
									print("</select>
								</div>
							  </div>");
						  }
						  if ($stockGroupDetails['fabricOption'] == 1)
						  {
							  print ("<div class='form-group'>
								<label for='frm_fabric' class='col-sm-2 control-label'>Fabric</label>
								<div class='col-sm-10'>
									<select name='frm_fabric' id='frm_fabric' class='form-control' style='width: 200px;'>");
										$strdbsql = "SELECT * FROM stock_fabrics";
										$strType = "multi";
										$stockFabrics = query($conn, $strdbsql, $strType);								  
										foreach ($stockFabrics AS $stockFabric)
										{
											$strSelected = "";
											if($stockFabric['recordID'] == $stockOptionDetails['fabricID'])
											{
												$strSelected = " selected";
											}
											print("<option value='".$stockFabric['recordID']."'".$strSelected.">".$stockFabric['description']."</option>");
										}
									print("</select>
								</div>
							  </div>");
						  }
						  //if ($stockGroupDetails['dimensionOption'] == 1)
						  //{
							print ("<div class='form-group'>
								<label for='frm_dimension' class='col-sm-2 control-label'>Dimensions</label>
								<div class='col-sm-10'>
									<select name='frm_dimension' id='frm_dimension' class='form-control' style='width: 200px;'>");
										$strdbsql = "SELECT * FROM stock_dimensions";
										$strType = "multi";
										$stockDimensions = query($conn, $strdbsql, $strType);								  
										foreach ($stockDimensions AS $stockDimension)
										{
											$strSelected = "";
											if($stockDimension['recordID'] == $stockOptionDetails['dimensionID'])
											{
												$strSelected = " selected";
											}
											print("<option value='".$stockDimension['recordID']."'".$strSelected.">".$stockDimension['description']."</option>");
										}
									print("</select>
								</div>
							  </div>");
						  //}
						  if ($stockGroupDetails['baseOption'] == 1)
						  {
							print ("<div class='form-group'>
								<label for='frm_base' class='col-sm-2 control-label'>Base</label>
								<div class='col-sm-10'>
									<select name='frm_base' id='frm_base' class='form-control' style='width: 200px;'>");
										$strdbsql = "SELECT * FROM stock_base";
										$strType = "multi";
										$stockBases = query($conn, $strdbsql, $strType);								  
										foreach ($stockBases AS $stockBase)
										{
											$strSelected = "";
											if($stockBase['recordID'] == $stockOptionDetails['baseID'])
											{
												$strSelected = " selected";
											}
											print("<option value='".$stockBase['recordID']."'".$strSelected.">".$stockBase['description']."</option>");
										}
									print("</select>
								</div>
							  </div>");
						  }
						  if ($stockGroupDetails['finishOption'] == 1)
						  {
							print ("<div class='form-group'>
								<label for='frm_finish' class='col-sm-2 control-label'>Finish</label>
								<div class='col-sm-10'>
									<select name='frm_finish' id='frm_finish' class='form-control' style='width: 200px;'>");
										$strdbsql = "SELECT * FROM stock_finish";
										$strType = "multi";
										$stockFinishes = query($conn, $strdbsql, $strType);								  
										foreach ($stockFinishes AS $stockFinish)
										{
											$strSelected = "";
											if($stockFinish['recordID'] == $stockOptionDetails['finishID'])
											{
												$strSelected = " selected";
											}
											print("<option value='".$stockFinish['recordID']."'".$strSelected.">".$stockFinish['description']."</option>");
										}
									print("</select>
								</div>
							  </div>");
						  }*/
						  print ("<div class='form-group'>
								<label for='frm_dimension' class='col-sm-2 control-label'>Dimensions</label>
								<div class='col-sm-10'>
									<select name='frm_dimension' id='frm_dimension' class='form-control' style='width: 200px;'>
										<option value='null'>None</option>");
										$strdbsql = "SELECT * FROM stock_dimensions";
										$strType = "multi";
										$stockDimensions = query($conn, $strdbsql, $strType);								  
										foreach ($stockDimensions AS $stockDimension)
										{
											$strSelected = "";
											if($stockDimension['recordID'] == $stockOptionDetails['dimensionID'])
											{
												$strSelected = " selected";
											}
											print("<option value='".$stockDimension['recordID']."'".$strSelected.">".$stockDimension['description']."</option>");
										}
									print("</select>
								</div>
							  </div>");
						  print ("<div class='form-group'>
							<div class='col-sm-offset-2 col-sm-10'>");
							
							if ($strcmd == "editStockOption")
							{
								print("<button onclick='return jsupdateStockOption();' type='submit' class='btn btn-success'>Update Stock Option</button> ");
							}
							elseif ($strcmd == "addStockOption")
							{
								print("<button onclick='return jsinsertStockOption();' type='submit' class='btn btn-success'>Add Stock Option</button> ");
							}
							  
						  print ("<button onclick='return jscancel(\"viewStockGroup\");' class='btn btn-danger'>Cancel</button>
							</div>
						  </div>");
						  
						print ("</fieldset>");
						
						if ($strcmd == "editStockOption")
						{
							print ("<fieldset class='inlineLabels'> <legend>Manage Item Images</legend>");
								$getImageTypesQuery = "SELECT * FROM stock_image_types ORDER BY recordID";
								$strType = "multi";
								$stockImageTypes = query($conn, $getImageTypesQuery, $strType);
								
								$dh = opendir("../images/overlays");
								while (false !== ($overlayImg = readdir($dh))) {
									if ($overlayImg != "." && $overlayImg != "..") {
										$overlays[] = $overlayImg;
									}
								}
								
								foreach($stockImageTypes AS $stockImageType)
								{
									$getStockImagesQuery = "SELECT * FROM stock_images WHERE imageTypeID = :typeID AND stockID = :stockID ORDER BY imageOrder";
									$strType = "multi";
									$arrdbparams = array(
														"typeID" => $stockImageType['recordID'],
														"stockID" => $stockOptionID
													);
									$stockImages = query($conn, $getStockImagesQuery, $strType, $arrdbparams);
									
									if($stockImageType['recordID'] == 1)
									{
										$unusedImages = array();
										$dh = opendir("../images/products/".$stockOptionID."/list");
										while (false !== ($image = readdir($dh))) {
											if ($image != "." && $image != "..") {
												$unusedImages[] = $image;
											}
										}
										
										$assignedImages = "";
										if(!empty($stockImages))
										{
											foreach ($stockImages AS $stockImage)
											{
												$assignedImages .= "<li id='".$stockImage['recordID']."' data-order='".$stockImage['imageOrder']."'><img src='../images/products/".$stockOptionID."/list/".$stockImage['imageLink']."' /></li>";
												$key = array_search($stockImage['imageLink'], $unusedImages);
												if($key !== false)
												{
													unset($unusedImages[$key]);
												}
											}
										}
										
										if (count($unusedImages) > 0 || $assignedImages != "")
										{
											print ("<div class='form-group'>");
												print ("<div class='col-sm-3'>");
													print ("<h3>Unused Stock Item Images</h3>");
													
													print ("<ul id='unusedImages'>");
														if(count($unusedImages) > 0)
														{	
															foreach ($unusedImages AS $unusedImage)
															{
																print ("<li><img src='../images/products/".$stockOptionID."/list/".$unusedImage."' /></li>");
															}
														}
													print ("</ul>");
													print ("<br/>");
													print ("<button onclick='return jsdeleteImages();' type='button' class='btn btn-danger'>Delete Selected</button>");
													print ("<input type='hidden' name='deleteImages' id='deleteImages' />");
												print ("</div>");
												print ("<div class='col-sm-3'>");
													print ("<h3>".$stockImageType['description']."</h3>");
													
													print ("<ul id='sortableStockImages'>");
														print($assignedImages);
													print ("</ul>");
												print ("</div>");
											print ("</div>");
										}
									}
									else
									{
										if (!empty($stockImages[0]['imageLink']))
										{
											print ("<div class='form-group'>");
												print ("<label class='col-sm-2 control-label'>Current Overlay for ".$stockImageType['description']."</label>");
												print ("<div class='col-sm-2'><img src='../images/overlays/".$stockImages[0]['imageLink']."' /></div>");
											print ("</div>");
										}
										print ("<div class='form-group'>");
											print ("<label for='frm_overlay_".$stockImageType['recordID']."' class='col-sm-2 control-label'>Available Overlays for ".$stockImageType['description']."</label>");
											print ("<select name='frm_overlay_".$stockImageType['recordID']."' id='frm_overlay_".$stockImageType['recordID']."' class='form-control' style='width: 300px; display: inline-block; margin-right: 20px;'>");
												print ("<option value='0'>None</option>");
												if(!empty($overlays))
												{
													foreach ($overlays AS $overlay)
													{
														if (!empty($stockImages[0]['imageLink']) && $overlay == $stockImages[0]['imageLink'])
														{
															$strselected = " selected";
														}
														else
														{
															$strselected = "";
														}
														print ("<option value='".$overlay."'".$strselected.">".$overlay."</option>");
													}
												}
											print ("</select>");
											print ("<button onclick='return jsupdateOverlay(\"".$stockImageType['recordID']."\");' type='submit' class='btn btn-success'>Update Image</button>");
										print ("</div>");
									}
								}
								
								print("<br/>");
								print("<legend>Add New Image</legend>");
								
								print ("<div class='form-group'>
										<label for='frm_image' class='col-sm-2 control-label'>Image</label>
										<div class='col-sm-10'>
										  <input style='width:600px;' type='file' class='form-control' id='frm_image' name='frm_image' />
										</div>
									</div>
									<div class='form-group'>
										<label for='frm_imagedescrip' class='col-sm-2 control-label'>Description</label>
										<div class='col-sm-10'>
										  <input style='width:600px;' type='text' class='form-control' id='frm_imagedescrip' name='frm_imagedescrip' />
										</div>
									</div>
									<div class='form-group'>
										<label for='frm_imagetype' class='col-sm-2 control-label'>Image Type</label>
										<div class='col-sm-10'>
											<select name='frm_imagetype' id='frm_imagetype' class='form-control' style='width: 200px;'>");
												foreach ($stockImageTypes AS $stockImageType)
												{
													print("<option value='".$stockImageType['recordID']."'>".$stockImageType['description']."</option>");
												}
											print("</select>
										</div>
									</div>
									<div class='form-group'>
										<label for='frm_applyall' class='col-sm-2 control-label'>Apply to All Images in Group</label>
										<div class='col-sm-1'>");
											print ("<input type='checkbox' id='frm_applyall' name='frm_applyall' value='1' />
										</div>
									</div>
									<div class='form-group'>
										<div class='col-sm-offset-2 col-sm-10'>");
											print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success'>Insert Image</button>
										</div>
									</div>");
								
							print ("</fieldset>");
						}
						
					
					break;
					
					default:

						//allow options to select the category / brand / range products
						print ("<button onclick='return jsaddStockGroup();' type='submit' class='btn btn-success'>Add New</button>");
						print ("<br/>");
						print ("<br/>");
						print ("<table id='sortableStockTable' class='table table-striped table-bordered table-hover table-condensed' >");
							print ("<thead><tr>");
								print ("<th>Name</th>");
								//print ("<th>Description</th>");
								//print ("<th>Stockcode/s</th>");
								print ("<th style='width: 52px;'></th>");
							print ("</tr></thead>");
							/*print ("<tbody>");
							
							//This needs to be drag and drop to change the order
							
							$strdbsql = "SELECT stock_group_information.*, stock_status.colour FROM stock_group_information INNER JOIN stock_status ON stock_group_information.statusID = stock_status.recordID";
							$strType = "multi";
							$resultdata = query($conn, $strdbsql, $strType);
							
							if (count($resultdata) != 0) {
								foreach ($resultdata as $row) {
									
									$strdbsql = "SELECT * FROM stock WHERE groupID = :groupID";
									$strType = "multi";
									$arrdbparams = array("groupID" => $row['recordID']);
									$resultdata2 = query($conn, $strdbsql, $strType, $arrdbparams);
									
									$numStockOptions = count($resultdata2);
									$stockCode = "";
									
									if ($numStockOptions != 0) {
										$i = 1;
										foreach ($resultdata2 as $row2) {
											$stockCode .= $row2['stockCode'];
											if ($i < $numStockOptions)
											{
												$stockCode .= ", ";
											}
											$i++;
										}
									}

									$tablecolour = $row['colour'];
									print ("<tr>");
										print ("<td class='$tablecolour'>".$row['name']."</td>");
										//print ("<td class='$tablecolour'>".$row['description']."</td>");
										print ("<td class='$tablecolour'>$stockCode</td>");
										print ("<td class='$tablecolour'><button onclick='jsviewStockGroup(".$row['recordID']."); return false;' class='center btn btn-primary' type='submit'>Modify</button></td>");				
									print ("</tr>");
								}
							} else {
								print ("<tr><td colspan='7'>Currently No Stock</td></tr>");
							}
							print ("</tbody>");*/
						print ("</table>");
						print ("<button onclick='return jsaddStockGroup();' type='submit' class='btn btn-success'>Add New</button>");
				
					break;
				}
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#sortableStockTable').dataTable({
				"processing": true,
				"serverSide": true,
				"ajax": {
					"type": "POST",
					"url": "includes/ajax_stockdatatablepagination.php"
				}
			});
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>