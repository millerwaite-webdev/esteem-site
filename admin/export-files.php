<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * export-files.php				                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

	// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "export-files"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	
	switch($strcmd)
	{
		case "exportFile":
			
			
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Export Files</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				
				function fnExportFile() {
					document.getElementById("cmd").value = "exportFile";
					document.getElementById("form").submit();
				}
				
			</script>
			<?php
		
			print ("<form action='generate-export-file.php' class='uniForm' target='_blank' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				
				switch($strcmd)
				{
					case "exportFile":
					
						break;
						
					default:
						
						print("<fieldset>");
							print("<legend>Export CSV Documents</legend>");
							print("<div class='form-group'>");
								print ("<label for='frm_exportFormatType' class='col-sm-2 control-label'>Export Format: </label><div class='col-sm-10'>");
								print ("<select name='frm_exportFormatType' id='frm_exportFormatType' class='form-control' style='width:400px;'>");
									print ("<option value='stock'>Stock Export</option>");
									print ("<option value='catalogueCategories'>Catalogue Export (Categories only)</option>");
									print ("<option value='catalogueRanges'>Catalogue Export (Ranges only)</option>");
									print ("<option value='catalogueAll'>Catalogue Export (All)</option>");
								print ("</select></div>");
							print ("</div>");
							print("<legend>Criteria</legend>");
							print("<div class='form-group'>");
								print ("<table class='table table-striped table-bordered table-hover table-condensed' style='table-layout: fixed;'>");
									print ("<thead>");
										print ("<tr>");
											print ("<th style='width: 33%;'>Stock Groups</th>");
											print ("<th style='width: 33%;'>Categories</th>");
											print ("<th style='width: 33%;'>Ranges</th>");
										print ("</tr>");
									print ("</thead>");
									print ("<tbody>");
										print ("<tr>");
											print ("<td>");
												print ("<select multiple name='frm_stock[]' style='height: 400px; width: 100%;'>");
													$getStockGroups = "SELECT name, recordID FROM stock_group_information ORDER BY name ASC";
													$strType = "multi";
													$stockGroups = query($conn, $getStockGroups, $strType);
													
													foreach($stockGroups AS $stockGroup)
													{
														print ("<option value='".$stockGroup['recordID']."'>".$stockGroup['name']."</option>");
													}
												print ("</select>");
											print ("</td>");
											print ("<td>");
												print ("<select multiple name='frm_cats[]' style='height: 400px; width: 100%;'>");
													$getCats = "SELECT contentName, recordID FROM category ORDER BY contentName ASC";
													$strType = "multi";
													$categories = query($conn, $getCats, $strType);
													
													foreach($categories AS $category)
													{
														print ("<option value='".$category['recordID']."'>".$category['contentName']."</option>");
													}
												print ("</select>");
											print ("</td>");
											print ("<td>");
												print ("<select multiple name='frm_ranges[]' style='height: 400px; width: 100%;'>");
													$getRanges = "SELECT rangeName, recordID FROM stock_ranges ORDER BY rangeName ASC";
													$strType = "multi";
													$ranges = query($conn, $getRanges, $strType);
													
													foreach($ranges AS $range)
													{
														print ("<option value='".$range['recordID']."'>".$range['rangeName']."</option>");
													}
												print ("</select>");
											print ("</td>");
										print ("</tr>");
									print ("</tbody>");
								print ("</table>");
							print("</div>");
							print("<div class='form-group'>");
									print("<div class='col-sm-10'>");
										print ("<button onclick='fnExportFile(); return false;' class='btn btn-success '>Export File</button>");
									print("</div>");
							print("</div>");
						print("</fieldset>");
					
						break;
				}
				
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>
