<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * cms-colours.php					                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


	// ************* Common page setup ******************** //
	//=====================================================//

//	use function inc\connect;
//use function inc\query;

session_start(); //stores session variables such as access levels and logon details
	$strpage = "cms-colours"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	include("includes/inc_imagefunctions.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['pageRecordID'])) $pageRecordID = $_REQUEST['pageRecordID']; else $pageRecordID = "";
	$strshowActive = (isset($_REQUEST['frm_active'])) ? 1 : 0;
	$strshowActive = 1;
	$pageRecordID = 1;
	
	switch($strcmd)
	{
		case "insertPage":
		case "updatePage":
			
			$arrdbparams = array(
								"colourPrimary" => $_POST['frm_primary'],
								"colourSecondary" => $_POST['frm_secondary'],
								"colourTertiary" => $_POST['frm_tertiary'],
								"colourBase" => $_POST['frm_base'],
								"colourBackground" => $_POST['frm_backgroundCol'],
								"imageBackground" => $_POST['frm_backgroundImg'],
								"font" => $_POST['frm_font'],
								"colourActive" => $strshowActive
							);
			
				//$strdbsql = "UPDATE site_colours SET colourPrimary = :colourPrimary, colourSecondary = :colourSecondary, colourTertiary = :colourTertiary, colourBase = :colourBase, colourBackground = :colourBackground, imageBackground = :imageBackground, 
				$strdbsql = "UPDATE site_colours SET colourPrimary = :colourPrimary, colourSecondary = :colourSecondary, colourTertiary = :colourTertiary, colourBase = :colourBase, colourBackground = :colourBackground, imageBackground = :imageBackground, font = :font,
							colourActive = :colourActive WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $pageRecordID;
				$strType = "update";
			
			$updatePage = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($updatePage <= 1)
			{
				$strsuccess = "Theme successfully updated";
			}
			elseif ($updatePage > 1)
			{
				$strwarning = "An error may have occurred while updating the theme";
			}
			
			$strcmd = "";
			
		break;
		
		case "insertImage":
			$newfilename = $_FILES['frm_newimage']['name'];
			$strfileextn = getExtension($newfilename);
			
			$strimguploadpath = "images/themes/";
			$struploaddir = $strrootpath.$strimguploadpath; // where to put full size image
			$upfile = $struploaddir.$newfilename;
			
			//        print ("$upfile - saving image<br>");

			if ($_FILES['frm_newimage']['error'] > 0)
			{
				switch ($_FILES['frm_newimage']['error'])
				{
					case 1:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 2:  $strerror = "Problem: File exceeded maximum filesize. Please try again with a smaller file.";  break;
					case 3:  $strerror = "Problem: File only partially uploaded. Please try again, and if it still fails, try a smaller file.";  break;
					case 4:  $strerror = "Problem: No file selected. Please try again.";  break;
				}
			}
			else
			{
				// Does the file have the right extension ?
				//if ($strfileextn != 'jpg' && $strfileextn != 'jpeg' && $strfileextn != 'png' && $strfileextn != 'gif')
				
				if (strcasecmp($strfileextn, 'jpg') != 0 && strcasecmp($strfileextn, 'jpeg') != 0 && strcasecmp($strfileextn, 'png') != 0 && strcasecmp($strfileextn, 'gif') != 0)
				{
					$strerror = "Problem: The file you selected is not an acceptable format.<br/>You may only upload jpeg, png or gif image files.<br/><br/>The file you are trying to upload is a ".$_FILES['frm_newimage']['type']." file.<br/><br/>Please convert the picture to the required format and try again.";
				}
				else
				{
					//      print ("Uploading to products image directory<br>");
					if (is_uploaded_file($_FILES['frm_newimage']['tmp_name']))
					{
						if (!move_uploaded_file($_FILES['frm_newimage']['tmp_name'], $upfile))
						{
							$strerror = "Problem: File is uploaded to :- ".$_FILES['frm_newimage']['tmp_name']." and could not move file to ".$upfile.". Please try again.";
						}
					}
					else
					{
						$strerror = "Problem: Possible file upload attack. Filename: ".$_FILES['frm_newimage']['name'].". Please try again.<br/>";
					}
				}
			}
			
			if ($strerror != "")
			{
				$strcommand = "ERRORMSG";
				//   print ("Got an Error - $strcommand - $strerror<br>");
			}
			else
			{
				$strpagepath = $strimguploadpath;
				
				$strpagedir = $strrootpath.$strpagepath;
				
				$newfile = $strpagedir.$newfilename;
			//	$range=copy($upfile,$newfile);
				chmod ($upfile, 0777);
				
			//	unlink($upfile);
				
				$updatePageImageQuery = "UPDATE site_colours SET imageBackground = :image WHERE recordID = :recordID";
				$strType = "update";
				$arrdbparams = array (
								"image" => $newfilename,
								"recordID" => $pageRecordID
							);
				$updatePageImage = query($conn, $updatePageImageQuery, $strType, $arrdbparams);
				
				$strsuccess = "New page image added";
			}
			
			$strcmd = "";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Colours</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddPage() {
					document.form.cmd.value="addPage";
					document.form.submit();
				}
				function jsviewPage(pageRecordID) {
					document.form.cmd.value="viewPage";
					document.form.pageRecordID.value=pageRecordID;
					document.form.submit();
				}
				function jsdeletePage(pageRecordID) {
					if(confirm("Are you sure you want to delete this page?"))
					{
						document.form.cmd.value="deletePage";
						document.form.pageRecordID.value=pageRecordID;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertPage() {
					document.form.cmd.value='insertPage';
					document.form.submit();
				}
				function jsupdatePage() {
					if ($('#form').valid()) {
						document.form.cmd.value='updatePage';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsinsertImage() {
					document.form.cmd.value='insertImage';
					document.form.submit();
				}
				function jsaddSiteBlock() {
					if ($('#form').valid()) {
						document.form.cmd.value='addSiteBlock';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jsdeleteSiteBlocks() {
					if ($(".blockPosList li.selected").length > 0)
					{
						if(confirm("Are you sure you want to delete this block(s)?"))
						{
							var deleteBlocks = "";
							$(".blockPosList li.selected").each(function (index, item) {
								deleteBlocks += $(item).attr("id")+",";
							});
							$("#blockPositionID").val($(".blockPosList li.selected").attr("data-pos"));
							//console.log($(".blockPosList li.selected").parent().attr("id"));
							deleteBlocks = deleteBlocks.replace(/,\s*$/, "");
							//console.log(deleteImages);
							document.form.cmd.value="deleteBlocks";
							document.form.deleteBlocks.value=deleteBlocks;
							document.form.submit();
						}
						else
						{
							return false;
						}
					}
					else
					{
						alert("Please selected the block(s) you wish to delete");
					}
					
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
				
				$().ready(function() {
					$(".blockPosList").on('click', 'li', function (e) {
						if (e.ctrlKey || e.metaKey) {
							$(this).toggleClass("selected");
						} else {
							$(this).addClass("selected").siblings().removeClass('selected');
						}
						$(this).parent().parent().siblings().children("ul").children().removeClass('selected');
					}).sortable({
						delay: 150, //Needed to prevent accidental drag when trying to select
						revert: 0,
						helper: function (e, item) {
							//Basically, if you grab an unhighlighted item to drag, it will deselect (unhighlight) everything else
							if (!item.hasClass('selected')) {
								item.addClass('selected').siblings().removeClass('selected');
							}
							//console.log(item);
							
							//Clone the selected items into an array
							//console.log(item.parent().children('.selected'));
							var elements = item.parent().children('.selected').clone();
							
							//Add a property to `item` called 'multidrag` that contains the 
							//  selected items, then remove the selected items from the source list
							item.data('multidrag', elements).siblings('.selected').remove();
							
							//Now the selected items exist in memory, attached to the `item`,
							//  so we can access them later when we get to the `stop()` callback
							
							//Create the helper
							var helper = $('<li/>');
							return helper.append(elements);
						},
						stop: function( event, ui ) {
							//Now we access those items that we stored in `item`s data!
							var elements = ui.item.data('multidrag');
							ui.item.after(elements).remove();
							
							var dataArray = [];
							var i = 0;
							var sortType = "";
							$(elements).each(function(index, item) {
								//console.log($(item).parent().is("ul#unassignedBrands"));
								//console.log($(item).parent().is("ul#assignedBrands"));
								//console.log($(item));
								//console.log($(item).attr('data-order'));
								var oldOrder = $(item).attr("data-order");
								
								$(item).parent().children("li").each(function(index, item) {
									$(item).attr("data-order", $(item).index() + 1);
								});
								
								var newOrder = $(item).attr("data-order");
								//console.log("New order: "+parseInt(newOrder)+" Old order: "+parseInt(oldOrder));
								if (parseInt(newOrder) > parseInt(oldOrder))
								{
									sortType = "DESC";
								}
								else if (parseInt(newOrder) < parseInt(oldOrder))
								{
									sortType = "ASC";
								}
								
								dataArray[i] = {};
								dataArray[i]["pageOrder"] = $(item).attr("data-order");
								dataArray[i]["positionID"] = $(item).attr("data-pos");
								dataArray[i]["recordID"] = $(item).attr("id");
								dataArray[i]["sitePageID"] = $("#pageRecordID").attr("value");
								dataArray[i]["type"] = "page";
								dataArray[i]["cmd"] = "reorder";
								//console.log(dataArray);
								i++;
							});
							dataArray.sort(function(a,b) {
								if (sortType == "ASC")
								{
									//console.log("sort ascending");
									return parseInt(a.pageOrder) - parseInt(b.pageOrder);
								}
								else if (sortType == "DESC")
								{
									//console.log("sort descending");
									return parseInt(b.pageOrder) - parseInt(a.pageOrder);
								}
							});
							//console.log(dataArray);
							$.ajax({
								type: "POST",
								url: "includes/ajax_siteblockrelationmanagement.php",
								data:{ array : dataArray }
							});
						}
					}).disableSelection();
				});
			</script>
			<?php
		
			print ("<form action='cms-colours.php' class='uniForm col-sm-12' method='post' name='form' id='form' enctype='multipart/form-data' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				print ("<input type='hidden' name='pageRecordID' id='pageRecordID' value='$pageRecordID'/>");
				print ("<input type='hidden' name='blockPositionID' id='blockPositionID' />");
						
				$strdbsql = "SELECT * FROM site_colours WHERE recordID = 1";
				$strType = "single";
				$pageDetails = query($conn, $strdbsql, $strType);
				/*print ("<pre>");
				print_r($pageDetails);
				print ("</pre>");*/
				
				print("<div class='row'>");
					print("<div class='col-md-3 crop-sm-right'>");
						print("<div class='section'>");
							print("<fieldset class='inlineLabels'>");
							
								print("<legend>Colours</legend>");

								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_primary' class='control-label'>Primary Colour</label>");
										print("<div class='input-group colour-picker colorpicker-component demo demo-auto'>");
											print("<input type='text' class='form-control' id='frm_primary' name='frm_primary' value='".$pageDetails['colourPrimary']."'/>");
											print("<span class='input-group-addon'><i></i></span>");
										print("</div>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_secondary' class='control-label'>Secondary Colour</label>");
										print("<div class='input-group colour-picker colorpicker-component demo demo-auto'>");
											print("<input type='text' class='form-control' id='frm_secondary' name='frm_secondary' value='".$pageDetails['colourSecondary']."'/>");
											print("<span class='input-group-addon'><i></i></span>");
										print("</div>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_tertiary' class='control-label'>Tertiary Colour</label>");
										print("<div class='input-group colour-picker colorpicker-component demo demo-auto'>");
											print("<input type='text' class='form-control' id='frm_tertiary' name='frm_tertiary' value='".$pageDetails['colourTertiary']."'/>");
											print("<span class='input-group-addon'><i></i></span>");
										print("</div>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_base' class='control-label'>Base Colour</label>");
										print("<div class='input-group colour-picker colorpicker-component demo demo-auto'>");
											print("<input type='text' class='form-control' id='frm_base' name='frm_base' value='".$pageDetails['colourBase']."'/>");
											print("<span class='input-group-addon'><i></i></span>");
										print("</div>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_backgroundCol' class='control-label'>Background Colour</label>");
										print("<div class='input-group colour-picker colorpicker-component demo demo-auto'>");
											print("<input type='text' class='form-control' id='frm_backgroundCol' name='frm_backgroundCol' value='".$pageDetails['colourBackground']."'/>");
											print("<span class='input-group-addon'><i></i></span>");
										print("</div>");
									print("</div>");
								print("</div>");
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_backgroundImg' class='control-label'>Background Image <i class='fa fa-info-circle pull-right' data-toggle='tooltip' data-placement='top' title='Use the ADD NEW IMAGE section to add images to this list. Images must be less than 8MB and should have no spaces or brackets in their filename.'></i></label>");
										print("<select id='frm_backgroundImg' name='frm_backgroundImg' class='form-control' >");
											print("<option value=''>None</option>");
												$dh = opendir("../images/themes/");
												while (false !== ($image = readdir($dh))) {
													if ($image != "." && $image != "..") {
														if($image == $pageDetails['imageBackground'])
														{
															$strselected = " selected";
														}
														else
														{
															$strselected = "";
														}
														print ("<option value='".$image."'".$strselected.">".$image."</option>");
													}
												}
										print ("</select>");
									print("</div>");
								print("</div>");
								
							print("</fieldset>");
						print("</div>");
						
						print("<div class='section'>");
							print("<fieldset class='inlineLabels'>");
		
							print("<legend>Add New Image</legend>");
							
							print("<div class='row'>");
								print("<div class='form-group'>");
									print("<label for='frm_newimage' class='control-label'>Image</label>");
									print("<div class='row'>");
										print("<div class='col-xs-9' style='padding-right: 7.5px;'>");
											print("<input type='file' class='form-control' id='frm_newimage' name='frm_newimage' style='padding:6px 12px;'/>");
										print("</div>");
										print("<div class='col-xs-3' style='padding-left: 7.5px;'>");
											print("<button onclick='return jsinsertImage();' type='submit' class='btn btn-success circle pull-left'><i class='fa fa-plus'></i></button>");
										print("</div>");
									print("</div>");
								print("</div>");
							print("</div>");
						
							print("</fieldset>");
						print("</div>");
						
					print("</div>");
					
					$strdbsql = "SELECT * FROM site_company_details WHERE recordID = 1";
					$strType = "single";
					$company = query($conn, $strdbsql, $strType);
					
					$strdbsql = "SELECT * FROM site_pages WHERE displayInNav = 1 AND parentPageID IS NULL";
					$strType = "multi";
					$links = query($conn, $strdbsql, $strType);
					
					$strgallery = "SELECT * FROM site_gallery_images ORDER BY galleryOrder";
					$strType = "single";
					$slide = query($conn, $strgallery, $strType);
					
					$strgallery = "SELECT * FROM site_gallery_images ORDER BY galleryOrder LIMIT 6";
					$strType = "multi";
					$thumbnails = query($conn, $strgallery, $strType);
					
					print("<div class='col-md-6 crop-sm-both'>");
						print("<div class='preview-back' style='");
							if($pageDetails['imageBackground'] != ''){
								print("background-image:url(/images/themes/".$pageDetails['imageBackground'].");");
							}
							print("background-color:".$pageDetails['colourBackground'].";");
						print("'>");
							
							print("<div class='row'>");
								print("<div class='col-xs-12'>");
									print("<div class='preview-div preview-head' style='background:".$pageDetails['colourBase'].";border-color:".$pageDetails['colourPrimary'].";'>");
										print("<img src='/images/company/".$company['companyLogo']."'/>");
										
										print("<div class='row'>");
											print("<div class='col-xs-12'>");
												print("<span style='color:".$pageDetails['colourSecondary'].";text-shadow:0px 0px 0px ".$pageDetails['colourSecondary'].";'><i class='fa fa-phone'></i> ".$company['companyPhone']."</span>");
												print("<span style='color:".$pageDetails['colourSecondary'].";text-shadow:0px 0px 0px ".$pageDetails['colourSecondary'].";'><i class='fa fa-envelope'></i> ".$company['companyEmail']."</span>");
												if($company['companyEmail2'] != "")print("<span style='color:".$pageDetails['colourSecondary'].";text-shadow:0px 0px 0px ".$pageDetails['colourSecondary'].";'>".$company['companyEmail2']."</span><br/>");
												print("<span style='color:".$pageDetails['colourSecondary'].";text-shadow:0px 0px 0px ".$pageDetails['colourSecondary'].";'><i class='fa fa-map-marker'></i> ".$company['addressNumber']." ".$company['addressStreet'].", ".$company['addressCity'].", ".$company['addressCounty'].", ".$company['addressPostCode'].", ".$company['addressCountry']."</span>");
											print("</div>");
										print("</div>");
										
										print("<ul>");
										
										foreach($links AS $link){
										
											print("<li style='color:".$pageDetails['colourTertiary'].";text-shadow:0px 0px 0px ".$pageDetails['colourTertiary'].";'>".$link['metaPageLink']."</li>");
										
										}
										
										print("</ul>");
										
									print("</div>");
								print("</div>");
							print("</div>");
							
							print("<div class='row'>");
								print("<div class='col-xs-12'>");
									print("<div class='preview-slider' style='background-image:url(/images/gallery/".$slide['imageLocation'].");'></div>");
								print("</div>");
							print("</div>");
							
							print("<div class='row'>");
								print("<div class='col-xs-12'>");
									print("<div class='preview-thumbnails'>");
									
									foreach($thumbnails AS $thumbnail)
									{
									print("<div class='col-xs-2' style='padding:0 5px;'>");
										print("<div class='thumbnail-box' style='background-image:url(/images/gallery/".$thumbnail['imageLocation'].");'></div>");
									print("</div>");
									}
							
									print("</div>");
								print("</div>");
							print("</div>");
							
							print("<div class='row' style='margin-top:10px;'>");
								print("<div class='col-xs-8' style='padding-right:8.5px;'>");
									print("<div class='preview-div' style='background-color:".$pageDetails['colourBase'].";'>");
										print("<h1 style='color:".$pageDetails['colourPrimary'].";font-family:".$pageDetails['font'].";'>".$company['companyName']."</h1>");
										print("<p style='color:".$pageDetails['colourTertiary'].";'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu quam finibus, malesuada nunc ac, auctor eros. Nam et facilisis libero. Aliquam erat volutpat. Praesent dui justo, sagittis non dictum blandit, semper et justo. Proin sed accumsan felis. Cras accumsan, lectus non auctor mollis, dui sapien feugiat augue, eget volutpat nunc orci eu arcu. Ut malesuada rhoncus urna, ullamcorper hendrerit metus hendrerit vitae. Sed placerat massa a auctor tincidunt. In feugiat justo nec sapien elementum bibendum. Phasellus sodales fringilla massa placerat faucibus. In molestie risus a turpis vestibulum tincidunt. In non ullamcorper sem. In egestas scelerisque scelerisque. Vestibulum tellus justo, semper ut malesuada a, tincidunt et sapien.</p>");
									print("</div>");
								print("</div>");
								print("<div class='col-xs-4' style='padding-left:3px;'>");
									print("<div class='preview-div' style='background-color:".$pageDetails['colourBase'].";'>");
										print("<h2><i class='fa fa-check'></i> Check availability</h2>");
										print("<div class='row'>");
											print("<div class='col-xs-6' style='padding-right:5px;'>");
												print("<input placeholder='Check In' disabled/>");
											print("</div>");
											print("<div class='col-xs-6' style='padding-left:5px;'>");
												print("<input placeholder='Check Out' disabled/>");
											print("</div>");
										print("</div>");
										print("<div class='row'>");
											print("<div class='col-xs-12' style='margin-top:10px;'>");
												print("<input placeholder='Executive Single' disabled/>");
											print("</div>");
										print("</div>");
									print("</div>");
								print("</div>");
							print("</div>");

						print("</div>");
					print("</div>");
					
					print("<div class='col-sm-3 crop-sm-left'>");
						print("<div class='section'>");
							print("<fieldset class='inlineLabels'>");
								print("<legend>Font</legend>");
								print("<div class='row'>");
									print("<div class='form-group'>");
										print("<label for='frm_font' class='control-label'>Primary Font</label>");
										print("<input type='text' class='form-control' id='frm_font' name='frm_font' value='".$pageDetails['font']."'/>");
									print("</div>");
								print("</div>");
							print("</fieldset>");
						print("</div>");
					print("</div>");
				print("</div>");
				
				print("<div class='row'>");
					print("<div class='col-xs-6'>");
						print("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>");
					print("</div>");
					print("<div class='col-xs-6'>");
						print("<button onclick='return jsupdatePage();' type='submit' class='btn btn-success pull-right'>Save</button> ");
					print("</div>");
				print("</div>");
					
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#pages-table').DataTable();
		});
		
		$(function(){
			$('.colour-picker').colorpicker();
			/*$('.colour-picker').colorpicker({
				customClass: 'colorpicker-2x',
				sliders: {
					saturation: {
						maxLeft: 200,
						maxTop: 200
					},
					hue: {
						maxTop: 200
					},
					alpha: {
						maxTop: 200
					}
				}
			});*/
		});
	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>