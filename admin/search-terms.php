<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * search-terms.php				                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '

// ************* Common page setup ******************** //
//=====================================================//
	session_start(); //stores session variables such as access levels and logon details
	$strpage = "search-terms"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['searchTermsID'])) $searchTermsID = $_REQUEST['searchTermsID']; else $searchTermsID = "";
	$strItemType = 2;
	
	switch($strcmd)
	{
		case "insertSearchTerms":
		case "updateSearchTerms":
			
			$searchTerms = serialize(explode(",", $_POST['frm_searchterms']));
			
			$arrdbparams = array(
								"location" => $_POST['frm_location'],
								"searchTermsArray" => $searchTerms
							);
			
			if ($strcmd == "insertSearchTerms")
			{	
				$strdbsql = "INSERT INTO site_search_pages (location, searchTermsArray) 
							VALUES (:location, :searchTermsArray)";
				$strType = "insert";
			}
			elseif ($strcmd == "updateSearchTerms")
			{
				$strdbsql = "UPDATE site_search_pages SET location = :location, searchTermsArray = :searchTermsArray WHERE recordID = :recordID";
				$arrdbparams['recordID'] = $searchTermsID;
				$strType = "update";
			}
			
			$updateSearchTerms = query($conn, $strdbsql, $strType, $arrdbparams);
			
			if ($strcmd == "insertSearchTerms")
			{
				$searchTermsID = $updateSearchTerms;
			}
			
			if ($strcmd == "insertSearchTerms")
			{
				if ($updateSearchTerms > 0)
				{
					$strsuccess = "Search terms item successfully added";
				}
				elseif ($updateSearchTerms == 0)
				{
					$strerror = "An error occurred while adding the search terms";
				}
			}
			elseif ($strcmd == "updateSearchTerms")
			{
				if ($updateSearchTerms <= 1)
				{
					$strsuccess = "Search terms item successfully updated";
				}
				elseif ($updateSearchTerms > 1)
				{
					$strwarning = "An error may have occurred while updating the search terms";
				}
			}
			
			$strcmd = "viewSearchTerms";
			
		break;
		
		case "deleteSearchTerms":
			
			$strdbsql = "DELETE FROM site_search_pages WHERE recordID = :recordID";
			$strType = "delete";
			$arrdbparams = array( "recordID" => $searchTermsID );
			$deleteSearchTerms = query($conn, $strdbsql, $strType, $arrdbparams);
			
			$strcmd = "";
			
		break;
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Search Terms Control</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsaddSearchTerms() {
					document.form.cmd.value="addSearchTerms";
					document.form.submit();
				}
				function jsviewSearchTerms(searchTermsID) {
					document.form.cmd.value="viewSearchTerms";
					document.form.searchTermsID.value=searchTermsID;
					document.form.submit();
				}
				function jsdeleteSearchTerms(searchTermsID) {
					if(confirm("Are you sure you want to delete these search terms?"))
					{
						document.form.cmd.value="deleteSearchTerms";
						document.form.searchTermsID.value=searchTermsID;
						document.form.submit();
					}
					else
					{
						return false;
					}
				}
				function jsinsertSearchTerms() {
					document.form.cmd.value='insertSearchTerms';
					document.form.submit();
				}
				function jsupdateSearchTerms() {
					if ($('#form').valid()) {
						document.form.cmd.value='updateSearchTerms';
						document.form.submit();
					} else {
						return false;
					}
				}
				function jscancel(cmdValue) {
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
			</script>
			<?php
		
			print ("<form action='search-terms.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				print ("<input type='hidden' name='searchTermsID' id='searchTermsID' value='".$searchTermsID."' />");
				
				switch($strcmd)
				{
					case "viewSearchTerms":
					case "addSearchTerms":
						
						if ($strcmd == "viewSearchTerms")
						{
							$strdbsql = "SELECT * FROM site_search_pages WHERE recordID = :recordID";
							$strType = "single";
							$arrdbparams = array("recordID" => $searchTermsID);
							$searchTermsDetails = query($conn, $strdbsql, $strType, $arrdbparams);
							/*print ("<pre>");
							print_r($searchTermsDetails);
							print ("</pre>");*/
							$searchTerms = implode(",", unserialize($searchTermsDetails['searchTermsArray']));
							
							print ("<fieldset class='inlineLabels'> <legend>Change Search Terms Details</legend>");
						}
						elseif ($strcmd == "addSearchTerms")
						{
							$searchTermsDetails = array(
												"location" => "",
												"searchTermsArray" => ""
											);
							$date = "";
							print ("<fieldset class='inlineLabels'> <legend>Add Search Terms Details</legend>");
						}
						
						print ("<div class='form-group'>
							<label for='frm_location' class='col-sm-2 control-label'>Location</label>
							<div class='col-sm-10'>
							  <input style='width:800px;' type='text' class='form-control' id='frm_location' name='frm_location' value='".$searchTermsDetails['location']."'>
							</div>
						  </div>
						  <div class='form-group'>
							<label for='frm_searchterms' class='col-sm-2 control-label'>Search Terms</label>
							<div class='col-sm-8'>
							  <textarea class='form-control' id='frm_searchterms' name='frm_searchterms' rows='12'>".$searchTerms."</textarea>
							</div>
						  </div>");
					print ("<div class='form-group'>
								<div class='col-sm-10'>");
								if ($strcmd == "addSearchTerms")
								{
								  print ("<button onclick='return jsinsertSearchTerms();' type='submit' class='btn btn-success'>Update Search Terms</button> ");
								}
								elseif ($strcmd == "viewSearchTerms")
								{
								  print ("<button onclick='return jsupdateSearchTerms();' type='submit' class='btn btn-success'>Update Search Terms</button> ");
								}
								print  ("<button onclick='return jscancel(\"\");' class='btn btn-danger'>Cancel</button>
								</div>
						  </div>");
						print ("</fieldset>");
					
						break;
						
					default:
						
						$strdbsql = "SELECT * FROM site_search_pages";
						$strType = "multi";
						$siteSearchTerms = query($conn, $strdbsql, $strType);
						
						print ("<button onclick='return jsaddSearchTerms();' type='submit' class='btn btn-success'>Add New</button>");
						print ("<br/>");
						print ("<br/>");
						print ("<table id='terms-table' class='table table-striped table-bordered table-hover table-condensed' >");
							print ("<thead><tr>");
								print ("<th>Location</th>");
								print ("<th>Search Terms</th>");
								print ("<th>Edit</th>");
								print ("<th>Delete</th>");
							print ("</tr></thead><tbody>");
							foreach($siteSearchTerms AS $siteSearchTerm)
							{
								print ("<tr>");
									print ("<td>".$siteSearchTerm['location']."</td>");
									print ("<td>".implode(",", unserialize($siteSearchTerm['searchTermsArray']))."</td>");
									print ("<td><button onclick='return jsviewSearchTerms(\"".$siteSearchTerm['recordID']."\");' type='submit' class='btn btn-primary'>Update Search Terms</button></td>");
									print ("<td><button onclick='return jsdeleteSearchTerms(\"".$siteSearchTerm['recordID']."\");' type='submit' class='btn btn-danger'>Delete Search Terms</button></td>");
								print ("</tr>");
							}
							print ("</tbody>");
						print ("</table>");
						print ("<br/>");
						print ("<button onclick='return jsaddSearchTerms();' type='submit' class='btn btn-success'>Add New</button>");
						
						
						break;
				}
				
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('#terms-table').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>