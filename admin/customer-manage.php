<?php
//	' ********************************************************************** '
//	' * by MillerWaite                                                     * '
//	' * Email address: support@millerwaite.com                             * '
//	' *                                                                    * '
//	' *                                                                    * '
//	' * customer-manage.php				                                   * '
//	' ********************************************************************** '
//	' ********************************************************************** '
//  ' * Parameters required                                                * '
//  ' * ===================                                                * '
//  ' * none                                                               * '
//  ' *--------------------------------------------------------------------* '
//  ' * Called from which pages                                            * '
//  ' * =======================                                            * '
//  ' * default web page                                                   * '
//	' ********************************************************************** '
//  ' * Description                                                        * '
//  ' * ===========                                                        * '
//	' ********************************************************************** '


// ************* Common page setup ******************** //
	//=====================================================//

	session_start(); //stores session variables such as access levels and logon details
	$strpage = "customer-manage"; //define the current page
	include("includes/inc_sitecommon.php"); // Standard include used throughout site
	$conn = connect(); // Open Connection to Database

	
	// *********** Custom Page Processing ***************** //
	//=====================================================//
	
	
	//details submitted
	if (isset($_REQUEST['cmd'])) $strcmd = $_REQUEST['cmd']; else $strcmd = "";
	if (isset($_REQUEST['userID'])) $userID = $_REQUEST['userID']; else $userID = "";
	
	switch($strcmd)
	{
		case "userSearch":
		
			$customersQuery = "SELECT * FROM customer WHERE ";
			$strType = "multi";
			$arrdbparams = array();
			$boolCriteria = false;
			
			if ($_POST['frm_fields'] == "any") $strwherejoin = " OR ";
			if ($_POST['frm_fields'] == "all") $strwherejoin = " AND ";
			
			if ($_REQUEST['frm_match'] == "partial") $strwhereoperator = "LIKE";
			if ($_REQUEST['frm_match'] == "exact") $strwhereoperator = "=";
			
			$firstname = $_POST['frm_firstname'];
			if(!empty($_POST['frm_firstname']))
			{
				if($boolCriteria)
				{
					$customersQuery .= " ".$strwherejoin." ";
				}
				if ($_REQUEST['frm_match'] == "partial")
				{
					$_POST['frm_firstname'] = "%".$_POST['frm_firstname']."%";
				}
				$customersQuery .= "firstname ".$strwhereoperator." :firstname";
				$arrdbparams['firstname'] = $_POST['frm_firstname'];
				$boolCriteria = true;
			}
			$surname = $_POST['frm_surname'];
			if(!empty($_POST['frm_surname']))
			{
				if($boolCriteria)
				{
					$customersQuery .= " ".$strwherejoin." ";
				}
				if ($_REQUEST['frm_match'] == "partial")
				{
					$_POST['frm_surname'] = "%".$_POST['frm_surname']."%";
				}
				$customersQuery .= "surname ".$strwhereoperator." :surname";
				$arrdbparams['surname'] = $_POST['frm_surname'];
				$boolCriteria = true;
			}
			$companyname = $_POST['frm_companyname'];
			if(!empty($_POST['frm_companyname']))
			{
				if($boolCriteria)
				{
					$customersQuery .= " ".$strwherejoin." ";
				}
				if ($_REQUEST['frm_match'] == "partial")
				{
					$_POST['frm_companyname'] = "%".$_POST['frm_companyname']."%";
				}
				$customersQuery .= "company ".$strwhereoperator." :company";
				$arrdbparams['company'] = $_POST['frm_companyname'];
				$boolCriteria = true;
			}
			$email = $_POST['frm_email'];
			if(!empty($_POST['frm_email']))
			{
				if($boolCriteria)
				{
					$customersQuery .= " ".$strwherejoin." ";
				}
				if ($_REQUEST['frm_match'] == "partial")
				{
					$_POST['frm_email'] = "%".$_POST['frm_email']."%";
				}
				$customersQuery .= "email ".$strwhereoperator." :email";
				$arrdbparams['email'] = $_POST['frm_email'];
				$boolCriteria = true;
			}
			
			if($boolCriteria)
			{
				$customersDetails = query($conn, $customersQuery, $strType, $arrdbparams);
			}
			else
			{
				$strwarning = "Please specify search criteria";
			}
			
		break;
		
		case "viewDetails":
			
			$customerDetailsQuery = "SELECT * FROM customer WHERE recordID = :userID";
			$strType = "single";
			$arrdbparam = array( "userID" => $userID );
			$customerDetails = query($conn, $customerDetailsQuery, $strType, $arrdbparam);
			
			if (count($customerDetails) < 1)
			{
				$strerror = "Matching customer not found";
			}
			else
			{
				if(!empty($customerDetails['firstname']) && !empty($customerDetails['surname']))
				{
					$strName = $customerDetails['firstname']." ".$customerDetails['surname'];
					if(!empty($customerDetails['custTitle']))
					{
						$strName = $customerDetails['custTitle']." ".$strName;
					}
				}
			}
		break;
	
	}
	
	// ************* Common page setup ******************** //
	//=====================================================//

	include("includes/inc_header.php");
	include("includes/inc_sidebar.php");

	// ************* Custom Page Code ******************** //
	//=====================================================//
	print("<div class='mainContent'>");
		print("<div class='whitePage'>");

			print ("<h1>Customer Manage</h1>");
			
			//Print out debug and error messages
			if ($booldebug AND $strpage != 'login') { print ("<div class='notification-warning'><h3>Debug</h3><p>Username = ".$_SESSION['username']."</p></div>"); }
			if ($strerror != '') { print ("<div id='not-erro' class='notification-error not-erro'><h3>Error</h3><p>$strerror</p></div>"); }
			if ($strwarning != '') { print ("<div id='not-warn' class='notification-warning not-warn'><h3>Warning</h3><p>$strwarning</p></div>"); }
			if ($strsuccess != '') { print ("<div id='not-succ' class='notification-success not-succ'><h3>Success</h3><p>$strsuccess</p></div>"); }
	
			?>
			<script language='Javascript'>
				function jsuserSearch() {
					document.form.cmd.value='userSearch';
					document.form.submit();
				}
				function jsorderHistory(userID) {
					document.form.userID.value = userID;
					document.form.action="sales-orders.php";
					document.form.cmd.value = "";
					document.form.submit();
				}
				function jsfullDetails(userID) {
					document.form.userID.value = userID;
					document.form.cmd.value='viewDetails';
					document.form.submit();
				}
				function jsupdateList() {
					var e = document.getElementById("user");
					var selected = e.options[e.selectedIndex].value;
					document.form.userID.value = e.options[e.selectedIndex].value;
					if (selected != "all" && selected != "recent")
					{
						document.form.cmd.value='viewDetails';
					}
					else
					{
						document.form.userID.value='';
						document.form.cmd.value='';
					}
					document.form.submit();
				}
				function jscancel(cmdValue) {
					document.form.userID.value='';
					document.form.cmd.value=cmdValue;
					document.form.submit();
				}
			</script>
			<?php
		
			print ("<form action='customer-manage.php' class='uniForm' method='post' name='form' id='form' accept-charset='UTF-8'>");
				print ("<input type='hidden' name='cmd' id='cmd'/>");
				print ("<input type='hidden' name='userID' id='userID' value='".$userID."'/>");
				
				switch($strcmd)
				{
					case "viewDetails":
					
						if (count($customerDetails) > 0)
						{
							print ("<fieldset class='inlineLabels'> <legend>JH Benson &amp; Co. Member Details ".$customerDetails['email']." - ".date("d M Y", time())."</legend>
									<div class='form-group'>
										<label class='col-sm-2 control-label'>Full Name</label>
										<div class='col-sm-10'>".
											$strName
										."</div>
									</div>
									<div class='form-group'>
										<label class='col-sm-2 control-label'>Username</label>
										<div class='col-sm-10'>".
											$customerDetails['username']
										."</div>
									</div>
									<div class='form-group'>
										<label class='col-sm-2 control-label'>Company</label>
										<div class='col-sm-10'>".
											$customerDetails['company']
										."</div>
									</div>
									<div class='form-group'>
										<label class='col-sm-2 control-label'>Phone</label>
										<div class='col-sm-10'>".
											$customerDetails['telephone']
										."</div>
									</div>
									<div class='form-group'>
										<label class='col-sm-2 control-label'>Fax</label>
										<div class='col-sm-10'>".
											$customerDetails['fax']
										."</div>
									</div>
									<div class='form-group'>
										<label class='col-sm-2 control-label'>Email</label>
										<div class='col-sm-10'>".
											$customerDetails['email']
										."</div>
									</div>");
									
									$getCustomerAddressesQuery = "SELECT * FROM customer_address WHERE customerID = :customerID";
									$strType = "multi";
									$arrdbparam = array( "customerID" => $customerDetails['recordID'] );
									$customerAddresses = query($conn, $getCustomerAddressesQuery, $strType, $arrdbparam);
									
									if(count($customerAddresses) > 0)
									{
										print ("<legend>Addresses</legend>");
										print ("<table class='table table-striped table-bordered table-hover table-condensed' >");
											print ("<tbody>");
												foreach($customerAddresses AS $customerAddress)
												{
													$boolDescAmended = false;
													if($customerDetails['defaultDeliveryAdd'] == $customerAddress['recordID'])
													{
														$customerAddress['addDescription'] = "Default Delivery";
														$boolDescAmended = true;
													}
													if($customerDetails['defaultBillingAdd'] == $customerAddress['recordID'])
													{
														if($boolDescAmended)
														{
															$customerAddress['addDescription'] .= ", Default Billing";
														}
														else
														{
															$customerAddress['addDescription'] = "Default Billing";
														}
													}
													if($boolDescAmended)
													{
														$customerAddress['addDescription'] .= " Address";
													}
													print ("<tr>");
														print ("<th>".$customerAddress['addDescription']."</th>");
													print ("</tr>");
													print ("<tr>");
														print ("<td>");
															if (!empty($customerAddress['firstname']) && !empty($customerAddress['surname']))
															{
																$strAddressName = $customerAddress['firstname']." ".$customerAddress['surname'];
																if (!empty($customerAddress['title']))
																{
																	$strAddressName = $customerAddress['title']." ".$strAddressName;
																}
															}
															else
															{
																$strAddressName = $strName." - Derived from account name as none entered for this address";
															}
															print ($strAddressName."<br/>");
															print ($customerAddress['add1']."<br/>");
															if (!empty($customerAddress['add2']))
															{
																print ($customerAddress['add2']."<br/>");
															}
															if (!empty($customerAddress['add3']))
															{
																print ($customerAddress['add3']."<br/>");
															}
															print ($customerAddress['town']."<br/>");
															print ($customerAddress['county']."<br/>");
															print ($customerAddress['country']."<br/>");
															print ($customerAddress['postcode']."<br/>");
														print ("</td>");
													print ("</tr>");
												}
											print ("</tbody>");
										print ("</table>");
									}
									else
									{
										print ("<p>No address information could be found</p>");
									}
									
									print ("<div class='form-group'>
										<div class='col-sm-10'>
											<button onclick='return jsorderHistory(\"".$customerDetails['recordID']."\")' class='btn btn-primary' type='submit'>Order History</button>
											<button onclick='return jscancel(\"\")' class='btn btn-primary' type='submit'>Return to List</button>
										</div>
									</div>
								</fieldset>");
						}
						break;
						
					default:
					case "userSearch":
					
						print ("<fieldset class='inlineLabels'> <legend>Search for User</legend>
							<div class='form-group'>
								<label for='frm_firstname' class='col-sm-2 control-label'>First Name</label>
								<div class='col-sm-10'>
									<input style='width:200px;' type='text' class='form-control' id='frm_firstname' name='frm_firstname' value='".$firstname."'>
								</div>
							</div>
							<div class='form-group'>
								<label for='frm_surname' class='col-sm-2 control-label'>Surname</label>
								<div class='col-sm-10'>
								  <input style='width:200px;' type='text' class='form-control' id='frm_surname' name='frm_surname' value='".$surname."'>
								</div>
							</div>
							<div class='form-group'>
								<label for='frm_companyname' class='col-sm-2 control-label'>Company</label>
								<div class='col-sm-10'>
								  <input style='width:200px;' type='text' class='form-control' id='frm_companyname' name='frm_companyname' value='".$companyname."'>
								</div>
							</div>
							<div class='form-group'>
								<label for='frm_email' class='col-sm-2 control-label'>Email</label>
								<div class='col-sm-10'>
								  <input style='width:200px;' type='text' class='form-control' id='frm_email' name='frm_email' value='".$email."'>
								</div>
							</div>
							<div class='form-group'>
								<label for='frm_fields' class='col-sm-2 control-label'>Search Options</label>
								<div class='col-sm-10'>
									<select name='frm_fields' id='frm_fields' class='form-control' style='width: 200px; margin-bottom: 14px;'>");
										$strselected = "";
										if(isset($_REQUEST['frm_fields']) && $_REQUEST['frm_fields'] == "all")
										{
											$strselected = " selected";
										}			
										print ("<option value='all'".$strselected.">All none empty fields</option>");
								
										$strselected = "";
										if(isset($_REQUEST['frm_fields']) && $_REQUEST['frm_fields'] == "any")
										{
											$strselected = " selected";
										}
										print ("<option value='any'".$strselected.">Any field</option>
									</select> 
									<select name='frm_match' id='frm_match' class='form-control' style='width: 200px;'>");
										$strselected = "";
										if(isset($_REQUEST['frm_match']) && $_REQUEST['frm_match'] == "partial")
										{
											$strselected = " selected";
										}			
										print ("<option value='partial'".$strselected.">Partial Match</option>");
								
										$strselected = "";
										if(isset($_REQUEST['frm_match']) && $_REQUEST['frm_match'] == "exact")
										{
											$strselected = " selected";
										}
										print ("<option value='exact'".$strselected.">Exact Match</option>
									</select>
								</div>
							</div>
						  <div class='form-group'>
							<div class='col-sm-10'><button onclick='return jsuserSearch();' type='submit' class='btn btn-success'>Search Users</button>
							</div>
						  </div>
						</fieldset>");
						
						if ($strcmd == "userSearch")
						{
							print ("<table class='member-list table table-striped table-bordered table-hover table-condensed' >");
								print ("<thead><tr>");
									print ("<th>Full Name</th>");
									print ("<th>Company</th>");
									print ("<th>Email Address</th>");
									print ("<th>Orders</th>");
									print ("<th>Details</th>");
								print ("</tr></thead><tbody>");
								/*if(count($customersDetails) > 0)
								{*/
								foreach($customersDetails AS $customerDetails)
								{
									if(!empty($customerDetails['firstname']) && !empty($customerDetails['surname']))
									{
										$strName = $customerDetails['firstname']." ".$customerDetails['surname'];
										if(!empty($customerDetails['custTitle']))
										{
											$strName = $customerDetails['custTitle']." ".$strName;
										}
									}
									print ("<tr>
												<td>".$strName."</td>
												<td>".$customerDetails['company']."</td>
												<td><a href='mailto:".$customerDetails['email']."'>".$customerDetails['email']."</a></td>
												<td><button onclick='return jsorderHistory(\"".$customerDetails['recordID']."\")' class='btn btn-primary' type='submit'>Order History</button></td>
												<td><button onclick='return jsfullDetails(\"".$customerDetails['recordID']."\")' class='btn btn-primary' type='submit'>Full Contact Details</button></td>
											</tr>");
								}
								/*}
								else
								{
									print ("<tr><td colspan='5'>No matching users found</td></tr>");
								}*/
								print ("</tbody>");
							print ("</table>");
						}
						else
						{
							$date = date("d M Y");
							$threemonthsago = mktime(0, 0, 0, date("m")-3, date("d"), date("Y"));
							
							if($userID == "") $userID = "all";
							
							print ("<br/>");
							if ($userID == "all") {
								$legend = "<legend>JH Benson &amp; Co. Member List - $date</legend>";
								$customerDetailsQuery = "SELECT * FROM customer WHERE recordID > 1 ORDER BY surname ASC";
							}
							else if ($userID == "recent")
							{
								$legend = "<legend>JH Benson &amp; Co. Active Member List - ".date("d M Y",$threemonthsago)." to $date</legend>";
								$customerDetailsQuery = "SELECT DISTINCT customer.* FROM customer INNER JOIN order_header ON order_header.customerID = customer.recordID WHERE order_header.timestampPayment >= $threemonthsago AND recordID > 1 ORDER BY surname ASC";
							
							}
							print ("<fieldset class='inlineLabels'>".$legend."
										<div class='form-group'>
											<label for='user' class='col-sm-2 control-label'>Show User</label>
											<div class='col-sm-10'>
												<select name='user' id='user' class='form-control' style='width: 200px; display: inline-block; margin-right: 20px;'>
													<option value='all'");
													if ($userID == "all") print (" selected");
													print(">All Users</option>
													<option value='recent'");
													if ($userID == "recent") print (" selected");
													print (">Users active in last 3 months</option>");
													
													$customerListQuery = "SELECT DISTINCT recordID, email FROM customer WHERE recordID > 1 ORDER BY email ASC";
													$strType = "multi";
													$customerList = query($conn, $customerListQuery, $strType);
													
													foreach($customerList AS $customer)
													{
														if ($customer['recordID'] == $userID && $strcmd != "")
														{
															$strselected = " selected";
														}
														else
														{
															$strselected = "";
														}
														print ("<option value='".$customer['recordID']."'".$strselected.">".$customer['email']."</option>");
													}
												print ("</select>
												<button onclick='return jsupdateList()' class='btn btn-primary' type='submit'>Update List</button>
											</div>
										</div>
										
									</fieldset>");
									
							$strType = "multi";
							$customerList = query($conn, $customerDetailsQuery, $strType);
							
							print ("<table class='member-list table table-striped table-bordered table-hover table-condensed' >");
								print ("<thead><tr>");
									print ("<th>Full Name</th>");
									print ("<th>Company</th>");
									print ("<th>Email Address</th>");
									print ("<th>Orders</th>");
									print ("<th>Details</th>");
								print ("</tr></thead><tbody>");
								/*if(count($customerList) > 0)
								{*/
									//$count = 0;
									$char = "";
									foreach($customerList AS $customerDetails)
									{
										/*if ($char != chr(ord(ucwords($customerDetails['surname']))))
										{
											$char = chr(ord(ucwords($customerDetails['surname'])));
											$flag = 1;
										}
										if ($char == chr(ord(ucwords($customerDetails['surname']))) && $flag == 1)
										{
											print ("<tr><td colspan='5'><h2>".$char."</h2></td></tr>");
											$flag = 0;
										}*/
										if(!empty($customerDetails['firstname']) && !empty($customerDetails['surname']))
										{
											$strName = $customerDetails['firstname']." ".$customerDetails['surname'];
											if(!empty($customerDetails['custTitle']))
											{
												$strName = $customerDetails['custTitle']." ".$strName;
											}
										}
										print ("<tr>
													<td>".$strName."</td>
													<td>".$customerDetails['company']."</td>
													<td><a href='mailto:".$customerDetails['email']."'>".$customerDetails['email']."</a></td>
													<td><button onclick='return jsorderHistory(\"".$customerDetails['recordID']."\")' class='btn btn-primary' type='submit'>Order History</button></td>
													<td><button onclick='return jsfullDetails(\"".$customerDetails['recordID']."\")' class='btn btn-primary' type='submit'>Full Contact Details</button></td>
												</tr>");
										//$count++;
									}
									//print ("<tr><th>Total Users</th><th colspan='4'>".$count."</th></tr>");
								/*}
								else
								{
									print ("<tr><td colspan='5'>No matching users found</td></tr>");
								}*/
								print ("</tbody>");
							print ("</table>");
						}
						
						break;
				}
				
			print ("</form>");
		print("</div>");
	print("</div>");
	
		?>
		<script language='Javascript'>
		$().ready(function() {

			// validate signup form on keyup and submit
			$("#form").validate({
				rules: {
				},
				messages: {
				}
			});
			
			$('.member-list').DataTable();
		});

	</script>
	
<?php

	
	// ************* Common page setup ******************** //
	//=====================================================//
	include("includes/inc_footer.php"); //include the page footer
	$conn = null; // close the Database connection after all processing
?>